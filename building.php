<?php
include('theme/db.php');
include('eclipse/classes/Queries.php');
include('objects/ErrorMessages.php');
include('objects/SearchData.php');
include('objects/LanguageQuery.php');
include('objects/FormatData.php');
include('eclipse/classes/DisplayHTML.php');
include('objects/Graphics.php');
//include('objects/SpecPages.php');
include('objects/StatsPackage.php');

include("assets/php/krumo/class.krumo.php");


require_once 'dist/Mobile_Detect.php';
$detect = new Mobile_Detect;


// initiate the StatsPackage class
$stats = new StatsPackage();
// Initiate the Graphics class
$graphics = new Graphics();
// Initiate the Queries class
$queries = new Queries();
//Initiate DisplayHTML
$displayHTML = new DisplayHTML();
//Initiate the Search Data class
$searchData = new SearchData();
//INitiate the Spec Pages
//$specPages = new SpecPages();
// Language Query
$languageQuery = new LanguageQuery();
$langArray = $languageQuery->getLanguageAndID();
$lang = $langArray[0];
$thelang = $langArray[0];
$lang_id = $langArray[1];
//           $language_query_string = 'select * from languages_dynamic ld, languages l, languages2 l2 where ld.lang_id = ' . $lang_id . ' and l.lang_id = ' . $lang_id . ' and l2.lang_id = ' . $lang_id . '';
//           $language_query = mysql_query($language_query_string)or die("language query error: ". mysql_error());
//           $language = mysql_fetch_array($language_query);

$language = array();
$language_query_string = "select * from languages_dynamic ld, languages l, languages2 l2 where ld.lang_id =? and l.lang_id =? and l2.lang_id =?";
$language_query_statement = $db->prepare($language_query_string);
$language_query_statement->bind_param('iii', mysqli_real_escape_string($db, $lang_id), mysqli_real_escape_string($db, $lang_id), mysqli_real_escape_string($db, $lang_id));
if ($language_query_statement->execute()) {
    $result = $language_query_statement->get_result();
    while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
        $language = $row;
    }
}
$language_query_statement->close();

$no_vacancies = 0;

//           $language_query_string2 = 'select * from languages_dynamic ld, languages l, languages2 l2 where ld.lang_id = ' . $lang_id . ' and l.lang_id = ' . $lang_id . ' and l2.lang_id = ' . $lang_id . '';
//           $language_query2 = mysql_query($language_query_string2)or die("language query2 error: ". mysql_error());
//           $language2 = mysql_fetch_array($language_query2);   


$language2 = array();
$language2_query_string = "select * from languages_dynamic ld, languages l, languages2 l2 where ld.lang_id =? and l.lang_id =? and l2.lang_id =?";
$language2_query_statement = $db->prepare($language2_query_string);
$language2_query_statement->bind_param('iii', mysqli_real_escape_string($db, $lang_id), mysqli_real_escape_string($db, $lang_id), mysqli_real_escape_string($db, $lang_id));
if ($language2_query_statement->execute()) {
    $result = $language2_query_statement->get_result();
    while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
        $language2 = $row;
    }
}
$language2_query_statement->close();




// Theme Query
$theme_query = mysql_query("select * from theme");
$theme = mysql_fetch_array($theme_query);

//setting locale for time, date, money, etc
date_default_timezone_set('America/New_York');
setlocale(LC_ALL, $language['locale_string']);
// for reporting
$today = date("Y/m/d");
$todaytimestamp = strtotime($today);


//Initiate FormatData
$formatData = new FormatData();

$province_selected = $searchData->get_province_selected($language, $lang, $db);
$region_selected = $searchData->get_region_selected($language, $lang, $db);
$sub_region_selected = $searchData->get_sub_region_selected($language, $lang, $db);
$size_selected = $searchData->get_size_selected();
$tag_selected = $searchData->get_tag_selected();
$size_selected_from = $searchData->get_size_selected_from();
$size_selected_to = $searchData->get_size_selected_to();
$availability_selected_array = $searchData->get_availability_selected();
$availability_notafter = $availability_selected_array['availability_notafter'];
$availability_notbefore = $availability_selected_array['availability_notbefore'];
$pageCurrent = '';






$regional_branding_count = 0;
?>
<?php
//Initiate the building variables
$building_selected = $_REQUEST['building'];
//   $buildings_query_string = "select * from buildings where building_code='" . $building_selected . "' and lang=" . $lang_id;
//   $buildings_query = mysql_query($buildings_query_string);
//   $Building = mysql_fetch_array($buildings_query);



$buildings_query_string = "select building_index, building_internal_identifier, lang, building_code, building_id, building_type, city, contact_info, country, description, flyer_header, modification_date, street_address, number_of_floors, floors_above_ground, floors_below_ground, available_space, office_area, industrial_area, postal_code, province, province_code, region, sub_region, leasing_node, renovated, retail_area, building_name, measurement_unit, thumbnail, static_map, total_space, uri, year_of_building, parking_stalls, parking_ratio, typical_floor_size, file_name, file_uri, dirty, orientation, add_rent_operating, add_rent_realty, add_rent_power, add_rent_total, truck_doors, drivein_doors, clear_height, power_text, marketing_text, sort_streetnumber, sort_streetname, latitude, longitude, client_name, new_development, no_vacancies from buildings where building_code=? and lang=?";
$buildings_query_statement = $db->prepare($buildings_query_string);
//bind parameters for markers, where (s = string, i = integer, d = double,  b = blob)
$buildings_query_statement->bind_param('si', mysqli_real_escape_string($db, $building_selected), mysqli_real_escape_string($db, $lang_id));
//execute query
$buildings_query_statement->execute();
//bind result variables
$buildings_query_statement->bind_result($building_index, $building_internal_identifier, $lang, $building_code, $building_id, $building_type, $city, $contact_info, $country, $description, $flyer_header, $modification_date, $street_address, $number_of_floors, $floors_above_ground, $floors_below_ground, $available_space, $office_area, $industrial_area, $postal_code, $province, $province_code, $region, $sub_region, $leasing_node, $renovated, $retail_area, $building_name, $measurement_unit, $thumbnail, $static_map, $total_space, $uri, $year_of_building, $parking_stalls, $parking_ratio, $typical_floor_size, $file_name, $file_uri, $dirty, $orientation, $add_rent_operating, $add_rent_realty, $add_rent_power, $add_rent_total, $truck_doors, $drivein_doors, $clear_height, $power_text, $marketing_text, $sort_streetnumber, $sort_streetname, $latitude, $longitude, $client_name, $new_development, $no_vacancies);
//must fetch the info after it's bound. Unsure why but there it is.
$buildings_query_statement->fetch();
$buildings_query_statement->close();


$building_id = $building_id;
$building_name = $building_name;
$building_code = $building_code;
$building_type = $building_type;
$building_street = $street_address;
$building_city = $city;
$building_province = $province;
$building_region = $region;
$building_sub_region = $sub_region;
$building_total_space = $total_space;
$building_measurement_unit = "SF";
$building_desc = strip_tags($description, '<p>,<br>,<br />,<strong>,<u>,<b>,<i>,<ul>,<li>,<ol>,<a>');
$building_desc = str_replace('style=', '', $building_desc);
$building_contact = $contact_info;
$building_header = $flyer_header;
if ($building_header == "") {
    $building_header = "images/no_header.png";
}
$building_url = $uri;

$building_availability = $modification_date;
$building_postal_code = $postal_code;
$building_total_space = $total_space;
$building_office_area = $office_area;
$building_retail_area = $retail_area;
$building_industrial_area = $industrial_area;
$building_floors = $number_of_floors;
$building_year_built = $year_of_building;
$building_year_renovated = $renovated;
$building_typical_floor_size = $typical_floor_size;
$building_parking_stalls = $parking_stalls;
$building_available_space = $available_space;

$additionalCostsPresent = 0;

if ($add_rent_operating == 0) {
    $building_add_rent_operating = '';
} else {
    $building_add_rent_operating = (money_format('%.2n', (double) $add_rent_operating));
    $additionalCostsPresent++;
}

if ($add_rent_realty == 0) {
    $building_add_rent_realty = '';
} else {
    $building_add_rent_realty = money_format('%.2n', (double) $add_rent_realty);
    $additionalCostsPresent++;
}

if ($add_rent_power == 0) {
    $building_add_rent_power = '';
} else {
    $building_add_rent_power = money_format('%.2n', (double) $add_rent_power);
}

if ($add_rent_total == 0) {
    $building_add_rent_total = '';
} else {
    $building_add_rent_total = money_format('%.2n', (double) $add_rent_total);
    $additionalCostsPresent++;
}



$building_truck_doors = $truck_doors;
$building_drivein_doors = $drivein_doors;
$building_clear_height = $clear_height;
$building_power_text = $power_text;
$building_orientation = $orientation;
$building_marketing_text = stripslashes($marketing_text);
$building_latitude = $latitude;
$building_longitude = $longitude;

$suitetype_selected = $searchData->get_suitetype_selected($language);

if (isset($_REQUEST['show_no_vacancy']) && $_REQUEST['show_no_vacancy'] != '') {
    $show_no_vacancy = $_REQUEST['show_no_vacancy'];
} else {
    if ($theme['no_vacancy_default'] == 0) {
        $show_no_vacancy = "no";
    } else if ($theme['no_vacancy_default'] == 1) {
        $show_no_vacancy = "yes";
    }
}


$video_query = $queries->file_extension_query($building_code, "mp4", $lang_id, $db);
if (is_bool($video_query)) {
    $video_count = 0;
} else {
    $video_count = count($video_query);
}

$video_query2 = $queries->file_extension_query($building_code, "mp4", $lang_id, $db);
if (is_bool($video_query2)) {
    $video_count = 0;
} else {
    $video_count2 = count($video_query2);
}

$pdf_query = $queries->file_extension_query($building_code, "pdf", $lang_id, $db);
if (is_bool($pdf_query)) {
    $pdf_count = 0;
} else {
    $pdf_count = count($pdf_query);
}





$pdf_queryBPF = $queries->file_extension_query($building_code, "pdf", $lang_id, $db);
if (is_bool($pdf_queryBPF)) {
    $pdf_countBPF = 0;
} else {
    $pdf_countBPF = count($pdf_queryBPF);
}

$docx_query = $queries->file_extension_query($building_code, "docx", $lang_id, $db);
if (is_bool($docx_query)) {
    $docx_count = 0;
} else {
    $docx_count = count($docx_query);
}

$docx_queryBPF = $queries->file_extension_query($building_code, "docx", $lang_id, $db);
if (is_bool($docx_queryBPF)) {
    $docx_count = 0;
} else {
    $docx_count = count($docx_queryBPF);
}



$ignore = array('mp4', 'pdf', 'avi', 'docx');
$other_docs_query = $queries->unusual_document_extensions($building_code, $ignore, $lang_id, $db);
if (is_bool($other_docs_query)) {
    $other_docs_count = 0;
} else {
    $other_docs_count = count($other_docs_query);
}

$other_docs_queryBPF = $queries->unusual_document_extensions($building_code, $ignore, $lang_id, $db);
if (is_bool($other_docs_queryBPF)) {
    $other_docs_count = 0;
} else {
    $other_docs_count = count($other_docs_queryBPF);
}

$threedee_query_building = $queries->threedee_query_building($building_selected, $lang_id, $db);
$threedee_building_present = count($threedee_query_building);

$threedee_query_building2 = $queries->threedee_query_building($building_selected, $lang_id, $db);
$threedee_building_present2 = count($threedee_query_building2);






$preview_images_query = $queries->preview_images_query($building_code, $lang_id, $db);
$preview_images_count = count($preview_images_query);



$pdf_asbuilt_query = $queries->pdf_asbuilt_query($building_code, $lang_id, $db);
$pdf_asbuilt_count = count($pdf_asbuilt_query);






$multimedia_count = 0;
if (($video_count + $preview_images_count ) > 0) {
    $multimedia_count = 1;
}



//<div class="item"><img src="images/slide2.jpg"></div>
// Hero Carousel
$carouselHTML = '';


if (count($pdf_query) > 0) {
    foreach($pdf_query as $pdf) {
//krumo($pdf);
        $document_name = $pdf['file_name'];
        
        if ($pdf['thumb'] != '') {
            $document_thumb = 'images/documents/thumb_' . $pdf['thumb'];
        } else {
            $document_thumb = 'images/no_thumb_200.png';
        }

//        $HTML .= '<div class="col fs_gallery-thumbnail">'
//                . '<a class="featuredocclick" href="images/twodee_files/' . $document_name . '" title="' . $document_name . ' "  target="_blank" ><img src="' . $document_thumb . '" class="img-fluid" /></a>'
//                . '</div>';
        
        $carouselHTML .= '<div class="item cover"><a href="images/documents/'.$document_name.'" data-fancybox="gallery"><img src="'.$document_thumb.'"></a></div>';
    }
}




// add building preview images to the carousel too
//$preview_images_query2
if ($preview_images_count > 0) {
    foreach ($preview_images_query as $buildingimages) {

        if (strlen($buildingimages['file_name']) > 30) {
            $galleryimagename = substr($buildingimages['file_name'], 0, 30).'...';
        } else {
            $galleryimagename = $buildingimages['file_name'];
        }

        $gallerythumbnail = str_ireplace('.' . $buildingimages['extension'], '', $buildingimages['file_name']) . "_thumbc." . $buildingimages['extension'];
    //            file_name.extension
//        $HTML .='<div class="col fs_gallery-thumbnail"><a data-fancybox="gallery" href="images/suite_preview_images/' . $buildingimages['file_name'] . '" title="' . $buildingimages['display_file_name'] . '"><img src="images/suite_preview_images/' . $gallerythumbnail . '" alt="' . $buildingimages['display_file_name'] . '"  class="img-fluid"></a></div>';
        $carouselHTML .= '<div class="item cover"><a href="images/preview_images/'.$buildingimages['file_name'].'" data-fancybox="gallery"><img src="images/preview_images/'.$gallerythumbnail.'"></a></div>';

              
        
    }
}


//$galleryPdfAsBuiltHTML = '';
//
//if (count($pdf_asbuilt_query) > 0) {
//    foreach($pdf_asbuilt_query as $pdf) {
//
//        $document_name = $pdf['plan_name'];
//        
//        if ($pdf['plan_thumb'] != '') {
//            $document_thumb = 'images/twodee_files/thumbc_' . $pdf['plan_thumb'];
//        } else {
//            $document_thumb = 'images/no_thumb_700.png';
//        }
//
////        $HTML .= '<div class="col fs_gallery-thumbnail">'
////                . '<a class="featuredocclick" href="images/twodee_files/' . $document_name . '" title="' . $document_name . ' "  target="_blank" ><img src="' . $document_thumb . '" class="img-fluid" /></a>'
////                . '</div>';
//        
//        $carouselHTML .= '<div class="item cover"><a data-fancybox="gallery" data-src="images/twodee_files/' . $pdf['plan_thumb'] . '" href="images/twodee_files/'.$pdf['plan_name'].'" data-fancybox="gallery"><img src="'.$document_thumb.'"></a></div>';
//        
//        $galleryPdfAsBuiltHTML .= '<div class="col fs_gallery-thumbnail"><a data-fancybox="gallery" data-src="images/twodee_files/' . $pdf['plan_thumb'] . '" href="images/twodee_files/'.$pdf['plan_name'].'" target="_blank" rel="noopener noreferrer"><img src="'.$document_thumb.'" class="img-fluid"></a></div>';
//        
//    }
//}

$theCarousel = 0;
if ($carouselHTML !== ""){
    $theCarousel = 1;
}

$pdf_asbuilt_query = $queries->pdf_asbuilt_query($building_code, $lang_id, $db);
$pdf_asbuilt_count = count($pdf_asbuilt_query);







$brokerpackage_check = 0;





$building_thumbnail_query_string = $queries->get_building_thumbnail_query_string($lang_id, $building_code);
//    $building_thumbnail_query = mysql_query($building_thumbnail_query_string) or die ("building thumbnail image error: " . mysql_error());
//    $building_thumbnail = mysql_fetch_array($building_thumbnail_query);
// process buildings query string
$building_thumbnail__string = $building_thumbnail_query_string[0];
//    $building_thumbnail__string .=" LIMIT ".$postnumbers." OFFSET ".$offset;
$pageCurrent = '';
//    $building_thumbnail__string = str_replace("and allb.building_name like '%none%'", "", $building_thumbnail__string);
if ($building_thumbnail__statement = $db->prepare($building_thumbnail__string)) {
    // carry on
} else {
    trigger_error('Wrong SQL: ' . $building_thumbnail__string . ' Error: ' . $db->error, E_USER_ERROR);
}
// prep dynamic variable query
$param_type = '';
$n = count($building_thumbnail_query_string[1]);
for ($i = 0; $i < $n; $i++) {
    $param_type .= $building_thumbnail_query_string[1][$i];
}

/* with call_user_func_array, array params must be passed by reference */
$a_params[] = & $param_type;

for ($i = 0; $i < $n; $i++) {
    /* with call_user_func_array, array params must be passed by reference */
    $a_params[] = & $building_thumbnail_query_string[2][$i];
}

// call_user_func_array is how we can pass dynamic quantities of variables to mysqli prepared statements.
call_user_func_array(array($building_thumbnail__statement, 'bind_param'), $a_params);
$building_thumbnail = array();
if ($building_thumbnail__statement->execute()) {
    $result = $building_thumbnail__statement->get_result();
    while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
        $building_thumbnail = $row;
    }
} else {
    trigger_error('Could not execute building_thumbnail query statement: Error: ' . $db->error, E_ALL);
}
$building_thumbnail__statement->close();




    $building_thumbnail_image_name = '';
    $building_thumbnail_thumb_name = '';
    if (isset($building_thumbnail['file_name']) && $building_thumbnail['file_name'] != '') {
        $building_thumbnail_image_name = "images/preview_images/" . $building_thumbnail['file_name'];
        $building_thumbnail_thumb_name = "images/preview_images/" . str_replace('.jpg', '_thumb.jpg', $building_thumbnail['file_name']);
        $building_thumbnail_image_name_list = "images/preview_images/" . str_replace('.jpg', '_listthumb.jpg', $building_thumbnail['file_name']);

    } else {
        $building_thumbnail_image_name = "images/no_thumb_700.png";
        $building_thumbnail_thumb_name = "images/no_thumb_700.png";
        $building_thumbnail_image_name_list = "images/no_thumb_200.png";
    }


//$building_thumbnail_image_name = '';
//$building_thumbnail_image_name = "images/preview_images/" . $building_thumbnail['file_name'];
//$building_thumbnail_thumb_name = "images/preview_images/" . str_replace('.jpg', '_thumb.jpg', $building_thumbnail['file_name']);
//
//if ($building_thumbnail_image_name == "images/preview_images/") {
//    $building_thumbnail_image_name = "images/no_thumb_700.png";
//}

$building_thumbnail_count = 0;
if ($building_thumbnail_thumb_name == "images/no_thumb_700.png") {
    $building_thumbnail_count = 0;
} else {
    $building_thumbnail_count = 1;
}

//BrokerPackage check
if (($building_thumbnail_count + $pdf_count + $preview_images_count + $pdf_asbuilt_count) > 0) {
    $brokerpackage_check = 1;
}

// Statistics Tracking
$stats->trackBuildingpageOpens($todaytimestamp, $today, $building_id, $building_code, $building_city);



// make the image slider string:
$gallery_array_string = "";
$first = 1;


$prev_next_query = $queries->prev_next_query($language, $lang_id);



//echo $prev_next_query;
//    $building_list = mysql_query($prev_next_query) or die ("prev_next_query query error: " . mysql_error());




$preview_images_query_string = $prev_next_query[0];
//echo $preview_images_query_string . "<br><br>";
//echo $prev_next_query[1] . "<br><br>";
//echo $prev_next_query[2] . "<br><br>";

if ($preview_images_query_statement = $db->prepare($preview_images_query_string)) {
    // carry on
} else {
    trigger_error('Wrong SQL: ' . $preview_images_query_string . ' Error: ' . $db->error, E_USER_ERROR);
}

//    $preview_images_query_statement->bind_param($prev_next_query[1], $prev_next_query[2]);
$a_params = array();
// prep dynamic variable query
$param_type = '';
$n = count($prev_next_query[1]);
for ($i = 0; $i < $n; $i++) {
    $param_type .= $prev_next_query[1][$i];
}
/* with call_user_func_array, array params must be passed by reference */
$a_params[] = & $param_type;
for ($i = 0; $i < $n; $i++) {
    /* with call_user_func_array, array params must be passed by reference */
    $a_params[] = & $prev_next_query[2][$i];
}




// call_user_func_array is how we can pass dynamic quantities of variables to mysqli prepared statements.
call_user_func_array(array($preview_images_query_statement, 'bind_param'), $a_params);
$preview_images_query_results = array();
if ($preview_images_query_statement->execute()) {
    $result = $preview_images_query_statement->get_result();
    while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
        $preview_images_query_results[] = $row;
    }
} else {
    trigger_error('Could not execute preview images query statement: Error: ' . $db->error, E_ALL);
}
$preview_images_query_statement->close();




$previous_building_code = "";
$next_building_code = "";
$n = 0;
$found = 0;
$num_builds = count($preview_images_query_results);

$industrial_office_space_selected = '';
$industrial_clear_height_selected = '';
$industrial_shipping_doors_selected = '';
$industrial_drivein_doors_selected = '';
$industrial_electrical_volts_selected = '';
$industrial_electrical_amps_selected = '';
$industrial_parking_stalls_selected = '';
$industrial_trucktrailer_parking_selected = '';
$industrialsearchelement = '';
$advanced_search = '';




// SUITE QUERY 
$suites_query_string_query = $queries->get_suites_query_string(
        $lang_id, $building_code, $size_selected, $size_selected_from, $size_selected_to, $suitetype_selected, 'notag', $availability_notbefore, $availability_notafter, $industrial_office_space_selected, $industrial_clear_height_selected, $industrial_shipping_doors_selected, $industrial_drivein_doors_selected, $industrial_electrical_volts_selected, $industrial_electrical_amps_selected, $industrial_parking_stalls_selected, $industrial_trucktrailer_parking_selected, $industrialsearchelement, $advanced_search, $db
);

//        $suites_query = mysql_query($suites_query_string) or die ("building page suites query error: " . mysql_error());




$suite_query_string = $suites_query_string_query[0];
$a_params = array();
if ($suite_query_statement = $db->prepare($suite_query_string)) {
    // carry on
} else {
    trigger_error('Wrong SQL: ' . $suite_query_string . ' Error: ' . $db->error, E_USER_ERROR);
}
// prep dynamic variable query
$param_type = '';
$n = count($suites_query_string_query[1]);
for ($i = 0; $i < $n; $i++) {
    $param_type .= $suites_query_string_query[1][$i];
}
/* with call_user_func_array, array params must be passed by reference */
$a_params[] = & $param_type;
for ($i = 0; $i < $n; $i++) {
    /* with call_user_func_array, array params must be passed by reference */
    $a_params[] = & $suites_query_string_query[2][$i];
}


// call_user_func_array is how we can pass dynamic quantities of variables to mysqli prepared statements.
call_user_func_array(array($suite_query_statement, 'bind_param'), $a_params);
$suites_query = array();
if ($suite_query_statement->execute()) {
    $result = $suite_query_statement->get_result();
    while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
        $suites_query[] = $row;
    }
} else {
    trigger_error('Could not execute preview images query statement: Error: ' . $db->error, E_ALL);
}
$suite_query_statement->close();




$suite_floor = '';
$suite_area = '';
$suite_name = '';
$suites_count = count($suites_query);

// GO THROUGH THE SUITES AND CREATE TABLE ROWS	
$i = 0;
$suites_html = "";
$suitedivs_html = "";
//	while($row = mysql_fetch_array($suites_query)){


$suites_html = '<table class="table fs_building-suites-table">';

if ($building_type == "Industrial" || $building_type == "Industriel") {
    $suites_html .= '<thead>';
    $suites_html .= '<tr>';
    $suites_html .= '<th class="fs_suite-name">'.$language['suite_text'] .'</th>';
    $suites_html .= '<th class="fs_suite-area">'.$language['size_text_lowercase'] .'<span class="small">/'.$language['building_information_sq_ft_trans'] .'</span></th>';
    $suites_html .= '<th class="fs_suite-availability">'.$language['availability_lowercase_text'].'</th>';
    $suites_html .= '<th class="fs_suite-rent">'.$language['price_text'].'<span class="small">/' . $language['building_information_sq_ft_trans'].'/'.$language['month_abbr'].'</span></th>';
    $suites_html .= '<th class="fs_suite-loading">'.$language['loading_text'].'</th>';
    $suites_html .= '<th class="fs_suite-power">'.$language['power_text'].'</th>';
    $suites_html .= '<th class="fs_suite-ceiling">'.$language['ceiling_text'].'</th>';
    $suites_html .= '</tr>';
    $suites_html .= '</thead>';
} else {
    $suites_html .= '<thead>';
    $suites_html .= '<tr>';
    $suites_html .= '<th class="fs_suite-name">'.$language['suite_text'] .'</th>';
    $suites_html .= '<th class="fs_suite-area">'.$language['size_text_lowercase'] .'<span class="small">/'.$language['building_information_sq_ft_trans'] .'</span></th>';
    $suites_html .= '<th class="fs_suite-availability">'.$language['availability_lowercase_text'].'</th>';
    $suites_html .= '<th class="fs_suite-rent">'.$language['price_text'].'<span class="small">/' . $language['building_information_sq_ft_trans'].'/'.$language['month_abbr'].'</span></th>';
    $suites_html .= '<th class="fs_suite-notes">'.$language['notes_text'].'</th>';
    $suites_html .= '</tr>';
    $suites_html .= '</thead>';}
        
        
        
        
        
        $suites_html .= '<tbody>';
        
        if ($suites_count == 0) {
            if ($building_type == "Retail" || $building_type == "Détail") {
                $suites_html .= '<tr class="'.$suiteTrClass.'">';
                $suites_html .= '<td class="fs_suite-name" colspan="5">' . $language['call_for_availability_text'] . '</td>';
            } else if ($building_type == "Office" || $building_type == "Bureau") {
                $suites_html .= '<tr class="'.$suiteTrClass.'">';
                $suites_html .= '<td class="fs_suite-name" colspan="5">' . $language['all_leased_text'] . '</td>';
            } else { // industrial gets more colspan 
                $suites_html .= '<tr class="'.$suiteTrClass.'">';
                $suites_html .= '<td class="fs_suite-name" colspan="7">' . $language['all_leased_text'] . '</td>';
            }
            
            
            
        } else {
            $areaOutput = '';
            foreach ($suites_query as $row) {
                $suite_description = strip_tags($row['description'], '<p>,<br>,<br />,<strong>,<u>,<b>,<i>,<ul>,<li>,<ol>,<a>');
                $suite_availability = $queries->get_suite_availability($language, $row);
                $suite_net_rentable_area = $queries->get_suite_net_rentable_area($thelang, $row);
                $suite_contiguous_area = $queries->get_suite_contiguous_area($thelang, $row);
                $suite_min_divisible_area = $queries->get_suite_min_divisible($thelang, $row);
                $suite_net_rent = $queries->get_suite_net_rent($language, $row);
                $suite_new = $row['new'];
                $suite_model = $row['model'];
                $suite_leased = $row['leased'];
                $suite_promoted = $row['promoted'];
                $row_format = $queries->output_row_format($suite_leased, $suite_promoted);
                $suite_name = $row['suite_name'];
                $suite_id = $row['suite_id'];

                $areaOutput = $suite_net_rentable_area;

                if ((intval($suite_contiguous_area) != 0 && $suite_contiguous_area != '') && (intval($suite_min_divisible_area) != 0 && $suite_min_divisible_area !='')) {
                    $areaOutput .= '<br> (' . $suite_min_divisible_area . ' | ' . $suite_contiguous_area . ')';
                } else if ((intval($suite_contiguous_area) == 0 || $suite_contiguous_area == '') && (intval($suite_min_divisible_area) != 0 && $suite_min_divisible_area !='')) {
                    $areaOutput .= '<br> (' . $suite_min_divisible_area . ')';
                } else if ((intval($suite_contiguous_area) != 0 && $suite_contiguous_area != '') && (intval($suite_min_divisible_area) == 0 || $suite_min_divisible_area =='')) {
                    $areaOutput .= '<br> (' . $suite_contiguous_area . ')';
                }

            $suiteBadge = '';
            $suiteTrClass = ''; 
             if ($suite_new === 'true') {
             $suiteBadge = '<span class="badge badge-secondary fs_badge-new">New</span>';
             $suiteTrClass = '';
             }
             if ($suite_model === 'true') {
             $suiteBadge = '<span class="badge badge-secondary fs_badge-new">Model</span>';
             $suiteTrClass = '';
             }
             if ($suite_leased === 'true') {
             $suiteBadge = '<span class="badge badge-secondary fs_badge-new">Leased</span>';
             $suiteTrClass = 'leased';
             }
             if ($suite_promoted === 'true') {
             $suiteBadge = '<span class="badge badge-secondary fs_badge-new">Promoted</span>';
             $suiteTrClass = '';
             }





                $contig = 0;
                $suite_add_rent_total = $row['add_rent_total'];
                $suite_type = $row['suite_type'];
                $suite_floor = $row['floor_number'];
                $suite_name = $row['suite_name'];
                $suite_area = $row['net_rentable_area'];

                $spec_qy_string = "select * from suites_specifications where suite_id='" . $suite_id . "' and lang = ".$lang_id."";
                $spec_qy = mysql_query($spec_qy_string);
                $row2 = mysql_fetch_array($spec_qy);

                $shippingDI = $row2['shipping_doors_drive_in'];
                $shippingTL = $row2['shipping_doors_truck'];
                $poweramps = $row2['available_electrical_amps'];
                $powervolts = $row2['available_electrical_volts'];
                $ceiling = $row2['clear_height'];
                $loadingoutputDI = '';
                $loadingoutputTL = '';
                $loadingoutput = '';
                

                if ($shippingDI !== '' && $shippingDI !== "0") {
                    $loadingoutputDI = $shippingDI . " " . $language['drivein_loading_abbreviation'];
                }
                if ($shippingTL !== '' && $shippingTL !== "0") {
                    $loadingoutputTL = $shippingTL . " " . $language['truck_loading_abbreviation'];
                }
                if ($loadingoutputDI != '' && $loadingoutputTL != '') {
                    $loadingoutput = $loadingoutputDI . ", " . $loadingoutputTL;
                } else if ($loadingoutputDI != '' && $loadingoutputTL == '') {
                    $loadingoutput = $loadingoutputDI;
                } else if ($loadingoutputDI == '' && $loadingoutputTL != '') {
                    $loadingoutput = $loadingoutputTL;
                } else {
                    // both are empty. do nothing
                    $loadingoutput = '';
                }



                $poweroutputAmps = '';
                $poweroutputVolts = '';
                if ($poweramps != '' && $poweramps != 0) {
                    $poweroutputAmps = $poweramps . " A";
                }
                if ($powervolts != '' && $powervolts != 0) {
                    $poweroutputVolts = $powervolts . " V";
                }
                if ($poweroutputAmps != '' && $poweroutputVolts != '') {
                    $poweroutput = $poweroutputAmps . ", " . $poweroutputVolts;
                } else if ($poweroutputAmps != '' && $poweroutputVolts == '') {
                    $poweroutput = $poweroutputAmps;
                } else if ($poweroutputAmps == '' && $poweroutputVolts != '') {
                    $poweroutput = $poweroutputVolts;
                } else {
                    // both are empty. leave a space
                    $poweroutput = "&nbsp;";
                }




                $otherstats = '';

                if ($loadingoutput != '') {
                    $otherstats = $otherstats . $loadingoutput;
                }
                if ($poweroutput != '') {
                    if ($otherstats != '') {
                        $otherstats = $otherstats . "<br>" . $poweroutput;
                    } else {
                        $otherstats = $poweroutput;
                    }
                }
                
                

                if ($building_type == "Industrial" || $building_type == "Industriel") {
                    $suites_html .= '<tr class="'.$suiteTrClass.'">';
                    $suites_html .= '<td class="fs_suite-name"><a class="suiteLink" href="suite.php?building='.$building_code.'&suiteid='.$suite_id.'">'.$suite_name.' </a> '.$suiteBadge.'</td>';
                    $suites_html .= '<td class="fs_suite-area">'. $areaOutput .'</td>';
                    $suites_html .= '<td class="fs_suite-availability">'.$suite_availability.'</td>';
                    $suites_html .= '<td class="fs_suite-rent">'.$suite_net_rent.'</td>';
                    $suites_html .= '<td class="fs_suite-loading">'.$loadingoutput.'</td>';
                    $suites_html .= '<td class="fs_suite-power">'.$poweroutput .'</td>';
                    $suites_html .= '<td class="fs_suite-ceiling">' .$ceiling . '</td>';
                    $suites_html .= '</tr>';
                } else {
                    $suites_html .= '<tr class="'.$suiteTrClass.'">';
                    $suites_html .= '<td class="fs_suite-name"><a class="suiteLink" href="suite.php?building='.$building_code.'&suiteid='.$suite_id.'">'.$suite_name.' </a> '.$suiteBadge.'</td>';
                    $suites_html .= '<td class="fs_suite-area">'. $areaOutput .'</td>';
                    $suites_html .= '<td class="fs_suite-availability">'.$suite_availability.'</td>';
                    $suites_html .= '<td class="fs_suite-rent">'.$suite_net_rent.'</td>';
                    $suites_html .= '<td class="fs_suite-notes">'.$suite_description.'</td>';
                    $suites_html .= '</tr>';
                }
            }
        }



$suites_html .= '</tbody>';
$suites_html .= '</table>';

$contacts_html = "";
$contacts_email_html = "";
$contacts_query = $queries->contacts_query($building_selected, $lang_id, $db);
$contacts_count = count($contacts_query);
if (!is_bool($contacts_query)) {
    foreach ($contacts_query as $row) {
        $contacts_html .= $displayHTML->building_contacts_html($row, $theme);
        $contacts_email_html .= ';' . $row['email'];
    }
}

$contacts_html = $contacts_html;






$contacts_tabfooter_html = "";
$contacts_email_html = "";
$contacts_query = $queries->contacts_tabfooter_query($building_selected, $lang_id, $db);
if (!is_bool($contacts_query)) {
    foreach ($contacts_query as $row) {
        $contacts_tabfooter_html .= $displayHTML->building_contacts_tabfooter_html($row, $theme);
    }
}

$allemails = $queries->contacts_tabfooter_emailaddress_query($building_selected, $lang_id, $db);
$allcontacts = '';
foreach ($allemails as $address) {
    $allcontacts .= $address['allemail'];
    $allcontacts .= ';';
}


if ($theme['page_width'] != '') {
    $page_width = $theme['page_width'];
} else {
    $page_width = '1170';
}

$showpdf = 1;
if (isset($theme['print_pdf_optional']) && $theme['print_pdf_optional'] == 0) {
    $showpdf = 0;
}



function format_numbers($value, $language) {
    if ($language['lang_id'] == "1") {
        $formatted_number = number_format((double) $value);
    }
    if ($language['lang_id'] == "0") {
        $formatted_number = number_format((double) $value, 0, ',', ' ');
    }
    return $formatted_number;
}

function checkOrX($value) {
    if ($value == "true" || $value == "Yes") {
        $checkOrX = '<i class="material-icons fs_icon-done">done</i>';
    }
    if ($value == "false" || $value == "No") {
        $checkOrX = '<i class="material-icons fs_icon-clear">clear</i>';
    }
    echo $checkOrX;
}

function checkForEmpty($input) {
    if ($input === '' || $input === '0' || $input === "n/a" || $input === '$0.00' || $input === "/1000") {
        return false;
    } else {
        return true;
    }
}


        $ipaddress = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '';
//        $hostname = ''; // too many pitfalls waiting for DNS which will kill the page load
        $referer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '';
        
        $browser = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '';
//        $screensize = ''; // only available via javascript. Need to ajax-ify this function if we want to capture it.
        $datetime = date("Y/m/d, g:ia", $_SERVER['REQUEST_TIME']) ; // format this YYYY/MM/DD

        
$sections_query_string = $queries->sections_query($building_code, $lang_id, $db);
//$sections_query = mysql_fetch_array($sections_query_string);
//$sections_count = count($sections_query);



//$arr = $displayHTML->features_tab_content($sections_count, $sections_query, $queries);
//
//$features_tab_content_present = $arr['features_tab_content_present'];
//$features_tab_content = $arr['features_tab_content'];        
        $features_tab_content = '';

 	    $secCount = 0;
            while ($row = mysql_fetch_array($sections_query_string)) {
                $features_title = $row['section_title'];
                $features_content = $row['section_content'];
                $features_tab_content .= '<div class="card-title-section"><h4>' . $features_title . '</h4></div>';

                $features_tab_content .= '<div class="row">';
                $features_tab_content .= '<div class="col fs_sections-content">';
                $features_content = str_replace('&nbsp;', '', $features_content);
                $features_tab_content .= strip_tags($features_content, '<p>,<br>,<br />,<strong>,<u>,<b>,<i>,<ul>,<li>,<ol>,<a>');
                $features_tab_content .= '</div></div>';
            $secCount++;
            }	
     
        
?>




<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css?family=Fira+Sans+Condensed|Roboto" rel="stylesheet">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<!--        <link rel="stylesheet" href="lib/owlcarousel/2.2.1/dist/assets/owl.carousel.min.css">
        <link rel="stylesheet" href="lib/owlcarousel/2.2.1/dist/assets/owl.theme.default.min.css">-->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.1.25/jquery.fancybox.min.css" />
        <link rel="stylesheet" href="style.css">
        <link rel="stylesheet" href="building.css">
        <?php include('eclipse/colour_styles.php'); ?>
        <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>

       
    </head>
    <body class="fs_page-building">
        <header id="fs_masthead-building" class="fs_site-header">
        <?php
        	
                echo '<div class="fs_client-header clearfix">';
                if ($lang_id == '1') {
                	include('theme/clientheader.php');
                } else {
                	if (file_exists('theme/clientheader_fr.php')) {
                    	include('theme/clientheader_fr.php');
                    }
					else {
                    	include('theme/clientheader.php');
                    }
              	} 
                echo '</div>';
        	
            ?>
            <div class="fs_utilities fs_building-utilities">
            <div class="fs_container container">
                <div class="row no-gutters">
                    <div class="col">
                        <ul class="nav fs_nav-utilities">
<!--                            <li class="nav-item d-none d-lg-inline-block">
                                <a href="#" class="btn btn-primary fs_btn-back" aria-label="Back">
                                    <i class="material-icons fs_btn-icon">chevron_left</i> <span class="fs_btn-text">Back</span>
                                </a>
                            </li>-->
<!--                            <li class="nav-item d-lg-none">
                                <a href="#" class="nav-link fs_nav-link fs_back-link" aria-label="Back">
                                    <i class="material-icons fs_nav-link-icon">chevron_left</i> <span class="fs_nav-link-text">Back</span>
                                </a>
                            </li>-->
                            <li class="nav-item d-none d-lg-inline-block">
                                <a href="#" class="nav-link fs_nav-link" data-toggle="modal" data-target="#fs_modal-print" aria-label="Print to PDF"><i class="material-icons fs_nav-link-icon">print</i> <span class="fs_nav-link-text"><?php echo $language['print_pdf_text']; ?></span></a>
                            </li>
                            <li class="nav-item d-none d-lg-inline-block">
                                <a href="#" class="nav-link fs_nav-link" data-toggle="modal" data-target="#fs_modal-share" aria-label="Share"><i class="material-icons fs_nav-link-icon">share</i> <span class="fs_nav-link-text"><?php echo $language['share_this_text']; ?></span></a>
                            </li>
<!--                            <li class="nav-item">
                                <a href="#" class="nav-link" data-toggle="modal" data-target="#fs_modal-property-flyer" aria-label="Print to PDF"><i class="material-icons fs_nav-link-icon">insert_drive_file</i> <span class="fs_nav-link-text"><span class="d-none d-lg-inline-block"><?php echo $language['property_flyer_text_firstword']; ?></span> <?php echo $language['property_flyer_text_secondword']; ?></span></a>
                            </li>-->
                            <li class="nav-item">
                                <a class="nav-link fs_nav-link fs_broker-info-link" href="#" data-toggle="modal" data-target="#fs_modal-broker-info" aria-label="Broker Info"><i class="material-icons fs_nav-link-icon fs_icon-broker-info">person</i> <span class="fs_nav-link-text"><span class="d-none d-lg-inline-block"><?php echo $language['broker_info_text_firstword']; ?></span> <?php echo $language['broker_info_text_secondword']; ?></span></a>
                            </li>
                        <?php if ($brokerpackage_check > 0) { ?>
                            <li class="nav-item d-lg-none">
                                <a class="nav-link fs_nav-link fs_broker-package-link" href="#" data-toggle="modal" data-target="#fs_modal-broker-package" aria-label="Download Broker Package"><i class="material-icons fs_nav-link-icon fs_icon-broker-package">chrome_reader_mode</i> <span class="fs_nav-link-text"><?php echo $language['broker_package_button_text']; ?></span></a>
                            </li>
                        <?php } ?>
                        <?php if ($theme['bilingual_fr'] == 1) { ?>
                            <li class="nav-item d-none d-lg-inline-block">
                                <div class="dropdown">
                                    <a class="dropdown-toggle nav-link fs_nav-link" href="#" role="button" id="dropdownLanguage" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons fs_nav-link-icon fs_icon-language">language</i> <span class="fs_nav-link-text"><?php echo $language['language_text']; ?></span><span class="material-icons fs_nav-link-icon">expand_more</span>
                                    </a>
                                <div class="dropdown-menu" aria-labelledby="dropdownLanguage">
                                    <a class="dropdown-item langclick" href="#" data-language="en_CA"><?php echo $language['english_text']; ?></a>
                                    <a class="dropdown-item langclick" href="#" data-language="fr_CA"><?php echo $language['french_text']; ?></a>
                                </div>
                              </div>
                            </li>
                        <?php } ?>
                        </ul>
                    </div>
                    <div class="col col-auto">
                        <ul class="nav">
                        <?php if ($brokerpackage_check > 0) { ?>
                            <li class="nav-item d-none d-lg-inline-block"><a href="#" class="btn btn-primary fs_btn-broker-package" data-toggle="modal" data-target="#fs_modal-broker-package" aria-label="Download Broker Package"><i class="material-icons fs_btn-icon">chrome_reader_mode</i> <span class="fs_btn-text"><?php echo $language['broker_package_button_text']; ?></span></a></li>
                        <?php } ?>
                            <li class="nav-item d-lg-none"><a href="#" class="nav-link fs_nav-link fs_toolbar-link"><i class="material-icons fs_nav-link-icon fs_icon-tools">build</i></a></li>
                        </ul>
                    </div>    
                </div>
            </div>
        </div>
        </header>   
        <?php 
        $hero_file_name = "building_hero_crop_".$building_code.".jpg";
        $hero_file_name_small = "building_hero_small_".$building_code.".jpg";
        
        $hero_file = $hero_file_name;
        
        if (is_file("images/preview_images/" . $hero_file_name)) { 
            
        if ( $detect->isMobile() ) {
            $hero_file = $hero_file_name_small;
        }
        ?>
        <div class="fs_hero-image">
            <a href="images/preview_images/<?php echo $hero_file; ?>" data-fancybox="gallery"><img class="img-fluid" src="images/preview_images/<?php echo $hero_file; ?>" alt="<?php echo $building_name; ?>"></a>
        </div>
        <?php
        }
        else { ?>
        <div class="fs_carousel-container">
            <div id="fs_building-carousel" class="coverflow">
                <?php echo $carouselHTML; ?>
            </div>
        </div>
        <?php
        }
        
        ?>
        
        <main class="fs_site-main fs_building-main">
            <div class="fs_container container">
                <header class="fs_page-header">
                    <h1 class="fs_page-title fs_suite-title"><?php echo $building_name; ?></h1>
                    <h2 class="fs_suite-address"><?php echo $building_street . ', ' . $building_city . ', ' . $building_province . ' ' . $building_postal_code; ?></h2>
                </header>
                <div class="fs_site-content">
                    <?php if($building_marketing_text != '') { ?>
                    <section class="fs_section fs_building-announcement">
                        <div class="alert alert-buildingMarketing fs_alert fs_alert-buildingMarketing alert-info" role="alert">
                            <span class="fs_announcement-icon"><i class="material-icons fs_icon-announcement">feedback</i></span>
                            <span class="fs_alert-text"><?php echo $building_marketing_text; ?></span>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true"><i class="material-icons fs_icon-clear">clear</i></span>
                          </button>
                        </div>
                    </section>
                    <?php } ?>
                    <?php if($building_desc != '') { ?>
                    <section class="fs_section fs_building-notes">
                        <div class="fs_notes">
                            
                                <!--<h3 class="fs_notes-title"><?php // echo $language['building_description_text']; ?></h3>-->
                                <p class="fs_notes-text"><?php echo $building_desc; ?></p>
                            
                        </div>
                    </section>
                    <?php } ?>
                    <div class="row fs_row-main-building">
                        <div class="col-lg-8 col-xl-9 fs_col-primary">
                            <ul class="nav nav-pills fs_nav-pills" id="fs-tab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="fs-info-tab" data-toggle="pill" href="#fs-info" role="tab" aria-controls="fs-info" aria-expanded="true"><i class="material-icons fs_nav-link-icon">business</i> <span class="fs_nav-link-text"><span class="d-none d-lg-inline-block"><?php echo $language['building_information_text']; ?></span><span class="d-lg-none">Info</span></span></a>
                                </li>
                                <li class="nav-item">
                                  <a class="nav-link" id="fs-suites-tab" data-toggle="pill" href="#fs-suites" role="tab" aria-controls="fs-suites" aria-expanded="false"><i class="material-icons fs_nav-link-icon">weekend</i> <span class="fs_nav-link-text"><?php echo $language['suites_plural_lowercase']; ?></span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="fs-map-tab" data-toggle="pill" href="#fs-map" role="tab" aria-controls="fs-map" aria-expanded="false"><i class="material-icons fs_nav-link-icon">map</i> <span class="fs_nav-link-text"><?php echo $language['map_text_map']; ?></span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="fs-media-tab" data-toggle="pill" href="#fs-media" role="tab" aria-controls="fs-media" aria-expanded="false"><i class="material-icons fs_nav-link-icon">filter</i> <span class="fs_nav-link-text"><?php echo $language['media_text']; ?></span></a>
                                </li>
                            </ul>
                            <div class="tab-content" id="fs-tab-content">



                                <div class="tab-pane fade show active" id="fs-info" role="tabpanel" aria-labelledby="fs-info-tab">
                                <?php
                                if ($building_type == "Industrial" || $building_type == "Industriel") {
                                    include("eclipse/includes/building_info_industrial.php");
                                } else if ($building_type == "Office" || $building_type == "Bureau") {
                                    include("eclipse/includes/building_info_office.php");
                                } else if ($building_type == "Retail" || $building_type == "Détail") {
                                    include("eclipse/includes/building_info_retail.php");
                                }
                                ?>                        
                                </div>



                                <div class="tab-pane fade" id="fs-suites" role="tabpanel" aria-labelledby="fs-suites-tab">
                                    <section class="fs_section fs_building-suites">
                                        <?php echo $suites_html; ?>
                                    <?php if ($additionalCostsPresent > 0) { ?>
                                        <div class="other-costs">
                                            <h5 class="other-costs-title"><?php echo $language['additional_costs_text']; ?></h5>
                                            <ul class="other-costs-list list-unstyled">
                                                <li class="result-additional-rent"><?php echo $language['additional_rent_text']; ?> <span class="result-value"><?php echo $building_add_rent_total; ?></span></li>
                                                <li class="result-operating-costs"><?php echo $language['operating_costs_text']; ?> <span class="result-value"><?php echo $building_add_rent_operating; ?></span></li>
                                                <li class="result-realty-tax"><?php echo $language['realty_tax_text']; ?> <span class="result-value"><?php echo $building_add_rent_realty; ?></span></li>
                                            </ul>
                                        </div>
                                    <?php } ?>
                                    </section>
                                </div>
                                <div class="tab-pane fade" id="fs-map" role="tabpanel" aria-labelledby="fs-map-tab">
                                    <section id="fs-map-section" class="fs_section fs_building-map">
                                        
                                        
                                        <div id="themap" class="fs_map-canvas embed-responsive embed-responsive-16by9">
                                        </div>
                                    </section>
                                </div>
                                <div class="tab-pane fade" id="fs-media" role="tabpanel" aria-labelledby="fs-media-tab">
                                    <section class="fs_section fs_building-media">
                                        <h3 class="fs_media-section-title">Documents</h3>
                                        <div class="row fs_gallery fs_gallery-documents">
                                            <?php echo $displayHTML->ifPdf($queries, $pdf_query, $docx_query, $other_docs_query, $graphics); ?>
                                            <?php // echo $galleryPdfAsBuiltHTML; ?>
                                        </div>
                                        <h3 class="fs_media-section-title">Images</h3>
                                        <div class="row fs_gallery fs_gallery-images">
                                            <?php echo $displayHTML->ifImagesAndVideos($language, $video_query2, $preview_images_query, $row); ?>
                                        </div>
                                        <h3 class="fs_media-section-title">Videos</h3>
                                        <div class="row fs_gallery fs_gallery-videos">
                                            <?php echo $displayHTML->getVideos($video_query, $row); ?>
                                        </div>
                                        <h3 class="fs_media-section-title">3D</h3>
                                        <div class="row fs_gallery fs_gallery-3d">
                                            <?php echo $displayHTML->if3D($threedee_query_building, $threedee_building_present); ?>
                                        </div>
                                    </section>
                                </div>
                              </div>
                            </div>
                        <div class="col-lg-4 col-xl-3 fs_col-secondary">
                            <section class="fs_section fs_building-contacts">
                                <ul class="fs_building-contacts-list">
                                    <?php echo $contacts_html; ?>
                                </ul>
                            </section>
                        </div>
                            
                    </div>
                    
                    
                </div>
            </div>
        </main>
        
<div class="modal fade" id="fs_modal-print" tabindex="-1" role="dialog" aria-labelledby="fs_modal-print-label" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="fs_modal-print-label"><?php echo $language['print_pdf_configuration_title_text']; ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <i class="material-icons">clear</i>
        </button>
      </div>
      <div class="modal-body">
          <div class="form-check form-check-inline">
            <label class="form-check-label">
              <input id="view-building" type="radio" name="view" value="building" checked="yes" class="form-check-input"> <?php echo $language['exclude_as_built_text']; ?>
            </label>
          </div>
          <div class="form-check form-check-inline">
            <label class="form-check-label">
              <input id="view-asbuilt" type="radio" name="view" value="asbuilt" class="form-check-input"> <?php echo $language['include_as_built_text']; ?>
            </label>
          </div>
          <button type="button" class="btn btn-primary btn-block btn-modal-print"><?php echo $language['print_text']; ?></button>
      </div>
    </div>
  </div>
</div>
        
        
<div class="modal fade" id="fs_modal-share" tabindex="-1" role="dialog" aria-labelledby="fs_modal-share-label" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
          <h5 class="modal-title" id="fs_modal-share-label"><?php echo $language['share_modal_header']; ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <i class="material-icons">clear</i>
        </button>
      </div>
      <div class="modal-body">
          <div class="alert alert-success d-none" role="alert">Request Sent. Thank you!</div>
          <div class="alert alert-danger d-none" role="alert">There is an error with your form. Please correct it.</div>
          
        <form>
            <div class="form-group">
                <label for="recipient-name" class="form-control-label"><?php echo $language['toname_placeholder_text']; ?></label>
                <input type="text" class="form-control form-control-danger" id="recipient-name">

<!--                <span class="form-control form-control-success d-none"></span>
                <span class="input-success-status sr-only">(success)</span>
                <span class="form-control alert-danger d-none"></span>
                <span class="input-error-status sr-only">(error)</span>-->
            </div>
            <div class="form-group has-feedback">
                <label for="recipient-email" class="control-label"><?php echo $language['toemail_placeholder_text']; ?></label>
                <input type="text" class="form-control form-control-danger" id="recipient-email">
<!--                <span class="glyphicon glyphicon-ok form-control-feedback feedback-success" aria-hidden="true"></span>
                <span class="input-success-status sr-only">(success)</span>
                <span class="glyphicon glyphicon-remove form-control-feedback feedback-error" aria-hidden="true"></span>
                <span class="input-error-status sr-only">(error)</span>-->
            </div>
            <div class="form-group has-feedback">
                <label for="sender-name" class="control-label"><?php echo $language['yourname_placeholder_text']; ?></label>
                <input type="text" class="form-control form-control-danger" id="sender-name">
<!--                <span class="glyphicon glyphicon-ok form-control-feedback feedback-success" aria-hidden="true"></span>
                <span class="input-success-status sr-only">(success)</span>
                <span class="glyphicon glyphicon-remove form-control-feedback feedback-error" aria-hidden="true"></span>
                <span class="input-error-status sr-only">(error)</span>-->
            </div>
            <div class="form-group has-feedback">
                <label for="sender-email" class="control-label"><?php echo $language['youremail_placeholder_text']; ?></label>
                <input type="text" class="form-control form-control-danger" id="sender-email">
<!--                <span class="glyphicon glyphicon-ok form-control-feedback feedback-success" aria-hidden="true"></span>
                <span class="input-success-status sr-only">(success)</span>
                <span class="glyphicon glyphicon-remove form-control-feedback feedback-error" aria-hidden="true"></span>
                <span class="input-error-status sr-only">(error)</span>-->
            </div>
            
            <div class="form-group">
                <label for="bshare-comments" class="control-label">Message:</label>
                <textarea class="form-control" id="bshare-comments"></textarea>
            </div>
            <div class="form-group">
            <label for="cc-me" class="custom-control custom-checkbox">
                <input id="cc-me" type="checkbox" name="cc-me" value="" checked="yes" class="custom-control-input">
                <span class="custom-control-indicator"></span>
                <span class="custom-control-description"><?php echo $language['send_a_copy_text']; ?></span>
            </label>
            </div>
        </form>
          
          
<!--          <form name="modal_building_share" id="modal_building_share">
                <div class="row">
                    <div class="col-sm-6">
                        <input name="bshare_youremail" type="email" id="bshare_youremail" placeholder="<?php echo $language['youremail_placeholder_text']; ?>" class="form-control">
                    </div>
                    <div class="col-sm-6">
                        <input name="bshare_yourname" id="bshare_yourname" type="text" placeholder="<?php echo $language['yourname_placeholder_text']; ?>" class="form-control">
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <input name="bshare_toemail" type="email" id="bshare_toemail" placeholder="<?php echo $language['toemail_placeholder_text']; ?>" class="form-control">
                    </div>
                    <div class="col-sm-6">
                        <input name="bshare_toname" id="bshare_toname" type="text" placeholder="<?php echo $language['toname_placeholder_text']; ?>" class="form-control">
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <textarea name="bshare_notes" id="bshare_notes" placeholder="<?php echo $language['share_placeholder_text1'] . ' ' . $building_street . ' ' . $language['share_placeholder_text2']; ?>" class="form-control" ></textarea>
                        <input name="lang" type="hidden" value="<?php echo $lang; ?>" id="lang" />
                        <input name="building" type="hidden" value="<?php echo $building_code; ?>" id="building" />
                        <input name="urlToShare" type="hidden" value="" id="bshare_query_string" />
                    </div>
                </div>
            </form>-->
          
          <div class="modal-body-bottom">
            <input type="hidden" id="jsuseripaddress" value="<?php echo $ipaddress; ?>">
            <div class="g-recaptcha" id="bshare_captcha" data-sitekey=""></div>
            <button type="button" class="btn btn-primary btn-block btn-modal-share-submit">Share</button>
          </div>
      </div>
    </div>
  </div>
</div>
        
<div class="modal fade" id="fs_modal-property-flyer" tabindex="-1" role="dialog" aria-labelledby="fs_modal-property-flyer-label" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="fs_modal-property-flyer-label">Property Flyer</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <i class="material-icons">clear</i>
        </button>
      </div>
      <div class="modal-body">
          
          <button type="button" class="btn btn-primary btn-block">Print Property Flyer</button>
      </div>
    </div>
  </div>
</div>
        
<div class="modal fade" id="fs_modal-broker-info" tabindex="-1" role="dialog" aria-labelledby="fs_modal-broker-info-label" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="fs_modal-broker-info-label"><?php echo $language['request_info_building_title_text']; ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <i class="material-icons">clear</i>
        </button>
      </div>
      <div class="modal-body">
            <div class="fs_modal-contact-details">
                <ul class="list-unstyled fs_modal-contact-details-list">
                    <li>Margaret Atwood</li>
                    <li>Super Lead Representative</li>
                    <li>Morguard</li>
                    <li>416-555-1234</li>
                </ul>
            </div>
                                        <div class="fs_modal-form-requestinfo">
            <form name="modal_requestinfo" id="modal_requestinfo">
                <div class="row">
                    <div class="col-sm-6">
                        <input name="youremail" type="email" id="youremail" placeholder="<?php echo $language['youremail_placeholder_text']; ?>" class="form-control">
                    </div>
                    <div class="col-sm-6">
                        <input name="yourname" id="yourname" type="text" placeholder="<?php echo $language['yourname_placeholder_text']; ?>" class="form-control">
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <textarea name="notes" id="notes" placeholder="<?php echo $language['notes_placeholder_text1'] . ' ' . $building_street . ' ' . $language['notes_placeholder_text2']; ?>" class="form-control" ></textarea>
                        <input name="lang" type="hidden" value="<?php echo $lang; ?>" id="lang" />
                        <input name="building" type="hidden" value="<?php echo $building_code; ?>" id="building" />
                        <input name="requesttype" type="hidden" value="Building" id="requesttype" />
                    </div>
                </div>
            </form>
            <button type="button" class="btn btn-primary btn-block btn-modal-requestinfo-submit"><?php echo $language['book_a_viewing_text']; ?></button>
      </div>
        <div class="fs_modal-form-infoconfirmation d-none">
            <div style="margin: 0px auto;font-size: 15px;"><?php echo $language['thank_you_info_request_message']; ?></div><br><br>
            <div id="thanks"></div>
        </div><!-- /.fs_modal-form-infoconfirmation --> 
    </div>
  </div>
</div>
</div>
        
<div class="modal fade" id="fs_modal-broker-package" tabindex="-1" role="dialog" aria-labelledby="fs_modal-broker-package-label" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="fs_modal-broker-package-label"><?php echo $language['broker_package_title_text']; ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <i class="material-icons">clear</i>
        </button>
      </div>
      <div class="modal-body">
          <p class="modal-copy"><?php echo $language['broker_package_paragraph1_text']; ?></p>
            <form name="modal_requestbrokerpackage" id="modal_requestbrokerpackage">
                <div class="row">
                    <div class="col-sm-6">
                        <input name="packageemail" type="text" id="packageemail" placeholder="<?php echo $language['youremail_placeholder_text']; ?>" class="form-control">
                        <input name="lang" type="hidden" value="<?php echo $lang; ?>" id="lang" />
                        <input name="building" type="hidden" value="<?php echo $building_code; ?>" id="building" />
                        <input name="packagetype" type="hidden" value="Building" id="packagetype" />
                    </div>
                    <div class="col-sm-6">
                        <input name="packagename" type="text" id="packagename" placeholder="<?php echo $language['yourname_placeholder_text']; ?>" class="form-control">
                    </div>
                </div>
            </form>
            <button type="button" class="btn btn-primary btn-block btn-modal-brokerpackage-submit"><?php echo $language['submit_text']; ?></button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="fs_modal-alerts" tabindex="-1" role="dialog" aria-labelledby="fs_modal-alerts-label" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="fs_modal-alerts-label">Set Alerts</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <i class="material-icons">clear</i>
        </button>
      </div>
      <div class="modal-body">
          
          <button type="button" class="btn btn-primary btn-block">Set Alerts</button>
      </div>
    </div>
  </div>
</div>
        
<div id="fs_toolbar">
    <div class="fs_toolbar-inner">
        <div class="fs_toolbar-actions clearfix">
            <button type="button" class="close" aria-label="Close">
                <i class="material-icons">clear</i>
            </button>
        </div>
        <ul class="nav flex-column fs_toolbar-menu">
            <li class="nav-item">
              <a class="nav-link" href="#collapseLanguageMenu" data-toggle="collapse" aria-expanded="false" aria-controls="collapseLanguageMenu"><i class="material-icons fs_nav-link-icon fs_icon-language">language</i> <span class="fs_nav-link-text">Language</span><i class="material-icons fs_nav-link-icon float-right">expand_more</i></a>
              <div class="collapse" id="collapseLanguageMenu">
                  <ul class="nav flex-column">
                      <li class="nav-item"><a href="#" class="nav-link">English</a></li>
                      <li class="nav-item"><a href="#" class="nav-link">French</a></li>
                  </ul>
              </div>
            </li>
<!--            <li class="nav-item">
              <a href="#" class="nav-link fs_nav-link" data-toggle="modal" data-target="#fs_modal-alerts" aria-label="Set alerts"><i class="material-icons fs_nav-link-icon fs_icon-subscribe">notifications</i> <span class="fs_nav-link-text">Set Alerts</span></a>
            </li>-->
            <li class="nav-item">
              <a href="#" class="nav-link" data-toggle="modal" data-target="#fs_modal-print" aria-label="Print to PDF"><i class="material-icons fs_nav-link-icon">print</i> <span class="fs_nav-link-text"><?php echo $language['print_pdf_text']; ?></span></a>
            </li>
            <li class="nav-item">
              <a href="#" class="nav-link" data-toggle="modal" data-target="#fs_modal-share" aria-label="Share"><i class="material-icons fs_nav-link-icon">share</i> <span class="fs_nav-link-text"><?php echo $language['share_this_text']; ?></span></a>
            </li>
        </ul>
        
    </div>
</div>
        
        <script type="text/javascript">
            //var isMobile = <?php //echo json_encode($mobileDevice); ?>;
        </script>
<!--        Moved Jquery up top to handle some JS that comes in from the 3D code. 
<script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>-->
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>
        <script src="lib/jquery.ui.touch-punch.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB5J3ADwMfQwtmYH0bLpKKaMz8cz-Po2Sk"></script>
<!--        <script src="eclipse/js/gmaps.js"></script>
        <script src="eclipse/js/markerclusterer.min.js"></script>-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.1.25/jquery.fancybox.min.js"></script>
        <!--<script src="lib/owlcarousel/2.2.1/dist/owl.carousel.min.js"></script>-->
        <!-- interpolate, depends on jQ 1.8.0+ -->
        <script src="eclipse/js/coverflow/jquery.interpolate.min.js"></script>
         <!--Coverflow--> 
        <script src="eclipse/js/coverflow/jquery.coverflow.js"></script>
        <!-- TouchSwipe for gallery -->
        <script src="eclipse/js/coverflow/jquery.touchSwipe.min.js"></script>
        <script src="https://www.google.com/recaptcha/api.js?onload=CaptchaCallback&render=explicit" async defer></script>

        
        <!-- <script src="main4.0.5.js"></script> -->
<?php if ($theme['clientjs'] == 1) { 
    include('theme/clientjs.php');
 } ?>
        <script type="text/javascript">
            
            
// This is required to handle our multiple recaptchas on this single page
// they all use the same key but need to be explicitly rendered
// https://developers.google.com/recaptcha/docs/display#explicit_render
var tourcaptcha;
var aitccaptcha;
var bshare_captcha;
var CaptchaCallback = function() {
    // Renders the HTML element with id 'example1' as a reCAPTCHA widget.
    // The id of the reCAPTCHA widget is assigned to 'widgetId1'.
//    tourcaptcha = grecaptcha.render('tourcaptcha', {
//        'sitekey' : '6LfgPBoUAAAAAHYde0AUWfiCTJlKXhyUTAcon9ko'
//    });
//    aitccaptcha = grecaptcha.render('aitccaptcha', {
//        'sitekey' : '6LfgPBoUAAAAAHYde0AUWfiCTJlKXhyUTAcon9ko'
//    });
    bshare_captcha = grecaptcha.render('bshare_captcha', {
        'sitekey' : '6LcsOjkUAAAAAOMAVbfLS3o8KUDhisF1yr4MvLZj'
    });
};
// End multiple recaptcha
            
            
            
            
        $(document).ready(function () {
            //intialize carousel    
        var carouselPresent = <?php echo $theCarousel; ?>;
        
        if (carouselPresent === 1){
            $('.coverflow').coverflow({
                innerAngle : 0,
                outerAngle : 0,
                innerOffset : 0,
                density : 1.25,
                select: function(event, ui){
//                        console.log('here');
                }
            });
        }

                
                $('.fs_toolbar-link').on('click', function(e) {
                    $('#fs_toolbar').addClass('open');
                });
                
                $('#fs_toolbar .close').on('click', function(e) {
                    $('#fs_toolbar').removeClass('open');
                });
            



                function validateEmail()
                {

                    var emailID = document.getElementById('youremail').value;
                    atpos = emailID.indexOf("@");
                    dotpos = emailID.lastIndexOf(".");
                    if (atpos < 1 || (dotpos - atpos < 2))
                    {

                        return false;
                    }
                    return(true);
                }







                $('#fs_modal-broker-info').on('hidden.bs.modal', function (e) {
                    $('.modal-form-requestinfo').removeClass('d-none');
                    $('.modal-form-infoconfirmation').addClass('d-none');
                    $('.btn-modal-requestinfo-submit').removeClass('d-none');
                });


                $('.btn-modal-requestinfo-submit').on('click', function (e) {
                    // VALIDATE BEFORE AJAX
                    var validated = 1;
                    if (document.modal_requestinfo.youremail.value == "") {
                        e.preventDefault();
                        alert("<?php echo $language['form_verification_message_email']; ?>");
                        document.modal_requestinfo.youremail.focus();
                        validated--;
                        return false;
                    }


                    if (!validateEmail())
                    {
<?php if ($lang_id == '1') { ?>
                            alert("Please use a valid Email address.");
<?php } else { ?>
                            alert("S'il vous plaît utiliser une courreil valide.");
<?php } ?>
                        document.modal_requestinfo.youremail.focus();
                        return false;

                    }

                    if (document.modal_requestinfo.yourname.value == "") {
                        e.preventDefault();
                        alert("<?php echo $language['form_verification_message_name']; ?>");
                        document.modal_requestinfo.yourname.focus();
                        validated--;
                        return false;
                    }



                    if (validated > 0) {
                        // END VALIDATE BEFORE AJAX
                        $.ajax({
                            type: "GET",
                            url: "send_inforequest_email.php?<?php echo $_SERVER['QUERY_STRING']; ?>",
                            data: $('form#modal_requestinfo').serialize(),
                            success: function (uniqueid) {
                                $('.fs_modal-form-requestinfo').addClass('d-none');
                                $('.fs_modal-form-infoconfirmation').removeClass('d-none');
                                $('.btn-modal-requestinfo-submit').addClass('d-none');
                                $('#fs_modal-broker-info').scrollTop(0);

                            },
                            error: function () {

                            }
                        });
                    }// end if (validated = 0)
                });















                $('#modal-request-brokerpackage').on('hidden.bs.modal', function (e) {
                    $('.modal-form-brokerpackage').removeClass('hidden');
                    $('.btn-modal-brokerpackage-submit').removeClass('hidden');
                });





                function validatePackageEmail()
                {

                    var emailID2 = document.getElementById('packageemail').value;
                    atpos = emailID2.indexOf("@");
                    dotpos = emailID2.lastIndexOf(".");
                    if (atpos < 1 || (dotpos - atpos < 2))
                    {
                        return false;
                    }
                    return(true);
                }




                $('.btn-modal-brokerpackage-submit').on('click', function (e) {


                    // VALIDATE BEFORE AJAX




                    var validated = 1;
                    if (document.modal_requestbrokerpackage.packageemail.value == "") {
                        e.preventDefault();
                        alert("<?php echo $language['form_verification_message_email']; ?>");
                        document.modal_requestbrokerpackage.packageemail.focus();
                        validated--;
                        return false;
                    }



                    if (!validatePackageEmail())
                    {
<?php if ($lang_id == '1') { ?>
                            alert("Please use a valid Email address.");
<?php } else { ?>
                            alert("S'il vous plaît utiliser une courreil valide.");
<?php } ?>
                        document.modal_requestbrokerpackage.packageemail.focus();
                        return false;

                    }



                    if (document.modal_requestbrokerpackage.packagename.value == "") {
                        e.preventDefault();
                        alert("<?php echo $language['form_verification_message_name']; ?>");
                        document.modal_requestbrokerpackage.packagename.focus();
                        validated--;
                        return false;
                    }





                    if (validated > 0) {
                        // END VALIDATE BEFORE AJAX

                        packageemail = document.modal_requestbrokerpackage.packageemail.value;
                        packagename = document.modal_requestbrokerpackage.packagename.value;

                        $('#fs_modal-broker-package').modal('toggle');

                        $.ajax({
                            type: "GET",
                            url: "broker_packages.php?process=createPackage&lang=<?php echo $lang_id; ?>&building=<?php echo $building_code; ?>&building_id=<?php echo $building_id; ?>&packagetype=Building",

                            success: function (response) {
                                var filename = response;
                                var fullpath = 'images/broker_packages/' + response;
                                window.location.href = fullpath;

                                $.get("broker_packages.php", {
                                    process: "sendEmail",
                                    fromemail: packageemail,
                                    fromname: packagename,
                                    building: "<?php echo $building_code; ?>",
                                    lang: "<?php echo $lang_id; ?>",
                                    packagetype: "Building"
                                }, function (response) {
                                    // no action in callback
                                });
                            },
                            error: function () {

                            }
                        });

                    }// end if (validated = 0)
                });

// activate tab on click
                $('#fs-tab').on('click', 'a', function (e) {
                    e.preventDefault();

                    var $this = $(this);
console.log("this[0].hash");
console.log($this[0].hash);
                    var $hash = $this[0].hash;
                    // has hasn't been converted yet
                    if ($hash === "#fs-map"){
                        updateMap();
                    }
                    var prefix="tab_";
                    $hash = $hash.replace("#", "#" + prefix);
                    
                    //prevent page jump/scroll on hash change
                    history.pushState(null, null, $hash);
                });

                // will show tab based on hash
                function refreshHash() {
//                    $('#fs-tab').find('a[href="' + window.location.hash + '"]').tab('show');

                    var hash = window.location.hash;
                    var prefix = "tab_";
                    $('#fs-tab').find('a[href="' +hash.replace(prefix,"")+ '"]').tab('show');
                    
//                    $('.nav-tabs a[href="'+hash.replace(prefix,"")+'"]').tab('show');
                    
                    if (window.location.hash === "#tab_fs-map"){
                        updateMap();
                    }
                }

                // show tab if hash changes in address bar
                $(window).bind('hashchange', refreshHash);

                
                // detect hash from address bar at page load
                // run refreshHash()
                if (window.location.hash) {
                    // show tab on load
                    refreshHash();
                }
                
                
            
                $('.btn-modal-print').on('click', function (e) {
                    var config_selection = $('input[name=view]:checked', '#print-pdf-search-form').val()
                    //alert("You selected " + config_selection);
                    var pdf_url = 'printpdfbuildingpackage.php?<?php echo $queries->get_query_str(); ?>';
                    //console.log("config selection is: " + config_selection);
                    if (config_selection == "asbuilt") {
                        window.open(pdf_url + "&twodee=include", "_self");
                    } else {
                        window.open(pdf_url, "_self");
                    }

                    $('#fs_modal-print').modal('hide');
                });
            
            
    // hey look the languages work
    $(".langclick").on('click', function(e) {
        e.preventDefault();
        // get the language clicked from data-language
        var selectedLang = $(this).data('language');
        // take current URL
        var urlNow = window.location.href;
        
        // get any hashes in the url
        var urlHash = window.location.hash;
        console.log("urlHash");
        console.log(urlHash);
        
        // remove any language reference and hashes or stray #'s
        urlNow = urlNow.replace(/&lang=en_CA/g,"").replace(/&lang=fr_CA/g,"").replace(/\?lang=en_CA/g,"").replace(/\?lang=fr_CA/g,"").replace(urlHash, "").replace(/#/g,"");
        // add new language reference
        if (urlNow.indexOf("?") > -1) {
            var langUrl = urlNow+"&lang="+selectedLang+urlHash;
        } else {
            var langUrl = urlNow+"?lang="+selectedLang+urlHash;
        }
        // reload the page with new url
        window.location.href = langUrl;
    });
    
    
//    // our back button takes you right back 
//    // to the last state of the search page    
//    var theUrl = window.location.href;
//    
//    theUrl = theUrl.replace("building.php", "index.php").replace("?building=<?php echo $building_code; ?>", "");
//
//    // count remaining query params with &
//    var ampersands = theUrl.split("&").length -1;
//    // check if only one ampersand left in url
//    if (ampersands === 1) {
//        // it's probably lang but check to be sure
//        if ((theUrl.match(/&lang/g)||[]).length > 0){
//            // change the &lang to ?lang so it works
//            theUrl = theUrl.replace("&lang", "?lang");
//        }
//    }



//    $('body').on('click','.fs_btn-back, .fs_back-link', function(e) {
////        console.log("clicked");
////        console.log(theUrl);
//        window.location.href = theUrl;
//    });
//            
    
    
    function updateMap() {
        setTimeout(function () {
////            theMap.refresh();
//            // nudge the map to fix the markerClusterer bug
//            // https://github.com/googlemaps/v3-utility-library/issues/252
////            theMap.setCenter((mapcenterLa + 0.0000001), mapcenterLg);
//            console.log("resize triggered");
////            google.maps.event.trigger(map,'resize');
////            google.maps.event.trigger(google.maps.map , 'resize')
            var center = map.getCenter();
            console.log("center");
            console.log(center);
            google.maps.event.trigger(map, "resize");
            map.setCenter(center); 
//
        }, 500);

//google.maps.event.trigger(map, 'resize');
    }
    
    
    
    
    var map;
    
//    function displayMap() {
//        document.getElementById('themap').style.display="block";
//        initialize();
//        setTimeout(function () {
////            theMap.setCenter((mapcenterLa + 0.0000001), mapcenterLg);
//            populateMap();
//        }, 250);
//    }
    
    
function initialize() {
  map = new google.maps.Map(document.getElementById("themap"), {
    center: {lat: <?php echo $building_latitude; ?>, lng: <?php echo $building_longitude; ?>},
    zoom: 14
  });
  
  
  
  
  
  
    var bounds = new google.maps.LatLngBounds();
    var buildingName = "<?php echo $building_name; ?>";
    var buildingType = "<?php echo $building_type; ?>";
    var streetAddress = "<?php echo $street_address; ?>";
    var city = "<?php echo $building_city; ?>";
    var province = "<?php echo $building_province; ?>";
    var postal = "<?php echo $building_postal_code; ?>";
    var buildingCode = "<?php echo $building_code; ?>";
    var thumbnail = "<?php echo $building_thumbnail_image_name_list; ?>";
    var latitude = "<?php echo $building_latitude; ?>";
    var longitude = "<?php echo $building_longitude; ?>";
    var numSuites = "<?php echo $suites_count; ?>";
    var suiteTextOutput = '';
    var markers = [];
    var infoWindowContent = [];
    
    var theLanguage = "<?php echo $lang; ?>";
    if (theLanguage === "fr_CA") {
    if (numSuites === 0) {
        if (buildingType === "Retail" || buildingType == "Détail"){
            suiteTextOutput = "Appelez-nous pour connaître les espaces disponible";
        } else {
            suiteTextOutput = "Ce bâtiment est actuellement loué à 100%";
        }
    } else {
        suiteTextOutput = numSuites + " Bureaux disponibles";
    }
} else {
    if (numSuites === 0) {
        if (buildingType === "Retail" || buildingType == "Détail"){
            suiteTextOutput = "Call For Availability";
        } else {
            suiteTextOutput = "This building is currently 100% leased";
        }
    } else {
        suiteTextOutput = numSuites + " Suites Available";
    }
}
    
    

    markers.push([buildingName, latitude, longitude, buildingCode, numSuites]);
            
            if (buildingName === streetAddress) {
                buildingName = "";
            } else {
                buildingName = '<span class="fs_iw-building-name">' + buildingName + '</span><br>';
            }
            infoWindowContent.push(['<div class="fs_infoWindow">' +
                    '<span class="fs_iw-close"><button type="button" class="fs_btn-iw-close"><i class="material-icons fs_icon-iw-close">clear</i></button></span><div class="fs_iw-image"><a class="fs_building-link" href="building.php?building='+buildingCode+'"><img alt="" src="' + thumbnail + '"></a></div>' +
                    '<div class="fs_iw-building">' + buildingName + '<span class="fs_iw-building-address">' + streetAddress + '</span><br><span class="fs_iw-building-city">' + city + '</span>, <span class="fs_iw-building-province">' + province + '</span><br><span class="fs_iw-building-postal-code">' + postal + '</span></div>' +
                    '<div class="fs_iw-available"><a class="fs_map-suites-link" href="building.php?building='+buildingCode+'">'+suiteTextOutput+'</div>' +
                    '</div>']);
    
    
    infoWindow = new google.maps.InfoWindow();
    var marker;
    var i;
    var theMarkerArray = [];
    // Loop through our array of markers & place each one on the map  
    for( i = 0; i < markers.length; i++ ) {
        var markerNumber = markers[i][4].toString();
        if (markerNumber == '0') {
            markerNumber = ' ';
        }
        var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
        bounds.extend(position);
        marker = new google.maps.Marker({
            position: position,
            map: map,
            title: markers[i][0],
            bcode: markers[i][3],
            icon: 'theme/fs_marker-icon.png',
//            label: markers[i][4].toString()
            label: {
                text: markerNumber,
                color: "#fff",
                fontSize: "12px",
                fontWeight: "bold"
            }
        });
    theMarkerArray.push(marker);
        // Allow each marker to have an info window    
        google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
                infoWindow.setContent(infoWindowContent[i][0]);
                infoWindow.open(map, marker);
            };
        })(marker, i));

        
    }    
    
    
//    map.fitBounds(bounds);
    
    // manipulating the map
$('body').on('click', '.fs_btn-iw-close', function(e) {
//    map.hideInfoWindows();
    infoWindow.close();
    
});
           google.maps.event.addListener(infoWindow, 'domready', function() {
              
              // Reference to the DIV which receives the contents of the infowindow using jQuery
              var iwOuter = $('.gm-style-iw');
              
              //reset width and height of parent div to better position assets (eg. close button)
              //iwOuter.parent().css({'width' : '225px', 'height' : '125px'});
              
              //iwOuter.parent().addClass('infowindow-parent');

              /* The DIV we want to change is above the .gm-style-iw DIV.
               * So, we use jQuery and create a iwBackground variable,
               * and took advantage of the existing reference to .gm-style-iw for the previous DIV with .prev().
               */
              var iwBackground = iwOuter.prev();

              // Remove the background shadow DIV
              iwBackground.children(':nth-child(2)').css({'display' : 'none'});

              // Remove the white background DIV
              iwBackground.children(':nth-child(4)').css({'display' : 'none'});
              
              
              
              // Moves the infowindow 115px to the right.
              //iwOuter.parent().parent().css({left: '115px'});
              
              // Moves the shadow of the arrow 76px to the left margin 
                //iwBackground.children(':nth-child(1)').attr('style', function(i,s){ return s + 'left: 76px !important;'});
                
                //hide the shadow of the arrow
                iwBackground.children(':nth-child(1)').css({'display' : 'none'});
                
                //hide the arrow
                iwBackground.children(':nth-child(3)').css({'display' : 'none'});

                // Moves the arrow 76px to the left margin 
                //iwBackground.children(':nth-child(3)').attr('style', function(i,s){ return s + 'left: 76px !important;'});
                
                
                // selector for info window close button
                var iwCloseBtn = iwOuter.next();
                
                //hide the close button since we are adding our own custom close to infowindow
                iwCloseBtn.css('display', 'none');

                // Apply the desired effect to the close button
                //iwCloseBtn.css({
                 // opacity: '1', // by default the close button has an opacity of 0.7
                  //right: '0px', top: '0px' // button repositioning
                  //border: '7px solid #48b5e9', // increasing button border and new color
                  //'border-radius': '13px', // circular effect
                  //'box-shadow': '0 0 5px #3990B9' // 3D effect to highlight the button
                  //});

                // The API automatically applies 0.7 opacity to the button after the mouseout event.
                // This function reverses this event to the desired value.
                //iwCloseBtn.mouseout(function(){
                //  $(this).css({opacity: '1'});
                //});

           });    
           
//  map.fitBounds(bounds);
  
  
}
// engage the map!
initialize();





    $(".suiteLink").on('click', function (e) {
        e.preventDefault();
        var queryString = window.location.search;
        queryString = queryString.replace("?building=<?php echo $building_code; ?>", "");
        var link = this.href;
        var newSuiteLink = link+queryString;        
        window.location.href = newSuiteLink;
    });
    
    



$('#fs_modal-share').on('hidden.bs.modal', function (e) {
    $('#fs_modal-share input').parent().removeClass('alert alert-danger');
});
    
    $('#fs_modal-share .btn-modal-share-submit').on('click', function(e) {
        $('#fs_modal-share input').parent().removeClass('alert alert-danger');
        
        var urlToShare = window.location.href;
//        console.log("urlToShare");
//        console.log(urlToShare);
        
        var tname = $('#fs_modal-share #recipient-name').val();
        var temail = $('#fs_modal-share #recipient-email').val();
        var fname = $('#fs_modal-share #sender-name').val();
        var femail = $('#fs_modal-share #sender-email').val();
        var fcomments = $('#fs_modal-share #bshare-comments').val();


        var googleinfo = grecaptcha.getResponse(bshare_captcha);

        var error = 0;

        if(tname === '') {
            $('#fs_modal-share #recipient-name').parent().addClass('alert alert-danger');
//            $('#fs_modal-share .alert-danger').removeClass('d-none');
            error = error+1;
        }

        if(temail === '') {
            $('#fs_modal-share #recipient-email').parent().addClass('alert alert-danger');
//            $('#fs_modal-share .alert-danger').removeClass('d-none');
            error = error+1;
        }

        if(fname === '') {
            $('#fs_modal-share #sender-name').parent().addClass('alert alert-danger');
//            $('#fs_modal-share .alert-danger').removeClass('d-none');
            error = error+1;
        }

        if(femail === '') {
            $('#fs_modal-share #sender-email').parent().addClass('alert alert-danger');
//            $('#fs_modal-share .alert-danger').removeClass('d-none');
            error = error+1;
        }
        
console.log("error");
console.log(error);
        if (error === 0) {            
            $.ajax({
                type: "GET",
                url: "verifyemail.php",
                data: {fromemail:femail,toemail:temail},
                beforeSend: function() {
                    $('#fs_modal-share .modal-body').prepend('<div class="loader"><img src="images/loader.svg"></div>');
                },
                success: function(data){
                    
                },
                error: function(){
                    
                }
            }).done( function(data) {
                if (data === '66') {
                    $('.loader').remove();
                    $('#fs_modal-share #recipient-email').parent().addClass('alert alert-danger');
                    $('#fs_modal-share .alert-danger').removeClass('hidden');
                    error = error+1;
                } else if (data === '77') {
                    $('.loader').remove();
                    $('#fs_modal-share #sender-email').parent().addClass('alert alert-danger');
                    $('#fs_modal-share .alert-danger').removeClass('hidden');
                    error = error+1;
                } else if (data === '88') {
                    $('.loader').remove();
                    $('#fs_modal-share #recipient-email').parent().addClass('alert alert-danger');
                    $('#fs_modal-share #sender-email').parent().addClass('alert alert-danger');
                    $('#fs_modal-share .alert-danger').removeClass('hidden');
                    error = error+1;
                } else {
                    $('#fs_modal-share input').parent().removeClass('alert alert-danger');
                    // email verified in PHP OK - start recaptcha
                    // console.log("recaptcha started");
                    var googleinfo = grecaptcha.getResponse(bshare_captcha);
                    var ipaddress = $("#jsuseripaddress").val();

                    $.ajax({
                        type: "GET",
                        url: "docaptcha.php",
                        data: {response: googleinfo, remoteip:ipaddress},
                        success: function(data){

                        },
                        error: function(){

                        }
                    }).done(function(data) {

                        var newdata = JSON.parse(data);
                        var reply = newdata[0][0];

                        if (newdata[0].length === 2) {
                            var error = newdata[0][1][0];
                        }

                        if (reply === '66') {
                            $('.loader').remove();
                            alert("Are you sure you're not a robot? Please try again.")
                            error = error+1;
                        } else {
                            var ccme = false;
                            if ($("#cc-me").is(':checked')) {
                                ccme = true;
                            }
                            // captcha succes, send email
                            $.ajax({
                                type: "GET",
                                url: "share_a_link.php",
                                data: {toname:tname,toemail:temail,fromname:fname,fromemail:femail,fromcomments:fcomments,urltoshare:urlToShare,requesttype:"Building",buildingname:"<?php echo $building_name; ?>",buildingstreet:'<?php echo $building_street; ?>', buildingcity:'<?php echo $building_city; ?>',buildingprovince:'<?php echo $building_province;?>',ccme:ccme},
                                    beforeSend: function() {

                                    },
                                    complete: function(){
                                        //hide preloader
                                        $('.loader').remove();
                                    },
                                    success: function(data){

                                    },
                                    error: function(){

                                    }
                            }).done(function(data) {
                                // console.log("data returned from sentctcrequest.php: ");
                                // console.log(data);
                                $('#fs_modal-share #sender-email, #fs_modal-share #sender-name').parent().removeClass('alert alert-danger');
                                if(data == 66) {
                                    //email error
                                    $('#fs_modal-share #sender-email').parent().addClass('alert alert-danger');
                                    $('#fs_modal-share .alert-danger').removeClass('hidden');
                                }
                                else {
                                    $('#fs_modal-share #sender-email, #fs_modal-share #sender-name').parent().removeClass('alert alert-danger');
                                    $('#fs_modal-share .alert-success').removeClass('hidden');
                                    $('#fs_modal-share .alert-danger').addClass('hidden');
                                    $('textarea, :input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
                                    $('#fs_modal-share').modal('toggle');
                                }
                            });
                        } // end else
                    });
                } //end else 
            });
        }
    });


//fix contacts div on scroll
    var targetElement = $('.fs_building-contacts');
    var elementTop = $('.fs_building-contacts').offset().top;
    var elementWidth = $('.fs_building-contacts').outerWidth();
    var headerHeight = $('.fs_site-header').height();
    //console.log(elementTop - ($(window).scrollTop() + headerHeight));

  $(window).scroll(function() {
    //console.log(elementTop - ($(window).scrollTop() + headerHeight));
    
    if( window.innerWidth > 991 ) {
        var relativePos = elementTop - ($(window).scrollTop() + headerHeight+40);

        if ( relativePos <= 0) {
            targetElement.addClass('fixed');
            targetElement.css('top', headerHeight+40);
            //targetElement.css({'position':'fixed', 'top': headerHeight+40, 'width': elementWidth})
        } else {
            targetElement.removeClass('fixed');
            targetElement.css('top', 'auto');
            //targetElement.css({'position':'static', 'top': 'auto', 'width': 'auto'})
        }
    }
    else {
        targetElement.removeClass('fixed');
        targetElement.css('top', 'auto');
    }
  });
  
  
    
    



    });// end document.ready
        </script>
    </body>
</html>

