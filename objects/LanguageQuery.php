<?php

/**
 * @author Rational Root
 * @see ../main_abstract/SearchData_abstract.php
 * @since March 2014
 * @version 1.6
 * */
class LanguageQuery {

    /**
     * This method gets language and its ID.
     * Accesses either the abstract or its overriden method from the classes it extends.
     * <p>
     * @return {Array} langArray
     * @see building.php
     * @see index.php
     * @see suite.php
     * @see region.php
     * @see sub-region.php
     * */
    public function getLanguageAndID() {
        $lang = "en_CA";
        $lang_id = 1;
        if (isset($_REQUEST['lang']) && $_REQUEST['lang'] == "fr_CA") {
            $lang = "fr_CA";
            $lang_id = 0;
        }
        return array(
            $lang,
            $lang_id
        );
    }

    /**
     * This method gets the suite net rentable area.
     * Accesses either the abstract or its overriden method from the classes it extends.
     * <p>
     * @param  {String} $Suites
     * @param  {String} $lang
     * @returns {String} $suite_net_rentable_area
     * @see suite.php
     * */
    public function suite_net_rentable_area($Suites, $lang) {
        $suite_net_rentable_area = $Suites['net_rentable_area'];
        if ($lang == "en_CA") {
            $suite_net_rentable_area = number_format($suite_net_rentable_area);
        } else if ($lang == "fr_CA") {
            $suite_net_rentable_area = number_format($suite_net_rentable_area, 0, ',', ' ');
        }
        return $suite_net_rentable_area;
    }

    /**
     * This method gets the suite contiguous area.
     * Accesses either the abstract or its overriden method from the classes it extends.
     * <p>
     * @param  {String} $Suites
     * @param  {String} $lang
     * @returns {String} $suite_contiguous_area
     * @see /main_abstract/Queries.php
     * @see suite.php
     * */
    public function suite_contiguous_area($Suites, $lang) {
        $suite_contiguous_area = $Suites['contiguous_area'];
        if ($lang == "en_CA") {
            $suite_contiguous_area = number_format($suite_contiguous_area);
        }
        if ($lang == "fr_CA") {
            $suite_contiguous_area = number_format($suite_contiguous_area, 0, ',', ' ');
        }
        return $suite_contiguous_area;
    }

    /**
     * This method gets the suite availability.
     * Accesses either the abstract or its overriden method from the classes it extends.
     * <p>
     * @param  {String} $suite_availability_raw
     * @param  {String} $lang
     * @param  {Array} $language
     * @returns {String}  $language['immediately_text']
     * @see /main_abstract/Queries.php
     * @see suite.php
     * */
    public function suite_availability($suite_availability_raw, $lang, $language) {
        if ($suite_availability_raw != "") {

            if ($suite_availability_raw < time()) {
                return $language['immediately_text'];
            } else {

                if ($lang == "fr_CA") {
                    return strftime('%e %b., %Y', $suite_availability_raw);
                }
                if ($lang == "en_CA") {
                    return strftime('%b. %e, %Y', $suite_availability_raw);
                }
            }
        } else {
            return $language['immediately_text'];
        }
    }

}

?>
