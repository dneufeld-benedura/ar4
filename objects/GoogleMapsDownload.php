<?php



class GoogleMapsDownload{
private   $length = 490; //default length value
private   $height = 190; //default height value
private   $zoom = 13; //default zoom value
private   $format = "png32"; //default image type
private   $markers_color = "blue"; //default markers colour
private   $building_street =""; //MUST BE PROVIDED BY A METHOD CALL!
private   $building_city = ""; //MUST BE PROVIDED BY A METHOD CALL!
private   $building_province = ""; //MUST BE PROVIDED BY A METHOD CALL!
private   $sensor = "false"; //default sensor option

        
   /**
     * This method needs all parameters defined, in order to download the Google Maps image
     * 
     * @param type $length
     * @param type $height
     * @param type $zoom
     * @param type $format
     * @param type $markers_color
     * @param type $building_street
     * @param type $building_city
     * @param type $building_province
     * @param type $sensor
     */

    public function getImage_all($length_, $height_, $zoom_, $format_, $markers_color_, $building_street_, $building_city_, $building_province_, $sensor_, $saveFolder_){
    $this->length = $length_; //sets the global $length value to a provided $length value
    $this->height = $height_; //sets the global $height value to a provided $height value
    $this->zoom = $zoom_; //sets the global $zoom value to a provided $zoom value
    $this->format = $format_; //sets the global $format value to a provided $format value
    $this->markers_color = $markers_color_; //sets the global $markers_color value to a provided $markers_color value
    $this->sensor=$sensor_; //sets the global $sensor value to a provided $sensor value
    
    $this->building_street = $building_street_; //sets the global $building_street value to a provided $building_street value
    $this->building_city = $building_city_; //sets the global $building_city value to a provided $building_city value
    $this->building_province = $building_province_; //sets the global $building_province value to a provided $building_province value
    
    if (!$this->endsWith($saveFolder_,"/")){$saveFolder_.="/";}
    
    $maplink = $this->getLink();
    $this->downloadFile ($maplink, $saveFolder_);
}



/**
 * This method needs only the building street (with a number), city, province, saveFolder to save the Google Maps as a PNG. 
 * It also uses the default values that were pre-defined within this code. If you need to change the default values, please use getImage_all or code a new method.
 * @param type $building_street
 * @param type $building_city
 * @param type $building_province
 * @param type $saveFolder
 */
public function getImage($building_street_, $building_city_, $building_province_, $saveFolder_){   
    $this->building_street = $building_street_; //sets the global $building_street value to a provided $building_street value
    $this->building_city = $building_city_; //sets the global $building_city value to a provided $building_city value
    $this->building_province = $building_province_; //sets the global $building_province value to a provided $building_province value
    
    if (!$this->endsWith($saveFolder_,"/")){$saveFolder_.="/";}
        
    //DBG:1  echo $maplink;
    $maplink = $this->getLink();

    //DBG2: download test!
    $this->downloadFile ($maplink, $saveFolder_);
}



/**
 * This method builds the link based on the inputted (or default) parameters, and returns a string representation of it
 * @return string
 */
private function getLink(){
    //Initialize the static map link
    $maplink = 'http://maps.googleapis.com/maps/api/staticmap?';
 
    //Build the map image link:
    //add length
    $maplink .= 'size='.$this->length;  
    //add height
    $maplink .= 'x'.$this->height; 
    //add zoom
    $maplink .= '&zoom='.$this->zoom; 
    //add format
    $maplink .='&format='.$this->format;
    //add markers colour
    $maplink .= '&markers=color:'.$this->markers_color.'%7C';
    //add street
    $maplink .= urlencode($this->building_street) .'%2C';
    //add city
    $maplink .= $this->building_city .'%2C';
    //add province
    $maplink .= $this->building_province;

    //Finish the maplink code
    $maplink .= '&sensor='.$this->sensor;
    
    //Return the maplink
    return $maplink;
    
}






/**
 * This method simply downloads the file based on a generated link, and saves as street_city_province.PNG within a desired directory.
 * @param type $maplink
 * @param type $saveFolder
 */
private function downloadFile ($maplink, $saveFolder) {
$path = $saveFolder."/".urlencode($this->building_street)."_".$this->building_city."_".$this->building_province.".png";
    
  $newfname = $path;
  $file = fopen ($maplink, "rb");
  if ($file) {
    $newf = fopen ($newfname, "wb");

    if ($newf)
    while(!feof($file)) {
      fwrite($newf, fread($file, 1024 * 8 ), 1024 * 8 );
    }
  }

  if ($file) {
    fclose($file);
  }

  if ($newf) {
    fclose($newf);
  }
 }



/**
 * This method simply tells us whether the string (haystack) ends with a certain string (needle)
 * @param type $haystack
 * @param type $needle
 * @return boolean
 */
private function endsWith($haystack, $needle)
{
    $length = strlen($needle);
    if ($length == 0) {
        return true;
    }

    return (substr($haystack, -$length) === $needle);
}


}
?>
