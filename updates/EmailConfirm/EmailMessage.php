<?php
/*
 * This class is used to generate the message that is to be sent to a subscriber.
 * @author RationalRoot
 * @version AR2.0
 * @since December 2014
 */

include('CreateHTML.php');

class EmailMessage {
 
    
    /**
     * Sends an e-mail for confirmation.
     * @param type $email
     * @param type $subject
     * @param type $message
     * @param type $clientname
     * @param type $emaildomain
     */
    public function sendEmail($email, $subject, $message, $clientname, $emaildomain){
    mail($email, $subject, $message, self::getHeaders($clientname, $emaildomain), '-f updates@' . $emaildomain);
    }
    
    
    /**
     * Gets HTML header
     * @return string
     */
    public function getHTMLHeader(){       
        return "<!DOCTYPE html>
                <html>
                <body>";
    }
    
    
    /**
     * Gets HTML footer
     * @return string
     */
    public function getHTMLFooter(){       
        return "</body>
                </html>";
    }    
    
    /**
     * Gets the e-mail message header
     * @param type $clientname
     * @param type $emaildomain
     * @return string
     */
    public static function getHeaders($clientname, $emaildomain) {
        $eol = "\n";
        $headers = 'MIME-Version: 1.0' . $eol;
        $headers .= 'Content-type: text/html; charset=utf-8' . $eol;
        $headers .= 'From: ' . ucwords($clientname) . ' Broker Updates <updates@' . $emaildomain . '>' . $eol;
        $headers .= 'X-Mailer: PHP/' . phpversion() . $eol;
        return $headers;
    }
    
    /**
     * Gets the confirmation message that displays just after the user registers.
     * @param type $name
     * @param type $email
     * @return type
     */
    public function getSentConfirmationMessage($uniqueid,$data,$language){
        $createHTML = new CreateHTML();
        $message = $createHTML->getRegistrationConfirmation($uniqueid,$data,$language);

        return $message;    
        
    }
    
 
}
