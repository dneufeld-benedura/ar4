<?php
//setting locale for time, date, money, etc
setlocale(LC_ALL, "en_US");
date_default_timezone_set('America/New_York');
/* 
 * This file is part of the Findspace system
 * 
 *  @copyright  Copyright (c) 2017 Rational Root (http://www.rationalroot.com)
 *  @package    Findspace
 *  @license    http://www.rationalroot.com/ Commercial
 *  @version    4.0 || 2017-11-17
 */


$toemail = isset($_REQUEST['toemail']) ? $_REQUEST['toemail'] : '';
$fromemail = isset($_REQUEST['fromemail']) ? $_REQUEST['fromemail'] : '';


$error = "00";
$errorcount = 0;

if (!filter_var($toemail, FILTER_VALIDATE_EMAIL)) {
    $errorcount = $errorcount + 1;
    $error = "66"; 
}
if (!filter_var($fromemail, FILTER_VALIDATE_EMAIL)) {
    $errorcount = $errorcount + 1;
    $error = "77"; 
}
if ($errorcount == 2) { // to_email and from_email are bad
    $error = "88";
}


echo $error;
    




