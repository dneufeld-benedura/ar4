<?php
$spec_qy = mysql_query("select * from building_specifications where building_code='" . $building_code . "' and lang = " . $lang_id . "");
$row2 = mysql_fetch_array($spec_qy);

$spec_qy_string2 = "select * from suites_specifications where suite_id='" . $suite_id . "' and lang = ".$lang_id."";
$spec_qy2 = mysql_query($spec_qy_string2);
$row2 = mysql_fetch_array($spec_qy2);


$lang = "en_CA";
$lang_id = 1;
if (isset($_REQUEST['lang'])) {
    if ($_REQUEST['lang'] == "fr_CA") {

        $lang = "fr_CA";
        $lang_id = 0;
    } else {
        
    }
} else {
    
}

setlocale(LC_ALL, $lang . '.utf8');



$suitecount_query_string = 'SELECT COUNT(*) AS realsuitecount FROM suites alls WHERE building_code = ? AND lang=1 AND leased="false"';
// Preparing the statement
$stmt = $db->prepare($suitecount_query_string);
// binding the parameters
$stmt->bind_param('s', $building_code);
// Executing the query
if (!$stmt->execute()) {
    trigger_error('The query execution failed; MySQL said (' . $stmt->errno . ') ' . $stmt->error, E_USER_ERROR);
}
$col1 = null;
$stmt->bind_result($suiteCount);
while ($stmt->fetch()) {
    // having to use this while makes no sense at all but it doesn't work w/o it.
}
$stmt->close();


if ($suite_net_rent ==! "Negotiable") {
    ($suite_net_rent !== '' && $suite_net_rent !== '0.00' && $suite_net_rent !== 0 ? money_format('%.2n', doubleval($suite_net_rent)) : $language['na_text']);
}

if ($suite_add_rent_total !== '' && $suite_add_rent_total !== '0.00' && $suite_add_rent_total !== 0) {
      $suite_add_rent_total =  money_format('%.2n', doubleval($suite_add_rent_total));
} else {
    $suite_add_rent_total = $language['na_text'];
}

?>



<section class="fs_section fs_suite-features">
    <div class="row">
        <div class="col">
            <div class="card fs_card-features">
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <div class="row">
                                <div class="col fs_col-feature-icon">
                                    <i class="material-icons fs_icon-feature">settings_overscan</i>
                                </div>
                                <div class="col fs_col-feature-value">
                                    <h3 class="card-title fs_card-feature-title"><?php echo $language['size_text']; ?> / <?php echo $language['building_information_sq_ft_trans']; ?></h3>
                                    <p class="fs_feature-value"><?php echo $suite_net_rentable_area; ?></p>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="row">
                                <div class="col fs_col-feature-icon">
                                    <i class="material-icons fs_icon-feature">dashboard</i>
                                </div>
                                <div class="col fs_col-feature-value">
                                    <h3 class="card-title fs_card-feature-title"><?php echo $language['availability_text']; ?></h3>
                                    <p class="fs_feature-value"><?php echo $suite_availability; ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="row">
                                <div class="col fs_col-feature-icon">
                                    <i class="material-icons fs_icon-feature">attach_money</i>
                                </div>
                                <div class="col fs_col-feature-value">
                                    <h3 class="card-title fs_card-feature-title"><?php echo $language['price_text']; ?> / <?php echo $language['month_abbr']; ?></h3>
                                    <p class="fs_feature-value"><?php echo $suite_net_rent; ?></p>                                                                
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="row">
                                <div class="col fs_col-feature-icon">
                                    <i class="material-icons fs_icon-feature">monetization_on</i>
                                </div>
                                <div class="col fs_col-feature-value">
                                    <h3 class="card-title fs_card-feature-title"><?php echo $language['additional_rent_text']; ?> / <?php echo $language['month_abbr']; ?></h3>
                                    <p class="fs_feature-value"><?php echo $suite_add_rent_total; ?></p>                                                                
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="row">
                                <div class="col fs_col-feature-icon">
                                    <i class="material-icons fs_icon-feature">vertical_align_top</i>
                                </div>
                                <div class="col fs_col-feature-value">
                                    <h3 class="card-title fs_card-feature-title"><?php echo $language['ceiling_text']; ?></h3>
                                    <p class="fs_feature-value"><?php echo ($row2['clear_height'] !== '' ? $row2['clear_height'] : $language['na_text']); ?></p>                                                                
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="row">
                                <div class="col fs_col-feature-icon">
                                    <i class="material-icons fs_icon-feature">label</i>
                                </div>
                                <div class="col fs_col-feature-value">
                                    <h3 class="card-title fs_card-feature-title"><?php echo $language['space_type_text']; ?></h3>
                                    <p class="fs_feature-value"><?php echo ($row2['suite_type'] !== '' ? ucwords(mb_strtolower($row2['suite_type'], 'UTF-8')) : $language['na_text']); ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>




<section class="fs_section fs_suite-specifications">
<!--    <div class="card fs_card-specifications">
        <div class="card-body">
            <div class="card-title"><h3><?php // echo $language['tenant_costs_title_text']; ?></h3></div>
            <div class="row">
                <div class="col fs_specification-label"><?php // echo $language['realty_tax_text']; ?></div>
                <div class="col fs_specification-value"><?php // echo ($row2['costs_realty'] !== '' ? money_format('%.2n', (double) $row2['costs_realty']) : $language['na_text']); ?></div>
            </div>
            <div class="row">
                <div class="col fs_specification-label"><?php // echo $language['operating_costs_text']; ?></div>
                <div class="col fs_specification-value"><?php // echo ($row2['costs_operating'] !== '' ? money_format('%.2n', (double) $row2['costs_operating']) : $language['na_text']); ?></div>
            </div>
            <div class="row">
                <div class="col fs_specification-label"><?php // echo $language['additional_rent_total_text']; ?></div>
                <div class="col fs_specification-value"><?php // echo ($row2['costs_add_rent_total'] !== '' ? money_format('%.2n', (double) $row2['costs_add_rent_total']) : $language['na_text']); ?></div>
            </div>
        </div>
    </div>-->

<!--    <div class="card fs_card-specifications">
        <div class="card-body">
            <div class="card-title"><h3><?php // echo $language['building_size_title_text']; ?> (<?php // echo $language['building_information_sq_ft_trans']; ?>)</h3></div>
            <div class="row">
                <div class="col fs_specification-label"><?php // echo $language['total_space_text_nocolon']; ?></div>
                <?php // $total_space = format_numbers($total_space, $language); ?>
                <div class="col fs_specification-value"><?php // echo ($total_space !== '0' ? $total_space : $language['na_text']); ?></div>
            </div>
            <div class="row">
                <div class="col fs_specification-label"><?php // echo $language['number_of_floors_text']; ?></div>
                <div class="col fs_specification-value"><?php // echo ($row2['floors'] !== '' ? $row2['floors'] : $language['na_text']); ?></div>
            </div>
            <div class="row">
                <div class="col fs_specification-label"><?php // echo $language['total_office_space_text']; ?></div>
                <?php // $total_office_space = format_numbers($row2['total_office_space'], $language); ?>
                <div class="col fs_specification-value"><?php // echo ($total_office_space !== '0' ? $total_office_space : $language['na_text']); ?></div>
            </div>
        </div>
    </div>-->
    
<?php if (!checkForEmpty($row2['office_space']) && !checkForEmpty($row2['warehouse_space']) && !checkForEmpty($row2['clear_height']) && !checkForEmpty($row2['available_electrical_notes']) && !checkForEmpty($row2['heating_hvac_notes']) && !checkForEmpty($row2['lighting_notes'])) {
    // this section is emtpy so skip it
} else { ?>
    <div class="card fs_card-specifications">
        <div class="card-body">
            <div class="card-title"><h3><?php echo $language['construction_title_text']; ?></h3></div>
        <?php if (checkForEmpty($row2['office_space'])) { ?>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['office_space_text']; ?></div>
                <?php $office_space = format_numbers($row2['office_space'], $language); ?>
                <div class="col fs_specification-value"><?php echo ($office_space !== '0' ? $office_space . ' ' . $language['building_information_sq_ft_trans'] : $language['na_text']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row2['warehouse_space'])) {
        ?>            
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['warehouse_space_text']; ?></div>
                <?php $warehouse_space = format_numbers($row2['warehouse_space'], $language); ?>
                <div class="col fs_specification-value"><?php echo ($warehouse_space !== '0' ? $warehouse_space . ' ' . $language['building_information_sq_ft_trans'] : $language['na_text']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row2['clear_height'])) {
        ?>            
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['clear_height_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row2['clear_height'] !== '' ? $row2['clear_height'] : $language['na_text']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row2['available_electrical_notes'])) {
        ?>            
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['electrical_notes_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row2['available_electrical_notes'] !== '' ? $row2['available_electrical_notes'] : $language['na_text']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row2['heating_hvac_notes'])) {
        ?>            
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['heating_hvac_notes_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row2['heating_hvac_notes'] !== '' ? $row2['heating_hvac_notes'] : $language['na_text']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row2['lighting_notes'])) {
        ?>            
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['lighting_notes_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row2['lighting_notes'] !== '' ? $row2['lighting_notes'] : $language['na_text']); ?></div>
            </div>
        <?php } ?>
        </div>
    </div>
<?php } ?>
<?php if (!checkForEmpty($row2['shipping_doors_drive_in']) && !checkForEmpty($row2['shipping_doors_truck']) && !checkForEmpty($row2['bay_size_y']) && !checkForEmpty($row2['bay_size_x']) && !checkForEmpty($row2['bay_size_notes']) && !checkForEmpty($row2['slab_notes']) && !checkForEmpty($row2['sprinkler_notes'])) {
    // this section is emtpy so skip it
} else { ?>
    <div class="card fs_card-specifications">
        <div class="card-body">
            <div class="card-title"><h3><?php echo $language['technical_title_text']; ?></h3></div>
        <?php if (checkForEmpty($row2['shipping_doors_drive_in'])) { ?>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['shipping_doors_drive_in_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row2['shipping_doors_drive_in'] !== '' ? $row2['shipping_doors_drive_in'] : $language['na_text']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row2['shipping_doors_truck'])) {
        ?>            
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['shipping_doors_truck_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row2['shipping_doors_truck'] !== '' ? $row2['shipping_doors_truck'] : $language['na_text']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row2['shipping_doors_notes'])) {
        ?>            
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['shipping_door_notes_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row2['shipping_doors_notes'] !== '' ? $row2['shipping_doors_notes'] : $language['na_text']); ?></div>
            </div>
        <?php }
        
        if (!checkForEmpty($row2['bay_size_x']) && !checkForEmpty($row2['bay_size_y'])) {
            // skip this one
        } else {
        ?>            
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['bay_size_text']; ?></div>
                <?php
                    $baySizeOut = ($row2['bay_size_x'] !== '0' ? $row2['bay_size_x'] : $language['na_text']); ?> x <?php echo ($row2['bay_size_y'] !== '0' ? $row2['bay_size_y'] . ' ' . $language['feet_text']: $language['na_text']);
                ?>
                <div class="col fs_specification-value"><?php echo $baySizeOut; ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row2['bay_size_notes'])) {
        ?>            
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['bay_notes_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row2['bay_size_notes'] !== '' ? $row2['bay_size_notes'] : $language['na_text']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row2['slab_notes'])) {
        ?>            
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['slab_notes_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row2['slab_notes'] !== '' ? $row2['slab_notes'] : $language['na_text']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row2['sprinkler_notes'])) {
        ?>            
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['sprinkler_system_notes_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row2['sprinkler_notes'] !== '' ? $row2['sprinkler_notes'] : $language['na_text']); ?></div>
            </div>
        <?php } ?>
        </div>
    </div>
<?php } ?>
<?php if (!checkForEmpty($row2['surface_stalls']) && !checkForEmpty($row2['surface_stalls_per_unit']) && !checkForEmpty($row2['truck_trailer_parking']) && !checkForEmpty($row2['parking_notes'])) {
    // this section is emtpy so skip it
} else { ?>    
        <div class="card fs_card-specifications">
        <div class="card-body">
            <div class="card-title"><h3><?php echo $language['parking_text']; ?></h3></div>
        <?php if (checkForEmpty($row2['surface_stalls'])) { ?>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['surface_stalls_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row2['surface_stalls'] !== '' ? $row2['surface_stalls'] : $language['na_text']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row2['surface_stalls_per_unit'])) {
        ?>            
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['surface_ratio_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row2['surface_stalls_per_unit'] !== '/1000' ? $row2['surface_stalls_per_unit'] : $language['na_text']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row2['truck_trailer_parking'])) {
        ?>            
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['truck_trailer_parking_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row2['truck_trailer_parking'] !== '' ? $row2['truck_trailer_parking'] : $language['na_text']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row2['parking_notes'])) {
        ?>            
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['parking_notes_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row2['parking_notes'] !== '' ? $row2['parking_notes'] : $language['na_text']); ?></div>
            </div>
        <?php } ?>
        </div>
    </div>
<?php } ?>

<!--    <div class="card fs_card-specifications">
        <div class="card-body">
            <div class="card-title"><h3><?php // echo $language['safety_and_access_text']; ?></h3></div>
            <div class="row">
                <div class="col fs_specification-label"><?php // echo $language['fire_detection_system_text']; ?></div>
                <div class="col fs_specification-value"><?php // checkOrX($row2['safety_fire_detection']); ?></div>
            </div>
            <div class="row">
                <div class="col fs_specification-label"><?php // echo $language['sprinkler_system_text']; ?></div>
                <div class="col fs_specification-value"><?php // checkOrX($row2['safety_sprinkler_system']); ?></div>
            </div>
            <div class="row">
                <div class="col fs_specification-label"><?php // echo $language['manned_security_text']; ?></div>
                <div class="col fs_specification-value"><?php // checkOrX($row2['safety_manned_security']); ?></div>
            </div>
            <div class="row">
                <div class="col fs_specification-label"><?php // echo $language['security_system_text']; ?></div>
                <div class="col fs_specification-value"><?php // checkOrX($row2['safety_security_system']); ?></div>
            </div>
            <div class="row">
                <div class="col fs_specification-label"><?php // echo $language['barrier_free_acccess_text']; ?></div>
                <div class="col fs_specification-value"><?php // checkOrX($row2['safety_barrier_free']); ?></div>
            </div>
        </div>
    </div>-->






</section>



<!--<section class="fs_section fs_suite-notes">
    <div class="card fs_card-notes">
        <div class="card-body">
            <div class="row">
                <div class="col fs_col-card-notes-icon">
                    <i class="material-icons fs_notes-icon">insert_drive_file</i>
                    <h2 class="fs_notes-title">Notes</h2>
                </div>
                <div class="col fs_col-card-notes-text">
                    <h3 class="fs_notes-title"><?php // echo $language['building_description_text']; ?></h3>
                    <p class="fs_notes-text"><?php // echo $building_desc; ?></p>
                    <h3 class="fs_notes-title">Sublease</h3>
                    <p class="fs_notes-text">Sed tempus sed fermentum hendrerit. Suspendisse ullamcorper euismod dignissim. etiam commodo varius lacus, et laoreet neque tristique. Phasellus eleifend urna sed accumsan scelerisque. Nulla quis semper lacus, eget congue eros.</p>
                </div>
            </div>
        </div>
    </div>
</section>-->
