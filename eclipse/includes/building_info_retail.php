<?php
$spec_qy = mysql_query("select * from building_specifications where building_code='" . $building_code . "' and lang = " . $lang_id . "");
$row = mysql_fetch_array($spec_qy);


$lang = "en_CA";
$lang_id = 1;
if (isset($_REQUEST['lang'])) {
    if ($_REQUEST['lang'] == "fr_CA") {

        $lang = "fr_CA";
        $lang_id = 0;
    } else {
        
    }
} else {
    
}

setlocale(LC_ALL, $lang . '.utf8');



$suitecount_query_string = 'SELECT COUNT(*) AS realsuitecount FROM suites alls WHERE building_code = ? AND lang=1 AND leased="false"';
// Preparing the statement
$stmt = $db->prepare($suitecount_query_string);
// binding the parameters
$stmt->bind_param('s', $building_code);
// Executing the query
if (!$stmt->execute()) {
    trigger_error('The query execution failed; MySQL said (' . $stmt->errno . ') ' . $stmt->error, E_USER_ERROR);
}
$col1 = null;
$stmt->bind_result($suiteCount);
while ($stmt->fetch()) {
    // having to use this while makes no sense at all but it doesn't work w/o it.
}
$stmt->close();


?>



<?php if ($theme['featured_specs_visible'] == 1) { ?>
<section class="fs_section fs_building-features">
    <div class="row">
        <div class="col">
            <div class="card fs_card-features">
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <div class="row">
                                <div class="col fs_col-feature-icon">
                                    <i class="material-icons fs_icon-feature">settings_overscan</i>
                                </div>
                                <div class="col fs_col-feature-value">
                                    <h3 class="card-title fs_card-feature-title"><?php echo $language['size_text']; ?> / <?php echo $language['building_information_sq_ft_trans']; ?></h3>
                                    <?php $total_space = format_numbers($row['total_space'], $language); ?>
                                    <p class="fs_feature-value"><?php echo ($total_space !== '0' ? $total_space : $language['na_text']); ?></p>                                                                
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="row">
                                <div class="col fs_col-feature-icon">
                                    <i class="material-icons fs_icon-feature">dashboard</i>
                                </div>
                                <div class="col fs_col-feature-value">
                                    <h3 class="card-title fs_card-feature-title"><?php echo $language['units_available_text']; ?></h3>
                                    <p class="fs_feature-value"><?php echo $suiteCount; ?></p>                                                                
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="row">
                                <div class="col fs_col-feature-icon">
                                    <i class="material-icons fs_icon-feature">attach_money</i>
                                </div>
                                <div class="col fs_col-feature-value">
                                    <h3 class="card-title fs_card-feature-title"><?php echo $language['price_text']; ?> / <?php echo $language['month_abbr']; ?></h3>
                                    <p class="fs_feature-value"><?php echo $language['negotiable_text']; ?></p>                                                                
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="row">
                                <div class="col fs_col-feature-icon">
                                    <i class="material-icons fs_icon-feature">layers</i>
                                </div>
                                <div class="col fs_col-feature-value">
                                    <h3 class="card-title fs_card-feature-title"><?php echo $language['number_of_floors_text']; ?></h3>
                                    <p class="fs_feature-value"><?php echo ($row['floors'] !== '' ? $row['floors'] : $language['na_text']); ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="row">
                                <div class="col fs_col-feature-icon">
                                    <i class="material-icons fs_icon-feature">local_parking</i>
                                </div>
                                <div class="col fs_col-feature-value">
                                    <h3 class="card-title fs_card-feature-title"><?php echo $language['parking_title_text']; ?></h3>
                                    <p class="fs_feature-value"><?php echo ($row['parking_total_stalls'] !== '0' ? $row['parking_total_stalls'] : $language['na_text']); ?></p>                                                                
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="row">
                                <div class="col fs_col-feature-icon">
                                    <i class="material-icons fs_icon-feature">store</i>
                                </div>
                                <div class="col fs_col-feature-value">
                                    <h3 class="card-title fs_card-feature-title"><?php echo $language['mall_type_text']; ?></h3>
                                    <p class="fs_feature-value"><?php echo ($row['mall_type'] !== '' ? $row['mall_type'] : $language['na_text']); ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php if ($row['boma'] !== '' || $row['leed'] !== '') { ?>
                    <div class="row">
                        <div class="col">
                            <div class="row">
                                <div class="col fs_col-feature-icon">
                                    <i class="material-icons fs_icon-feature">new_releases</i>
                                </div>
                                <div class="col fs_col-feature-value">
                                    <h3 class="card-title fs_card-feature-title"><?php echo $language['boma_title_text']; ?></h3>
                                    <p class="fs_feature-value"><?php echo ($row['boma'] !== '' ? $row['boma'] : $language['na_text']); ?></p>
                                    </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="row">
                                <div class="col fs_col-feature-icon">
                                    <i class="material-icons fs_icon-feature">spa</i>
                                </div>
                                <div class="col fs_col-feature-value">
                                    <h3 class="card-title fs_card-feature-title"><?php echo $language['leed_title_text']; ?></h3>
                                    <p class="fs_feature-value"><?php echo ($row['leed'] !== '' ? $row['leed'] : $language['na_text']); ?></p>                                                                
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php } ?>

<?php if ($secCount > 0) { ?>
<section class="fs_section building-sections">
    <div class="card fs_card-sections">
        <div class="card-body">
            <div class="card-title"><h4><?php echo $language['features_text']; ?></h4></div>

                <?php echo $features_tab_content; ?>

        </div>
    </div>
</section>
<?php } ?>

<section class="fs_section building-specifications">
<?php if (!checkForEmpty($row['costs_posted_net_rate']) && !checkForEmpty($row['costs_realty']) && !checkForEmpty($row['costs_power']) && !checkForEmpty($row['costs_operating']) && !checkForEmpty($row['costs_other']) && !checkForEmpty($row['costs_add_rent_total'])) {
    // this section is emtpy so skip it
} else { ?>
    <div class="card fs_card-specifications">
        <div class="card-body">
            <div class="card-title"><h3><?php echo $language['tenant_costs_title_text']; ?></h3></div>
        <?php if (checkForEmpty($row['costs_posted_net_rate'])) { ?>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['posted_net_rate_text']; ?></div>
                <div class="col fs_specification-value"><?php echo money_format('%.2n', (double) $row['costs_posted_net_rate']). ' ' . $language['building_information_sq_ft_trans']; ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['costs_realty'])) {
        ?>            
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['realty_tax_text']; ?></div>
                <div class="col fs_specification-value"><?php echo money_format('%.2n', (double) $row['costs_realty']). ' ' . $language['building_information_sq_ft_trans']; ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['costs_power'])) {
        ?>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['utilities_text']; ?></div>
                <div class="col fs_specification-value"><?php echo money_format('%.2n', (double) $row['costs_power']). ' ' . $language['building_information_sq_ft_trans']; ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['costs_operating'])) {
        ?>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['operating_costs_text']; ?></div>
                <div class="col fs_specification-value"><?php echo money_format('%.2n', (double) $row['costs_operating']). ' ' . $language['building_information_sq_ft_trans']; ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['costs_other'])) {
        ?>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['other_text']; ?></div>
                <div class="col fs_specification-value"><?php echo money_format('%.2n', (double) $row['costs_other']). ' ' . $language['building_information_sq_ft_trans']; ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['costs_add_rent_total'])) {
        ?>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['additional_rent_total_text']; ?></div>
                <div class="col fs_specification-value"><?php echo money_format('%.2n', (double) $row['costs_add_rent_total']). ' ' . $language['building_information_sq_ft_trans']; ?></div>
            </div>
        <?php } ?>
        </div>
    </div>
<?php } ?>
<?php if (!checkForEmpty($row['year_built']) && !checkForEmpty($row['renovated']) && !checkForEmpty($row['website'])) {
    // this section is emtpy so skip it
} else { ?>
    <div class="card fs_card-specifications">
        <div class="card-body">
            <div class="card-title"><h3><?php echo $language['general_title_text']; ?></h3></div>
        <?php if (checkForEmpty($row['year_built'])) { ?>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['year_built_text_nocolon']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row['year_built'] !== '' ? $row['year_built'] : $language['na_text']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['renovated'])) {
        ?>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['last_renovated_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row['renovated'] !== '' ? $row['renovated'] : $language['na_text']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['website'])) {
        ?>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['website_text']; ?></div>
                <?php if (strpos($row['website'], "http://") > -1 || strpos($row['website'], "https://") > -1) {
                    $theWebsite = '<a href="' . $row['website'] . '" target="_blank">'.$row['website'].'</a>';
                } else {
                    $theWebsite = '<a href="http://' . $row['website'] . '" target="_blank">'.$row['website'].'</a>';
                } ?>
                <div class="col fs_specification-value"><?php echo ($row['website'] !== '' ? $theWebsite : $language['na_text']); ?></div>
            </div>
        <?php } ?>
        </div>
    </div>
    
<?php } ?>
<?php if (!checkForEmpty($row['floors']) && !checkForEmpty($row['number_of_stores']) && !checkForEmpty($row['total_retail_space']) && !checkForEmpty($row['total_office_space']) && !checkForEmpty($row['total_space'])) {
    // this section is emtpy so skip it
} else { ?>
    
    <div class="card fs_card-specifications">
        <div class="card-body">
            <div class="card-title"><h3><?php echo $language['retail_profile_text']; ?></h3></div>
        <?php if (checkForEmpty($row['floors'])) { ?>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['number_of_floors_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row['floors'] !== '' ? $row['floors'] : $language['na_text']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['number_of_stores'])) {
        ?>            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['number_of_stores_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row['number_of_stores'] !== '' ? $row['number_of_stores'] : $language['na_text']); ?></div>
            </div>
        <?php }
        
//        if (checkForEmpty($row['website'])) {
        ?>            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['retail_food_court_text']; ?></div>
                <div class="col fs_specification-value"><?php echo checkOrX($row['retail_food_court']); ?></div>
            </div>
        <?php // }
        
        if (checkForEmpty($row['total_retail_space'])) {
        ?>            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['total_retail_space_text']; ?></div>
                <?php $total_retail_space = format_numbers($row['total_retail_space'], $language);; ?>
                <div class="col fs_specification-value"><?php echo ($total_retail_space !== '0' ? $total_retail_space . ' ' . $language['building_information_sq_ft_trans'] : $language['na_text']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['total_office_space'])) {
        ?>            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['total_office_space_text']; ?></div>
                <?php $total_office_space = format_numbers($row['total_office_space'], $language); ?>
                <div class="col fs_specification-value"><?php echo ($total_office_space !== '0' ? $total_office_space . ' ' . $language['building_information_sq_ft_trans'] : $language['na_text']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['total_space'])) {
        ?>            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['total_space_text_nocolon']; ?></div>
                <?php $total_space = format_numbers($row['total_space'], $language); ?>
                <div class="col fs_specification-value"><?php echo ($total_space !== '0' ? $total_space . ' ' . $language['building_information_sq_ft_trans'] : $language['na_text']); ?></div>
            </div>
        <?php } ?>
        </div>
    </div>
    
<?php } ?>
<?php if (!checkForEmpty($row['parking_total_stalls']) && !checkForEmpty($row['parking_total_stalls'])) {
    // this section is emtpy so skip it
} else { ?>
    
    <div class="card fs_card-specifications">
        <div class="card-body">
            <div class="card-title"><h3><?php echo $language['parking_title_text']; ?></h3></div>
        <?php if (checkForEmpty($row['parking_total_stalls'])) { ?>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['total_stalls_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row['parking_total_stalls'] !== '0' ? $row['parking_total_stalls'] : $language['na_text']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['parking_description'])) {
        ?>            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['parking_desc_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row['parking_description'] !== '' ? $row['parking_description'] : $language['na_text']); ?></div>
            </div>
        <?php } ?>
        </div>
    </div>
    
<?php } ?>
<?php if (!checkForEmpty($row['operating_hours_M']) && !checkForEmpty($row['operating_hours_T']) && !checkForEmpty($row['operating_hours_W']) && !checkForEmpty($row['operating_hours_R']) && !checkForEmpty($row['operating_hours_F']) && !checkForEmpty($row['operating_hours_S']) && !checkForEmpty($row['operating_hours_D'])) {
    // this section is emtpy so skip it
} else { ?>

    <div class="card fs_card-specifications">
        <div class="card-body">
            <div class="card-title"><h3><?php echo $language['operating_hours_text']; ?></h3></div>
        <?php if (checkForEmpty($row['operating_hours_M'])) { ?>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['monday_operating_hours_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row['operating_hours_M'] !== '' ? $row['operating_hours_M'] : $language['na_text']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['operating_hours_T'])) {
        ?>            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['tuesday_operating_hours_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row['operating_hours_T'] !== '' ? $row['operating_hours_T'] : $language['na_text']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['operating_hours_W'])) {
        ?>            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['wednesday_operating_hours_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row['operating_hours_W'] !== '' ? $row['operating_hours_W'] : $language['na_text']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['operating_hours_R'])) {
        ?>            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['thursday_operating_hours_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row['operating_hours_R'] !== '' ? $row['operating_hours_R'] : $language['na_text']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['operating_hours_F'])) {
        ?>            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['friday_operating_hours_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row['operating_hours_F'] !== '' ? $row['operating_hours_F'] : $language['na_text']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['operating_hours_S'])) {
        ?>            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['saturday_operating_hours_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row['operating_hours_S'] !== '' ? $row['operating_hours_S'] : $language['na_text']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['operating_hours_D'])) {
        ?>            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['sunday_operating_hours_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row['operating_hours_D'] !== '' ? $row['operating_hours_D'] : $language['na_text']); ?></div>
            </div>
        <?php } ?>
        </div>
    </div>
    
<?php } ?>
<?php if (!checkForEmpty($row['primary_population']) && !checkForEmpty($row['primary_number_of_households']) && !checkForEmpty($row['primary_household_income']) && !checkForEmpty($row['secondary_population']) && !checkForEmpty($row['secondary_number_of_households']) && !checkForEmpty($row['secondary_household_income']) && !checkForEmpty($row['customer_median_age']) && !checkForEmpty($row['customer_persons_per_household']) && !checkForEmpty($row['customer_household_income']) && !checkForEmpty($row['traffic_pedestrian_traffic']) && !checkForEmpty($row['traffic_demographic_source'])) {
    // this section is emtpy so skip it
} else { ?>

    <div class="card fs_card-specifications">
        <div class="card-body">
            <div class="card-title"><h3><?php echo $language['demographics_text']; ?></h3></div>
            <div class="card-title-type"><h4><?php echo $language['primary_trade_area_text']; ?></h4></div>
        <?php if (checkForEmpty($row['primary_population'])) { ?>
            <div class="row">
                <div class="col fs_specification-label">Population</div>
                <?php $primary_population = format_numbers($row['primary_population'], $language); ?>
                <div class="col fs_specification-value"><?php echo ($primary_population !== '0' ? $primary_population : $language['na_text']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['primary_number_of_households'])) {
        ?>            
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['number_of_households_text']; ?></div>
                <?php $primary_number_of_households = format_numbers($row['primary_number_of_households'], $language);?>
                <div class="col fs_specification-value"><?php echo ($primary_number_of_households !== '0' ? $primary_number_of_households : $language['na_text']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['primary_household_income'])) {
        ?>            
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['avg_household_income_text']; ?></div>
                <?php $primary_household_income = money_format('%.2n', (double) $row['primary_household_income']);?>
                <div class="col fs_specification-value"><?php echo ($primary_household_income !== '0' ? $primary_household_income : $language['na_text']); ?></div>
            </div>
            
            <div class="card-title-type"><h4><?php echo $language['secondary_trade_area_text']; ?></h4></div>
        <?php }
        
        if (checkForEmpty($row['secondary_population'])) {
        ?>            
            <div class="row">
                <div class="col fs_specification-label">Population</div>
                <?php $secondary_population = format_numbers($row['secondary_population'], $language); ?>
                <div class="col fs_specification-value"><?php echo ($secondary_population !== '0' ? $secondary_population : $language['na_text']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['secondary_number_of_households'])) {
        ?>            
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['number_of_households_text']; ?></div>
                <?php $secondary_number_of_households = format_numbers($row['secondary_number_of_households'], $language);?>
                <div class="col fs_specification-value"><?php echo ($secondary_number_of_households !== '0' ? $secondary_number_of_households : $language['na_text']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['secondary_household_income'])) {
        ?>            
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['avg_household_income_text']; ?></div>
                <?php $secondary_household_income = money_format('%.2n', (double) $row['secondary_household_income']);?>
                <div class="col fs_specification-value"><?php echo ($secondary_household_income !== '0' ? $secondary_household_income : $language['na_text']); ?></div>
            </div>
        
            <div class="card-title-type"><h4><?php echo $language['customer_profile_text']; ?></h4></div>
        <?php }
        
        if (checkForEmpty($row['customer_median_age'])) {
        ?>            
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['median_age_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row['customer_median_age']!== '' ? $row['customer_median_age'] : $language['na_text']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['customer_persons_per_household'])) {
        ?>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['persons_per_household_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row['customer_persons_per_household'] !== '' ? $row['customer_persons_per_household'] : $language['na_text']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['customer_household_income'])) {
        ?>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['avg_household_income_text']; ?></div>
                <?php $customer_household_income = money_format('%.2n', (double) $row['customer_household_income']); ?>
                <div class="col fs_specification-value"><?php echo ($customer_household_income !== '' ? $customer_household_income : $language['na_text']); ?></div>
            </div>
        
            <div class="card-title-type"><h4><?php echo $language['traffic_profile_text']; ?></h4></div>
        <?php }
        
        if (checkForEmpty($row['traffic_pedestrian_traffic'])) {
        ?>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['pedestrian_traffic_text']; ?></div>
                <?php $traffic_pedestrian_traffic = format_numbers($row['traffic_pedestrian_traffic'], $language); ?>
                <div class="col fs_specification-value"><?php echo ($traffic_pedestrian_traffic !== '' ? $traffic_pedestrian_traffic : $language['na_text']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['traffic_demographic_source'])) {
        ?>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['demographic_source_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row['traffic_demographic_source'] !== '' ? $row['traffic_demographic_source'] : $language['na_text']); ?></div>
            </div>
        <?php } ?>
        </div>
    </div>
    
<?php } ?>
    
<?php if (!checkForEmpty($row['anchor_tenant1']) && !checkForEmpty($row['anchor_tenant2']) && !checkForEmpty($row['anchor_tenant3']) && !checkForEmpty($row['anchor_tenant4']) && !checkForEmpty($row['anchor_tenant5']) && !checkForEmpty($row['anchor_tenant6'])) {
    // this section is emtpy so skip it
} else { ?>

    <div class="card fs_card-specifications">
        <div class="card-body">
            <div class="card-title"><h3><?php echo $language['anchor_tenants_text']; ?></h3></div>
        <?php if (checkForEmpty($row['anchor_tenant1'])) { ?>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['tenant_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row['anchor_tenant1'] !== '' ? $row['anchor_tenant1'] : $language['na_text']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['anchor_tenant2'])) {
        ?>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['tenant_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row['anchor_tenant2'] !== '' ? $row['anchor_tenant2'] : $language['na_text']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['anchor_tenant3'])) {
        ?>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['tenant_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row['anchor_tenant3'] !== '' ? $row['anchor_tenant3'] : $language['na_text']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['anchor_tenant4'])) {
        ?>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['tenant_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row['anchor_tenant4'] !== '' ? $row['anchor_tenant4'] : $language['na_text']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['anchor_tenant5'])) {
        ?>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['tenant_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row['anchor_tenant5'] !== '' ? $row['anchor_tenant5'] : $language['na_text']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['anchor_tenant6'])) {
        ?>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['tenant_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row['anchor_tenant6'] !== '' ? $row['anchor_tenant6'] : $language['na_text']); ?></div>
            </div>
        <?php } ?>
        </div>
    </div>
    
<?php } ?>
</section>
    
    
    
    
    

<!--<section class="fs_section building-notes">
    <div class="card fs_card-notes">
        <div class="card-body">
            <div class="row">
                <div class="col fs_col-card-notes-icon">
                    <i class="material-icons fs_notes-icon">insert_drive_file</i>
                    <h2 class="fs_notes-title">Notes</h2>
                </div>
                <div class="col fs_col-card-notes-text">
                    <h3 class="fs_notes-title"><?php // echo $language['building_description_text']; ?></h3>
                    <p class="fs_notes-text"><?php // echo $building_desc; ?></p>
                    <h3 class="fs_notes-title">Sublease</h3>
                    <p class="fs_notes-text">Sed tempus sed fermentum hendrerit. Suspendisse ullamcorper euismod dignissim. etiam commodo varius lacus, et laoreet neque tristique. Phasellus eleifend urna sed accumsan scelerisque. Nulla quis semper lacus, eget congue eros.</p>
                </div>
            </div>
        </div>
    </div>
</section>-->
