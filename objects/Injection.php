<?php

/**
 * @author Rational Root
 * @see ../main_abstract/SearchData_abstract.php
 * @since March 2014
 * @version 1.6
 * */
class Injection {
    
    public function injPreventString($request) {
        $cleanVal = filter_input(INPUT_GET, $request, FILTER_SANITIZE_STRING);
        return $cleanVal;
    }
    
    public function injPreventInt($request) {
        $cleanVal = filter_input(INPUT_GET, $request, FILTER_SANITIZE_NUMBER_INT);
        return $cleanVal;
    }

}