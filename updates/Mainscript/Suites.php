<?php

/**
 * This class does all the logic at the suite level.
 * @author RationalRoot
 * @version AR2.0
 * @since December 2014
 */
class Suites {
    private static $queries;
    private static $language;
    private static $lang;
    private static $createHTML;
    private static $newsuite;
    private static $leasedsuite;
    private static $totalSuitesAdded; 

    /**
     * This is a constructor function that sets all the required parameters for the class to work.
     * @param type $language
     * @param type $lang
     * @param type $queries
     * @param type $createHTML
     */
    public final function __construct($language, $lang, $queries, $createHTML) {
        self::$queries = $queries;
        self::$language = $language;
        self::$lang = $lang;
        self::$createHTML = $createHTML;
        self::$newsuite = 0;
        self::$leasedsuite = 0;
    }

    
    /**
     * Function used to get the count of a newsuite, to see whether to display the building information or not.
     * @return type
     */
    public function getNewSuite(){return self::$newsuite;}
    
    public function getLeasedSuite(){return self::$leasedsuite;}
    
    /**
     * A function used to reset the suite counter back to 0, while moving onto a next building.
     */
    public function resetNewSuite(){self::$newsuite=0;}
    
    public function resetLeasedSuite(){self::$leasedsuite=0;}
    
    /**
     * A function which taverses through each suote driver and gets the relevant data for the message output, as well as the count of the suites.
     * @param type $Building
     * @param type $suitedriver
     * @param type $suitediffbottom
     * @param type $nobuilding
     * @return type
     */
    public static function traverseSuitedriver($Building, $suitedriver, $suitediffbottom, $nobuilding) { 
        $changedsuite=0; 
        $traverseSuites='';
        $new = '';
        $updated = '';
        $leased = '';
        
        foreach ($suitedriver as $code_suite) {
            $changecount = 0;
           
            $suites_query_string = self::$queries->getSuitesQueryString($Building, $code_suite); //echo $suites_query_string;
            $suites_query = mysql_query($suites_query_string) or die("Suites query error: " . mysql_error());

            $traverseSuites = self::traverseSuites($suites_query, $Building, $code_suite, $suitediffbottom, $nobuilding, $changedsuite);
            $changedsuite = $traverseSuites['changedsuite'];
            $changecount = $traverseSuites['changecount'];
            $arrangeTypes = $traverseSuites['arrangeTypes'];
            $new .= $arrangeTypes['new_suite'];
            $updated .= $arrangeTypes['updated_suite'];
            $leased .= $arrangeTypes['leased_suite'];       
        } // end foreach suitedriver
        $data = array();
        $data['changecount'] = $changecount;
        $data['changedsuite'] = $changedsuite;
        
        $suitediffbottom =  $new.$updated.$leased;
        $data['suitediffbottom'] = $suitediffbottom;
        return $data;
    }

    /**
     * A function which traverses through the suites and gets the relevant data for the message output, as well as the count of the changes.
     * @param type $suites_query
     * @param type $Building
     * @param type $code_suite
     * @param type $suitediffbottom
     * @param type $nobuilding
     * @param type $changedsuite
     * @return type
     */
    private function traverseSuites($suites_query, $Building, $code_suite, $suitediffbottom, $nobuilding, $changedsuite) { 
        $changecount = 0;
        $arrangeTypes = array(); //this array is used to sort the types of changes into a new, update, lease order.
        $arrangeTypes['new_suite'] = '';
        $arrangeTypes['updated_suite'] = '';
        $arrangeTypes['leased_suite'] = '';
        
        while ($Suites = mysql_fetch_array($suites_query)) {  
            $suiteData = self::getSuiteData($Building, $Suites, $code_suite);
            $changecount = $suiteData['changecount'];
            $changeCountArray = self::getSuiteRow($Building, $nobuilding, $Suites, $suiteData, $changedsuite);
            
            //suites are classified as new, update, lease
            $arrangeTypes['new_suite'] .=$changeCountArray['new_suite'];
            $arrangeTypes['updated_suite'] .=$changeCountArray['updated_suite'];
            $arrangeTypes['leased_suite'] .=$changeCountArray['leased_suite'];
             
            $changedsuite = $changeCountArray['changedsuite'];

            $nobuilding = $changeCountArray['nobuilding'];
        } // end while $suites    

        //join all $arrangeTypes data to obtain the $suitediffbottom
     
        $data = array();
        $data['changedsuite'] = $changedsuite;
        $data['changecount'] = $changecount;
        $data['suitediffbottom'] = $suitediffbottom;
        $data['arrangeTypes']=$arrangeTypes; //the array of classified suite types
        return $data;
    }

 
    
    
    /**
     * This function returns the yesterday_suites, netRentData, contiguousData, suiteAvailabilityData, suite_availability, nochange_suite_availability, addrentTotalData, changecount
     * It is called by the loop that goes through each suite and calulates/generates the output.
     * @param type $Building
     * @param type $Suites
     * @param type $code_suite
     * @return type
     */
    private static function getSuiteData($Building, $Suites, $code_suite) { 

        
        $yesterday_suites = self::$queries->getYesterdaySuitesArray($Suites);
        $netRentData = self::$queries->getSuiteNetRentableAreaData($Building, $code_suite);

        $compare_with_contig = $netRentData['net_rentable_area'];
        if ($compare_with_contig == '') {
            $compare_with_contig = $yesterday_suites['net_rentable_area'];
        }

        $contiguousData = self::$queries->getSuiteContiguousAreaData($compare_with_contig, $Building, $code_suite);
        $suiteAvailabilityData = self::$queries->getSuiteAvailabilityData($Building, $code_suite);

        $suite_availability_raw = $suiteAvailabilityData['availability'];
        $suite_availability = self::getSuiteAvailability($suite_availability_raw);
        $nochange_suite_availability = self::getNochangeSuiteAvalabilityRaw($Suites);
        $justLeasedData = self::$queries->getJustLeasedData($Building, $code_suite);
        $removedSuitesData = self::$queries->getRemovedSuitesData($Building, $code_suite);
        $addrentTotalData = self::$queries->getSuiteAddRentTotalData($Building, $code_suite);

 
        $changecount = self::getChangeCount($netRentData['suite_net_rentable_count'], $contiguousData['suite_contiguous_area_count'], 
                                            $suiteAvailabilityData['suite_availability_count'], $addrentTotalData['suite_add_rent_total_count'],
                                            $justLeasedData['suite_just_leased_total_count'], $justLeasedData['suite_just_unleased_total_count'], $removedSuitesData['count'] );


        
        $suiteData = array();
        $suiteData['yesterday_suites'] = $yesterday_suites;
        $suiteData['netRentData'] = $netRentData;
        $suiteData['contiguousData'] = $contiguousData;
        $suiteData['suiteAvailabilityData'] = $suiteAvailabilityData;
        $suiteData['suite_availability'] = $suite_availability;
        $suiteData['nochange_suite_availability'] = $nochange_suite_availability;
        $suiteData['justLeasedData'] = $justLeasedData;
        $suiteData['removedSuitesData'] = $removedSuitesData;
        $suiteData['addrentTotalData'] = $addrentTotalData;
        $suiteData['changecount'] = $changecount;
        return $suiteData;
    }

    /**
     * This is where the logic for diplaying the NEW, UPDATE, JUST-LEASED suites, per row, occurs.
     * Main idea is to store the suites into an array that has ['new_suite'], ['updated_suite'], ['leased_suite']
     * these are just the additional elements in the $ret array
     * Later, these elements are joined into a String, per suite update type, in a traverseSuitedriver method.
     * After joining, the regular string is returned, and that way everything is sorted-out.
     * @param type $Building
     * @param type $nobuilding
     * @param type $Suites
     * @param type $suiteData
     * @param type $changedsuite
     * @return type
     */
    private function getSuiteRow($Building, $nobuilding, $Suites, $suiteData, $changedsuite) { 
        $ret = array();
        $ret['changedsuite'] = '';
        $ret['suitediffbottom'] = '';
        $ret['nobuilding'] = '';
        $ret['new_suite'] = '';
        $ret['updated_suite'] = '';
        $ret['leased_suite'] = '';
 

        $nochange_suite_availability = $suiteData['nochange_suite_availability'];
        $changecount = $suiteData['changecount'];
        $unleasedcount = $suiteData['justLeasedData']['suite_just_unleased_total_count'];
 
        if ($changecount > 0 ) {
            $changedsuite++;
            //gets the UPDATE suite OR NEW if the suite is back on the market from "just leased".
            
            if ($unleasedcount >0){
                $typeOf = self::$createHTML->getNewSuite($Building, $Suites, $nochange_suite_availability);
                $numberOfSuitesAdded = self::$createHTML->getNumberOfSuitesAdded();
                self::$totalSuitesAdded = self::$totalSuitesAdded+$numberOfSuitesAdded;
                if ($numberOfSuitesAdded > 0){
                $ret['new_suite'] .= $typeOf['html_code'];
                }
            }
            else {
                $typeOf = self::$createHTML->getUpdatedSuite($suiteData, $Suites, $Building);
                $numberOfSuitesAdded = self::$createHTML->getNumberOfSuitesAdded();
                self::$totalSuitesAdded = self::$totalSuitesAdded+$numberOfSuitesAdded;
                if ($typeOf['change_type'] == 'update' && $numberOfSuitesAdded > 0){
                $ret['updated_suite'] .= $typeOf['html_code'];                
                }
                else if ($typeOf['change_type'] == 'lease' && $numberOfSuitesAdded > 0){
                self::$leasedsuite = self::$leasedsuite+1;    
                $ret['leased_suite'] .= $typeOf['html_code'];    
                }   
                }
        } else {  // (changecount = 0)

            $yesterdaybuilding_query_string = self::$queries->getYesterdayBuildingQueryString($Suites);
            $yesterdaybuilding_query = mysql_query($yesterdaybuilding_query_string) or die("yesterday query failed: " . mysql_error());
            $yesterdaybuilding_count = mysql_num_rows($yesterdaybuilding_query);
            



            if ($yesterdaybuilding_count > 0) 
             {
                // this is not a new suite.
                // Need to prevent the building header from displaying
                $nobuilding++;

            } else {
                self::$newsuite = self::$newsuite+1;
            //gets the NEW suite     
                $typeOf = self::$createHTML->getNewSuite($Building, $Suites, $nochange_suite_availability);
                $numberOfSuitesAdded = self::$createHTML->getNumberOfSuitesAdded();
                self::$totalSuitesAdded = self::$totalSuitesAdded+$numberOfSuitesAdded;
                if ($numberOfSuitesAdded > 0){
                $ret['new_suite'] .= $typeOf['html_code'];  
                }
            } // end if $yesterdaybuilding_count > 0 
        }  // end if ($changecount


//        echo $ret['leased_suite'];
        $ret['changedsuite'] = $changedsuite;
        $ret['nobuilding'] = $nobuilding;

        return $ret;
    }

    /**
     * Returns the availability of a suite
     * @param type $suite_availability_raw
     * @return type
     */
    private function getSuiteAvailability($suite_availability_raw) {
        $suite_availability = "";
        if ($suite_availability_raw != "") {

            if ($suite_availability_raw < time()) {
                $suite_availability = self::$language['immediately_text'];
            } else {
                $suite_availability = strftime('%b %Y', $suite_availability_raw);
            }
        } else {
            $suite_availability = self::$language['immediately_text'];
        }
		
        return $suite_availability;
    }

    /**
     * This function represents the logic to incrementing a counter used for generating the building output.
     * @param type $netRentData_count
     * @param type $contiguousData_count
     * @param type $suiteAvailabilityData_count
     * @param type $addrentTotalData_count
     * @return type
     */
    private static function getChangeCount($netRentData_count, $contiguousData_count, $suiteAvailabilityData_count, $addrentTotalData_count, $justLeasedData, $justUnleasedData, $removedSuitesData) {
        $changecount = 0;
        if ($netRentData_count > 0 ) {
            $changecount = $changecount + 1;
        }
        if ($contiguousData_count > 0) {
            $changecount = $changecount + 1;
        }
        if ($suiteAvailabilityData_count > 0) {
            $changecount = $changecount + 1;
        }
        if ($addrentTotalData_count > 0) {
            $changecount = $changecount + 1;
        }
         
        if ($justLeasedData > 0 || $justUnleasedData > 0) {
            $changecount = $changecount + 1;
        }
        
        if ($removedSuitesData>0){
        $changecount = $changecount + 1;    
        }
        
        return $changecount;
    }

    /**
     * Returns the information regarding the suite availability.
     * @param type $Suites
     * @return type
     */
    private function getNochangeSuiteAvalabilityRaw($Suites) {
        $nochange_suite_availability_raw = $Suites['availability'];
        $nochange_suite_availability = "";
        if ($nochange_suite_availability_raw != "") {
            if ($nochange_suite_availability_raw < time()) {
                //$suite_availability = "Immediate";
                $nochange_suite_availability = self::$language['immediately_text'];
            } else {
                $nochange_suite_availability = strftime('%b %Y', $nochange_suite_availability_raw);
            }
        } else {
            $nochange_suite_availability = self::$language['immediately_text'];
        }
        return $nochange_suite_availability;
    }
 
    
    public function getTotalSuitesAdded(){return self::$totalSuitesAdded;}

}
