<?php
$spec_qy_string = "select * from suites_specifications where suite_id='" . $suite_id . "' and lang = ".$lang_id."";
$spec_qy = mysql_query($spec_qy_string);
$row2 = mysql_fetch_array($spec_qy);

//        krumo($row2);

$lang = "en_CA";
$lang_id = 1;
if (isset($_REQUEST['lang'])) {
    if ($_REQUEST['lang'] == "fr_CA") {

        $lang = "fr_CA";
        $lang_id = 0;
    } else {
        
    }
} else {
    
}

setlocale(LC_ALL, $lang . '.utf8');



?>





<section class="fs_section fs_suite-features">
    <div class="row">
        <div class="col">
            <div class="card fs_card-features">
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <div class="row">
                                <div class="col fs_col-feature-icon">
                                    <i class="material-icons fs_icon-feature">photo_size_select_small</i>
                                </div>
                                <div class="col fs_col-feature-value">
                                    <h3 class="card-title fs_card-feature-title"><?php echo $language['size_text']; ?> / <?php echo $language['building_information_sq_ft_trans']; ?></h3>
                                    <p class="fs_feature-value"><?php echo $suite_net_rentable_area; ?></p>                                                                
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="row">
                                <div class="col fs_col-feature-icon">
                                    <i class="material-icons fs_icon-feature">local_shipping</i>
                                </div>
                                <div class="col fs_col-feature-value">
                                    <h3 class="card-title fs_card-feature-title"><?php echo $language['loading_text']; ?></h3>
                                    <p class="fs_feature-value"><?php echo ($row2['shipping_doors_drive_in'] !== '' ? $row2['shipping_doors_drive_in'] : $language['na_text']); ?> / <?php echo ($row2['shipping_doors_truck'] !== '' ? $row2['shipping_doors_truck'] : $language['na_text']); ?></p>                                                                
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="row">
                                <div class="col fs_col-feature-icon">
                                    <i class="material-icons fs_icon-feature">store</i>
                                </div>
                                <div class="col fs_col-feature-value">
                                    <h3 class="card-title fs_card-feature-title"><?php echo $language['availability_text']; ?></h3>
                                    <p class="fs_feature-value"><?php echo $suite_availability; ?></p>                                                                
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="row">
                                <div class="col fs_col-feature-icon">
                                    <i class="material-icons fs_icon-feature">flash_on</i>
                                </div>
                                <div class="col fs_col-feature-value">
                                    <h3 class="card-title fs_card-feature-title"><?php echo $language['power_text']; ?></h3>
                                    <p class="fs_feature-value"><?php echo ($row2['available_electrical_volts'] !== '' ? $row2['available_electrical_volts'] : $language['na_text']); ?>v / <?php echo ($row2['available_electrical_amps'] !== '' ? $row2['available_electrical_amps'] : $language['na_text']); ?>a</p>                                                                
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="row">
                                <div class="col fs_col-feature-icon">
                                    <i class="material-icons fs_icon-feature">attach_money</i>
                                </div>
                                <div class="col fs_col-feature-value">
                                    <h3 class="card-title fs_card-feature-title"><?php echo $language['price_text']; ?> / <?php echo $language['month_abbr']; ?></h3>
                                    <p class="fs_feature-value"><?php echo $suite_net_rent; ?></p>                                                                
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="row">
                                <div class="col fs_col-feature-icon">
                                    <i class="material-icons fs_icon-feature">vertical_align_top</i>
                                </div>
                                <div class="col fs_col-feature-value">
                                    <h3 class="card-title fs_card-feature-title"><?php echo $language['ceiling_text']; ?></h3>
                                    <p class="fs_feature-value"><?php echo ($row2['clear_height'] !== '' ? $row2['clear_height'] : $language['na_text']); ?></p>                                                                
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>




<section class="fs_section fs_suite-specifications">
    <div class="card fs_card-specifications">
        <div class="card-body">
            <div class="card-title"><h3><?php echo $language['tenant_costs_title_text']; ?></h3></div>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['realty_tax_text']; ?></div>
                <div class="col fs_specification-value"><?php echo money_format('%.2n', (double) $row['costs_realty']); ?></div>
            </div>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['operating_costs_text']; ?></div>
                <div class="col fs_specification-value"><?php echo money_format('%.2n', (double) $row['costs_operating']); ?></div>
            </div>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['additional_rent_total_text']; ?></div>
                <div class="col fs_specification-value"><?php echo money_format('%.2n', (double) $row['costs_add_rent_total']); ?></div>
            </div>
        </div>
    </div>


    <div class="card fs_card-specifications">
        <div class="card-body">
            <div class="card-title"><h3><?php echo $language['construction_title_text']; ?></h3></div>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['office_space_text']; ?></div>
                <?php $office_space = format_numbers($row2['office_space'], $language); ?>
                <div class="col fs_specification-value"><?php echo $office_space; ?></div>
            </div>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['warehouse_space_text']; ?></div>
                <?php $warehouse_space = format_numbers($row2['warehouse_space'], $language); ?>
                <div class="col fs_specification-value"><?php echo $warehouse_space; ?></div>
            </div>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['clear_height_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row2['clear_height'] !== '' ? $row2['clear_height'] : $language['na_text']); ?></div>
            </div>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['electrical_notes_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row2['available_electrical_notes'] !== '' ? $row2['available_electrical_notes'] : $language['na_text']); ?></div>
            </div>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['heating_hvac_notes_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row2['heating_hvac_notes'] !== '' ? $row2['heating_hvac_notes'] : $language['na_text']); ?></div>
            </div>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['lighting_notes_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row2['lighting_notes'] !== '' ? $row2['lighting_notes'] : $language['na_text']); ?></div>
            </div>
        </div>
    </div>

    
    
    
    
    
    
    
    
    <div class="card fs_card-specifications">
        <div class="card-body">
            <div class="card-title"><h3><?php echo $language['technical_title_text']; ?></h3></div>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['shipping_doors_drive_in_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row2['shipping_doors_drive_in'] !== '' ? $row2['shipping_doors_drive_in'] : $language['na_text']); ?></div>
            </div>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['shipping_doors_truck_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row2['shipping_doors_truck'] !== '' ? $row2['shipping_doors_truck'] : $language['na_text']); ?></div>
            </div>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['bay_size_text']; ?></div>
                <div class="col fs_specification-value"><?php echo $row2['bay_size_x'] . ' x ' .  $row2['bay_size_y'] . ' ' . $language['feet_text']; ?></div>
            </div>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['bay_notes_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row2['bay_size_notes'] !== '' ? $row2['bay_size_notes'] : $language['na_text']); ?></div>
            </div>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['slab_notes_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row2['slab_notes'] !== '' ? $row2['slab_notes'] : $language['na_text']); ?></div>
            </div>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['sprinkler_system_notes_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row2['sprinkler_notes'] !== '' ? $row2['sprinkler_notes'] : $language['na_text']); ?></div>
            </div>
        </div>
    </div>    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   


    <div class="card fs_card-specifications">
        <div class="card-body">
            <div class="card-title"><h3><?php echo $language['parking_text']; ?></h3></div>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['surface_stalls_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row2['surface_stalls'] !== '' ? $row2['surface_stalls'] : $language['na_text']); ?></div>
            </div>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['surface_ratio_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row2['surface_stalls_per_unit'] !== '/1000' ? $row2['surface_stalls_per_unit'] : $language['na_text']); ?></div>
            </div>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['truck_trailer_parking_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row2['truck_trailer_parking'] !== '' ? $row2['truck_trailer_parking'] : $language['na_text']); ?></div>
            </div>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['parking_notes_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row2['parking_notes'] !== '' ? $row2['parking_notes'] : $language['na_text']); ?></div>
            </div>
        </div>
    </div>




    <div class="card fs_card-specifications">
        <div class="card-body">
            <div class="card-title"><h3><?php echo $language['public_transport_title_text']; ?></h3></div>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['transit_surface_route_text']; ?></div>
                <div class="col fs_specification-value"><?php checkOrX($row['public_surface_route']); ?></div>
            </div>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['direct_subway_access_text']; ?></div>
                <div class="col fs_specification-value"><?php checkOrX($row['public_subway_access']); ?></div>
            </div>
        </div>
    </div>

</section>