<?php

/**
 * @author Rational Root
 * @see ../main_abstract/SearchData_abstract.php
 * @since September 2014
 * @version 1.7
 * */
class DisplayHTML{


    public function provinceQueryHTML($language, $queries, $lang_id, $province_selected) {
        $HTML = "";
        $building_province_query = mysql_query("select distinct province from buildings where buildings.lang=" . $lang_id . " order by province asc");
        $building_province_option_query = mysql_query("select distinct province from buildings where buildings.lang= 1 order by province asc");
        $HTML .= '<option value="All">' . $language['province_text'] . '</option>';
        while ($provinces = mysql_fetch_array($building_province_query)) {

            $provinces_options = mysql_fetch_array($building_province_option_query);
            if ($provinces_options['province'] != "") {

                if ($provinces_options['province'] == $province_selected) {
                    $HTML .= '<option selected="yes" value="' . $provinces_options['province'] . '">' . $provinces['province'] . '</option>' . "\r\n";
                } else {
                    $HTML .= '<option value="' . $provinces_options['province'] . '">' . $provinces['province'] . '</option>' . "\r\n";
                }
            }
        }


        return $HTML;
    }


    public function buildingRegionHTML($language, $queries, $province_selected, $region_selected, $lang_id) {
        $HTML = "";

        $building_region_query = $queries->buildingRegionQuery($province_selected, '1'); // this should always be English


        $HTML .= '<option value="All">' . $language['region_text'] . '</option>\r\n';

        while ($regions = mysql_fetch_array($building_region_query)) {

            $current_lang_options_array = mysql_query("select region from buildings where building_code ='" . $regions['building_code'] . "' and lang=" . $lang_id);
            $current_lang_options_result = mysql_fetch_array($current_lang_options_array);

            if ($regions['region'] != "") {

                if ($regions['region'] == $region_selected) {
                    $HTML .= '<option selected="yes" value="' . $regions['region'] . '">' . $current_lang_options_result['region'] . '</option>' . "\r\n";
                } else {
                    $HTML .= '<option value="' . $regions['region'] . '">' . $current_lang_options_result['region'] . '</option>' . "\r\n";
                }
            }
        }


        return $HTML;
    }


    public function buildingSubRegionHTML($language, $queries, $region_selected, $sub_region_selected, $lang_id) {
        $HTML = "";

        $building_sub_region_query = $queries->buildingsubRegionQuery($region_selected, '1'); // this should always be English

        $HTML .= '<option value="All">' . $language['sub_region_text'] . '</option>\r\n';

        while ($sub_regions = mysql_fetch_array($building_sub_region_query)) {

            $current_lang_options_array = mysql_query("select sub_region from buildings where building_code ='" . $sub_regions['building_code'] . "' and lang=" . $lang_id);
            $current_lang_options_result = mysql_fetch_array($current_lang_options_array);

            if ($sub_regions['sub_region'] != "") {

                if ($sub_regions['sub_region'] == $sub_region_selected) {
                    $HTML .= '<option selected="yes" value="' . $sub_regions['sub_region'] . '">' . $current_lang_options_result['sub_region'] . '</option>' . "\r\n";
                } else {
                    $HTML .= '<option value="' . $sub_regions['sub_region'] . '">' . $current_lang_options_result['sub_region'] . '</option>' . "\r\n";
                }
            }
        }

        return $HTML;
    }


    public function suitesQueryHTML($language, $queries, $lang_id, $suitetype_selected) {
        $HTML = "";


        $suite_types_query = mysql_query("select distinct s.suite_type from suites s INNER JOIN building_types bt ON (s.suite_type=bt.building_type) where s.lang=" . $lang_id . " order by sort_order asc");
//echo "typequery: " . $suite_types_query . "<br>";
        $suite_types_option_query = mysql_query("select distinct s.suite_type from suites s INNER JOIN building_types bt ON (s.suite_type=bt.building_type) where s.lang= 1 order by sort_order asc");
        $HTML .= '<option value="All">' . $language['space_type_text'] . '</option>\r\n';
        while ($suite_types = mysql_fetch_array($suite_types_query)) {
            $suite_types_options = mysql_fetch_array($suite_types_option_query);

            $suite_types['suite_type'] = strtoupper($suite_types['suite_type']);

            if ($suite_types['suite_type'] == 'OFFICE' || $suite_types['suite_type'] == 'BUREAU') {
                $suite_types['suite_type'] = $language['building_type_office'];
            }
            if ($suite_types['suite_type'] == 'INDUSTRIAL' || $suite_types['suite_type'] == 'INDUSTRIEL') {
                $suite_types['suite_type'] = $language['building_type_industrial'];
            }
            if ($suite_types['suite_type'] == 'RETAIL' || $suite_types['suite_type'] == 'DÉTAIL') {
                $suite_types['suite_type'] = $language['building_type_retail'];
            }

            if ($suite_types_options['suite_type'] == $suitetype_selected) {

                if (strpos($HTML, $suite_types['suite_type']) == false) {
                    $HTML .= '<option selected="yes" value="' . $suite_types_options['suite_type'] . '">' . ucwords(mb_strtolower($suite_types['suite_type'], 'UTF-8')) . '</option>' . "\r\n";
                }
            } else {

                if (strpos($HTML, $suite_types['suite_type']) == false) {
                    $HTML .= '<option value="' . $suite_types_options['suite_type'] . '">' . ucwords(mb_strtolower($suite_types['suite_type'], 'UTF-8')) . '</option>' . "\r\n";
                }
            }
        }
        return $HTML;
    }


    public function sizesOptionHTML($language, $queries, $lang_id, $size_selected) {
        $HTML = "";
        $sizes_query = mysql_query("select distinct size, size_display from sizes where lang=" . $lang_id . " order by size_index asc");
        $sizes_option_query = mysql_query("select distinct size from sizes where lang= 1 order by size_index asc");
        while ($size_row = mysql_fetch_array($sizes_query)) {
            $size_row_options = mysql_fetch_array($sizes_option_query);
            if ($size_row_options['size'] == $size_selected) {
                $HTML .= '<option selected="yes" value="' . $size_row_options['size'] . '">' . $size_row['size_display'] . '</option>' . "\r\n";
            } else {
                if ($size_row['size_display'] !== "CUSTOM"){
                    $HTML .= '<option value="' . $size_row_options['size'] . '">' . $size_row['size_display'] . '</option>' . "\r\n";
                }
            }
        }
        return $HTML;
    }


    public function availOptionHTML($language, $queries, $lang_id) {
        $HTML = "";
        $avail_query = mysql_query("select distinct availability_period, availabilities_display from availabilities where lang=" . $lang_id . " order by availability_index asc");
        $avail_option_query = mysql_query("select distinct availability_period, availabilities_display from availabilities where lang= 1  order by availability_index asc");
        while ($avail_row = mysql_fetch_array($avail_query)) {
            $avail_row_options = mysql_fetch_array($avail_option_query);
            if (array_key_exists('Availability', $_REQUEST)) {
                if ($avail_row_options['availability_period'] == $_REQUEST['Availability']) {
                    $HTML .= '<option selected="yes" value="' . $avail_row_options['availability_period'] . '">' . $avail_row['availabilities_display'] . '</option>' . "\r\n";
                } else {

                    $HTML .= '<option value="' . $avail_row_options['availability_period'] . '">' . $avail_row['availabilities_display'] . '</option>' . "\r\n";
                }
            } else {

                $HTML .= '<option value="' . $avail_row_options['availability_period'] . '">' . $avail_row['availabilities_display'] . '</option>' . "\r\n";
            }
        }

        return $HTML;
    }


    public function divNarrowResults($language) {
        
        if ($language['lang_id'] == 1) {
        $results_text = str_replace(' Results', '', $language['narrow_results_text']);
        } else {
        $results_text = str_replace(' votre recherche', '', $language['narrow_results_text']);
        }
        $HTML = "";
		$HTML .= "<div class=\"row visible-xs\">";
		$HTML .= "<div class=\"col-xs-4 col-sm-3 col-md-2\"><a href=\"#\" class=\"btn btn-primary btn-sm btn-narrow-results visible-xs\">" . $results_text . "</a><a href=\"#\" class=\"btn btn-primary btn-narrow-results hidden-xs\">" . $language['narrow_results_text']. "</a>";
        $HTML .= "</div><!-- /.col -->";
        $HTML .= "<div class=\"col-xs-8 col-sm-9 col-md-10\"><span class=\"total-selected\">";
        $HTML .= "</span>";
        $HTML .= "<span class=\"total-results\">";
        $HTML .= "</span></div><!-- /.col -->";
		$HTML .= "</div><!-- /.row -->";
		$HTML .= "<div class=\"row hidden-xs\">";
		$HTML .= "<div class=\"col-xs-12\">";
        $HTML .= "<a href=\"#\" class=\"btn btn-primary btn-narrow-results\">" . $language['narrow_results_text'];
        $HTML .= "</a>";
        $HTML .= "<span class=\"total-selected\">";
        $HTML .= "</span>";
        $HTML .= "<span class=\"total-results\">";
        $HTML .= "</span>";
		$HTML .= "</div><!-- /.col -->";
		$HTML .= "</div><!-- /.row -->";

        return $HTML;
    }


    public function pdfPrint($pdfquery, $language) {
        $HTML = "";

        //echo "#";
        $pdf_url = $_SERVER['PHP_SELF'];

        if (strpos($pdf_url, 'index.php') > -1) {
            $pdf_url = str_replace('index.php', 'printpdf.php', $pdf_url);
			
			$HTML .= '<a class="btn btn-primary btn-sm btn-print" href="' . $pdf_url . '?' . $pdfquery . '"><span class="fa fa-file-pdf-o btn-icon"></span>' . $language['pdf_printing_link_text'] . '</a>';
        } elseif (strpos($pdf_url, 'building.php') > -1) {
            $pdf_url = str_replace('building.php', 'printpdfbuilding.php', $pdf_url);
			$HTML .= '<a class="btn btn-primary btn-sm btn-print" href="' . $pdf_url . '?' . $pdfquery . '"><span class="fa fa-file-pdf-o btn-icon"></span>' . $language['pdf_printing_link_text'] . '</a>';
        }


        return $HTML;
    }


    public function tourbookUrl($building_code, $theme) {

        $HTML = "";

        $temp_accounts_query = mysql_query("select download_link from temp_accounts where building_code = '" . $building_code . "'") or die("temp acct query error: " . mysql_error());
        $temp_accounts = mysql_fetch_array($temp_accounts_query);


        if ($theme['tourbook_link'] == 1) {
            if (strpos($_SERVER['HTTP_USER_AGENT'], 'iPad')) {

                $tourbookurl = $temp_accounts['download_link'];
                $tourbookurl = str_replace(" ", "%20", $tourbookurl);
                $tourbookurl = str_replace("11:00:00", "11%3A00%3A00", $tourbookurl);

                $HTML .=  '<div class="tourbook-button">';
                $HTML .=  '<a href="'.$tourbookurl.'" onclick="tourbookLink();"><img border="0" src="images/tourbook.png"/></a>';
                $HTML .=  '</div>';
            }
		return $HTML;
        } // end if $theme                                                    
    }


    public function buildingRent($building_add_rent_total, $building_add_rent_operating, $building_add_rent_realty, $building_add_rent_power, $language, $page_width) {
        $rentdiv = 0;
        if ($building_add_rent_total != '') {
            $rentdiv++;
        }
        if ($building_add_rent_operating != '') {
            $rentdiv++;
        }
        if ($building_add_rent_realty != '') {
            $rentdiv++;
        }
        if ($building_add_rent_power != '') {
            $rentdiv++;
        }
        
        $HTML = "";
        if ($rentdiv > 0) {

            if ($page_width < 1080) {
                $col_lg_fix = 'col-lg-6';
            } else {
                $col_lg_fix = 'col-lg-3';
            }
            $HTML = "";
                    $HTML .= '<div class="additional-costs clearfix bg-info">';
                    $HTML .= '<ul class="list-unstyled row">';

            if (strlen($building_add_rent_total) > 0) {
                $HTML .= '<li class="col-sm-6 col-md-6 ' . $col_lg_fix . '"><span class="costs-title">' . $language['add_rent_total_text'] . '</span>';
                $HTML .= '<span class="costs-value"> ' . $building_add_rent_total . ' ' . $language['per_sqft_text'] . '</span></li>';
            }

            if (strlen($building_add_rent_operating) > 0) {
                $HTML .= '<li class="col-sm-6 col-md-6 ' . $col_lg_fix . '"><span class="costs-title">' . $language['add_rent_operating_text'] . '</span>';
                $HTML .= '<span class="costs-value"> ' . $building_add_rent_operating . ' ' . $language['per_sqft_text'] . '</span></li>';
            }

            if (strlen($building_add_rent_realty) > 0) {
                $HTML .= '<li class="col-sm-6 col-md-6 ' . $col_lg_fix . '"><span class="costs-title">' . $language['add_rent_realty_text'] . '</span>';
                $HTML .= '<span class="costs-value"> ' . $building_add_rent_realty . ' ' . $language['per_sqft_text'] . '</span></li>';
            }

            if (strlen($building_add_rent_power) > 0) {
                if ($building_add_rent_power == "0" || $building_add_rent_power == "") {
                    $HTML .= '<li class="col-sm-6 col-md-6 ' . $col_lg_fix . '"><span class="costs-title">' . $language['add_rent_power_text'] . ': Included</span></li>';
                } else {
                    $HTML .= '<li class="col-sm-6 col-md-6 ' . $col_lg_fix . '"><span class="costs-title">' . $language['add_rent_power_text'] . '</span>';
                    $HTML .= '<span class="costs-value"> ' . $building_add_rent_power . ' ' . $language['per_sqft_text'] . '</span></li>';
                }
            }

            $HTML .= '</ul>';
            $HTML .= '</div><!-- /.additional-costs -->';
        }// end if ($rentdiv > 0)

        return $HTML;
    }


    public function showGoogleAnalytics($theme) {
        $HTML = "
         <script>
         (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
         (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
         m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
         })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
         
         ga('create', '";
        $HTML .= $theme['google_analytics'];
        $HTML .= "', '";

        if (array_key_exists('analytics_domain', $theme)) {
            $HTML .= $theme['analytics_domain'];
        }

        $HTML .= "');
         ga('send', 'pageview');
         
      </script>";
        return $HTML;
    }





    public function showGoogleTag($theme) {
        $HTML = "
            <!-- Google Tag Manager -->
<noscript><iframe src='//www.googletagmanager.com/ns.html?id=";
        
        $HTML .= "'". $theme['google_tag_code'] . "'";
        
        $HTML .= "height='0' width='0' style='display:none;visibility:hidden'></iframe></noscript>
            <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});
            var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';
            j.async=true;
            j.src='//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer',";
        
        $HTML .= "'". $theme['google_tag_code'] . "'";
        
        $HTML .= ");</script><!-- End Google Tag Manager -->";

        return $HTML;
    }







     public function if3D($threedee_query_building, $threedee_building_present) {
        $HTML = "";

        if ($threedee_building_present > 0) {
            $prescounter = 0;

            $HTML .= '

        <div class="col-12">
            <div id="floorplan" class="fs_media-container">
                <iframe id="threedeeiframe" name="threedeeiframe" height="100%" width="100%" scrolling="no" frameborder="0" marginwidth="0" src="">
                </iframe>
            </div>
        </div>
		
        <div class="col-12">
            <div class="row">';
//            while ($presentations = mysql_fetch_array($threedee_query_building))
            foreach ($threedee_query_building as $presentations) {

                if ($presentations['type_3d'] == 'pres') {
                    $linkClassClick = 'building3dClick';
                    $endLink = "&hideSidebar=true');	
                            </script>";
                    $iframeSidebar = '&hideSidebar=true"';
                } else if ($presentations['type_3d'] == 'pano') {
                    $linkClassClick = 'buildingPanoClick';
                    $endLink = "')</script>";
                    $iframeSidebar = '"';
                }

                if (strlen($presentations['presentation_name']) > 20) {
                    $presentationname = substr($presentations['presentation_name'], 0, 20) . '...';
                } else {
                    $presentationname = $presentations['presentation_name'];
                }

                
                if ($prescounter == 0) {
                    $HTML .= '
                            <div class="col-3 fs_thumbnail-3d">
                                <a class="selected" href="' . $presentations['presentation_link'] . $iframeSidebar .' target="threedeeiframe"><img src="images/threedee_files/' . $presentations['presentation_thumb'] . '" class="img-fluid" /></a>
                                <p class="caption"><a class="' . $linkClassClick . '" title="' . $presentations['presentation_name'] . '" href="' . $presentations['presentation_link'] . '&hideSidebar=true" target="threedeeiframe">' . $presentationname . '</a></p></div><!-- /layout-thumb --><div class="clear"></div>';
                    $HTML .= "<script type=\"text/javascript\">
                                $('#threedeeiframe').attr('src','";
                    $HTML .= $presentations['presentation_link'];
                    
                        $HTML .= $endLink;	
                    
                    

                } else {
                    $HTML .= '
    	<div class="col-3 fs_thumbnail-3d">
        <a href="' . $presentations['presentation_link'] .  $iframeSidebar .' target="threedeeiframe"><img src="images/threedee_files/' . $presentations['presentation_thumb'] . '" class="img-fluid" /></a>
         <p class="caption"><a class="' . $linkClassClick . '" title="' . $presentations['presentation_name'] . '" href="' . $presentations['presentation_link'];
                    $HTML .= $iframeSidebar;
                    $HTML .= ' target="threedeeiframe">' . $presentationname . '</a></p></div><!-- /layout-thumb --><div class="clear"></div>';
                }
                $prescounter++;
            }

            $HTML .= '</div></div>';
        }
        return $HTML;
    }






    public function if3DMobile($threedee_query_building, $threedee_building_present) {
        $HTML = "";

        if ($threedee_building_present > 0) {
            $prescounter = 0;

            $HTML .= '
<h3 class="tab-content-title">Building 3D</h3>
    <div class="row">
		<div class="col-md-9">
			<div id="floorplan">
   				<img id="threedee_image" src="" width="100%" height="auto">
     		</div><!-- /#floorplan -->
		</div><!-- /.col -->
		
		<div class="col-md-3">';


            while ($presentations = mysql_fetch_array($threedee_query_building)) {


	if (strlen($presentations['presentation_name']) > 20) {
		$presentationname = substr($presentations['presentation_name'], 0, 20).'...';
	} else {
		$presentationname = $presentations['presentation_name'];
	}


  
 if ($prescounter == 0) {
	$HTML .= '
    	<div class="layout-thumb">
        <a class="threedee_link building3dClick selected" href="images/threedee_files/' . $presentations['presentation_thumb'] . '"><img src="images/threedee_files/' . $presentations['presentation_thumb'] . '" width="150" height="150" /></a>
         <p class="caption"><a title="' . $presentations['presentation_name'] . '" class="threedee_link" href="images/threedee_files/' . $presentations['presentation_thumb'] . '">' . $presentationname . '
	     </a></p>
		 </div><!-- /layout-thumb --> 
		 <div class="clear"></div>
    	'; 
 }
 else {
                $HTML .= '
    	<div class="layout-thumb">
        <a class="threedee_link building3dClick" href="images/threedee_files/' . $presentations['presentation_thumb'] . '"><img src="images/threedee_files/' . $presentations['presentation_thumb'] . '" width="150" height="150" /></a>
         <p class="caption"><a title="' . $presentations['presentation_name'] . '" class="threedee_link" href="images/threedee_files/' . $presentations['presentation_thumb'] . '">' . $presentationname . '
	     </a></p>
		 </div><!-- /layout-thumb --> 
		 <div class="clear"></div>
    	';
 }
                if ($prescounter == 0) {
                    $HTML .= "<script type=\"text/javascript\">
	$('#threedee_image').attr('src','images/threedee_files/". $presentations['presentation_thumb']."');";
					$HTML .= "\n\r$('#threedee_image').attr('width','100%').attr('height','auto');\n\r";
                    $HTML .= "</script>";
	
                } // end if prescounter == 0
                $prescounter++;
            }

            $HTML .= '</div><!-- /.col --></div><!-- /.row -->';
        }
        return $HTML;
    }



    public function if3DSuite($threedee_query, $threedee_present, $building_code, $suite_name) {
        $HTML = "";

        if ($threedee_present > 0) {
            $prescounter = 0;

            $HTML .= '
<h3 class="tab-content-title">Suite 3D</h3>
    <div class="row">
		<div class="col-md-9">
			<div id="floorplan">';
            

            
          $HTML .= '<iframe id="threedeeiframe" name="threedeeiframe" height="600px" width="730px" scrolling="no" frameborder="0" marginwidth="0" src="" style="padding: 10px;">
                    </iframe>';
            
            
            $HTML .= '</div><!-- /#floorplan -->
		</div><!-- /.col -->
		
		<div class="col-md-3">';


            foreach ($threedee_query as $presentations) {

	if (strlen($presentations['presentation_name']) > 20) {
		$presentationname = substr($presentations['presentation_name'], 0, 20).'...';
	} else {
		$presentationname = $presentations['presentation_name'];
	}

  
  if ($prescounter == 0) {
	  
	   $HTML .= '
    	<div class="layout-thumb">
        <a class="suite3dClick selected" href="' . $presentations['presentation_link'] . '&hideSidebar=true" target="threedeeiframe"><img src="images/threedee_files/' . $presentations['presentation_thumb'] . '" width="150" height="150" /></a>
         <p class="caption"><a title="' . $presentations['presentation_name'] . '" href="' . $presentations['presentation_link'] . '&hideSidebar=true" target="threedeeiframe">' .
                        $presentationname . '
	     </a></p>
		 </div><!-- /layout-thumb --> 
		 
    	';
  }
  else {
                $HTML .= '
    	<div class="layout-thumb">
        <a class="suite3dClick" href="' . $presentations['presentation_link'] . '&hideSidebar=true" target="threedeeiframe"><img src="images/threedee_files/' . $presentations['presentation_thumb'] . '" width="150" height="150" /></a>
         <p class="caption"><a title="' . $presentations['presentation_name'] . '" href="' . $presentations['presentation_link'] . '&hideSidebar=true" target="threedeeiframe">' .
                        $presentationname . '
	     </a></p>
		 </div><!-- /layout-thumb --> 
		 
    	';
  }
                if ($prescounter == 0) {
                    $HTML .= "<script type=\"text/javascript\">
	$('#threedeeiframe').attr('src','";
                    $HTML .= $presentations['presentation_link'];
                    $HTML .= "&hideSidebar=true');	
	</script>";
                } // end if prescounter == 0
                $prescounter++;
            }

            $HTML .= '</div><!-- /.col --></div><!-- /.row -->';
        }
        return $HTML;
    }







    

    public function if3DSketchfab($threedee_query, $threedee_present, $building_code, $suite_name) {
        $eol = "\n\r";
        $newline = "\n";
        $HTML = "";

        if ($threedee_present > 0) {
            $prescounter = 0;

            $HTML .= '
<h3 class="tab-content-title">Suite 3D</h3>
    <div class="row">
		<div class="col-md-9">
			<div id="floorplan">';
            

          $HTML .= '<a id="sketchfablink" class="sketchfablink suite3dClick" href="" target="_blank" border="0">';
          $HTML .= '<img id="sketchfabiframe" name="sketchfabiframe" height="600px" width="730px" scrolling="no" frameborder="0" marginwidth="0" src="" style="padding: 10px;">';
          $HTML .= '<span class="fa fa-play-circle-o sketchfabplay"></span>';
          $HTML .= '</a>';
            
            
            $HTML .= '</div><!-- /#floorplan -->
		</div><!-- /.col -->
		
		<div class="col-md-3 layoutThumbContainer">';


            foreach ($threedee_query as $presentations) {
                if ($presentations['sketchfabEmbedCode'] != '') {

                    if (strlen($presentations['presentation_name']) > 20) {
                            $presentationname = substr($presentations['presentation_name'], 0, 20).'...';
                    } else {
                            $presentationname = $presentations['presentation_name'];
                    }

  
  if ($prescounter == 0) {
	  $HTML .= '<div class="layout-thumb">'
                        . '<a class="switchSketchLink selected" title="' . $presentations['presentation_name'] . '" href="images/threedee_files/' . $presentations['presentation_thumb'] . '" rel="' . $presentations['sketchfabEmbedCode'] . '" idrel="' . $presentations['threedee_id'] . '">'
                        . '<img src="images/threedee_files/' . $presentations['presentation_thumb'] . '" width="150" height="150" />'
                        . '</a> '
                        . '<p class="caption"><a class="switchSketchLink" title="' . $presentations['presentation_name'] . '" href="images/threedee_files/' . $presentations['presentation_thumb'] . '" rel="' . $presentations['sketchfabEmbedCode'] . '" idrel="' . $presentations['threedee_id'] . '">' . $presentationname . ' </a>'
                        . '</p></div><!-- /layout-thumb --> ';
  }
  
  else {

                $HTML .= '<div class="layout-thumb">'
                        . '<a class="switchSketchLink" title="' . $presentations['presentation_name'] . '" href="images/threedee_files/' . $presentations['presentation_thumb'] . '" rel="' . $presentations['sketchfabEmbedCode'] . '" idrel="' . $presentations['threedee_id'] . '">'
                        . '<img src="images/threedee_files/' . $presentations['presentation_thumb'] . '" width="150" height="150" />'
                        . '</a> '
                        . '<p class="caption"><a class="switchSketchLink" title="' . $presentations['presentation_name'] . '" href="images/threedee_files/' . $presentations['presentation_thumb'] . '" rel="' . $presentations['sketchfabEmbedCode'] . '" idrel="' . $presentations['threedee_id'] . '">' . $presentationname . ' </a>'
                        . '</p></div><!-- /layout-thumb --> ';
  }
                if ($prescounter == 0) {
                    $HTML .= "<script type=\"text/javascript\">";
                    
                    
                    $HTML .= "$(document).ready(function () {";
                    
                    $HTML .= "$('#sketchfabiframe').attr('src','images/threedee_files/";
                    $HTML .= $presentations['presentation_thumb'];
                    $HTML .= "');";
                    
                            
                    $HTML .= "$('#sketchfablink').attr('href','presentations.php?threedee_id=";
                    $HTML .= $presentations['threedee_id'];
                    $HTML .= "');";
                    
                    
                    $HTML .= "$(document).on('click','.switchSketchLink',function(){";
                    $HTML .= "event.preventDefault();";
                    $HTML .= "var img = $(this).attr('href');";
                    $HTML .= "var sketchurl = $(this).attr('rel');";
                    $HTML .= "var threedee_id = $(this).attr('idrel');";
                    $HTML .= "$('#sketchfabiframe').attr('src',img);";
                    $HTML .= "$('#sketchfablink').attr('href','presentations.php?threedee_id='+threedee_id);";
                    $HTML .= "});";
                    
                    $HTML .= "});";
                    
                    $HTML .= "</script>";
                } // end if prescounter == 0
                $prescounter++;
                }
            }

            $HTML .= '</div><!-- /.col --></div><!-- /.row -->';
        }
        return $HTML;
    }



    
    public function if3DSuiteMobileXS($threedee_query2) {
        
        $HTML = "";
        $HTML .= '<ul class="documents clearfix list-unstyled row">';
        foreach ($threedee_query2 as $row) {
            $plan_name = $row['presentation_name'];
            if (strlen($plan_name) > 20) {
                $plan_name = substr($plan_name, 0, 18) . '...';
            }


            $HTML .= '<li class="col-sm-4 "><div class="thumbnail thumbnail-twodee">';

            $HTML .= '<img src="images/threedee_files/' . $row['presentation_thumb'] . '" />';

            
            $HTML .='<p class="caption">';

            $HTML .= $plan_name;
            
            $HTML .='</p></div></li>';
        }

        $HTML .= '</ul>';
        return $HTML;
    }



    public function if3DSuiteMobile($threedee_query, $threedee_present) {
        $HTML = "";

        if ($threedee_present > 0) {
            $prescounter = 0;

            $HTML .= '
<h3 class="tab-content-title">Suite 3D</h3>
    <div class="row">
		<div class="col-md-9">
			<div id="floorplan">
   				<img id="threedee_image" src="" width="100%" height="auto">
     		</div><!-- /#floorplan -->
		</div><!-- /.col -->
		
		<div class="col-md-3">';


            while ($presentations = mysql_fetch_array($threedee_query)) {

	if (strlen($presentations['presentation_name']) > 20) {
		$presentationname = substr($presentations['presentation_name'], 0, 20).'...';
	} else {
		$presentationname = $presentations['presentation_name'];
	}
	
  
 if ($prescounter == 0) {
	 $HTML .= '
    	<div class="thumbnail thumbnail-twodee">
        <a class="threedee_link suite3dClick selected" href="images/threedee_files/' . $presentations['presentation_thumb'] . '"><img src="images/threedee_files/' . $presentations['presentation_thumb'] . '" width="150" height="150" /></a>
         <p class="caption"><a title="' . $presentations['presentation_name'] . '" class="threedee_link suite3dClick" href="images/threedee_files/' . $presentations['presentation_thumb'] . '">' . $presentationname . '
	     </a></p>
		 </div><!-- /layout-thumb -->
    	';
 }
 
 else {
                $HTML .= '
    	<div class="thumbnail thumbnail-twodee">
        <a class="threedee_link suite3dClick" href="images/threedee_files/' . $presentations['presentation_thumb'] . '"><img src="images/threedee_files/' . $presentations['presentation_thumb'] . '" width="150" height="150" /></a>
         <p class="caption"><a title="' . $presentations['presentation_name'] . '" class="threedee_link suite3dClick" href="images/threedee_files/' . $presentations['presentation_thumb'] . '">' . $presentationname . '
	     </a></p>
		 </div><!-- /layout-thumb -->
    	';
 }
            }

            $HTML .= '</div><!-- /.col --></div><!-- /.row -->';
        }
        return $HTML;
    }



    public function ifPdf($queries, $pdf_query, $docx_query, $other_docs_query, $graphics) {
        $HTML = "";
        if ((count($pdf_query) + count($docx_query) + count($other_docs_query)) > 0) {
            
            $HTML .= $queries->query_fetch_array($pdf_query, $graphics);
            $HTML .= $queries->query_fetch_array($docx_query, $graphics);
            $HTML .= $queries->query_fetch_array($other_docs_query, $graphics);
           
        }
        return $HTML;
    }



    public function ifPdf_justpdfs($pdf_query2) {
            $HTML = "";
            $HTML .= '<ul class="documents clearfix list-unstyled row">';
            while ($pdf_query2 <= count($pdf_query2)) {
            $plan_name = $display_filename;
            if (strlen($display_filename) > 20) {
                $plan_name = substr($plan_name, 0, 18) . '...';
            }
            $HTML .= '<li class="col-sm-4 "><div class="thumbnail thumbnail-twodee"><a href="' . $plan_uri . '" title="' . $display_filename . ' "  target="_blank" >
            <img src="images/twodee_files/' . $plan_thumb . '" /></a><p class="caption">
            <a class="pdfclick" href="' . $plan_uri . '" title="' . $display_filename . '"  target="_blank">' . $plan_name . '</a>
            </p></div></li>';


            }
            $HTML .= '</ul>';
        return $HTML;
    }




    public function ifImagesAndVideos($language, $video_query, $preview_images_query, $row) {
        $HTML = "";
        $HTML .= '';

        foreach ($preview_images_query as $galleryimages) {

            if (strlen($galleryimages['file_name']) > 30) {
                $galleryimagename = substr($galleryimages['file_name'], 0, 30).'...';
            } else {
                $galleryimagename = $galleryimages['file_name'];
            }
            
            $gallerythumbnail = str_ireplace('.' . $galleryimages['extension'], '', $galleryimages['file_name']) . "_thumb." . $galleryimages['extension'];
//            file_name.extension
            $HTML .='<div class="col fs_gallery-thumbnail"><a data-fancybox="gallery" href="images/preview_images/' . $galleryimages['file_name'] . '" title="' . $galleryimages['display_file_name'] . '"><img src="images/preview_images/' . $gallerythumbnail . '" alt="' . $galleryimages['display_file_name'] . '"  class="img-fluid"></a></div>';
            
        }
        
        return $HTML;
    }
    
    public function getVideos($video_query, $row) {
        $HTML = '';
        $count = 1;
        foreach ($video_query as $row) {
            $HTML .= $this->documentsWhy($row, $count);
            $count = $count + 1;
        }
        
        return $HTML;
    }
	
	

    public function xsImageCarousel($language, $building_thumbnail_image_name, $preview_images_query, $threedee_query_building, $threedee_building_present, $video_query, $video_count) {
        $HTML = "";
        $HTML .= '';
        
        $ttl_images =  count($preview_images_query) + $threedee_building_present + $video_count;

        if ($building_thumbnail_image_name != "images/preview_images/" && $building_thumbnail_image_name != "") {
            $ttl_images = $ttl_images + 1;
        }
        
        if ($ttl_images === 0) {
            // show no images image
            echo '<img class="img-responsive" src="images/no_thumb_700.png">';
        }
        $count = 0;

        $HTML .= '<ol class="carousel-indicators">';
            
        $ci = 0;

        for ($i = 1; $i <= $ttl_images; $i++) {

            if ($ci == 0) {
                $HTML .= '<li data-target="#carousel-property" data-slide-to="'.$ci.'" class="active"></li>';
            } else {
                $HTML .= '   <li data-target="#carousel-property" data-slide-to="'.$ci.'"></li>';
            }

        $ci++;
        }
        
        $HTML .= '</ol>';
        $HTML .= '<!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">';

        $ci2 = 0;


        if ($building_thumbnail_image_name != "images/preview_images/" && $building_thumbnail_image_name != "") {
            $ci2 = 1;
            $building_thumbnail_thumb_image_name = str_replace(".jpg", "_thumb.jpg", $building_thumbnail_image_name);

                $HTML .= '<div class="item active">
                <img rel="buildingthumb" class="img-responsive" src="'. $building_thumbnail_image_name .'" alt="">
                </div><!-- /.item -->'; 
        }


        foreach($preview_images_query as $carouselimages) {
            
            $image_thumbnail_name = str_ireplace('.' . $carouselimages['extension'], '', $carouselimages['file_name']) . "_thumb." . $carouselimages['extension'];
            
            if ($ci2 == 0) {
                $HTML .= '<div class="item active">
                <img class="img-responsive" src="images/preview_images/'. $image_thumbnail_name .'" alt="">
                </div><!-- /.item -->'; 


            } else {
                $HTML .= '<div class="item">
                <img class="img-responsive" src="images/preview_images/'. $image_thumbnail_name .'" alt="">
                </div><!-- /.item -->';


            }
        $ci2++;
        }
        
        // add 3D thumbnails and force their dimensions to match
        foreach ($threedee_query_building as $threedee) {
                $HTML .= '<div class="item img-responsive">
                <img class="img-responsive" src="images/threedee_files/'. $threedee['presentation_thumb'] .'" alt=""  >
                </div><!-- /.item -->';
            


        }
        
        
        
        
        foreach ($video_query as $videos) {

            $HTML .= '<div class="item">
                <a href="images/video_files/' . $videos['file_name'] . '" class="fancybox fancybox.iframe"><div class="play-button"><span class="fa fa-play-circle"></span></div><img class="img-responsive" src="'. $videos['uri'] .'?width=700&height=500&resizeMode=INNER" alt="video thumbnail"></a>
                </div><!-- /.item -->';
        }
            

        
            $HTML .= '  </div><!-- /.carousel-inner -->';

        return $HTML;
    }




    
      public function xsSuiteImageCarousel($language, $suite_preview_images_query, $suite_preview_images_count) {

        $HTML = "";
        $HTML .= '';
        $ttl_images =  count($suite_preview_images_query);

        if ($ttl_images === 0) {
            
            // show no images image
            echo '<img src="images/no_thumb_700.png">';

        }
        
        $count = 0;

        $HTML .= '<ol class="carousel-indicators">';
            
        $ci = 0;

        // change this to a simple counter
        for ($i = 1; $i <= $ttl_images; $i++) {

            if ($ci == 0) {
                $HTML .= '<li data-target="#carousel-property" data-slide-to="'.$ci.'" class="active"></li>';
            } else {
                $HTML .= '   <li data-target="#carousel-property" data-slide-to="'.$ci.'"></li>';
            }

        $ci++;
        }
        
        $HTML .= '</ol>';
        $HTML .= '<!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">';

        $ci2 = 0;
        foreach($suite_preview_images_query as $carouselimages) {
            $image_thumbnail_name = str_ireplace('.' . $carouselimages['extension'], '', $carouselimages['file_name']) . "_thumb." . $carouselimages['extension'];
            if ($ci2 == 0) {
                $HTML .= '<div class="item active">
  <img src="images/suite_preview_images/'. $image_thumbnail_name .'" alt="">
</div><!-- /.item -->';  
            } else {
                $HTML .= '<div class="item">
  <img src="images/suite_preview_images/'. $image_thumbnail_name .'" alt="">
 </div><!-- /.item -->';
            }
        $ci2++;
        }


								     
            
            $HTML .= '  </div><!-- /.carousel-inner -->';

        return $HTML;
    }

    
 
    

    //used by $this->ifMultimedia(...);
    private function documentsWhy($row, $count) {

        $HTML = "";
        $document_name = $row['file_name'];
		$document_name_fullsize = $document_name;

        if (strlen($document_name) > 30) {
            $document_name = substr($document_name, 0, 30) . '...';
        } else {
			$document_name = $document_name;
		}

        $document_uri = $row['uri'];
        $document_extension = $row['extension'];

        if ($document_extension == "mp4") {
            $HTML .= $this->ifExtensionMp4($document_uri, $document_extension, $count, $document_name, $document_name_fullsize);
        } // end if (mp4)

        return $HTML;
    }

    //used by $this->ifMultimedia(...);
    private function ifExtensionMp4($document_uri, $document_extension, $count, $document_name, $document_name_fullsize) {
        
        $video_thumbnail_name = str_replace('.' . $document_extension ,"_thumb.jpg", $document_name_fullsize);

			
			$HTML = '<div class="col fs_gallery-thumbnail">
                                <div class="video-wrapper">
                                    <div class="video-content">
                                      <video id="vid' . $count . '" class="video-js vjs-default-skin img-fluid"
                                      controls preload="none" width="auto" height="auto" poster="images/video_files/' . $video_thumbnail_name . '">
                                             <source src="images/video_files/' . $document_name_fullsize . '" type=\'video/mp4\' />
                                      </video>                                   </div><!-- /.video-content -->
                                    </div><!-- /.video-wrapper -->
				</div>';
//		}
	
        return $HTML;
    }


    public function buildingStacking($building_type, $building_id, $building_floors, $suites_html, $contacts_query, $building_code, $suite_floor, $language, $building_typical_floor_size) {

        $HTML = "";
        if ($building_type != "Retail") {

            $ii = $building_floors;
            $stacking_html = '
                           	<table id="comparison-report">
                                     <thead>
                                     <tr>
                           	<th>
                           	<ul class="stacking-plan building-stack">';



            while ($ii > 0) {

                if ($ii == 1) {
                    $stacking_html .= '<li class="last">';
                } else {
                    $stacking_html .= '<li>';
                }


                $suite_floor_query = mysql_query("select * from suites where building_code = '" . $building_code . "' and floor_number = '" . $ii . "' order by suite_name asc");
                $suites_on_floor_count = mysql_num_rows($suite_floor_query);


                $jj = 1;

                if ($suites_on_floor_count > 0) {

                    $stacking_html .= '<ul>';

                    while ($suite_floor = mysql_fetch_array($suite_floor_query)) {

                        if ($suite_floor['availability'] < time()) {

                            $stacking_html .= '<li class="stack-immediate"';
                        } elseif ($suite_floor['availability'] > time()) {
                            $stacking_html .= '<li class="stack-future"';
                        }

                        if ($building_typical_floor_size == '') {

                            $total_floorspace_query = mysql_query('select sum(net_rentable_area) from suites where building_code = "' . $building_code . '" and floor_number ="' . $ii . '"');
                            $total_floorspace = mysql_fetch_array($total_floorspace_query);
                            $total = $total_floorspace['sum(net_rentable_area)'];
                        } else {
                            $total = $building_typical_floor_size;
                        }
                        $thissuite = $suite_floor['net_rentable_area'];

                        if ($building_typical_floor_size != '') {
                            if ($total > $building_typical_floor_size) {
                                $total = $building_typical_floor_size;
                            }
                        }


                        $percent_calc = ($thissuite / $total) * 100;
                        if ($percent_calc > 100) {
                            $percent = 100;
                        } else {
                            $percent = $percent_calc;
                        }

                        if ($percent >= 10) {
                            $stackWidth = round($percent) - 1;
                        } else {
                            $stackWidth = $percent;
                        }

                        $stacking_html .= 'style="width: ' . $stackWidth . '%">';

                        if ($suite_floor['presentation_link'] != "") {

                            $stacking_html .= '<a href="suite.php?building=' . $suite_floor['building_code'] . '&suiteid=' . $suite_floor['suite_id'] . '" title="suite ' . $suite_floor['suite_name'] . ' - ' . number_format($suite_floor['net_rentable_area']) . ' SF"></a></li>';
                        } else if ($suite_floor['pdf_plan'] != "") {
                            $stacking_html .= '<a href="' . $suite_floor['pdf_plan'] . '" target="_blank" title="suite ' . $suite_floor['suite_name'] . ' - ' . number_format($suite_floor['net_rentable_area']) . ' SF"></a></li>';
                        } else {
                            $stacking_html .= '<a href="building.php?building=' . $suite_floor['building_code'] . '" title="suite ' . $suite_floor['suite_name'] . ' - ' . number_format($suite_floor['net_rentable_area']) . ' SF"></a></li>';
                        }

                        $jj++;
                    }

                    $stacking_html .= '</ul>';
                }

                $stacking_html .= '</li>';

                $ii--;
            }

            $stacking_html .= '
                           	</ul>
                           	</th>
                           	<th>
                           	<ul class="floornames">';

            $ii = $building_floors;

            while ($ii > 0) {

                if ($suite_floor['floorname'] == '') {
                    $floorname = $ii;
                } else {
                    $floorname = $suite_floor['floorname'];
                }

                $stacking_html .= '<li class="floorname">Floor ' . $floorname . ' </li>';
                $ii--;
            }

            $stacking_html .= '</ul></th></tr>
                                     </thead>
                                     </table>';
        }

//*******************************
//*******************************
//*******************************
//*******************************
//*******************************
//*******************************
//*******************************
//*******************************
		if ($language['lang_id'] == 1) {
			$translateretail = "Retail";
		}
		if ($language['lang_id'] == 0) {
			$translateretail = "Détail";
		}


        if ($suites_html == "" && $building_type != $translateretail) {
			
		
            $main_contact_result = $contacts_query;
			

           $HTML .= '<div class="table-responsive">
                        <table id="availabilities" class="table table-striped table-hover">
                           <thead>
                              <tr>';
           
     if ($building_type == "Industrial" || $building_type == "Industriel") {
           $HTML .= '<th class="suite">' . $language['unit_text'] . '</th>';
    } else {
           $HTML .= '<th class="suite">' . $language['suite_text'] . '</th>';
    }          
           
           $HTML .= '<th class="area">'.$language['index_table_header_area_text'] . ' <br>'
                    . ' (' . $language['min_divisible_text'] . ' | ' . $language['contiguous_text'] . ')</th>
                    <th class="rent">' . $language['rent_text'] . '</th>
                    <th class="availability">' . $language['availability_header_text'] . '</th>
                    <th class="description hidden-sm">' . $language['notes_text'] . '</th>

                 </tr>
              </thead>';
                
           $HTML .= '<tr>
                        <td colspan="6">&nbsp;</td>
                        </tr>
                        <tr>
                        <td colspan="6">
                        <p class="leasedbuildingmessage_bold">' . $language['all_leased_text'] . '</p>
                        <p class="leasedbuildingmessage">' . $language['no_availabilities_text'] . '</p>
                        </td>
                        </tr>
                        </table>
                        </div><!-- /.table-responsive -->';
            $no_vacancies = 1;
			
        } elseif ($suites_html == "" && $building_type == $translateretail) {
			$HTML .= ' <div class="table-responsive">
		   				<table id="availabilities" class="table table-striped table-hover">
                           <thead>
                              <tr>';
           
     if ($building_type == "Industrial" || $building_type == "Industriel") {
           $HTML .= '<th class="suite">' . $language['unit_text'] . '</th>';
    } else {
           $HTML .= '<th class="suite">' . $language['suite_text'] . '</th>';
    }          
           
           $HTML .= '
                                 <th class="area">'.$language['index_table_header_area_text'] . ' <br>'
                                    . ' (' . $language['min_divisible_text'] . ' | ' . $language['contiguous_text'] . ')</th>
                                 <th class="rent">' . $language['rent_text'] . '</th>
                                 <th class="availability">' . $language['availability_header_text'] . '</th>
                                 <th class="description hidden-sm">' . $language['notes_text'] . '</th>
                         
                              </tr>
                           </thead>';
			$HTML .= '		  <tr>
							  <td colspan="6">&nbsp;</td>
							  </tr>
							  <tr>
							  <td colspan="6">
							  <p class="leasedbuildingmessage_bold">' . $language['call_for_availability_text'] . '</p>
							  </td>
							  </tr>
							  </table></div><!-- /.table-responsive -->';
			$no_vacancies = 1;

		} else {

            $HTML .= ' <div class="table-responsive">
		   				<table id="availabilities" class="table table-striped table-hover">
                           <thead>
                              <tr>';
           
     if ($building_type == "Industrial" || $building_type == "Industriel") {
           $HTML .= '<th class="suite">' . $language['unit_text'] . '</th>';
        $HTML .= '<th class="area">'.$language['index_table_header_area_text'] . ' <br>'
                    . ' (' . $language['min_divisible_text'] . ' | ' . $language['contiguous_text'] . ')</th>';
        $HTML .= '<th class="industrialrent">' . $language['rent_text'] . '</th>';
        $HTML .= '<th class="industrialavailability">' . $language['availability_header_text'] . '</th>';
        $HTML .= '<th class="loading">' . $language['loading_text'] . '</th>';
        $HTML .= '<th class="power">' . $language['power_text'] . '</th>';
        $HTML .= '<th class="ceiling">' . $language['ceiling_text'] . '</th>';
        $HTML .= '<th class="description hidden-sm">' . $language['notes_text'] . '</th>';
//        $HTML .= '</tr>';       

           
    } else {
           $HTML .= '<th class="suite">' . $language['suite_text'] . '</th>';
        $HTML .= '<th class="area">'.$language['index_table_header_area_text'] . ' <br>'
                    . ' (' . $language['min_divisible_text'] . ' | ' . $language['contiguous_text'] . ')</th>';
        $HTML .= '<th class="rent">' . $language['rent_text'] . '</th>';
        $HTML .= '<th class="availability">' . $language['availability_header_text'] . '</th>';
        $HTML .= '<th class="description hidden-sm">' . $language['notes_text'] . '</th>';
    }          
           
        $HTML .= '</tr></thead>';
                         
            $HTML .= $suites_html;

            $HTML .= '</table></div><!-- /.table-responsive -->';
        }

        return $HTML;
    }



    public function SuiteDivs_buildingpage($building_code, $queries, $row, $lang, $lang_id, $language, $i, $theme, $db) {
        $suite_description = strip_tags($row['description'], '<p>,<br>,<br />,<strong>,<u>,<b>,<i>,<ul>,<li>,<ol>,<a>');
        $suite_availability = $queries->get_suite_availability($language, $row);
        $suite_net_rentable_area = $queries->get_suite_net_rentable_area($lang, $row);
        $suite_contiguous_area = $queries->get_suite_contiguous_area($lang, $row);
        $suite_min_divisible_area = $queries->get_suite_min_divisible($lang, $row);
        $suite_net_rent = $queries->get_suite_net_rent($language, $row);
        $suite_new = $row['new'];
        $suite_model = $row['model'];
        $suite_leased = $row['leased'];
        $suite_promoted = $row['promoted'];
        $cross_out = $queries->get_cross_out($row['leased']);
        $suite_name = $row['suite_name'];
		$suite_id = $row['suite_id'];

		$suitedivs_html = '';
		
		$icon = '';
        if ($suite_new == 'true') {
            $icon = '<span class="label label-danger">' . $language['new_text'] . '</span>';
        } elseif ($suite_model == 'true') {
            $icon = '<span class="label label-warning">' . $language['model_text'] . '</span>';
        } elseif ($suite_leased == 'true') {
            $icon = '<span class="label label-primary">' . $language['leased_text'] . '</span>';
        } elseif ($suite_promoted == 'true') {
            $icon = '<span class="label label-promoted">' . $language['promoted_text'] . '</span>';
        } else {
            $icon = '';
        }

		$suite_pdf_query = $queries->pdf_query($building_code, $suite_id, $lang_id, $db);
                $suite_pdf_count= count($suite_pdf_query);
		$suite_pdf = $suite_pdf_query;

if ($suite_pdf_count > 0) {
		if ($suite_pdf[0]['plan_thumb'] != '') {
			$pdf_image = 'images/twodee_files/thumb_'.$suite_pdf[0]['plan_thumb'];
		} else {
			$pdf_image = 'images/no_pdf_image.png';
		}
} else {
    $pdf_image = 'images/no_pdf_image.png';
}

                    
                    if ($suite_net_rent == "0.00") {
                        $suite_net_rent = $language['negotiable_text'];
                    }
//                        $suite_net_rent = "test";

                $suitedivs_html = '<div class="col-sm-4">
                <div class="thumbnail thumbnail-available-space">';
                
                if ($suite_promoted == 'true') {
                    $suitedivs_html .= '<span style="font-style: italic;font-weight: bold;">';
                }
                
                
                
                if ($theme['result_links_to_corp_website'] == 1) {
                    $suitedivs_html .= '<a href=""><img src="'. $pdf_image .'" class="img-responsive" /></a>';
                } else {
                    $suitedivs_html .= '<a href="suite.php?suiteid='. $suite_id . $queries->get_query_str() .'"><img src="'. $pdf_image .'" class="img-responsive" /></a>';
                }
                $suitedivs_html .= '<div class="caption clearfix">';
                
                if ($theme['result_links_to_corp_website'] == 1) {
                    $suitedivs_html .= '<h3 class="caption-title"><a href="">Suite '. $suite_name .' ' . $icon .'</a></h3>';
                } else {
                    $suitedivs_html .= '<h3 class="caption-title"><a href="suite.php?suiteid='. $suite_id . $queries->get_query_str() .'">Suite '. $suite_name .' ' . $icon .'</a></h3>';
                }
                
                
                
                

                $suitedivs_html .='<div class="row"><div class="col-xs-6">';
                
                
                
                $suitedivs_html .='<p class="available-space"><a class="panellink" href="suite.php?building=' . $row['building_code'] . '&lang=' . $lang . '&suiteid=' . $row['suite_id'] . '">' . $suite_net_rentable_area . ' ' . $language['sq_ft_trans'] . '</a></p>';                
                
                if ($suite_contiguous_area != '0' || $suite_min_divisible_area != '0') {
                $suitedivs_html .='<p class="contiguous-space text-muted">(<a class="panellink" href="suite.php?building=' . $row['building_code'] . '&lang=' . $lang . '&suiteid=' . $row['suite_id'] . '">';


                if ((intval($suite_contiguous_area) != 0) && (intval($suite_min_divisible_area) != 0)) {
                    $suitedivs_html .= $suite_min_divisible_area . ' | ' . $suite_contiguous_area;
                } else if ((intval($suite_contiguous_area) == 0) && (intval($suite_min_divisible_area) != 0)) {
                    $suitedivs_html .= $suite_min_divisible_area;
                } else if ((intval($suite_contiguous_area) != 0) && (intval($suite_min_divisible_area) == 0)) {
                    $suitedivs_html .= $suite_contiguous_area;
                } else {

                }
                    $suitedivs_html .='</a>)';
                }
                    $suitedivs_html .="</p>";
                    
                if ($suite_promoted == 'true') {
                    $suitedivs_html .= '</span>';
                }
                
                $suitedivs_html .='</div>';
                

                
                $suitedivs_html .='<div class="col-xs-6">';
                    $suitedivs_html .= '<p>'. $suite_availability . '</p>';
                    $suitedivs_html .= '<p>' . $suite_net_rent . '</p>';
                $suitedivs_html .='</div>';
                
                $suitedivs_html .= '</div>
                                    </div>
                                    </div></div>';					
				
				if( $i%3==0 ) {
					$suitedivs_html .= '<div class="clearfix visible-sm-block"></div>';
					$suitedivs_html .= '<div class="clearfix visible-md-block"></div>';
					$suitedivs_html .= '<div class="clearfix visible-lg-block"></div>';
				}

		return $suitedivs_html;
	}



    public function features_tab_content($sections_count, $sections_query, $queries) {

        $features_tab_content_present = 0;
//        $pdf_present = 0;
//        $pdfoutput = $this->ifPdf($queries, $pdf_query,$docx_query, $other_docs_query, $graphics);
//        if ( $pdfoutput != '') {
//            $features_tab_content_present++;
//            $pdf_present = 1;
//        } 
        

        
        $features_tab_content = "";


        if ($sections_count > 0) {
            $features_tab_content_present++;
            $ii = 1;
			
//			$features_tab_content .= '<div class="row">';
			
            while ($row = mysql_fetch_array($sections_query)) {
                $features_title = $row['section_title'];
                $features_content = $row['section_content'];
                $features_tab_content .= '<div class="card-title-section"><h4>' . $features_title . '</h4></div>';

                $features_tab_content .= '<div class="row">';
                $features_tab_content .= '<div class="col fs_sections-content">';
                $features_content = str_replace('&nbsp;', '', $features_content);
                $features_tab_content .= strip_tags($features_content, '<p>,<br>,<br />,<strong>,<u>,<b>,<i>,<ul>,<li>,<ol>,<a>');
                $features_tab_content .= '</div></div>';
                if ($ii === 2) {


                }
                


                $ii++;
            }
			
//		$features_tab_content .= '</div><!-- /.row -->';	
        } else {
            if ($pdf_present == 0) {
                $features_tab_content .= '<p>There are no features listed for this building</p>';
            }
            
        }
//            $features_tab_content .= '<footer class="tab-content-footer hidden-xs hidden-sm">
//                        				
//                        				<div class="row">
//											' . $contacts_tabfooter_html . '
//                                        </div><!-- /.row -->
//                        			
//                        			</footer><!-- /.tab-content-footer -->';
        $arr = array(
            'features_tab_content' => $features_tab_content,
            'features_tab_content_present' => $features_tab_content_present
        );

        return $arr;
    }


    public function pdfPrint_building($pdfquery, $language) {


        $HTML = "";
        $pdf_url = $_SERVER['PHP_SELF'];

        if (strpos($pdf_url, 'index.php') > -1) {
            $pdf_url = str_replace('index.php', 'printpdf.php', $pdf_url);
			$HTML .= '<a class="btn btn-primary btn-sm btn-print" href="'. $pdf_url . '?' . $pdfquery . '"><span class="fa fa-file-pdf-o btn-icon"></span>' . $language['pdf_printing_link_text'] . '</a>';
        } elseif (strpos($pdf_url, 'building.php') > -1) {
            $pdf_url = str_replace('building.php', 'printpdfbuildingpackage.php', $pdf_url);
			$HTML .= '<a class="btn btn-primary btn-sm btn-print" href="' . $pdf_url . '?' . $pdfquery . '"><span class="fa fa-file-pdf-o btn-icon"></span>' . $language['pdf_printing_link_text'] . '</a>';
        }
        return $HTML;
    }
	
	

    public function pdfPrint_building_mobile($pdfquery, $language) {


        $HTML = "";
        $pdf_url = $_SERVER['PHP_SELF'];
		

        if (strpos($pdf_url, 'index.php') > -1) {
            $pdf_url = str_replace('index.php', 'printpdf.php', $pdf_url);
            $HTML .= '<a class="btn-print" href="'. $pdf_url . '?' . $pdfquery . '"><span class="fa fa-file-pdf-o"></span>' . $language['pdf_printing_link_text'] . '</a>';
        } elseif (strpos($pdf_url, 'building.php') > -1) {
            $pdf_url = str_replace('building.php', 'printpdfbuildingpackage.php', $pdf_url);
            $HTML .= '<a class="btn-print" href="' . $pdf_url . '?' . $pdfquery . '"><span class="fa fa-file-pdf-o"></span>' . $language['pdf_printing_link_text'] . '</a>';
        }
        return $HTML;
    }



    public function building_contacts_html($row, $theme) {
        $contacts_html = "";
        $contacts_html .= '<li class="row fs_building-contact">';
        $contacts_html .= '<div class="col fs_col-contact-icon">';
        $contacts_html .= '<div class="fs_contact-link"><a href="mailto:' . $row['email'] . '"><i class="material-icons fs_contact-link-icon">email</i>';
        $contacts_html .= '<div class="fs_contact-link-text">Contact</div>';
        $contacts_html .= '</a></div></div>';
        $contacts_html .= '<div class="col fs_col-contact-details">';
        $contacts_html .= '<ul class="fs_contact-details-list">';
        $contacts_html .= '<li><b>' . $row['name'] . '</b></li>';    
        $contacts_html .= '<li>' . $row['title'] . '</li>';
        $contacts_html .= '<li>' . $row['company'] . '</li>';
        $contacts_html .= '<li>' . $row['phone'] . '</li>';
        $contacts_html .= '<li><a class="mailClick" href="mailto:' . $row['email'] . '">' . $row['email'] . '</a></li>';
        $contacts_html .= '</ul>';
        $contacts_html .= '</div>';
        $contacts_html .= '</li>';

        return $contacts_html;
    }


    public function building_contacts_tabfooter_html($row, $theme) {
        
        if ($theme['page_width'] < 860) {
            $footercontact_col = "col-md-4";
        } else {
            $footercontact_col = "col-md-5";
        }
        $contacts_tabfooter_html = "";
        $contacts_tabfooter_html .= '<ul class="list-unstyled suite-contacts-list ' . $footercontact_col . ' col-sm-6">';
        $contacts_tabfooter_html .= '<li class="suite-contact-item"><b>' . $row['name'] . '</b></li>';
        $contacts_tabfooter_html .= '<li class="suite-contact-item"><b><span title="'. $row['title'].'">' . $row['title'] . '</span></b></li>';
        $contacts_tabfooter_html .= '<li class="suite-contact-item">' . $row['company'] . '</li>';
        $contacts_tabfooter_html .= '<li class="suite-contact-item"><a class="mailClick" href="mailto:' . $row['email'] . '">' . $row['email'] . '</a></li>';
        $contacts_tabfooter_html .= '<li class="suite-contact-item">' . $row['phone'] . '</li>';
        $contacts_tabfooter_html .= '</ul>';

        return $contacts_tabfooter_html;
    }
    
    

   public function ifPDF_suite($row, $multimedia_tab_content, $suite_documents_count, $suite_documents_query,  $graphics) {
 
            if ($suite_documents_count > 0) {
            $multimedia_tab_content .= '<h3 class="tab-content-title">Documents</h3>';
        }

        $multimedia_tab_content .= '<ul class="documents clearfix list-unstyled row">';


        foreach($suite_documents_query as $row) {

            $document_name = $row['file_name'];
            if (strlen($document_name) > 30) {
                $document_name_short = substr($document_name, 0, 30) . '...';
            } else {
                $document_name_short = $document_name;
            }

            if ($row['thumb'] != '') {
                $document_thumb = 'images/suite_files/' . $row['thumb'];
            } else {
                $document_thumb = 'images/no_thumb_700.png';
            }

if (strpos(strtoupper($row['extension']), 'MP4') !== false || strpos(strtoupper($row['extension']), 'PDF') !== false) {
            $multimedia_tab_content .= '<li class="col-sm-4"><div class="thumbnail thumbnail-document"><a target="_blank" href="images/suite_files/' . $document_name . '" title="' . $document_name . '"><img src="' . $document_thumb . '" width="180" /></a><p class="caption"><a target="_blank" href="images/suite_files/' . $document_name . '" title="' . $document_name . '">' . $document_name_short . '</a></p></div><!-- /.thumbnail --></li>';
        }
        
        else{
            
            $pth = $graphics->addTextToPNG($row['extension'], "images/newFileExtensions/icon.png", "images/newFileExtensions/");
            
             $multimedia_tab_content .= '<li class="col-sm-4"><div class="thumbnail thumbnail-document"><a target="_blank" href="images/suite_files/' . 
                     $document_name . '" title="' . $document_name . '"><img src="' . $pth . 
                     '" width="180" /></a><p class="caption"><a target="_blank" href="images/suite_files/' . $document_name . 
                     '" title="' . $document_name . '">' . $document_name_short . '</a></p></div><!-- /.thumbnail --></li>';

             
             
        }
        
        
        
}

        $multimedia_tab_content .= '</ul>';
 
        return $multimedia_tab_content;
        
}
    

    public function ifImagesAndVideos_suite($language, $suite_video_count, $suite_preview_images_count, $row, $video_query, $multimedia_tab_content, $suite_preview_images_query2,  $contacts_tabfooter_html) {




        if ($suite_video_count + $suite_preview_images_count > 0) {
            $multimedia_tab_content .= '<h3 class="tab-content-title">' . $language['images_and_video_text'] . '</h3>';
        }
        $multimedia_tab_content .= '<div class="gallery clearfix row">';


        $count = 1;
        foreach ($video_query as $row) {
            $document_name = $row['file_name'];
			$document_name_full = $row['file_name'];

            if (strlen($document_name) > 30) {
                $document_name = substr($document_name, 0, 30) . '...';
            } else {
				$document_name = $document_name;
			}

            $document_uri = $row['uri'];
            $document_extension = $row['extension'];

            if ($document_extension == "mp4") {

               $multimedia_tab_content .= '<div class="col-sm-4">';
                $multimedia_tab_content .= '<div class="thumbnail thumbnail-video"><div class="video-wrapper"><div class="video-content"><video id="vid' . $count . '" class="video-js vjs-default-skin img-responsive" controls preload="none" width="auto" height="auto" poster="' . $document_uri . '?width=700&height=500&resizeMode=INNER">';
                $multimedia_tab_content .= '<source src="' . $document_uri . '" type="video/mp4" />';
                $multimedia_tab_content .= '</video>';

                $multimedia_tab_content .= '<p class="caption"><a title="' . $document_name_full . '" href="#">' . $document_name . '</a></p>';
                $multimedia_tab_content .= '</div><!-- /.video-content --></div><!-- /.video-wrapper --></div><!-- /.thumbnail --></div>';
            }
			
			if( $count%3==0 ) {
					$multimedia_tab_content .= '<div class="clearfix visible-sm-block"></div>';
					$multimedia_tab_content .= '<div class="clearfix visible-md-block"></div>';
					$multimedia_tab_content .= '<div class="clearfix visible-lg-block"></div>';
				}
				
            $count = $count + 1;
			
        }
		
        foreach($suite_preview_images_query2 as $galleryimages) {
            $imagelink = $galleryimages['uri'];



            if (strlen($galleryimages['file_name']) > 30) {
                $imagename = substr($galleryimages['file_name'], 0, 30) . '...';
            } else {
                $imagename = $galleryimages['file_name'];
            }


            $gallerythumbnail = str_ireplace('.' . $galleryimages['extension'], '', $galleryimages['file_name']) . "_thumb." . $galleryimages['extension'];

            $multimedia_tab_content .= "<div class=\"col-sm-4 \">
						 <div class=\"thumbnail thumbnail-image\">
						 <a href=\"images/suite_preview_images/" . $galleryimages['file_name'] . "\" class=\"fancybox fancybox.image suiteMultimediaGalleryClick\" data-fancybox-group=\"suiteimages\">
						 <img src=\"images/suite_preview_images/" . $gallerythumbnail . "\"></a>
						 <p class=\"caption\">
						 <a href=\"images/suite_preview_images/" . $galleryimages['file_name'] . "\" class=\"fancybox fancybox.image suiteMultimediaGalleryClick\" data-fancybox-group=\"suiteimages\">" . $galleryimages['display_file_name'] . "
						 </a>
						</div><!-- /.thumbnail -->
                           </div>";
       
	   if( $count%3==0 ) {
					$multimedia_tab_content .= '<div class="clearfix visible-sm-block"></div>';
					$multimedia_tab_content .= '<div class="clearfix visible-md-block"></div>';
					$multimedia_tab_content .= '<div class="clearfix visible-lg-block"></div>';
				}
				
			$count = $count + 1;
	    }
		
		

        $multimedia_tab_content .= '
				</div>';
		


        return $multimedia_tab_content;
	}

    
    
    
    
    public function shareThisHover($theme) {
        $HTML = "";
        if ($theme['sharethis_hover'] == 1) {
            $HTML .='<script type="text/javascript">stLight.options({publisher:"' . $theme['sharethis_account'] . '", doNotHash: true, doNotCopy: false, hashAddressBar: false});</script>';
        } else {
            $HTML .='<script type="text/javascript">stLight.options({publisher:"' . $theme['sharethis_account'] . '", doNotHash: true, doNotCopy: false, onhover: false, hashAddressBar: false});</script>';
        }
        return $HTML;
    }


    public function breadCrumbsHTML($language, $lang, $province, $region, $subregion, $suitetype_selected, $show_no_vacancy, $show_searchform){
 
        $search_text = $language['search_text'];

        if ($show_searchform == 0) {
            if ($language['lang_id'] == '0'){
                $search_text = "Accueil";
            }
            $search_text = "Home";
        }
        
        
        
                 //back to search
                 $HTML = '<a href="'.$language['vacancy_report_website'].'/index.php?lang='.$lang.'">'.$search_text.'</a>';
                 //back to region
                 if ($province!='All' && $province!=''){
                     //back to search province level
                     $HTML .= ' > <a href="'.$language['vacancy_report_website'].'/index.php?province='.$province.'&SuiteType='.$suitetype_selected.'&Size=All&Availability=All&show_no_vacancy='.$show_no_vacancy.'&lang='.$lang.'&Submit=SEARCH">'.$province.'</a>';
                     if ($region!='All' && $region!=''){
                       //back to search region level
                        $HTML .= ' > <a href="'.$language['vacancy_report_website'].'/index.php?province='.$province.'&region='.$region.'&SuiteType='.$suitetype_selected.'&Size=All&Availability=All&show_no_vacancy='.$show_no_vacancy.'&lang='.$lang.'&Submit=SEARCH">'.$region.'</a>';  
                        if ($subregion!='All' && $subregion!=''){
                        //back to search region level
                        $HTML .= ' > <a href="'.$language['vacancy_report_website'].'/index.php?province='.$province.'&region='.$region.'&sub-region='.$subregion.'&SuiteType='.$suitetype_selected.'&Size=All&Availability=All&show_no_vacancy='.$show_no_vacancy.'&lang='.$lang.'&Submit=SEARCH">'.$subregion.'</a>';  
                        }          
                     }
                 }
                 
                 return $HTML;   
    }


    
    

    public function industrialAdvancedOfficeSizeSearch($language, $queries, $lang_id, $size_selected) {
        $HTML = "";
        $sizes_query = mysql_query("select distinct size, size_display from office_space where lang=" . $lang_id . " order by size_index asc");
        $sizes_option_query = mysql_query("select distinct size from office_space where lang= 1 order by size_index asc");
        while ($size_row = mysql_fetch_array($sizes_query)) {
            $size_row_options = mysql_fetch_array($sizes_option_query);
            if ($size_row_options['size'] == $size_selected) {
                $HTML .= '<option selected="yes" value="' . $size_row_options['size'] . '">' . $size_row['size_display'] . '</option>' . "\r\n";
            } else {
                $HTML .= '<option value="' . $size_row_options['size'] . '">' . $size_row['size_display'] . '</option>' . "\r\n";
            }
        }
        return $HTML;
    }    
    

    public function industrialAdvancedClearHeightSearch($language, $queries, $lang_id, $size_selected) {
        $HTML = "";
        $sizes_query = mysql_query("select distinct size, size_display from clear_height_ranges where lang=" . $lang_id . " order by size_index asc");
        $sizes_option_query = mysql_query("select distinct size from clear_height_ranges where lang= 1 order by size_index asc");
        while ($size_row = mysql_fetch_array($sizes_query)) {
            $size_row_options = mysql_fetch_array($sizes_option_query);
            if ($size_row_options['size'] == $size_selected) {
                $HTML .= '<option selected="yes" value="' . $size_row_options['size'] . '">' . $size_row['size_display'] . '</option>' . "\r\n";
            } else {
                $HTML .= '<option value="' . $size_row_options['size'] . '">' . $size_row['size_display'] . '</option>' . "\r\n";
            }
        }
        return $HTML;
    }    
    
    
    

    public function industrialBaySizeWidthSearch($language, $queries, $lang_id, $size_selected) {
        $HTML = "";
        $sizes_query = mysql_query("select distinct size, size_display from bay_width_ranges where lang=" . $lang_id . " order by size_index asc");
        $sizes_option_query = mysql_query("select distinct size from bay_width_ranges where lang= 1 order by size_index asc");
        while ($size_row = mysql_fetch_array($sizes_query)) {
            $size_row_options = mysql_fetch_array($sizes_option_query);
            if ($size_row_options['size'] == $size_selected) {
                $HTML .= '<option selected="yes" value="' . $size_row_options['size'] . '">' . $size_row['size_display'] . '</option>' . "\r\n";
            } else {
                $HTML .= '<option value="' . $size_row_options['size'] . '">' . $size_row['size_display'] . '</option>' . "\r\n";
            }
        }
        return $HTML;
    }    
    
    
    

    public function industrialBaySizeDepthSearch($language, $queries, $lang_id, $size_selected) {
        $HTML = "";
        $sizes_query = mysql_query("select distinct size, size_display from bay_depth_ranges where lang=" . $lang_id . " order by size_index asc");
        $sizes_option_query = mysql_query("select distinct size from bay_depth_ranges where lang= 1 order by size_index asc");
        while ($size_row = mysql_fetch_array($sizes_query)) {
            $size_row_options = mysql_fetch_array($sizes_option_query);
            if ($size_row_options['size'] == $size_selected) {
                $HTML .= '<option selected="yes" value="' . $size_row_options['size'] . '">' . $size_row['size_display'] . '</option>' . "\r\n";
            } else {
                $HTML .= '<option value="' . $size_row_options['size'] . '">' . $size_row['size_display'] . '</option>' . "\r\n";
            }
        }
        return $HTML;
    }    
    
    
    

    public function industrialAdvancedWarehouseSizeSearch($language, $queries, $lang_id, $size_selected) {
        $HTML = "";
        $sizes_query = mysql_query("select distinct size, size_display from warehouse_space where lang=" . $lang_id . " order by size_index asc");
        $sizes_option_query = mysql_query("select distinct size from warehouse_space where lang= 1 order by size_index asc");
        while ($size_row = mysql_fetch_array($sizes_query)) {
            $size_row_options = mysql_fetch_array($sizes_option_query);
            if ($size_row_options['size'] == $size_selected) {
                $HTML .= '<option selected="yes" value="' . $size_row_options['size'] . '">' . $size_row['size_display'] . '</option>' . "\r\n";
            } else {
                $HTML .= '<option value="' . $size_row_options['size'] . '">' . $size_row['size_display'] . '</option>' . "\r\n";
            }
        }
        return $HTML;
    }        
    
    
    

    public function industrialAdvancedElectricalVoltsSearch($language, $queries, $lang_id, $size_selected) {
        $HTML = "";
        $sizes_query = mysql_query("select distinct size, size_display from electrical_volts where lang=" . $lang_id . " order by size_index asc");
        $sizes_option_query = mysql_query("select distinct size from electrical_volts where lang= 1 order by size_index asc");
        while ($size_row = mysql_fetch_array($sizes_query)) {
            $size_row_options = mysql_fetch_array($sizes_option_query);
            if ($size_row_options['size'] == $size_selected) {
                $HTML .= '<option selected="yes" value="' . $size_row_options['size'] . '">' . $size_row['size_display'] . '</option>' . "\r\n";
            } else {
                $HTML .= '<option value="' . $size_row_options['size'] . '">' . $size_row['size_display'] . '</option>' . "\r\n";
            }
        }
        return $HTML;
    }      
    

    public function industrialAdvancedElectricalAmpsSearch($language, $queries, $lang_id, $size_selected) {
        $HTML = "";
        $sizes_query = mysql_query("select distinct size, size_display from electrical_amps where lang=" . $lang_id . " order by size_index asc");
        $sizes_option_query = mysql_query("select distinct size from electrical_amps where lang= 1 order by size_index asc");
        while ($size_row = mysql_fetch_array($sizes_query)) {
            $size_row_options = mysql_fetch_array($sizes_option_query);
            if ($size_row_options['size'] == $size_selected) {
                $HTML .= '<option selected="yes" value="' . $size_row_options['size'] . '">' . $size_row['size_display'] . '</option>' . "\r\n";
            } else {
                $HTML .= '<option value="' . $size_row_options['size'] . '">' . $size_row['size_display'] . '</option>' . "\r\n";
            }
        }
        return $HTML;
    }         
    
    
    

    public function industrialAdvancedShippingDoorsSearch($language, $queries, $lang_id, $size_selected) {
        $HTML = "";
        $sizes_query = mysql_query("select distinct size, size_display from num_shipping_doors where lang=" . $lang_id . " order by size_index asc");
        $sizes_option_query = mysql_query("select distinct size from num_shipping_doors where lang= 1 order by size_index asc");
        while ($size_row = mysql_fetch_array($sizes_query)) {
            $size_row_options = mysql_fetch_array($sizes_option_query);
            if ($size_row_options['size'] == $size_selected) {
                $HTML .= '<option selected="yes" value="' . $size_row_options['size'] . '">' . $size_row['size_display'] . '</option>' . "\r\n";
            } else {
                $HTML .= '<option value="' . $size_row_options['size'] . '">' . $size_row['size_display'] . '</option>' . "\r\n";
            }
        }
        return $HTML;
    }      
    
    
    
    
    

    public function industrialAdvancedDriveInDoorsSearch($language, $queries, $lang_id, $size_selected) {
        $HTML = "";
        $sizes_query = mysql_query("select distinct size, size_display from num_drivein_doors where lang=" . $lang_id . " order by size_index asc");
        $sizes_option_query = mysql_query("select distinct size from num_drivein_doors where lang= 1 order by size_index asc");
        while ($size_row = mysql_fetch_array($sizes_query)) {
            $size_row_options = mysql_fetch_array($sizes_option_query);
            if ($size_row_options['size'] == $size_selected) {
                $HTML .= '<option selected="yes" value="' . $size_row_options['size'] . '">' . $size_row['size_display'] . '</option>' . "\r\n";
            } else {
                $HTML .= '<option value="' . $size_row_options['size'] . '">' . $size_row['size_display'] . '</option>' . "\r\n";
            }
        }
        return $HTML;
    }     
    
    
 

    public function industrialAdvancedParkingSearch($language, $queries, $lang_id, $size_selected) {
        $HTML = "";
        $sizes_query = mysql_query("select distinct size, size_display from num_parking where lang=" . $lang_id . " order by size_index asc");
        $sizes_option_query = mysql_query("select distinct size from num_parking where lang= 1 order by size_index asc");
        while ($size_row = mysql_fetch_array($sizes_query)) {
            $size_row_options = mysql_fetch_array($sizes_option_query);
            if ($size_row_options['size'] == $size_selected) {
                $HTML .= '<option selected="yes" value="' . $size_row_options['size'] . '">' . $size_row['size_display'] . '</option>' . "\r\n";
            } else {
                $HTML .= '<option value="' . $size_row_options['size'] . '">' . $size_row['size_display'] . '</option>' . "\r\n";
            }
        }
        return $HTML;
    }     
        
 

    public function industrialAdvancedSurfaceStallsPer1000Search($language, $queries, $lang_id, $size_selected) {
        $HTML = "";
        $sizes_query = mysql_query("select distinct size, size_display from surface_stalls_per1000_ranges where lang=" . $lang_id . " order by size_index asc");
        $sizes_option_query = mysql_query("select distinct size from surface_stalls_per1000_ranges where lang= 1 order by size_index asc");
        while ($size_row = mysql_fetch_array($sizes_query)) {
            $size_row_options = mysql_fetch_array($sizes_option_query);
            if ($size_row_options['size'] == $size_selected) {
                $HTML .= '<option selected="yes" value="' . $size_row_options['size'] . '">' . $size_row['size_display'] . '</option>' . "\r\n";
            } else {
                $HTML .= '<option value="' . $size_row_options['size'] . '">' . $size_row['size_display'] . '</option>' . "\r\n";
            }
        }
        return $HTML;
    } 
    
    
    

    public function industrialAdvancedSurfaceStallsSearch($language, $queries, $lang_id, $size_selected) {
        $HTML = "";
        $sizes_query = mysql_query("select distinct size, size_display from surface_stalls_all where lang=" . $lang_id . " order by size_index asc");
        $sizes_option_query = mysql_query("select distinct size from surface_stalls_all where lang= 1 order by size_index asc");
        while ($size_row = mysql_fetch_array($sizes_query)) {
            $size_row_options = mysql_fetch_array($sizes_option_query);
            if ($size_row_options['size'] == $size_selected) {
                $HTML .= '<option selected="yes" value="' . $size_row_options['size'] . '">' . $size_row['size_display'] . '</option>' . "\r\n";
            } else {
                $HTML .= '<option value="' . $size_row_options['size'] . '">' . $size_row['size_display'] . '</option>' . "\r\n";
            }
        }
        return $HTML;
    } 

    public function industrialAdvancedTruckTrailerParkingSearch($language, $queries, $lang_id, $size_selected) {
        $HTML = "";
        $sizes_query = mysql_query("select distinct size, size_display from num_trucktrailer_parking where lang=" . $lang_id . " order by size_index asc");
        $sizes_option_query = mysql_query("select distinct size from num_trucktrailer_parking where lang= 1 order by size_index asc");
        while ($size_row = mysql_fetch_array($sizes_query)) {
            $size_row_options = mysql_fetch_array($sizes_option_query);
            if ($size_row_options['size'] == $size_selected) {
                $HTML .= '<option selected="yes" value="' . $size_row_options['size'] . '">' . $size_row['size_display'] . '</option>' . "\r\n";
            } else {
                $HTML .= '<option value="' . $size_row_options['size'] . '">' . $size_row['size_display'] . '</option>' . "\r\n";
            }
        }
        return $HTML;
    }      
    

    
    
    
    
    
    
    public function zipBrokerPackage($lang_id, $building_id, $building_code, $packagetype, $suite_id) {
        include('Queries.php');
        $queries               = new Queries();
        
//        $preview_images_query, $pdf_query, $pdf_asbuilt_query
        
        $preview_images_query = $queries->preview_images_query($building_code, $lang_id);
        if ($suite_id != '') {
            $suite_preview_images_query = $queries->suite_preview_images_query($suite_id, $building_code, $lang_id);
        }
        $building_thumbnail_query_string = $queries->get_building_thumbnail_query_string($lang_id, $building_code);
        $building_thumbnail_query = mysql_query($building_thumbnail_query_string) or die ("Building thumb query error: " . mysql_error());
        $pdf_query = $queries->file_extension_query($building_code,"pdf",$lang_id);
        if ($packagetype == "Building") {
            $pdf_asbuilt_query = $queries->pdf_asbuilt_query($building_code, $lang_id);
        }
        if ($packagetype == "Suite"){
            $suite_pdf_query = $queries->pdf_query($building_code, $suite_id, $lang_id);
        }
        
        
        $uniqueID = $building_code . '_' . uniqid("brokerPackage_");
        $path = "images/broker_packages/";
        //create new zip archive
        
        $zip = new ZipArchive;
        $destination = $path . $uniqueID . '.zip';
        $zzz = $zip->open($destination, ZipArchive::CREATE);
        if($zzz === true) {
            if($zip->addEmptyDir('Images')) {
                
                // loop through preview images and insert into the Images directory
                if ($zip->open($destination) === TRUE) {
                    
                    
                    while ($previewimages = mysql_fetch_array($preview_images_query)) {
                        $filepath = "images/preview_images/";
                        $filename = $previewimages['file_name'];
                        $zip->addFile($filepath . $filename , 'Images/'.$filename);
                    }
//                }
                if ($packagetype == "Suite"){
                    while ($suitepreviewimages = mysql_fetch_array($suite_preview_images_query)) {
                        $filepath = "images/suite_preview_images/";
                        $filename = $suitepreviewimages['file_name'];
                        $zip->addFile($filepath . $filename , 'Images/'.$filename);
                    }
                }    
                // Same for the building thumbnail
                    while ($buildingthumb = mysql_fetch_array($building_thumbnail_query)) {
                        $filepath = "images/preview_images/";
                        $filename = $buildingthumb['file_name'];
                        $zip->addFile($filepath . $filename , 'Images/'.$filename);
                    }
                } else {
                        
//echo $filename . 'failed';
                }
                
                
                
            } else {
//echo 'Could not create the Images directory';
            }
            if($zip->addEmptyDir('AsBuiltFiles')) {
                
                // loop through asbuilt files and insert into the AsBuiltFiles directory
                if ($zip->open($destination) === TRUE) {
                    if ($packagetype == "Building") {
                        while ($asbuiltfiles = mysql_fetch_array($pdf_asbuilt_query)) {
                            $filepath = "images/twodee_files/";
                            $filename = $asbuiltfiles['plan_name'];
                            $zip->addFile($filepath . $filename , 'AsBuiltFiles/'.$filename);
                        }
                    
                    } else { // $packagetype == "Suite" //
//                        while ($asbuiltfiles = mysql_fetch_array($suite_pdf_query)) {
                            foreach ($suite_pdf_query as $asbuiltfiles) {
                            $filepath = "images/twodee_files/";
                            $filename = $asbuiltfiles['plan_name'];
                            $zip->addFile($filepath . $filename , 'AsBuiltFiles/'.$filename);
                        }

                    }
                    
                    
                    

                } else {
                        
                }
                
            } else {
            }
            
            if($zip->addEmptyDir('Marketing')) {
                
                // loop through pdf files and insert into the Marketing directory
                if ($zip->open($destination) === TRUE) {
                    while ($marketingfiles = mysql_fetch_array($pdf_query)) {
                        $filepath = "images/documents/";
                        $filename = $marketingfiles['file_name'];
                        
                        $zip->addFile($filepath . $filename , 'Marketing/'.$filename);
                        
                    }    
                } else {
                        
                }
                
            } else {
            }

            if($zip->addEmptyDir('Location')) {

                if ($zip->open($destination) === TRUE) {

                        $filepath = "images/twodee_files/";
                        $smallmap = $building_id."_zoom11.png";
                        $largemap = $building_id."_zoom13.png";
                        
                        $zip->addFile($filepath . $smallmap , 'Location/mapfar.png');
                        $zip->addFile($filepath . $largemap , 'Location/mapclose.png');
                        

                } else {
                        
                }
                
            } else {
            }
            $zip->close();
        } else {
        }


return $uniqueID.".zip";
    } // end zipBrokerPackage
    


}

?>
