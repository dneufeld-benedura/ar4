<?php
date_default_timezone_set('America/New_York');

include('../theme/db.php');
include('../objects/SearchData.php');
include_once('../objects/LanguageQuery.php');
include('Queries.php'); //THIS IS NOT THE SAME AS IN INDEX.PHP!
include('CreateHTML.php');
include('Drivers.php');

/**
 * This class is the main class for sending the e-mail alerts.
 * @author RationalRoot
 * @version AR2.0
 * @since December 2014
 */
class Mainscript {

//variables that are used across the methods are to be declared here
//these variables are to be used with self::$variableName
//private static declaration means for this class only AND whatever concerns this class only

    private static $queries;
    private static $searchData;
    private static $language;
    private static $lang;
    private static $lang_id;
    private static $squarefoot;
    private static $timestamp;
    private static $createHTML;
    private static $drivers;
    private static $multiple_emails; //the collection of e-mails
    private static $clientname;
    private static $uniqueid;

    /**
     * This is a constructor method for initiating/constructing the data that is essential
     * for the functioning of most of the methods.
     */
    public final function __construct() {
        self::$timestamp = time();
        self::$searchData = new SearchData();

        $languageQuery = new LanguageQuery();
        $langArray = $languageQuery->getLanguageAndID();
        self::$lang = $langArray[0];
        self::$lang_id = $langArray[1];

        self::$squarefoot = "sf";
        if (self::$lang_id == '0') {
            self::$squarefoot = "";
        }
        self::$queries = new Queries(self::$lang_id, self::$timestamp); //These queries are not from the objects folder that, for example, index.php 
        $language_query = self::$queries->getLanguageQuery();
        self::$language = mysql_fetch_array($language_query);
        self::$createHTML = new CreateHTML(self::$language, self::$lang, self::$lang_id, self::$squarefoot, self::$queries);
        //setting locale for time, date, money, etc
        
        setlocale(LC_ALL, self::$language['locale_string']);

        self::$drivers = new Drivers(self::$language, self::$lang, self::$queries, self::$createHTML);

        self::$multiple_emails = array();
        if (isset($_REQUEST['clientname'])){self::$clientname = $_REQUEST['clientname'];}
    }

 
 
    /**
     * This class is used to send a generated e-mail.
     * @param type $EmailMessage
     */
    private function sendMessage($EmailMessage) {
        mail($EmailMessage['to_email'], $EmailMessage['subject'], $EmailMessage['message'], $EmailMessage['headers'], '-f updates@' . $EmailMessage['emaildomain'] . '');
    }

    /**
     * This function sends the joined e-mail messages
     * @param type $to_email
     */
    public function sendMultipleMessages($to_email) {
        $email_link_url = '';
        if (self::$language['bu_email_link_url'] != '') {
            $email_link_url = self::$language['bu_email_link_url'];
        } else {
            $email_link_url = self::$language['vacancy_report_website'];
        }
        
        self::displayMultipleMessages();
        if (count(self::$multiple_emails) > 0) {
            $EmailMessage = array();
            $EmailMessage['to_email'] = $to_email;
            $EmailMessage['emaildomain'] = str_replace('http://', '', $email_link_url); //renamed self::$clientname to '', we can add it if necessary
            $EmailMessage['headers'] = self::$createHTML->getHeaders('', $EmailMessage['emaildomain']);
            $EmailMessage['subject'] = $EmailMessage['emaildomain'] . ' ' . self::$language['email_subject_text'] . ' ' . date('Y-m-d');
            $EmailMessage['message'] = self::$createHTML->generateMessage(self::$multiple_emails, self::$uniqueid);
 
            $this->sendMessage($EmailMessage);
        }
    }

    /**
     * This is the main class for gathering the data and sending it, for the joined subscriptions.
     */
    public function multiple_main($dataArray) {

        //First, get all the data
        $selectedData = $this->getSelectedData_multiple($dataArray);  //this is the array of all data mentioned above. //
        //get all the queries
        $buildings_query = self::$queries->getBuildingsQuery($selectedData);


        //get all the relevant query data    
        $initial_suites_count = mysql_num_rows($buildings_query);
        

        //Initiate the classes responsible for creating the output
    
        $ifSuitesCountArr = self::$drivers->buildDriversAndOutput($buildings_query, $initial_suites_count, $selectedData['uniqueid']);


        $message = $ifSuitesCountArr['output'];
        if (!is_null($message) && $message!=''){
        //add the tabs with a link to the subscription to the $message
        $message = self::$createHTML->addTab($selectedData['uniqueid'], $message, self::$language);    
        array_push(self::$multiple_emails, $message);
        }
    }

 
    
    /**
     * This returns all the data according to user's settings.
     * @return type
     */
    private function getSelectedData_multiple($dataArray) {
        $data = array();
        if (isset($data['clientname'])){
        $data['clientname'] = $dataArray['bu_name'];
        self::$clientname = $dataArray['bu_name'];}
        else{$data['clientname'] ='';  self::$clientname =''; }
        $data['bu_email'] = $dataArray['email'];
        $data['province_selected'] = $dataArray['province'];
        $data['SuiteType_selected'] = $dataArray['SuiteType'];
        $data['region_selected'] = $dataArray['region'];
        $data['sub_region_selected'] = $dataArray['sub-region'];
        $data['size_selected'] = $dataArray['Size'];
        $data['uniqueid'] = $dataArray['unique'];
        self::$uniqueid = $dataArray['unique']; //echo self::$uniqueid."<br>";
        $data['requests'] = $data; //is this one necessary ?? 

        $availability_selected_array = self::$searchData->get_availability_selected_str($dataArray['Availability']);
        $availability_notafter = $availability_selected_array['availability_notafter'];
        $availability_notbefore = $availability_selected_array['availability_notbefore'];


        $data['availability_selected_array'] = $availability_selected_array;
        $data['availability_notafter'] = $availability_notafter;
        $data['availability_notbefore'] = $availability_notbefore;
        return $data;
    }





    /**
     * Mainly used for testing purposes, to display the joined messages that are sent.
     */
    private static function displayMultipleMessages() {
        echo self::$createHTML->getHeaders(self::$clientname, self::$uniqueid);
        $data = self::$createHTML->generateMessage(self::$multiple_emails, self::$clientname, self::$uniqueid);
        $error='-webkit-text-stroke-widt</td>';
        $correction='-webkit-text-stroke-width: 0px; background-color: #ffffff; display: inline !important; float: none;"></span></td>';
        $data = str_replace($error, $correction, $data);
        
    }

}
