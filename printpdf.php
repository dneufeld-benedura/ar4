<?php

$lang    = "en_CA";
$lang_id = 1;
if (isset($_REQUEST['lang'])) {
    if ($_REQUEST['lang'] == "fr_CA") {
        
        $lang    = "fr_CA";
        $lang_id = 0;
    } else {
        
        
    }
} else {
    
}

include('theme/db.php');

// Language Query

$language_query_string = 'select * from languages_dynamic ld, languages l where ld.lang_id = ' . $lang_id . ' and l.lang_id = ' . $lang_id;

$language_query = mysql_query($language_query_string) or die("language query error: " . mysql_error());
$language = mysql_fetch_array($language_query);

//setting locale for time, date, money, etc
setlocale(LC_ALL, $language['locale_string']);
date_default_timezone_set('America/New_York');




require('HTTPRequestString.php');
function get_query_str()
{
    if ($_SERVER['QUERY_STRING'] == "") {
        return "";
    } else {
        return '&' . $_SERVER['QUERY_STRING'];
    }
}

function get_query_str_print()
{
    if ($_SERVER['QUERY_STRING'] == "") {
        return "";
    } else {
        return $_SERVER['QUERY_STRING'];
    }
}

if (isset($_REQUEST['province'])) {
    $descriptor = $_REQUEST['province'];
} else {
    if (isset($_REQUEST['BuildingType'])) {
        $descriptor = $_REQUEST['BuildingType'];
    } else {
        $descriptor = "";
    }
}

$date_string = date('Y-m-d');

//check query string for pdf config variable
$pdf_config_selection = $_GET['config'];

//if query string has pdf config variable, which configuration is it?
if ($pdf_config_selection) {
    if ($pdf_config_selection == 1) { //condensed view

        
        $url              = $language['vacancy_report_website'] . '/eclipse/pdfs/pdf_vacancies_condensed.php?' . get_query_str_print();
        $filename_for_pdf = $language['pdf_vacancy_filename_condensed_text'] . $descriptor . '-' . $date_string . ".pdf";
        
        
    } else { //properties
        
    }
}
//no config variable found in query string, use default pdf_vacancies page
else {
    
    $url              = $language['vacancy_report_website'] . '/eclipse/pdfs/pdf_vacancies.php?' . get_query_str_print();
    $filename_for_pdf = $language['pdf_vacancy_filename_text'] . $descriptor . '-' . $date_string . ".pdf";
}



$r               = new HTTPRequestString($url);
$string_to_parse = $r->DownloadToString();

require_once("dompdf/dompdf_config.inc.php");
ob_start();
echo $string_to_parse;
$layoutHTML = ob_get_contents();
ob_end_clean();


$dompdf = new DOMPDF();

$dompdf->set_base_path("/var/www/html/");


$dompdf->load_html($layoutHTML);
$dompdf->render();
$dompdf->stream($filename_for_pdf);

?>
