<?php
/*
 * This class is used to generate the message that is to be sent to a subscriber.
 * @author RationalRoot
 * @version AR2.0
 * @since December 2014
 */

include('../../theme/db.php');
include('EmailMessage.php');
include_once('../../objects/LanguageQuery.php');
include('../Mainscript/Queries.php'); //THIS IS NOT THE SAME AS IN INDEX.PHP!

$cm = new confirmationMain();
$cm->main();

class confirmationMain {

    private static $province;
    private static $region;
    private static $sub_region;
    private static $suiteType;
    private static $size;
    private static $availability;
    private static $bu_name;
    private static $lang;
    private static $email;
    private static $timestamp;
    private static $lang_id;
    private static $queries;
    private static $language;
    private static $emaildomain;
    private static $title;
    private static $uniqueid;

    /**
     * A constructor method.
     */
    public function __construct() {

        self::$bu_name = $_REQUEST['bu_name'];
        self::$lang = $_REQUEST['lang'];
        self::translateTitleData();
        self::$email = $_REQUEST['email'];
        self::$timestamp = time();
        self::$lang_id = 0;
        if (self::$lang == 'en_CA') {
            self::$lang_id = 1;
        }
        self::$queries = new Queries(self::$lang_id, self::$timestamp); //These queries are not from the objects folder that, for example, index.php 
        $language_query = self::$queries->getLanguageQuery();
        self::$language = mysql_fetch_array($language_query);
        self::$emaildomain = str_replace('http://', '', self::$language['vacancy_report_website']);
        self::$title = self::buildReportTitle();
        self::$uniqueid = uniqid('bu_');
        
    }

    
    
    
    private static function getProvinceRegionSubRegionTranslated(){
        
    $queryString = "Select building_id from buildings where province = '".self::$province."' and region ='".self::$region."' and sub_region='".self::$sub_region."'";
    $queryString = "Select * from buildings where building_id=(".$queryString.") and lang='0'"; 
    $queryString = str_replace("Tous", "", $queryString);
    
    $query = mysql_query($queryString);
    $data = mysql_fetch_array($query);
    
    if ($data['province'] != '' and ! is_null($data['province'])) {
            self::$province = $data['province'];
        }

    if ($data['region'] != '' and ! is_null($data['region'])) {
            self::$region = $data['region'];
        }
        
    if ($data['sub_region'] != '' and ! is_null($data['sub_region'])) {
            self::$sub_region = $data['sub_region'];
    }    
    
    }
    
    
    
    private static function translateTitleData(){
        
        
 

        self::$province = $_REQUEST['province'];
        if (self::$province == 'All' && self::$lang == 'fr_CA'){
        self::$province = 'Tous';    
        }
        
        self::$region = $_REQUEST['region'];
        if (self::$region == 'All' && self::$lang == 'fr_CA'){
        self::$region = 'Tous';    
        }
        
        self::$sub_region = $_REQUEST['sub-region'];
        if (self::$sub_region== 'All' && self::$lang == 'fr_CA'){
        self::$sub_region = 'Tous';    
        }
        
        
        
        
        
        self::$suiteType = $_REQUEST['SuiteType'];
        if (self::$suiteType== 'All' && self::$lang == 'fr_CA'){ self::$suiteType = 'Tous'; }
        else if(self::$suiteType== 'OFFICE' && self::$lang == 'fr_CA'){self::$suiteType='BUREAU';}
        else if(self::$suiteType== 'INDUSTRIAL' && self::$lang == 'fr_CA'){self::$suiteType='INDUSTRIEL';}
        else if(self::$suiteType== 'RETAIL' && self::$lang == 'fr_CA'){self::$suiteType='DÉTAIL';}
        
        self::$size = $_REQUEST['Size'];
        if (self::$size== 'All' && self::$lang == 'fr_CA'){self::$size = 'Tous';}
        else if(self::$size== '5,000 and Under' && self::$lang == 'fr_CA'){self::$size='5,000 et moins';}
        else if(self::$size== '40,000 and Above' && self::$lang == 'fr_CA'){self::$size='40,000 et plus';}
        
        
        
        self::$availability = $_REQUEST['Availability'];
        if (self::$availability== 'All' && self::$lang == 'fr_CA'){self::$availability = 'Tous';}
        else if(self::$availability== 'Immediately' && self::$lang == 'fr_CA'){self::$availability='Immédiatement';}
        else if(self::$availability== 'Under 3 Months' && self::$lang == 'fr_CA'){self::$availability='Moins de 3 mois';}
        else if(self::$availability== '3-6 Months' && self::$lang =='fr_CA'){self::$availability='3-6 mois';}
        else if(self::$availability== '6-12 Months' && self::$lang == 'fr_CA'){self::$availability='6-12 mois';}
        else if(self::$availability== 'Over 12 Months' && self::$lang == 'fr_CA'){self::$availability='Plus de 12 mois';}
        
        if (self::$lang == 'fr_CA'){
        self::getProvinceRegionSubRegionTranslated(); 
       }   
        
    }
    
    
    
    
    /**
     * The main method that generates the key, expiry time and message that is sent to recipient's e-mail address.
     */
    public function main() {
        $this->addRegistration();
        $emailMessage = new EmailMessage();
        

        $data = self::getDataArray(); 
        echo $emailMessage->getSentConfirmationMessage(self::$uniqueid,$data,self::$language);
 
    }

    private static function getDataArray(){        
        $data = array();
        $data['province']=self::$province;
        $data['region']=self::$region;
        $data['sub-region']=self::$sub_region;
        $data['SuiteType']=self::$suiteType;
        $data['Size']=self::$size;
        $data['Availability']=self::$availability;
        $data['bu_name']=self::$bu_name;
        $data['lang']=self::$lang;
        $data['email']=self::$email;
        return $data;
    }
 

    

 
    
    

    /**
     * Adds registration to client's database.
     * @param type $key
     * @param type $expireOn
     */
    private function addRegistration() {
        
        if (is_null(self::$title) && self::$lang_id=='1'){self::$title="All Listings";}
        else if (is_null(self::$title) && self::$lang_id=='0'){self::$title="Toutes les inscriptions";}
        
        
        if (!is_null(self::$title) && !is_null(self::$bu_name) && !is_null(self::$email)){
            
         //check if it is only an inactive user, and if it is, just activate it. Else, add it.   
         if ($this->isInactive()){
             $this->activateSubscription();
         }
         else{
             $this->addNewSubscription();
         }
        }
    }
    
    
    private function activateSubscription(){
    $query_string = "UPDATE bu_subscriptions SET active='1' WHERE title = '".self::$title."' AND bu_name = '".self::$bu_name."' AND email = '".self::$email."' and active = '0' LIMIT 1;";
    self::executeQuery($query_string);
    }
    
    private function addNewSubscription() {

        $brokerupdates_signup_query = "insert into bu_subscriptions (title, bu_name, email, bu_query_string, unique_id, active, subscription_language, clientname) VALUES ('"
                . mysql_real_escape_string(self::$title) . "','" . mysql_real_escape_string(self::$bu_name) . "','" . mysql_real_escape_string(self::$email) . "','"
                . mysql_real_escape_string($this->get_query_str_print()) . "','" . mysql_real_escape_string(self::$uniqueid) . "', '1', '" . mysql_real_escape_string(self::$lang) . "', '" . mysql_real_escape_string(self::$language['clientname']) . "')";


        self::executeQuery($brokerupdates_signup_query);
    }

    

    
    
    private function isInactive(){
    $inactive_query = "SELECT * FROM bu_subscriptions where title = '".self::$title."' AND bu_name = '".self::$bu_name."' AND email = '".self::$email."'  AND active = '0';";
    $query = mysql_query($inactive_query)or die("Confirm isInactive user error: " . mysql_error());
    $countSubscriptions = mysql_num_rows($query);
    if ($countSubscriptions>0){return true;}
    return false;
    }
    

    /**
     * Executes the query for adding a temporary registration
     * @param type $query
     */
    private static function executeQuery($query) {
        mysql_query($query) or die("Error at adding a temporary registration, confirmationMain Class executeQuery method: " . mysql_error());
    }

    /**
     * Gets query srting print from $_SERVER['QUERY_STRING']
     * @return string
     */
    private function get_query_str_print() {
        if ($_SERVER['QUERY_STRING'] == "") {
            return "";
        } else {
            return $_SERVER['QUERY_STRING'];
        }
    }

    /**
     * Creates and returns the subscription title
     * @return string
     */
    private static function buildReportTitle() {
        $title = '';
        if (self::$province != 'All' && self::$province != 'Tous') {
            $title .= self::$province;
        } else {
            $title .= "All";
        }
        if (self::$region != 'All' && self::$province != 'Tous') {
            $title .= ' | ' . self::$region;
        }
        if (self::$sub_region != 'All' && self::$province != 'Tous') {
            $title .= ' | ' . self::$sub_region;
        }
        if (self::$suiteType != 'All' && self::$province != 'Tous') {
            $title .= ' | ' . self::$suiteType;
        }
        if (self::$size != 'All' && self::$province != 'Tous') {
            $title .= ' | ' . self::$size;
        }
        if (self::$availability != 'All' && self::$province != 'Tous') {
            $title .= ' | ' . self::$availability;
        }


        $title = str_replace("| Tous", "", $title);
        $title = str_replace("| All", "", $title);
        if ((self::$province == 'All') && (self::$region == 'All') && (self::$sub_region == 'All') && (self::$suiteType == 'All') && (self::$size == 'All') && (self::$availability == 'All')) {
                $title = self::$language['all_listings'];
        }
        else if ((self::$province == 'Tous') && (self::$region == 'Tous') && (self::$sub_region == 'Tous') && (self::$suiteType == 'Tous') && (self::$size == 'Tous') && (self::$availability == 'Tous')) {
        $title = self::$language['all_listings'];
        }

        return $title;
    }

}

?>
