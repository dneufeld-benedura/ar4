<?php

if (isset($_REQUEST['requesttype']) && ($_REQUEST['requesttype'] == "Building" || $_REQUEST['requesttype'] == "Suite" || $_REQUEST['requesttype'] == "Favourite")) {

    include('theme/db.php');
    include('objects/LanguageQuery.php');


    $languageQuery         = new LanguageQuery();
    $langArray             = $languageQuery->getLanguageAndID();
    $lang                  = $langArray[0];
    $lang_id               = $langArray[1];
    
    $language_query_string = 'select * from languages_dynamic ld, languages l, languages2 l2 where ld.lang_id = ' . $lang_id . ' and l.lang_id = ' . $lang_id . ' and l2.lang_id = ' . $lang_id;
    $language_query = mysql_query($language_query_string) or die("language query error: " . mysql_error());
    $language = mysql_fetch_array($language_query);

    
    date_default_timezone_set('America/New_York');
    $today = date("Y/m/d");
    $todaytimestamp = strtotime($today);
    $exacttime = date("Y/m/d, g:ia");
    
$fs_to_name = isset($_REQUEST['toname']) ? $_REQUEST['toname'] : '';
$fs_to_email = isset($_REQUEST['toemail']) ? $_REQUEST['toemail'] : '';
$fs_clientid = isset($_REQUEST['clientid']) ? $_REQUEST['clientid'] : '';
$fs_address = isset($_REQUEST['paddress']) ? $_REQUEST['paddress'] : '';
$fs_from_name = isset($_REQUEST['fromname']) ? $_REQUEST['fromname'] : '';
$fs_from_email = isset($_REQUEST['fromemail']) ? $_REQUEST['fromemail'] : '';
$fs_comments = isset($_REQUEST['fromcomments']) ? $_REQUEST['fromcomments'] : '';
$fs_propertyid = isset($_REQUEST['propertyid']) ? $_REQUEST['propertyid'] : '';
$fs_urltoshare = isset($_REQUEST['urltoshare']) ? $_REQUEST['urltoshare'] : '';
$fs_request_type = isset($_REQUEST['requesttype']) ? $_REQUEST['requesttype'] : '';
$fs_cc_me = isset($_REQUEST['ccme']) ? $_REQUEST['ccme'] : '';


$building_name = isset($_REQUEST['buildingname']) ? $_REQUEST['buildingname'] : '';
$suite_selected = isset($_REQUEST['suiteselected']) ? $_REQUEST['suiteselected'] : '';
$building_street = isset($_REQUEST['buildingstreet']) ? $_REQUEST['buildingstreet'] : '';
$building_city = isset($_REQUEST['buildingcity']) ? $_REQUEST['buildingcity'] : '';
$building_province = isset($_REQUEST['buildingprovince']) ? $_REQUEST['buildingprovince'] : '';



   $notes = '';
   if (isset($fs_comments) && $fs_comments != '') {
        $notes = $fs_comments;
    }
   
   
      
    $eol = "\r\n";
    $eol2 = "\r\n\r\n";
    $eolhtml = "<br>";
    $eolhtml2 = "<br><br>";

    
   $request_info_body = '';
   


if ($notes === '') {
    if ($fs_request_type == "Building") {    
        $request_info_body .= $language['share_placeholder_text1'] . '<a href="'.$fs_urltoshare.'">' . $building_street . ', ' . $building_city . ', ' . $building_province . '</a>' . $language['share_placeholder_text2'] . $eolhtml2;
    } else if ($fs_request_type == "Suite") { 
        $request_info_body .= $language['share_placeholder_text1'] . '<a href="'.$fs_urltoshare.'">' . $building_street . ', ' . $building_city . ', ' . $building_province . ', Suite ' . $suite_selected . '</a>' . $language['share_placeholder_text2'] . $eolhtml2;
    } else if ($fs_request_type == "Favourite") { 
        $request_info_body .= $language['favourites_share_placeholder_text1'] . '<a href="'.$fs_urltoshare.'">'.$language['favourites_share_link_text'].'</a>' . $eolhtml2;
    }
} else {
   if ($fs_request_type === "Building") {    
        $request_info_body .= '<a href="'.$fs_urltoshare.'">' . $building_street . ', ' . $building_city . ', ' . $building_province . '</a>' . $eolhtml2;
    } else if ($fs_request_type == "Suite") { 
        $request_info_body .= '<a href="'.$fs_urltoshare.'">' . $building_street . ', ' . $building_city . ', ' . $building_province . ', Suite ' . $suite_selected . '</a>' . $eolhtml2;
    } else if ($fs_request_type == "Favourite") { 
        $request_info_body .= $language['favourites_share_placeholder_text1'] . '<a href="'.$fs_urltoshare.'">'.$language['favourites_share_link_text'].'</a>' . $eolhtml2;
    }
   $request_info_body .= $notes . $eolhtml2; 
}




// assemble and send the email

    if ($fs_request_type == "Building") {   
        $subject = $language['share_email_subject_text'] . $building_name;
    } else if ($fs_request_type == "Suite") {   
        $subject = $language['share_email_subject_text'] . $building_name . ' Suite ' . $suite_selected;
    } else if ($fs_request_type == "Favourite") {   
        $subject = $language['favourites_email_subject_text'] ;
    }


    $message = '';
    $message .= '<html><head>' . $eol;
    $message .= '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />' . $eol;
    if ($fs_request_type == "Building") {
        $message .= '<title> ' . $language['share_email_subject_text'] . $building_name .'</title>' . $eol;
        $email_body_header = $language['share_email_subject_text'] . $building_name;
    } else if ($fs_request_type == "Suite") {
        $message .= '<title> ' . $language['share_email_subject_text'] . $building_name . ' Suite ' . $suite_selected . '</title>' . $eol;
        $email_body_header = $language['share_email_subject_text'] . $building_name . ' Suite ' . $suite_selected;
    } else if ($fs_request_type == "Favourite") {
        $message .= '<title> ' . $language['favourites_email_subject_text'] . '</title>' . $eol;
        $email_body_header = $language['favourites_email_subject_text'];
    }
    
    $message .= '</head><body>' . $eol;


    $message .= $eol . '<div><h3><div style="padding:5px 5px 5px 5px; background-color: #4B5F78;color: #fff;font-family: sans-serif;border-radius: 4px 4px 4px 4px;">' . $email_body_header . '</div></div>';



    $message .= $request_info_body;

        $message .= '<table style="width:100%"><tr>';
        $message .= '<td>&nbsp;</td>';
        $message .= '<td align="right">
<span style="margin: 110px auto;font-size: 15px;">
<span style="color:#000000">' . $language['powered_by'] . '</span>
<a target="_blank" href = "http://www.arcestra.com" style="text-decoration: none; white-space: nowrap;">
<span style="color:#4B5F78;letter-spacing: -3px;">A</span>
<span style="color:#4B5F78;letter-spacing: -3px;">R</span>
<span style="color:#4B5F78;letter-spacing: -3px;">C</span>
<span style="color:#EBD93D;letter-spacing: -3px; font-size: 21px;">e</span>
<span style="color:#40ABBB;letter-spacing: -3px;">S</span>
<span style="color:#584787;letter-spacing: -3px;">T</span>
<span style="color:#38B456;letter-spacing: -3px;">R</span>
<span style="color:#CF2C57;letter-spacing: -3px;">A</span>
</a>
</span>
</td>
</tr>
</table>';
    
    
    
    
        $message .= "</body></html>" . $eol;


        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
        $to_email = $fs_to_email;
        // send CC or not
        if ($fs_cc_me === "true") {
            $headers .= 'From: ' . $fs_from_name . ' <' . $fs_from_email . '>' . "\r\n";
            $headers .= 'Cc: ' . $fs_from_name . ' <' . $fs_from_email . '>' . "\r\n";
        } else {
            $headers .= 'From: ' . $fs_from_name . ' <' . $fs_from_email . '>' . "\r\n";
        }

// Send second email to arcestra sales with same content
        $headers2  = 'MIME-Version: 1.0' . "\r\n";
        $headers2 .= 'Content-type: text/html; charset=utf-8' . "\r\n";
        $arcestra_email = "skelly+sales@arcestra.com";  
        $headers2 .= 'From: ' . $fs_from_name . ' <' . $fs_from_email . '>' . $eol;



     if (strpos($_SERVER['HTTP_HOST'],'rrrd.ca') > -1) {
        // Send any emails to Sean if we're on an rrrd.ca dev site
        // STAGING.FINDSPACE.COM SENDS LIVE EMAILS
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
        $to_email = "sean+testSharing@rationalroot.com";
        $arcestra_email = "skelly+sales@arcestra.com";
        $headers .= 'From: ' . $fs_from_name . ' <' . $fs_from_email . '>' . $eol;
        @mail($to_email, $subject, $message, $headers, '-f sharingTEST@findspace.com');
        @mail($arcestra_email, $subject, $message, $headers2, '-f sharingTEST@findspace.com');
    } else {
        // send the email to leasing contacts
        @mail($to_email, $subject, $message, $headers, '-f sharing@findspace.com');
        // send the email to arcestra sales
        @mail($arcestra_email, $subject, $message, $headers2, '-f sharing@findspace.com');
    }       



// STATS TRACKING

        
//if ($fs_request_type == "Suite") {
//    $suitelist = $suite_selected;
//    $sqft = $suite_sqft;
//} else {
//    $suitelist = '';
//    //  get available sqft of entire building
//    $buildingSqft_query_string = "SELECT SUM(net_rentable_area) AS sqft FROM suites WHERE building_id = '".$building_id."' and leased = 'false'";
//    $buildingSqft_query = mysql_query($buildingSqft_query_string) or die ("building sqft query error: " . mysql_error());
//    $buildingSqft = mysql_fetch_array($buildingSqft_query);    
//    $sqft = $buildingSqft['sqft'];
//}


//if ($contact_email2 != '') {
//    $submitcontacts = $contact_email1 . ', ' . $contact_email2;
//} else {
//    $submitcontacts = $contact_email1;
//}
//        



   }
   
   
   
   
   
   
   
   
   
   
//   
//       function processUrl() {
////        console.log("processUrl url begins");
//        $linkToShare = '';
//        $queryParams = '';
//
////console.log("$queryParams");
////console.log($queryParams);
//
//            if (($queryParams["province"] !== 'undefined') && ($queryParams["province"] !== '') && ($queryParams["province"] !== "All")) {
////                console.log("province is set");
//                $theText = $queryParams["province"];
//            }
//
//            if (($queryParams["city"] !== 'undefined') && ($queryParams["city"] !== '') && ($queryParams["city"] !== "All")) {
////                console.log("city is set");
//                $theText = $queryParams["city"];
//            }
//
//            if (($queryParams["region"] !== 'undefined') && ($queryParams["region"] !== '') && ($queryParams["region"] !== "All")) {
////                console.log("region is set");
//                $theText = $queryParams["region"];
//            }
//
//            if (($queryParams["pc"] !== 'undefined') && ($queryParams["pc"] !== '') && ($queryParams["pc"] !== "All")) {
////                console.log("pc is set");
//                $theText = $queryParams["pc"];
//            }
//
//            if (($queryParams["street_address"] !== 'undefined') && ($queryParams["street_address"] !== '') && ($queryParams["street_address"] !== "All")) {
////                console.log("street_address is set");
//                $theText = $queryParams["street_address"];
//            }
//
//            if (($queryParams["building_name"] !== 'undefined') && ($queryParams["building_name"] !== '') && ($queryParams["building_name"] !== "All")) {
////                console.log("building_name is set");
//                $theText = $queryParams["building_name"];
//            }
//
//            if (($queryParams["SuiteType"] !== 'undefined') && ($queryParams["SuiteType"] !== '') && ($queryParams["SuiteType"] !== "All")) {
////                console.log("SuiteType is set");
//                $lowerCaseVal = $queryParams["SuiteType"].toLowerCase();
//
//            }
//        
//            if (($queryParams["Size"] !== 'undefined') && ($queryParams["Size"] !== '') && ($queryParams["Size"] !== "All")) {
////                console.log("size is set");
//                $sizeVals = $queryParams["Size"];
////                console.log("sizeVals");
////                console.log(sizeVals);
////                    sizeVals = sizeVals.replace(/%2C/g, "").replace(/\+/g,"");
////                if ((sizeVals.indexOf("+-+") > -1) || (sizeVals.indexOf("+and+") > -1)) {
////                    sizeVals = $queryParams["Size"].toString().replace(/ /g, '').replace(/,/g, '').replace(/%2C/g, "").replace(/%20/g, "").replace('-', ',').replace(/\+/g, "");
////                }
////                if (sizeVals.indexOf("Under") > -1) {
////                    sizeVals = "0,5000";
////                } else if (sizeVals.indexOf("above") > -1) {
////                    sizeVals = "200000,2147483646";
////                }
//
////                switch(sizeVals) {
////                    case "0,5000":
////                        $("select#Size").prop('selectedIndex', 1);
////                        break;
////                    case "5000,10000":
////                        $("select#Size").prop('selectedIndex', 2);
////                        break;
////                    case "10000,20000":
////                        $("select#Size").prop('selectedIndex', 3);
////                        break;
////                    case "20000,40000":
////                        $("select#Size").prop('selectedIndex', 4);
////                        break;
////                    case "40000,80000":
////                        $("select#Size").prop('selectedIndex', 5);
////                        break;
////                    case "80000,120000":
////                        $("select#Size").prop('selectedIndex', 6);
////                        break;
////                    case "120000,200000":
////                        $("select#Size").prop('selectedIndex', 7);
////                        break;
////                    case "200000,2147483646":
////                        $("select#Size").prop('selectedIndex', 8);
////                        break;
////                    default:
////                        $("select#Size").prop('selectedIndex', 0);                            
////                }
////                var sizeVals = $queryParams["Size"].replace("%2C", "").replace("+","");
//                $sizeStrings = sizeVals.split(',');
//            } 
//
//
//
//            if (($queryParams["Availability"] !== 'undefined') && ($queryParams["Availability"] !== '') && ($queryParams["Availability"] !== "All")) {
////                console.log("Availability is set");
//                // clean up urlencoded text coming in from url
//                $theText = $queryParams["Availability"];
//                }
//
//            if (($queryParams["SuiteOfficeSize"] !== 'undefined') && ($queryParams["SuiteOfficeSize"] !== '') && ($queryParams["SuiteOfficeSize"] !== "All")) {
////                console.log("SuiteOfficeSize is set");
//                $officeDropVal = $queryParams['SuiteOfficeSize'];
//            }
//
//            if (($queryParams["SuiteClearHeight"] !== 'undefined') && ($queryParams["SuiteClearHeight"] !== '') && ($queryParams["SuiteClearHeight"] !== "All")) {
////                console.log("SuiteClearHeight is set");
//                    $heightVal = $queryParams["SuiteClearHeight"];
//            }
//
//            if (($queryParams["SuiteShippingDoors"] !== 'undefined') && ($queryParams["SuiteShippingDoors"] !== '') && ($queryParams["SuiteShippingDoors"] !== "All")) {
////                console.log("SuiteShippingDoors is set");
//                    $heightVal = $queryParams["SuiteShippingDoors"];
//            }
//
//            if (($queryParams["SuiteDriveInDoors"] !== 'undefined') && ($queryParams["SuiteDriveInDoors"] !== '') && ($queryParams["SuiteDriveInDoors"] !== "All")) {
////                console.log("SuiteDriveInDoors is set");
//                $heightVal = $queryParams["SuiteDriveInDoors"];
//            }
//
//            if (($queryParams["SuiteAvailElecAmps"] !== 'undefined') && ($queryParams["SuiteAvailElecAmps"] !== '') && ($queryParams["SuiteAvailElecAmps"] !== "All")) {
////                console.log("SuiteAvailElecAmps is set");
//                // set the dropdown before processing value
//                $dropVal = $queryParams["SuiteAvailElecAmps"];
//            }
//
//            if (($queryParams["SuiteAvailElecVolts"] !== 'undefined') && ($queryParams["SuiteAvailElecVolts"] !== '') && ($queryParams["SuiteAvailElecVolts"] !== "All")) {
////                console.log("SuiteAvailElecVolts is set");
//                // set the dropdown before processing value
//                $dropVal = $queryParams["SuiteAvailElecVolts"];
//            }
//
//            if (($queryParams["SuiteParkingSurfaceStalls"] !== 'undefined') && ($queryParams["SuiteParkingSurfaceStalls"] !== '') && ($queryParams["SuiteParkingSurfaceStalls"] !== "All")) {
////                console.log("SuiteParkingSurfaceStalls is set");
//                $parkVal = $queryParams["SuiteParkingSurfaceStalls"];
//            }
//
//            if (($queryParams["SuiteTruckTrailerParking"] !== 'undefined') && ($queryParams["SuiteTruckTrailerParking"] !== '') && ($queryParams["SuiteTruckTrailerParking"] !== "All")) {
////                console.log("SuiteTruckTrailerParking is set");
//                $truckParkVal = $queryParams['SuiteTruckTrailerParking'];
//            }
//    }// end function processUrl()


?>