<?php

/**
 * @author Rational Root
 * @see ../main_abstract/SearchData_abstract.php
 * @since September 2015
 * @version 1.0
 * */
class StatsPackage{
    
    
     /**
     * This method updates the stats_traffic database table whenever the building page is opened:
     * @param  {String} $todaytimestamp
     * @param  {String} $today
     * @param  {String} $building_id
     * @param  {String} $building_code
     * @see building.php
     * */
    public function trackBuildingpageOpens($todaytimestamp, $today, $building_id, $building_code, $city) {
    //  BU REPORTING
    //  Check reporting db for a 'today' entry
        $today_check_query_string = "select * from stats_traffic where statsDate = '" . $today . "'and buildingID = '" . $building_id . "' and recordType = 'building'";
        $today_check_query = mysql_query($today_check_query_string) or die ("today building query error: " . mysql_error());
        $today_check = mysql_fetch_array($today_check_query);
        $today_check = mysql_num_rows($today_check_query);
        
        if ($today_check == 0) {
    //  Add a new row for today

    //  get available sqft of entire building
        $buildingSqft_query_string = "SELECT SUM(net_rentable_area) AS sqft FROM suites WHERE building_id = '".$building_id."' and leased = 'false'";
        $buildingSqft_query = mysql_query($buildingSqft_query_string) or die ("building sqft query error: " . mysql_error());
        $buildingSqft = mysql_fetch_array($buildingSqft_query);    
        
        $buildingopens_query_string = "INSERT INTO stats_traffic (statsTimestamp, statsDate, buildingID, building_code, city, building_sqft, buildingView, recordType) VALUES ('" . $todaytimestamp . "', '" . $today . "', '" . $building_id . "', '" . $building_code . "', '". $city ."', '". $buildingSqft['sqft'] ."', '1', 'building')";
        $buildingopens_query = mysql_query($buildingopens_query_string) or die ("buildingopens query error: " . mysql_error());
        } else {
    //  Count sent email
        $buildingopens_query_string = "UPDATE stats_traffic SET buildingView=buildingView+1 WHERE statsDate = '" . $today . "' and buildingID = '" . $building_id . "' and recordType = 'building'";
        $buildingopens_query = mysql_query($buildingopens_query_string) or die ("buildingopens stats update error: " . mysql_error());
        } // end if today_report_check

    }
    
    
    

    
    //1.2 Executed {Print to PDF} (only executed action)
    public function trackBuildingPdfPrint($todaytimestamp, $today, $building_id, $building_code) {

        //  Check traffic db for a 'today' entry
            $today_check_query_string = "select buildingActivity from stats_traffic where statsDate = '" . $today . "'and buildingID = '" . $building_id . "' and recordType = 'building'";
            $today_check_query = mysql_query($today_check_query_string) or die ("today building query error: " . mysql_error());
            $today_check = mysql_fetch_array($today_check_query);
            $today_check_count = mysql_num_rows($today_check_query);
            
        //  Add a new row for today
            if ($today_check_count == 0) {
            $buildingPdfPrint_query_string = "INSERT INTO stats_traffic (clientname, statsTimestamp, statsDate, buildingID, building_code, buildingActivity, recordType) VALUES ('" . $clientname . "', '" . $todaytimestamp . "', '" . $today . "', '" . $building_id . "', '" . $building_code . "', '1', 'building')";
            $buildingPdfPrint_query = mysql_query($buildingPdfPrint_query_string) or die ("buildingPdfPrint query error: " . mysql_error());
            } else {
                if ($today_check['buildingActivity'] != '') {
                    $buildingPdfPrint_query_string = "UPDATE stats_traffic SET buildingActivity=buildingActivity+1 WHERE statsDate = '" . $today . "' and buildingID = '" . $building_id . "' and recordType = 'building'";
                    $buildingPdfPrint_query = mysql_query($buildingPdfPrint_query_string) or die ("buildingPdfPrint stats update error: " . mysql_error());
                } else {
                    $buildingPdfPrint_query_string = "UPDATE stats_traffic SET buildingActivity=1 WHERE statsDate = '" . $today . "' and buildingID = '" . $building_id . "' and recordType = 'building'";
                    $buildingPdfPrint_query = mysql_query($buildingPdfPrint_query_string) or die ("buildingPdfPrint stats update error: " . mysql_error());
                }
            
            } // end if today_report_check

    }
    
    //1.3 Executed {Share}
    
    

    

    
    
    
     /**
     * This method updates the stats_traffic database table whenever the suite page is opened:
     * @param  {String} $todaytimestamp
     * @param  {String} $today
     * @param  {String} $building_id
     * @param  {String} $building_code
     * @param  {String} $suite_id
     * @param  {String} $suite_name
     * @see suite.php
     * */
    public function trackSuitepageOpens($todaytimestamp, $today, $building_id, $building_code, $city, $suite_id, $suite_net_rentable_area, $suite_name) {
    //  BU REPORTING
    //  Check reporting db for a 'today' entry
        $today_check_query_string = "select * from stats_traffic where statsDate = '" . $today . "' and buildingID = '" . $building_id . "' and suiteID = '" . $suite_id . "' and recordType = 'suite'";
        $today_check_query = mysql_query($today_check_query_string) or die ("today suite query error: " . mysql_error());
        $today_check = mysql_num_rows($today_check_query);

        if ($today_check == 0) {
    //  Add a new row for today
        $suiteopens_query_string = "INSERT INTO stats_traffic (statsTimestamp, statsDate, buildingID, building_code, city, suiteID, sqft, suite_name, suiteView, recordType) VALUES ('" . $todaytimestamp . "','" . $today . "', '" . $building_id . "', '" . $building_code . "', '" . $city . "', '" . $suite_id . "', '" . $suite_net_rentable_area . "', '" . $suite_name . "', '1', 'suite')";
        $suiteopens_query = mysql_query($suiteopens_query_string) or die ("suiteopens query error: " . mysql_error());
        } else {
    //  Count sent email
        $suiteopens_query_string = "UPDATE stats_traffic SET suiteView=suiteView+1 WHERE statsDate = '" . $today . "' and buildingID = '" . $building_id . "' and suiteID = '" . $suite_id . "' and recordType = 'suite'";
        $suiteopens_query = mysql_query($suiteopens_query_string) or die ("suiteopens stats update error: " . mysql_error());
        } // end if today_report_check

    }
}
