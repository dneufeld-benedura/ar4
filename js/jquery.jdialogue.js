(function($)
{ 
	$.fn.jDialogue=function(options)
	{
		//Defaults values
		var defaults=
		{
			'validText': '',
			'cancelText': '',
			'title': 'Dialogue',
			'message': '',
			'content': null,
			//'thumbs': false,
			//'thumbnails': null, 
			'splash': true,
			'defaultButton': 'valid',
			'onconfirm': null,
			'oncancel': null,
			'onclick': null,
			'onopen': null,
			'onclose': null
		};  
		var parameters=$.extend(defaults, options); 
		
		if(parameters.splash)
		{
			//If the splash screen is not created, we add it
			var splash=$('#jDialogueSplash');
			if(splash.length==0)
			{
				splash=$('<div id="jDialogueSplash"></div>');
				splash.css(
				{
					'position': 'fixed',
					'width': '100%',
					'height': '100%',
					'top': '0px',
					'left': '0px',
					'opacity': 0.7,
					'z-index': 999999
				}).hide().appendTo($('body'));
			}
		}
	
		//begin plugin
		return this.each(function()
		{
			var link=$(this);			
			
			//handle click event
			link.click(function(clickEvent)
			{
				clickEvent.preventDefault();
				
				//create the confirm window
				var dialogueWindow=$('<div></div>');
				dialogueWindow.addClass('jDialogueWindow').css(
				{
					'position': 'fixed',
					'left': '50%',
					'top': '50%',
					'z-index': 1000000
				}).hide().appendTo($('body'));
				
				var title=$('<h1>');
				title.addClass('jDialogueTitle').html(parameters.title).appendTo(dialogueWindow);
				
				var message=$('<div>');
				message.addClass('jDialogueMessage').html(parameters.message).appendTo(dialogueWindow);
				
				
				if(parameters.content) {
					var dialogueContent = $('<div>');
					dialogueContent.addClass('jDialogueContent').html('<div id="loader"><img src="images/ajax-loader.gif" /></div>').appendTo(dialogueWindow);
					
					$('#loader')
    					//.hide()  // hide it initially
    					.ajaxStart(function() {
        					$(this).show();
    					})
    					.ajaxStop(function() {
        					//$(this).hide();
    				});
					var url = parameters.content;
					//$.get(url, function(data) {
						$('.jDialogueContent').load(url,function(data) {
  						//$('.jDialogueContent').html(data);
 						 //alert('Load was performed.');
						 //callback - center the window
						dialogueWindow.css(
						{
							//'margin-left': '-'+(confirmWindow.outerWidth())/2+'px',
							'margin-left': '-'+(dialogueWindow.width())/2+'px',
							//'margin-top': '-'+(confirmWindow.outerHeight())/2+'px'
							'margin-top': '-'+(dialogueWindow.height())/2+'px'
						});
					});
					
						
				}
				//center the confirm Window
				//*** doesn't seem to work, but did/does in orginal dev code
				//there is a bug in jquery 1.8.x regarding outerWidth and outerHeight so switched to using width and height instead
				//using jquery 1.9 in dev version so that is why it worked
				dialogueWindow.css(
				{
					//'margin-left': '-'+(confirmWindow.outerWidth())/2+'px',
					'margin-left': '-'+(dialogueWindow.width())/2+'px',
					//'margin-top': '-'+(confirmWindow.outerHeight())/2+'px'
					'margin-top': '-'+(dialogueWindow.height())/2+'px'
				});
							
				//Function called when the confirm window is closed
				function jDialogueWindowClosed(callbackFunction)
				{
					var data={};
					/*dialogueWindow.find('*[name]').each(function()
					{
						data[$(this).attr('name')]=$(this).val();
					});
					
					dialogueWindow.find('input').attr('disabled','true');*/
					if(callbackFunction)
					{
						callbackFunction(link,data);
					}
					
					if(parameters.splash)
					{
						splash.fadeOut('slow');
					}
					dialogueWindow.fadeOut('slow',function()
					{
						if(parameters.onclose)
						{
							parameters.onclose(link,data);
						}
						$(this).remove();
					});
				}
				
				//Thumbs container
				/*if(parameters.thumbs) {
					var thumbsContainer=$('<div></div>');
					thumbsContainer.addClass('jConfirmThumbsContainer');
					thumbsContainer.append('<ul class="jConfirmThumbs"><li>' + parameters.thumbnails[0] + '</li><li>' + parameters.thumbnails[1] + '</li><li>' + parameters.thumbnails[2] + '</li></ul>');
					thumbsContainer.appendTo(confirmWindow);
				}*/
				
				//Buttons container
				var buttonsContainer=$('<p>');
				buttonsContainer.addClass('jDialogueButtonsContainer').appendTo(dialogueWindow);

				//Add the OK button
				if(parameters.validText) {
					var validButton=$('<input/>');
					validButton.attr('type','button').attr('value',parameters.validText)
					.addClass('jDialogueValidButton').click(function()
					{
						jDialogueWindowClosed(parameters.onconfirm);
					}).appendTo(buttonsContainer);
				}
				//Add the Cancel button
				if(parameters.cancelText) {
					var cancelButton=$('<input/>');
					cancelButton.attr('type','button').attr('value',parameters.cancelText)
					.addClass('jDialogueCancelButton').click(function()
					{
						jDialogueWindowClosed(parameters.oncancel);
					}).appendTo(buttonsContainer);
				}
				
				
				
				if(parameters.splash)
				{
					splash.fadeIn('slow');
				}
				
				dialogueWindow.fadeIn('slow',function()
				{
					if(parameters.defaultButton=='valid')
					{
						validButton.focus();
					}
					else if(parameters.defaultButton=='cancel')
					{
						cancelButton.focus();
					}
					
					if(parameters.onopen)
					{
						parameters.onopen(link);
					}
				});
			});			
		});						   
	};
})(jQuery);