<?php
$spec_qy = mysql_query("select * from building_specifications where building_code='" . $building_code . "' and lang = " . $lang_id . "");
$row = mysql_fetch_array($spec_qy);


$spec_qy_string2 = "select * from suites_specifications where suite_id='" . $suite_id . "' and lang = ".$lang_id."";
$spec_qy2 = mysql_query($spec_qy_string2);
$row2 = mysql_fetch_array($spec_qy2);


$lang = "en_CA";
$lang_id = 1;
if (isset($_REQUEST['lang'])) {
    if ($_REQUEST['lang'] == "fr_CA") {

        $lang = "fr_CA";
        $lang_id = 0;
    } else {
        
    }
} else {
    
}

setlocale(LC_ALL, $lang . '.utf8');



$suitecount_query_string = 'SELECT COUNT(*) AS realsuitecount FROM suites alls WHERE building_code = ? AND lang=1 AND leased="false"';
// Preparing the statement
$stmt = $db->prepare($suitecount_query_string);
// binding the parameters
$stmt->bind_param('s', $building_code);
// Executing the query
if (!$stmt->execute()) {
    trigger_error('The query execution failed; MySQL said (' . $stmt->errno . ') ' . $stmt->error, E_USER_ERROR);
}
$col1 = null;
$stmt->bind_result($suiteCount);
while ($stmt->fetch()) {
    // having to use this while makes no sense at all but it doesn't work w/o it.
}
$stmt->close();

if ($suite_net_rent ==! "Negotiable") {
    ($suite_net_rent !== '' && $suite_net_rent !== '0.00' && $suite_net_rent !== 0 ? money_format('%.2n', doubleval($suite_net_rent)) : $language['na_text']);
}

if ($suite_add_rent_total !== '' && $suite_add_rent_total !== '0.00' && $suite_add_rent_total !== 0) {
      $suite_add_rent_total =  money_format('%.2n', doubleval($suite_add_rent_total));
} else {
    $suite_add_rent_total = $language['na_text'];
}
?>

<section class="fs_section fs_suite-features">
    <div class="row">
        <div class="col">
            <div class="card fs_card-features">
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <div class="row">
                                <div class="col fs_col-feature-icon">
                                    <i class="material-icons fs_icon-feature">settings_overscan</i>
                                </div>
                                <div class="col fs_col-feature-value">
                                    <h3 class="card-title fs_card-feature-title"><?php echo $language['size_text']; ?> / <?php echo $language['building_information_sq_ft_trans']; ?></h3>
                                    <p class="fs_feature-value"><?php echo $suite_net_rentable_area; ?></p>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="row">
                                <div class="col fs_col-feature-icon">
                                    <i class="material-icons fs_icon-feature">dashboard</i>
                                </div>
                                <div class="col fs_col-feature-value">
                                    <h3 class="card-title fs_card-feature-title"><?php echo $language['availability_text']; ?></h3>
                                    <p class="fs_feature-value"><?php echo $suite_availability; ?></p>                                                                
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="row">
                                <div class="col fs_col-feature-icon">
                                    <i class="material-icons fs_icon-feature">attach_money</i>
                                </div>
                                <div class="col fs_col-feature-value">
                                    <h3 class="card-title fs_card-feature-title"><?php echo $language['price_text']; ?> / <?php echo $language['month_abbr']; ?></h3>
                                    <p class="fs_feature-value"><?php echo $suite_net_rent; ?></p>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="row">
                                <div class="col fs_col-feature-icon">
                                    <i class="material-icons fs_icon-feature">monetization_on</i>
                                </div>
                                <div class="col fs_col-feature-value">
                                    <h3 class="card-title fs_card-feature-title"><?php echo $language['additional_rent_text']; ?> / <?php echo $language['month_abbr']; ?></h3>
                                    <p class="fs_feature-value"><?php echo $suite_add_rent_total; ?></p>                                                                
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="row">
                                <div class="col fs_col-feature-icon">
                                    <i class="material-icons fs_icon-feature">label</i>
                                </div>
                                <div class="col fs_col-feature-value">
                                    <h3 class="card-title fs_card-feature-title"><?php echo $language['space_type_text']; ?></h3>
                                    <p class="fs_feature-value"><?php echo ($row2['suite_type'] !== '' ? ucwords(mb_strtolower($row2['suite_type'], 'UTF-8')) : $language['na_text']); ?></p>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="row">
                                <div class="col fs_col-feature-icon">
                                    <i class="material-icons fs_icon-feature">filter_1</i>
                                </div>
                                <div class="col fs_col-feature-value">
                                    <h3 class="card-title fs_card-feature-title"><?php echo $language['floor_text']; ?></h3>
                                    <p class="fs_feature-value"><?php echo ($row2['floor_name'] !== '0' ? $row2['floor_name'] : $language['na_text']); ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>




<section class="fs_section fs_suite-specifications">
<?php if (!checkForEmpty($row['costs_posted_net_rate']) && !checkForEmpty($row['costs_realty']) && !checkForEmpty($row['costs_power']) && !checkForEmpty($row['costs_operating']) && !checkForEmpty($row['costs_other']) && !checkForEmpty($row['costs_add_rent_total'])) {
    // this section is emtpy so skip it
} else { ?>
    <div class="card fs_card-specifications">
        <div class="card-body">
            <div class="card-title"><h3><?php echo $language['tenant_costs_title_text']; ?></h3></div>
        <?php if (checkForEmpty($row['costs_posted_net_rate'])) { ?>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['posted_net_rate_text']; ?></div>
                <div class="col fs_specification-value"><?php echo money_format('%.2n', (double) $row['costs_posted_net_rate']). ' ' . $language['building_information_sq_ft_trans']; ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['costs_realty'])) {
        ?>            
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['realty_tax_text']; ?></div>
                <div class="col fs_specification-value"><?php echo money_format('%.2n', (double) $row['costs_realty']). ' ' . $language['building_information_sq_ft_trans']; ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['costs_power'])) {
        ?>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['utilities_text']; ?></div>
                <div class="col fs_specification-value"><?php echo money_format('%.2n', (double) $row['costs_power']). ' ' . $language['building_information_sq_ft_trans']; ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['costs_operating'])) {
        ?>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['operating_costs_text']; ?></div>
                <div class="col fs_specification-value"><?php echo money_format('%.2n', (double) $row['costs_operating']). ' ' . $language['building_information_sq_ft_trans']; ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['costs_other'])) {
        ?>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['other_text']; ?></div>
                <div class="col fs_specification-value"><?php echo money_format('%.2n', (double) $row['costs_other']). ' ' . $language['building_information_sq_ft_trans']; ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['costs_add_rent_total'])) {
        ?>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['additional_rent_total_text']; ?></div>
                <div class="col fs_specification-value"><?php echo money_format('%.2n', (double) $row['costs_add_rent_total']). ' ' . $language['building_information_sq_ft_trans']; ?></div>
            </div>
        <?php } ?>
        </div>
    </div>
<?php } ?>
    
<?php if (!checkForEmpty($row['year_built']) && !checkForEmpty($row['renovated']) && !checkForEmpty($row['website'])) {
    // this section is emtpy so skip it
} else { ?>
    <div class="card fs_card-specifications">
        <div class="card-body">
            <div class="card-title"><h3><?php echo $language['general_title_text']; ?></h3></div>
        <?php if (checkForEmpty($row['year_built'])) { ?>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['year_built_text_nocolon']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row['year_built'] !== '' ? $row['year_built'] : $language['na_text']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['renovated'])) {
        ?>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['last_renovated_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row['renovated'] !== '' ? $row['renovated'] : $language['na_text']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['website'])) {
        ?>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['website_text']; ?></div>
                <?php if (strpos($row['website'], "http://") > -1 || strpos($row['website'], "https://") > -1) {
                    $theWebsite = '<a href="' . $row['website'] . '" target="_blank">'.$row['website'].'</a>';
                } else {
                    $theWebsite = '<a href="http://' . $row['website'] . '" target="_blank">'.$row['website'].'</a>';
                } ?>
                <div class="col fs_specification-value"><?php echo ($row['website'] !== '' ? $theWebsite : $language['na_text']); ?></div>
            </div>
        <?php } ?>
        </div>
    </div>
    
<?php } ?>

<?php if (!checkForEmpty($row['total_space']) && !checkForEmpty($row['available_space']) && !checkForEmpty($row['largest_contiguous_available']) && !checkForEmpty($row['floors']) && !checkForEmpty($row['highrise_floor_plate']) && !checkForEmpty($row['lowrise_floor_plate']) && !checkForEmpty($row['total_space']) && !checkForEmpty($row['total_office_space']) && !checkForEmpty($row['available_office_space']) && !checkForEmpty($row['retail_units']) && !checkForEmpty($row['total_retail_space']) && !checkForEmpty($row['available_retail_space'])) {
    // this section is emtpy so skip it
} else { ?>    
    <div class="card fs_card-specifications">
        <div class="card-body">
            <div class="card-title"><h3><?php echo $language['building_size_title_text']; ?> (<?php echo $language['building_information_sq_ft_trans']; ?>)</h3></div>
        <?php if (checkForEmpty($row['total_space'])) { ?>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['total_space_text_nocolon']; ?></div>
                <?php $total_space = format_numbers($row['total_space'], $language); ?>
                <div class="col fs_specification-value"><?php echo ($total_space !== '0' ? $total_space . ' ' . $language['building_information_sq_ft_trans'] : $language['na_text']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['available_space'])) {
        ?>            
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['total_available_space_text']; ?></div>
                <?php $total_available_space = format_numbers($row['available_space'], $language);?>
                <div class="col fs_specification-value"><?php echo ($total_available_space !== '0' ? $total_available_space . ' ' . $language['building_information_sq_ft_trans'] : $language['na_text']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['largest_contiguous_available'])) {
        ?>            
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['largest_contiguous_avl_text']; ?></div>
                <?php $largest_contiguous_available = format_numbers($row['largest_contiguous_available'], $language);?>
                <div class="col fs_specification-value"><?php echo ($largest_contiguous_available !== '0' ? $largest_contiguous_available . ' ' . $language['building_information_sq_ft_trans'] : $language['na_text']); ?></div>
            </div>
            
            <div class="card-title-type"><h4><?php echo $language['building_type_office']; ?></h4></div>
        <?php }
        
        if (checkForEmpty($row['floors'])) {
        ?>            
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['number_of_floors_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row['floors'] !== '' ? $row['floors'] : $language['na_text']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['highrise_floor_plate'])) {
        ?>            
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['typ_hirise_floor_text']; ?></div>
                <?php $highrise_floor_plate = format_numbers($row['highrise_floor_plate'], $language); ?>
                <div class="col fs_specification-value"><?php echo ($highrise_floor_plate !== '0' ? $highrise_floor_plate . ' ' . $language['building_information_sq_ft_trans'] : $language['na_text']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['lowrise_floor_plate'])) {
        ?>            
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['typ_lowrise_floor_text']; ?></div>
                <?php $lowrise_floor_plate = format_numbers($row['lowrise_floor_plate'], $language); ?>
                <div class="col fs_specification-value"><?php echo ($lowrise_floor_plate !== '0' ? $lowrise_floor_plate . ' ' . $language['building_information_sq_ft_trans'] : $language['na_text']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['total_space'])) {
        ?>            
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['total_space_text_nocolon']; ?></div>
                <?php $total_space = format_numbers($row['total_space'], $language); ?>
                <div class="col fs_specification-value"><?php echo ($total_space !== '0' ? $total_space . ' ' . $language['building_information_sq_ft_trans'] : $language['na_text']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['total_office_space'])) {
        ?>            
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['total_office_space_text']; ?></div>
                <?php $total_office_space = format_numbers($row['total_office_space'], $language); ?>
                <div class="col fs_specification-value"><?php echo ($total_office_space !== '0' ? $total_office_space . ' ' . $language['building_information_sq_ft_trans'] : $language['na_text']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['available_office_space'])) {
        ?>            
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['avl_office_space_text']; ?></div>
                <?php $available_office_space = format_numbers($row['available_office_space'], $language); ?>
                <div class="col fs_specification-value"><?php echo ($available_office_space !== '0' ? $available_office_space . ' ' . $language['building_information_sq_ft_trans'] : $language['na_text']); ?></div>
            </div>
            
            <div class="card-title-type"><h4><?php echo $language['building_type_retail']; ?></h4></div>
        <?php }
        
        if (checkForEmpty($row['retail_units'])) {
        ?>            
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['number_retail_units_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row['retail_units']!== '' ? $row['retail_units'] : $language['na_text']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['total_retail_space'])) {
        ?>            
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['total_retail_space_text']; ?></div>
                <?php $total_retail_space = format_numbers($row['total_retail_space'], $language); ?>
                <div class="col fs_specification-value"><?php echo ($total_retail_space !== '0' ? $total_retail_space . ' ' . $language['building_information_sq_ft_trans'] : $language['na_text']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['available_retail_space'])) {
        ?>            
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['avl_retail_space_text']; ?></div>
                <?php $available_retail_space = format_numbers($row['available_retail_space'], $language); ?>
                <div class="col fs_specification-value"><?php echo ($available_retail_space !== '0' ? $available_retail_space . ' ' . $language['building_information_sq_ft_trans'] : $language['na_text']); ?></div>
            </div>
        <?php } ?>
        </div>
    </div>
<?php } ?>
    
    
    
<?php if (!checkForEmpty($row['const_fibre_optic_capability']) && !checkForEmpty($row['const_shipping_receiving']) && !checkForEmpty($row['const_ceiling_height']) && !checkForEmpty($row['const_washrooms_per_floor']) && !checkForEmpty($row['const_exterior_finish']) && !checkForEmpty($row['const_hvac_hours_of_operation']) && !checkForEmpty($row['const_after_hours_hvac']) && !checkForEmpty($row['const_hvac_description'])) {
    // this section is emtpy so skip it
} else { ?>     
    <div class="card fs_card-specifications">
        <div class="card-body">
            <div class="card-title"><h3><?php echo $language['construction_title_text']; ?></h3></div>
        <?php if (checkForEmpty($row['const_fibre_optic_capability'])) { ?>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['fibre_optic_capability_text']; ?></div>
                <div class="col fs_specification-value"><?php checkOrX($row['const_fibre_optic_capability']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['const_shipping_receiving'])) {
        ?>            
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['shipping_receiving_text']; ?></div>
                <div class="col fs_specification-value"><?php checkOrX($row['const_shipping_receiving']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['const_ceiling_height'])) {
        ?>            
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['ceiling_height_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row['const_ceiling_height'] !== '' ? $row['const_ceiling_height'] : $language['na_text']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['const_washrooms_per_floor'])) {
        ?>            
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['washrooms_per_floor_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row['const_washrooms_per_floor'] !== '' ? $row['const_washrooms_per_floor'] : $language['na_text']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['const_exterior_finish'])) {
        ?>            
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['exterior_finish_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row['const_exterior_finish'] !== '' ? $row['const_exterior_finish'] : $language['na_text']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['const_hvac_hours_of_operation'])) {
        ?>            
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['hvac_hours_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row['const_hvac_hours_of_operation'] !== '' ? $row['const_hvac_hours_of_operation'] : $language['na_text']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['const_after_hours_hvac'])) {
        ?>            
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['after_hours_hvac_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row['const_after_hours_hvac'] !== '' ? money_format('%.2n', (double) $row['const_after_hours_hvac']) : $language['na_text']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['const_hvac_description'])) {
        ?>            
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['hvac_description']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row['const_hvac_description'] !== '' ? $row['const_hvac_description'] : $language['na_text']); ?></div>
            </div>
        <?php } ?>
        </div>
    </div>
<?php } ?>
<?php $total = (int) $row['elevators_hirise'] + (int) $row['elevators_midrise'] + (int) $row['elevators_lowrise']; ?>
<?php if ($total === 0 && !checkForEmpty($row['elevators_parking']) && !checkForEmpty($row['elevators_freight'])) {
    // this section is emtpy so skip it
} else { ?>     
    <div class="card fs_card-specifications">
        <div class="card-body">
            <div class="card-title"><h3><?php echo $language['elevators_title_text']; ?></h3></div>
        <?php if ($total !== 0) { ?>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['number_of_elevators_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($total !== '' ? $total : $language['na_text']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['elevators_parking'])) {
        ?>            
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['number_parking_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row['elevators_parking'] !== '' ? $row['elevators_parking'] : $language['na_text']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['elevators_freight'])) {
        ?>            
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['number_freight_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row['elevators_freight'] !== '' ? $row['elevators_freight'] : $language['na_text']); ?></div>
            </div>
        <?php } ?>
        </div>
    </div>    
<?php } ?>
    
<?php if (!checkForEmpty($row['safety_fire_detection']) && !checkForEmpty($row['safety_sprinkler_system']) && !checkForEmpty($row['safety_manned_security']) && !checkForEmpty($row['safety_security_system']) && !checkForEmpty($row['safety_barrier_free'])) {
    // this section is emtpy so skip it
} else { ?>     
    <div class="card fs_card-specifications">
        <div class="card-body">
            <div class="card-title"><h3><?php echo $language['safety_and_access_text']; ?></h3></div>
        <?php if (checkForEmpty($row['safety_fire_detection'])) { ?>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['fire_detection_system_text']; ?></div>
                <div class="col fs_specification-value"><?php checkOrX($row['safety_fire_detection']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['safety_sprinkler_system'])) {
        ?>            
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['sprinkler_system_text']; ?></div>
                <div class="col fs_specification-value"><?php checkOrX($row['safety_sprinkler_system']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['safety_manned_security'])) {
        ?>            
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['manned_security_text']; ?></div>
                <div class="col fs_specification-value"><?php checkOrX($row['safety_manned_security']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['safety_security_system'])) {
        ?>            
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['security_system_text']; ?></div>
                <div class="col fs_specification-value"><?php checkOrX($row['safety_security_system']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['safety_barrier_free'])) {
        ?>            
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['barrier_free_acccess_text']; ?></div>
                <div class="col fs_specification-value"><?php checkOrX($row['safety_barrier_free']); ?></div>
            </div>
        <?php } ?>
        </div>
    </div>
<?php } ?>
<?php if (!checkForEmpty($row['parking_surface_stalls']) && !checkForEmpty($row['parking_surface_ratio']) && !checkForEmpty($row['parking_total_stalls']) && !checkForEmpty($row['parking_above_stalls']) && !checkForEmpty($row['parking_above_ratio']) && !checkForEmpty($row['parking_below_stalls']) && !checkForEmpty($row['parking_below_ratio']) && !checkForEmpty($row['parking_description'])) {
    // this section is emtpy so skip it
} else { ?> 
    <div class="card fs_card-specifications">
        <div class="card-body">
            <div class="card-title"><h3><?php echo $language['parking_text']; ?></h3></div>
        <?php if (checkForEmpty($row['parking_surface_stalls'])) { ?>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['surface_stalls_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row['parking_surface_stalls'] !== '' ? $row['parking_surface_stalls'] : $language['na_text']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['parking_surface_ratio'])) {
            $theRatio = str_replace('\\', '/', $row['parking_surface_ratio']);
            $theRatio = str_replace('.00', '', $theRatio);
        ?>            
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['surface_ratio_text']; ?></div>
                <div class="col fs_specification-value"><?php echo str_replace('\\', '/', $theRatio) . ' ' . $language['building_information_sq_ft_trans']; ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['parking_total_stalls'])) {
        ?>            
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['total_stalls_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row['parking_total_stalls'] !== '' ? $row['parking_total_stalls'] : $language['na_text']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['parking_above_stalls'])) {
        ?>            
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['above_gnd_stalls_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row['parking_above_stalls'] !== '' ? $row['parking_above_stalls'] : $language['na_text']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['parking_above_ratio'])) {
            $theAbvRatio = str_replace('\\', '/', $row['parking_above_ratio']);
            $theAbvRatio = str_replace('.00', '', $theAbvRatio);
        ?>            
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['above_gnd_ratio_text']; ?></div>
                <div class="col fs_specification-value"><?php echo str_replace('\\', '/', $theAbvRatio) . ' ' . $language['building_information_sq_ft_trans']; ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['parking_below_stalls'])) {
        ?>            
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['below_gnd_stalls_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row['parking_below_stalls'] !== '' ? $row['parking_below_stalls'] : $language['na_text']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['parking_below_ratio'])) {
            $theBlwRatio = str_replace('\\', '/', $row['parking_below_ratio']);
            $theBlwRatio = str_replace('.00', '', $theBlwRatio);
        ?>            
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['below_gnd_ratio_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($theBlwRatio !== '' ? str_replace('\\', '/', $theBlwRatio) . ' ' . $language['building_information_sq_ft_trans'] : $language['na_text']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['parking_description'])) {
        ?>            
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['parking_desc_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row['parking_description'] !== '' ? $row['parking_description'] : $language['na_text']); ?></div>
            </div>
        <?php } ?>
        </div>
    </div>
<?php } ?>

<?php if (!checkForEmpty($row['public_surface_route']) && !checkForEmpty($row['public_subway_access'])) {
    // this section is emtpy so skip it
} else { ?> 
    <div class="card fs_card-specifications">
        <div class="card-body">
            <div class="card-title"><h3><?php echo $language['public_transport_title_text']; ?></h3></div>
        <?php if (checkForEmpty($row['public_surface_route'])) { ?>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['transit_surface_route_text']; ?></div>
                <div class="col fs_specification-value"><?php checkOrX($row['public_surface_route']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['public_subway_access'])) {
        ?>            
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['direct_subway_access_text']; ?></div>
                <div class="col fs_specification-value"><?php checkOrX($row['public_subway_access']); ?></div>
            </div>
        <?php } ?>
        </div>
    </div>
<?php } ?>
<?php if (!checkForEmpty($row['anchor_tenant1']) && !checkForEmpty($row['anchor_tenant2']) && !checkForEmpty($row['anchor_tenant3']) && !checkForEmpty($row['anchor_tenant4']) && !checkForEmpty($row['anchor_tenant5']) && !checkForEmpty($row['anchor_tenant6'])) {
    // this section is emtpy so skip it
} else { ?>     
    <div class="card fs_card-specifications">
        <div class="card-body">
            <div class="card-title"><h3><?php echo $language['anchor_tenants_text']; ?></h3></div>
        <?php if (checkForEmpty($row['anchor_tenant1'])) { ?>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['tenant_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row['anchor_tenant1'] !== '' ? $row['anchor_tenant1'] : $language['na_text']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['anchor_tenant2'])) {
        ?>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['tenant_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row['anchor_tenant2'] !== '' ? $row['anchor_tenant2'] : $language['na_text']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['anchor_tenant3'])) {
        ?>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['tenant_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row['anchor_tenant3'] !== '' ? $row['anchor_tenant3'] : $language['na_text']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['anchor_tenant4'])) {
        ?>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['tenant_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row['anchor_tenant4'] !== '' ? $row['anchor_tenant4'] : $language['na_text']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['anchor_tenant5'])) {
        ?>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['tenant_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row['anchor_tenant5'] !== '' ? $row['anchor_tenant5'] : $language['na_text']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['anchor_tenant6'])) {
        ?>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['tenant_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row['anchor_tenant6'] !== '' ? $row['anchor_tenant6'] : $language['na_text']); ?></div>
            </div>
        <?php } ?>
        </div>
    </div>
<?php } ?>
</section>



<!--<section class="fs_section fs_suite-notes">
    <div class="card fs_card-notes">
        <div class="card-body">
            <div class="row">
                <div class="col fs_col-card-notes-icon">
                    <i class="material-icons fs_notes-icon">insert_drive_file</i>
                    <h2 class="fs_notes-title">Notes</h2>
                </div>
                <div class="col fs_col-card-notes-text">
                    <h3 class="fs_notes-title"><?php // echo $language['building_description_text']; ?></h3>
                    <p class="fs_notes-text"><?php // echo $building_desc; ?></p>
                    <h3 class="fs_notes-title">Sublease</h3>
                    <p class="fs_notes-text">Sed tempus sed fermentum hendrerit. Suspendisse ullamcorper euismod dignissim. etiam commodo varius lacus, et laoreet neque tristique. Phasellus eleifend urna sed accumsan scelerisque. Nulla quis semper lacus, eget congue eros.</p>
                </div>
            </div>
        </div>
    </div>
</section>-->
