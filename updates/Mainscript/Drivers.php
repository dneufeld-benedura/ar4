<?php

 

include ('Suites.php');

/**
 * This class is a driver class for building the output. 
 * @author RationalRoot
 * @version AR2.0
 * @since December 2014
 */
class Drivers {

    private static $queries;
    private static $language;
    private static $lang;
    private static $createHTML;
    private static $suites;
    private static $uniqueid;

    /**
     * A constructor for setting up the Driver class
     * @param type $language
     * @param type $lang
     * @param type $queries
     * @param type $createHTML
     */
    public final function __construct($language, $lang, $queries, $createHTML) {
        self::$queries = $queries;
        self::$language = $language;
        self::$lang = $lang;
        self::$createHTML = $createHTML;
        self::$suites = new Suites($language, $lang, $queries, $createHTML);
    }

    /**
     * This function builds the drivers and output(s) based on the building query fetch, and the count of suites
     * @param type $buildings_query
     * @param type $initial_suites_count
     * @param type $output
     * @param type $suitedifftop
     * @return type
     */
    public function buildDriversAndOutput($buildings_query, $initial_suites_count, $uniqueid) {
        self::$uniqueid = $uniqueid; 
        $drivers = "";
        $buildingdrivertemp = "";
        $suitedriver = "";
        $buildingdriver = "";
        $buildingDriverData = "";
        $output = "";
        $suitedifftop = "";

        if ($initial_suites_count > 0) {

            $drivers = $this->getDrivers($buildings_query);
            $buildingdrivertemp = $drivers['buildingdrivertemp'];
            $suitedriver = $drivers['suitedriver'];
            $buildingdriver = array_unique($buildingdrivertemp); // remove doubles from buildingdriver array. we only want unique building codes.
			
			
            $buildingDriverData = $this->getAllBuildingData($buildingdriver, $suitedriver, $output);
            $output = $buildingDriverData['output'];
            $suitedifftop = $buildingDriverData['suitedifftop'];
        } //end if mysql_num_rows $buildings_query (that is, $initial_suites_count)


        $data = array();
        $data['drivers'] = $drivers;
        $data['buildingdrivertemp'] = $buildingdrivertemp;
        $data['suitedriver'] = $suitedriver;
        $data['buildingdriver'] = $buildingdriver;
        $data['buildingDriverData'] = $buildingDriverData;
        $data['output'] = $output;
        $data['suitedifftop'] = $suitedifftop;

        return $data;
    }

    /**
     * Gets and returns the temporary building driver, and the suite driver
     * @param type $buildings_query
     * @return array
     */
    private function getDrivers($buildings_query) {
        $drivers = array();
        $buildingdrivertemp = array();
        $suitedriver = array();

        while ($driver = mysql_fetch_array($buildings_query)) {

            array_push($buildingdrivertemp, $driver['building_code']);
            array_push($suitedriver, $driver['suite_id']);
        }
        $drivers['buildingdrivertemp'] = $buildingdrivertemp;
        $drivers['suitedriver'] = $suitedriver;
        return $drivers;
    }

    /**
     * Brings all Building data together. Returns the message ($output) as well as $suitedifftop, $suitediffbottom
     * @param type $buildingdriver
     * @param type $suitedriver
     * @param string $output
     * @return string
     */
    private static function getAllBuildingData($buildingdriver, $suitedriver, $output) {
        $changecount = 0;
        foreach ($buildingdriver as $code_building) {
            $nobuilding = 0;
            $suitedifftop = '';
            $suitediffbottom = '';

            $realbuildingsquery = self::$queries->getRealBuildingsQuery($code_building); //gets all building data

            $real_buildings = self::getBuildingData($realbuildingsquery, $suitedriver, $suitediffbottom, $nobuilding);

            $building_suites_count = $real_buildings['building_suites_count'];
            $suitedifftop = $real_buildings['suitedifftop'];
            $changecount = $real_buildings['changecount'];
            $suitediffbottom = $real_buildings['suitediffbottom']; 
            $building_suites_count = $real_buildings['building_suites_count'];
            $newsuite = self::$suites->getNewSuite();
            $leasedsuite = self::$suites->getLeasedSuite();

            $changedsuite = $real_buildings['changedsuite'];
            
            if (($nobuilding < $building_suites_count) && ($changedsuite != 0) || ($newsuite > 0) || ($leasedsuite>0)) {
                $output .= $suitedifftop . $suitediffbottom;
            }
            self::$suites->resetNewSuite();
            self::$suites->resetLeasedSuite();
            $suitedifftop = ''; 
            $suitediffbottom = '';
        }  //end foreach building driver
        $data = array();
        $data['output'] = $output;
        $data['suitedifftop'] = $suitedifftop;
        $data['suitediffbottom'] = $suitediffbottom; 

        return $data;
    }

    /**
     * Returns the relevant data (Count of suites, Message, Difference count), per building.
     * @param type $realbuildingsquery
     * @param type $suitedriver
     * @param type $suitediffbottom
     * @param type $nobuilding
     * @return type
     */
    private function getBuildingData($realbuildingsquery, $suitedriver, $suitediffbottom, $nobuilding) {
        $suitedifftop = '';
        $changedsuite=0; 
        while ($Building = mysql_fetch_array($realbuildingsquery)) {
            $suite_count_query = self::$queries->suitesCountQuery($Building);
            $building_suites_count = mysql_num_rows($suite_count_query);


            $contacts_query = self::$queries->getContactsQuery($Building);

            $suitedifftop .= self::$createHTML->getSuiteDiffTop($Building);

            $forEachSuite = self::$suites->traverseSuitedriver($Building, $suitedriver, $suitediffbottom, $nobuilding);

            $changedsuite = $changedsuite+$forEachSuite['changedsuite']; 
            $changecount = $forEachSuite['changecount'];
            $suitediffbottom = $forEachSuite['suitediffbottom'];
            //if there is no data, do not add any contacts!
            if ($suitediffbottom!=''){
            $suitediffbottom .= self::$createHTML->getContacts($contacts_query, self::$uniqueid);}
        } // end while $building 
 
        $data = array();
        $data['building_suites_count'] = $building_suites_count;
        
        $totalSuitesAdded = self::$suites->getTotalSuitesAdded();
//        echo "Total Suites Added:".$totalSuitesAdded."<br>";
        if ($totalSuitesAdded>0 && $suitediffbottom!=''){
        $data['suitedifftop'] = $suitedifftop;
        }
        $data['changecount'] = $changecount;
        $data['changedsuite'] = $changedsuite;
        if ($totalSuitesAdded>0){
            $data['suitediffbottom'] = $suitediffbottom;
        }
        $data['building_suites_count'] = $building_suites_count;
        return $data;
    }

}
