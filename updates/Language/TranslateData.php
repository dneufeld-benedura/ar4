<?php
include('../../theme/db.php');

class TranslateData {


public function getEnglishToFrenchDropdown($province, $region, $sub_region, $space_type, $size, $availability) {
    
    
    
        $returnData = array();
        
        if ($province == 'All' ){
        $province = 'Tous';    
        }
        

        if ($region == 'All' ){
        $region = 'Tous';    
        }
        

        if ($sub_region== 'All' ){
        $sub_region = 'Tous';    
        }
        
        
        
        

        if ($space_type== 'All' ){ $space_type = 'Tous'; }
        else if($space_type== 'OFFICE' ){$space_type='BUREAU';}
        else if($space_type== 'INDUSTRIAL' ){$space_type='INDUSTRIEL';}
        else if($space_type== 'RETAIL' ){$space_type='DÉTAIL';}
        

        if ($size== 'All' ){$size = 'Tous';}
        else if($size== '5,000 and Under' ){$size='5,000 et moins';}
        else if($size== '40,000 and Above' ){$size='40,000 et plus';}
        
        
        

        if ($availability== 'All' ){$availability = 'Tous';}
        else if($availability== 'Immediately' ){$availability='Immédiatement';}
        else if($availability== 'Under 3 Months' ){$availability='Moins de 3 mois';}
        else if($availability== '3-6 Months'){$availability='3-6 mois';}
        else if($availability== '6-12 Months' ){$availability='6-12 mois';}
        else if($availability== 'Over 12 Months' ){$availability='Plus de 12 mois';}
        

        $dataArray = self::getProvinceRegionSubRegionTranslated($province, $region, $sub_region); 
        
        
        if ($dataArray['province']!='' and !  is_null($dataArray['province'])){
        $returnData['province'] = $dataArray['province'];}else{$returnData['province']=$province;}
        
        if ($dataArray['region']!='' and !  is_null($dataArray['region'])){
        $returnData['region'] = $dataArray['region'];}else{$returnData['region']=$region;}
        
        if ($dataArray['sub_region']!='' and !  is_null($dataArray['sub_region'])){
        $returnData['sub_region'] = $dataArray['sub_region'];}else{$returnData['sub_region']=$sub_region;}
        
        
        $returnData['space_type'] = $space_type;
        $returnData['size'] = $size;
        $returnData['availability'] = $availability;
        
        return $returnData;

}

 



    private static function getProvinceRegionSubRegionTranslated($province, $region, $sub_region){
    $dataArray = array();    
    $queryString = "Select building_id from buildings where province = '".$province."' and region ='".$region."' and sub_region='".$sub_region."'";
    $queryString = "Select * from buildings where building_id=(".$queryString.") and lang='0'"; 
    $queryString = str_replace("Tous", "", $queryString);
    
    $query = mysql_query($queryString);
    $data = mysql_fetch_array($query);
    
    if ($data['province'] != '' and ! is_null($data['province'])) {
            $province = $data['province'];
        }

    if ($data['region'] != '' and ! is_null($data['region'])) {
            $region = $data['region'];
        }
        
    if ($data['sub_region'] != '' and ! is_null($data['sub_region'])) {
            $sub_region = $data['sub_region'];
    }    
    
    $dataArray['province'] = $province;
    $dataArray['region'] = $region;
    $dataArray['sub_region'] = $sub_region;
    
    return $dataArray;
    
    }
    
    
}
