<?php
/**
 * @author Rational Root
 * @since October 2014
 *
 */


class getPageAddressesAndLinks {
    private $returned_content;
    
    public function getPageAddressesAndLinks($url){
        
        $this->returned_content=$this->loadPage($url);
        
    }
    
    
    public function getBuildingsData($property_div){
      // Split the content into array elements

		$pieces = explode($property_div, $this->returned_content);
		$cnt = count($pieces);

		// initiate the stack

		$stack = array();
		for ($t = 1; $t < $cnt; $t++)
			{
			if (strpos($pieces[$t], "<div class=") !== FALSE)
				{
				$pos = strpos($pieces[$t], "<div class=");
				$s = substr($pieces[$t], 0, $pos);
				$s = trim($s);

				// now split the filtered string and put into a 2d array

				$ns = explode('">', $s);

				$link = $url . substr($ns[0], 9);
				$name = substr($ns[1], 1, -4);
				$arr = array($link,$name);

					array_push($stack, $arr);

				}
			}

		return $stack;  
        
    }
    
    
    
	public function getSuitesData($property_div, $suite_href)
		{

                // Split the content into array elements

		$pieces = explode($property_div, $this->returned_content);
		$cnt = count($pieces);

		// initiate the stack

		$stack = array();
		for ($t = 1; $t < $cnt; $t++)
			{
			if (strpos($pieces[$t], "<div class=") !== FALSE)
				{
				$pos = strpos($pieces[$t], "<div class=");
				$s = substr($pieces[$t], 0, $pos);
				$s = trim($s);

				// split the $pieces into more pieces for each suite

				$push_suites = FALSE;
				if (strpos($pieces[$t], 'href="suite.php?') !== FALSE)
					{
					$stack_suites = array();
					$push_suites = TRUE;
					$pieces_suites = explode($suite_href, $pieces[$t]);
					for ($tt = 0; $tt < count($pieces_suites); $tt++)
						{

						//  extract the information and add it to array
						$pos_suite = strpos($pieces_suites[$tt], '">');
						$suiteUrl = substr($pieces_suites[$tt], 0, $pos_suite);
						if (strpos($suiteUrl, 'building.php?') == FALSE)
							{
							$suiteUrl = 'suite.php' . $suiteUrl;
							
							$name_suite = substr($suiteUrl, strpos($suiteUrl, 'suitenum=')+9);
							$name_suite = substr($name_suite, 0, strpos($name_suite,'&'));
							
							
							$pos_area = strpos($pieces_suites[$tt], '<td class="area">') + 17;
							$area = substr($pieces_suites[$tt], $pos_area);
							$pos_area = strpos($area, '</td>');
							$area = substr($area, 0, $pos_area);
							$arr_suite = array(
								$suiteUrl,
								$name_suite,
								$area
							);
							array_push($stack_suites, $arr_suite);
							}
						}
					}


				$ns = explode('">', $s);

				$link = $url . substr($ns[0], 9);
				$name = substr($ns[1], 1, -4);
				$arr = array(
					$link,
					$name
				);
				if ($push_suites == TRUE)
					{
					array_push($arr, $stack_suites);
					array_push($stack, $arr);
					}

				

				}
			}

		return $stack;
		}

	private function loadPage($url)
		{
		$ch = curl_init();
		$timeout = 5;
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
		$data = curl_exec($ch);
		curl_close($ch);
		return $data;
		}
	}

?>
