<?php

/**
 * The purpose of this php file is to output the building and suites data as XML format
 */

include('theme/db.php');
// Theme Query
$theme_query = mysql_query("select * from theme");
$theme       = mysql_fetch_array($theme_query);

//initiate the finalStr
$finalStr = "";

//initiate the header and save it to a file
$header = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
$finalStr.=$header;

if ($theme['publishxmlfeed'] == 1) {



//initiate building query string

$building_query = "select distinct allb.building_code, allb.building_name, allb.building_type, allb.street_address, allb.city, allb.province, allb.postal_code from buildings allb, suites alls where allb.lang='1' and alls.lang='1' order by allb.country asc, allb.province asc, allb.city asc, allb.sort_streetname asc, allb.sort_streetnumber asc";
//Execute the buldings query string
$result = mysql_query($building_query);
$finalStr .="<data>\n";
while ($building_data = mysql_fetch_assoc($result)) {
    $finalStr.="<building>\n";

    $finalStr.="\t<building_code>" . $building_data['building_code'] . "</building_code>\n";
    $finalStr.="\t<building_name>" . $building_data['building_name'] . "</building_name>\n";
    $finalStr.="\t<building_type>" . $building_data['building_type'] . "</building_type>\n";
    $finalStr.="\t<street_address>" . $building_data['street_address'] . "</street_address>\n";
    $finalStr.="\t<city>" . $building_data['city'] . "</city>\n";
    $finalStr.="\t<province>" . $building_data['province'] . "</province>\n";
    $finalStr.="\t<postal_code>" . $building_data['postal_code'] . "</postal_code>\n";


// generate the suites' query data and loop for each
    $suite_query = "select * from suites where lang=1 and building_code ='" . $building_data['building_code'] . "' order by floor_number desc, suite_name asc";
    $result_suite = mysql_query($suite_query);
    while ($suite_data = mysql_fetch_assoc($result_suite)) {
        $finalStr.="\t\t<suite>\n";


        $finalStr.="\t\t\t<suite_index>" . $suite_data['suite_index'] . "</suite_index>\n";
        $finalStr.="\t\t\t<lang>" . $suite_data['lang'] . "</lang>\n";
        $finalStr.="\t\t\t<building_code>" . $suite_data['building_code'] . "</building_code>\n";
        $finalStr.="\t\t\t<building_id>" . $suite_data['building_id'] . "</building_id>\n";
        $finalStr.="\t\t\t<floor_name>" . $suite_data['floor_name'] . "</floor_name>\n";
        $finalStr.="\t\t\t<floor_number>" . $suite_data['floor_number'] . "</floor_number>\n";
        $finalStr.="\t\t\t<suite_id>" . $suite_data['suite_id'] . "</suite_id>\n";
        $finalStr.="\t\t\t<suite_name>" . $suite_data['suite_name'] . "</suite_name>\n";
        $finalStr.="\t\t\t<suite_type>" . $suite_data['suite_type'] . "</suite_type>\n";
        $finalStr.="\t\t\t<net_rentable_area>" . $suite_data['net_rentable_area'] . "</net_rentable_area>\n";
        $finalStr.="\t\t\t<contiguous_area>" . $suite_data['contiguous_area'] . "</contiguous_area>\n";
        $finalStr.="\t\t\t<taxes_and_operating>" . $suite_data['taxes_and_operating'] . "</taxes_and_operating>\n";
        $finalStr.="\t\t\t<availability>" . $suite_data['availability'] . "</availability>\n";
        $finalStr.="\t\t\t<description>" . $suite_data['description'] . "</description>\n";
        $finalStr.="\t\t\t<net_rent>" . $suite_data['net_rent'] . "</net_rent>\n";
        $finalStr.="\t\t\t<add_rent_total>" . $suite_data['add_rent_total'] . "</add_rent_total>\n";
        $finalStr.="\t\t\t<presentation_link>" . $suite_data['presentation_link'] . "</presentation_link>\n";
        $finalStr.="\t\t\t<presentation_name>" . $suite_data['presentation_name'] . "</presentation_name>\n";
        $finalStr.="\t\t\t<presentation_description>" . $suite_data['presentation_description'] . "</presentation_description>\n";
        $finalStr.="\t\t\t<pdf_plan>" . $suite_data['pdf_plan'] . "</pdf_plan>\n";
        $finalStr.="\t\t\t<spp_link>" . $suite_data['spp_link'] . "</spp_link>\n";
        $finalStr.="\t\t\t<model>" . $suite_data['model'] . "</model>\n";
        $finalStr.="\t\t\t<new>" . $suite_data['new'] . "</new>\n";
        $finalStr.="\t\t\t<leased>" . $suite_data['leased'] . "</leased>\n";
        $finalStr.="\t\t\t<record_date>" . $suite_data['record_date'] . "</record_date>\n";
        $finalStr.="\t\t\t<record_source>" . $suite_data['record_source'] . "</record_source>\n";

        $finalStr.="\t\t</suite>\n";
    }



//initiate the suite query

    $finalStr.="</building>\n";
}
$finalStr .="</data>\n";

echo $finalStr;


} else {
    echo "Feed not enabled<br>";
}

?>