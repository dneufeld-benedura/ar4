<div id="specswrapper">

    <h3 class="specifications"><?php echo $language['specifications_text']; ?></h3>
<?php 

include ('../objects/SpecPages2.0pdf.php');
$specPages = new SpecPages();

	$spec_qy = mysql_query("select * from building_specifications where building_code='" . $building_code . "' and lang = ".$lang_id."");
	
	$row = mysql_fetch_array($spec_qy);
	
	setlocale(LC_MONETARY, 'en_US');

?>
<?php //----------------------------------------------------------------------?>

<table class="properties" id="properties">
<thead>
<tr>
<th colspan="6"><?php echo $language['tenant_costs_title_text']; ?>
</tr>
</thead>
<tr class="odd">
<?php
$specPages-> posted_net_rate_text($language, $row);
$specPages-> realty_tax_text($language, $row);
$specPages-> utilities_text($language, $row); 
?>
</tr>
<tr class="even">
<?php            
$specPages-> operating_costs_text($language, $row);
$specPages-> other_text($language, $row); 
$specPages-> additional_rent_total_text($language, $row);
?>
</tr>
</table>
<?php //----------------------------------------------------------------------?>

<table class="properties" id="properties">
<thead>
<tr>
<th colspan="6"><?php echo $language['site_description_text']; ?></th>
</tr>
</thead>
<tr class="odd">
<?php
$specPages-> year_built_text_nocolon($language, $row);
$specPages-> building_zoned_text($language, $row);
$specPages-> avg_office_percent_area_text($language, $row);
?>
</tr>
<tr class="even">
<?php
$specPages-> number_of_buildings_text($language, $row);
$specPages-> number_of_units_text($language, $row);
echo '<td>&nbsp;</td>';
echo '<td>&nbsp;</td>';
?>
</tr>
</table>
<?php //----------------------------------------------------------------------?>

<table class="properties" id="properties">
<thead>
<tr>
<th colspan="6"><?php echo $language['building_size_title_text']; ?></th>
</tr>
</thead>
<tr class="odd">
<?php
$specPages-> total_space_text_nocolon($language, $row);
$specPages-> warehouse_size_text($language, $row);
echo '<td>&nbsp;</td>';
echo '<td>&nbsp;</td>';
?>
</tr>
</table>
<?php //----------------------------------------------------------------------?>

<table class="properties" id="properties">

<tr>
<td colspan="6"><font size="1"><?php echo $language['building_type_industrial']; ?></font></td>
</tr>

<tr class="odd">
<?php
$specPages-> total_industrial_space_text($language, $row);
$specPages-> avl_industrial_space_text($language, $row);
echo '<td>&nbsp;</td>';
echo '<td>&nbsp;</td>';
?>
</tr>
</table>
<?php //----------------------------------------------------------------------?>

<table class="properties" id="properties">

<tr>
<td colspan="6"><font size="1"><?php echo $language['building_type_office']; ?></font></td>
</tr>

<tr class="odd">
<?php
$specPages-> total_office_space_text($language, $row);
$specPages-> avl_office_space_text($language, $row);
echo '<td>&nbsp;</td>';
echo '<td>&nbsp;</td>';
?>
</tr>
</table>
<?php //----------------------------------------------------------------------?>

<table class="properties" id="properties">
<thead>
<tr>
<th colspan="6"><?php echo $language['technical_title_text']; ?></th>
</tr>
</thead>
<tr class="odd">
<?php
$specPages-> ceiling_height_text($language, $row);
$specPages-> max_door_height_text($language, $row);
$specPages-> building_plan_text($language, $row);
?>
</tr>
<tr class="even">
<?php
$specPages-> shipping_doors_drive_in_text($language, $row);
$specPages-> shipping_doors_truck_text($language, $row);
$specPages-> rail_loading_text($language, $row);
?>
</tr>
<tr class="odd">
<?php
$specPages-> marshalling_area_text($language, $row);
$specPages-> dolly_pad_text($language, $row);
$specPages-> outside_storage_text($language, $row);
?>
</tr>
<tr class="even">

</tr>
</table>
<?php //----------------------------------------------------------------------?>

<table class="properties" id="properties">
<thead>
<tr>
<th colspan="6"><?php echo $language['construction_title_text']; ?></th>
</tr>
</thead>
<tr class="odd">
<?php
$specPages-> avl_electrical_volts_text($language, $row);
$specPages-> avl_electrical_amps_text($language, $row);
$specPages-> heating_text($language, $row);
?>
</tr>
<tr class="even">
<?php 
$specPages->const_power_text($language, $row);  
$specPages-> exterior_finish_text($language, $row);
echo '<td>&nbsp;</td>';
echo '<td>&nbsp;</td>';
?>
</tr>
</table>
<?php //----------------------------------------------------------------------?>

<table class="properties" id="properties">
<thead>
<tr>
<th colspan="6"><?php echo $language['safety_and_parking_title_text']; ?></th>
</tr>
</thead>
<tr class="odd">
<?php
$specPages-> sprinkler_system_text($language, $row);
$specPages-> total_stalls_text($language, $row);
echo '<td>&nbsp;</td>';
echo '<td>&nbsp;</td>';
?>
</tr>
<tr class="even">
 <?php
 $specPages-> parking_description($language, $row);
 ?>

                  </tr>
       </table>


</div>
