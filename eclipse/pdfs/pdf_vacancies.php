<?php

	include('../classes/Queries.php');
	include('../classes/SearchData.php');
        include('../../objects/LanguageQuery.php');
        include('../../objects/StatsPackage.php');
        include('../../theme/db.php');
        include("../../assets/php/krumo/class.krumo.php");

        // Initiate the Queries class
	$queries = new Queries();
        //Initiate the Search Data class
	$searchData = new SearchData();
        // initiate the StatsPackage class
        $stats = new StatsPackage();

                                
	$languageQuery         = new LanguageQuery();
	$langArray             = $languageQuery->getLanguageAndID();
	$lang                  = $langArray[0];
	$lang_id               = $langArray[1];

        $language = array();  
        $language_query_string = "select * from languages_dynamic ld, languages l, languages2 l2 where ld.lang_id =? and l.lang_id =? and l2.lang_id =?";
        $language_query_statement = $db->prepare($language_query_string);
        $language_query_statement->bind_param('iii', mysqli_real_escape_string($db, $lang_id), mysqli_real_escape_string($db, $lang_id), mysqli_real_escape_string($db, $lang_id));
        if ($language_query_statement->execute()){
            $result = $language_query_statement->get_result();
            while($row = $result->fetch_array(MYSQLI_ASSOC)){ 
                $language = $row;
            }
        }
        $language_query_statement->close();
        
        
	// Theme Query
	$theme_query = mysql_query("select * from theme");
	$theme = mysql_fetch_array($theme_query);
				
	if (isset($_REQUEST['show_no_vacancy'])) {
		if ($_REQUEST['show_no_vacancy'] == 'on') {
			$show_no_vacancy = 1;
		} else {
			$show_no_vacancy = 0;
		}
	} else {
		if ($theme['no_vacancy_default'] == 1) {
			$show_no_vacancy = 1;
		} else {
			$show_no_vacancy = 0;
		}
	}
        
        
        
	//setting locale for time, date, money, etc
		setlocale(LC_ALL, $language['locale_string']);
                date_default_timezone_set('America/Toronto');
	


	
	
		$no_results_html = '
                <div class="result">
                    <div class="search-result clearfix">
                        <div class="search-result-left">
                            <div class="thumbnail"><img src="'.$language['vacancy_report_website'].'/images/no_thumb_700.png" width="100" height="100" alt="No Buildings Match Your Search"></a></div>
                        </div>
                        <div class="search-result-right">
                            <div class="title-bar">
                                <div class="property-name">' . $language['no_matches'] . '</div>
                            </div>
                            <!-- eo title-bar //-->
          
                            <div class="property-contact">
                                <div class="contact-name">' . $language['no_matches_advice'] . '</div>
                            </div>
                      
                        </div>
                        <!-- eo search-result-right //-->
                    </div>
                    <!-- eo search-result //-->
      
                </div>';
			
$building_name_sterm_array   = '';
$building_name               = '';
$building_name_sterm_prepop  = '';
$current_page_result         = '';
$availability_selected_array = '';
$availability_notafter       = '';
$availability_notbefore      = '';
$building_codes_narrow       = '';
$sub_region_selected         = '';
$region_selected             = '';
$city_selected               = '';
$province_selected           = '';
$buildingtype_selected       = '';
$suitetype_selected          = '';
$size_selected               = '';
	// this drives the narrowed results
	


$building_name_sterm_array  = $searchData->get_building_name_sterm_array();
$building_name       = $building_name_sterm_array['building_name_sterm'];
$building_name_sterm       = $building_name_sterm_array['building_name_sterm'];
$building_name_sterm_prepop = $building_name_sterm_array['building_name_sterm_prepop'];                                
$current_page_result         = $searchData->get_current_page_result(); 

$availability_selected_array = $searchData->get_availability_selected();
$availability_notafter       = $availability_selected_array['availability_notafter'];
$availability_notbefore      = $availability_selected_array['availability_notbefore'];        
$building_codes_narrow       = $searchData->get_building_codes_narrow();       
$sub_region_selected         = $searchData->get_sub_region_selected($language, $lang, $db);        
$region_selected             = $searchData->get_region_selected($language, $lang, $db);        
$city_selected               = $searchData->get_city_selected($language, $lang, $db);
$province_selected           = $searchData->get_province_selected($language, $lang, $db);
$buildingtype_selected       = $searchData->get_buildingtype_selected();        
$suitetype_selected = $searchData->get_suitetype_selected($language);	
$size_selected = $searchData->get_size_selected();
        $sizeFrom                    = "";
        $sizeTo                      = "";
        if (isset($_REQUEST['Size']) && $_REQUEST['Size'] == "Custom"){
            $size_selected = "Custom";
            $sizeFrom = $_REQUEST['sizeFrom'];
            $sizeTo = $_REQUEST['sizeTo'];
        }





    function get_suite_availability($language, $row) {
        $suite_availability_raw = $row['availability'];
        $suite_availability = $language['immediately_building'];
        $suite_notice_period = $row['notice_period'];
        
        if ($suite_notice_period != "no") {
            switch ($suite_notice_period) {
                case "30d":
                    $suite_availability = $language['30days_text'];
                    break;
                case "60d":
                    $suite_availability = $language['60days_text'];
                    break;
                case "90d":
                    $suite_availability = $language['90days_text'];
                    break;
                case "120d":
                    $suite_availability = $language['120days_text'];
                    break;
            }
        } else if ($suite_availability_raw != "" && $suite_availability_raw >= time()) {
            $suite_availability = strftime('%b. %Y', $suite_availability_raw);
        } else {
            // no availability nothin
        }       
        return $suite_availability;
    }	










	if (isset($_GET['Address'])) {
		$addresses = explode(',',$_GET['Address']);
                $province_selected = $searchData->get_province_selected($language, $lang, $db);

	} else {
                                

        $advanced_search = 0;
        
        if ($theme['advanced_search'] == 1) {
            $advanced_search = 1;
        } else {
            $advanced_search = 0;
        }



$industrialsearchelement = 0;
if ($advanced_search == 1) {
            
// industrial unit advanced search elements    
        $webQuery = '';
        $industrialsearchelement = 0;
        $industrial_office_space_selected               = "All";
        if (isset($_REQUEST['SuiteOfficeSize']) && $_REQUEST['SuiteOfficeSize'] != "All"){$industrial_office_space_selected = $_REQUEST['SuiteOfficeSize'];
        $webQuery .="&SuiteOfficeSize=".$industrial_office_space_selected;
        $industrialsearchelement++;
        } 

        $industrial_clear_height_selected               = "All";
        if (isset($_REQUEST['SuiteClearHeight']) && $_REQUEST['SuiteClearHeight'] != "All"){$industrial_clear_height_selected = $_REQUEST['SuiteClearHeight'];
        $webQuery .="&SuiteClearHeight=".$industrial_clear_height_selected;
        $industrialsearchelement++;
        } 

        

        $industrial_parking_stalls_selected               = "All";
        if (isset($_REQUEST['SuiteParkingSurfaceStalls']) && $_REQUEST['SuiteParkingSurfaceStalls'] != "All"){$industrial_parking_stalls_selected = $_REQUEST['SuiteParkingSurfaceStalls'];
        $webQuery .="&SuiteParkingSurfaceStalls=".$industrial_parking_stalls_selected;
        $industrialsearchelement++;
        } 
//echo "ise: " . $industrialsearchelement . "<br><br>";

        $industrial_electrical_volts_selected               = "All";
        if (isset($_REQUEST['SuiteAvailElecVolts']) && $_REQUEST['SuiteAvailElecVolts'] != "All"){$industrial_electrical_volts_selected = $_REQUEST['SuiteAvailElecVolts'];
        $webQuery .="&SuiteAvailElecVolts=".$industrial_electrical_volts_selected;
        $industrialsearchelement++;
        } 
//echo "ise: " . $industrialsearchelement . "<br><br>";

        $industrial_electrical_amps_selected               = "All";
        if (isset($_REQUEST['SuiteAvailElecAmps']) && $_REQUEST['SuiteAvailElecAmps'] != "All"){$industrial_electrical_amps_selected = $_REQUEST['SuiteAvailElecAmps'];
        $webQuery .="&SuiteAvailElecAmps=".$industrial_electrical_amps_selected;
        $industrialsearchelement++;
        } 
//echo "ise: " . $industrialsearchelement . "<br><br>";

        $industrial_shipping_doors_selected               = "All";
        if (isset($_REQUEST['SuiteShippingDoors']) && $_REQUEST['SuiteShippingDoors'] != "All"){$industrial_shipping_doors_selected = $_REQUEST['SuiteShippingDoors'];
        $webQuery .="&SuiteShippingDoors=".$industrial_shipping_doors_selected;
        $industrialsearchelement++;
        } 
//echo "ise: " . $industrialsearchelement . "<br><br>";

        $industrial_drivein_doors_selected               = "All";
        if (isset($_REQUEST['SuiteDriveInDoors']) && $_REQUEST['SuiteDriveInDoors'] != "All"){$industrial_drivein_doors_selected = $_REQUEST['SuiteDriveInDoors'];
        $webQuery .="&SuiteDriveInDoors=".$industrial_drivein_doors_selected;
        $industrialsearchelement++;
        } 
//echo "ise: " . $industrialsearchelement . "<br><br>";

        $industrial_parking_selected               = "All";
        if (isset($_REQUEST['SuiteParking']) && $_REQUEST['SuiteParking'] != "All"){$industrial_parking_selected = $_REQUEST['SuiteParking'];
        $webQuery .="&SuiteParking=".$industrial_parking_selected;
        $industrialsearchelement++;
        } 
//echo "ise: " . $industrialsearchelement . "<br><br>";

        $industrial_trucktrailer_parking_selected               = "All";
        if (isset($_REQUEST['SuiteTruckTrailerParking']) && $_REQUEST['SuiteTruckTrailerParking'] != "All"){$industrial_trucktrailer_parking_selected = $_REQUEST['SuiteTruckTrailerParking'];
        $webQuery .="&SuiteTruckTrailerParking=".$industrial_trucktrailer_parking_selected;
        $industrialsearchelement++;
        } 
//echo "ise: " . $industrialsearchelement . "<br><br>";

// End industrial unit advanced search elements 
        
} // end if ($advanced_search == 1) {
























$buildings_query_string_query = $queries->getBuildingsQueryString($size_selected, $sizeFrom, $sizeTo, $suitetype_selected, $buildingtype_selected, $province_selected, $region_selected, $city_selected, $sub_region_selected, $building_codes_narrow, $lang_id, $availability_notafter, $availability_notbefore, $building_name_sterm, $show_no_vacancy, $industrial_office_space_selected, $industrial_clear_height_selected, $industrial_shipping_doors_selected, $industrial_drivein_doors_selected, $industrial_electrical_volts_selected, $industrial_electrical_amps_selected, $industrial_parking_stalls_selected, $industrial_trucktrailer_parking_selected, $industrialsearchelement, $advanced_search, $db);
//$buildings_query = mysql_query($buildings_query_string);

    // process buildings query string
    $buildings_query_string = $buildings_query_string_query[0];
//    $buildings_query_string .=" LIMIT ".$postnumbers." OFFSET ".$offset;
    $pageCurrent = '';
//    $buildings_query_string = str_replace("and allb.building_name like '%none%'", "", $buildings_query_string);
    if ($buildings_query_statement = $db->prepare($buildings_query_string)) {
        // carry on
    } else {
        trigger_error('Wrong SQL: ' . $buildings_query_string . ' Error: ' . $db->error, E_USER_ERROR);
    }
    // prep dynamic variable query
    $param_type = '';
    $n = count($buildings_query_string_query[1]);
    for($i = 0; $i < $n; $i++) {
      $param_type .= $buildings_query_string_query[1][$i];
    }
    // Add in 2 more string fields for $postnumbers and $offset for infinite scrolling / loading
        $param_type .="ss";
    
    /* with call_user_func_array, array params must be passed by reference */
    $a_params[] = & $param_type;

    for($i = 0; $i < $n; $i++) {
      /* with call_user_func_array, array params must be passed by reference */
      $a_params[] = & $buildings_query_string_query[2][$i];
    }
    // Add in $postnumbers and $offset for infinite scrolling / loading
    $postnumbers = 999;
    $offset = 0;
    $a_params[] = & $postnumbers;
    $a_params[] = & $offset;
    
    // call_user_func_array is how we can pass dynamic quantities of variables to mysqli prepared statements.
    call_user_func_array(array($buildings_query_statement, 'bind_param'), $a_params);
    $buildings_query = array();
    if ($buildings_query_statement->execute()){
        $result = $buildings_query_statement->get_result();
        while($row = $result->fetch_array(MYSQLI_ASSOC)){ 
            $buildings_query[] = $row;
        }
    } else {
        trigger_error('Could not execute preview images query statement: Error: ' . $db->error, E_ALL);
    }
    $buildings_query_statement->close();

	$building_driver = array();
	
	// loop through results and push them in to an array 
//		while ($row = mysql_fetch_array($buildings_query)) {
//				$building_driver[] = $row['building_code'];
//		}
		foreach($buildings_query as $row) {
				$building_driver[] = $row['building_code'];
		}		
} // end if/else $Address / not $Address 		



date_default_timezone_set('America/New_York'); 

				
				


        
        // Branding Query
	$branding_name = '';

//	$branding_query_string = "Select * from branding where region = '" . $province_selected . "'";
//	$branding_query = mysql_query($branding_query_string) or die ("branding_query error: " . mysql_error());
//        $branding = mysql_fetch_array($branding_query);
        $branding = array();  
        $branding_query_string = "Select * from branding where region =?";
        $branding_query_statement = $db->prepare($branding_query_string);
        $branding_query_statement->bind_param('s', mysqli_real_escape_string($db, $province_selected));
        if ($branding_query_statement->execute()){
            $result = $branding_query_statement->get_result();
            $row = $result->fetch_array(MYSQLI_ASSOC);
                $branding = $row;
        }
        $branding_query_statement->close();
        
        
	$branding_count = count($branding);
	if ($branding_count == 0) {
		$branding_name = $language['page_title'];
	} else {
		$branding_name = $branding['company_name'];
	}
        
        
        ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $language['page_title']; ?></title>
<link rel="stylesheet" type="text/css" href="../../css/reset.css" />

<style type="text/css">



/*---------------------*/
/*COLOURED CSS ELEMENTS*/
/*---------------------*/
/*---------------------*/
/*---------------------*/

#header {
	width: 775px;
	margin-top: -80px;	
	background: <?php echo $theme['primary_colour_dompdf_hex']; ?>;
	padding: 0;
	color: #fff;
}

.header-border {
	 background: <?php echo $theme['secondary_colour_dompdf_hex']; ?>;	
}



<?php 
$tertiaryColour = '';
if ($theme['fourth_colour_hex'] != '') {
    $tertiaryColour = $theme['fourth_colour_hex'];
} else {
    $tertiaryColour = $theme['secondary_colour_dompdf_hex'];
};

?>


.result h2 {
	font-size: 1.4em;
	padding: 5px 0 0 0;
	margin-bottom: 0;
	border-top: 1px solid <?php echo $tertiaryColour; ?>;
/*	border-bottom: 2px solid <?php //echo $theme['secondary_colour_dompdf_hex']; ?>;
	margin: 0 0 10px 0;*/
	color: <?php echo $theme['primary_colour_dompdf_hex']; ?>;
	text-transform: uppercase;		
}

.result div.smallcityprov {
	border-bottom: 1px solid <?php echo $tertiaryColour; ?>;
	font-size: .8em;
	padding: 0 0 5px 0;
	/*margin-top: -10px;*/
}



table#sub-header h2 {
	font-size: 1.2em;
	color: <?php echo $theme['primary_colour_dompdf_hex']; ?>;
	text-align: right;
	text-transform: uppercase;
	border: none;
	margin: 0 0 5px 0;
}



h3 {
	font-size: 1.4em;
	text-transform: uppercase;
	border-bottom: 1px solid <?php echo $tertiaryColour; ?>;
	margin-bottom: 10px;
}

table.additional-costs {
	width: 600px;
	border-top: 1px solid <?php echo $tertiaryColour; ?>;
	border-bottom: 1px solid <?php echo $tertiaryColour; ?>;
	margin-bottom: 10px;
	/*font-size: 0.7em;*/
	font-size: 1.0em;
}

#additional-costs {
	width: 600px;
	border-top: 1px solid <?php echo $tertiaryColour; ?>;
	border-bottom: 1px solid <?php echo $tertiaryColour; ?>;
	margin-bottom: 10px;
	/*font-size: 0.7em;*/
	font-size: 1.0em;
}


hr.resultspacer {
	display: block;
	height: 1px;
	border: 0;
	border-top: 1px solid <?php echo $tertiaryColour; ?>;
    margin: 1em 0;
	padding: 0;
}

table.result2 thead {
	border-top: 1px solid <?php echo $tertiaryColour; ?>;
	border-bottom: 1px solid <?php echo $tertiaryColour; ?>;
}

table.result2 tfoot {
	border-top: 1px solid <?php echo $tertiaryColour; ?>;
	border-bottom: 1px solid <?php echo $tertiaryColour; ?>;
}

<?php if ($theme['contiguous_text_colour'] != '') {
	$contiguous_text_color = $theme['contiguous_text_colour'];
} else {
	$contiguous_text_color = $theme['fontcolor'];
}
?>

span.contiguous {
    color: <?php echo $contiguous_text_color; ?>;    
}

.result a,
.result2 a {
    text-decoration: none;
    color: <?php echo $theme['primary_colour_dompdf_hex']; ?>;
}
/*---------------------*/
/*---------------------*/
/* END COLOUR ELEMENTS */
/*---------------------*/
/*---------------------*/
/*---------------------*/
/*---------------------*/
/*---------------------*/








@page {
	margin: 160px 20px 10px 20px;
	padding: 0;	
}

body {
	margin: 0;
	padding: 0;
	font-family: Arial, Helvetica, sans-serif;
	color: #555555;
	font-size: 10px;
}

.clear
{
	clear: both;
	font-size: 0px;
	line-height: 0px;
	visibility: collapse;
}




#logo {
	margin: 20px 0 20px 20px;	
	width: 148px;
	height: 55px;
	
}

table#header h1 {
	font-size: 3.1em;
	color: #fff;
	display: inline;
	/*border-top: 1px solid #fff;
	border-bottom: 1px solid #fff;
	text-transform: uppercase;*/
	text-align: right;
	padding-top:5px;
	/*margin: 40px 40px 40px 0;*/
}

.titleblock {
	/*border-top: 1px solid #fff;
	border-bottom: 1px solid #fff;*/
	vertical-align:middle;
	height:50px;
}


.titleblock-padding {
	/*height:10px;*/
}

.titleblock-pin-line-top {
	border-bottom: 1px solid #fff;
	/*height:4px;*/
}

.titleblock-pin-line-bottom {
	border-top: 1px solid #fff;
	/*height:10px;*/
}

.titleblock-rightside-padding {
	width:20px;
}

table#sub-header {
	width: 775px;
	margin: 10px 0 5px 0;	
}



.second-building-page{
	padding-top:20px;
}

#breakit {
	page-break-before: always;
}


div.toptoprow{
	text-align: right;
	font-size: 12px;
	font-weight: bold;
	color: #000000;
}



table.result {
	width: 775px;
	font-size: 1.2em;
	padding: 0;
	/*margin-bottom: 10px;*/
	page-break-inside: avoid;
}

table.result2 {
	width: 775px;
	font-size: 1.2em;
	padding: 0;
	margin-bottom: 10px;
	page-break-inside: avoid;
}


table.result2 th {
	/*padding: 5px;*/
	font-weight: bold;
	text-align: center;	
	color: #000;
	/*padding: 5px;*/
}

table.result2 td {
	text-align: center;
	/*padding: 2px;*/
	vertical-align: text-top;
}




/*table.result2 th.suite, table.result2 td.suite{
	width: 50px;
}

table.result2 th.area, table.result2 td.area{
	width: 85px;
}

table.result2 th.rent, table.result2 td.rent{
	width: 50px;
}



table.result2 th.availability, table.result2 td.availability{
	width: 100px;
}*/

table.result2 th.description, table.result2 td.description{
	/*width: 140px;*/
	text-align: left;	
        max-width: 260px;
}
.thumb {
	width: 150px;
}

.contactname, .contacttitle {
	color: #000;	
}

.thumb{
	width: 150px;
}


table.contactstable {
	width: 592px;
	font-size: 10px;
}

table.contactstable td {
	padding: 2px;
	vertical-align: text-top;
}

table.contactstable td.contactname {
	width: 88px;	
}

table.contactstable td.contacttitle {
	width: 88px;
}

table.contactstable td.contactcompany {
	width: 88px;	
}

table.contactstable td.contactphone {
	width: 88px;
}

table.contactstable td.contactemail {
	width: 200px;
}


div.fullyleased1 {
	text-align: center;
	/*font-size: 1.4em;*/
	font-weight: bold;
	/*margin: -10px 0 0 0;*/
}

div.fullyleased2 {
	text-align: center;
	/*font-size: 1.4em;*/
	margin: 5px 0 0 0;
}



</style>

</head>
<body>

<?php 
		if ($lang_id == 0){
			$page_text = "Page {PAGE_NUM} de {PAGE_COUNT}";
			
		}else{
			$page_text = "Page {PAGE_NUM} of {PAGE_COUNT}";
			
		}
	?>

	<script type="text/php">
		if ( isset($pdf) ) {
			$font = Font_Metrics::get_font("Arial", "bold");
			
			$pdf->page_text(542, 765, "<?php echo $page_text; ?>", $font, 10, array(0,0,0));
			
		}
	</script>
    


<script type="text/php">

	date_default_timezone_set('America/New_York'); 
	setlocale(LC_ALL, <?php echo $language['locale_string']; ?>);


			
if ( isset($pdf) ) {
	$pdf->page_script('	

			// start a new object
		
			$obj3 = $pdf->open_object();
		
			// set the font
	
			$w = $pdf->get_width();
			$h = $pdf->get_height();
		
			$pdf->image("<?php echo $language['pdflogo']; ?>", 10, 10, 592, 55);

			$font = Font_Metrics::get_font("Arial", "bold");
			
			<?php
			// HEX ENCODE POTENTIALLY FRENCH TEXT - HEADER TITLE TEXT BIG
			$str = $language['header_title_text_big'];
			$header_title_text_big_dompdf = '';
			for ($i=0;$i<strlen($str);$i++) {
				$header_title_text_big_dompdf .= sprintf('\\x%lx', ord($str[$i]));
			}
			
	
			// HEX ENCODE POTENTIALLY FRENCH TEXT - HEADER TITLE TEXT SMALL
			$str = $language['header_title_text_small'];
			$header_title_text_small_dompdf = '';
			for ($i=0;$i<strlen($str);$i++) {
				$header_title_text_small_dompdf .= sprintf('\\x%lx', ord($str[$i]));
			}

			
			// HEX ENCODE POTENTIALLY FRENCH TEXT - DATE
			$str = strftime('%B %Y');
			$datetext = '';
			for ($i=0;$i<strlen($str);$i++) {
				$datetext .= sprintf('\\x%lx', ord($str[$i]));
			}
                        
                        // HEX ENCODE POTENTIALLY FRENCH TEXT - Branding Line
			$str = $branding_name;
			$branding = '';
			for ($i=0;$i<strlen($str);$i++) {
				$branding .= sprintf('\\x%lx', ord($str[$i]));
			}
			?>

			$pagetext2 = "<?php echo $header_title_text_big_dompdf .' '. $header_title_text_small_dompdf; ?>";
//			$pagetext3 = "<?php // echo $language['page_title'] . ' | ' . $datetext; ?>";
                        $pagetext3 = "<?php echo $branding . ' | ' . $datetext; ?>";

		//	get text width
			$width = Font_Metrics::get_text_width($pagetext2, $font, 18);
			$width2 = Font_Metrics::get_text_width($pagetext3, $font, 10);

		//	Building Name
		//	$pdf->text(($w-$width-20), 75, $pagetext2, $font, 18, array(<?php echo $theme['primary_colour_dompdf']; ?>));
			$pdf->text(15, 75, $pagetext2, $font, 18, array(<?php echo $theme['primary_colour_dompdf']; ?>));
			
		// 	Broker and Date line 
			$pdf->text(($w-$width2-20), 82, $pagetext3, $font, 10, array(<?php echo $theme['primary_colour_dompdf']; ?>));

			// close the bject
			$pdf->close_object();
			$pdf->add_object($obj3);
			

	');
}
	</script>


                
   
    
<?php   
  	// decide which mechanism will drive the page
	
		$buildings_sorted = array();

	if (isset($_GET['Address'])) {
	
		$address_sorting_query_string = "SELECT building_code, sort_streetname, sort_streetnumber FROM buildings WHERE building_code IN ('" . str_replace(",", "','", $_GET['Address']) . "') and lang=" . $lang_id . " order by province asc, region asc, sort_streetname asc, sort_streetnumber asc";
	
		$address_sorting_query = mysql_query($address_sorting_query_string);


			
		// loop through results and push them in to an array (like the $addresses array we're exploding above)
			while ($sortrow = mysql_fetch_array($address_sorting_query)) {
					$buildings_sorted[] = $sortrow['building_code'];
			}

		$driver = $buildings_sorted;	

	} else {
		$driver = $building_driver;
	}

//*****************************//
// Province Name / City header //
//*****************************//
	$curr_province = "";
	$curr_region = "";
	$prov_change = 0;
	$region_change = 0;
	$geo_label = '';

    
    
    
 //if (mysql_num_rows($buildings_query) > 0){ 
	if (count($driver) > 0) {

		foreach ($driver as $building_code) {
			
			$newbuildings_query_string = "select * from buildings where lang=" . $lang_id . " and building_code ='" . $building_code . "'";
                        $newbuildings_query = mysql_query($newbuildings_query_string);
			$Building = mysql_fetch_array($newbuildings_query);
                        
                        $building_id = $Building['building_id'];
                        

            // for reporting
            $today = date("Y/m/d");
            $todaytimestamp = strtotime($today);
            // Statistics Tracking
//            $stats->trackBuildingPdfPrint($todaytimestamp, $today, $building_id, $building_code); 
	
if ($suitetype_selected == '') {
    $suitetype_selected = "All";
}

if ($availability_notbefore == '') {
    $availability_notbefore = '0';
}

if ($availability_notafter == '') {
    $availability_notafter = '2147483640';
}


//        if ($advanced_search == 1) { 
//                $suites_query_string = $queries->get_suites_query_string($lang_id, $building_code, $size_selected, $sizeFrom, $sizeTo, $suitetype_selected, "notag", $availability_notbefore, $availability_notafter, $industrial_office_space_selected, $industrial_clear_height_selected, $industrial_shipping_doors_selected, $industrial_drivein_doors_selected, $industrial_electrical_volts_selected, $industrial_electrical_amps_selected, $industrial_parking_stalls_selected, $industrial_trucktrailer_parking_selected, $industrialsearchelement, $advanced_search, $db);
//
//                $suitecount_query_string = $queries->get_suites_count_noleased($lang_id, $building_code, $size_selected, $sizeFrom, $sizeTo, $suitetype_selected, "notag", $availability_notbefore, $availability_notafter, $industrial_office_space_selected, $industrial_clear_height_selected, $industrial_shipping_doors_selected, $industrial_drivein_doors_selected, $industrial_electrical_volts_selected, $industrial_electrical_amps_selected, $industrial_parking_stalls_selected, $industrial_trucktrailer_parking_selected, $industrialsearchelement, $advanced_search, $db);
//
//        } else {
//            
//                $suites_query_string = $queries->get_suites_query_string($lang_id, $building_code, $size_selected, $sizeFrom, $sizeTo, $suitetype_selected, "notag", $availability_notbefore, $availability_notafter, $db);
//
//                $suitecount_query_string = $queries->get_suites_count_noleased($lang_id, $building_code, $size_selected, $sizeFrom, $sizeTo, $suitetype_selected, "notag", $availability_notbefore, $availability_notafter, $db);
//
//        }


        if ($advanced_search == 1) { 
                $suites_query_string_query = $queries->get_suites_query_string($lang_id, $building_code, $size_selected, $sizeFrom, $sizeTo, $suitetype_selected, "notag", $availability_notbefore, $availability_notafter, $industrial_office_space_selected, $industrial_clear_height_selected, $industrial_shipping_doors_selected, $industrial_drivein_doors_selected, $industrial_electrical_volts_selected, $industrial_electrical_amps_selected, $industrial_parking_stalls_selected, $industrial_trucktrailer_parking_selected, $industrialsearchelement, $advanced_search, $db);

                $suitecount_query_string_query = $queries->get_suites_count_noleased($lang_id, $building_code, $size_selected, $sizeFrom, $sizeTo, $suitetype_selected, "notag", $availability_notbefore, $availability_notafter, $industrial_office_space_selected, $industrial_clear_height_selected, $industrial_shipping_doors_selected, $industrial_drivein_doors_selected, $industrial_electrical_volts_selected, $industrial_electrical_amps_selected, $industrial_parking_stalls_selected, $industrial_trucktrailer_parking_selected, $industrialsearchelement, $advanced_search, $db);

        } else {
                $suites_query_string_query = $queries->get_suites_query_string($lang_id, $building_code, $size_selected, $sizeFrom, $sizeTo, $suitetype_selected, "notag", $availability_notbefore, $availability_notafter, $db);

                $suitecount_query_string_query = $queries->get_suites_count_noleased($lang_id, $building_code, $size_selected, $sizeFrom, $sizeTo, $suitetype_selected, "notag", $availability_notbefore, $availability_notafter, $db);

        }

        $suite_query_string = $suites_query_string_query[0];
        $a_params = array();
        if ($suite_query_statement = $db->prepare($suite_query_string)) {
            // carry on
        } else {
            trigger_error('Wrong SQL: ' . $suite_query_string . ' Error: ' . $db->error, E_USER_ERROR);
        }
        // prep dynamic variable query
        $param_type = '';
        $n = count($suites_query_string_query[1]);
        for($i = 0; $i < $n; $i++) {
          $param_type .= $suites_query_string_query[1][$i];
        }
         /* with call_user_func_array, array params must be passed by reference */
        $a_params[] = & $param_type;
        for($i = 0; $i < $n; $i++) {
          /* with call_user_func_array, array params must be passed by reference */
          $a_params[] = & $suites_query_string_query[2][$i];
        }

        // call_user_func_array is how we can pass dynamic quantities of variables to mysqli prepared statements.
        call_user_func_array(array($suite_query_statement, 'bind_param'), $a_params);
        $suites_query = array();
        if ($suite_query_statement->execute()){
            $result = $suite_query_statement->get_result();
            while($row = $result->fetch_array(MYSQLI_ASSOC)){ 
                $suites_query[] = $row;
            }
        } else {
            trigger_error('Could not execute preview images query statement: Error: ' . $db->error, E_ALL);
        }
        $suite_query_statement->close();
        

        
        
        
        $suitecount_query = $suitecount_query_string_query[0];
//        $suitecount_result = mysql_fetch_array($suitecount_query);
        
        $suitecount_query_string = $suitecount_query_string_query[0];
        $a_params = array();
        if ($suitecount_query_statement = $db->prepare($suitecount_query_string)) {
            // carry on
        } else {
            trigger_error('Wrong SQL: ' . $suitecount_query_string . ' Error: ' . $db->error, E_USER_ERROR);
        }
        // prep dynamic variable query
        $param_type = '';
        $n = count($suitecount_query_string_query[1]);
        for($i = 0; $i < $n; $i++) {
          $param_type .= $suitecount_query_string_query[1][$i];
        }
         /* with call_user_func_array, array params must be passed by reference */
        $a_params[] = & $param_type;
        for($i = 0; $i < $n; $i++) {
          /* with call_user_func_array, array params must be passed by reference */
          $a_params[] = & $suitecount_query_string_query[2][$i];
        }

        // call_user_func_array is how we can pass dynamic quantities of variables to mysqli prepared statements.
        call_user_func_array(array($suitecount_query_statement, 'bind_param'), $a_params);
        $suitecount_result = array();
        if ($suitecount_query_statement->execute()){
            $result = $suitecount_query_statement->get_result();
            $row = $result->fetch_array(MYSQLI_ASSOC);
            $suitecount_result = $row;
            
        } else {
            trigger_error('Could not execute suitecount query statement: Error: ' . $db->error, E_ALL);
        }
        $suitecount_query_statement->close();

        $suitecount = $suitecount_result['realsuitecount'];

	
		
		$suitetype_query_string = "select distinct suite_type from suites where lang=" . $lang_id . " and building_code ='" . $building_code . "'";
		$suitetype_query = mysql_query($suitetype_query_string) or die ('suitetype_query error: ' . mysql_error());
		$suitetypearray = array();
		while ($suitetypecheck = mysql_fetch_array($suitetype_query)) {
			array_push($suitetypearray, strtolower($suitetypecheck['suite_type']));
		}
		
//echo "suite type array: " . $suitetypearray . "<br><br>";

		$othersuitetypes_query_string = "select suite_index from suites where lang=" . $lang_id . " and building_code ='" . $building_code . "' and suite_type !='" . $suitetype_selected . "'";
		$othersuitetypes_query = mysql_query($othersuitetypes_query_string) or die ('othersuitetypes_query error: ' . mysql_error());
		$othersuitetypes_count = mysql_num_rows($othersuitetypes_query);
		
		$allsuites_query_string = "select * from suites where lang=" . $lang_id . " and building_code ='" . $building_code . "'";
		$allsuites_query = mysql_query($allsuites_query_string) or die ('allsuites_query error: ' . mysql_error());
		$allsuites_count = mysql_num_rows($allsuites_query);
		
        $suite_type_chosen = false;

  
  
  	if ($lang_id == 1) {
  		$translateretail = "Retail";
	}
	if ($lang_id == 0) {
		$translateretail = "Détail";
	}

	$type_lang_array = '';
		if ((strtolower($suitetype_selected) == "office") || (strtolower($suitetype_selected) == "bureau")) {
			$type_lang_array = array('office','bureau');
		} else if ((strtolower($suitetype_selected) == "industrial") || (strtolower($suitetype_selected) == "industriel")) {
			$type_lang_array = array('industrial','industriel');
		} else if ((strtolower($suitetype_selected) == "retail") || (strtolower($suitetype_selected) == "détail")) {
			$type_lang_array = array('retail','détail');
		} else $type_lang_array = array('All','Tous');

	$inarray = '';
	if (in_array(strtolower($Building['building_type']),$type_lang_array)) {
		$inarray = "true";
	}else{
		$inarray = "false";
	}
	
	
	$donotshow = 0;
	if ($suitecount == 0) {
		if ((($suitetype_selected != "All") && (!in_array(strtolower($Building['building_type']),$type_lang_array)) && (!in_array(strtolower($Building['building_type']),$suitetypearray))) ||  (($allsuites_count > $suitecount) && $Building['no_vacancies'] == 0)) {
//		if ((($suitetype_selected != "All") && (!in_array(strtolower($Building['building_type']),$type_lang_array)) && (!in_array(strtolower($Building['building_type']),$suitetypearray)))) {
                    $donotshow = 1;
 		}
		if (($Building['no_vacancies'] == 1) && (in_array(strtolower($Building['building_type']),$type_lang_array))) {
			$donotshow = 0;
 		} //else {

                if (($Building['no_vacancies'] == 1) && (in_array(strtolower($Building['building_type']),$type_lang_array))) {
			$donotshow = 0;
 		}

	}



	
	// conditional to show vacant buildings or not, retail buildings and normal search results.
	if (($suitecount == 0 && $show_no_vacancy == 1 ) || ($suitecount > 0 ) || ($Building['building_type'] == $translateretail) || (in_array($suitetype_selected,$suitetypearray))) {	  
            if ($donotshow != 1) {

		$building_name = $Building['building_name'];
		$building_street = $Building['street_address'];
		$js_address = '\'' . $building_street . '\','; 
		$building_city = $Building['city'];
		$building_province = $Building['province'];
		$building_province_code = $Building['province_code'];
                $building_type = $Building['building_type'];
		$building_total_space = $Building['total_space'];
		$building_measurement_unit = $Building['measurement_unit'];
		$building_desc = $Building['description'];
		$building_contact = $Building['contact_info'];
		$building_header = $Building['flyer_header'];
		$building_url = $Building['uri'];

		$building_availability = $Building['modification_date'];
		$building_add_rent_operating = $Building['add_rent_operating'];
		$building_add_rent_realty = $Building['add_rent_realty'];
		$building_add_rent_power = $Building['add_rent_power'];
		$building_add_rent_total = $Building['add_rent_total'];	
                
    $building_thumbnail_query_string = $queries->get_building_thumbnail_query_string($lang_id, $building_code);
    

    
    // process buildings query string
    $building_thumbnail__string = $building_thumbnail_query_string[0];
//    $building_thumbnail__string .=" LIMIT ".$postnumbers." OFFSET ".$offset;
    $pageCurrent = '';
//    $building_thumbnail__string = str_replace("and allb.building_name like '%none%'", "", $building_thumbnail__string);
    if ($building_thumbnail__statement = $db->prepare($building_thumbnail__string)) {
        // carry on
    } else {
        trigger_error('Wrong SQL: ' . $building_thumbnail__string . ' Error: ' . $db->error, E_USER_ERROR);
    }
    // prep dynamic variable query
    $param_type = '';
    $n = count($building_thumbnail_query_string[1]);
    for($i = 0; $i < $n; $i++) {
      $param_type .= $building_thumbnail_query_string[1][$i];
    }
    
    /* with call_user_func_array, array params must be passed by reference */
    $a_params = array();
    $a_params[] = & $param_type;

    for($i = 0; $i < $n; $i++) {
      /* with call_user_func_array, array params must be passed by reference */
      $a_params[] = & $building_thumbnail_query_string[2][$i];
    }



    // call_user_func_array is how we can pass dynamic quantities of variables to mysqli prepared statements.
    call_user_func_array(array($building_thumbnail__statement, 'bind_param'), $a_params);
    $building_thumbnail = array();
    if ($building_thumbnail__statement->execute()){
        $result = $building_thumbnail__statement->get_result();
        while($row = $result->fetch_array(MYSQLI_ASSOC)){ 
            $building_thumbnail = $row;
        }
    } else {
        trigger_error('Could not execute building_thumbnail query statement: Error: ' . $db->error, E_ALL);
    }
    $building_thumbnail__statement->close();
    
    
    
    $building_thumbnail_image_name = '';
    if (isset($building_thumbnail['file_name']) && $building_thumbnail['file_name'] != '') {
        $building_thumbnail_image_name = "images/preview_images/" . $building_thumbnail['file_name'];
    } else {
        $building_thumbnail_image_name = "images/no_thumb_700.png";
    }
    
    
		// CONTACT QUERY 
		$contacts_query_string = "select * from contacts where building_code ='" . $building_code . "' and lang=" . $lang_id . " order by contact_index asc";
		$contacts_query = mysql_query($contacts_query_string);

	
				$suites_html = "";
                $rent_html = "";
                $tax_html = "";
                $building_rent_high = 0;
                $building_rent_low = 0;
                $building_tax_high = 0;
                $building_tax_low = 0;
                $contig = 0;
				$suite_counter = 0;
		   		$suite_html = "";
				$suite_new = "";
				$suite_model = "";
				$suite_leased = "";
                                $suite_promoted = "";
				$spacer = 0;
				$cross_out = '';
                $suite_output = "";
                
                $thumb_img_src = '<img src = "'.$language['vacancy_report_website'] . '/' . $building_thumbnail_image_name.'" width="100px" height="100px">';	
                                

				//Contacts Table
				// cycle through contact query
				$contacts_html = '';
				
				while($contactrow = mysql_fetch_assoc($contacts_query)){
	
					$leasing_contact_name = $contactrow['name'];
						if ($leasing_contact_name == '') {
								$leasing_contact_name = "&nbsp;";
						}
					$leasing_contact_title = $contactrow['title'];
						if ($leasing_contact_title == '') {
								$leasing_contact_title = "&nbsp;";
						}
					$leasing_contact_company = $contactrow['company'];
						if ($leasing_contact_company == '') {
								$leasing_contact_company = "&nbsp;";
						}
					$leasing_contact_phone = $contactrow['phone'];
						if ($leasing_contact_phone == '') {
								$leasing_contact_phone = "&nbsp;";
						}
					$leasing_contact_email = $contactrow['email'];
						if ($leasing_contact_email == '') {
								$leasing_contact_email = "&nbsp;";
						}
					
					$contacts_html .= '<tr><td class="contactname">'. $leasing_contact_name .'</td>';
					$contacts_html .= '<td class="contacttitle">'. $leasing_contact_title .'</td>';
					$contacts_html .= '<td class="contactcompany">'. $leasing_contact_company .'</td>';
					$contacts_html .= '<td class="contactphone">'. $leasing_contact_phone .'</td>';
					$contacts_html .= '<td class="contactemail">'. $leasing_contact_email .'</td></tr>';
	
				}  //end contacts_query while

	$geo_label = '';
	
	if ($Building['province'] != $curr_province){
		$curr_province = $Building['province'];
		$prov_change = 1;
	}else{
		$prov_change = 0;
	}
	
	if ($Building['region'] != $curr_region){
		$curr_region = $Building['region'];
		$region_change = 1;
	}else{
		$region_change = 0;
	}
	
	if ( $prov_change == 1 || $region_change == 1){
		$geo_label = '<div class="toptoprow">' . $curr_province . ' - ' . $curr_region . '</div>';
	}



if ($geo_label != '') {
	echo $geo_label;
}



        $building_specs_query_string = "select website from building_specifications where building_code='" . $building_code . "' and lang = ".$lang_id."";
        $building_specs_query = mysql_query($building_specs_query_string);
	$building_specs = mysql_fetch_array($building_specs_query);

$building_result_div = 
				'<table class="result">
					<tr>
						<td colspan="2">';


    if ($theme['result_links_to_corp_website'] == 1) {
        $building_result_div .='<h2><a href="' . $building_specs['website'] . '">' . $building_name . '</a></h2>';
    } else {
        $building_result_div .='<h2><a href="' . $language['vacancy_report_website'] . '/building.php?building=' . $Building['building_code'] . '&lang=' . $lang . '">' . $building_name . '</a></h2>'; 
    }




$building_result_div .=
							'<div class="smallcityprov">' . $building_city . ', ' . $building_province_code .'</div>
						</td>
					</tr>
					<tr>
						<td>
                                                    <div class="thumb">' . $thumb_img_src . '</div>
						</td>';

$building_result_div .='<td>';

if ($contacts_html != '') {
	$building_result_div .= '<table class="contactstable">';
	$building_result_div .=  $contacts_html;
	$building_result_div .= '</table>';
} else {
	$building_result_div .= '&nbsp;';
}

$building_result_div .= '</td>';

$building_result_div .='</tr></table>' . "\n\r\n\r";

$building_result_div .='<table class="result2">
						<thead>
							<tr>
								<th class="icon">&nbsp;</th>';

if ($building_type == "Industrial" || $building_type == "Industriel") {
    $building_result_div .='<th class="suite">' . $language['unit_text'] . '</th>';
    $building_result_div .='<th class="area">'.$language['index_table_header_area_text'] . ' <br>'
                          . ' (' . $language['min_divisible_text'] . ' | ' . $language['contiguous_text'] . ')</th>';
    $building_result_div .='<th class="rent">' . $language['rent_text'] . '</th>';
    $building_result_div .='<th class="availability">' . $language['availability_header_text'] . '</th>';
    $building_result_div .='<th class="description">' . $language['loading_text'] . '</th>';
    $building_result_div .='<th class="description">' . $language['power_text'] . '</th>';
    $building_result_div .='<th class="description">' . $language['ceiling_text'] . '</th>';
    $building_result_div .='<th class="description">' . $language['notes_text'] . '</th>';
    $building_result_div .='</tr></thead>';  

    
} else {
    $building_result_div .='<th class="suite">' . $language['suite_text'] . '</th>';
    $building_result_div .='<th class="area">'.$language['index_table_header_area_text'] . ' <br>'
                            . ' (' . $language['min_divisible_text'] . ' | ' . $language['contiguous_text'] . ')</th>';
    $building_result_div .='<th class="rent">' . $language['rent_text'] . '</th>';
    $building_result_div .='<th class="availability">' . $language['availability_header_text'] . '</th>';
    $building_result_div .='<th class="description">' . $language['notes_text'] . '</th>';
    $building_result_div .='</tr></thead>';
}


				foreach($suites_query as $row){
					$spacer = 0;
					$suite_counter++;
//echo $lang . "<br/>";
                                        $suite_name = str_replace("“", "\"", $row['suite_name']);
                                        $suite_name = str_replace("”", "\"", $suite_name);
                                        $suite_id = $row['suite_id'];
					$suite_name_short = substr($suite_name, 0, 12);
					
					// Set up English vs French sqft formatting
					if ($lang == "en_CA") {
                                                $suite_net_rentable_area = $row['net_rentable_area'];
                                                $suite_net_rentable_area = number_format($suite_net_rentable_area);
                                                $suite_contiguous_area = $row['contiguous_area'];
                                                $suite_contiguous_area = number_format($suite_contiguous_area);
                                                $suite_min_divisible_area = $row['min_divisible_area'];
                                                $suite_min_divisible_area = number_format($suite_min_divisible_area);
                                        }
                                        if ($lang == "fr_CA") {
                                                $suite_net_rentable_area = $row['net_rentable_area'];
                                                $suite_net_rentable_area = number_format($suite_net_rentable_area, 0,',',' ');
                                                $suite_contiguous_area = $row['contiguous_area'];
                                                $suite_contiguous_area = number_format($suite_contiguous_area, 0, ',', ' ');
                                                $suite_min_divisible_area = $row['min_divisible_area'];
                                                $suite_min_divisible_area = number_format($suite_min_divisible_area, 0, ',', ' ');
                                        }

					
					$suite_description = strip_tags($row['description'],'<b><i><a><br><strong><ul><u>');
					$suite_description = str_replace('style=','',$suite_description);

					
					
			$suite_threedee_query_string = 'select presentation_link from files_3d where suite_id ="'.$suite_id.'" limit 1';
                        $suite_threedee_query = mysql_query($suite_threedee_query_string);
                        $suite_threedee = mysql_fetch_array($suite_threedee_query);
                        $suite_presentation_link = $suite_threedee['presentation_link'];		
					
			$suite_pdf_query_string = "select * from files_2d where building_code = '".$building_code."' and suite_name = '".$row['suite_name']."'";
			$suite_pdf_query = mysql_query($suite_pdf_query_string) or die ("Suite pdf query error: " . mysql_error());
			$suite_pdf = mysql_fetch_array($suite_pdf_query);
                        $suite_pdf_plan = $suite_pdf['plan_uri'];
					
					
					
					
					
					$suite_rent = doubleval(str_replace("$", "", $row['net_rent']));

					$suite_taxes = doubleval(str_replace("$", "", $row['taxes_and_operating']));
					
					if ($suite_rent > 0 && $building_rent_low == 0){
						$building_rent_low = $suite_rent;
					}
					if ($suite_taxes > 0 && $building_tax_low == 0){
						$building_tax_low = $suite_taxes;
					}
					
					if ($suite_rent > $building_rent_high){
						$building_rent_high = $suite_rent;
					}
					if ($suite_rent < $building_rent_low && doubleval($suite_rent) > 0){
						$building_rent_low = $suite_rent;
					}
					if ($suite_taxes > $building_tax_high){
						$building_tax_high = $suite_taxes;
					}
					if ($suite_taxes < $building_tax_low && doubleval($suite_taxes) > 0){
						$building_tax_low = $suite_taxes;
					}
		
		
		
		
                                if ($theme['result_links_to_corp_website'] == 1) {
                                    
                                    $suite_link = $suite_name ;
                                    
                                } else {
                                    
                                    if ($suite_contiguous_area != 0 && $suite_contiguous_area != ''){
                                        $contig = 1;
                                        $suite_link = '<a class="contiguous" href="'. $language['vacancy_report_website'] .'/suite.php?building=' . $building_code . '&suiteid=' . $suite_id . $queries->get_query_str() . '">' . addslashes($suite_name) . '</a>';
                                    }else{
                                        $suite_link = '<a href="'. $language['vacancy_report_website'] .'/suite.php?building=' . $building_code . '&suiteid=' . $suite_id . $queries->get_query_str() . '">' . $suite_name_short . '</a>';
                                    }


                                } // end if/else - if ($theme['result_links_to_corp_website'] == 1)			
                                
                                
                                
					if ($suite_rent == "0" || $suite_rent == ""){
						$rent_display = $language['negotiable_text'];
					}else{
						$rent_display = $suite_rent;
						$rent_display = money_format('%.2n',$rent_display);
					}
		


	
				$suite_new = $row['new'];
				$suite_model = $row['model'];
				$suite_leased = $row['leased'];
                                $suite_promoted = $row['promoted'];
				$cross_out = "";
				
				if ($suite_new == "true") {
					if ($lang == "en_CA"){	
						$icon = '<img src="'. $language['vacancy_report_website'] . '/images/icon-new_EN.png" />';
					}else{
						$icon = '<img src="'. $language['vacancy_report_website'] . '/images/icon-new_FR.png" />';
					}
					$cross_out = '';
					$spacer = 1;
				}
				
				if ($suite_model == "true") {
					if ($lang == "en_CA"){	
					$icon = '<img src="'. $language['vacancy_report_website'] . '/images/icon-model-suite_EN.png" />';
					}else{
					$icon = '<img src="'. $language['vacancy_report_website'] . '/images/icon-model-suite_FR.png" />';					
					}
					$cross_out = '';
					$spacer = 1;
				}
				
				if ($suite_leased == "true") {
					if ($lang == "en_CA"){						
						$icon = '<img src="'. $language['vacancy_report_website'] . '/images/icon-leased_EN.png" />';
					}else{
						$icon = '<img src="'. $language['vacancy_report_website'] . '/images/icon-leased_FR.png" />';					
					}
					$cross_out = ' style="text-decoration:line-through;"';
					$spacer = 1;
				}
                                
                                if ($suite_promoted == "true") {
					if ($lang == "en_CA"){						
						$icon = '<img src="'. $language['vacancy_report_website'] . '/images/icon-promoted_EN.png" />';
					}else{
						$icon = '<img src="'. $language['vacancy_report_website'] . '/images/icon-promoted_FR.png" />';					
					}
					$cross_out = ' style="font-style: italic;font-weight: bold;"';
					$spacer = 1;
				}
				
				if ($spacer == 0) {
					$icon = '';
					
				}


$suite_output .= '
    <tr class="'. $row['suite_id'].' '.$row['building_code'].'">
        <td class="icon"><span ' . $cross_out . '>' . $icon . '</span></td>
        <td class="suite"><span ' . $cross_out . '>' . $suite_link . '</span></td>
        <td class="area"><span ' . $cross_out . '>' . $suite_net_rentable_area . '<br />';




            if ((intval($suite_contiguous_area) != 0) && (intval($suite_min_divisible_area) != 0)) {
                $suite_output .= '(' . $suite_min_divisible_area . ' | ' . $suite_contiguous_area . ')</span></td>';
            } else if ((intval($suite_contiguous_area) == 0) && (intval($suite_min_divisible_area) != 0)) {
                $suite_output .= '(' . $suite_min_divisible_area . ')</span></td>';
            } else if ((intval($suite_contiguous_area) != 0) && (intval($suite_min_divisible_area) == 0)) {
                $suite_output .= '(' . $suite_contiguous_area . ')</span></td>';
            } else {
                $suite_output .= '</span></td>';
            }






$suite_output .='
        <td class="rent"><span ' . $cross_out . '>' . $rent_display . '</span></td>
        <td class="availability"><span ' . $cross_out . '>' . get_suite_availability($language, $row) . '</span></td>';
            
// Availability Table changes here depending on whether it's an Industrial Building or not

if ($building_type == "Industrial" || $building_type == "Industriel") {
    
    $suite_specs_query_string = "select shipping_doors_drive_in, shipping_doors_truck, available_electrical_amps, available_electrical_volts, clear_height from suites_specifications where building_id = '".$building_id."' and suite_id = '".$row['suite_id']."'";
    $suite_specs_query = mysql_query($suite_specs_query_string) or die ("Suite specs query error: " . mysql_error());
    $suite_specs = mysql_fetch_array($suite_specs_query);
    
    
    $shippingDI = $suite_specs['shipping_doors_drive_in'];
    $shippingTL = $suite_specs['shipping_doors_truck'];
    $poweramps = $suite_specs['available_electrical_amps'];
    $powervolts = $suite_specs['available_electrical_volts'];
    $ceiling = $suite_specs['clear_height'];
            $loadingoutputDI = '';
            $loadingoutputTL = '';
            $loadingoutput = '';

            if ($shippingDI != '' && $shippingDI != 0) {
                $loadingoutputDI = $shippingDI . " " . $language['drivein_loading_abbreviation'];
            } 
            if ($shippingTL != '' && $shippingTL != 0) {
                $loadingoutputTL = $shippingTL . " " . $language['truck_loading_abbreviation'];
            }
            if ($loadingoutputDI != '' && $loadingoutputTL != '') {
                $loadingoutput = $loadingoutputDI . "<br />" . $loadingoutputTL;
            } else if ($loadingoutputDI != '' && $loadingoutputTL == '') {
                $loadingoutput = $loadingoutputDI;
            } else if ($loadingoutputDI == '' && $loadingoutputTL != '') {
                $loadingoutput = $loadingoutputTL;
            } else {
                // both are empty leave a space
                $loadingoutput = '&nbsp;';
            }
    
            $poweroutputAmps = '';
            $poweroutputVolts = '';
            if ($poweramps != '' && $poweramps != 0) {
                $poweroutputAmps = $poweramps . " A";
            } 
            if ($powervolts != '' && $powervolts != 0) {
                $poweroutputVolts = $powervolts . " V";
            }
            if ($poweroutputAmps != '' && $poweroutputVolts != '') {
                $poweroutput = $poweroutputAmps . "<br />" . $poweroutputVolts;
            } else if ($poweroutputAmps != '' && $poweroutputVolts == '') {
                $poweroutput = $poweroutputAmps;
            } else if ($poweroutputAmps == '' && $poweroutputVolts != '') {
                $poweroutput = $poweroutputVolts;
            } else {
                // both are empty leave a space
                $poweroutput = "&nbsp;";
            }

    $suite_output .='<td class="loading"><span ' . $cross_out . '>' . $loadingoutput . '</span></td>';
    $suite_output .='<td class="power"><span ' . $cross_out . '>' . $poweroutput . '</span></td>';
    $suite_output .='<td class="ceiling"><span ' . $cross_out . '>' . $ceiling . '</span></td>';
    

    $suite_output .='<td class="description"><span ' . $cross_out . '>' . strip_tags(str_replace('&nbsp;', '', $suite_description)) . '</span></td>';
    $suite_output .='</tr>';
} else {
    $suite_output .='<td class="description"><span ' . $cross_out . '>' . strip_tags(str_replace('&nbsp;', '', $suite_description)) . '</span></td>';
    $suite_output .='</tr>';
}

			//}
	
				} 	// end WHILE suites_query 	?>   





	
<?php 

	//Additional Costs Table
	$hit = 0;
        if ($building_type == "Industrial" || $building_type == "Industriel") {
            $additionalcosts_html = '<tfoot><tr><td colspan="9" class="add-cost-cell">';
        } else {
	$additionalcosts_html = '<tfoot><tr><td colspan="9" class="add-cost-cell">';
        }
	if (strlen($building_add_rent_operating) > 0){
		$hit = 1;
		$additionalcosts_html .= $language['add_rent_operating_text'] . ': ' . money_format('%.2n',$building_add_rent_operating) .' '.$language['per_sqft_text'].' &nbsp; &nbsp;';
	}
	if (strlen($building_add_rent_realty) > 0){
		$hit = 1;
		$additionalcosts_html .= $language['add_rent_realty_text'] . ': ' . money_format('%.2n',$building_add_rent_realty) .' '.$language['per_sqft_text'].' &nbsp; &nbsp;';
	}
	if (strlen($building_add_rent_power) > 0){
		$hit = 1;
		if ($building_add_rent_power == "0"){
			$additionalcosts_html .= $language['add_rent_power_text'] . ': Included &nbsp; &nbsp;';
		}else{
			$additionalcosts_html .= $language['add_rent_power_text'] . ': ' . money_format('%.2n',$building_add_rent_power) .' '.$language['per_sqft_text'].' &nbsp; &nbsp;';
		}
	}
	if (strlen($building_add_rent_total) > 0){
		$hit = 1;
		$additionalcosts_html .= $language['add_rent_total_text'] . ': ' . money_format('%.2n',$building_add_rent_total) .' '.$language['per_sqft_text'].' &nbsp; &nbsp;';
	}

	$additionalcosts_html .='</td></tr></tfoot>';
	
	




$is_retail = 0;	
if ($Building['building_type'] == "Retail") {
	$is_retail = 1;	
}
if ($Building['building_type'] == "Détail") {
	$is_retail = 1;	
}


echo $building_result_div;

		


echo '<tbody>';				
if (($suitecount > 0) && ($Building['no_vacancies'] == 0)) {
//if (($suitecount > 0)) {
	// Normal building with available suites

	echo $suite_output;	
	
} 
// True no vacancy building
// Prevents 'no vacancy' results due to search parameters
elseif (($suitecount == 0) && ($Building['no_vacancies'] == 1) && ($is_retail == 0)) {
	echo '<tr><td colspan="7">';
	echo '<div class="fullyleased1">' . $language['all_leased_text']. '</div>
	<div class="fullyleased2">' . $language['no_availabilities_text'] . '</div>';
	echo "</td></tr>";		
} 
// Retail building
elseif (($suitecount == 0) && ($Building['no_vacancies'] == 0) && ($is_retail == 1)) {
	echo '<tr><td colspan="7">';
	echo '<div class="fullyleased1">' . $language['call_for_availability_text'] . '</div>';
	echo "</td></tr>";	
}
echo '</tbody>';



if ($hit==1) {
	echo $additionalcosts_html;
}  else {
    if ($building_type == "Industrial" || $building_type == "Industriel") {
	echo '<tr><td colspan="9"><hr class="resultspacer"></hr></td></tr>';
    } else {
        echo '<tr><td colspan="7"><hr class="resultspacer"></hr></td></tr>';
    }
}
		



echo '</table><!-- eo result2 -->';
?>


            


<div class="clear"></div>


<!-- eo result -->
<?php } 	// end IF donotshow 	?>

<?php } 	// end IF suites_query 	?>

<?php }     // end WHILE buildings_query ?>
<?php }     // end IF buildings_query ?>





<?php 
	
//	$disclaimer_q = mysql_query("select * from disclaimers where disclaimer_province = '" . $province_selected . "'");
        
        
        
        $disclaimer_query = array();  
        $disclaimer_query_string = "select * from disclaimers where disclaimer_province =?";
        $disclaimer_query_statement = $db->prepare($disclaimer_query_string);
        $disclaimer_query_statement->bind_param('s', mysqli_real_escape_string($db, $province_selected));
        if ($disclaimer_query_statement->execute()){
            $result = $disclaimer_query_statement->get_result();
            $row = $result->fetch_array(MYSQLI_ASSOC);
            $disclaimer_query = $row;
        }
        $disclaimer_query_statement->close();

        $disclaimer_array = array();
        
	if (count($disclaimer_query) > 0){
		$disclaimer_array = $disclaimer_query;
	?>



        <div id="footer">
            <div id="copyright">
            <strong><?php echo $disclaimer_array['disclaimer_company']; ?></strong>
            <br /><?php echo $disclaimer_array['disclaimer_line_one']; ?>
            <br /><?php echo $disclaimer_array['disclaimer_line_two']; ?>
            <br /><?php echo $disclaimer_array['disclaimer_line_three']; ?>
            </div>
            <!-- eo copyright //-->
        </div>
        <!-- eo footer //-->
  
    <?php 
	}
	?>

</body>
</html>
