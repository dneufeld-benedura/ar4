<?php
include('../classes/Queries.php');
include('../classes/SearchData.php');
include('../../objects/LanguageQuery.php');
include('../../objects/StatsPackage.php');
include('../../theme/db.php');

include("../../assets/php/krumo/class.krumo.php");


// Initiate the Queries class
$queries = new Queries();
//Initiate the Search Data class
$searchData = new SearchData();
// initiate the StatsPackage class
$stats = new StatsPackage();


$languageQuery = new LanguageQuery();
$langArray = $languageQuery->getLanguageAndID();
$lang = $langArray[0];
$lang_id = $langArray[1];


$language = array();
$language_query_string = "select * from languages_dynamic ld, languages l, languages2 l2 where ld.lang_id =? and l.lang_id =? and l2.lang_id =?";
$language_query_statement = $db->prepare($language_query_string);
$language_query_statement->bind_param('iii', mysqli_real_escape_string($db, $lang_id), mysqli_real_escape_string($db, $lang_id), mysqli_real_escape_string($db, $lang_id));
if ($language_query_statement->execute()) {
    $result = $language_query_statement->get_result();
    while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
        $language = $row;
    }
}
$language_query_statement->close();


// Theme Query
$theme_query = mysql_query("select * from theme");
$theme = mysql_fetch_array($theme_query);


function get_suite_availability($language, $row) {
    $suite_availability_raw = $row['availability'];
    $suite_availability = $language['immediately_building'];
    $suite_notice_period = $row['notice_period'];

    if ($suite_notice_period != "no") {
        switch ($suite_notice_period) {
            case "30d":
                $suite_availability = $language['30days_text'];
                break;
            case "60d":
                $suite_availability = $language['60days_text'];
                break;
            case "90d":
                $suite_availability = $language['90days_text'];
                break;
            case "120d":
                $suite_availability = $language['120days_text'];
                break;
        }
    } else if ($suite_availability_raw != "" && $suite_availability_raw >= time()) {
        $suite_availability = strftime('%b. %Y', $suite_availability_raw);
    } else {
        // no availability nothin
    }
    return $suite_availability;
}

//setting locale for time, date, money, etc
setlocale(LC_ALL, $language['locale_string']);
date_default_timezone_set('America/New_York');



//krumo($_REQUEST['suites']);
$faveSuites =$_REQUEST['suites'];

//if (isset($_REQUEST['suites'])) {
//    $faveSuites = explode(',', $_REQUEST['suites']);
//} else {
//    $building_codes_narrow = '';
//    $buildings_query_string_query = $queries->getBuildingsQueryString($size_selected, $sizeFrom, $sizeTo, $suitetype_selected, $buildingtype_selected, $province_selected, $region_selected, $sub_region_selected, $building_codes_narrow, $lang_id, $availability_notafter, $availability_notbefore, $building_name_sterm, $show_no_vacancy, $industrial_office_space_selected, $industrial_clear_height_selected, $industrial_shipping_doors_selected, $industrial_drivein_doors_selected, $industrial_electrical_volts_selected, $industrial_electrical_amps_selected, $industrial_parking_stalls_selected, $industrial_trucktrailer_parking_selected, $industrialsearchelement, $advanced_search, $db);
////$buildings_query = mysql_query($buildings_query_string);
//    // process buildings query string
//    $buildings_query_string = $buildings_query_string_query[0];
////    $buildings_query_string .=" LIMIT ".$postnumbers." OFFSET ".$offset;
//    $pageCurrent = '';
////    $buildings_query_string = str_replace("and allb.building_name like '%none%'", "", $buildings_query_string);
//    if ($buildings_query_statement = $db->prepare($buildings_query_string)) {
//        // carry on
//    } else {
//        trigger_error('Wrong SQL: ' . $buildings_query_string . ' Error: ' . $db->error, E_USER_ERROR);
//    }
//    // prep dynamic variable query
//    $param_type = '';
//    $n = count($buildings_query_string_query[1]);
//    for ($i = 0; $i < $n; $i++) {
//        $param_type .= $buildings_query_string_query[1][$i];
//    }
//    // Add in 2 more string fields for $postnumbers and $offset for infinite scrolling / loading
//    $param_type .= "ss";
//
//    /* with call_user_func_array, array params must be passed by reference */
//    $a_params[] = & $param_type;
//
//    for ($i = 0; $i < $n; $i++) {
//        /* with call_user_func_array, array params must be passed by reference */
//        $a_params[] = & $buildings_query_string_query[2][$i];
//    }
//    // Add in $postnumbers and $offset for infinite scrolling / loading
//    $postnumbers = 999;
//    $offset = 0;
//    $a_params[] = & $postnumbers;
//    $a_params[] = & $offset;
//
//    // call_user_func_array is how we can pass dynamic quantities of variables to mysqli prepared statements.
//    call_user_func_array(array($buildings_query_statement, 'bind_param'), $a_params);
//    $buildings_query = array();
//    if ($buildings_query_statement->execute()) {
//        $result = $buildings_query_statement->get_result();
//        while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
//            $buildings_query[] = $row;
//        }
//    } else {
//        trigger_error('Could not execute preview images query statement: Error: ' . $db->error, E_ALL);
//    }
//    $buildings_query_statement->close();
//
//
//    $building_driver = array();
//
//    // loop through results and push them in to an array 
////		while ($row = mysql_fetch_array($buildings_query)) {
////				$building_driver[] = $row['building_code'];
////		}
//    foreach ($buildings_query as $row) {
//        $building_driver[] = $row['building_code'];
//    }
//}


//// Branding Query
//$branding_name = '';
//
//$branding_query_string = "Select * from branding where region = '" . $province_selected . "'";
//$branding_query = mysql_query($branding_query_string) or die("branding_query error: " . mysql_error());
//$branding = mysql_fetch_array($branding_query);
//$branding_count = mysql_num_rows($branding_query);
//if ($branding_count == 0) {
//    $branding_name = $language['page_title'];
//} else {
//    $branding_name = $branding['company_name'];
//}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><?php echo $language['page_title']; ?></title>
        <link rel="stylesheet" type="text/css" href="../../css/reset.css" />

        <style type="text/css">



            div.building-information h3 {
                margin-top: -4px;
                padding-top: 0px;
            }


            table.portrait td.right {
                margin-top: 0px;
                padding-top: 0px;
                vertical-align: top;
            }



            table.buildinginfopdf,  table.buildinginfopdf tr,  table.buildinginfopdf td {
                padding: 0;
                margin: 0;
            }


            /*COLOURED CSS ELEMENTS*/
            /*---------------------*/
            /*---------------------*/
            /*---------------------*/

            #header {
                width: 760px;
                background: <?php echo $theme['primary_colour_dompdf_hex']; ?>;
                margin: 0 0 5px 0;
                padding: 0;
                color: #fff;
                visibility: hidden;
            }

            .header-border {
                background: <?php echo $theme['secondary_colour_dompdf_hex']; ?>;	
            }

            table#sub-header h2 {
                font-size: 1.2em;
                color: <?php echo $theme['primary_colour_dompdf_hex']; ?>;
                text-align: right;
                text-transform: uppercase;
            }


<?php
$tertiaryColour = '';
if ($theme['fourth_colour_hex'] != '') {
    $tertiaryColour = $theme['fourth_colour_hex'];
} else {
    $tertiaryColour = $theme['secondary_colour_dompdf_hex'];
};
?>


            h3 {
                font-size: 1.0em;
                text-transform: uppercase;
                border-bottom: 2px solid <?php echo $tertiaryColour; ?>;
                margin-bottom: 10px;
                color: <?php echo $theme['primary_colour_dompdf_hex']; ?>;
                text-align: left;
            }

            table#availabilities-heading h3 {
                font-size: 1.2em;
                text-transform: uppercase;
                border-bottom: 2px solid <?php echo $tertiaryColour; ?>;
                margin-bottom: 10px;
                color: <?php echo $theme['primary_colour_dompdf_hex']; ?>;
            }

            table#additional-costs {
                width: 760px;
                border-top: 1px solid <?php echo $tertiaryColour; ?>;
                border-bottom: 1px solid <?php echo $tertiaryColour; ?>;
                font-size: 1.2em;	
                margin-bottom: 20px;
                /*page-break-after: auto;*/
            }

            .result a,
            .result2 a,
            .availabilities a {
                text-decoration: none;
                color: <?php echo $theme['primary_colour_dompdf_hex']; ?>;
            }

<?php
if ($theme['contiguous_text_colour'] != '') {
    $contiguous_text_color = $theme['contiguous_text_colour'];
} else {
    $contiguous_text_color = $theme['fontcolor'];
}
?>

            span.contiguous {
                color: <?php echo $contiguous_text_color; ?>;    
            }

            /*---------------------*/
            /*---------------------*/
            /* END COLOUR ELEMENTS */
            /*---------------------*/
            /*---------------------*/
            /*---------------------*/
            /*---------------------*/
            /*---------------------*/







            @page {
                margin: 160px 30px 60px 30px;
                padding: 0;	
            }

            body {
                margin: 0;
                padding: 0;
                font-family: Arial, sans-serif;
                color: #555555;
                font-size: 10px;
            }



            

            #breakit {
                page-break-before: always;
            }

            

            /* AR4 CSS */
            
            .fs_favourite-details-header {
                -ms-flex-item-align: center!important;
                align-self: center!important;
                font-size: 14px;
            }
            
            .fs_favourites-carousel {
                
            }
            
            .fs_user-favourites-container {
                
            }
            
            .fs_favourites-headings {
                float: left;
                max-width: 150px;
                padding-top: 180px;
                font-weight:700;
            }
            
            .fs_favourite {
                float: left;
                max-width: 200px;
            }

            
            ul {
                list-style-type: none;
            }
            
            
            .fs_favourite-headings-list,
            .fs_favourite-headings-other-costs {
                list-style: none;
                padding: 0;
                font-size: 14px;
                margin-bottom: 20px;
            }
            
            .fs_favourite-headings-list li,
            .fs_favourite-headings-other-costs li {
                padding: 3px 5px;
            }
            
            .fs_favourite-headings-list li:nth-child(odd),
            .fs_favourite-headings-other-costs li {
                background-color: #f4f5f7;
            }

/*            .fs_favourite-headings-list li:first-child {
                min-height: 36px;
                background-color: transparent;
            }*/

            .fs_page-search .fs_favourite-headings-list li:first-child {
                background-color: #fff;
            }
            
            .fs_favourite-details-list,
            .fs_favourite-details-other-costs {
                list-style: none;
                padding: 0;
                font-size: 14px;
                /*font-size: 0.875rem;*/
                margin-bottom: 0;
            }

            .fs_favourite-details-list {
                margin-bottom: 20px;
            }
            
            .fs_favourite-details-list li,
            .fs_favourite-details-other-costs li {
                padding: 3px 0px;
            }
            
            .fs_favourite-details-list li:first-child {
                font-weight: 500;
                color: #000;
                background-color: transparent;
            }
            
            .fs_page-search .fs_favourite-details-list li:first-child {
                background-color: #fff;
            }
            
            .fs_favourite-details-list li:nth-child(odd),
            .fs_favourite-details-other-costs li {
                background-color: #f4f5f7;
            }
            
            
            .fs_favourite-details-list .fs_icon-clear {
                color: #2baf5f;
            }

            .fs_favourite-details-list .fs_icon-done {
                color: #db1a7a;
            }

            .fs_favourite-details-list .fs_icon-clear,
            .fs_favourite-details-list .fs_icon-done {
                /*font-size: 0.875rem;*/
            }


            .faveThumb {
                width: 180px;
                /*height: 40px
            }
            
            .fs_favourite-header {
                /*margin-bottom: 20px;*/
                font-size: 10px;
            }
            
            H5 a {
                text-decoration: none;
                color: <?php echo $theme['primary_colour_dompdf_hex']; ?>;
            }
            
            .suiteType {
                text-transform: lowercase;
            }
            
        </style>

    </head>

    
    
    
    
<?php
$industrialPresent = 0;
foreach ($faveSuites as $suiteID) {
    $industrial_test_query = array();  
        $industrial_test_query_string = "SELECT suite_type FROM suites WHERE suite_id = ? AND lang = ?";
        if ($industrial_test_query_statement = $db->prepare($industrial_test_query_string)) {
            // carry on
        } else {
            trigger_error('Wrong SQL: ' . $industrial_test_query_string . ' Error: ' . $db->error, E_USER_ERROR);
        }
    $industrial_test_query_statement->bind_param('si', mysqli_real_escape_string($db, $suiteID), mysqli_real_escape_string($db, $lang_id));
    if ($industrial_test_query_statement->execute()){
            $result = $industrial_test_query_statement->get_result();
            while($row = $result->fetch_array(MYSQLI_ASSOC)){ 
                if (strcasecmp($row['suite_type'], "Industrial") == 0 || strcasecmp($row['suite_type'], "Industriel") == 0) {
                    $industrialPresent++;
                }
            }
    }  else {
            trigger_error('Wrong SQL: ' . $industrial_test_query_string . ' Error: ' . $db->error, E_USER_ERROR);
    }
    $industrial_test_query_statement->close();
    
    
}

//ENORMOUS FOREACH THAT WRAPS THE ENTIRE PAGE	
foreach ($faveSuites as $suite_favourited) {
 
    
    
        $suites_query = array();  
        $suites_query_string = "SELECT DISTINCT s.building_code, s.suite_id, s.suite_name, s.suite_type, s.net_rentable_area, s.contiguous_area, s.min_divisible_area, s.availability, s.notice_period, s.net_rent, ss.shipping_doors_drive_in, ss.shipping_doors_truck, ss.available_electrical_volts, ss.available_electrical_amps, ss.clear_height, sb.building_name, sb.street_address, sb.city, sb.province, sbs.costs_add_rent_total, sbs.costs_operating, sbs.costs_realty FROM suites s, suites_specifications ss, buildings sb, building_specifications sbs WHERE s.suite_id = ? AND ss.suite_id = s.suite_id AND s.building_code = sb.building_code AND sbs.building_code = s.building_code AND s.lang = ? AND sb.lang=s.lang AND sbs.lang=s.lang";
        if ($suites_query_statement = $db->prepare($suites_query_string)) {
            // carry on
        } else {
            trigger_error('Wrong SQL: ' . $suites_query_string . ' Error: ' . $db->error, E_USER_ERROR);
        }
        
        $suites_query_statement->bind_param('si', mysqli_real_escape_string($db, $suite_favourited), mysqli_real_escape_string($db, $lang_id));
        if ($suites_query_statement->execute()){
            $result = $suites_query_statement->get_result();
            while($row = $result->fetch_array(MYSQLI_ASSOC)){ 
                $suites_query[] = $row;
            }
        } else {
            trigger_error('Wrong SQL: ' . $suites_query_string . ' Error: ' . $db->error, E_USER_ERROR);
        }
        $suites_query_statement->close();

//krumo($suites_query[0]);
        

        
// Set up English vs French number formatting
$suite_net_rentable_area = $languageQuery->suite_net_rentable_area($suites_query[0], $lang);
$suite_contiguous_area = $languageQuery->suite_contiguous_area($suites_query[0], $lang);
$suite_contiguous_area_formatted = $suite_contiguous_area;
$suite_min_divisible_area = $queries->get_suite_min_divisible($lang, $suites_query[0]);
$suite_availability = $queries->get_suite_availability($language, $suites_query[0]);
$suite_net_rent = '';
if ($suites_query[0]['net_rent'] == "0" || $suites_query[0]['net_rent'] == "") {
    $suite_net_rent = $language['negotiable_text'];
} else {
    $suite_net_rent = money_format("%.2n", $suites_query[0]['net_rent']);
}

//$suite_contiguous_area_formatted = '';
//if ($lang == "en_CA") {
//    $suite_contiguous_area_formatted = number_format($suite_contiguous_area);
//}
//if ($lang == "fr_CA") {
//    $suite_contiguous_area_formatted = number_format($suite_contiguous_area, 0, ',', ' ');
//}

$areaOutput = $suite_net_rentable_area ;

if ((intval($suite_contiguous_area) != 0) && (intval($suite_min_divisible_area) != 0)) {
    $areaOutput .= ' (' . $suite_min_divisible_area . ' | ' . $suite_contiguous_area_formatted . ')';
} else if ((intval($suite_contiguous_area) == 0) && (intval($suite_min_divisible_area) != 0)) {
    $areaOutput .= ' (' . $suite_min_divisible_area . ')';
} else if ((intval($suite_contiguous_area) != 0) && (intval($suite_min_divisible_area) == 0)) {
    $areaOutput .= ' (' . $suite_contiguous_area_formatted . ')';
}


if (($suites_query[0]['costs_operating '] == 0) || ($suites_query[0]['costs_operating '] == '')) {
    $building_add_rent_operating = 'n/a';
} else {
    $building_add_rent_operating = (money_format('%.2n', (double) $suites_query[0]['costs_operating ']));
}

if (($suites_query[0]['costs_realty '] == 0) || ($suites_query[0]['costs_realty '] == '')) {
    $building_add_rent_realty = 'n/a';
} else {
    $building_add_rent_realty = money_format('%.2n', (double) $suites_query[0]['costs_realty ']);
}

//if (($suites_query[0]['costs_power'] == 0) || ($suites_query[0]['costs_power'] == '')) {
//    $building_add_rent_power = 'n/a';
//} else {
//    $building_add_rent_power = money_format('%.2n', (double) $suites_query[0]['costs_power']);
//}

if (($suites_query[0]['costs_add_rent_total'] == 0) || ($suites_query[0]['costs_add_rent_total'] == '')) {
    $building_add_rent_total = 'n/a';
} else {
    $building_add_rent_total = money_format('%.2n', (double) $suites_query[0]['costs_add_rent_total']);
}

$loadingOut = '';
 if ($suites_query[0]['shipping_doors_drive_in'] === 'n/a' || $suites_query[0]['shipping_doors_drive_in'] === 0) { 
    $loadingOut = "n/a" ;
 } else {
    $loadingOut = $suites_query[0]['shipping_doors_drive_in'] . " D/I" ;
   }
 if ($suites_query[0]['shipping_doors_truck'] === 'n/a' || $suites_query[0]['shipping_doors_truck'] === 0) { 
    $loadingOut .= " / n/a" ;
 } else {
    $loadingOut .= " / " . $suites_query[0]['shipping_doors_truck'] . " DK" ;
    }

   
$elecOut = '';
 if ($suites_query[0]['available_electrical_amps'] === 'n/a' || $suites_query[0]['available_electrical_amps'] === 0) { 
    $elecOut = "n/a"; 
 } else {
    $elecOut = $suites_query[0]['available_electrical_amps'] . "a"; 
   }
if ($suites_query[0]['available_electrical_volts'] === 'n/a' || $suites_query[0]['available_electrical_volts'] === 0) { 
    $elecOut .= " / n/a" ;
 } else {
    $elecOut .= " / " . $suites_query[0]['available_electrical_volts'] . "v" ;
   }

$building_thumbnail_query_string = $queries->get_building_thumbnail_query_string($lang_id, $suites_query[0]['building_code']);
//    $building_thumbnail_query = mysql_query($building_thumbnail_query_string) or die ("building thumbnail image error: " . mysql_error());
//    $building_thumbnail = mysql_fetch_array($building_thumbnail_query);
// process buildings query string
$building_thumbnail__string = $building_thumbnail_query_string[0];
//    $building_thumbnail__string .=" LIMIT ".$postnumbers." OFFSET ".$offset;
$pageCurrent = '';
//    $building_thumbnail__string = str_replace("and allb.building_name like '%none%'", "", $building_thumbnail__string);
if ($building_thumbnail__statement = $db->prepare($building_thumbnail__string)) {
    // carry on
} else {
    trigger_error('Wrong SQL: ' . $building_thumbnail__string . ' Error: ' . $db->error, E_USER_ERROR);
}
// prep dynamic variable query
$param_type = '';
$n = count($building_thumbnail_query_string[1]);
for ($i = 0; $i < $n; $i++) {
    $param_type .= $building_thumbnail_query_string[1][$i];
}
$a_params = array();
//krumo($a_params);
/* with call_user_func_array, array params must be passed by reference */
$a_params[] = & $param_type;

for ($i = 0; $i < $n; $i++) {
    /* with call_user_func_array, array params must be passed by reference */
    $a_params[] = & $building_thumbnail_query_string[2][$i];
}

//krumo($a_params);
// call_user_func_array is how we can pass dynamic quantities of variables to mysqli prepared statements.
call_user_func_array(array($building_thumbnail__statement, 'bind_param'), $a_params);
$building_thumbnail = array();
if ($building_thumbnail__statement->execute()) {
    $result = $building_thumbnail__statement->get_result();
    while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
        $building_thumbnail = $row;
    }
} else {
    trigger_error('Could not execute building_thumbnail query statement: Error: ' . $db->error, E_ALL);
}
$building_thumbnail__statement->close();


$building_thumbnail_count = 0;
$building_thumbnail_image_name = '';
$building_thumbnail_thumb_name = '';
if (isset($building_thumbnail['file_name']) && $building_thumbnail['file_name'] != '') {
    $building_thumbnail_image_name = "images/preview_images/" . $building_thumbnail['file_name'];
    $building_thumbnail_image_name_grid = str_replace('.jpg', '_gridthumb.jpg', $building_thumbnail_image_name);
    $building_thumbnail_image_name_list = str_replace('.jpg', '_listthumb.jpg', $building_thumbnail_image_name);                                        
} else {
    $building_thumbnail_image_name = "images/no_thumb_200.png";
    $building_thumbnail_image_name_grid = "images/no_thumb_200.png";
    $building_thumbnail_image_name_list = "images/no_thumb_100.png";
}

$faveHTML .= '
            <div data-suite-id="'.$suite_favourited.'" class="fs_favourite fs_favourite-'.$suite_favourited.' ui-sortable-handle">
                <div class="fs_favourite-header row">
                    <div class="col fs_favourite-thumbnail">
                        <a href="suite.php?building='.$suites_query[0]['building_code'].'&suiteid='.$suites_query[0]['suite_id'].'&lang='.$lang.'"><img src="'. $language['vacancy_report_website'] .'/'.$building_thumbnail_image_name_grid.'" alt="thumbnail" class="faveThumb"></a>
                    </div>
                    <div class="col d-lg-none fs_favourite-details-header">
                        <h5 class="fs_favourite-title"><a href="suite.php?building='.$suites_query[0]['building_code'].'&suiteid='.$suites_query[0]['suite_id'].'&lang='.$lang.'">'.$suites_query[0]['building_name'].'</a></h5>
                        <h6 class="fs_favourite-address">'.$suites_query[0]['street_address'].',<br/> '.$suites_query[0]['city'].', '.$suites_query[0]['province'].'</h6>
                    </div>
                </div>
                <ul class="fs_favourite-details-list">
                    
                    <li class="row">
                        <div class="col-6 col-lg-12">'.$suites_query[0]['suite_name'].'</div>
                    </li>
                    <li class="row">
                        <div class="col-6 col-lg-12">'.ucfirst(strtolower($suites_query[0]['suite_type'])).'</div>
                    </li>
                    <li class="row">
                        <div class="col-6 col-lg-12">'.$areaOutput.'</div>
                    </li>
                    <li class="row">
                        <div class="col-6 col-lg-12">'.$suite_net_rent.'</div>
                    </li>
                    <li class="row">
                        <div class="col-6 col-lg-12">'.$suite_availability.'</div>
                    </li>';

            if ($industrialPresent > 0) {
                    $faveHTML .= '
                        <li class="row">
                        <div class="col-6 col-lg-12">'.$loadingOut.'</div>
                    </li>
                    <li class="row">
                        <div class="col-6 col-lg-12">'.$elecOut.'</div>
                    </li>
                    <li class="row">
                        <div class="col-6 col-lg-12">'.($suites_query[0]['clear_height'] != '' ? $suites_query[0]['clear_height'] : "n/a").'</div>
                    </li>';
            }
            
            $faveHTML .= '                
                </ul>
                <ul class="fs_favourite-details-other-costs">
                    <li class="row">
                        <div class="col-6 col-lg-12"> '.$building_add_rent_total.'</div>
                    </li>
                    <li class="row">
                        <div class="col-6 col-lg-12"> '.$building_add_rent_operating.'</div>
                    </li>
                    <li class="row">
                        <div class="col-6 col-lg-12"> '.$building_add_rent_realty.'</div>
                    </li>
                </ul>
            </div>';
    
    
    
    
    
    
    
    
        
    
    
    
    
    
    
} // end foreach ($driver as $suites_wheatever)
?>
        <body class="fs_page-favourites">
            
<?php 
		if ($lang_id == 0){
			$page_text = "Page {PAGE_NUM} de {PAGE_COUNT}";
			
		}else{
			$page_text = "Page {PAGE_NUM} of {PAGE_COUNT}";
			
		}
	?>

	<script type="text/php">
		if ( isset($pdf) ) {
			$font = Font_Metrics::get_font("Arial", "bold");
			
			$pdf->page_text(542, 765, "<?php echo $page_text; ?>", $font, 10, array(0,0,0));
			
		}
	</script>
    


<script type="text/php">

	date_default_timezone_set('America/New_York'); 
	setlocale(LC_ALL, <?php echo $language['locale_string']; ?>);


			
                if ( isset($pdf) ) {
                        $pdf->page_script('	

			// start a new object
		
			$obj3 = $pdf->open_object();
		
			// set the font
	
			$w = $pdf->get_width();
			$h = $pdf->get_height();
		
			$pdf->image("<?php echo $language['pdflogo']; ?>", 10, 10, 592, 55);

			$font = Font_Metrics::get_font("Arial", "bold");
			
			<?php
			// HEX ENCODE POTENTIALLY FRENCH TEXT - HEADER TITLE TEXT BIG
//			$str = $language['header_title_text_big'];
//			$header_title_text_big_dompdf = '';
//			for ($i=0;$i<strlen($str);$i++) {
//				$header_title_text_big_dompdf .= sprintf('\\x%lx', ord($str[$i]));
//			}
			
	
			// HEX ENCODE POTENTIALLY FRENCH TEXT - HEADER TITLE TEXT SMALL
//			$str = $language['header_title_text_small'];
//			$header_title_text_small_dompdf = '';
//			for ($i=0;$i<strlen($str);$i++) {
//				$header_title_text_small_dompdf .= sprintf('\\x%lx', ord($str[$i]));
//			}

			
			// HEX ENCODE POTENTIALLY FRENCH TEXT - DATE
//			$str = strftime('%B %Y');
//			$datetext = '';
//			for ($i=0;$i<strlen($str);$i++) {
//				$datetext .= sprintf('\\x%lx', ord($str[$i]));
//			}
                        
                        // HEX ENCODE POTENTIALLY FRENCH TEXT - Branding Line
//			$str = $branding_name;
//			$branding = '';
//			for ($i=0;$i<strlen($str);$i++) {
//				$branding .= sprintf('\\x%lx', ord($str[$i]));
//			}
			?>

//			$pagetext2 = "<?php // echo $header_title_text_big_dompdf .' '. $header_title_text_small_dompdf; ?>";
//			$pagetext3 = "<?php // echo $language['page_title'] . ' | ' . $datetext; ?>";
//                        $pagetext3 = "<?php // echo $branding . ' | ' . $datetext; ?>";
//
//		//	get text width
//			$width = Font_Metrics::get_text_width($pagetext2, $font, 18);
//			$width2 = Font_Metrics::get_text_width($pagetext3, $font, 10);
//
//		//	Building Name
//		//	$pdf->text(($w-$width-20), 75, $pagetext2, $font, 18, array(<?php // echo $theme['primary_colour_dompdf']; ?>));
//			$pdf->text(15, 75, $pagetext2, $font, 18, array(<?php // echo $theme['primary_colour_dompdf']; ?>));
//			
//		// 	Broker and Date line 
//			$pdf->text(($w-$width2-20), 82, $pagetext3, $font, 10, array(<?php // echo $theme['primary_colour_dompdf']; ?>));

			// close the bject
			$pdf->close_object();
			$pdf->add_object($obj3);
			

	');
}
	</script>
            
            
            
            
            
            
            
                    <div class="fs_user-favourites">
                        <div class="fs_user-favourites-container">
                            <div class="row no-gutters">
                                <div class="fs_favourites-headings col">
                                    <ul class="fs_favourite-headings-list">
                                        <li>Suite</li>
                                        <li><?php echo $language['type_text']; ?></li>
                                        <li><?php echo $language['size_text_lowercase']; ?>(<?php echo $language['building_information_sq_ft_trans']; ?>)</li>
                                        <li><?php echo $language['price_text']; ?> (<?php echo $language['per_sqft_text']; ?>)</li>
                                        <li><?php echo $language['index_table_header_availability_text']; ?></li>
                                    <?php if ($industrialPresent > 0) { ?>
                                        <li><?php echo $language['loading_text']; ?></li>
                                        <li><?php echo $language['power_text']; ?></li>
                                        <li><?php echo $language['ceiling_text']; ?></li>
                                    <?php } ?>
                                    </ul>
                                    <ul class="fs_favourite-headings-other-costs">
                                        <li><?php echo $language['additional_rent_text']; ?></li>
                                        <li><?php echo $language['operating_costs_text']; ?></li>
                                        <li><?php echo $language['realty_tax_text']; ?></li>
                                    </ul>
                                </div>
                                <div class="col-12 col-lg-10">
                                    <div id="fs_favourites-carousel" class="owl-carousel owl-theme">
                                        
                                        <?php echo $faveHTML; ?>
                                    </div>
                                </div>    
                            </div>
                        </div>
                    </div>
        </body>
    
    
    
    
    
    
    
    
    
    
    
    
    

</html>

