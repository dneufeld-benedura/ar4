<?php

/**
 * This method is for the mysql query purposes only.
 * @author RationalRoot
 * @version AR2.0
 * @since December 2014
 */

include('MainQueryBuilder.php');


class Queries {

    private static $lang_id;
    private static $timestamp;

    /**
     * A constructor for setting-up the Query class
     * @param type $langID
     * @param type $time
     */
    public final function __construct($langID, $time) {
        self::$lang_id = $langID;
        self::$timestamp = $time;
    }

    /**
     * Gets the $_SERVER['QUERY_STRING']
     * @return string
     */
    public function get_query_str() {
        if ($_SERVER['QUERY_STRING'] == "") {
            return "";
        }
        return '&' . $_SERVER['QUERY_STRING'];
    }

    private function getRemovedSuitesDataString($Building, $code_suite) {
             $query_string = "SELECT * FROM suites_diff WHERE suite_id NOT IN (SELECT suite_id FROM suites_diff WHERE record_source = \"today\")
                    AND    building_id = '" . $Building['building_id'] . "'
                    AND   suite_id = '" . $code_suite . "';";
             return $query_string;
    }    

    /**
     * Gets subsription query string for the unique id $uniqueid
     * @param type $uniqueid
     * @return string
     */
    private function getQueryString($uniqueid) {
        $bu_query_string = "select * from bu_subscriptions where unique_id = '" . $uniqueid . "' and active='1'";
        return $bu_query_string;
    }

    /**
     * Gets the language query string from the languages_dynamic table
     * @return string
     */
    private function getLanguageQueryString() {
        $language_query_string = 'select * from languages_dynamic ld, languages l where ld.lang_id = ' . self::$lang_id . ' and l.lang_id = ' . self::$lang_id;
        return $language_query_string;
    }
    
    /**
     * Gets the query string for the buildings table joined with the suites, and includes the availability data.
     * @param type $selectedData
     * @return string
     */
   private function getBuildingsQueryString($selectedData) {
       $mainQueryBuilder = new MainQueryBuilder($selectedData, self::$lang_id);
       $filteredQueryString = $mainQueryBuilder->getBuildingUpdateQueryString();
       return $filteredQueryString;
    }

    /**
     * Gets the query string for counting the suites per building_code
     * @param type $Building
     * @return string
     */
    private function getSuiteCountQueryString($Building) {
        $suite_count_query_string = "select * from suites where building_code = '" . $Building['building_code'] . "' and lang = '" . self::$lang_id . "'";
        return $suite_count_query_string;
    }

    /**
     * Gets the query string for contacts, per building_code
     * @param type $Building
     * @return string
     */
    private function getContactsQueryString($Building) {
        $contacts_query_string = "Select * from contacts where building_code = '" . $Building['building_code'] . "' and type ='LEASING' and lang ='" . self::$lang_id . "' order by priority asc";
        return $contacts_query_string;
    }

    /**
     * Gets the query string for the suites data per building_code
     * @param type $Building
     * @param type $code_suite
     * @return string
     */
    public function getSuitesQueryString($Building, $code_suite) {
        $suites_query_string = " SELECT * FROM ((SELECT sd.suite_id,
                               sd.lang,
                               sd.building_code,
                               s.suite_name,
                               sd.net_rentable_area,
                               sd.contiguous_area,
                               sd.availability,
                               sd.add_rent_total,
                               s.pdf_plan,
                               sd.record_date,
                               sd.record_source,
                               s.description
                        FROM   suites_diff sd,
                               suites s
                        WHERE  sd.building_code = '" . $Building['building_code'] . "'
                        AND    s.building_code = '" . $Building['building_code'] . "'
                        AND    sd.lang = '" . self::$lang_id . "'
                        AND    s.lang = '" . self::$lang_id . "'
                        AND    sd.suite_id = '" . $code_suite . "'
                        AND    s.suite_id = '" . $code_suite . "'
                        AND    sd.record_source = 'today'
                        AND    s.record_source = 'today' ) 
                        UNION 
                        
                              (SELECT DISTINCT sd.suite_id,
                               sd.lang,
                               sd.building_code,
                               s.suite_name,
                               sd.net_rentable_area,
                               sd.contiguous_area,
                               sd.availability,
                               sd.add_rent_total,
                               s.pdf_plan,
                               sd.record_date,
                               sd.record_source,
                               s.description
                        FROM   suites_diff sd,
                               suites_yesterday s
                        WHERE  sd.building_code = '" . $Building['building_code'] . "'
                        AND    s.building_code = '" . $Building['building_code'] . "'
                        AND    sd.lang = '" . self::$lang_id . "'
                        AND    s.lang = '" . self::$lang_id . "'
                        AND    sd.suite_id = '" . $code_suite . "'
                        AND    s.suite_id = '" . $code_suite . "'
                        AND    sd.record_source = 'yesterday'
                          )) u1 GROUP BY u1.suite_id;";
        return $suites_query_string;
    }

    /**
     * Gets the query string for yesterday's suites per suite_id
     * @param type $Suites
     * @return string
     */
    private function getYesterdaySuitesQueryString($Suites) {
        $yesterday_suites_query_string = "SELECT * FROM suites_diff sd WHERE sd.lang = '" . self::$lang_id . "' and sd.suite_id = '" . $Suites['suite_id'] . "' and sd.record_source = 'yesterday'";
        return $yesterday_suites_query_string;
    }

    /**
     * Gets the net rentable query string for a suite
     * @param type $Building
     * @param type $code_suite
     * @return string
     */
    private function getSuiteNetRentableQueryString($Building, $code_suite) {
        $suite_net_rentable_query_string = "	
             SELECT suites.suite_id,
                   suites.suite_name,
                   suites.net_rentable_area
            FROM   suites
            JOIN   suites_yesterday
            ON     suites.suite_id = suites_yesterday.suite_id
            WHERE  suites.net_rentable_area <> suites_yesterday.net_rentable_area
            AND    suites_yesterday.building_id = '" . $Building['building_id'] . "'
            AND    suites_yesterday.suite_id = '" . $code_suite . "' 
	";
        return $suite_net_rentable_query_string;
    }

    /**
     * Gets suite contguous area query string
     * @param type $compare_with_contig
     * @param type $Building
     * @param type $code_suite
     * @return string
     */
    private function getSuiteContiguousAreaQueryString($compare_with_contig, $Building, $code_suite) {
        $add_contig_query = '';
        if (strlen($compare_with_contig) > 0) {
            $add_contig_query = 'suites.contiguous_area > ' . $compare_with_contig . ' AND ';
        }

        $suite_contiguous_area_query_string = "				
         SELECT suites.suite_id,
               suites.suite_name,
               suites.contiguous_area
        FROM   suites
        JOIN   suites_yesterday
        ON     suites.suite_id = suites_yesterday.suite_id
        WHERE  " . $add_contig_query . "suites.contiguous_area <> suites_yesterday.contiguous_area
        AND    suites_yesterday.building_id = '" . $Building['building_id'] . "'
        AND    suites_yesterday.suite_id = '" . $code_suite . "' 
	";
        return $suite_contiguous_area_query_string;
    }

    /**
     * Gets the suite availability query string
     * @param type $code_suite
     * @param type $Building
     * @return string
     */
    private function getSuiteAvailabilityQueryString($code_suite, $Building) {
        $suite_availability_query_string = "
            SELECT suites.suite_id,
                   suites.suite_name,
                   suites.availability
            FROM   suites
            JOIN   suites_yesterday
            ON     suites.suite_id = suites_yesterday.suite_id
            WHERE  ((
                                 suites_yesterday.availability < " . self::$timestamp . "
                          AND    suites.availability > " . self::$timestamp . ")
                   OR     (
                                 suites.availability < " . self::$timestamp . "
                          AND    suites_yesterday.availability > " . self::$timestamp . ")
                   OR     (
                                 suites.availability > " . self::$timestamp . "
                          AND    suites_yesterday.availability > " . self::$timestamp . "))
            AND    suites.availability <> suites_yesterday.availability
            AND    suites_yesterday.building_id= '" . $Building['building_id'] . "'
            AND    suites_yesterday.suite_id= '" . $code_suite . "'";
        return $suite_availability_query_string;
    }

     /**
     * Gets the just leased query string
     * @param type $code_suite
     * @param type $Building
     * @return string
     */
    private function getJustLeasedQueryString($code_suite, $Building) { 
        $suite_just_leased_query_string = "				
            SELECT suites.suite_id,
                  suites.suite_name,
                  suites.leased
           FROM   suites
           JOIN   suites_yesterday
           ON     suites.suite_id = suites_yesterday.suite_id
           WHERE  suites.leased != suites_yesterday.leased
           AND    suites.leased = 'true'
           AND suites.lang = " . $Building['lang'] . " 
           AND suites_yesterday.lang = " . $Building['lang'] . "
           AND    suites_yesterday.building_id = '" . $Building['building_id'] . "'
           AND    suites_yesterday.suite_id = '" . $code_suite . "' 
	";
        return $suite_just_leased_query_string;
    }
    
    
        /**
     * Gets the just leased query string
     * @param type $code_suite
     * @param type $Building
     * @return string
     */
    private function getJustUnleasedQueryString($code_suite, $Building) {
        $suite_just_leased_query_string = "				
            SELECT suites.suite_id,
                  suites.suite_name,
                  suites.leased
           FROM   suites
           JOIN   suites_yesterday
           ON     suites.suite_id = suites_yesterday.suite_id
           WHERE  suites.leased != suites_yesterday.leased
           AND    suites.leased = 'false'
           AND suites.lang = " . $Building['lang'] . " 
           AND suites_yesterday.lang = " . $Building['lang'] . "
           AND    suites_yesterday.building_id = '" . $Building['building_id'] . "'
           AND    suites_yesterday.suite_id = '" . $code_suite . "' 
	";
        return $suite_just_leased_query_string;
    } 
    
    /**
     * Gets the suire add rent total query string
     * @param type $code_suite
     * @param type $Building
     * @return string
     */
    private function getSuiteAddRentTotalQueryString($code_suite, $Building) {
        $suite_add_rent_total_query_string = "				
            SELECT suites.suite_id,
                  suites.suite_name,
                  suites.add_rent_total
           FROM   suites
           JOIN   suites_yesterday
           ON     suites.suite_id = suites_yesterday.suite_id
           WHERE  suites.add_rent_total <> suites_yesterday.add_rent_total
           AND    suites_yesterday.building_id = '" . $Building['building_id'] . "'
           AND    suites_yesterday.suite_id = '" . $code_suite . "' 
	";
        return $suite_add_rent_total_query_string;
    }

    /**
     * Gets the yesterday's building query string, per suite_id
     * @param type $Suites
     * @return string
     */
    public function getYesterdayBuildingQueryString($Suites) {
        $yesterdaybuilding_query_string = "SELECT record_source FROM suites_diff WHERE suite_id = '" . $Suites['suite_id'] . "' AND record_source = 'yesterday'";
        return $yesterdaybuilding_query_string;
    }

    /**
     * Gets the buildings query string per building code.
     * @param type $code_building
     * @return string
     */
    private function getRealBuildingsQueryString($code_building) {
        $realbuildingsquery_string = "select * from buildings b where b.lang = '" . self::$lang_id . "' and b.building_code = '" . $code_building . "'";
        return $realbuildingsquery_string;
    }
    
    /**
     * Gets the broker updates query with a getQueryString fucntion.
     * @param type $uniqueid
     * @return type
     */
    public function getBuQuery($uniqueid) {
        $bu_query_string = $this->getQueryString($uniqueid);
        $bu_query = mysql_query($bu_query_string)or die("broker updates query error: " . mysql_error());
        return $bu_query;
    }

    /**
     * Gets the buildings query with a getBuildingsQueryString fucntion.
     * @param type $selectedData
     * @return type
     */
    public function getBuildingsQuery($selectedData) {
        $buildings_query_string = $this->getBuildingsQueryString($selectedData);
        $buildings_query = mysql_query($buildings_query_string) or die("buildings query error: " . mysql_error());
        return $buildings_query;
    }

    /**
     * Gets the yesterday's suites query with a getYesterdaySuitesQueryString fucntion.
     * @param type $Suites
     * @return type
     */    
    public function getYesterdaySuitesArray($Suites) {
        $yesterday_suites_query_string = $this->getYesterdaySuitesQueryString($Suites);
        $yesterday_suites_query = mysql_query($yesterday_suites_query_string) or die("Yesterday Suites query error: " . mysql_error());
        return mysql_fetch_array($yesterday_suites_query);
    }

    /**
     * Gets the suite net rentable area, and the suite net rentable count query with a getSuiteNetRentableQueryString fucntion.
     * @param type $Building
     * @param type $code_suite
     * @return type
     */
    public function getSuiteNetRentableAreaData($Building, $code_suite) {
        $suite_net_rentable_query_string = $this->getSuiteNetRentableQueryString($Building, $code_suite);
        $suite_net_rentable_query = mysql_query($suite_net_rentable_query_string) or die("net rentable query error: " . mysql_error());
        $suite_net_rentable_change = mysql_fetch_array($suite_net_rentable_query);
        $net_rentable_area = $suite_net_rentable_change['net_rentable_area'];
        $suite_net_rentable_count = mysql_num_rows($suite_net_rentable_query);
        $data = array();
        $data['net_rentable_area'] = $net_rentable_area;
        $data['suite_net_rentable_count'] = $suite_net_rentable_count;
        
        return $data;
    }

    /**
     * Gets the suite contiguous area query with a getSuiteContiguousAreaQueryString fucntion, as well as the suite contiguous area count.
     * @param type $compare_with_contig
     * @param type $Building
     * @param type $code_suite
     * @return type
     */
    public function getSuiteContiguousAreaData($compare_with_contig, $Building, $code_suite) {
        $suite_contiguous_area_query_string = $this->getSuiteContiguousAreaQueryString($compare_with_contig, $Building, $code_suite);
        $suite_contiguous_area_query = mysql_query($suite_contiguous_area_query_string) or die("suite contig query error: " . mysql_error());
        $suite_contiguous_area_change = mysql_fetch_array($suite_contiguous_area_query);
        $contiguous_area = $suite_contiguous_area_change['contiguous_area'];
        $suite_contiguous_area_count = mysql_num_rows($suite_contiguous_area_query);
        $data = array();
        $data['contiguous_area'] = $contiguous_area;
        $data['suite_contiguous_area_count'] = $suite_contiguous_area_count;
        
        
        
        return $data;
    }

    /**
     * Gets the buildings query with a getBuildingsQueryString fucntion.
     * @param type $Building
     * @param type $code_suite
     * @return type
     */
    public function getSuiteAvailabilityData($Building, $code_suite) {
        $suite_availability_query_string = $this->getSuiteAvailabilityQueryString($code_suite, $Building);
        $suite_availability_query = mysql_query($suite_availability_query_string) or die("suite contig query error: " . mysql_error());
        $suite_availability_change = mysql_fetch_array($suite_availability_query);
        $availability = $suite_availability_change['availability'];
        $suite_availability_count = mysql_num_rows($suite_availability_query);
        $data = array();
        $data['availability'] = $availability;
        $data['suite_availability_count'] = $suite_availability_count;
        
        return $data;
    }

    
    
    //TODO
    /**
     * Gets the suite add rent total query with a getSuiteAddRentTotalQueryString fucntion, as well as the add rent total count
     * @param type $Building
     * @param type $code_suite
     * @return type
     */
    public function getJustLeasedData($Building, $code_suite) {
        $suite_just_leased_query_string = $this->getJustLeasedQueryString($code_suite, $Building);
        $suite_just_unleased_query_string = $this->getJustUnleasedQueryString($code_suite, $Building);
        
        $suite_just_leased_query = mysql_query($suite_just_leased_query_string) or die("suite just leased query error: " . mysql_error());
        $suite_just_leased_change = mysql_fetch_array($suite_just_leased_query);
        $suite_just_leased_count = mysql_num_rows($suite_just_leased_query);
        
        $suite_just_unleased_query = mysql_query($suite_just_unleased_query_string) or die("suite just unleased query error: " . mysql_error());
        $suite_just_unleased_change = mysql_fetch_array($suite_just_unleased_query);
        $suite_just_unleased_count = mysql_num_rows($suite_just_unleased_query);
        
        $data = array();
        $data['leased_suite_id'] = $suite_just_leased_change['suite_id'];
        $data['leased_suite_name']= $suite_just_leased_change['suite_name'];
        $data['leased']= $suite_just_leased_change['leased'];
        $data['suite_just_leased_total_count'] = $suite_just_leased_count;
        
        $data['unleased_suite_id'] = $suite_just_unleased_change['suite_id'];
        $data['unleased_suite_name']= $suite_just_unleased_change['suite_name'];
        $data['unleased']= $suite_just_unleased_change['leased'];
        $data['suite_just_unleased_total_count'] = $suite_just_unleased_count;
        
        return $data;
    }
    
    
    
    
    /**
     * Gets the suite add rent total query with a getSuiteAddRentTotalQueryString fucntion, as well as the add rent total count
     * @param type $Building
     * @param type $code_suite
     * @return type
     */
    public function getSuiteAddRentTotalData($Building, $code_suite) {
        $suite_add_rent_total_query_string = $this->getSuiteAddRentTotalQueryString($code_suite, $Building);
        $suite_add_rent_total_query = mysql_query($suite_add_rent_total_query_string) or die("suite contig query error: " . mysql_error());
        $suite_add_rent_total_change = mysql_fetch_array($suite_add_rent_total_query);
        $add_rent_total = $suite_add_rent_total_change['add_rent_total'];
        $suite_add_rent_total_count = mysql_num_rows($suite_add_rent_total_query);
        $data = array();
        $data['add_rent_total'] = $add_rent_total;
        $data['suite_add_rent_total_count'] = $suite_add_rent_total_count;
        
        return $data;
    }

    /**
     * Gets the language query with a getLanguageQueryString fucntion.
     * @return type
     */
    public function getLanguageQuery() {
        $language_query_string = $this->getLanguageQueryString();
        $language_query = mysql_query($language_query_string)or die("language query error: " . mysql_error());
        return $language_query;
    }

    /**
     * Gets the real buildings query with a getRealBuildingsQueryString fucntion.
     * @param type $code_building
     * @return type
     */
    public function getRealBuildingsQuery($code_building) {
        $realbuildingsquery_string = $this->getRealBuildingsQueryString($code_building);
        $realbuildingsquery = mysql_query($realbuildingsquery_string);
        return $realbuildingsquery;
    }

    /**
     * Gets the suites count query with a getSuiteCountQueryString fucntion.
     * @param type $Building
     * @return type
     */
    public function suitesCountQuery($Building) {
        $suite_count_query_string = $this->getSuiteCountQueryString($Building);
        $suite_count_query = mysql_query($suite_count_query_string);
        return $suite_count_query;
    }

    /**
     * Gets the contacts query with the getContactsQueryString function.
     * @param type $Building
     * @return type
     */
    public function getContactsQuery($Building) {
        $contacts_query_string = $this->getContactsQueryString($Building);
        $contacts_query = mysql_query($contacts_query_string);
        return $contacts_query;
    }
    
   

    
        //TODO
    /**
     * Gets the suite add rent total query with a getSuiteAddRentTotalQueryString fucntion, as well as the add rent total count
     * @param type $Building
     * @param type $code_suite
     * @return type
     */
    public function getRemovedSuitesData($Building, $code_suite) {
        $query_string = $this->getRemovedSuitesDataString($Building, $code_suite);

        $query = mysql_query($query_string) or die("getRemovedBuildingData query error: " . mysql_error());

        $query_data = mysql_fetch_array($query);
        $count = mysql_num_rows($query);
        
            
        
        $data = array();
        $data['suite_index'] = $query_data['suite_index'];
        $data['lang'] = $query_data['lang'];
        $data['building_code'] = $query_data['building_code'];
        $data['building_id'] = $query_data['building_id'];
        $data['floor_name'] = $query_data['floor_name'];
        $data['floor_number'] = $query_data['floor_number'];
        $data['suite_id'] = $query_data['suite_id'];
        $data['suite_name'] = $query_data['suite_name'];
        $data['suite_type'] = $query_data['suite_type'];
        $data['net_rentable_area'] = $query_data['net_rentable_area'];
        $data['contiguous_area'] = $query_data['contiguous_area'];
        $data['taxes_and_operating'] = $query_data['taxes_and_operating'];
        $data['availability'] = $query_data['availability'];
        $data['description'] = $query_data['description'];
        $data['net_rent'] = $query_data['net_rent'];
        $data['add_rent_total'] = $query_data['add_rent_total'];
        $data['presentation_link'] = $query_data['presentation_link'];
        $data['presentation_name'] = $query_data['presentation_name'];
        $data['presentation_description'] = $query_data['presentation_description'];
        $data['pdf_plan'] = $query_data['pdf_plan'];
        $data['spp_link'] = $query_data['spp_link'];
        $data['model'] = $query_data['model'];
        $data['new'] = $query_data['new'];
        $data['leased'] = $query_data['leased'];
        $data['record_date'] = $query_data['record_date'];
        $data['record_source'] = $query_data['record_source'];
        $data['count'] = $count;
        $data['removed'] = 'false';
        if ($count>1){ $data['removed'] = 'true';}
                
        return $data;
    }
            

}
