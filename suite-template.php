<?php
include('theme/db.php');
include('eclipse/classes/Queries.php');
include('objects/ErrorMessages.php');
include('objects/SearchData.php');
include('objects/LanguageQuery.php');
include('objects/FormatData.php');
include('eclipse/classes/DisplayHTML.php');
include('objects/Graphics.php');
//include('objects/SpecPages.php');
include('objects/StatsPackage.php');

require_once 'dist/Mobile_Detect.php';
$detect = new Mobile_Detect;

include("assets/php/krumo/class.krumo.php");

date_default_timezone_set('America/New_York');

// initiate the StatsPackage class
$stats = new StatsPackage();


// Initiate the Graphics class
$graphics = new Graphics();

// Initiate the Queries class
$queries = new Queries();
//Initiate DisplayHTML
$displayHTML = new DisplayHTML();
//Initiate the Search Data class
$searchData = new SearchData();
//INitiate the Spec Pages
//$specPages = new SpecPages();
// Language Query
$languageQuery = new LanguageQuery();
$langArray = $languageQuery->getLanguageAndID();
$lang = $langArray[0];
$thelang = $langArray[0];
$lang_id = $langArray[1];
//$language_query_string = 'select * from languages2 ll, languages_dynamic ld, languages l  where ld.lang_id = ' . $lang_id . ' and l.lang_id = ' . $lang_id . ' and ll.lang_id = ' . $lang_id;
//$language_query = mysql_query($language_query_string) or die("language query error: " . mysql_error());
//$language = mysql_fetch_array($language_query);

$language = array();
$language_query_string = "select * from languages_dynamic ld, languages l, languages2 l2 where ld.lang_id =? and l.lang_id =? and l2.lang_id =?";
$language_query_statement = $db->prepare($language_query_string);
$language_query_statement->bind_param('iii', mysqli_real_escape_string($db, $lang_id), mysqli_real_escape_string($db, $lang_id), mysqli_real_escape_string($db, $lang_id));
if ($language_query_statement->execute()) {
    $result = $language_query_statement->get_result();
    while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
        $language = $row;
    }
}
$language_query_statement->close();


//           $language_query_string2 = 'select * from languages_dynamic ld, languages l, languages2 l2 where ld.lang_id = ' . $lang_id . ' and l.lang_id = ' . $lang_id . ' and l2.lang_id = ' . $lang_id . '';
//           $language_query2 = mysql_query($language_query_string2)or die("language query2 error: ". mysql_error());
//           $language2 = mysql_fetch_array($language_query2);   


$language2 = array();
$language2_query_string = "select * from languages_dynamic ld, languages l, languages2 l2 where ld.lang_id =? and l.lang_id =? and l2.lang_id =?";
$language2_query_statement = $db->prepare($language2_query_string);
$language2_query_statement->bind_param('iii', mysqli_real_escape_string($db, $lang_id), mysqli_real_escape_string($db, $lang_id), mysqli_real_escape_string($db, $lang_id));
if ($language2_query_statement->execute()) {
    $result = $language2_query_statement->get_result();
    while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
        $language2 = $row;
    }
}
$language2_query_statement->close();

// Theme Query
$theme_query = mysql_query("select * from theme");
$theme = mysql_fetch_array($theme_query);

//setting locale for time, date, money, etc
//            date_default_timezone_set('America/New_York');
setlocale(LC_ALL, $language['locale_string']);
// for reporting
$today = date("Y/m/d");
$todaytimestamp = strtotime($today);
//Initiate FormatData
$formatData = new FormatData();


$building_selected = $_REQUEST['building'];
$building_code = $building_selected;
$suite_selected = $_REQUEST['suiteid'];




$province_selected = $searchData->get_province_selected($language, $lang, $db);


$regional_branding_count = 0;

//$buildings_query_string = "select * from buildings where building_code='" . $building_selected . "' and lang=" . $lang_id;

$buildings_query_string = "select building_index, building_internal_identifier, lang, building_code, building_id, building_type, city, contact_info, country, description, flyer_header, modification_date, street_address, number_of_floors, floors_above_ground, floors_below_ground, available_space, office_area, industrial_area, postal_code, province, province_code, region, sub_region, leasing_node, renovated, retail_area, building_name, measurement_unit, thumbnail, static_map, total_space, uri, year_of_building, parking_stalls, parking_ratio, typical_floor_size, file_name, file_uri, dirty, orientation, add_rent_operating, add_rent_realty, add_rent_power, add_rent_total, truck_doors, drivein_doors, clear_height, power_text, marketing_text, sort_streetnumber, sort_streetname, latitude, longitude, client_name, new_development, no_vacancies from buildings where building_code=? and lang=?";
$buildings_query_statement = $db->prepare($buildings_query_string);
//bind parameters for markers, where (s = string, i = integer, d = double,  b = blob)
$buildings_query_statement->bind_param('si', mysqli_real_escape_string($db, $building_selected), mysqli_real_escape_string($db, $lang_id));
//execute query
$buildings_query_statement->execute();
//bind result variables
$buildings_query_statement->bind_result($building_index, $building_internal_identifier, $lang, $building_code, $building_id, $building_type, $city, $contact_info, $country, $description, $flyer_header, $modification_date, $street_address, $number_of_floors, $floors_above_ground, $floors_below_ground, $available_space, $office_area, $industrial_area, $postal_code, $province, $province_code, $region, $sub_region, $leasing_node, $renovated, $retail_area, $building_name, $measurement_unit, $thumbnail, $static_map, $total_space, $uri, $year_of_building, $parking_stalls, $parking_ratio, $typical_floor_size, $file_name, $file_uri, $dirty, $orientation, $add_rent_operating, $add_rent_realty, $add_rent_power, $add_rent_total, $truck_doors, $drivein_doors, $clear_height, $power_text, $marketing_text, $sort_streetnumber, $sort_streetname, $latitude, $longitude, $client_name, $new_development, $no_vacancies);
//must fetch the info after it's bound. Unsure why but there it is.
$buildings_query_statement->fetch();
$buildings_query_statement->close();


$building_thumbnail_query_string = $queries->get_building_thumbnail_query_string($lang_id, $building_code);
//    $building_thumbnail_query = mysql_query($building_thumbnail_query_string) or die ("building thumbnail image error: " . mysql_error());
//    $building_thumbnail = mysql_fetch_array($building_thumbnail_query);
// process buildings query string
$building_thumbnail__string = $building_thumbnail_query_string[0];
//    $building_thumbnail__string .=" LIMIT ".$postnumbers." OFFSET ".$offset;
$pageCurrent = '';
//    $building_thumbnail__string = str_replace("and allb.building_name like '%none%'", "", $building_thumbnail__string);
if ($building_thumbnail__statement = $db->prepare($building_thumbnail__string)) {
    // carry on
} else {
    trigger_error('Wrong SQL: ' . $building_thumbnail__string . ' Error: ' . $db->error, E_USER_ERROR);
}
// prep dynamic variable query
$param_type = '';
$n = count($building_thumbnail_query_string[1]);
for ($i = 0; $i < $n; $i++) {
    $param_type .= $building_thumbnail_query_string[1][$i];
}

/* with call_user_func_array, array params must be passed by reference */
$a_params[] = & $param_type;

for ($i = 0; $i < $n; $i++) {
    /* with call_user_func_array, array params must be passed by reference */
    $a_params[] = & $building_thumbnail_query_string[2][$i];
}

// call_user_func_array is how we can pass dynamic quantities of variables to mysqli prepared statements.
call_user_func_array(array($building_thumbnail__statement, 'bind_param'), $a_params);
$building_thumbnail = array();
if ($building_thumbnail__statement->execute()) {
    $result = $building_thumbnail__statement->get_result();
    while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
        $building_thumbnail = $row;
    }
} else {
    trigger_error('Could not execute building_thumbnail query statement: Error: ' . $db->error, E_ALL);
}
$building_thumbnail__statement->close();




    $building_thumbnail_image_name = '';
    $building_thumbnail_thumb_name = '';
    if (isset($building_thumbnail['file_name']) && $building_thumbnail['file_name'] != '') {
        $building_thumbnail_image_name = "images/preview_images/" . $building_thumbnail['file_name'];
        $building_thumbnail_thumb_name = "images/preview_images/" . str_replace('.jpg', '_thumb.jpg', $building_thumbnail['file_name']);
        $building_thumbnail_image_name_list = "images/preview_images/" . str_replace('.jpg', '_listthumb.jpg', $building_thumbnail['file_name']);

    } else {
        $building_thumbnail_image_name = "images/no_thumb_700.png";
        $building_thumbnail_thumb_name = "images/no_thumb_700.png";
        $building_thumbnail_image_name_list = "images/no_thumb_200.png";
    }


//$building_thumbnail_image_name = '';
//$building_thumbnail_image_name = "images/preview_images/" . $building_thumbnail['file_name'];
//$building_thumbnail_thumb_name = "images/preview_images/" . str_replace('.jpg', '_thumb.jpg', $building_thumbnail['file_name']);
//
//if ($building_thumbnail_image_name == "images/preview_images/") {
//    $building_thumbnail_image_name = "images/no_thumb_700.png";
//}

$building_thumbnail_count = 0;
if ($building_thumbnail_thumb_name == "images/preview_images/") {
    $building_thumbnail_count = 0;
} else {
    $building_thumbnail_count = 1;
}



$building_id = $building_id;
$building_name = $building_name;
$building_street = $street_address;
$building_city = $city;
$building_province = $province;
$building_region = $region;
$building_sub_region = $sub_region;
$building_postal_code = $postal_code;
$building_type = $building_type;

$building_latitude = $latitude;
$building_longitude = $longitude;

$building_desc = strip_tags($description, '<p>,<br>,<br />,<strong>,<u>,<b>,<i>,<ul>,<li>,<ol>,<a>');
$building_desc = str_replace('style=', '', $building_desc);











$suites_html = "";
$building_rent_high = 0;
$building_rent_low = 0;
$building_tax_high = 0;
$building_tax_low = 0;
$prescounter = 0;
$pageCurrent = '';

$video_query = $queries->video_query($building_code, $suite_selected, $lang_id, $db);
$suite_video_count = count($video_query);

//BUILDING LEVEL VIDEO QUERY
$video_query2 = $queries->file_extension_query($building_code, "mp4", $lang_id, $db);
if (is_bool($video_query2)) {
    $video_count = 0;
} else {
    $video_count2 = count($video_query2);
}

$pdf_query = $queries->pdf_query($building_code, $suite_selected, $lang_id, $db);
if (is_bool($pdf_query)) {
    $suite_pdf_count = 0;
} else {
    $suite_pdf_count = count($pdf_query);
}

//// From building.php
//$pdf_query = $queries->file_extension_query($building_code, "pdf", $lang_id, $db);
//if (is_bool($pdf_query)) {
//    $pdf_count = 0;
//} else {
//    $pdf_count = count($pdf_query);
//}






$buildingpdf_query = $queries->file_extension_query($building_code, "pdf", $lang_id, $db);
if (is_bool($buildingpdf_query)) {
    $buildingpdf_count = 0;
} else {
    $buildingpdf_count = count($pdf_query);
}

$pdf_query2 = $queries->pdf_query($building_code, $suite_selected, $lang_id, $db);
$suite_pdf_count2 = count($pdf_query2);

$threedee_query = $queries->threedee_query($building_code, $suite_selected, $lang_id, $db);
$threedee_present = count($threedee_query);


$threedee_query2 = $queries->threedee_query($building_code, $suite_selected, $lang_id, $db);
$threedee_present2 = count($threedee_query);

$sketchfab_query = $queries->threedeeSketchfab_query($building_code, $suite_selected, $lang_id, $db);
$sketchfab_present = count($sketchfab_query);

// BUILDING LEVEL 3D QUERY
$threedee_query_building = $queries->threedee_query_building($building_selected, $lang_id, $db);
$threedee_building_present = count($threedee_query_building);


// SUITE PREVIEW IMAGES QUERY
$suite_preview_images_query = $queries->suite_preview_images_query($suite_selected, $building_code, $lang_id, $db);
$suite_preview_images_count = count($suite_preview_images_query);

$suite_preview_images_query2 = $queries->suite_preview_images_query($suite_selected, $building_code, $lang_id, $db);
$suite_preview_images_count2 = count($suite_preview_images_query);

// Budilng Preview images query
$preview_images_query2 = $queries->preview_images_query($building_code, $lang_id, $db);
$preview_images_count = count($preview_images_query2);

// Suite Documents Query
$suite_documents_query = $queries->suite_documents_query($suite_selected, $building_code, $lang_id, $db);
$suite_documents_count = count($suite_documents_query);
$multimedia_count = false;
if (($suite_video_count > 0) || ($suite_documents_count > 0) || $suite_preview_images_count > 0) {
    $multimedia_count = true;
}


function suiteMedia($pdf_query, $threedee_query, $sketchfab_query, $suite_preview_images_query, $suite_documents_query, $preview_images_query2, $graphics) {
    $HTML = '';
    
    if (count($pdf_query) + count($suite_documents_query) > 0){
        $HTML .= '<h3 class="fs_media-section-title">Documents</h3>';
        $HTML .= '<div class="row fs_gallery fs_gallery-documents">';
    }
    foreach($pdf_query as $pdf) {

        $document_name = $pdf['plan_name'];
        
        if ($pdf['plan_thumb'] != '') {
            $document_thumb = 'images/twodee_files/' . $pdf['plan_thumb'];
        } else {
            $document_thumb = 'images/no_thumb_700.png';
        }

        $HTML .= '<div class="col fs_gallery-thumbnail">'
                . '<a class="featuredocclick" href="images/twodee_files/' . $document_name . '" title="' . $document_name . ' "  target="_blank" rel="noopener noreferrer"><img src="' . $document_thumb . '" class="img-fluid" /></a>'
                . '</div>';
    }
    
    foreach ($suite_documents_query as $row) {
            if (strpos(strtoupper($row['extension']), 'PNG') !== false) { 
    
            if (($row['extension'] == 'png') || ($row['extension'] == 'jpg') || ($row['extension'] == 'gif')) {
                    $thumb = 'images/documents/'.$row['file_name'];
            }

            $HTML .= '<div class="col fs_gallery-thumbnail">'
                    . '<a class="featuredocclick" href="' . $row['uri'] . '" title="' . $row['display_filename'] . ' "  target="_blank" rel="noopener noreferrer"><img src="' . $thumb . '" class="img-responsive" /></a>'
                    . '</div';
            } else {

                $pth = $graphics->addTextToPNG($row['extension'], "images/newFileExtensions/icon.png", "images/newFileExtensions/");

                $HTML .= '<div class="col fs_gallery-thumbnail">'
                        . '<a class="featuredocclick" href="' . $row['uri'] . '" title="' . $row['file_name'] . ' "  target="_blank" rel="noopener noreferrer"><img src="' . $pth . '" class="img-responsive" /></a>'
                        . '</div>';
            }
        }
        if (count($pdf_query) + count($suite_documents_query) > 0){
            $HTML .= "</div>";
        }
    
    
//    foreach($threedee_query as $threedee) {
//
//    }
    
//    foreach($sketchfab_query as $sketchfab) {
//
//    }
    
    
    if (count($suite_preview_images_query) > 0 || count($preview_images_query2) > 0){
        $HTML .= '<h3 class="fs_media-section-title">Images</h3>';
        $HTML .= '<div class="row fs_gallery fs_gallery-images">';
    }
        foreach ($suite_preview_images_query as $galleryimages) {

            if (strlen($galleryimages['file_name']) > 30) {
                $galleryimagename = substr($galleryimages['file_name'], 0, 30).'...';
            } else {
                $galleryimagename = $galleryimages['file_name'];
            }
            
            $gallerythumbnail = str_ireplace('.' . $galleryimages['extension'], '', $galleryimages['file_name']) . "_thumb." . $galleryimages['extension'];
//            file_name.extension
            $HTML .='<div class="col fs_gallery-thumbnail"><a data-fancybox="gallery" href="images/suite_preview_images/' . $galleryimages['file_name'] . '" title="' . $galleryimages['display_file_name'] . '"><img src="images/suite_preview_images/' . $gallerythumbnail . '" alt="' . $galleryimages['display_file_name'] . '"  class="img-fluid"></a></div>';
            
        }
        
        
        foreach ($preview_images_query2 as $buildingimages) {
            if (strlen($buildingimages['file_name']) > 30) {
            $buildingimagename = substr($buildingimages['file_name'], 0, 30).'...';
        } else {
            $buildingimagename = $buildingimages['file_name'];
        }
            
            $buildingthumbnail = str_ireplace('.' . $buildingimages['extension'], '', $buildingimages['file_name']) . "_thumb." . $buildingimages['extension'];
//            file_name.extension
            $HTML .='<div class="col fs_gallery-thumbnail"><a data-fancybox="gallery" href="images/preview_images/' . $buildingimages['file_name'] . '" title="' . $buildingimages['display_file_name'] . '"><img src="images/preview_images/' . $buildingthumbnail . '" alt="' . $buildingimages['display_file_name'] . '"  class="img-fluid"></a></div>';
            
        }
        
        
        
        if (count($suite_preview_images_query) > 0 || count($preview_images_query2) > 0){
            $HTML .= "</div>";
        }
				


    echo $HTML;
}





//<div class="item"><img src="images/slide2.jpg"></div>
//-----
// Hero Carousel
//-----
$carouselHTML = '';
if (count($pdf_query) > 0) {
    foreach($pdf_query as $pdf) {
        $document_name = $pdf['plan_name'];
        
        if ($pdf['plan_thumb'] != '') {
            $document_thumb = 'images/twodee_files/thumbc_' . $pdf['plan_thumb'];
        } else {
            $document_thumb = 'images/no_thumb_700.png';
        }

//        $HTML .= '<div class="col fs_gallery-thumbnail">'
//                . '<a class="featuredocclick" href="images/twodee_files/' . $document_name . '" title="' . $document_name . ' "  target="_blank" ><img src="' . $document_thumb . '" class="img-fluid" /></a>'
//                . '</div>';
        
        $carouselHTML .= '<div class="item cover"><a href="images/twodee_files/'.$pdf['plan_name'].'" data-fancybox="gallery"><img src="'.$document_thumb.'"></a></div>';
    }
}

if ($suite_preview_images_count > 0) {
    foreach ($suite_preview_images_query as $galleryimages) {

        if (strlen($galleryimages['file_name']) > 30) {
            $galleryimagename = substr($galleryimages['file_name'], 0, 30).'...';
        } else {
            $galleryimagename = $galleryimages['file_name'];
        }

        $gallerythumbnail = str_ireplace('.' . $galleryimages['extension'], '', $galleryimages['file_name']) . "_thumbc." . $galleryimages['extension'];
    //            file_name.extension
//        $HTML .='<div class="col fs_gallery-thumbnail"><a data-fancybox="gallery" href="images/suite_preview_images/' . $galleryimages['file_name'] . '" title="' . $galleryimages['display_file_name'] . '"><img src="images/suite_preview_images/' . $gallerythumbnail . '" alt="' . $galleryimages['display_file_name'] . '"  class="img-fluid"></a></div>';
        $carouselHTML .= '<div class="item cover"><a href="images/suite_preview_images/'.$galleryimages['file_name'].'" data-fancybox="gallery"><img src="images/suite_preview_images/'.$gallerythumbnail.'"></a></div>';

    }
}

// add building preview images to the carousel too
//$preview_images_query2
if ($preview_images_count > 0) {
    foreach ($preview_images_query2 as $buildingimages) {

        if (strlen($buildingimages['file_name']) > 30) {
            $galleryimagename = substr($buildingimages['file_name'], 0, 30).'...';
        } else {
            $galleryimagename = $buildingimages['file_name'];
        }

        $gallerythumbnail = str_ireplace('.' . $buildingimages['extension'], '', $buildingimages['file_name']) . "_thumbc." . $buildingimages['extension'];
    //            file_name.extension
//        $HTML .='<div class="col fs_gallery-thumbnail"><a data-fancybox="gallery" href="images/suite_preview_images/' . $buildingimages['file_name'] . '" title="' . $buildingimages['display_file_name'] . '"><img src="images/suite_preview_images/' . $gallerythumbnail . '" alt="' . $buildingimages['display_file_name'] . '"  class="img-fluid"></a></div>';
        $carouselHTML .= '<div class="item cover"><a href="images/preview_images/'.$buildingimages['file_name'].'" data-fancybox="gallery"><img src="images/preview_images/'.$gallerythumbnail.'"></a></div>';

    }
}

$theCarousel = 0;
if ($carouselHTML !== ""){
    $theCarousel = 1;
}
                


// Suite Picker 
$suitePickQuery = array();
$suiteCounter = 0;
$suitePickQuery_string = "Select suite_id, suite_name from suites where building_code =? and lang=1 and suite_id !=? order by floor_number desc, suite_name desc";
if ($suitePickQuery_statement = $db->prepare($suitePickQuery_string)) {
    // carry on
} else {
    trigger_error('Wrong SQL: ' . $suitePickQuery_string . ' Error: ' . $db->error, E_USER_ERROR);
}
$suitePickQuery_statement->bind_param('si', mysqli_real_escape_string($db, $building_code), mysqli_real_escape_string($db, $suite_selected));
if ($suitePickQuery_statement->execute()) {
    $result = $suitePickQuery_statement->get_result();
    while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
        $suitePickQuery[] = $row;
        $suiteCounter++;
    }
} else {
    trigger_error('Wrong SQL: ' . $suitePickQuery_string . ' Error: ' . $db->error, E_USER_ERROR);
}
$suitePickQuery_statement->close();



//<a class="dropdown-item" href="#">Suite 202</a>
$suiteList = '';
foreach ($suitePickQuery as $suitePick) {
    $suiteList .= '<a class=" suiteLink dropdown-item" href="suite.php?building=' . $building_code . '&suiteid=' . $suitePick['suite_id'] . '">' . $language['suite_text'] . ' ' . $suitePick['suite_name'] . '</a>';
}





$suitetype_selected = $searchData->get_suitetype_selected($language);

if (isset($_REQUEST['show_no_vacancy']) && $_REQUEST['show_no_vacancy'] != '') {
    $show_no_vacancy = $_REQUEST['show_no_vacancy'];
} else {
    if ($theme['no_vacancy_default'] == 0) {
        $show_no_vacancy = "no";
    } else if ($theme['no_vacancy_default'] == 1) {
        $show_no_vacancy = "yes";
    }
}


//$building_thumbnail_query_string = $queries->get_building_thumbnail_query_string($lang_id, $building_code);
//$building_thumbnail_query = mysql_query($building_thumbnail_query_string) or die("building thumbnail image error: " . mysql_error());
//$building_thumbnail = mysql_fetch_array($building_thumbnail_query);

$building_thumbnail_query_string = $queries->get_building_thumbnail_query_string($lang_id, $building_code);
//    $building_thumbnail_query = mysql_query($building_thumbnail_query_string) or die ("building thumbnail image error: " . mysql_error());
//    $building_thumbnail = mysql_fetch_array($building_thumbnail_query);
// process buildings query string
$building_thumbnail__string = $building_thumbnail_query_string[0];
//    $building_thumbnail__string .=" LIMIT ".$postnumbers." OFFSET ".$offset;
$pageCurrent = '';
//    $building_thumbnail__string = str_replace("and allb.building_name like '%none%'", "", $building_thumbnail__string);
if ($building_thumbnail__statement = $db->prepare($building_thumbnail__string)) {
    // carry on
} else {
    trigger_error('Wrong SQL: ' . $building_thumbnail__string . ' Error: ' . $db->error, E_USER_ERROR);
}
// prep dynamic variable query
$a_params = array();
$param_type = '';
$n = count($building_thumbnail_query_string[1]);
for ($i = 0; $i < $n; $i++) {
    $param_type .= $building_thumbnail_query_string[1][$i];
}
/* with call_user_func_array, array params must be passed by reference */
$a_params[] = & $param_type;

for ($i = 0; $i < $n; $i++) {
    /* with call_user_func_array, array params must be passed by reference */
    $a_params[] = & $building_thumbnail_query_string[2][$i];
}


// call_user_func_array is how we can pass dynamic quantities of variables to mysqli prepared statements.
call_user_func_array(array($building_thumbnail__statement, 'bind_param'), $a_params);
$building_thumbnail = array();
if ($building_thumbnail__statement->execute()) {
    $result = $building_thumbnail__statement->get_result();
    while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
        $building_thumbnail = $row;
    }
} else {
    trigger_error('Could not execute building_thumbnail query statement: Error: ' . $db->error, E_USER_ERROR);
}
$building_thumbnail__statement->close();


$building_thumbnail_count = 0;
$building_thumbnail_image_name = '';
$building_thumbnail_thumb_name = '';
if (isset($building_thumbnail['file_name']) && $building_thumbnail['file_name'] != '') {
    $building_thumbnail_image_name = "images/preview_images/" . $building_thumbnail['file_name'];
    $building_thumbnail_thumb_name = "images/preview_images/" . str_replace('.jpg', '_thumb.jpg', $building_thumbnail['file_name']);
    $building_thumbnail_count = 1;
} else {
    $building_thumbnail_image_name = "images/no_thumb_700.png";
}



//$building_thumbnail_count = 0;
//if ($building_thumbnail_thumb_name == "images/preview_images/") {
//    $building_thumbnail_count = 0;
//} else {
//    $building_thumbnail_count = 1;
//}
//BrokerPackage check
if (($building_thumbnail_count + $suite_pdf_count + $preview_images_count + $buildingpdf_count) > 0) {
    $brokerpackage_check = 1;
}



////// SUITE QUERY
$suites_query = $queries->suites_query($building_selected, $suite_selected, $lang_id, $db);

$Suites = $suites_query[0];


//$suites_query_string = "select building_index, building_internal_identifier, lang, building_code, building_id, building_type, city, contact_info, country, description, flyer_header, modification_date, street_address, number_of_floors, floors_above_ground, floors_below_ground, available_space, office_area, industrial_area, postal_code, province, province_code, region, sub_region, leasing_node, renovated, retail_area, building_name, measurement_unit, thumbnail, static_map, total_space, uri, year_of_building, parking_stalls, parking_ratio, typical_floor_size, file_name, file_uri, dirty, orientation, add_rent_operating, add_rent_realty, add_rent_power, add_rent_total, truck_doors, drivein_doors, clear_height, power_text, marketing_text, sort_streetnumber, sort_streetname, latitude, longitude, client_name, new_development, no_vacancies from buildings where building_code=? and lang=?";
//$suites_query_statement = $db->prepare($suites_query_string);
////bind parameters for markers, where (s = string, i = integer, d = double,  b = blob)
//$suites_query_statement->bind_param('si', mysqli_real_escape_string($db, $building_selected), mysqli_real_escape_string($db, $lang_id));
////execute query
//$suites_query_statement->execute();
////bind result variables
//$suites_query_statement->bind_result($building_index, $building_internal_identifier, $lang, $building_code, $building_id, $building_type, $city, $contact_info, $country, $description, $flyer_header, $modification_date, $street_address, $number_of_floors, $floors_above_ground, $floors_below_ground, $available_space, $office_area, $industrial_area, $postal_code, $province, $province_code, $region, $sub_region, $leasing_node, $renovated, $retail_area, $building_name, $measurement_unit, $thumbnail, $static_map, $total_space, $uri, $year_of_building, $parking_stalls, $parking_ratio, $typical_floor_size, $file_name, $file_uri, $dirty, $orientation, $add_rent_operating, $add_rent_realty, $add_rent_power, $add_rent_total, $truck_doors, $drivein_doors, $clear_height, $power_text, $marketing_text, $sort_streetnumber, $sort_streetname, $latitude, $longitude, $client_name, $new_development, $no_vacancies);
////must fetch the info after it's bound. Unsure why but there it is.
//$suites_query_statement->fetch();
//$suites_query_statement->close();





$suite_id = $Suites['suite_id'];
$suite_name = $Suites['suite_name'];
$suite_description = strip_tags($Suites['description'], '<p>,<br>,<br />,<strong>,<u>,<b>,<i>,<ul>,<li>,<ol>,<a>');
$suite_description = str_replace('style=', '', $suite_description);
$suite_net_rent = doubleval($Suites['net_rent']);
$suite_add_rent_total = doubleval($Suites['add_rent_total']);
$suite_new = $Suites['new'];
$suite_model = $Suites['model'];
$suite_leased = $Suites['leased'];
$suite_promoted = $Suites['promoted'];
$suite_net_rentable_area = $Suites['net_rentable_area'];

if ($suite_net_rent == "0" || $suite_net_rent == "") {
    $suite_net_rent = $language['negotiable_text'];
} else {
    $suite_net_rent = money_format("%.2n", $suite_net_rent);
}

// Statistics Tracking
$stats->trackSuitepageOpens($todaytimestamp, $today, $building_id, $building_code, $building_city, $suite_id, $suite_net_rentable_area, $suite_name);







// Set up English vs French number formatting
$suite_net_rentable_area = $languageQuery->suite_net_rentable_area($Suites, $thelang);
$suite_contiguous_area = $languageQuery->suite_contiguous_area($Suites, $thelang);
$suite_min_divisible_area = $queries->get_suite_min_divisible($thelang, $Suites);

$suite_availability = $queries->get_suite_availability($language, $Suites);
$suite_type = $Suites['suite_type'];
$contacts_html = "";
$contacts_email_html = "";
$contacts_query = $queries->contacts_query($building_selected, $lang_id, $db);
foreach ($contacts_query as $row) {
    $contacts_html .= $displayHTML->building_contacts_html($row, $theme);
    $contacts_email_html .= ';' . $row['email'];
}
$contacts_html = $contacts_html;





$contacts_tabfooter_html = "";
$contacts_email_html = "";
$contacts_query = $queries->contacts_tabfooter_query($building_selected, $lang_id, $db);
$contacts_count = count($contacts_query);
if (!is_bool($contacts_query)) {
    foreach ($contacts_query as $row) {
        $contacts_tabfooter_html .= $displayHTML->building_contacts_tabfooter_html($row, $theme);
        $contacts_email_html .= ';' . $row['email'];
    }
}


$allemails = $queries->contacts_tabfooter_emailaddress_query($building_selected, $lang_id, $db);
$allcontacts = '';
foreach ($allemails as $address) {
    $allcontacts .= $address['allemail'];
    $allcontacts .= ';';
}

function format_numbers($value, $language) {
    if ($language['lang_id'] == "1") {
        $formatted_number = number_format((double) $value);
    }
    if ($language['lang_id'] == "0") {
        $formatted_number = number_format((double) $value, 0, ',', ' ');
    }
    return $formatted_number;
}

function checkOrX($value) {
    if ($value == "true" || $value == "Yes") {
        $checkOrX = '<i class="material-icons fs_icon-done">done</i>';
    }
    if ($value == "false" || $value == "No") {
        $checkOrX = '<i class="material-icons fs_icon-clear">clear</i>';
    }
    echo $checkOrX;
}

function checkForEmpty($input) {
    if ($input === '' || $input === '0' || $input === "n/a" || $input === '$0.00' || $input === "/1000") {
        return false;
    } else {
        return true;
    }
}

?>

<?php
//if ($theme['wrapper_width'] != '') {
//    $wrapper_width = $theme['wrapper_width'];
//} else {
//    $wrapper_width = '960';
//}
















if ($building_type == "Industrial" || $building_type == "Industriel") {

    $suite_specs_query_string = "select shipping_doors_drive_in, shipping_doors_truck, available_electrical_amps, available_electrical_volts, clear_height from suites_specifications where building_id = '" . $building_id . "' and suite_id = '" . $suite_id . "'";
    $suite_specs_query = mysql_query($suite_specs_query_string) or die("Suite specs query error: " . mysql_error());
    $suite_specs = mysql_fetch_array($suite_specs_query);

    $shippingDI = $suite_specs['shipping_doors_drive_in'];
    $shippingTL = $suite_specs['shipping_doors_truck'];
    $poweramps = $suite_specs['available_electrical_amps'];
    $powervolts = $suite_specs['available_electrical_volts'];
    $ceiling = $suite_specs['clear_height'];
    $loadingoutputDI = '';
    $loadingoutputTL = '';
    $loadingoutput = '';

    if ($shippingDI != '' && $shippingDI != 0) {
        $loadingoutputDI = $shippingDI . " " . $language['drivein_loading_abbreviation'];
    }
    if ($shippingTL != '' && $shippingTL != 0) {
        $loadingoutputTL = $shippingTL . " " . $language['truck_loading_abbreviation'];
    }
    if ($loadingoutputDI != '' && $loadingoutputTL != '') {
        $loadingoutput = $loadingoutputDI . ", " . $loadingoutputTL;
    } else if ($loadingoutputDI != '' && $loadingoutputTL == '') {
        $loadingoutput = $loadingoutputDI;
    } else if ($loadingoutputDI == '' && $loadingoutputTL != '') {
        $loadingoutput = $loadingoutputTL;
    } else {
        // both are empty then do nothing
        $loadingoutput = '';
    }

    $poweroutputAmps = '';
    $poweroutputVolts = '';
    if ($poweramps != '' && $poweramps != 0) {
        $poweroutputAmps = $poweramps . " A";
    }
    if ($powervolts != '' && $powervolts != 0) {
        $poweroutputVolts = $powervolts . " V";
    }
    if ($poweroutputAmps != '' && $poweroutputVolts != '') {
        $poweroutput = $poweroutputAmps . ", " . $poweroutputVolts;
    } else if ($poweroutputAmps != '' && $poweroutputVolts == '') {
        $poweroutput = $poweroutputAmps;
    } else if ($poweroutputAmps == '' && $poweroutputVolts != '') {
        $poweroutput = $poweroutputVolts;
    } else {
        // both are empty leave a space
        $poweroutput = "&nbsp;";
    }
}








$translatedsuitetype = "";
if ($lang_id == 0) {
    switch ($suite_type) {
        case "OFFICE":
            $translatedsuitetype = "BUREAU";
            break;
        case "RETAIL":
            $translatedsuitetype = "DÉTAIL";
            break;
        case "INDUSTRIAL":
            $translatedsuitetype = "INDUSTRIEL";
            break;
    }
} else { // if lang_id == 1
    switch ($suite_type) {
        case "BUREAU":
            $translatedsuitetype = "OFFICE";
            break;
        case "DÉTAIL":
            $translatedsuitetype = "RETAIL";
            break;
        case "INDUSTRIEL":
            $translatedsuitetype = "INDUSTRIAL";
            break;
    }
}
if ($lang_id == 1) {
    $type_suite = $suite_type;
} else {
    $type_suite = $translatedsuitetype;
}




        $ipaddress = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '';
//        $hostname = ''; // too many pitfalls waiting for DNS which will kill the page load
        $referer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '';
        
        $browser = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '';
//        $screensize = ''; // only available via javascript. Need to ajax-ify this function if we want to capture it.
        $datetime = date("Y/m/d, g:ia", $_SERVER['REQUEST_TIME']) ; // format this YYYY/MM/DD
?>


<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!--        <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css?family=Fira+Sans+Condensed|Roboto" rel="stylesheet">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.1.25/jquery.fancybox.min.css" />
    <link rel="stylesheet" href="lib/owlcarousel/2.2.1/dist/assets/owl.carousel.min.css">
        <link rel="stylesheet" href="lib/owlcarousel/2.2.1/dist/assets/owl.theme.default.min.css">
        <link rel="stylesheet" href="style.css">-->
<!--        <link rel="stylesheet" href="suite.css">
        <link rel="stylesheet" href="suite-integrated.css">-->
        <?php // include('eclipse/colour_styles.php'); ?>
    </head>
    <body class="fs_page-suite">
        <header id="fs_masthead-suite" class="fs_site-header">
        <div class="fs_utilities fs_suite-utilities">
            <div class=" fs_container container">
                <div class="row no-gutters">
                    <div class="col">
                        <ul class="nav fs_nav-utilities">
<!--                            <li class="nav-item d-none d-lg-inline-block">
                                <a href="#" class="nav-link" data-toggle="modal" data-target="#fs_modal-print" aria-label="Print to PDF"><i class="material-icons fs_nav-link-icon">print</i> <span class="fs_nav-link-text">Print</span></a>
                            </li>-->
<!--                            <li class="nav-item d-none d-lg-inline-block">
                                <a href="#" class="btn btn-primary fs_btn-back" aria-label="Back">
                                    <i class="material-icons fs_btn-icon">chevron_left</i> <span class="fs_btn-text">Back</span>
                                </a>
                            </li>-->
<!--                            <li class="nav-item d-lg-none">
                                <a href="#" class="nav-link fs_nav-link fs_back-link" aria-label="Back">
                                    <i class="material-icons fs_nav-link-icon">chevron_left</i> <span class="fs_nav-link-text">Back</span>
                                </a>
                            </li>  -->
<!--                            <li class="nav-item d-none d-lg-inline-block">
                                <a href="#" class="nav-link" data-toggle="modal" data-target="#fs_modal-share" aria-label="Share"><i class="material-icons fs_nav-link-icon">share</i> <span class="fs_nav-link-text">Share</span></a>
                            </li>-->
<!--                            <li class="nav-item">
                                <a href="#" class="nav-link" data-toggle="modal" data-target="#fs_modal-property-flyer" aria-label="Print to PDF"><i class="material-icons fs_nav-link-icon">insert_drive_file</i> <span class="fs_nav-link-text"><span class="d-none d-lg-inline-block">Property</span> Flyer</span></a>
                            </li>-->
                            <li class="nav-item">
                                <a class="nav-link fs_nav-link fs_broker-info-link" href="#" data-toggle="modal" data-target="#fs_modal-broker-info" aria-label="Broker Info"><i class="material-icons fs_nav-link-icon fs_icon-broker-info">person</i> <span class="fs_nav-link-text"><span class="d-none d-lg-inline-block"><?php echo $language['broker_info_text_firstword']; ?></span> <?php echo $language['broker_info_text_secondword']; ?></span></a>
                            </li>
                        <?php if ($brokerpackage_check > 0) { ?>
                            <li class="nav-item d-lg-none">
                                <a class="nav-link fs_nav-link fs_broker-package-link" href="#" data-toggle="modal" data-target="#fs_modal-broker-package" aria-label="Download Broker Package"><i class="material-icons fs_nav-link-icon fs_icon-broker-package">chrome_reader_mode</i> <span class="fs_nav-link-text"><?php echo $language['broker_package_button_text']; ?></span></a>
                            </li>
                        <?php } ?>
                        <?php // if ($theme['bilingual_fr'] == 1) { ?>
<!--                            <li class="nav-item d-none d-lg-inline-block">
                                <div class="dropdown">
                                    <a class="dropdown-toggle nav-link fs_nav-link" href="#" role="button" id="dropdownLanguage" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons fs_nav-link-icon fs_icon-language">language</i> <span class="fs_nav-link-text"><?php // echo $language['language_text']; ?></span><span class="material-icons fs_nav-link-icon">expand_more</span>
                                    </a>
                                <div class="dropdown-menu" aria-labelledby="dropdownLanguage">
                                    <a class="dropdown-item langclick" href="#" data-language="en_CA"><?php // echo $language['english_text']; ?></a>
                                    <a class="dropdown-item langclick" href="#" data-language="fr_CA"><?php // echo $language['french_text']; ?></a>
                                </div>
                              </div>
                            </li>-->
                        <?php // } ?>
                        </ul>
                    </div>
                    <div class="col col-auto">
                        <ul class="nav">
                            <?php if ($brokerpackage_check > 0) { ?>
                            <li class="nav-item d-none d-lg-inline-block"><a href="#" class="btn btn-primary fs_btn-broker-package" data-toggle="modal" data-target="#fs_modal-broker-package" aria-label="Download Broker Package"><i class="material-icons fs_btn-icon">chrome_reader_mode</i> <span class="fs_btn-text"><?php echo $language['broker_package_button_text']; ?></span></a></li>
                            <?php } ?>
                            <li class="nav-item d-lg-none"><a href="#" class="nav-link fs_nav-link fs_toolbar-link"><i class="material-icons fs_nav-link-icon fs_icon-tools">build</i></a></li>
                        </ul>
                    </div>    
                </div>
            </div>
        </div>
        </header> 
        <div class="fs_carousel-container">
            <div id="fs_suite-carousel" class="coverflow">
                <?php echo $carouselHTML; ?>

                
                
                
<!--                <div class="item"><img src="images/slide2.jpg"></div>
                <div class="item"><img src="images/slide2.jpg"></div>
                <div class="item"><img src="images/slide2.jpg"></div>-->
            </div>
<!--            <a href="#" class="owl-next fs_nav-link fs_carousel-prev"><i class="material-icons fs_nav-link-icon fs_icon-carousel">chevron_left</i></a>
            <a href="#" class="owl-prev fs_nav-link fs_carousel-next"><i class="material-icons fs_nav-link-icon fs_icon-carousel">chevron_right</i></a>-->
        </div>
        <main class="fs_site-main fs_suite-main">
            <div class="fs_container container">
                <header class="fs_page-header">
                    <h1 class="fs_page-title fs_suite-title"><a class="fs_buildingInfoLink" href="building.php?building=<?php echo $building_code; ?>"><?php echo $building_name; ?></a></h1>
                    <span class="fs_slash">&#47;</span>
                    <div class="dropdown fs_dropdown">
                        <button class="btn fs_btn-dropdown-suite dropdown-toggle fs_dropdown-toggle" type="button" id="dropdownSuiteButton" data-toggle="dropdown" data-placement="bottom" aria-haspopup="true" aria-expanded="false">
                            <span class="fs_btn-text"><?php echo $language['suite_text'] . ' ' . $suite_name; ?></span> <i class="material-icons fs_btn-icon">expand_more</i>
                        </button>
                        <div class="dropdown-menu"  aria-labelledby="dropdownSuiteButton">
<?php echo $suiteList; ?>
                        </div>
                    </div>
                    <h2 class="fs_suite-address"><?php echo $building_street . ', ' . $building_city . ', ' . $building_province . ' ' . $building_postal_code; ?></h2>
                </header>
                <div class="fs_site-content">
                    <?php if($suite_description != '') { ?>
                    <section class="fs_section fs_suite-notes">
                        <div class="fs_notes">
                            <!--<h3 class="fs_notes-title"><?php // echo $language['suite_notes_text']; ?></h3>-->
                                <p class="fs_notes-text"><?php echo $suite_description; ?></p>
                           
                        </div>
                    </section>
                    <?php } ?>
                    <div class="row fs_row-main-suite">
                        <div class="col-lg-8 col-xl-9 fs_col-primary">
                    <ul class="nav nav-pills fs_nav-pills" id="fs-tab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link fs_buildingInfoLink" id="fs-info-tab" href="building.php?building=<?php echo $building_code; ?>&lang=<?php echo $thelang; ?>" ><i class="material-icons fs_nav-link-icon">business</i> <span class="fs_nav-link-text"><span class="d-none d-lg-inline-block"><?php echo $language['building_information_text']; ?></span><span class="d-lg-none">Info</span></span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" id="fs-suite-info-tab" data-toggle="pill" href="#fs-suite-info" role="tab" aria-controls="fs-suite-info" aria-expanded="false"><i class="material-icons fs_nav-link-icon">weekend</i> <span class="fs_nav-link-text"><?php echo $language['suites_plural_lowercase']; ?></span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="fs-map-tab" data-toggle="pill" href="#fs-map" role="tab" aria-controls="fs-map" aria-expanded="false"><i class="material-icons fs_nav-link-icon">map</i> <span class="fs_nav-link-text"><?php echo $language['map_text_map']; ?></span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="fs-media-tab" data-toggle="pill" href="#fs-media" role="tab" aria-controls="fs-media" aria-expanded="false"><i class="material-icons fs_nav-link-icon">filter</i> <span class="fs_nav-link-text"><?php echo $language['media_text']; ?></span></a>
                        </li>
                    </ul>









                    <div class="tab-content" id="fs-tab-content">




                        <!--<div class="tab-pane fade show active" id="fs-info" role="tabpanel" aria-labelledby="fs-info-tab">-->
<?php

//if ($building_type == "Industrial" || $building_type == "Industriel") {
//    include("eclipse/includes/building_info_industrial.php");
//} else if ($building_type == "Office" || $building_type == "Bureau") {
//    include("eclipse/includes/building_info_office.php");
//} else if ($building_type == "Retail" || $building_type == "Détail") {
//    include("eclipse/includes/building_info_retail.php");
//}
?>
                        <!--</div>-->
                        <!-- end building info -->


                        <div class="tab-pane fade show active" id="fs-suite-info" role="tabpanel" aria-labelledby="fs-suite-info">
<?php

if ($suite_type == "INDUSTRIAL" || $suite_type == "INDUSTRIEL") {
    include("eclipse/includes/suite_info_industrial.php");
} else if ($suite_type == "OFFICE" || $suite_type == "BUREAU") {
    include("eclipse/includes/suite_info_office.php");
} else if ($suite_type == "RETAIL" || $suite_type == "DETAIL") {
    include("eclipse/includes/suite_info_retail.php");
}
?>                        </div>
                        <!-- end Suite Info -->

                        <div class="tab-pane fade" id="fs-map" role="tabpanel" aria-labelledby="fs-map-tab">
                            <section class="fs_section fs_suite-map">
                                <div id="themap" class="fs_map-canvas embed-responsive embed-responsive-16by9">
                                </div>
                            </section>
                        </div>
                        <!-- end Suite Info -->


                        <div class="tab-pane fade" id="fs-media" role="tabpanel" aria-labelledby="fs-media-tab">
                            <section class="fs_section fs_suite-media">

                                
<?php
        $mediaOutput = suiteMedia($pdf_query, $threedee_query, $sketchfab_query, $suite_preview_images_query, $suite_documents_query, $preview_images_query2, $graphics);
        echo $mediaOutput;
?>
                                    
                                    
                                
                            </section>
                        </div>
                        <!-- end Media -->






                    </div>
                    <!-- end Pills-tab-Content -->




                </div>
                        <div class="col-lg-4 col-xl-3 fs_col-secondary">
                            <section class="fs_section fs_building-contacts">
                                <ul class="fs_building-contacts-list">
                                    <?php echo $contacts_html; ?>
                                </ul>
                            </section>
                        </div>
                            
                    </div>











                    
                       <!-- <section class="fs_section fs_building-contacts d-none d-lg-block">
                                <div class="card fs_card-contacts">
                                    <div class="card-body">
                                        <table class="table fs_contacts-table">
                                            <tbody>
                                                <tr>
                                                    <th scope="row">George Orwell</th>
                                                    <td>Lead Representative</td>
                                                    <td>Morguard</td>
                                                    <td>416-555-1234</td>
                                                    <td><a href="#">georwell@animalfarm.com</a></td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">Margaret Atwood</th>
                                                    <td>Super Lead Representative</td>
                                                    <td>Morguard</td>
                                                    <td>647-555-5678</td>
                                                    <td><a href="#">marge_atwood@handmaid.ca</a></td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">Franz Kafka</th>
                                                    <td>Director, Leasing</td>
                                                    <td>Morguard</td>
                                                    <td>437-912-9101</td>
                                                    <td><a href="#">thefranz@metamorphosis.com</a></td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">Charles Dickens</th>
                                                    <td>Real Estate Consultant</td>
                                                    <td>Morguard</td>
                                                    <td>416-318-1121</td>
                                                    <td><a href="#">chuckd@twocities.com</a></td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">Jane Austen</th>
                                                    <td>Property Manager</td>
                                                    <td>Morguard</td>
                                                    <td>647-555-3145</td>
                                                    <td><a href="#">jane@sensiblesenses.com</a></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>    
                                </div>
                            </section> -->
                    <!-- <div class="fs_lead-contact">
                         <div class="card fs_card-lead-contact">
                             <div class="card-body">
                                 <div class="fs_lead-contact-image"><img src="images/george.jpg" class="img-fluid"></div>
                                 <h3 class="card-title fs_lead-contact-title">George Orwell</h3>
                                 <ul class="fs_lead-contact-list">
                                     <li class="card-text fs_lead-contact-role">Lead Representative</li>
                                     <li class="card-text fs_lead-contact-company">Morguard</li>
                                     <li class="card-text fs_lead-contact-phone">416-555-1234</li>
                                 </ul>
                                 <a href="#" class="btn btn-primary btn-block fs_btn-book">Book a Viewing</a>
                             </div>
                         </div>
                     </div> -->
                </div>
            </div>
        </main>
        
        
        <div class="modal fade" id="fs_modal-print" tabindex="-1" role="dialog" aria-labelledby="fs_modal-print-label" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="fs_modal-print-label">Print to PDF Configuration</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <i class="material-icons">clear</i>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <input id="view-building" type="radio" name="view" value="building" checked="yes" class="form-check-input"> Exclude As Built File
                                            </label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <input id="view-asbuilt" type="radio" name="view" value="asbuilt" class="form-check-input"> Include As Built File
                                            </label>
                                        </div>
                                        <button type="button" class="btn btn-primary btn-block">Print this View</button>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="modal fade" id="fs_modal-share" tabindex="-1" role="dialog" aria-labelledby="fs_modal-share-label" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="fs_modal-share-label"><?php echo $language['share_modal_header']; ?></h5>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <i class="material-icons">clear</i>
                                  </button>
                                </div>
                                <div class="modal-body">
                                    <div class="alert alert-success d-none" role="alert">Request Sent. Thank you!</div>
                                    <div class="alert alert-danger d-none" role="alert">There is an error with your form. Please correct it.</div>

                                  <form>
                                      <div class="form-group">
                                          <label for="recipient-name" class="form-control-label"><?php echo $language['toname_placeholder_text']; ?></label>
                                          <input type="text" class="form-control form-control-danger" id="recipient-name">
                                      </div>
                                      <div class="form-group has-feedback">
                                          <label for="recipient-email" class="control-label"><?php echo $language['toemail_placeholder_text']; ?></label>
                                          <input type="text" class="form-control form-control-danger" id="recipient-email">
                                      </div>
                                      <div class="form-group has-feedback">
                                          <label for="sender-name" class="control-label"><?php echo $language['yourname_placeholder_text']; ?></label>
                                          <input type="text" class="form-control form-control-danger" id="sender-name">
                                      </div>
                                      <div class="form-group has-feedback">
                                          <label for="sender-email" class="control-label"><?php echo $language['youremail_placeholder_text']; ?></label>
                                          <input type="text" class="form-control form-control-danger" id="sender-email">
                                      </div>

                                      <div class="form-group">
                                          <label for="bshare-comments" class="control-label">Message:</label>
                                          <textarea class="form-control" id="bshare-comments"></textarea>
                                      </div>
                                      <div class="form-group">
                                      <label for="cc-me" class="custom-control custom-checkbox">
                                          <input id="cc-me" type="checkbox" name="cc-me" value="" checked="yes" class="custom-control-input">
                                          <span class="custom-control-indicator"></span>
                                          <span class="custom-control-description"><?php echo $language['send_a_copy_text']; ?></span>
                                      </label>
                                      </div>
                                  </form>


                          <!--          <form name="modal_building_share" id="modal_building_share">
                                          <div class="row">
                                              <div class="col-sm-6">
                                                  <input name="bshare_youremail" type="email" id="bshare_youremail" placeholder="<?php echo $language['youremail_placeholder_text']; ?>" class="form-control">
                                              </div>
                                              <div class="col-sm-6">
                                                  <input name="bshare_yourname" id="bshare_yourname" type="text" placeholder="<?php echo $language['yourname_placeholder_text']; ?>" class="form-control">
                                              </div>
                                          </div>
                                          <div class="row">
                                              <div class="col-sm-6">
                                                  <input name="bshare_toemail" type="email" id="bshare_toemail" placeholder="<?php echo $language['toemail_placeholder_text']; ?>" class="form-control">
                                              </div>
                                              <div class="col-sm-6">
                                                  <input name="bshare_toname" id="bshare_toname" type="text" placeholder="<?php echo $language['toname_placeholder_text']; ?>" class="form-control">
                                              </div>
                                          </div>
                                          <div class="row">
                                              <div class="col">
                                                  <textarea name="bshare_notes" id="bshare_notes" placeholder="<?php echo $language['share_placeholder_text1'] . ' ' . $building_street . ' ' . $language['share_placeholder_text2']; ?>" class="form-control" ></textarea>
                                                  <input name="lang" type="hidden" value="<?php echo $lang; ?>" id="lang" />
                                                  <input name="building" type="hidden" value="<?php echo $building_code; ?>" id="building" />
                                                  <input name="urlToShare" type="hidden" value="" id="bshare_query_string" />
                                              </div>
                                          </div>
                                      </form>-->

                                    <div class="modal-body-bottom">
                                      <input type="hidden" id="jsuseripaddress" value="<?php echo $ipaddress; ?>">
                                      <div class="g-recaptcha" id="integrated_captcha" data-sitekey=""></div>
                                      <button type="button" class="btn btn-primary btn-block btn-modal-share-submit">Share</button>
                                    </div>
                                </div>
                              </div>
                            </div>
                          </div>

                        <div class="modal fade" id="fs_modal-property-flyer" tabindex="-1" role="dialog" aria-labelledby="fs_modal-property-flyer-label" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="fs_modal-property-flyer-label">Property Flyer</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <i class="material-icons">clear</i>
                                        </button>
                                    </div>
                                    <div class="modal-body">

                                        <button type="button" class="btn btn-primary btn-block">Print Property Flyer</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="modal fade" id="fs_modal-broker-info" tabindex="-1" role="dialog" aria-labelledby="fs_modal-broker-info-label" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="fs_modal-broker-info-label"><?php echo $language['request_info_suite_title_text']; ?></h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <i class="material-icons">clear</i>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="fs_modal-contact-details">
                                            <ul class="list-unstyled fs_modal-contact-details-list">
                            <!--                    <li>Margaret Atwood</li>
                                                <li>Super Lead Representative</li>
                                                <li>Morguard</li>
                                                <li>416-555-1234</li>-->
                                                <?php echo $contacts_html; ?>
                                            </ul>
                                        </div>
                                        <div class="fs_modal-form-requestinfo">
                                            <form name="modal_requestinfo" id="modal_requestinfo">
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <input name="youremail" type="email" id="youremail" placeholder="<?php echo $language['youremail_placeholder_text']; ?>" class="form-control">
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <input name="yourname" id="yourname" type="text" placeholder="<?php echo $language['yourname_placeholder_text']; ?>" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col">
                                                        <textarea name="notes" id="notes" placeholder="<?php echo $language['notes_placeholder_text1'] . ' ' . $building_street . ' ' . $language['notes_placeholder_text2']; ?>" class="form-control" ></textarea>
                                                        <input name="lang" type="hidden" value="<?php echo $lang; ?>" id="lang" />
                                                        <input name="building" type="hidden" value="<?php echo $building_code; ?>" id="building" />
                                                        <input name="suitename" type="hidden" value="<?php echo $suite_name; ?>" id="suitename" />
                                                        <input name="suite_id" type="hidden" value="<?php echo $suite_id; ?>" id="suite_id" />
                                                        <input name="requesttype" type="hidden" value="Suite" id="requesttype" />
                                                    </div>
                                                </div>
                                            </form>
                                            <button type="button" class="btn btn-primary btn-block btn-modal-requestinfo-submit">Book a Viewing</button>
                                        </div>
                                        <div class="fs_modal-form-infoconfirmation d-none">
                                            <div style="margin: 0px auto;font-size: 15px;"><?php echo $language['thank_you_info_request_message']; ?></div><br><br>
                                            <div id="thanks"></div>
                                        </div><!-- /.fs_modal-form-infoconfirmation --> 
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="modal fade" id="fs_modal-broker-package" tabindex="-1" role="dialog" aria-labelledby="fs_modal-broker-package-label" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="fs_modal-broker-package-label"><?php echo $language['broker_package_title_text']; ?></h5>                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <i class="material-icons">clear</i>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <p class="modal-copy"><?php echo $language['broker_package_paragraph1_text']; ?></p>
                                        <form name="modal_requestbrokerpackage" id="modal_requestbrokerpackage">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <input name="packageemail" type="text" id="packageemail" placeholder="<?php echo $language['youremail_placeholder_text']; ?>" class="form-control">
                                                    <input name="lang" type="hidden" value="<?php echo $lang; ?>" id="lang" />
                                                    <input name="building" type="hidden" value="<?php echo $building_code; ?>" id="building" />
                                                    <input name="suitename" type="hidden" value="<?php echo $suite_name; ?>" id="suitename" />
                                                    <input name="suite_id" type="hidden" value="<?php echo $suite_id; ?>" id="suite_id" />
                                                    <input name="packagetype" type="hidden" value="Suite" id="packagetype" />
                                                </div>
                                                <div class="col-sm-6">
                                                    <input name="packagename" type="text" id="packagename" placeholder="<?php echo $language['yourname_placeholder_text']; ?>" class="form-control">
                                                </div>
                                            </div>
                                        </form>
                                        <button type="button" class="btn btn-primary btn-block btn-modal-brokerpackage-submit"><?php echo $language['submit_text']; ?></button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="modal fade" id="fs_modal-alerts" tabindex="-1" role="dialog" aria-labelledby="fs_modal-alerts-label" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="fs_modal-alerts-label">Set Alerts</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <i class="material-icons">clear</i>
                                        </button>
                                    </div>
                                    <div class="modal-body">

                                        <button type="button" class="btn btn-primary btn-block">Set Alerts</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="fs_toolbar">
                            <div class="fs_toolbar-inner">
                                <div class="fs_toolbar-actions clearfix">
                                    <button type="button" class="close" aria-label="Close">
                                        <i class="material-icons">clear</i>
                                    </button>
                                </div>
                                <ul class="nav flex-column fs_toolbar-menu">
                                    <li class="nav-item">
                                        <a class="nav-link" href="#collapseLanguageMenu" data-toggle="collapse" aria-expanded="false" aria-controls="collapseLanguageMenu"><i class="material-icons fs_nav-link-icon fs_icon-language">language</i> <span class="fs_nav-link-text">Language</span><i class="material-icons fs_nav-link-icon float-right">expand_more</i></a>
                                        <div class="collapse" id="collapseLanguageMenu">
                                            <ul class="nav flex-column">
                                                <li class="nav-item"><a href="#" class="nav-link">English</a></li>
                                                <li class="nav-item"><a href="#" class="nav-link">French</a></li>
                                            </ul>
                                        </div>
                                    </li>
<!--                                    <li class="nav-item">
                                        <a href="#" class="nav-link fs_nav-link" data-toggle="modal" data-target="#fs_modal-alerts" aria-label="Set alerts"><i class="material-icons fs_nav-link-icon fs_icon-subscribe">notifications</i> <span class="fs_nav-link-text">Set Alerts</span></a>
                                    </li>-->
                                    <li class="nav-item">
                                        <a href="#" class="nav-link" data-toggle="modal" data-target="#fs_modal-print" aria-label="Print to PDF"><i class="material-icons fs_nav-link-icon">print</i> <span class="fs_nav-link-text">Print</span></a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="#" class="nav-link" data-toggle="modal" data-target="#fs_modal-share" aria-label="Share"><i class="material-icons fs_nav-link-icon">share</i> <span class="fs_nav-link-text">Share</span></a>
                                    </li>
                                </ul>

                            </div>
                        </div>


<?php if ($theme['clientjs'] == 1) { 
    include('theme/clientjs.php');
 } ?>
        <script type="text/javascript">
            
            
            $(document).ready(function () {
                //intialize carousel
                
                
        var carouselPresent = <?php echo $theCarousel; ?>;
        
        if (carouselPresent === 1){
            $('.coverflow').coverflow({
                innerAngle : 0,
                outerAngle : 0,
                innerOffset : 0,
                density : 1.25,
                select: function(event, ui){
//                        console.log('here');
                }
            });
        }                
                
                
//                owl = $('#fs_suite-carousel');
//                owl.owlCarousel({
//                    loop: false,
//                    margin: 0,
//                    nav: false,
//                    dots: true,
//                    items: 1,
//                    responsive: {
//                        0: {
//                        },
//                        640: {
//                        },
//                        768: {
//                        }
//                    }
//                });
//
//
//                //carousel controls
//                // Go to the next item
//                $('body').on('click', '.fs_carousel-next', function (e) {
//                    e.preventDefault();
//                    owl.trigger('next.owl.carousel');
//                });
//                // Go to the previous item
//                $('body').on('click', '.fs_carousel-prev', function (e) {
//                    e.preventDefault();
//                    owl.trigger('prev.owl.carousel');
//                });

                $('.fs_toolbar-link').on('click', function (e) {
                    $('#fs_toolbar').addClass('open');
                });

                $('#fs_toolbar .close').on('click', function (e) {
                    $('#fs_toolbar').removeClass('open');
                });


                function validatePackageEmail()
                {

                    var emailID2 = document.getElementById('packageemail').value;
                    atpos = emailID2.indexOf("@");
                    dotpos = emailID2.lastIndexOf(".");
                    if (atpos < 1 || (dotpos - atpos < 2))
                    {
                        return false;
                    }
                    return(true);
                }



                $('.btn-modal-brokerpackage-submit').on('click', function (e) {


                    // VALIDATE BEFORE AJAX




                    var validated = 1;
                    if (document.modal_requestbrokerpackage.packageemail.value == "") {
                        e.preventDefault();
                        alert("<?php echo $language['form_verification_message_email']; ?>");
                        document.modal_requestbrokerpackage.packageemail.focus();
                        validated--;
                        return false;
                    }



                    if (!validatePackageEmail())
                    {
<?php if ($lang_id == '1') { ?>
                            alert("Please use a valid Email address.");
<?php } else { ?>
                            alert("S'il vous plaît utiliser une courreil valide.");
<?php } ?>
                        document.modal_requestbrokerpackage.packageemail.focus();
                        return false;

                    }



                    if (document.modal_requestbrokerpackage.packagename.value == "") {
                        e.preventDefault();
                        alert("<?php echo $language['form_verification_message_name']; ?>");
                        document.modal_requestbrokerpackage.packagename.focus();
                        validated--;
                        return false;
                    }





                    if (validated > 0) {
                        // END VALIDATE BEFORE AJAX


                        packageemail = document.modal_requestbrokerpackage.packageemail.value;
                        packagename = document.modal_requestbrokerpackage.packagename.value;

                        $('#modal-request-brokerpackage').modal('toggle');

                        $.ajax({
                            type: "GET",
                            url: "broker_packages.php?process=createPackage&lang=<?php echo $lang_id; ?>&building=<?php echo $building_code; ?>&building_id=<?php echo $building_id; ?>&suitename=<?php echo $suite_name; ?>&suite_id=<?php echo $suite_id; ?>&packagetype=Suite",
                            //                data: $('form#modal_requestinfo').serialize(),
                            success: function (response) {
                                var filename = response;
                                var fullpath = 'images/broker_packages/' + response;
                                window.location.href = fullpath;

                                $.get("broker_packages.php", {
                                    process: "sendEmail",
                                    fromemail: packageemail,
                                    fromname: packagename,
                                    building: "<?php echo $building_code; ?>",
                                    suitename: "<?php echo $suite_name; ?>",
                                    suite_id: "<?php echo $suite_id; ?>",
                                    lang: "<?php echo $lang_id; ?>",
                                    suitesqft: "<?php echo $suite_net_rentable_area; ?>",
                                    packagetype: "Suite"
                                }, function (response) {
                                    // no action in callback
                                });
                            },
                            error: function () {
                            }
                        });



                    }// end if (validated = 0)
                });


                function validateEmail()
                {

                    //var emailID = document.register.email.value;
                    var emailID = document.getElementById('youremail').value;
                    atpos = emailID.indexOf("@");
                    dotpos = emailID.lastIndexOf(".");
                    if (atpos < 1 || (dotpos - atpos < 2))
                    {

                        return false;
                    }
                    return(true);
                }

                $('#fs_modal-broker-info').on('hidden.bs.modal', function (e) {
                    $('.modal-form-requestinfo').removeClass('d-none');
                    $('.modal-form-infoconfirmation').addClass('d-none');
                    $('.btn-modal-requestinfo-submit').removeClass('d-none');
                });

                $('.btn-modal-requestinfo-submit').on('click', function (e) {


                    // VALIDATE BEFORE AJAX


                    var validated = 1;
                    if (document.modal_requestinfo.youremail.value == "") {
                        e.preventDefault();
                        alert("<?php echo $language['form_verification_message_email']; ?>");
                        document.modal_requestinfo.youremail.focus();
                        validated--;
                        return false;
                    }


                    if (!validateEmail())
                    {
<?php if ($lang_id == '1') { ?>
                            alert("Please use a valid Email address.");
<?php } else { ?>
                            alert("S'il vous plaît utiliser une courreil valide.");
<?php } ?>
                        document.modal_requestinfo.youremail.focus();
                        return false;

                    }

                    if (document.modal_requestinfo.yourname.value == "") {
                        e.preventDefault();
                        alert("<?php echo $language['form_verification_message_name']; ?>");
                        document.modal_requestinfo.yourname.focus();
                        validated--;
                        return false;
                    }



                    if (validated > 0) {
                        // END VALIDATE BEFORE AJAX
                        var subbedlang = '&lang=' + document.modal_requestinfo.lang.value;
                        $.ajax({
                            type: "GET",
                            url: "send_inforequest_email.php?<?php echo $_SERVER['QUERY_STRING']; ?>&suitesqft=<?php echo $suite_net_rentable_area; ?>",
                            data: $('form#modal_requestinfo').serialize(),
                            success: function (uniqueid) {
                                $('.fs_modal-form-requestinfo').addClass('d-none');
                                $('.fs_modal-form-infoconfirmation').removeClass('d-none');
                                $('.btn-modal-requestinfo-submit').addClass('d-none');
                                $('#fs_modal-broker-info').scrollTop(0);

                            },
                            error: function () {
                                //    alert("failure");
                            }
                        });
                    }// end if (validated = 0)
                });





// activate tab on click
                $('#fs-tab').on('click', 'a', function (e) {
                    e.preventDefault();

                    if (this.href.indexOf("building.php") > -1) {
//                       console.log("building click");
//                       window.location.href = this.href;
                    } else {
                        var $this = $(this);
//                        console.log("this[0].hash");
//                        console.log($this[0].hash);
                        var $hash = $this[0].hash;
                    
                    if ($hash === "#fs-map"){
                        updateMap();
                    }
                    var prefix="tab_";
                    $hash = $hash.replace("#", "#" + prefix);
                    
                        //prevent page jump/scroll on hash change
                        history.pushState(null, null, $hash);
                    }
                    

                });

                // will show tab based on hash
                function refreshHash() {
//                    $('#fs-tab').find('a[href="' + window.location.hash + '"]').tab('show');

                    var hash = window.location.hash;
                    var prefix = "tab_";
                    $('#fs-tab').find('a[href="' +hash.replace(prefix,"")+ '"]').tab('show');
                    
//                    $('.nav-tabs a[href="'+hash.replace(prefix,"")+'"]').tab('show');
                    
                    if (window.location.hash === "#tab_fs-map"){
                        updateMap();
                    }
                }

                // show tab if hash changes in address bar
                $(window).bind('hashchange', refreshHash);

                // read has from address bar and show it
                if (window.location.hash) {
                    // show tab on load
                    refreshHash();
                }


    // hey look the languages work
    $(".langclick").on('click', function(e) {
        e.preventDefault();
        // get the language clicked from data-language
        var selectedLang = $(this).data('language');
        // take current URL
        var urlNow = window.location.href;
        
        // get any hashes in the url
        var urlHash = window.location.hash;
        console.log("urlHash");
        console.log(urlHash);
        
        // remove any language reference and hashes or stray #'s
        urlNow = urlNow.replace(/&lang=en_CA/g,"").replace(/&lang=fr_CA/g,"").replace(/\?lang=en_CA/g,"").replace(/\?lang=fr_CA/g,"").replace(urlHash, "").replace(/#/g,"");
        // add new language reference
        if (urlNow.indexOf("?") > -1) {
            var langUrl = urlNow+"&lang="+selectedLang+urlHash;
        } else {
            var langUrl = urlNow+"?lang="+selectedLang+urlHash;
        }
        // reload the page with new url
        window.location.href = langUrl;
    });


//    // our back button takes you right back 
//    // to the last state of the search page    
//    var theUrl = window.location.href;
//    
//    theUrl = theUrl.replace("suite.php", "index.php").replace("&building=<?php echo $building_code; ?>", "").replace("?building=<?php echo $building_code; ?>", "").replace("&suiteid=<?php echo $suite_id; ?>", "").replace("?suiteid=<?php echo $suite_id; ?>", "");
//
//    // count remaining query params with &
//    var ampersands = theUrl.split("&").length -1;
//    // check if only one ampersand left in url
//    if (ampersands === 1) {
//        // it's probably lang but check to be sure
//        if ((theUrl.match(/&lang/g)||[]).length > 0){
//            // change the &lang to ?lang so it works
//            theUrl = theUrl.replace("&lang", "?lang");
//        }
//    }
//    
//    $('body').on('click','.fs_btn-back, .fs_back-link', function(e) {
//        window.location.href = theUrl;
//    });




    function updateMap() {
        setTimeout(function () {
////            theMap.refresh();
//            // nudge the map to fix the markerClusterer bug
//            // https://github.com/googlemaps/v3-utility-library/issues/252
////            theMap.setCenter((mapcenterLa + 0.0000001), mapcenterLg);
//            console.log("resize triggered");
////            google.maps.event.trigger(map,'resize');
////            google.maps.event.trigger(google.maps.map , 'resize')
            var center = map.getCenter();
            console.log("center");
            console.log(center);
            google.maps.event.trigger(map, "resize");
            map.setCenter(center); 
//
        }, 500);

//google.maps.event.trigger(map, 'resize');
    }
    
    
    
    
    var map;
    
//    function displayMap() {
//        document.getElementById('themap').style.display="block";
//        initialize();
//        setTimeout(function () {
////            theMap.setCenter((mapcenterLa + 0.0000001), mapcenterLg);
//            populateMap();
//        }, 250);
//    }
    
    
function initialize() {
  map = new google.maps.Map(document.getElementById("themap"), {
    center: {lat: <?php echo $latitude; ?>, lng: <?php echo $longitude; ?>},
    zoom: 14
  });
  
  
  
  
  
  
    var bounds = new google.maps.LatLngBounds();
    var buildingName = "<?php echo $building_name; ?>";
    var buildingType = "<?php echo $building_type; ?>";
    var streetAddress = "<?php echo $street_address; ?>";
    var city = "<?php echo $city; ?>";
    var province = "<?php echo $province; ?>";
    var postal = "<?php echo $postal_code; ?>";
    var buildingCode = "<?php echo $building_code; ?>";
    var latitude = "<?php echo $latitude; ?>";
    var longitude = "<?php echo $longitude; ?>";
    
    var thumbnail = "<?php echo $building_thumbnail_image_name_list; ?>";
    var numSuites = "<?php echo $suiteCounter; ?>";
    
    
    
    var suiteTextOutput = '';
    var markers = [];
    var infoWindowContent = [];
    
    var theLanguage = "<?php echo $lang; ?>";
    if (theLanguage === "fr_CA") {
    if (numSuites === 0) {
        if (buildingType === "Retail" || buildingType == "Détail"){
            suiteTextOutput = "Appelez-nous pour connaître les espaces disponible";
        } else {
            suiteTextOutput = "Ce bâtiment est actuellement loué à 100%";
        }
    } else {
        suiteTextOutput = numSuites + " Bureaux disponibles";
    }
} else {
    if (numSuites === 0) {
        if (buildingType === "Retail" || buildingType == "Détail"){
            suiteTextOutput = "Call For Availability";
        } else {
            suiteTextOutput = "This building is currently 100% leased";
        }
    } else {
        suiteTextOutput = numSuites + " Suites Available";
    }
}
    
    

    markers.push([buildingName, latitude, longitude, buildingCode, numSuites]);
            
            if (buildingName === streetAddress) {
                buildingName = "";
            } else {
                buildingName = '<span class="fs_iw-building-name">' + buildingName + '</span><br>';
            }
            infoWindowContent.push(['<div class="fs_infoWindow">' +
                    '<span class="fs_iw-close"><button type="button" class="fs_btn-iw-close"><i class="material-icons fs_icon-iw-close">clear</i></button></span><div class="fs_iw-image"><a class="fs_building-link" href="building.php?building='+buildingCode+'"><img alt="" src="' + thumbnail + '"></a></div>' +
                    '<div class="fs_iw-building">' + buildingName + '<span class="fs_iw-building-address">' + streetAddress + '</span><br><span class="fs_iw-building-city">' + city + '</span>, <span class="fs_iw-building-province">' + province + '</span><br><span class="fs_iw-building-postal-code">' + postal + '</span></div>' +
                    '<div class="fs_iw-available"><a class="fs_map-suites-link" href="building.php?building='+buildingCode+'">'+suiteTextOutput+'</div>' +
                    '</div>']);
    
    
    infoWindow = new google.maps.InfoWindow();
    var marker;
    var i;
    var theMarkerArray = [];
    // Loop through our array of markers & place each one on the map  
    for( i = 0; i < markers.length; i++ ) {
        var markerNumber = markers[i][4].toString();
        if (markerNumber == '0') {
            markerNumber = ' ';
        }
        var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
        bounds.extend(position);
        marker = new google.maps.Marker({
            position: position,
            map: map,
            title: markers[i][0],
            bcode: markers[i][3],
            icon: 'theme/fs_marker-icon.png',
//            label: markers[i][4].toString()
            label: {
                text: markerNumber,
                color: "#fff",
                fontSize: "12px",
                fontWeight: "bold"
            }
        });
    theMarkerArray.push(marker);
        // Allow each marker to have an info window    
        google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
                infoWindow.setContent(infoWindowContent[i][0]);
                infoWindow.open(map, marker);
            };
        })(marker, i));

        
    }    
    
    
//    map.fitBounds(bounds);
    
    // manipulating the map
$('body').on('click', '.fs_btn-iw-close', function(e) {
//    map.hideInfoWindows();
    infoWindow.close();
    
});
           google.maps.event.addListener(infoWindow, 'domready', function() {
              
              // Reference to the DIV which receives the contents of the infowindow using jQuery
              var iwOuter = $('.gm-style-iw');
              
              //reset width and height of parent div to better position assets (eg. close button)
              //iwOuter.parent().css({'width' : '225px', 'height' : '125px'});
              
              //iwOuter.parent().addClass('infowindow-parent');

              /* The DIV we want to change is above the .gm-style-iw DIV.
               * So, we use jQuery and create a iwBackground variable,
               * and took advantage of the existing reference to .gm-style-iw for the previous DIV with .prev().
               */
              var iwBackground = iwOuter.prev();

              // Remove the background shadow DIV
              iwBackground.children(':nth-child(2)').css({'display' : 'none'});

              // Remove the white background DIV
              iwBackground.children(':nth-child(4)').css({'display' : 'none'});
              
              
              
              // Moves the infowindow 115px to the right.
              //iwOuter.parent().parent().css({left: '115px'});
              
              // Moves the shadow of the arrow 76px to the left margin 
                //iwBackground.children(':nth-child(1)').attr('style', function(i,s){ return s + 'left: 76px !important;'});
                
                //hide the shadow of the arrow
                iwBackground.children(':nth-child(1)').css({'display' : 'none'});
                
                //hide the arrow
                iwBackground.children(':nth-child(3)').css({'display' : 'none'});

                // Moves the arrow 76px to the left margin 
                //iwBackground.children(':nth-child(3)').attr('style', function(i,s){ return s + 'left: 76px !important;'});
                
                
                // selector for info window close button
                var iwCloseBtn = iwOuter.next();
                
                //hide the close button since we are adding our own custom close to infowindow
                iwCloseBtn.css('display', 'none');

                // Apply the desired effect to the close button
                //iwCloseBtn.css({
                 // opacity: '1', // by default the close button has an opacity of 0.7
                  //right: '0px', top: '0px' // button repositioning
                  //border: '7px solid #48b5e9', // increasing button border and new color
                  //'border-radius': '13px', // circular effect
                  //'box-shadow': '0 0 5px #3990B9' // 3D effect to highlight the button
                  //});

                // The API automatically applies 0.7 opacity to the button after the mouseout event.
                // This function reverses this event to the desired value.
                //iwCloseBtn.mouseout(function(){
                //  $(this).css({opacity: '1'});
                //});

           });    
           
//  map.fitBounds(bounds);
  
  
}
// engage the map!
initialize();








function validatePackageEmail()
                {

                    var emailID2 = document.getElementById('packageemail').value;
                    atpos = emailID2.indexOf("@");
                    dotpos = emailID2.lastIndexOf(".");
                    if (atpos < 1 || (dotpos - atpos < 2))
                    {
                        return false;
                    }
                    return(true);
                }
                
                function validateEmail()
                {

                    var emailID = document.getElementById('youremail').value;
                    atpos = emailID.indexOf("@");
                    dotpos = emailID.lastIndexOf(".");
                    if (atpos < 1 || (dotpos - atpos < 2))
                    {

                        return false;
                    }
                    return(true);
                }
                
                
                
                
                
                
                
            

                    $('body').on('click','.btn-modal-brokerpackage-submit', function(e) {
//                $('.btn-modal-brokerpackage-submit').on('click', function (e) {


                    // VALIDATE BEFORE AJAX




                    var validated = 1;
                    if (document.modal_requestbrokerpackage.packageemail.value == "") {
                        e.preventDefault();
                        alert("<?php echo $language['form_verification_message_email']; ?>");
                        document.modal_requestbrokerpackage.packageemail.focus();
                        validated--;
                        return false;
                    }



                    if (!validatePackageEmail())
                    {
                        if (theLanguage === "fr_CA") {
                            alert("S'il vous plaît utiliser une courreil valide.");
                        } else {
                            alert("Please use a valid Email address.");
                        }
                        document.modal_requestbrokerpackage.packageemail.focus();
                        return false;

                    }



                    if (document.modal_requestbrokerpackage.packagename.value == "") {
                        e.preventDefault();
                        alert("<?php echo $language['form_verification_message_name']; ?>");
                        document.modal_requestbrokerpackage.packagename.focus();
                        validated--;
                        return false;
                    }





                    if (validated > 0) {
                        // END VALIDATE BEFORE AJAX

                        packageemail = document.modal_requestbrokerpackage.packageemail.value;
                        packagename = document.modal_requestbrokerpackage.packagename.value;

                        $('#fs_modal-broker-package').modal('toggle');
                        var thePackageBuilding = $('#buildingPackage').val();
                        var thePackageBuildingID = $('#buildingPackageID').val();
                        var theLanguage = "<?php echo $lang; ?>";
                        $.ajax({
                            type: "GET",
                            url: "broker_packages.php?process=createPackage&lang="+theLanguage+"&building="+thePackageBuilding+"&building_id="+thePackageBuildingID+"&packagetype=Building",

                            success: function (response) {
                                var filename = response;
                                var fullpath = 'images/broker_packages/' + response;
                                window.location.href = fullpath;

                                $.get("broker_packages.php", {
                                    process: "sendEmail",
                                    fromemail: packageemail,
                                    fromname: packagename,
                                    building: "<?php echo $building_code; ?>",
                                    lang: "<?php echo $lang_id; ?>",
                                    packagetype: "Building"
                                }, function (response) {
                                    // no action in callback
                                });
                            },
                            error: function () {

                            }
                        });

                    }// end if (validated = 0)
                });
                
                
                $('#fs_modal-broker-info').on('hidden.bs.modal', function (e) {
                    $('.modal-form-requestinfo').removeClass('d-none');
                    $('.modal-form-infoconfirmation').addClass('d-none');
                    $('.btn-modal-requestinfo-submit').removeClass('d-none');
                });


                $('body').on('click','.btn-modal-requestinfo-submit', function(e) {
                    e.preventDefault();
                    console.log("requestinfo submitted");
                    // VALIDATE BEFORE AJAX
                    var validated = 1;
                    if (document.modal_requestinfo.youremail.value == "") {
                        e.preventDefault();
                        alert("<?php echo $language['form_verification_message_email']; ?>");
                        document.modal_requestinfo.youremail.focus();
                        validated--;
                        return false;
                    }


                    if (!validateEmail())
                    {
                        
                        
                        
                        if (theLanguage === "fr_CA") {
                            alert("S'il vous plaît utiliser une courreil valide.");
                        } else {
                            alert("Please use a valid Email address.");
                        }

                        document.modal_requestinfo.youremail.focus();
                        return false;

                    }

                    if (document.modal_requestinfo.yourname.value == "") {
                        e.preventDefault();
                        alert("<?php echo $language['form_verification_message_name']; ?>");
                        document.modal_requestinfo.yourname.focus();
                        validated--;
                        return false;
                    }



                    if (validated > 0) {
                        // END VALIDATE BEFORE AJAX
                        $.ajax({
                            type: "GET",
                            url: "send_inforequest_email.php?<?php echo $_SERVER['QUERY_STRING']; ?>",
                            data: $('form#modal_requestinfo').serialize(),
                            success: function (uniqueid) {
                                $('.fs_modal-form-requestinfo').addClass('d-none');
                                $('.fs_modal-form-infoconfirmation').removeClass('d-none');
                                $('.btn-modal-requestinfo-submit').addClass('d-none');
                                $('#fs_modal-broker-info').scrollTop(0);

                            },
                            error: function () {

                            }
                        });
                    }// end if (validated = 0)
                });
                
                
                
$('#fs_modal-share').on('hidden.bs.modal', function (e) {
    $('#fs_modal-share input').parent().removeClass('alert alert-danger');
});

    $('#fs_modal-share .btn-modal-share-submit').on('click', function(e) {
        $('#fs_modal-share input').parent().removeClass('alert alert-danger');

        var urlToShare = window.location.href;
//        console.log("urlToShare");
//        console.log(urlToShare);

        var tname = $('#fs_modal-share #recipient-name').val();
        var temail = $('#fs_modal-share #recipient-email').val();
        var fname = $('#fs_modal-share #sender-name').val();
        var femail = $('#fs_modal-share #sender-email').val();
        var fcomments = $('#fs_modal-share #bshare-comments').val();



        var error = 0;

        if(tname === '') {
            $('#fs_modal-share #recipient-name').parent().addClass('alert alert-danger');
//            $('#fs_modal-share .alert-danger').removeClass('d-none');
            error = error+1;
        }

        if(temail === '') {
            $('#fs_modal-share #recipient-email').parent().addClass('alert alert-danger');
//            $('#fs_modal-share .alert-danger').removeClass('d-none');
            error = error+1;
        }

        if(fname === '') {
            $('#fs_modal-share #sender-name').parent().addClass('alert alert-danger');
//            $('#fs_modal-share .alert-danger').removeClass('d-none');
            error = error+1;
        }

        if(femail === '') {
            $('#fs_modal-share #sender-email').parent().addClass('alert alert-danger');
//            $('#fs_modal-share .alert-danger').removeClass('d-none');
            error = error+1;
        }
        
console.log("error");
console.log(error);
        if (error === 0) {            
            $.ajax({
                type: "GET",
                url: "verifyemail.php",
                data: {fromemail:femail,toemail:temail},
                beforeSend: function() {
                    $('#fs_modal-share .modal-body').prepend('<div class="loader"><img src="images/loader.svg"></div>');
                },
                success: function(data){
                    
                },
                error: function(){
                    
                }
            }).done( function(data) {
                if (data === '66') {
                    $('.loader').remove();
                    $('#fs_modal-share #recipient-email').parent().addClass('alert alert-danger');
                    $('#fs_modal-share .alert-danger').removeClass('hidden');
                    error = error+1;
                } else if (data === '77') {
                    $('.loader').remove();
                    $('#fs_modal-share #sender-email').parent().addClass('alert alert-danger');
                    $('#fs_modal-share .alert-danger').removeClass('hidden');
                    error = error+1;
                } else if (data === '88') {
                    $('.loader').remove();
                    $('#fs_modal-share #recipient-email').parent().addClass('alert alert-danger');
                    $('#fs_modal-share #sender-email').parent().addClass('alert alert-danger');
                    $('#fs_modal-share .alert-danger').removeClass('hidden');
                    error = error+1;
                } else {
                    $('#fs_modal-share input').parent().removeClass('alert alert-danger');
                    // email verified in PHP OK - start recaptcha
                    // console.log("recaptcha started");
                    var googleinfo = grecaptcha.getResponse(integrated_captcha);
                    var ipaddress = $("#jsuseripaddress").val();

                    $.ajax({
                        type: "GET",
                        url: "docaptcha.php",
                        data: {response: googleinfo, remoteip:ipaddress},
                        success: function(data){

                        },
                        error: function(){

                        }
                    }).done(function(data) {

                        var newdata = JSON.parse(data);
                        var reply = newdata[0][0];

                        if (newdata[0].length === 2) {
                            var error = newdata[0][1][0];
                        }

                        if (reply === '66') {
                            $('.loader').remove();
                            alert("Are you sure you're not a robot? Please try again.")
                            error = error+1;
                        } else {
                            var ccme = false;
                            if ($("#cc-me").is(':checked')) {
                                ccme = true;
                            }
                            // captcha succes, send email
                            $.ajax({
                                type: "GET",
                                url: "share_a_link.php",
                                data: {toname:tname,toemail:temail,fromname:fname,fromemail:femail,fromcomments:fcomments,urltoshare:urlToShare,requesttype:"Suite",buildingname:"<?php echo $building_name; ?>",buildingstreet:'<?php echo $building_street; ?>', buildingcity:'<?php echo $building_city; ?>',buildingprovince:'<?php echo $building_province;?>',suiteselected:'<?php echo $suite_name; ?>',ccme:ccme},
                                    beforeSend: function() {

                                    },
                                    complete: function(){
                                        //hide preloader
                                        $('.loader').remove();
                                    },
                                    success: function(data){

                                    },
                                    error: function(){

                                    }
                            }).done(function(data) {
                                // console.log("data returned from sentctcrequest.php: ");
                                // console.log(data);
                                $('#fs_modal-share #sender-email, #fs_modal-share #sender-name').parent().removeClass('alert alert-danger');
                                if(data == 66) {
                                    //email error
                                    $('#fs_modal-share #sender-email').parent().addClass('alert alert-danger');
                                    $('#fs_modal-share .alert-danger').removeClass('hidden');
                                }
                                else {
                                    $('#fs_modal-share #sender-email, #fs_modal-share #sender-name').parent().removeClass('alert alert-danger');
                                    $('#fs_modal-share .alert-success').removeClass('hidden');
                                    $('#fs_modal-share .alert-danger').addClass('hidden');
                                    $('textarea, :input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
                                    $('#fs_modal-share').modal('toggle');
                                }
                            });
                        } // end else
                    });
                } //end else 
            });
        }
    });



//fix contacts div on scroll
    var targetElement = $('.fs_building-contacts');
    var elementTop = $('.fs_building-contacts').offset().top;
    var elementWidth = $('.fs_building-contacts').outerWidth();
    var headerHeight = $('.fs_site-header').height();
    //console.log(elementTop - ($(window).scrollTop() + headerHeight));

  $(window).scroll(function() {
    //console.log(elementTop - ($(window).scrollTop() + headerHeight));
    
    if( window.innerWidth > 991 ) {
        var relativePos = elementTop - ($(window).scrollTop() + headerHeight+40);

        if ( relativePos <= 0) {
            targetElement.addClass('fixed');
            targetElement.css('top', headerHeight+40);
            //targetElement.css({'position':'fixed', 'top': headerHeight+40, 'width': elementWidth})
        } else {
            targetElement.removeClass('fixed');
            targetElement.css('top', 'auto');
            //targetElement.css({'position':'static', 'top': 'auto', 'width': 'auto'})
        }
    }
    else {
        targetElement.removeClass('fixed');
        targetElement.css('top', 'auto');
    }
  });



    });// end document.ready
        </script>
    </body>
</html>

