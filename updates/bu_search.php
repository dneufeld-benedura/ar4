<?php

		$lang = "en_CA";
		$lang_id = 1;
		if (isset($_REQUEST['lang'])){
			if ($_REQUEST['lang'] == "fr_CA"){
			//	
				$lang = "fr_CA";
				$lang_id = 0;
			}else{
		//				
			}
		}else{
		//	
		}
		

include('../theme/db.php');
include('../objects/Queries.php');
include('../objects/ErrorMessages.php');
include('../objects/SearchData.php');
include('../objects/LanguageQuery.php');
include('../objects/FormatData.php');
include('../objects/DisplayHTML.php');

		

		// Look for subscriptions passed in from external forms
                $buemail = NULL;
                $bufirstname = NULL;
                $bulastname = NULL;
                $bufullname = NULL;

                if (isset($_REQUEST['email'])) {
                $buemail = $_REQUEST['email'];
                }
                if (isset($_REQUEST['firstname'])) {
                $bufirstname = $_REQUEST['firstname'];
                }
                if (isset($_REQUEST['lastname'])) {
                $bulastname = $_REQUEST['lastname'];
                }
                if ($bufirstname == '' && $bulastname == '') {
                $bufullname = NULL;
                } else {
                $bufullname = $bufirstname . ' ' . $bulastname;
                }
		


		//Initiate DisplayHTML
		$displayHTML = new DisplayHTML();
	
		// Initiate the Queries class
		$queries = new Queries();
		
		//Initiate FormatData
		$formatData = new FormatData();
		
		//Initiate the Search Data class
		$searchData = new SearchData();

		// Language Query
		$languageQuery = new LanguageQuery();
		$langArray = $languageQuery -> getLanguageAndID();
		$lang = $langArray[0];
		$lang_id = $langArray[1];
		$language_query_string = 'select * from languages_dynamic ld, languages l where ld.lang_id = ' . $lang_id . ' and l.lang_id = ' . $lang_id;

		$language_query = mysql_query($language_query_string)or die("language query error: ". mysql_error());
		$language = mysql_fetch_array($language_query);



	   // Theme Query
	   $theme_query = mysql_query("select * from theme");
	   $theme = mysql_fetch_array($theme_query); 
	   
	   //setting locale for time, date, money, etc
	   setlocale(LC_ALL, $language['locale_string']);
	   date_default_timezone_set("Canada/Eastern");
	
	

//setting locale for time, date, money, etc
date_default_timezone_set('America/New_York');
setlocale(LC_ALL, $language['locale_string']);

// Initiate the Queries class
$queries               = new Queries();
//Initiate the ErrorMessages class
$errors                = new ErrorMessages();
//Initiate the Search Data class
$searchData            = new SearchData();
//Initiate the FormatData class
$formatData            = new FormatData();
//Initiate DisplayHTML
$displayHTML           = new DisplayHTML();
// Language Query
$languageQuery         = new LanguageQuery();
$langArray             = $languageQuery->getLanguageAndID();
$lang                  = $langArray[0];
$lang_id               = $langArray[1];
$language_query_string = 'select * from languages_dynamic ld, languages l where ld.lang_id = ' . $lang_id . ' and l.lang_id = ' . $lang_id;

$language_query = mysql_query($language_query_string) or die("language query error: " . mysql_error());
$language = mysql_fetch_array($language_query);

// Theme Query
$theme_query = mysql_query("select * from theme");
$theme       = mysql_fetch_array($theme_query);


//getting the page search form results
		$current_page_result         = $searchData->get_current_page_result();
		$province_selected           = $searchData->get_province_selected($language);
		$buildingtype_selected       = $searchData->get_buildingtype_selected();
		$suitetype_selected          = $searchData->get_suitetype_selected();
		$region_selected             = $searchData->get_region_selected();
		$sub_region_selected         = $searchData->get_sub_region_selected();
		$size_selected               = $searchData->get_size_selected();
		$availability_selected_array = $searchData->get_availability_selected();
		$availability_notafter       = $availability_selected_array['availability_notafter'];
		$availability_notbefore      = $availability_selected_array['availability_notbefore'];
		$building_name_sterm_array  = $searchData->get_building_name_sterm_array();
		$building_name_sterm        = $building_name_sterm_array['building_name_sterm'];
		$building_name_sterm_prepop = $building_name_sterm_array['building_name_sterm_prepop'];
        $buildingtype_selected       = $searchData->get_buildingtype_selected();
        $province_selected           = $searchData->get_province_selected($language);
        $region_selected             = $searchData->get_region_selected();
        $sub_region_selected         = $searchData->get_sub_region_selected();
        $building_codes_narrow       = $searchData->get_building_codes_narrow();
        $availability_notafter       = $availability_selected_array['availability_notafter'];
		$availability_notbefore      = $availability_selected_array['availability_notbefore'];
        $size_selected               = $searchData->get_size_selected();
        $suitetype_selected          = $searchData->get_suitetype_selected();
$buildings_query_string = $queries->getBuildingsQueryString($buildingtype_selected, $province_selected, $region_selected, $sub_region_selected, $building_codes_narrow, $lang_id, $availability_notafter, $availability_notbefore, $size_selected);
$buildings_query       = mysql_query($buildings_query_string);
if (!$buildings_query) { die($buildings_query_string."<br />".mysql_error()); } 



$search_results_count = mysql_num_rows($buildings_query);
$pageCurrent          = '';


if ($theme['wrapper_width'] != '') {
	$wrapper_width = $theme['wrapper_width'];
} else {
	$wrapper_width = '960';
}
?>





<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" style="height: 101%;">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <meta name="viewport" content="width=<?php echo $wrapper_width; ?>" />
      <title><?php echo $language['page_title']; ?></title>
      <link rel="shortcut icon" href="../theme/favicon.ico" type="image/x-icon" />
      <link rel="stylesheet" type="text/css" href="../css/reset.css" />
      <link rel="stylesheet" type="text/css" href="../css/styles.css" />
      <?php include('../css/colour_styles.php'); ?>

      <script src="../lightbox/js/jquery-1.10.2.min.js"></script>
      <script type="text/javascript" src="../js/jquery-ui-1.8.13.custom.min.js"></script>
      <script type="text/javascript" src="../js/jquery.pagination.js"></script> 
      <script type="text/javascript" src="../js/jquery.tools.min.js"></script>
      <script type="text/javascript" src="../js/jquery.jdialogue.js"></script>


<script type="text/javascript"> 





	$(document).ready(function(){      

		
		$('#loader').hide();

		$('#province').change(function(){
			// type
			$('#region').fadeOut();
			$('#sub-region').fadeOut();
			$('#loader').show();

			$.post("../region.php", {
				province: $('#province').val()
				<?php
				if (isset($_REQUEST['lang'])){
					echo ", lang: '" . $_REQUEST['lang'] . "'"; 
				}
				?>
			}, function(response){
				setTimeout("finishAjax('region', '"+escape(response)+"')", 400);
			});
			
			
			$.post("../sub-region.php", {
				province: $('#province').val(),
				region: ''
				<?php
				if (isset($_REQUEST['lang'])){
					echo ", lang: '" . $_REQUEST['lang'] . "'"; 
				}
				?>
			}, function(response){
				setTimeout("finishAjax('sub-region', '"+escape(response)+"')", 400);
			});
			
			
			return false;
		});


			
		$('#region').change(function(){
		// make
		$('#sub-region').fadeOut();
		$('#loader').show();

		var slashed = $('#region').val();

		$.post("../sub-region.php", {
			region: slashed
			<?php
			if (isset($_REQUEST['lang'])){
				echo ", lang: '" . $_REQUEST['lang'] . "'"; 
			}
			?>
		}, function(response){
			setTimeout("finishAjax('sub-region', '"+escape(response)+"')", 400);
		});
		return false;
	});




	//END DOCUMENT READY	
	});
	
		function finishAjax(id, response){
		$('#loader').hide();
		$('#'+id).html(unescape(response));
		$('#'+id).fadeIn();
	}

        function validateEmail()
	{
	
	   var emailID = document.getElementById('email').value;
	   atpos = emailID.indexOf("@");
	   dotpos = emailID.lastIndexOf(".");
	   if (atpos < 1 || ( dotpos - atpos < 2 )) 
	   {
		   
		   return false;
	   }
	   return( true );
	}
        
        
        
        
function BUsubmit() {
    
    
        if( document.buildingsearch.subscription_language.value == "" ) {
            alert( "<?php echo $language['form_verification_message_language']; ?>" );
            document.buildingsearch.subscription_language.focus() ;
//            validated--;
            return false; 
        }

        if( document.buildingsearch.subscription_frequency.value == "" ) {
            alert( "<?php echo $language['form_verification_message_frequency']; ?>" );
            document.buildingsearch.subscription_frequency.focus() ;
//            validated--;
            return false; 
        }
        
    
    
    
    if( document.buildingsearch.bu_name.value == "" )
    {
        <?php if ($lang_id=='1') {?>
        alert( "Your name is a mandatory field. Please provide your first and last names." );
        <?php } else { ?>
        alert("Votre nom est un champ obligatoire. S'il vous plaît fournir vos nom et prénom.");    
        <?php } ?>
        document.buildingsearch.bu_name.focus() ;
        return false;
        
    }

    if( document.buildingsearch.email.value == "" )
    {
        <?php if ($lang_id=='1') {?>       
        alert( "Email is a mandatory field. Please provide your email address." );
        <?php } else { ?> 
        alert("Le courrier électronique est un champ obligatoire. Se il vous plaît indiquer votre adresse e-mail.");      
        <?php } ?> 
        document.buildingsearch.email.focus() ;
        return false;
               
    }


        
    if( !validateEmail())
    {
         <?php if ($lang_id=='1') {?> 
        alert( "Please use a valid Email address." );
        <?php } else { ?>
        alert("Vous n'avez pas d' abonnements.");  
        <?php } ?>     
        document.buildingsearch.email.focus() ;
        return false;
               
    }


    document.buildingsearch.submit();
}
</script>

<style>
body {
	padding: 20px;	
}

#search-form {
	top: auto;
	float:none;
}

.dropdown-label {
	text-align: left;
	padding-left: 0;	
}

/*input#email {
	margin-left: 10px;	
}

input#name {
	margin-left: 10px;
}*/

input.bu {
	border: 1px solid #4b74b2;
	width: 125px;
}

/*a.modal-button {
	background: #608e3a;
	border: 1px solid #608e3a;
	padding: 5px;
	color: #fff !important;
	text-transform: uppercase;
	text-align: center;
	text-decoration: none;
	width: 50px;
	font-weight: bold;
	margin-right: 5px;
	display: inline-block;
	zoom:1;
	*display:inline;	
}*/

</style>

</head>

<body>








<h1 class="modal-title"><?php echo $language['register_page_title_text']; ?></h1>
<p class="modal-copy"><?php echo $language['register_page_paragraph1_text']; ?></p>
<p class="modal-copy"><?php echo $language['register_page_paragraph2_text']; ?></p>
<h2 class="modal-title"><?php echo $language['search_criteria_text']; ?></h2>
<div id="search-form">  





    <form action="EmailConfirm/ConfirmationMain.php?lang=<?php echo $lang; ?>" method="get" name="buildingsearch" id="buildingsearch">
                  <table valign="top" cellpadding="0" cellspacing="0" border="0">
                     <tr>
                        <!--FIRST - ROW-->
                        <td class="<?php echo $language['left_dropdown_padding_class']; ?>"><?php echo $language['province_text']; ?></td>
                        <td class="search-field">
                           <select id="province" class="selectbox" name="province" size="1">


       <?php
echo $displayHTML->provinceQueryHTML($language, $queries, $lang_id, $province_selected);
?>
                           </select>
                        </td>
                        <td class="dropdown-label-narrow"><?php
echo $language['region_text'];
?></td>
                        <td class="search-field">
                           <select id="region" name="region" class="selectbox" size="1">
   <?php
echo $displayHTML->buildingRegionHTML($language, $queries, $province_selected, $region_selected, $lang_id);
?>
                           </select>
                        </td>
				</tr>
<tr>
<td colspan="4" height="10">&nbsp;</td>
</tr>
<!-- eo SPACER ROW //-->
<!--SECOND - ROW-->
                <tr>
                        <td class="dropdown-label"><?php
echo $language['sub_region_text'];
?></td>
                        <td class="search-field">
                           <select id="sub-region" name="sub-region" class="selectbox"  size="1">
   <?php
echo $displayHTML->buildingSubRegionHTML($language, $queries, $region_selected, $sub_region_selected, $lang_id);
?>
                           </select>
                        </td>

                     
                     
 
                        <td class="<?php
echo $language['left_dropdown_padding_class'];
?>"><?php
echo $language['space_type_text'];
?></td>
                        <td class="search-field">
                           <select class="selectbox" name="SuiteType" id="SuiteType" size="1">
           <?php
echo $displayHTML->suitesQueryHTML($language, $queries, $lang_id, $suitetype_selected);
?>
                           </select>
                        </td>
                 </tr>
<tr>
<td colspan="4" height="10">&nbsp;</td>
</tr>
<!-- eo SPACER ROW //-->       
				<tr>
						<td class="dropdown-label-narrow"><?php
echo $language['size_text'];
?></td>
                        <td class="search-field">
                           <select class="selectbox" name="Size" id="Size" size="1">
                           <?php
echo $displayHTML->sizesOptionHTML($language, $queries, $lang_id, $size_selected);
?>
                           </select>
                        </td>
                        <td class="dropdown-label"><?php
echo $language['availability_text'];
?></td>
                        <td class="search-field">
                           <select class="selectbox" name="Availability" id="Availability" size="1">
                 <?php 
echo $displayHTML->availOptionHTML($language, $queries, $lang_id);
?>
                           </select>
                        </td>
                        
                        
                </tr>



                    <tr>
                    <td colspan="4" height="10">&nbsp;</td>
                    </tr>



                    <tr>
                        <td class="dropdown-label-narrow" align="right"><?php echo $language['language_change_placeholder_text']; ?></td>
                        <td class="search-field">
                            <select name="subscription_language" id="subscription_language" class="selectbox form-control subscription_language">
                                <option value=""></option>
                                <option <?php if($lang == 'en_CA'){echo("selected");}?> value="en_CA"><?php echo $language['english_text']; ?> </option>
                                <option <?php if($lang == 'fr_CA'){echo("selected");}?> value="fr_CA"><?php echo $language['french_text']; ?></option>
                            </select>
                        </td>
                        <td class="dropdown-label-narrow" align="right"><?php echo $language['email_frequency_placeholder_text']; ?></td>
                        <td class="search-field">
                            <select name="subscription_frequency" id="subscription_frequency" class="selectbox form-control subscription_frequency">
                                <option value=""></option>
                                <option value="Daily"><?php echo $language['email_frequency_daily_text']; ?></option>
                                <option value="Weekly"><?php echo $language['email_frequency_weekly_text']; ?></option>
                                <option value="Monthly"><?php echo $language['email_frequency_monthly_text']; ?></option>
                            </select>  
                        </td>
                    </tr>


                    <tr>
                    <td colspan="4" height="10">&nbsp;</td>
                    </tr>
                    <!-- eo SPACER ROW //-->  
                    
                    <tr>                
                        <td class="dropdown-label-narrow" align="right">
                        <?php echo $language['name_text']; ?>
                        </td>
                        <td class="search-field">
                        <input class="bu" name="bu_name" type="text" id="bu_name" value="<?php echo $bufullname; ?>" />
                        <input name="lang" type="hidden" value="<?php echo $lang; ?>" id="lang" />
                        </td>
                        <td class="dropdown-label-narrow" align="right">
                        <?php echo $language['email_text']; ?>
                        </td>
                        <td class="search-field">
                        <input class="bu" name="email" type="text" id="email" value="<?php echo $buemail; ?>" />
                        </td>
                    </tr>


 
 <?php
 

 ?>
                     <!-- SPACER ROW //-->
                     <tr>
                        <td colspan="4" height="10">&nbsp;</td>
                     </tr>
                     <!-- eo SPACER ROW //-->
                   
						<tr> 

                        
                        <td class="search-label" align="left">
						</td>
                        
							
                        <td></td>
                        <td align="right" valign="top"><a href="javascript:void(0);" class="modal-button" onclick="BUsubmit()">Ok</a></td>
                        <td valign="top"><a href="javascript:parent.jQuery.fancybox.close();" class="modal-button" >Close</a></td>
                        </tr>


<!-- SPACER ROW //-->
<tr>
	<td colspan="4" height="10">&nbsp;</td>
</tr>

<!-- eo SPACER ROW //-->

<tr>
	<td colspan="4" height="10"><?php echo $language['register_page_footer_message']; ?></td>
</tr>
<tr>
	<td colspan="4" height="10">&nbsp;</td>
</tr>
<tr>
	<td colspan="4" height="10"><?php echo $language['register_page_footer_casl_disclaimer_1']; ?></td>
</tr>
<tr>
	<td colspan="4" height="10">&nbsp;</td>
</tr>
<tr>
	<td colspan="4" height="10"><?php echo $language['register_page_footer_casl_disclaimer_2']; ?></td>
</tr>
<!-- eo SPACER ROW //-->
				</table>
</form>

       	</div>
</body>
</html>