<?php
//// for forcing a search by type as the page loads the first time, send a ?p=<suitetype>
//if (isset($_REQUEST['p']) && $_REQUEST['p'] != '') {
//    $page_manual = $_REQUEST['p'];
//    $newURL = "http://" . $_SERVER['HTTP_HOST'] . "/?province=All&SuiteType=" . strtoupper($page_manual);
////            echo "New URL: " . $newURL . "<br><br>";
    ?>
    <script>//
//        window.location.href = "<?php // echo $newURL ?>";
//    </script>
    <?php
//}

include('theme/db.php');
include('eclipse/classes/Queries.php');
include('eclipse/classes/SearchData.php');
include('eclipse/classes/DisplayHTML.php');
include('objects/LanguageQuery.php');



include('assets/php/krumo/class.krumo.php');






// Initiate the Queries class
$queries = new Queries();
//Initiate the ErrorMessages class
//	$errors                = new ErrorMessages();
//Initiate the Search Data class
$searchData = new SearchData();
//Initiate the FormatData class
//	$formatData            = new FormatData();
//Initiate DisplayHTML
$displayHTML = new DisplayHTML();
// Language Query
$languageQuery = new LanguageQuery();
$langArray = $languageQuery->getLanguageAndID();
$lang = $langArray[0];
$thelang = $langArray[0];
$lang_id = $langArray[1];



//$language_query_string = 'select * from languages_dynamic ld, languages l, languages2 where ld.lang_id = ' . $lang_id . ' and l.lang_id = ' . $lang_id;
//	$language_query_string = 'select * from languages_dynamic ld, languages l, languages2 l2 where ld.lang_id = ' . $lang_id . ' and l.lang_id = ' . $lang_id . ' and l2.lang_id = ' . $lang_id;
////echo "Language Query String: " . $language_query_string . "<br><br>";
//	$language_query = mysql_query($language_query_string) or die("language query error: " . mysql_error());
//	$language = mysql_fetch_array($language_query);


$language = array();
$language_query_string = "select * from languages_dynamic ld, languages l, languages2 l2 where ld.lang_id =? and l.lang_id =? and l2.lang_id =?";
$language_query_statement = $db->prepare($language_query_string);
$language_query_statement->bind_param('iii', mysqli_real_escape_string($db, $lang_id), mysqli_real_escape_string($db, $lang_id), mysqli_real_escape_string($db, $lang_id));
if ($language_query_statement->execute()) {
    $result = $language_query_statement->get_result();
    while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
        $language = $row;
    }
}
$language_query_statement->close();


//setting locale for time, date, money, etc
setlocale(LC_ALL, $language['locale_string']);
date_default_timezone_set('America/New_York');

// Theme Query
$theme_query = mysql_query("select * from theme");
$theme = mysql_fetch_array($theme_query);


$advanced_search = 0;

if ($theme['advanced_search'] == 1) {
    $advanced_search = 1;
} else {
    $advanced_search = 0;
}

$ar4DefaultLayout = $theme['ar4_default_layout'];

//
switch ($ar4DefaultLayout) {
    case 'g': // grid only
        $bodyClass = 'fs_view-grid';
        break;
    case 'mg': // map and grid view
        $bodyClass = 'fs_view-map fs_view-grid';
        break;
    case 'gf': // grid and filter view
        $bodyClass = 'fs_view-grid fs_view-filter';
        break;
    case 'mgf': // map and grid and filter view
        $bodyClass = 'fs_view-map fs_view-grid fs_view-filter';
        break;
    case 'l': // list only
        $bodyClass = '';
        break;
    case 'lf': // list and filter fiew
        $bodyClass = 'fs_view-filter';
        break;
    case 'mlf': // map and list and filter view
        $bodyClass = 'fs_view-map fs_view-filter';
        break;
    default: // map and list view
        $bodyClass = 'fs_view-map ';
        break;
}

//
//
//getting the page search form results
$current_page_result = 'All';
$province_selected = 'All';

$buildingtype_selected = 'All';
$suitetype_selected = 'All';
$region_selected = 'All';
$city_selected = 'All';
$sub_region_selected = 'All';
$size_selected = 'All';
$availability_selected_array = '';
$availability_notafter = '2147483639';
$availability_notbefore = '0';
$building_name_sterm_array = 'All';
$building_name_sterm = 'All';
$building_name_sterm_prepop = 'All';
$building_codes_narrow = '';





//ADVANCED INDUSTRIAL SUITE SEARCH ELEMENTS
$industrial_office_space_selected = 'All';
$industrial_clear_height_selected = 'All';

$industrial_shipping_doors_selected = 'All';
$industrial_drivein_doors_selected = 'All';
$industrial_electrical_volts_selected = 'All';
$industrial_electrical_amps_selected = 'All';

$industrial_parking_stalls_selected = 'All';
$industrial_trucktrailer_parking_selected = 'All';

$industrialsearchelement = '';
$sizeFrom = '';
$sizeTo = '';


if (isset($_REQUEST['show_no_vacancy'])) {
    if ($_REQUEST['show_no_vacancy'] == 'on') {
        $show_no_vacancy = 1;
    } else {
        $show_no_vacancy = 0;
    }
} else {
    if ($theme['no_vacancy_default'] == 1) {
        $show_no_vacancy = 1;
    } else {
        $show_no_vacancy = 0;
    }
}


$pageCurrent = '';

$showpdf = 1;
if (isset($theme['print_pdf_optional']) && $theme['print_pdf_optional'] == 0) {
    $showpdf = 0;
}


$regional_branding_count = 0;


$search_text = $language['search_text'];

if ($theme['show_searchform'] == 0) {
    if ($language['lang_id'] == '0') {
        $search_text = "Accueil";
    }
    $search_text = "Home";
}


$types_query = array();
$types_query_string = "select * from building_types where building_type =?";
$types_query_statement = $db->prepare($types_query_string);
$types_query_statement->bind_param('s', mysqli_real_escape_string($db, ucfirst($buildingtype_selected)));
if ($types_query_statement->execute()) {
    $result = $types_query_statement->get_result();
    while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
        $types_query = $row;
    }
}
$types_query_statement->close();



function escapeJsonString($value) { # list from www.json.org: (\b backspace, \f formfeed)
    $escapers = array("\\", "/", "\"", "\n", "\r", "\t", "\x08", "\x0c");
    $replacements = array("\\\\", "\\/", "\\\"", "\\n", "\\r", "\\t", "\\f", "\\b");
    $result = str_replace($escapers, $replacements, $value);
    return $result;
}

        $ipaddress = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '';
//        $hostname = ''; // too many pitfalls waiting for DNS which will kill the page load
        $referer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '';
        
        $browser = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '';
//        $screensize = ''; // only available via javascript. Need to ajax-ify this function if we want to capture it.
        $datetime = date("Y/m/d, g:ia", $_SERVER['REQUEST_TIME']) ; // format this YYYY/MM/DD
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="x-ua-compatible" content="IE=edge">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css?family=Fira+Sans+Condensed|Roboto" rel="stylesheet">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="stylesheet" href="lib/owlcarousel/2.2.1/dist/assets/owl.carousel.min.css">
        <link rel="stylesheet" href="lib/owlcarousel/2.2.1/dist/assets/owl.theme.default.min.css">
        <link rel="stylesheet" href="eclipse/easy-autocomplete.min.css">
        <link rel="stylesheet" href="lib/malihu-custom-scrollbar/3.1.5/jquery.mCustomScrollbar.css">
        <link rel="stylesheet" href="style.css">
        <?php include('eclipse/colour_styles.php'); ?>
    </head>
    <body class="fs_page-search <?php echo $bodyClass; ?>">
        <header id="fs_masthead" class="fs_site-header">
        <?php
        	
                echo '<div class="fs_client-header clearfix">';
                if ($lang_id == '1') {
                	include('theme/clientheader.php');
                } else {
                	if (file_exists('theme/clientheader_fr.php')) {
                    	include('theme/clientheader_fr.php');
                    }
					else {
                    	include('theme/clientheader.php');
                    }
              	} 
                echo '</div>';
        	
            ?>
        <div class="fs_search-container">
            <div class="fs_search">
                <div class="row">
                    <div class="col col-toggle-filter align-self-center">
                        <a href="#" class="fs_toggle-filter fs_nav-link" aria-label="Toggle property filter"><i class="material-icons fs_nav-link-icon fs_icon-filter">filter_list</i> <span class="fs_toggle-text d-none d-md-inline-block fs_nav-link-text">Filter</span></a>
                    </div>
                    <div class="col col-search align-self-center">
                        <input type="search" id="property-search" name="property-search" placeholder="Search" aria-label="Search property listings">
                    </div>
                    <div class="col-auto align-self-center">
                        <ul class="nav">
                            <li class="nav-item d-none d-lg-inline-block">
                                <a href="#" class="nav-link fs_nav-link" data-toggle="modal" data-target="#fs_modal-print" aria-label="Print to PDF"><i class="material-icons fs_nav-link-icon fs_icon-print">print</i> <span class="fs_nav-link-text"><?php echo $language['print_pdf_text']; ?></span></a>
                            </li>
                            <li class="nav-item d-none d-lg-inline-block">
                                <a href="#" class="fs_subscribe nav-link fs_nav-link" data-toggle="modal" data-target="#fs_modal-alerts" aria-label="Set alerts"><i class="material-icons fs_nav-link-icon fs_icon-subscribe">notifications</i> <span class="fs_nav-link-text"><?php echo $language['set_alerts_text']; ?></span></a>
                            </li>
                        <?php if ($theme['bilingual_fr'] == 1) { ?>
                            <li class="nav-item d-none d-lg-inline-block">
                                <div class="dropdown">
                                    <a class="dropdown-toggle nav-link fs_nav-link" href="#" role="button" id="dropdownLanguage" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons fs_nav-link-icon fs_icon-language">language</i> <span class="fs_nav-link-text"><?php echo $language['language_text']; ?></span><span class="material-icons fs_nav-link-icon">expand_more</span>
                                    </a>
                                <div class="dropdown-menu" aria-labelledby="dropdownLanguage">
                                    <a class="dropdown-item langclick" href="#" data-language="en_CA"><?php echo $language['english_text']; ?></a>
                                    <a class="dropdown-item langclick" href="#" data-language="fr_CA"><?php echo $language['french_text']; ?></a>
                                </div>
                              </div>
                            </li>
                        <?php } ?>
                            <li class="nav-item d-lg-none"><a href="#" class="nav-link fs_nav-link fs_toolbar-link"><i class="material-icons fs_nav-link-icon fs_icon-tools">build</i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            
            <div class="fs_results-utilities d-lg-none">
                <div class="row no-gutters">
                    <div class="col">
                    <div class="fs_counts"><span class="fs_count-suites"><span class="suitescounted"></span> <?php echo $language['suites_plural_lowercase']; ?> / </span><span class="fs_count-favourites"><i class="material-icons fs_count-favourites-icon">favorite</i> <span class="fs_count-favourites-text">0 <?php echo $language['favourites_text']; ?></span></span></div>
                    </div>
                    <div class="col-auto">
                        <div class="fs_ui-controls">
                            <ul class="nav">
                                <li class="nav-item">
                                    <a href="#" class="nav-link fs_toggle-map fs_nav-link"><i class="material-icons fs_nav-link-icon fs_icon-map">map</i> <span class="fs_nav-link-text d-none d-md-inline"><?php echo $language['map_text_show']; ?></span> <span class="fs_nav-link-text"><?php echo $language['map_text_map']; ?></span></a>
                                </li>
                                <li class="nav-item">
                                    <a href="#" class="nav-link fs_toggle-view fs_nav-link"><i class="material-icons fs_nav-link-icon fs_icon-view">view_list</i> <span class="d-none d-md-inline fs_nav-link-text"><?php echo $language['view_as_text']; ?></span> <span class="fs_nav-link-text"><?php echo $language['view_as_text_grid']; ?></span></a>
                                </li>
                            </ul>
                        </div>
                        <div class="fs_favourites-carousel-controls">
                            <a href="#" class="owl-next fs_nav-link fs_carousel-prev"><i class="material-icons fs_nav-link-icon fs_icon-carousel">chevron_left</i></a>
                            <a href="#" class="owl-prev fs_nav-link fs_carousel-next"><i class="material-icons fs_nav-link-icon fs_icon-carousel">chevron_right</i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>  
</header>
        <div class="row no-gutters" id="fs_row-main">
            <div class="col" id="fs_filter">
                <div class="fs_filter-container">
                    <ul class="fs_filter-list">
                        <li>
                            <a class="fs_filter-list-toggle" data-toggle="collapse" href="#collapseFilter1" aria-expanded="false" aria-controls="collapseFilter1">
                                <span class="fs_toggle-text"><?php echo $language['space_type_lowercase_text']; ?></span> <i class="material-icons fs_nav-link-icon float-right">expand_less</i>
                            </a>
                            <div class="collapse show" id="collapseFilter1">

                                <div class="form-group">
                                    <label for="industrial" class="custom-control custom-checkbox">
                                        <input name="suite_type" id="industrial" class="custom-control-input" type="checkbox" value="industrial">
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-description"><?php echo $language['industrial_lowercase_text']; ?></span>
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label for="office" class="custom-control custom-checkbox">
                                        <input name="suite_type" id="office" class="custom-control-input" type="checkbox" value="office">
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-description"><?php echo $language['office_lowercase_text']; ?></span>
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label for="retail" class="custom-control custom-checkbox">
                                        <input name="suite_type" id="retail" class="custom-control-input" type="checkbox" value="retail">
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-description"><?php echo $language['retail_lowercase_text']; ?></span>
                                    </label>
                                </div>

                            </div>
                        </li>
                        <li>
                            <a class="fs_filter-list-toggle" data-toggle="collapse" href="#collapseFilter2" aria-expanded="false" aria-controls="collapseFilter2">
                                <span class="fs_toggle-text"><?php echo $language['suite_size_text']; ?></span> <i class="material-icons fs_nav-link-icon float-right">expand_less</i>

                            </a>
                            <div class="collapse show" id="collapseFilter2">
                                
                                        <select class="custom-select selectbox form-control" name="Size" id="Size" size="1">
                                            <?php echo $displayHTML->sizesOptionHTML($language, $queries, $lang_id, $size_selected); ?>
                                        </select>



                            </div>
                        </li>
<!--                        <li>
                            <a class="filter-list-toggle collapsed" data-toggle="collapse" href="#collapseFilter3" aria-expanded="false" aria-controls="collapseFilter3">
                                <span class="fs_toggle-text">Price Range</span> <i class="material-icons fs_toggle-icon float-right">expand_more</i>

                            </a>
                            <div class="collapse" id="collapseFilter3">
                                <div class="row">
                                    <div class="col">
                                        <select class="form-control">
                                            <option>Min</option>
                                        </select>
                                    </div>
                                    <div class="col">
                                        <select class="form-control">
                                            <option>Max</option>
                                        </select>
                                    </div>
                                </div>

                            </div>
                        </li>-->
                        <li>
                            <a class="fs_filter-list-toggle" data-toggle="collapse" href="#collapseFilter4" aria-expanded="false" aria-controls="collapseFilter4">
                                <span class="fs_toggle-text"><?php echo $language['availability_lowercase_text']; ?></span> <i class="material-icons fs_nav-link-icon float-right">expand_less</i>

                            </a>
                            <div class="collapse show" id="collapseFilter4">

                                
                                <select class="custom-select selectbox form-control" name="Availability" id="Availability" size="1">
                                    <?php echo $displayHTML->availOptionHTML($language, $queries, $lang_id); ?>
                                </select>
                                <label for="show_no_vacancy" class="custom-control custom-checkbox">
                                    <input name="show_no_vacancy" id="show_no_vacancy" class="custom-control-input custom-checkbox" type="checkbox" value="show_no_vacancy">    
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description"><?php echo $language['fully_leased_property_text']; ?></span>
                                </label>
                                </div>
                        </li>
                        <li class="d-none" id="industrialFilters">
                            <a class="fs_filter-list-toggle" data-toggle="collapse" href="#collapseFilter5" aria-expanded="false" aria-controls="collapseFilter5">
                                <span class="fs_toggle-text">More Options</span> <i class="material-icons fs_nav-link-icon float-right">expand_less</i>

                            </a>
                            <div class="collapse show" id="collapseFilter5">

                                <select class="custom-select selectbox form-control d-none" name="SuiteOfficeSize" id="SuiteOfficeSize" size="1">
                                        <?php echo $displayHTML->industrialAdvancedOfficeSizeSearch($language, $queries, $lang_id, $industrial_office_space_selected); ?>
                                </select>

                                <select class="custom-select selectbox form-control" name="SuiteClearHeight" id="SuiteClearHeight" size="1">
                                        <?php echo $displayHTML->industrialAdvancedClearHeightSearch($language, $queries, $lang_id, $industrial_clear_height_selected); ?>
                                </select>

                                <select class="custom-select selectbox form-control" name="SuiteShippingDoors" id="SuiteShippingDoors" size="1">
                                        <?php echo $displayHTML->industrialAdvancedShippingDoorsSearch($language, $queries, $lang_id, $industrial_shipping_doors_selected); ?>
                                </select>

                                <select class="custom-select selectbox form-control" name="SuiteDriveInDoors" id="SuiteDriveInDoors" size="1">
                                        <?php echo $displayHTML->industrialAdvancedDriveInDoorsSearch($language, $queries, $lang_id, $industrial_drivein_doors_selected); ?>
                                </select>

                                <select class="custom-select selectbox form-control" name="SuiteAvailElecAmps" id="SuiteAvailElecAmps" size="1">
                                        <?php echo $displayHTML->industrialAdvancedElectricalAmpsSearch($language, $queries, $lang_id, $industrial_electrical_amps_selected); ?>
                                </select>

                                <select class="custom-select selectbox form-control" name="SuiteAvailElecVolts" id="SuiteAvailElecVolts" size="1">
                                        <?php echo $displayHTML->industrialAdvancedElectricalVoltsSearch($language, $queries, $lang_id, $industrial_electrical_volts_selected); ?>
                                </select>

                                <select class="custom-select selectbox form-control" name="SuiteParkingSurfaceStalls" id="SuiteParkingSurfaceStalls" size="1">
                                        <?php echo $displayHTML->industrialAdvancedSurfaceStallsSearch($language, $queries, $lang_id, $industrial_parking_stalls_selected); ?>
                                </select>

                                <select class="custom-select selectbox form-control" name="SuiteTruckTrailerParking" id="SuiteTruckTrailerParking" size="1">
                                        <?php echo $displayHTML->industrialAdvancedTruckTrailerParkingSearch($language, $queries, $lang_id, $industrial_trucktrailer_parking_selected); ?>
                                </select>

                                </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col" id="fs_content">
                <main class="fs_site-main fs_search-main">
                    <div class="row no-gutters d-none d-lg-flex">
                        <div class="col">
                            <div class="fs_search-utilities row no-gutters">
                                <div class="col filters align-self-center">
                                    <div class="fs_thechips">
<!--                                        <div class="fs_chip"><span class="fs_chip-text">Office</span> <button type="button" class="fs_chip-remove" aria-label="Close"><i class="material-icons fs_icon-chip">clear</i></button></div>
                                        <div class="fs_chip"><span class="fs_chip-text">Calgary</span> <button type="button" class="fs_chip-remove" aria-label="Close"><i class="material-icons fs_icon-chip">clear</i></button></div>
                                        <div class="fs_chip"><span class="fs_chip-text">5,000 and Under</span> <button type="button" class="fs_chip-remove" aria-label="Close"><i class="material-icons fs_icon-chip">clear</i></button></div>-->
                                     </div>
                                    <a href="#" class="fs_clear-filters" aria-label="Clear search filters"><?php echo $language['reset_text']; ?></a>
                                </div>
<!--                            <div class="col-auto align-self-center">
                                    <a href="#" class="fs_share-search" aria-label="Share search results"><i class="material-icons fs_nav-link-icon fs_icon-share">share</i> <span class="fs_nav-link-text"><?php echo $language['share_text_share']; ?> <?php echo $language['share_text_search']; ?></span></a>   
                                </div> -->
                                </div>

                        </div>
                    </div>
                    <div id="fs_row-main-inner" class="row no-gutters">
                        <div id="fs_map">
                            <div id="themap"></div>
                            <a href="#" class="btn btn-default fs_toggle-results"><i class="material-icons fs_nav-link-icon">chevron_right</i></a>
                        </div>
                        <div id="fs_results">
                            <div class="fs_results-controls-mobile">
                                <div class="row no-gutters">
                                    <div class="col">
                                        <a class="fs_nav-link fs_toggle-results-mobile" href="#"><span class="fs_nav-link-text"><?php echo $language['hide_results_text']; ?></span> <i class="material-icons fs_nav-link-icon fs_icon-expand">expand_less</i></a>
                                    </div>
                                    <div class="col col-auto align-self-center">
                                        <a class="fs_nav-link fs_reset-search" href="#"><i class="material-icons fs_nav-link-icon fs_icon-reset-search">youtube_searched_for</i><span class="fs_nav-link-text">Reset Search</span></a>
                                    </div>
                                </div>
                            </div>    
                            <div class="row d-none d-lg-flex no-gutters fs_row-results-utilities">
                                <div class="col">
                                    <div class="fs_results-utilities d-none d-lg-block">
                                        <div class="row no-gutters">
                                            <div class="col col-auto align-self-center">
                                                <div class="fs_counts"><span class="fs_count-suites"><span class="suitescounted"></span> <?php echo $language['suites_plural_lowercase']; ?> / </span><span class="fs_count-favourites"><i class="material-icons fs_count-favourites-icon">favorite</i> <span class="fs_count-favourites-text">0 <?php echo $language['favourites_text']; ?></span></span></div>
                                            </div>
                                            <div class="col">
                                                <div class="fs_ui-controls">
                                                    <ul class="nav">
                                                        <li class="nav-item">
                                                            <a href="#" class="nav-link fs_nav-link fs_toggle-map"><i class="material-icons fs_nav-link-icon fs_icon-map">map</i> <span class="fs_nav-link-text d-none d-md-inline"><?php echo $language['map_text_hide']; ?></span> <span class="fs_nav-link-text"><?php echo $language['map_text_map']; ?></span></a>
                                                        </li>
                                                        <li class="nav-item">
                                                            <a href="#" class="nav-link fs_nav-link fs_toggle-view"><i class="material-icons fs_nav-link-icon fs_icon-view">view_module</i> <span class="d-none d-md-inline fs_nav-link-text"><?php echo $language['view_as_text']; ?></span> <span class="fs_nav-link-text"><?php echo $language['view_as_text_grid']; ?></span></a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
// get and process buildings query string
                            $search_results_count = 0;
                            if (count($types_query) > 0 || $buildingtype_selected == 'All') {
                                if ($advanced_search == 1) {
                                    $buildingDataQueryStringArray = $queries->getBuildingsQueryString($size_selected, $sizeFrom, $sizeTo, $suitetype_selected, $buildingtype_selected, $province_selected, $region_selected, $city_selected, $sub_region_selected, $building_codes_narrow, $lang_id, $availability_notafter, $availability_notbefore, $building_name_sterm, $show_no_vacancy, $industrial_office_space_selected, $industrial_clear_height_selected, $industrial_shipping_doors_selected, $industrial_drivein_doors_selected, $industrial_electrical_volts_selected, $industrial_electrical_amps_selected, $industrial_parking_stalls_selected, $industrial_trucktrailer_parking_selected, $industrialsearchelement, $advanced_search, $db);
                                } else {
                                    $buildingDataQueryStringArray = $queries->getBuildingsQueryString($size_selected, $sizeFrom, $sizeTo, $suitetype_selected, $buildingtype_selected, $province_selected, $region_selected, $city_selected, $sub_region_selected, $building_codes_narrow, $lang_id, $availability_notafter, $availability_notbefore, $size_selected, $building_name_sterm, $show_no_vacancy, $db);
                                }
                            }
                            $buildings_query_string = $buildingDataQueryStringArray[0];

                            
//    $buildings_query_string .=" LIMIT ".$postnumbers." OFFSET ".$offset;
                            $pageCurrent = '';
//    $buildings_query_string = str_replace("and allb.building_name like '%none%'", "", $buildings_query_string);
                            if ($buildings_query_statement = $db->prepare($buildings_query_string)) {
                                // carry on
                            } else {
                                trigger_error('buildings query SQL: ' . $buildings_query_string . ' Error: ' . $db->error, E_USER_ERROR);
                            }
                            
//            krumo($buildingDataQueryStringArray);
// prep dynamic variable query
                            $param_type = '';
                            $n = count($buildingDataQueryStringArray[1]);
                            for ($i = 0; $i < $n; $i++) {
                                $param_type .= $buildingDataQueryStringArray[1][$i];
                            }

// Add in 2 more string fields for $postnumbers and $offset for infinite scrolling / loading
                            $param_type .="ss";

                            /* with call_user_func_array, array params must be passed by reference */
                            $a_params[] = & $param_type;

                            for ($i = 0; $i < $n; $i++) {
                                /* with call_user_func_array, array params must be passed by reference */
                                $a_params[] = & $buildingDataQueryStringArray[2][$i];
                            }
// Add in $postnumbers and $offset for infinite scrolling / loading
                            $postnumbers = 999;
                            $offset = 0;
                            $a_params[] = & $postnumbers;
                            $a_params[] = & $offset;



// call_user_func_array is how we can pass dynamic quantities of variables to mysqli prepared statements.
                            call_user_func_array(array($buildings_query_statement, 'bind_param'), $a_params);
                            $buildings_query = array();
                            $theBigArray = array();
                            $theSuiteArray = array();

                            if ($buildings_query_statement->execute()) {
                                $result = $buildings_query_statement->get_result();
                                while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
                                    $buildings_query[] = $row;
//                                    $thisBuilding = array();
//                                    array_push($thisBuilding, $row['latitude']);
//                                    array_push($thisBuilding, $row['longitude']);
//                                    array_push($thisBuilding, $row['building_name']);
//                                    array_push($thisBuilding, $row['street_address']);
//                                    array_push($thisBuilding, $row['city']);
//                                    array_push($thisBuilding, $row['province']);
//                                    array_push($thisBuilding, $row['pc']);
//                                    array_push($thisBuilding, $row['building_code']);
//                                    $theBigArray[$row['building_code']] = $thisBuilding;

                                    $theBigArray[$row['building_code']] = $row;
                                }
                            } else {
                                trigger_error('Could not execute building query statement: Error: ' . $db->error, E_ALL);
                            }
                            $buildings_query_statement->close();


                            ?>



                            <div class="row no-gutters fs_search-results">
                                <?php
                                $b = 0;
                                foreach ($buildings_query as $building) {
                                    $building_code = $building['building_code'];
                                    $building_thumbnail_query_string = $queries->get_building_thumbnail_query_string($lang_id, $building_code);
                                    // process buildings query string
                                    $building_thumbnail__string = $building_thumbnail_query_string[0];
//    $building_thumbnail__string .=" LIMIT ".$postnumbers." OFFSET ".$offset;
                                    $pageCurrent = '';
//    $building_thumbnail__string = str_replace("and allb.building_name like '%none%'", "", $building_thumbnail__string);
                                    if ($building_thumbnail__statement = $db->prepare($building_thumbnail__string)) {
                                        // carry on
                                    } else {
                                        trigger_error('Wrong SQL: ' . $building_thumbnail__string . ' Error: ' . $db->error, E_USER_ERROR);
                                    }
                                    // prep dynamic variable query
                                    $param_type = '';
                                    $n = count($building_thumbnail_query_string[1]);
                                    for ($i = 0; $i < $n; $i++) {
                                        $param_type .= $building_thumbnail_query_string[1][$i];
                                    }

                                    /* with call_user_func_array, array params must be passed by reference */
                                    $a_params = array();
                                    $a_params[] = & $param_type;

                                    for ($i = 0; $i < $n; $i++) {
                                        /* with call_user_func_array, array params must be passed by reference */
                                        $a_params[] = & $building_thumbnail_query_string[2][$i];
                                    }



                                    // call_user_func_array is how we can pass dynamic quantities of variables to mysqli prepared statements.
                                    call_user_func_array(array($building_thumbnail__statement, 'bind_param'), $a_params);
                                    $building_thumbnail = array();
                                    if ($building_thumbnail__statement->execute()) {
                                        $result = $building_thumbnail__statement->get_result();
                                        while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
                                            $building_thumbnail = $row;
                                        }
                                    } else {
                                        trigger_error('Could not execute building_thumbnail query statement: Error: ' . $db->error, E_ALL);
                                    }
                                    $building_thumbnail__statement->close();



                                    $building_thumbnail_image_name = '';
                                    $building_thumbnail_image_name_list = '';
                                    $building_thumbnail_image_name_grid = '';
                                    if (isset($building_thumbnail['file_name']) && $building_thumbnail['file_name'] != '') {
                                        $building_thumbnail_image_name = "images/preview_images/" . $building_thumbnail['file_name'];
                                        $building_thumbnail_image_name_grid = str_replace('.jpg', '_gridthumb.jpg', $building_thumbnail_image_name);
                                        $building_thumbnail_image_name_list = str_replace('.jpg', '_listthumb.jpg', $building_thumbnail_image_name);                                        
                                    } else {
                                        $building_thumbnail_image_name = "images/no_thumb_200.png";
                                        $building_thumbnail_image_name_grid = "images/no_thumb_200.png";
                                        $building_thumbnail_image_name_list = "images/no_thumb_100.png";
                                    }

$add_rent_operating = $building['add_rent_operating'];
$add_rent_realty = $building['add_rent_realty'];
$add_rent_power = $building['add_rent_power'];
$add_rent_total = $building['add_rent_total'];

if (($add_rent_operating == 0) || ($add_rent_operating == '')) {
    $building_add_rent_operating = 'n/a';
} else {
    $building_add_rent_operating = (money_format('%.2n', (double) $add_rent_operating));
}

if (($add_rent_realty == 0) || ($add_rent_realty == '')) {
    $building_add_rent_realty = 'n/a';
} else {
    $building_add_rent_realty = money_format('%.2n', (double) $add_rent_realty);
}

if (($add_rent_power == 0) || ($add_rent_power == '')) {
    $building_add_rent_power = 'n/a';
} else {
    $building_add_rent_power = money_format('%.2n', (double) $add_rent_power);
}

if (($add_rent_total == 0) || ($add_rent_total == '')) {
    $building_add_rent_total = 'n/a';
} else {
    $building_add_rent_total = money_format('%.2n', (double) $add_rent_total);
}

  











//                                    if ($advanced_search == 1) {
                                    $suites_query_string_query = $queries->get_suites_query_string_ar4_index($lang_id, $building_code, $db);

////        $suitecount_query_string_query = $queries->get_suites_count_noleased($lang_id, $building_code, $size_selected, $sizeFrom, $sizeTo, $suitetype_selected, "notag", $availability_notbefore, $availability_notafter, $industrial_office_space_selected, $industrial_clear_height_selected, $industrial_shipping_doors_selected, $industrial_drivein_doors_selected, $industrial_electrical_volts_selected, $industrial_electrical_amps_selected, $industrial_parking_stalls_selected, $industrial_trucktrailer_parking_selected, $industrialsearchelement, $advanced_search, $db);
//                                    } else {
//                                        $suites_query_string_query = $queries->get_suites_query_string_ar4_index($lang_id, $building_code, $size_selected, $sizeFrom, $sizeTo, $suitetype_selected, "notag", $availability_notbefore, $availability_notafter, $db);
//
////        $suitecount_query_string_query = $queries->get_suites_count_noleased($lang_id, $building_code, $size_selected, $sizeFrom, $sizeTo, $suitetype_selected, "notag", $availability_notbefore, $availability_notafter, $db);
//                                    }


                    
                                    $suite_query_string = $suites_query_string_query[0];
                                    $a_params = array();
                                    if ($suite_query_statement = $db->prepare($suite_query_string)) {
                                        // carry on
                                    } else {
                                        trigger_error('Wrong SQL: ' . $suite_query_string . ' Error: ' . $db->error, E_USER_ERROR);
                                    }
// prep dynamic variable query
                                    $param_type = '';
                                    $n = count($suites_query_string_query[1]);
                                    for ($i = 0; $i < $n; $i++) {
                                        $param_type .= $suites_query_string_query[1][$i];
                                    }
                                    /* with call_user_func_array, array params must be passed by reference */
                                    $a_params[] = & $param_type;
                                    for ($i = 0; $i < $n; $i++) {
                                        /* with call_user_func_array, array params must be passed by reference */
                                        $a_params[] = & $suites_query_string_query[2][$i];
                                    }

// call_user_func_array is how we can pass dynamic quantities of variables to mysqli prepared statements.
                                    call_user_func_array(array($suite_query_statement, 'bind_param'), $a_params);
                                    $suites_query = array();
                                    if ($suite_query_statement->execute()) {
                                        $result = $suite_query_statement->get_result();
                                        $numAvailSuites = 0;
                                        $availSuites = '';
                                        $ttlSqFt = 0;
                                        while ($row = $result->fetch_array(MYSQLI_ASSOC)) {

                                            $suites_query[] = $row;
                                            if ($row['leased'] != 'true') {
                                                $numAvailSuites++;
                                            }
                                            $ttlSqFt = $ttlSqFt + $row['net_rentable_area'];
                                        }
                                    } else {
                                        trigger_error('Could not execute preview images query statement: Error: ' . $db->error, E_ALL);
                                    }
                                    $suite_query_statement->close();

                                    if ($lang == "en_CA") {
                                        $ttlSqFt = number_format($ttlSqFt);
                                    }
                                    if ($lang == "fr_CA") {
                                        $ttlSqFt = number_format($ttlSqFt, 0, ',', ' ');
                                    }

                                    if ($numAvailSuites > 0) {
                                        if ($numAvailSuites > 1) {
                                            $availSuites = $numAvailSuites . $language['units_available_text'];
                                        } else {
                                            $availSuites = $numAvailSuites . $language['unit_available_text'];
                                        }
                                    } else {
                                        $availSuites = $language['no_available_unit_text'];
                                    }
                                    
                                    
                                    
                                    
                                    // add thumbnails and total cost items to the $theBigArray array
                                    foreach ($theBigArray as $key => $value) {
                                        if ($building_code == $value['building_code']) {
                                            $theBigArray[$key]['thumbnail'] = $building_thumbnail_image_name;
                                            $theBigArray[$key]['thumbnailGrid'] = $building_thumbnail_image_name_grid;
                                            $theBigArray[$key]['thumbnailList'] = $building_thumbnail_image_name_list;
                                            
                                            $theBigArray[$key]['add_rent_operating'] = $building_add_rent_operating;
                                            $theBigArray[$key]['add_rent_realty'] = $building_add_rent_realty;
                                            $theBigArray[$key]['add_rent_power'] = $building_add_rent_power;
                                            $theBigArray[$key]['add_rent_total'] = $building_add_rent_total;
//                                            $theBigArray[$key]['available_suites'] = $numAvailSuites;
                                        }
                                    }                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    ?>        


                                    <?php
                                    

                                    $si = 0;
                                    foreach ($suites_query as $suite) {
                                        $suite_availability = $queries->get_suite_availability($language, $suite);
                                        $suite_availability_raw = $suite['availability'];
                                        $suite_net_rentable_area = $queries->get_suite_net_rentable_area($lang, $suite);
                                        $suite_contiguous_area = $queries->get_suite_contiguous_area_noformat($lang, $suite);
                                        $suite_min_divisible_area = $queries->get_suite_min_divisible($lang, $suite);
                                        $suite_net_rent = $queries->get_suite_net_rent($language, $suite);
                                        $suite_description = strip_tags($suite['description'], '<p>,<br>,<br />,<u>,<b>,<i>,<ul>,<li>,<ol>,<a>');
                                        $suite_contiguous_area_formatted = '';
                                        if ($lang == "en_CA") {
                                            $suite_contiguous_area_formatted = number_format($suite_contiguous_area);
                                        }
                                        if ($lang == "fr_CA") {
                                            $suite_contiguous_area_formatted = number_format($suite_contiguous_area, 0, ',', ' ');
                                        }
                                        
                                        
                                        $areaOutput = $suite_net_rentable_area . ' ' . $language['building_information_sq_ft_trans'];

                                        if ((intval($suite_contiguous_area) != 0) && (intval($suite_min_divisible_area) != 0)) {
                                            $areaOutput .= '<br> (' . $suite_min_divisible_area . ' | ' . $suite_contiguous_area_formatted . ')';
                                        } else if ((intval($suite_contiguous_area) == 0) && (intval($suite_min_divisible_area) != 0)) {
                                            $areaOutput .= '<br> (' . $suite_min_divisible_area . ')';
                                        } else if ((intval($suite_contiguous_area) != 0) && (intval($suite_min_divisible_area) == 0)) {
                                            $areaOutput .= '<br> (' . $suite_contiguous_area_formatted . ')';
                                        }
                                        ?>




                                        <?php
                                        // add Suites to the $theBigArray array
//                                        $theSuiteArray = array();
//                                        foreach ($theBigArray as $key => $value) {
//                                            if ($suite['building_code'] == $value['building_code']) {
//                                                $theSuiteArray[$value['building_code']]['Suites'][$si] = $suite;
                $theSuiteArray[$suite['building_code']][$suite['suite_id']]['suite_id'] = $suite['suite_id'];
                $theSuiteArray[$suite['building_code']][$suite['suite_id']]['building_code'] = $building_code;
                $theSuiteArray[$suite['building_code']][$suite['suite_id']]['suite_name'] = $suite['suite_name'];
                $theSuiteArray[$suite['building_code']][$suite['suite_id']]['new'] = $suite['new'];
                $theSuiteArray[$suite['building_code']][$suite['suite_id']]['leased'] = $suite['leased'];
                $theSuiteArray[$suite['building_code']][$suite['suite_id']]['promoted'] = $suite['promoted'];
                $theSuiteArray[$suite['building_code']][$suite['suite_id']]['model'] = $suite['model'];
                $areaCommaRemoved = str_replace(',', '', $suite_net_rentable_area);
                $theSuiteArray[$suite['building_code']][$suite['suite_id']]['rawArea'] = str_replace(' ', '', $areaCommaRemoved);
                
                $contigCommaRemoved = str_replace(",","", $suite_contiguous_area);
                $theSuiteArray[$suite['building_code']][$suite['suite_id']]['rawContigArea'] = str_replace(' ', '', $contigCommaRemoved);
                
                $divisCommaRemoved = str_replace(",","", $suite_min_divisible_area);
                $theSuiteArray[$suite['building_code']][$suite['suite_id']]['rawDivisArea'] = str_replace(' ', '', $divisCommaRemoved);
                
                
                
                $theSuiteArray[$suite['building_code']][$suite['suite_id']]['formattedArea'] = $areaOutput;
                $theSuiteArray[$suite['building_code']][$suite['suite_id']]['rawAvailability'] = $suite_availability_raw;
                $theSuiteArray[$suite['building_code']][$suite['suite_id']]['formattedAvailability'] = $suite_availability;
                $theSuiteArray[$suite['building_code']][$suite['suite_id']]['formattedNetRent'] = $suite_net_rent;
                $theSuiteArray[$suite['building_code']][$suite['suite_id']]['availSuites'] = $availSuites;
                $theSuiteArray[$suite['building_code']][$suite['suite_id']]['suite_type'] = $suite['suite_type'];
                $theSuiteArray[$suite['building_code']][$suite['suite_id']]['description'] = $suite_description;
                // industrial search elements
$theSuiteArray[$suite['building_code']][$suite['suite_id']]['office_space'] = ($suite['office_space'] != '' ? $suite['office_space'] : "n/a");
$theSuiteArray[$suite['building_code']][$suite['suite_id']]['warehouse_space'] = ($suite['warehouse_space'] != '' ? $suite['warehouse_space'] : "n/a");;
$theSuiteArray[$suite['building_code']][$suite['suite_id']]['clear_height'] = ($suite['clear_height'] != '' ? $suite['clear_height'] : "n/a");
$theSuiteArray[$suite['building_code']][$suite['suite_id']]['clear_height_sort'] = ($suite['clear_height_sort'] != '' ? $suite['clear_height_sort'] : "n/a");
$theSuiteArray[$suite['building_code']][$suite['suite_id']]['shipping_doors_drive_in'] = ($suite['shipping_doors_drive_in'] != '' ? $suite['shipping_doors_drive_in'] : "n/a");
$theSuiteArray[$suite['building_code']][$suite['suite_id']]['shipping_doors_truck'] = ($suite['shipping_doors_truck'] != '' ? $suite['shipping_doors_truck'] : "n/a");
$theSuiteArray[$suite['building_code']][$suite['suite_id']]['bay_size_x'] = ($suite['bay_size_x'] != '' ? $suite['bay_size_x'] : "n/a");
$theSuiteArray[$suite['building_code']][$suite['suite_id']]['bay_size_y'] = ($suite['bay_size_y'] != '' ? $suite['bay_size_y'] : "n/a");
$theSuiteArray[$suite['building_code']][$suite['suite_id']]['available_electrical_volts'] = ($suite['available_electrical_volts'] != '' ? $suite['available_electrical_volts'] : "n/a");
$theSuiteArray[$suite['building_code']][$suite['suite_id']]['available_electrical_amps'] = ($suite['available_electrical_amps'] != '' ? $suite['available_electrical_amps'] : "n/a");
$theSuiteArray[$suite['building_code']][$suite['suite_id']]['surface_stalls_per_unit'] = ($suite['surface_stalls_per_unit'] != '' ? $suite['surface_stalls_per_unit'] : "n/a");
$theSuiteArray[$suite['building_code']][$suite['suite_id']]['surface_stalls_per_unit_sort'] = ($suite['surface_stalls_per_unit_sort'] != '' ? $suite['surface_stalls_per_unit_sort'] : "n/a");
$theSuiteArray[$suite['building_code']][$suite['suite_id']]['surface_stalls'] = ($suite['surface_stalls'] != '' ? $suite['surface_stalls'] : "n/a");
$theSuiteArray[$suite['building_code']][$suite['suite_id']]['truck_trailer_parking'] = ($suite['truck_trailer_parking'] != '' ? $suite['truck_trailer_parking'] : "n/a");
                
                
//                                            }
//                                        }
                                        $si++;
                                            }
//                                }
//                            } // maybe not this one
                                    ?>


                                    <?php
                                    $b++;
                                }
                                
                                
                                ?>
                        </div>
                    </div>
                    </div>
                </main>
            </div>
        </div>
        <div id="theBuildings" class="d-none" >
            <?php echo json_encode($theBigArray); ?>
        </div>
        <div id="theSuites" class="d-none" >
            <?php echo json_encode($theSuiteArray); ?>
        </div>
        <div id="moreData" class="d-none" >
            <span id="no_vacancy_val" data-value="<?php echo $show_no_vacancy; ?>"></span>
        </div>
        <div id="testJson"class="d-none" >

        </div>
        <div id="testmapinfo" class="d-none">
            <div class="infoWindow">
                <div class="iwImage"><img src=""></div>
                <div class="iwBuilding"></div>
                <div class="iwAvailable"></div>
            </div>
        </div>
        <div id="fs_favourites" class="">
            <div class="fs_favourites-tab d-none d-lg-inline-block">
                <a href="#" class="fs_favourites-toggle"><i class="material-icons fs_nav-link-icon fs_icon-favourite">favorite</i> <span class="fs_nav-link-text"><?php echo $language['favourites_text']; ?></span> <i class="material-icons fs_nav-link-icon fs_icon-expand">expand_more</i></a>
            </div>
            <div class="fs_favourites-container">
                <div class="row no-gutters">
                    <div class="d-none d-lg-block col-lg-3 col-xl-2">
                        <ul class="fs_favourite-utilities-list">
                            <li><a href="#" class="fs_favourites-print fs_nav-link"><i class="material-icons fs_nav-link-icon fs_icon-print">print</i> <span class="fs_nav-link-text"><?php echo $language['print_text']; ?></span></a></li>
                            <li><a href="#" class="fs_favourites-share fs_nav-link" data-toggle="modal" data-target="#fs_modal-share" ><i class="material-icons fs_nav-link-icon fs_icon-share">share</i> <span class="fs_nav-link-text"><?php echo $language['share_this_text']; ?></span></a></li>
                            <li><a href="#" class="fs_favourites-clear fs_nav-link"><i class="material-icons fs_nav-link-icon fs_icon-clear">clear</i> <span class="fs_nav-link-text"><?php echo $language['reset_text']; ?></span></a></li>
                        </ul>
                        <ul class="fs_favourite-headings-list">
                            <li><span class="sr-only"><?php echo $language['property_text']; ?> <?php echo $language['name_text']; ?></span></li>
                            <li><?php echo $language['suites_plural_lowercase']; ?> #</li>
                            <li><?php echo $language['size_text_lowercase']; ?>(<?php echo $language['building_information_sq_ft_trans']; ?>)</li>
                            <li><?php echo $language['price_text']; ?> (<?php echo $language['price_text']; ?>)</li>
                            <li><?php echo $language['index_table_header_availability_text']; ?></li>
                            <li><?php echo $language['loading_text']; ?></li>
                            <li><?php echo $language['power_text']; ?></li>
                            <li><?php echo $language['ceiling_text']; ?></li>
                        </ul>
                        <ul class="fs_favourite-headings-other-costs">
                            <li><?php echo $language['additional_rent_text']; ?></li>
                            <li><?php echo $language['operating_costs_text']; ?></li>
                            <li><?php echo $language['realty_tax_text']; ?></li>
                        </ul>
                    </div>
                    <div class="col-12 col-lg-9 col-xl-10">
                        <div id="fs_favourites-carousel" class="owl-carousel owl-theme">


                        </div>
                    </div>    
                </div>

            </div>
        </div><!-- end div id="favourites" -->

<div class="modal fade" id="fs_modal-print" tabindex="-1" role="dialog" aria-labelledby="fs_modal-print-label" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="fs_modal-print-label"><?php echo $language['print_pdf_configuration_title_text']; ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <i class="material-icons">clear</i>
        </button>
      </div>
      <div class="modal-body">
          <ul class="nav justify-content-center fs_nav-print-view">
              <li class="nav-item">
                  <a href="#" class="nav-link printSelect" data-pdftype="Thumbnail">
                      <img src="images/dialogue-view-thumbs.jpg" class="img-fluid printChoice" />
                      <div class="fs_nav-link-text"><?php echo $language['thumbnail_view_text']; ?></div>
                  </a>
              </li>
              <li class="nav-item">
                  <a href="#" class="nav-link printSelect" data-pdftype="Condensed">
                      <img src="images/dialogue-view-condensed.jpg" class="img-fluid printChoice" />
                      <div class="fs_nav-link-text"><?php echo $language['condensed_view_text']; ?></div>
                  </a>
              </li>
              <li class="nav-item">
                  <a href="#" class="nav-link printSelect" data-pdftype="Properties">
                      <img src="images/dialogue-view-properties.jpg" class="img-fluid printChoice" />
                      <div class="fs_nav-link-text"><?php echo $language['property_pages_text']; ?></div>
                  </a>
              </li>
          </ul>
          <button type="button" class="btn btn-primary btn-block btn-modal-print"><?php echo $language['print_this_view_text']; ?></button>
      </div>
    </div>
  </div>
</div>
        
        
<div class="modal fade" id="fs_modal-alerts" tabindex="-1" role="dialog" aria-labelledby="fs_modal-alerts-label" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="fs_modal-alerts-label"><?php echo $language['set_alerts_text']; ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <i class="material-icons">clear</i>
        </button>
      </div>
      <div class="modal-body">
          
          <button type="button" class="btn btn-primary btn-block"><?php echo $language['set_alerts_text']; ?></button>
      </div>
    </div>
  </div>
</div>
        
<div class="modal fade" id="fs_modal-share-search" tabindex="-1" role="dialog" aria-labelledby="fs_modal-share-label" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="fs_modal-share-label"><?php echo $language['share_text_share']; ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <i class="material-icons">clear</i>
        </button>
      </div>
      <div class="modal-body">
          
          <button type="button" class="btn btn-primary btn-block"><?php echo $language['share_text_share']; ?></button>
      </div>
    </div>
  </div>
</div>






<div class="modal fade" id="fs_modal-share" tabindex="-1" role="dialog" aria-labelledby="fs_modal-share-label" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="fs_modal-share-label"><?php echo $language['favourites_share_modal_header']; ?></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <i class="material-icons">clear</i>
          </button>
        </div>
        <div class="modal-body">
            <div class="alert alert-success d-none" role="alert">Your favourites have been shared. Thank you!</div>
            <div class="alert alert-danger d-none" role="alert">There is an error with your form. Please correct it.</div>

          <form>
              <div class="form-group">
                  <label for="recipient-name" class="form-control-label"><?php echo $language['toname_placeholder_text']; ?></label>
                  <input type="text" class="form-control form-control-danger" id="recipient-name">
              </div>
              <div class="form-group has-feedback">
                  <label for="recipient-email" class="control-label"><?php echo $language['toemail_placeholder_text']; ?></label>
                  <input type="text" class="form-control form-control-danger" id="recipient-email">
              </div>
              <div class="form-group has-feedback">
                  <label for="sender-name" class="control-label"><?php echo $language['yourname_placeholder_text']; ?></label>
                  <input type="text" class="form-control form-control-danger" id="sender-name">
              </div>
              <div class="form-group has-feedback">
                  <label for="sender-email" class="control-label"><?php echo $language['youremail_placeholder_text']; ?></label>
                  <input type="text" class="form-control form-control-danger" id="sender-email">
              </div>

              <div class="form-group">
                  <label for="bshare-comments" class="control-label">Message:</label>
                  <textarea class="form-control" id="bshare-comments"></textarea>
              </div>
              <div class="form-group">
              <label for="cc-me" class="custom-control custom-checkbox">
                  <input id="cc-me" type="checkbox" name="cc-me" value="" checked="yes" class="custom-control-input">
                  <span class="custom-control-indicator"></span>
                  <span class="custom-control-description"><?php echo $language['send_a_copy_text']; ?></span>
              </label>
              </div>
          </form>


            <div class="modal-body-bottom">
              <input type="hidden" id="jsuseripaddress" value="<?php echo $ipaddress; ?>">
              <div class="g-recaptcha" id="fshare_captcha" data-sitekey=""></div>
              <button type="button" class="btn btn-primary btn-block btn-modal-share-submit">Share</button>
            </div>
        </div>
      </div>
    </div>
</div>





<div id="fs_toolbar">
    <div class="fs_toolbar-inner">
        <div class="fs_toolbar-actions clearfix">
            <button type="button" class="close" aria-label="Close">
                <i class="material-icons">clear</i>
            </button>
        </div>
        <ul class="nav flex-column fs_toolbar-menu">
            <li class="nav-item">
              <a class="nav-link" href="#collapseLanguageMenu" data-toggle="collapse" aria-expanded="false" aria-controls="collapseLanguageMenu"><i class="material-icons fs_nav-link-icon fs_icon-language">language</i> <span class="fs_nav-link-text"><?php echo $language['language_text']; ?></span><i class="material-icons fs_nav-link-icon float-right">expand_more</i></a>
              <div class="collapse" id="collapseLanguageMenu">
                  <ul class="nav flex-column">
                      <li class="nav-item"><a href="#" class="nav-link langclick" data-language="en_CA"><?php echo $language['english_text']; ?></a></li>
                      <li class="nav-item"><a href="#" class="nav-link langclick" data-language="fr_CA"><?php echo $language['french_text']; ?></a></li>
                  </ul>
              </div>
            </li>
            <li class="nav-item">
              <a href="#" class="nav-link fs_nav-link" data-toggle="modal" data-target="#fs_modal-alerts" aria-label="Set alerts"><i class="material-icons fs_nav-link-icon fs_icon-subscribe">notifications</i> <span class="fs_nav-link-text"><?php echo $language['set_alerts_text']; ?></span></a>
            </li>
            <li class="nav-item">
              <a href="#" class="nav-link" data-toggle="modal" data-target="#fs_modal-print" aria-label="Print to PDF"><i class="material-icons fs_nav-link-icon">print</i> <span class="fs_nav-link-text"><?php echo $language['print_text']; ?></span></a>
            </li>
            <li class="nav-item">
              <a href="#" class="nav-link" data-toggle="modal" data-target="#fs_modal-share" aria-label="Share"><i class="material-icons fs_nav-link-icon">share</i> <span class="fs_nav-link-text"><?php echo $language['share_this_text']; ?></span></a>
            </li>
        </ul>
    </div>
</div>





        <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>
        
        <script src="https://cdn.polyfill.io/v2/polyfill.min.js"></script>
        <script src="eclipse/js/babel.min.js"></script>
        
        <script src="lib/jquery.ui.touch-punch.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB5J3ADwMfQwtmYH0bLpKKaMz8cz-Po2Sk"></script>
        <!--<script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>-->
        <script src="eclipse/js/js-marker-clusterer-20160907/src/markerclusterer.js"></script>
        <script src="lib/owlcarousel/2.2.1/dist/owl.carousel.min.js"></script>
        <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.3.3/underscore-min.js"></script>
        <!--<script src="eclipse/js/underscorejs-edgeversion.js"></script>-->
       
        <script src="lib/malihu-custom-scrollbar/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
        
        <script src="eclipse/js/jquery.easy-autocomplete.min.js"></script>
        <script src="eclipse/js/moment.min.js"></script>
        <!--<script src="eclipse/js/urlsearchparams-MSEDGE.js"></script>-->
        <!--<script src="eclipse/js/url-polyfill.js"></script>-->
        
        <script src="eclipse/js/history.js/scripts/bundled/html4+html5/jquery.history.js"></script>
        <script src="https://www.google.com/recaptcha/api.js?onload=CaptchaCallback&render=explicit" async defer></script>

        <!--<script src="eclipse/js/history.js/scripts/uncompressed/history.js"></script>-->
        <!--<script type="text/babel" src="eclipse/js/processurl.js"></script>-->
        <script type="text/javascript" src="main.js"></script>
        <!--<script type="text/babel" src="eclipse/js/processurl.js"></script>-->
        



<?php if ($theme['clientjs'] == 1) { 
    include('theme/clientjs.php');
 } ?>










        <script type="text/template" id="ourHTML">
            <% var qq = 0 %>
            <% _.each( target[0], function(i) {%>
            <div class="col fs_search-result" data-buildingCode="<%= i.building_code %>">
            <div class="fs_search-result-property">    
            <div class="row no-gutters">
            <div class="col fs_search-result-thumbnail">
            <a class="fs_building-link" href="building-integrated.php?building=<%=i.building_code %>"><img src="<%= i.thumbnailGrid %>" alt="thumbnail" class="img-fluid fs_thumbnail-grid"></a>
            <a class="fs_building-link" href="building-integrated.php?building=<%=i.building_code %>"><img src="<%= i.thumbnailList %>" alt="thumbnail" class="img-fluid fs_thumbnail-list"></a>
            <div class="fs_search-result-grid-action">
            <button type="button" class="fs_search-result-remove-grid" data-buildingcode="<%= i.building_code %>"  aria-label="Close">
            <i class="material-icons fs_btn-icon">clear</i>
            </button>
            </div>
            </div>
            <div class="col fs_search-result-details">
            <div class="row no-gutters">
            <div class="col fs_search-result-details-header">
            <h3 class="fs_search-result-title"><a class="fs_building-link" href="building-integrated.php?building=<%=i.building_code %>"><%= i.building_name %></a></h3>
            <h4 class="fs_search-result-address"><%= i.street_address %> <%= i.city %> <%= i.province %></h4>
            <% var jTest = 0 
               var noSuites = 0
            _.each(target[1][i.building_code], function(i) {
            jTest++ 
            }); 
            if (jTest != 0) { 
            if (jTest === 1) { 
            jTest = "View " + jTest + " <?php echo $language['unit_available_text']; ?>" 
            } 
            if (jTest > 1) { 
            jTest = "View " + jTest + " <?php echo $language['units_available_text']; ?>" 
            } 
            } 
            if (jTest === 0) { 
            noSuites = 1
            jTest = "<?php echo $language['no_available_unit_text']; ?>" 
            if (i.building_type == "Retail" || i.building_type == "Detail") {
                jTest = "<?php echo $language['call_for_availability_text']; ?>"
            } else {
                jTest = "<?php echo $language['all_leased_text']; ?>"
            }

            
            
            
            
            } %>  
            <a href="building-integrated.php?building=<%=i.building_code %>" class="fs_available-suites-link"><%= jTest %></span></a>
            <div id="fs_collapse-<%= i.building_code %>" class="fs_search-result-collapse fs_collapse-availabilities" role="tablist">

            <div class="fs_collapse-header" role="tab" id="heading-<%= i.building_code %>">
            <h5 class="fs_collapse-title">
            <a class="fs_collapse-toggle" data-toggle="collapse" href="#collapse-<%= i.building_code %>" aria-expanded="true" aria-controls="collapse-<%= i.building_code %>">

            <span class="fs_nav-link-text"><%= jTest %></span> <i class="material-icons fs_nav-link-icon">expand_less</i>
            </a>
            </h5>
            </div>
            </div>
            </div>
            <div class="col-auto fs_col-remove">
            <button type="button" class="fs_search-result-remove" data-buildingcode="<%= i.building_code %>" aria-label="Close">
            <i class="material-icons fs_btn-icon">clear</i>
            </button>
            </div>
            </div>
            </div>
            </div>
            
            <div class="row no-gutters fs_results-availabilities">
            <div class="col">
            
            <div id="collapse-<%= i.building_code %>" class="collapse show fs_search-result-collapse">

<% if (noSuites === 0) { %>

            <table class="table table-sm fs_search-result-table">

            <% if (i.building_type == "Industrial" || i.building_type == "Industriel") { %>                
            <thead>
            <tr>
            <th></th>
            <th class="fs_result-suite-name"><?php echo $language['suites_plural_lowercase']; ?></th>
            <th class="fs_result-suite-area"><?php echo $language['size_text_lowercase']; ?><span class="small">/<?php echo $language['building_information_sq_ft_trans']; ?></span></th>
            <th class="fs_result-suite-availability"><?php echo $language['index_table_header_availability_text']; ?></th>
            <th class="fs_result-suite-rent"><?php echo $language['price_text']; ?><span class="small">/<?php echo $language['month_abbr']; ?></span></th>
            <th class="fs_result-suite-loading"><?php echo $language['loading_text']; ?></th>
            <th class="fs_result-suite-power"><?php echo $language['power_text']; ?></th>
            <th class="fs_result-suite-ceiling"><?php echo $language['ceiling_text']; ?></th>
            </tr>
            </thead>
            <% } else { %>
            <thead>
            <tr>
            <th></th>
            <th class="fs_result-suite-name"><?php echo $language['suites_plural_lowercase']; ?></th>
            <th class="fs_result-suite-area"><?php echo $language['size_text_lowercase']; ?><span class="small">/<?php echo $language['building_information_sq_ft_trans']; ?></span></th>
            <th class="fs_result-suite-availability"><?php echo $language['index_table_header_availability_text']; ?></th>
            <th class="fs_result-suite-rent"><?php echo $language['price_text']; ?><span class="small">/<?php echo $language['month_abbr']; ?></span></th>    
            <th class=""><?php echo $language['notes_text']; ?></th>

            </tr>
            </thead>
            <% } %>


            <tbody>


            <% if (i.building_type == "Industrial"|| i.building_type == "Industriel" ) { %>                
            <% _.each( target[1][i.building_code], function(jj) {%>
            <% var suiteBadge = '' 
               var suiteTrClass = 'suitesList'; %>
            <% if (jj.new == 'true') {
                suiteBadge = '<span class="badge badge-secondary fs_badge-new">New</span>';
                suiteTrClass = 'suitesList';
                }
                if (jj.model === 'true') {
                suiteBadge = '<span class="badge badge-secondary fs_badge-new">Model</span>';
                suiteTrClass = 'suitesList';
                }
                if (jj.leased === 'true') {
                suiteBadge = '<span class="badge badge-secondary fs_badge-new">Leased</span>';
                suiteTrClass = 'suitesList leased';
                }
                if (jj.promoted === 'true') {
                suiteBadge = '<span class="badge badge-secondary fs_badge-new">Promoted</span>';
                suiteTrClass = 'suitesList';
                }%>
            
            
            <tr class="<%= suiteTrClass %>">
            <td><button type="button" class="fs_btn-favourite" data-suite-id="<%= jj.suite_id %>" data-building-id="<%= jj.building_code %>"><i class="material-icons fs_icon-favorite">favorite</i></button></td>  
            <td class="fs_result-suite-name"><a class="suiteLink" href="building-integrated.php?building=<%=i.building_code %>&suiteid=<%= jj.suite_id %>"><span class="fs_btn-text"><%= jj.suite_name %></span><%= suiteBadge %></a></td>
            <td class="fs_result-suite-area" data-rawsize="<%= jj.rawArea %>"><%= jj.formattedArea %></td>
            <td class="fs_result-suite-availability"><%= jj.formattedAvailability %></td>
            <td class="fs_result-suite-rent"><%= jj.formattedNetRent %></td>
            <% if (jj.shipping_doors_drive_in != 'n/a') { %>
            <% var loadingOut = jj.shipping_doors_drive_in + " D/I" %>
            <% } else {
               var loadingOut = jj.shipping_doors_drive_in 
               }%>
            <% if (jj.shipping_doors_truck != 'n/a') { %>
            <% loadingOut += " / " + jj.shipping_doors_truck + " DK" %>
            <% } else {
               loadingOut += " / " + jj.shipping_doors_truck 
               }%>
            <td class="fs_result-suite-loading"><%= loadingOut %></td>
            <% if (jj.available_electrical_amps != 'n/a') { %>
            <% var elecOut = jj.available_electrical_amps + "a"; %>
            <% } else {
                var elecOut = jj.available_electrical_amps;
               }%>
            <% if (jj.available_electrical_volts != 'n/a') { %>
            <% elecOut += " / " + jj.available_electrical_volts + "v" %>
            <% } else {
               elecOut += " / " + jj.available_electrical_volts
               }%>
            <td class="fs_result-suite-power"><%= elecOut %></td>
            <td class="fs_result-suite-ceiling"><%= jj.clear_height %></td>
            </tr>  
            <% }); %>
            <% } else { %>

            <% _.each( target[1][i.building_code], function(jj) {%>
            <% var suiteBadge = '' 
               var suiteTrClass = 'suitesList'; %>
            <% if (jj.new == 'true') {
                suiteBadge = '<span class="badge badge-secondary fs_badge-new">New</span>';
                suiteTrClass = 'suitesList';
                }
                if (jj.model === 'true') {
                suiteBadge = '<span class="badge badge-secondary fs_badge-new">Model</span>';
                suiteTrClass = 'suitesList';
                }
                if (jj.leased === 'true') {
                suiteBadge = '<span class="badge badge-secondary fs_badge-new">Leased</span>';
                suiteTrClass = 'suitesList leased';
                }
                if (jj.promoted === 'true') {
                suiteBadge = '<span class="badge badge-secondary fs_badge-new">Promoted</span>';
                suiteTrClass = 'suitesList';
                }%>
            <% if (jj.description == '') {
            notes = '';
            } %>
            <tr class="<%= suiteTrClass %>">
            <td><button type="button" data-suite-id="<%= jj.suite_id %>" data-building-id="<%= jj.building_code %>" class="fs_btn-favourite"><i class="material-icons fs_icon-favorite">favorite</i></button></td>  
            <td class="fs_result-suite-name"><a class="suiteLink" href="building-integrated.php?building=<%=i.building_code %>&suiteid=<%= jj.suite_id %>"><span class="fs_btn-text"><%= jj.suite_name %></span><%= suiteBadge %></a></td>
            <td class="fs_result-suite-area" data-rawsize="<%= jj.rawArea %>"><%= jj.formattedArea %></td>
            <td class="fs_result-suite-availability"><%= jj.formattedAvailability %></td>
            <td class="fs_result-suite-rent"><%= jj.formattedNetRent %></td>    
            <td class="fs_result-suite-notes"><%= jj.description %></td>
            </tr>  
            <% }); %>
            <% } %>






            </tbody>
            </table>
    
    
    <% } else { %>
                
                
                
                
                
                
    <% } %>
    
    
            <div class="fs_other-costs">
            <h5 class="fs_other-costs-title"><?php echo $language['additional_costs_text']; ?></h5>
            <ul class="fs_other-costs-list">
            <li class="fs_result-additional-rent"><?php echo $language['additional_rent_text']; ?> <span class="fs_result-value"> <%= i.add_rent_total %></span></li>
            <li class="fs_result-operating-costs"><?php echo $language['operating_costs_text']; ?><span class="fs_result-value"> <%= i.add_rent_operating %></span></li>
            <li class="fs_result-realty-tax"><?php echo $language['realty_tax_text']; ?><span class="fs_result-value"> <%= i.add_rent_realty %></span></li>
            </ul>
            </div>

            </div>
            </div>
            </div>
            
            
            </div>
            </div>
            <% qq++ %>
            <% }); %>
        </script>

        <!--        <div class="navbar navbar-fixed-top">
                    <div class="navbar-inner">
                        <div class="container">
                            <a href="" class="btn" id="genJson">Show JSON String</a>
                            <a href="" class="btn" id="genHtml">Generate HTML</a>
                        </div>
                    </div>
                </div>
                <div id="output">Click a button above to transform the raw JS data.</div>

    </body>
</html>



