<?php
	include('theme/db.php');

if (isset($_POST['bu_name'])) {
$bu_name = strip_tags($_POST['bu_name']);
$bu_email = $_POST['email'];
$province_selected = $_POST['province'];
$SuiteType_selected = $_POST['SuiteType'];
$region_selected = $_POST['region'];
$sub_region_selected = $_POST['sub-region'];
$size_selected = $_POST['Size'];
$availability_selected = $_POST['Availability'];
$subscription_language = $_POST['subscription_language'];
$subscription_frequency = $_POST['subscription_frequency'];
$query_string = '';

if (isset($_POST['lang']) && $_POST['lang'] != '') {
    $lang = $_POST['lang'];
} else {
    $lang = $subscription_language;
}


$query_string = 'region='.$region_selected.'&province='.$province_selected.'&sub-region='.$sub_region_selected.'&SuiteType='.$SuiteType_selected.'&Size='.$size_selected.'&Availability='.$availability_selected.'&email='.$bu_email.'&lang='.$lang;


// Build Report title
		
		$title = '';

		if ($province_selected != 'All') {
			$title .= $province_selected;
		} else {
			$title .= "All";
		}

		if ($region_selected != 'All') {
			$title .= ' | '.$region_selected;
		}

		if ($sub_region_selected != 'All') {
			$title .= ' | '. $sub_region_selected;
		}

		if ($SuiteType_selected != 'All') {
			$title .= ' | '. $SuiteType_selected;
		}

		if ($size_selected != 'All') {
			$title .= ' | '. $size_selected;
		}

		if ($availability_selected != 'All') {
			$title .= ' | '. $availability_selected;
		}

	
		
		if (($province_selected == 'All') && ($region_selected == 'All') && ($sub_region_selected == 'All') && ($SuiteType_selected == 'All') && ($size_selected == 'All') && ($availability_selected == 'All')) {
			
			if ($lang == 'fr_CA') {
				$title = "toutes les inscriptions";
			} else {
				$title = "All Listings";
			}
			
		}
		
		

		
		
		
		
		// generate unique request id
		$uniqueid = uniqid('bu_');
		
		// add signup criteria to database
		$brokerupdates_signup_query = "insert into bu_subscriptions (title, bu_name, email, bu_query_string, unique_id, subscription_language, subscription_frequency, date_of_consent, updated_at, type) VALUES ('".mysql_real_escape_string($title)."','". mysql_real_escape_string($bu_name) ."','". mysql_real_escape_string($bu_email) ."','". mysql_real_escape_string($query_string) ."','". mysql_real_escape_string($uniqueid) ."','". mysql_real_escape_string($subscription_language) ."','". mysql_real_escape_string($subscription_frequency) ."', '". date("Y/m/d, g:ia") . "', '". date("Y/m/d, g:ia") . "', 'self registered')";
//	echo $brokerupdates_signup_query."<br><br>";
	$brokerupdates_signup = mysql_query($brokerupdates_signup_query) or die ("brokerupdates signup query error: " . mysql_error());

        
        
        echo $uniqueid;

}




if (isset($_GET['action']) && $_GET['action'] == "checksubscriptionlang") {
    $enteredEmail = $_GET['enteredEmail'];
    
    $checksubscriptionlang_query_string = "select * from bu_subscriptions where email = '" . $enteredEmail . "'";
    $checksubscriptionlang_query = mysql_query($checksubscriptionlang_query_string) or die ("check subscriptionlang error: " . mysql_error());
    $checksubscriptionlang = mysql_fetch_array($checksubscriptionlang_query);
    $checksublang_count = mysql_num_rows($checksubscriptionlang_query);
    
    $already_chosen_language="";
    if ($checksublang_count > 0) {
        $already_chosen_language = $checksubscriptionlang['subscription_language'];
    }
    // return $already_chosen_language
    echo $already_chosen_language;    
}


if (isset($_GET['action']) && $_GET['action'] == "checksubscriptionfreq") {
    $enteredEmail = $_GET['enteredEmail'];
    
    $checksubscriptionfreq_query_string = "select * from bu_subscriptions where email = '" . $enteredEmail . "'";
    $checksubscriptionfreq_query = mysql_query($checksubscriptionfreq_query_string) or die ("check subscriptionfreq error: " . mysql_error());
    $checksubscriptionfreq = mysql_fetch_array($checksubscriptionfreq_query);
    $checksubfreq_count = mysql_num_rows($checksubscriptionfreq_query);
    
    $already_chosen_frequency="";
    if ($checksubfreq_count > 0) {
        $already_chosen_frequency = $checksubscriptionfreq['subscription_frequency'];
    }
    // return $already_chosen_frequency
    echo $already_chosen_frequency;    
}


?>