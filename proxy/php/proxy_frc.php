<?php
// Adapted from 'proxy.php' by Troy Wolf <troy@troywolf.com>
// By Sebastian Kun <skun@arcestra.com>
//
//   DESCRIPTION: Allow scripts to request content they otherwise may not be
//                able to. For example, AJAX (XmlHttpRequest) requests from a
//                client script are only allowed to make requests to the same
//                host that the script is served from. This is to prevent
//                "cross-domain" scripting. With proxy.php, the javascript
//                client can pass the requested URL in and get back the
//                response from the external server.
//
//         USAGE: "proxy_url" required parameter. HOSTNAME is prepended to it.
//                For example:
//                  http://www.example.com/proxy.php?proxy_url=path/to/resource.html
//                Would retrieve the contents of HOSTNAME/path/to/resource.html
//

// proxy.php requires Troy's class_http. http://www.troywolf.com/articles
// Alter the path according to your environment.

	mysql_connect('localhost', 'dbmaster', 'zu:b@px', true);
	$link = mysql_connect('localhost', 'dbmaster', 'zu:b@px', true);
	mysql_select_db('crons');
	mysql_set_charset('utf8',$link);


	$clientname = $_REQUEST['clientname'];

	// Cron is now controlled by the DB
	
	$cron_query_statement = "Select * from crondata where client_name = '".$clientname."'";
	$cron_query = mysql_query($cron_query_statement);
	
	$cron = mysql_fetch_array($cron_query);
		$apiuser = $cron['api_username'];
		$apipass = $cron['api_password'];
		$apilink = $cron['api_hostname'];

	require_once("class_http.php");
	
	define ('HOSTNAME', $apilink);
	define ('USERNAME', $apiuser);
	define ('PASSWORD', $apipass);

$proxy_url = isset($_GET['proxy_url'])?$_GET['proxy_url']:false;
if (!$proxy_url) {
    header("HTTP/1.0 400 Bad Request");
    echo "proxy.php failed because proxy_url parameter is missing";
    exit();
}
$encoded_url = HOSTNAME.str_ireplace("%2F", "/", urlencode($proxy_url));

// Instantiate the http object used to make the web requests.
// More info about this object at www.troywolf.com/articles
if (!$h = new http()) {
    header("HTTP/1.0 501 Script Error");
    echo "proxy.php failed trying to initialize the http object";
    exit();
}

$h->url = $encoded_url . "?language=fr_CA";
$h->postvars = $_POST;
if (!$h->fetch($h->url, 0, "", USERNAME, PASSWORD)) {
    header("HTTP/1.0 501 Script Error");
    echo "proxy.php had an error attempting to query the url";
    exit();
}

// Forward the headers to the client.
$ary_headers = split("\n", $h->header);
foreach($ary_headers as $hdr) { header($hdr); }

// Send the response body to the client.
echo $h->body;
?>
