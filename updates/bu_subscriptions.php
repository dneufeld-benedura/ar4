<?php
include('../theme/db.php');


$bu = new bu_subscriptions();
$arr = $bu->getSubscriptionData("");


?>
<html>
    <head>
        <style>
        table, th, td {
            
            border-collapse: collapse;
        }
        </style>
        
        
        
        <title>Broker Update Statistics for <?php echo  $bu->mysql_current_db();?></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        
        <h2>Broker Update Statistics for <?php echo  $bu->mysql_current_db();?></h2>
        
        <table style="width:25%">
  <tr>
    <td>Active Subscriptions: </td>
    <td><?php echo $arr['active']?></td>
  </tr>
  
    <tr>
    <td>Inactive Subscriptions: </td>
    <td><?php echo $arr['inactive']?></td>
  </tr>
  
    <tr>
    <td>Number of Unique Email Addresses: </td>
    <td><?php echo $arr['emails']?></td>
  </tr>
  
 </table>
        
    </body>
</html>

<?php

class bu_subscriptions{
    
    /**
     * This function return the current database
     * @return type
     */
    public function mysql_current_db() {
    $r = mysql_query("SELECT DATABASE()") or die(mysql_error());
    return mysql_result($r,0);
    }
    
   
    /**
     * This function returns an array of subscription data, for a current client
     * The array keys are: active (the number of active users), inactive (the number of inactive users), emails (the total number of emails)
     */
    public function getSubscriptionData(){
        

        
        //Initiate the query to get activities
        $activityQuery = 'SELECT 
        id,
        title,
        bu_name,
        bu_query_string,
        unique_id,
        active
        FROM
        bu_subscriptions 
        WHERE email IS NOT null
        AND 
        email !=\'\'
        AND
        email NOT LIKE "%@rationalroot.com%" 
        AND 
        email NOT LIKE "%@arcestra.com%";';

        //Initiate the query to get the number of emails
        $countEmailsQuery = '
        SELECT COUNT(*) as numberOfEmails FROM (SELECT DISTINCT email 
        FROM
        bu_subscriptions 
        WHERE email IS NOT null
        AND 
        email !=\'\'
        AND
        email NOT LIKE "%@rationalroot.com%" 
        AND 
        email NOT LIKE "%@arcestra.com%") as t1;';
        
        
        //Initiate the array to return
        $ret = array();
        
        //now run the queries
        
        $activity_result = mysql_query($activityQuery);
        //Count the number of active VS non-active, this is O(1)
        $activeUsers = 0;
        $inactiveUsers = 0;
        while ($row = mysql_fetch_assoc($activity_result)){
           if ($row['active'] == "1"){$activeUsers=$activeUsers+1;}
           else {$inactiveUsers=$inactiveUsers+1;}
        }
        
        //add values to HashMap
        $ret['active'] = $activeUsers;
        $ret['inactive'] = $inactiveUsers;
        
        //get the number of e-mails
        $emails_result = mysql_query($countEmailsQuery);
        //since we have only one row, no loop is necessary
        $email_row = mysql_fetch_assoc($emails_result);
        $numberOfEmails = $email_row['numberOfEmails'];
        
        //add value to HashMap
        $ret['emails'] = $numberOfEmails;
        
        //return
        return $ret;
    }
    
    
    
}

