<?php

/**
 * @author Rational Root
 * @see ../main_abstract/SearchData_abstract.php
 * @since March 2014
 * @version 1.6
 * */
class Queries {

    /**
     * This method simply adds a variable to the query string, gracefully.
     * It returns the concatenated $query_string, $var, and $appendee, depending on the content of the mentioned variables.
     * Accesses either the abstract or its overriden method from the classes it extends.
     * <p>
     * @param  {String} $query_string
     * @param  {String} $var
     * @param  {String} $appendee
     * @returns {String} $concat
     * @see building.php
     * @see index.php
     * @see suite.php
     * */
    public function append_to_query_str($query_string, $var, $appendee) {
        $concat = $var . "=" . $appendee;
        if ($query_string != "" && $query_string != "&") {
            $concat = $query_string . "&" . $concat;
        }
        return $concat;
    }

    /*
     * This method gets the query string from $_SERVER array
     * Accesses either the abstract or its overriden method from the classes it extends.
     * <p>
     * @returns {String} $_SERVER['QUERY_STRING']
     * @see building.php
     * @see index.php
     * @see suite.php
     * @see /main_abstract/Queries.php
     * @see printpdf.php
     * */

    public function get_query_str() {
        if ($_SERVER['QUERY_STRING'] == "") {
            return "";
        }
        return '&' . $_SERVER['QUERY_STRING'];
    }

    /*
     * This method gets the query string from $_SERVER array
     * Accesses either the abstract or its overriden method from the classes it extends.
     * <p>
     * @returns {String} $_SERVER['QUERY_STRING']
     * @see building.php
     * @see index.php
     * @see suite.php
     * @see /main_abstract/Queries.php
     * @see printpdf.php
     * */

    public function get_query_str_print() {
        if ($_SERVER['QUERY_STRING'] == "") {
            return "";
        }

        return $_SERVER['QUERY_STRING'];
    }
    
    
    
    
    
    
    
    
    
    public function minMaxQuery($type_selected, $size_selected, $db) {
        $size_result_query_string = "SELECT minimum,maximum FROM ".$type_selected." WHERE size =?";
                        
        if ($size_result_query_statement = $db->prepare($size_result_query_string)) {
            // carry on
        } else {
            trigger_error('min max query string error ' . $type_selected . ' SQL: ' . $size_result_query_string . '-- Error: ' . $db->error, E_USER_ERROR);
        }
        $size_result_query_statement->bind_param('s', mysqli_real_escape_string($db, $size_selected));
        $size_row = array();
        if ($size_result_query_statement->execute()){
            $result = $size_result_query_statement->get_result();
            // MYSQLI_BOTH gives us both named and indexed array fields
            // so we can use $row['minimum'] OR $row[0]
            $row = $result->fetch_array(MYSQLI_BOTH);
        } else {
            trigger_error('min max statement error: ' . $type_selected . ' SQL: ' . $size_result_query_string . ' Error: ' . $db->error, E_USER_ERROR);
        }
        $size_result_query_statement->close();
        return $row;
    }
    
    

    /*
     * This method gets the query string from $_SERVER array
     * Accesses either the abstract or its overriden method from the classes it extends.
     * <p>
     * @param {String} $size_selected
     * @param {String} $suitetype_selected
     * @param {String} $buildingtype_selected
     * @param {String} $province_selected
     * @param {String} $region_selected
     * @param {String} $sub_region_selected
     * @param {String} $building_codes_narrow
     * @param {Integer} $lang_id
     * @param {Integer} $availability_notafter
     * @param {Integer} $availability_notbefore
     * @returns {String} $buildings_query_string
     * @see /main_abstract/Queries.php
     * @see index.php
     * */

    public function getBuildingsQueryString($size_selected, $sizeFrom, $sizeTo, $suitetype_selected, $buildingtype_selected, $province_selected, $region_selected, $city_selected, $sub_region_selected, $building_codes_narrow, $lang_id, $availability_notafter, $availability_notbefore, $building_name_sterm, $show_no_vacancy, $industrial_office_space_selected, $industrial_clear_height_selected, $industrial_shipping_doors_selected, $industrial_drivein_doors_selected, $industrial_electrical_volts_selected, $industrial_electrical_amps_selected, $industrial_parking_stalls_selected, $industrial_trucktrailer_parking_selected, $industrialsearchelement, $advanced_search, $db) {
//echo "CALLED IT<br>";
        
        $building_paramtypes = array();
        $building_bindvars = array();
        $buildingSuffix_paramtypes = array();
        $buildingSuffix_bindvars = array();
        $buildingUnion_param = array();
        $buildingUnion_bindvars = array();
        
        
        $building_name_selected = "";
        if ($building_name_sterm != '') {
            $building_name_selected = $building_name_sterm;
        }

//        $building_name_selected = "";
//        if (isset($building_name_sterm) && $building_name_sterm != "" && $building_name_sterm != "none") {
//            $pipesposition = stripos($building_name_sterm, " || ");
//            $justbuildingname = substr($building_name_sterm, 0, $pipesposition);
//            $building_name_selected = mysql_real_escape_string($justbuildingname);
//        }
//        
            // size query
            if ($size_selected == "Custom") {
                $size_row[0] = $sizeFrom;
                if ($sizeTo == '') {
                    $size_row[1] = 2147483635;
                } else {
                    $size_row[1] = $sizeTo;
                }
                
            } else if ($size_selected != "All" && $size_selected != "Tous") {
                        $size_row = $this->minMaxQuery("sizes", $size_selected, $db);
                        
		} else {
			$size_row[0] = -1;
			$size_row[1] = 2147483647;
		}
 
        
		$industrial_search = 0;
		if ($suitetype_selected == "INDUSTRIAL" || $suitetype_selected == "INDUSTRIEL" || $suitetype_selected == "industrial" || $suitetype_selected == "industriel" || $suitetype_selected == "Industrial" || $suitetype_selected == "Industriel") {
		 	$industrial_search++;
		}
 
 
 

 
 
 
        if (isset($industrial_office_space_selected) && $industrial_office_space_selected !='All' && $industrial_office_space_selected !='') {
                // industrial unit office space query
//            $industrial_unit_office_space_result = mysql_query("SELECT minimum,maximum FROM office_space WHERE size ='" . $industrial_office_space_selected . "'")or die("industrial office space min max query string error #2: " . mysql_error());
//            $industrial_unit_office_space_row = mysql_fetch_array($industrial_unit_office_space_result, MYSQL_NUM);
            
            $industrial_unit_office_space_row = $this->minMaxQuery("office_space", $industrial_office_space_selected, $db);
            
        }
        
        if (isset($industrial_clear_height_selected) && $industrial_clear_height_selected !='All' && $industrial_clear_height_selected !='') {
//            $industrial_clear_height_result = mysql_query("SELECT minimum,maximum FROM clear_height_ranges WHERE size ='" . $industrial_clear_height_selected . "'")or die("industrial clear_height min max query string error #3: " . mysql_error());
//            $industrial_clear_height_row = mysql_fetch_array($industrial_clear_height_result, MYSQL_NUM);
            $industrial_clear_height_row = $this->minMaxQuery("clear_height_ranges", $industrial_clear_height_selected, $db);
        }
        

        
        if (isset($industrial_shipping_doors_selected) && $industrial_shipping_doors_selected !='All' && $industrial_shipping_doors_selected !='') {
//        $industrial_unit_shipping_doors_result = mysql_query("SELECT minimum,maximum FROM num_shipping_doors WHERE size ='" . $industrial_shipping_doors_selected . "'")or die("industrial shipping_doors min max query string error #4: " . mysql_error());
//        $industrial_unit_shipping_doors_row = mysql_fetch_array($industrial_unit_shipping_doors_result, MYSQL_NUM);
        $industrial_unit_shipping_doors_row = $this->minMaxQuery("num_shipping_doors", $industrial_shipping_doors_selected, $db);
        }
        
        if (isset($industrial_drivein_doors_selected) && $industrial_drivein_doors_selected !='All' && $industrial_drivein_doors_selected !='') {
//        $industrial_unit_drivein_doors_result = mysql_query("SELECT minimum,maximum FROM num_drivein_doors WHERE size ='" . $industrial_drivein_doors_selected . "'")or die("industrial drivein_doors min max query string error #5: " . mysql_error());
//        $industrial_unit_drivein_doors_row = mysql_fetch_array($industrial_unit_drivein_doors_result, MYSQL_NUM);       
        $industrial_unit_drivein_doors_row = $this->minMaxQuery("num_drivein_doors", $industrial_drivein_doors_selected, $db);
        }     
        

        if (isset($industrial_electrical_volts_selected) && $industrial_electrical_volts_selected !='All' && $industrial_electrical_volts_selected !='') {
//            $industrial_unit_electrical_volts_result = mysql_query("SELECT minimum,maximum FROM electrical_volts WHERE size ='" . $industrial_electrical_volts_selected . "'")or die("$ndustrial electrical_volts min max query string error: #6 " . mysql_error());
//            $industrial_unit_electrical_volts_row = mysql_fetch_array($industrial_unit_electrical_volts_result, MYSQL_NUM);
            $industrial_unit_electrical_volts_row = $this->minMaxQuery("electrical_volts", $industrial_electrical_volts_selected, $db);
        }
        
        if (isset($industrial_electrical_amps_selected) && $industrial_electrical_amps_selected !='All' && $industrial_electrical_amps_selected !='') {
//            $industrial_unit_electrical_amps_result = mysql_query("SELECT minimum,maximum FROM electrical_amps WHERE size ='" . $industrial_electrical_amps_selected . "'")or die("industrial electrical_amps min max query string error: #7 " . mysql_error());
//            $industrial_unit_electrical_amps_row = mysql_fetch_array($industrial_unit_electrical_amps_result, MYSQL_NUM);
            $industrial_unit_electrical_amps_row = $this->minMaxQuery("electrical_amps", $industrial_electrical_amps_selected, $db);
        }


        if (isset($industrial_parking_stalls_selected) && $industrial_parking_stalls_selected !='All' && $industrial_parking_stalls_selected !='') {
//            $industrial_unit_parking_stalls_result = mysql_query("SELECT minimum,maximum FROM surface_stalls_all WHERE size ='" . $industrial_parking_stalls_selected . "'")or die("industrial surface_stalls min max query string error #8: " . mysql_error());
//            $industrial_unit_parking_stalls_row = mysql_fetch_array($industrial_unit_parking_stalls_result, MYSQL_NUM);   
            $industrial_unit_parking_stalls_row = $this->minMaxQuery("surface_stalls_all", $industrial_parking_stalls_selected, $db);
        }

        $suitestuff_selected = 0;
		$buildings_query_string = "select distinct allb.region, allb.city, allb.building_id, allb.building_code, allb.building_name, allb.building_type, allb.street_address, allb.city, allb.province, allb.country, allb.postal_code pc, allb.sort_streetname, allb.sort_streetnumber, allb.latitude, allb.longitude, allb.add_rent_operating, allb.add_rent_realty, allb.add_rent_power, allb.add_rent_total";

		if ($suitetype_selected != 'All') {
			$buildings_query_string .= ", alls.suite_type as ourtype";
		}
		

		if (($suitetype_selected != 'All') || ($size_row[0] != '-1' && $size_row[1] != '2147483647') || ($availability_notbefore != '0' && $availability_notafter != '2147483640')) {                
			$suitestuff_selected = 1;
		}


        
		$buildings_query_string .= ' from buildings allb';
		
	   
		
		if ($suitestuff_selected > 0 || $show_no_vacancy != 1) {
			$buildings_query_string .= ', suites alls';
		} 
		
		if ($industrial_search > 0 && $advanced_search > 0) {
			$buildings_query_string .= ', suites_specifications allss';
		}             
			
		$buildings_query_string .= ' where allb.lang=?';    
                array_push($building_paramtypes, "i");
                array_push($building_bindvars, $lang_id);
                
                
                if ($city_selected != '' && $city_selected != "All") {
                    $buildings_query_string .= ' and allb.city=?';    
                    array_push($building_paramtypes, "s");
                    array_push($building_bindvars, $city_selected);
                }
                
                
                
			
		if ($suitestuff_selected > 0 || $show_no_vacancy != 1) {
			$buildings_query_string .= ' and alls.lang=?';
                        array_push($building_paramtypes, "i");
                        array_push($building_bindvars, $lang_id);
		} 
		
		if ($industrial_search > 0 && $advanced_search > 0) {
			$buildings_query_string .= ' and allss.lang=?';
                        array_push($building_paramtypes, "i");
                        array_push($building_bindvars, $lang_id);
		}             
			
//		if ($suitestuff_selected > 0 || $show_no_vacancy != 1) {
//			$buildings_query_string .= ' AND allb.building_id = alls.building_id';
//		} 
		
		if ($suitetype_selected != 'All') {
			$buildings_query_string .= " AND alls.suite_type =? AND alls.suite_type =?";
                        array_push($building_paramtypes, "s");
                        array_push($building_bindvars, $suitetype_selected);
                        array_push($building_paramtypes, "s");
                        array_push($building_bindvars, $buildingtype_selected);
			
		}
//		if (($industrial_search > 0 && $advanced_search > 0)) {
//			$buildings_query_string .= " AND allb.building_id = allss.building_id";
//			
//		}
            



        if ($size_row[0] != '-1' && $size_row[1] != '2147483647') {
            $buildings_query_string .= ' AND ((alls.net_rentable_area BETWEEN ? and ?) OR (alls.contiguous_area BETWEEN ? and ?) OR (alls.min_divisible_area BETWEEN ? and ?))';
            $suitestuff_selected = 2;
            array_push($building_paramtypes, "s");
            array_push($building_bindvars, $size_row[0]);
            array_push($building_paramtypes, "s");
            array_push($building_bindvars, $size_row[1]);
            array_push($building_paramtypes, "s");
            array_push($building_bindvars, $size_row[0]);
            array_push($building_paramtypes, "s");
            array_push($building_bindvars, $size_row[1]);
            array_push($building_paramtypes, "s");
            array_push($building_bindvars, $size_row[0]);
            array_push($building_paramtypes, "s");
            array_push($building_bindvars, $size_row[1]);
        }


        
        if (($availability_notbefore != '0' && $availability_notbefore != '') && ($availability_notafter != '2147483640' && $availability_notafter != '')) {
//            echo "ping<br><br>";
            $buildings_query_string .= " AND alls.availability > ? and alls.availability < ? ";
            
            $suitestuff_selected = 2;
            array_push($building_paramtypes, "i");
            array_push($building_bindvars, $availability_notbefore);
            array_push($building_paramtypes, "i");
            array_push($building_bindvars, $availability_notafter);
        }
        
        
        
        

    if ($industrial_search > 0 && $advanced_search > 0) {       
        // industrial suite advanced search elements
        // 
        // "Office Space" search by percentage
        if (($industrial_unit_office_space_row[0] != '-1' && $industrial_unit_office_space_row[0] != '') && ($industrial_unit_office_space_row[1] != '101' && $industrial_unit_office_space_row[1] != '')) {
            
            // ONLY active if SIZE is selected in main search
            if ($size_row[0] != '-1' && $size_row[1] != '2147483647') {

                $min_total_space = $size_row[0];
                $max_total_space = $size_row[1];

                $minpercentage = $industrial_unit_office_space_row[0] / 100;
                $maxpercentage = $industrial_unit_office_space_row[1] / 100;

                $min_office_space = $min_total_space * $minpercentage;
                $max_office_space = $max_total_space * $maxpercentage;
                


                $buildings_query_string .= ' AND (allss.office_space BETWEEN ? AND ?)';
                array_push($building_paramtypes, "s");
                array_push($building_bindvars, $min_office_space);
                array_push($building_paramtypes, "s");
                array_push($building_bindvars, $max_office_space);
            }
        }

	
        if (isset($industrial_clear_height_row[0]) && ($industrial_clear_height_row[0] != '-1' && $industrial_clear_height_row[0] != '') && ($industrial_clear_height_row[1] != '201' && $industrial_clear_height_row[1] != '')) {
                $buildings_query_string .= ' AND (allss.clear_height_sort BETWEEN ? and ?)';
                array_push($building_paramtypes, "s");
                array_push($building_bindvars, $industrial_clear_height_row[0]);
                array_push($building_paramtypes, "s");
                array_push($building_bindvars, $industrial_clear_height_row[1]);
        }  
                        
 
	
        if (isset($industrial_unit_electrical_volts_row[0]) && ($industrial_unit_electrical_volts_row[0] != '-1' && $industrial_unit_electrical_volts_row[0] != '') && ($industrial_unit_electrical_volts_row[1] != '2147483647' && $industrial_unit_electrical_volts_row[1] != '')) {
            $buildings_query_string .= ' AND (allss.available_electrical_volts BETWEEN ? and ?)';
            array_push($building_paramtypes, "s");
            array_push($building_bindvars, $industrial_unit_electrical_volts_row[0]);
            array_push($building_paramtypes, "s");
            array_push($building_bindvars, $industrial_unit_electrical_volts_row[1]);
        }    

        if (isset($industrial_unit_electrical_amps_row[0]) && ($industrial_unit_electrical_amps_row[0] != '-1' && $industrial_unit_electrical_amps_row[0] != '') && ($industrial_unit_electrical_amps_row[1] != '2147483647' && $industrial_unit_electrical_amps_row[1] != '')) {
            $buildings_query_string .= ' AND (allss.available_electrical_amps BETWEEN ? and ?)';
            array_push($building_paramtypes, "s");
            array_push($building_bindvars, $industrial_unit_electrical_amps_row[0]);
            array_push($building_paramtypes, "s");
            array_push($building_bindvars, $industrial_unit_electrical_amps_row[1]);
        }    

        if (isset($industrial_unit_shipping_doors_row[0]) && ($industrial_unit_shipping_doors_row[0] != '-1' && $industrial_unit_shipping_doors_row[0] != '') && ($industrial_unit_shipping_doors_row[1] != '2147483647' && $industrial_unit_shipping_doors_row[1] != '')) {
            $buildings_query_string .= ' AND (allss.shipping_doors_truck BETWEEN ? and ?)';
            array_push($building_paramtypes, "s");
            array_push($building_bindvars, $industrial_unit_shipping_doors_row[0]);
            array_push($building_paramtypes, "s");
            array_push($building_bindvars, $industrial_unit_shipping_doors_row[1]);
        }    

        if (isset($industrial_unit_drivein_doors_row[0]) && ($industrial_unit_drivein_doors_row[0] != '-1' && $industrial_unit_drivein_doors_row[0] != '') && ($industrial_unit_drivein_doors_row[1] != '2147483647' && $industrial_unit_drivein_doors_row[1] != '')) {
            $buildings_query_string .= ' AND (allss.shipping_doors_drive_in BETWEEN ? and ?)';
            array_push($building_paramtypes, "s");
            array_push($building_bindvars, $industrial_unit_drivein_doors_row[0]);
            array_push($building_paramtypes, "s");
            array_push($building_bindvars, $industrial_unit_drivein_doors_row[1]);
        }    

        if (isset($industrial_unit_parking_stalls_row[0]) && ($industrial_unit_parking_stalls_row[0] != '-1' && $industrial_unit_parking_stalls_row[0] != '') && ($industrial_unit_parking_stalls_row[1] != '2147483647' && $industrial_unit_parking_stalls_row[1] != '')) {
            $buildings_query_string .= ' AND (allss.surface_stalls BETWEEN ? and ?)';
            array_push($building_paramtypes, "s");
            array_push($building_bindvars, $industrial_unit_parking_stalls_row[0]);
            array_push($building_paramtypes, "s");
            array_push($building_bindvars, $industrial_unit_parking_stalls_row[1]);
        }    

        if ($industrial_trucktrailer_parking_selected !='All' && $industrial_trucktrailer_parking_selected !='') {
            if ($industrial_trucktrailer_parking_selected =='Yes') {
            $buildings_query_string .= ' AND (allss.truck_trailer_parking > 0)';
            } else if ($industrial_trucktrailer_parking_selected =='No') {
                $buildings_query_string .= ' AND (allss.truck_trailer_parking < 1)';
            }
        }
        
        	// end industrial suite advanced search
        
    } // end if ($industrial_search > 0 && $advanced_search > 0) {       
            
        
    
    
    

        

        $other_language_id = 0;
        if ($lang_id == 1) {
            $other_language_id = 0;
        } else {
            $other_language_id = 1;
        }


        
        $buildings_query_string_suffix = "";
        
        
        if ($province_selected != "All" && $province_selected != "Tous") {
                //if there are no buildings with this province string then it is the case that someone has _just_clicked "Francais", 
                //in this case the province from the query string may be in English, find out that province's French or English name
                $province_language_query_array = array();  
                $province_other_language_query_string = "select building_code, province from buildings where province=? and lang=?";
                $province_other_language_query_statement = $db->prepare($province_other_language_query_string);
                $province_other_language_query_statement->bind_param('si', mysqli_real_escape_string($db, $province_selected), mysqli_real_escape_string($db, $other_language_id));
                if ($province_other_language_query_statement->execute()){
                    $result = $province_other_language_query_statement->get_result();
                    $row = $result->fetch_array(MYSQLI_ASSOC);
                    $province_language_query_array = $row;
                }
                $province_other_language_query_statement->close();
                //get the found mistmatched building and convert it to the current language.
                $mismatched_building = $province_language_query_array['building_code'];
                
                $matched_building_province_query_array = array();  
                $matched_building_province_query_string = "select province from buildings where lang=? and building_code=?";
                $matched_building_province_query_statement = $db->prepare($matched_building_province_query_string);
                $matched_building_province_query_statement->bind_param('is', mysqli_real_escape_string($db, $lang_id), mysqli_real_escape_string($db, $mismatched_building));
                if ($matched_building_province_query_statement->execute()){
                    $result = $matched_building_province_query_statement->get_result();
                    $row = $result->fetch_array(MYSQLI_ASSOC);
                    $matched_building_province_query_array = $row;
                }
                $matched_building_province_query_statement->close();

                $other_province_selected = $matched_building_province_query_array['province'];

                $buildings_query_string_suffix .= " and ((allb.lang = ? AND allb.province=?) or (allb.lang=? AND allb.province=?)) ";
                array_push($buildingSuffix_paramtypes, "s");
                array_push($buildingSuffix_bindvars, $lang_id);
                array_push($buildingSuffix_paramtypes, "s");
                array_push($buildingSuffix_bindvars, $province_selected);
                array_push($buildingSuffix_paramtypes, "s");
                array_push($buildingSuffix_bindvars, $lang_id);
                array_push($buildingSuffix_paramtypes, "s");
                array_push($buildingSuffix_bindvars, $other_province_selected);

        }

        if ($region_selected != "All" && $region_selected != "Tous") {
                //if there are no buildings with this region string then it is the case that someone has _just_clicked "Francais", 
                //in this case the region from the query string may be in English, find out that region's French or English name
                $region_language_query_array = array();  
                $region_other_language_query_string = "select building_code, region from buildings where region=? and lang=?";
                $region_other_language_query_statement = $db->prepare($region_other_language_query_string);
                $region_other_language_query_statement->bind_param('si', mysqli_real_escape_string($db, $region_selected), mysqli_real_escape_string($db, $other_language_id));
                if ($region_other_language_query_statement->execute()){
                    $result = $region_other_language_query_statement->get_result();
                    $row = $result->fetch_array(MYSQLI_ASSOC);
                    $region_language_query_array = $row;
                }
                $region_other_language_query_statement->close();
                //get the found mistmatched building and convert it to the current language.
                $mismatched_building = $region_language_query_array['building_code'];
                
                
                $matched_building_region_query_array = array();  
                $matched_building_region_query_string = "select region from buildings where lang=? and building_code=?";
                $matched_building_region_query_statement = $db->prepare($matched_building_region_query_string);
                $matched_building_region_query_statement->bind_param('is', mysqli_real_escape_string($db, $lang_id), mysqli_real_escape_string($db, $mismatched_building));
                if ($matched_building_region_query_statement->execute()){
                    $result = $matched_building_region_query_statement->get_result();
                    $row = $result->fetch_array(MYSQLI_ASSOC);
                    $matched_building_region_query_array = $row;
                }
                $matched_building_region_query_statement->close();

                $other_region_selected = $matched_building_region_query_array['region'];
                
                $buildings_query_string_suffix .= " and ((allb.lang =? AND allb.region=?)  or (allb.lang=? AND allb.region=?)) ";
                array_push($buildingSuffix_paramtypes, "s");
                array_push($buildingSuffix_bindvars, $lang_id);
                array_push($buildingSuffix_paramtypes, "s");
                array_push($buildingSuffix_bindvars, $region_selected);
                array_push($buildingSuffix_paramtypes, "s");
                array_push($buildingSuffix_bindvars, $other_language_id);
                array_push($buildingSuffix_paramtypes, "s");
                array_push($buildingSuffix_bindvars, $other_region_selected);
       }
       
       
       
       
       
        if ($sub_region_selected != "All" && $sub_region_selected != "Tous") {
                //if there are no buildings with this sub_region string then it is the case that someone has _just_clicked "Francais", 
                //in this case the sub_region from the query string may be in English, find out that sub_region's French or English name
                //get the found mistmatched building and convert it to the current language.
                $sub_region_language_query_array = array();  
                $sub_region_other_language_query_string = "select building_code, sub_region from buildings where sub_region=? and lang=?";
                $sub_region_other_language_query_statement = $db->prepare($sub_region_other_language_query_string);
                $sub_region_other_language_query_statement->bind_param('si', mysqli_real_escape_string($db, $sub_region_selected), mysqli_real_escape_string($db, $other_language_id));
                if ($sub_region_other_language_query_statement->execute()){
                    $result = $sub_region_other_language_query_statement->get_result();
                    $row = $result->fetch_array(MYSQLI_ASSOC);
                    $sub_region_language_query_array = $row;
                }
                $sub_region_other_language_query_statement->close();

                $mismatched_building = $sub_region_language_query_array['building_code'];
                
                $matched_building_sub_region_query = mysql_query("select sub_region from buildings where lang=" . $lang_id . " and building_code='" . $mismatched_building . "'");
                $matched_building_sub_region_query_array = array();  
                $matched_building_sub_region_query_string = "select sub_region from buildings where lang=? and building_code=?";
                $matched_building_sub_region_query_statement = $db->prepare($matched_building_sub_region_query_string);
                $matched_building_sub_region_query_statement->bind_param('is', mysqli_real_escape_string($db, $lang_id), mysqli_real_escape_string($db, $mismatched_building));
                if ($matched_building_sub_region_query_statement->execute()){
                    $result = $matched_building_sub_region_query_statement->get_result();
                    $row = $result->fetch_array(MYSQLI_ASSOC);
                    $matched_building_sub_region_query_array = $row;
                }
                $matched_building_sub_region_query_statement->close();

                $other_sub_region_selected = $matched_building_sub_region_query_array['sub_region'];            

                $buildings_query_string_suffix .= " and ((allb.lang =? AND allb.sub_region=?) or(allb.lang=? AND allb.sub_region=?)) ";
                array_push($buildingSuffix_paramtypes, "s");
                array_push($buildingSuffix_bindvars, $lang_id);
                array_push($buildingSuffix_paramtypes, "s");
                array_push($buildingSuffix_bindvars, $sub_region_selected);
                array_push($buildingSuffix_paramtypes, "s");
                array_push($buildingSuffix_bindvars, $other_language_id);
                array_push($buildingSuffix_paramtypes, "s");
                array_push($buildingSuffix_bindvars, $other_sub_region_selected);
        }




        $switchSuiteType = "";
        if ($suitetype_selected == 'OFFICE') {
            $switchSuiteType = "BUREAU";
        } else if ($suitetype_selected == 'BUREAU') {
            $switchSuiteType = "OFFICE";
        } else if ($suitetype_selected == 'INDUSTRIAL') {
            $switchSuiteType = "INDUSTRIEL";
        } else if ($suitetype_selected == 'INDUSTRIEL') {
            $switchSuiteType = "INDUSTRIAL";
        } else if ($suitetype_selected == 'RETAIL') {
            $switchSuiteType = "DÉTAIL";
        } else if ($suitetype_selected == 'DÉTAIL') {
            $switchSuiteType = "RETAIL";
        } 



        if ($building_name_selected != "All" && $building_name_selected != "Tous" && $building_name_selected != "" && $building_name_selected != "none") {
//            $buildings_query_string_suffix .= " and allb.building_name like '%" . mysql_real_escape_string($building_name_selected) . "%' ";
            $buildings_query_string_suffix .= " and allb.building_name like CONCAT('%',?,'%') ";
//            $buildings_query_string_suffix .= " and allb.building_name like ? ";
            
            array_push($buildingSuffix_paramtypes, "s");
            array_push($buildingSuffix_bindvars, $building_name_selected);
        }


    
        $buildings_query_string_order = " order by allb.country asc, allb.province asc, allb.city asc, allb.sort_streetname asc, allb.sort_streetnumber asc ";
        

    	if (($suitetype_selected != 'All' && $suitestuff_selected < 2 && $show_no_vacancy == 1) || $suitetype_selected == "RETAIL") { 
// If suitestuff is at 2 or higher there is more than just suite level element selected and we don't need this union.

       		$buildings_query_string = "SELECT * FROM (" . $buildings_query_string . " " . $buildings_query_string_suffix . ""; 
        
        
        
        	$buildings_query_string .= " UNION ALL
                                      
                                      SELECT DISTINCT 
                                        region,
                                        building_id,
                                        building_code,
                                        building_name,
                                        building_type,
                                        street_address,
                                        city,
                                        province,
                                        country,
                                        postal_code,
                                        sort_streetname,
                                        sort_streetnumber,
                                        add_rent_operating, 
                                        add_rent_realty, 
                                        add_rent_power, 
                                        add_rent_total,
                                        building_type AS ourtype
                                      FROM
                                        buildings allb
                                      WHERE lang =?";
                array_push($buildingUnion_param, "s");
                array_push($buildingUnion_bindvars, $lang_id);
                
                if (($buildingtype_selected) == "RETAIL" || $buildingtype_selected == "DÉTAIL") {

                } else {
                    $buildings_query_string .= " AND no_vacancies = 1 " ;
                }
                
                
                $buildings_query_string .= " AND (building_type = ? OR building_type = ?)" . $buildings_query_string_suffix . "
                                      ) a "; // this ') a' NEEDS TO BE HERE it's an alias for the entire union side of the query.
                
                array_push($buildingUnion_param, "s");
                array_push($buildingUnion_bindvars, $buildingtype_selected);
                array_push($buildingUnion_param, "s");
                array_push($buildingUnion_bindvars, $switchSuiteType);
                
                
                $buildings_query_string .= "group by building_code order by country asc, province asc, city asc, sort_streetname asc, sort_streetnumber asc ";
                
                $buildings_query_string .=" LIMIT ? OFFSET ?";
                
//                $buildings_query_string = str_replace("and allb.building_name like '%none%'", "", $buildings_query_string);
//                $buildings_query_string = str_replace("allb.building_name like CONCAT('%','none','%')", "", $buildings_query_string);
        
                // merge the parameters and suffixes for
                // $buildings_query_string and then $buildings_query_string_suffix 
                // and then the later UNION part also has another $buildings_query_string_suffix
                $paramsArray = array_merge($building_paramtypes, $buildingSuffix_paramtypes);
                $paramsArray = array_merge($paramsArray, $buildingUnion_param);
                $paramsArray = array_merge($paramsArray, $buildingSuffix_paramtypes);
                
                $bindsArray = array_merge($building_bindvars, $buildingSuffix_bindvars);
                $bindsArray = array_merge($bindsArray, $buildingUnion_bindvars);
                $bindsArray = array_merge($bindsArray, $buildingSuffix_bindvars);
                
                // build the array to send back
                $buildings_query_string_array = array();
                array_push($buildings_query_string_array, $buildings_query_string);
                array_push($buildings_query_string_array, $paramsArray);
                array_push($buildings_query_string_array, $bindsArray);
                
                
                
		} else {
//                    $buildings_query_string_order .=" LIMIT ? OFFSET ?";
//                    $buildings_query_string = str_replace("and allb.building_name like '%none%'", "", $buildings_query_string);
//                    $buildings_query_string = str_replace("allb.building_name like CONCAT('%','none','%')", "", $buildings_query_string);
                    
                    $buildings_query_string .= $buildings_query_string_suffix . $buildings_query_string_order . " LIMIT ? OFFSET ?";
                    
                    
                        
                    
                    $paramsArray = array_merge($building_paramtypes, $buildingSuffix_paramtypes);
                    $bindsArray = array_merge($building_bindvars, $buildingSuffix_bindvars);
                        
                    // build the array to send back
                    $buildings_query_string_array = array();
                    array_push($buildings_query_string_array, $buildings_query_string);
                    array_push($buildings_query_string_array, $paramsArray);
                    array_push($buildings_query_string_array, $bindsArray);
		}


        $buildingNarrow_param = array();
        $buildingNarrow_bindvars = array();
        if ($building_codes_narrow != '') {
            // We are using an IN() clause and need a question mark for each element
            // this loops through the $building_codes_narrow array and adds a ? for each element
            $narrowQs = implode(',', array_fill(0, count($building_codes_narrow), '?'));
            $buildings_query_string = "Select distinct building_id, building_code, building_name, street_address, city, province, country, postal_code, sort_streetname, sort_streetnumber, add_rent_operating, add_rent_realty, add_rent_power, add_rent_total FROM buildings allb where allb.building_code IN(".$narrowQs.") and allb.lang=? order by country asc, province asc, city asc, sort_streetname asc, sort_streetnumber asc LIMIT ? OFFSET ?";
            // loop through the $building_codes_narrow array again
            // add each building code individually to the array
            foreach($building_codes_narrow as $bcn) {
                array_push($buildingNarrow_param, "s");
                array_push($buildingNarrow_bindvars, $bcn);
            }
            array_push($buildingNarrow_param, "s");
            array_push($buildingNarrow_bindvars, $lang_id);

            // build the array to send back
            $buildings_query_string_array = array();
            array_push($buildings_query_string_array, $buildings_query_string);
            array_push($buildings_query_string_array, $buildingNarrow_param);
            array_push($buildings_query_string_array, $buildingNarrow_bindvars);
        }  
        return $buildings_query_string_array;
    }
    
    
    
    
    
    
    
    
    public function get_building_thumbnail_query_string($lang_id, $building_code) {
        $buildingthumb_param = array();
        $buildingthumb_bindvars = array();
        $query = "Select file_name from preview_images where building_code =? and thumbnail=1";
        array_push($buildingthumb_param, "s");
        array_push($buildingthumb_bindvars, $building_code);
        // build the array to send back
        $building_thumbnail_query_string_array = array();
        array_push($building_thumbnail_query_string_array, $query);
        array_push($building_thumbnail_query_string_array, $buildingthumb_param);
        array_push($building_thumbnail_query_string_array, $buildingthumb_bindvars);
        return $building_thumbnail_query_string_array;
    }
    
    

    

    /*
     * This method gets the regional query by reading the province post var that js is sending us from index.php onchange event.
     * Outputs the default option for the selected data and iterates the option values, checks is @this option value is selected.
     * Accesses either the abstract or its overriden method from the classes it extends.
     * <p>
     * @param   {Array} $language
     * @param 	{Integer} $lang_id
     * @param	{String} $region_type
     * @param	{String} $region_area,
     * @param	{String} $region_selected
     * @returns {String} $HTML
     * @see /main_abstract/Queries.php
     * @see index.php
     * */

    public function regional_query($language, $lang_id, $region_type, $region_area, $region_selected) {
        $HTML = "";
        if (isset($_POST[$region_type])) {

            $region_type_selected = $_POST[$region_type];


            $query = "SELECT DISTINCT b.building_code, " . $region_area . " FROM buildings b where " . $region_type . "='" . mysql_real_escape_string($region_type_selected) . "' and " . $region_area . " != '' and b.lang='" . $lang_id . "' group by " . $region_area;
			
			
            $building_region_query = mysql_query($query)or die("Regional query error: " . mysql_error());
            ;

            $subject = $region_area.'_text';

            $HTML .= '<option value="All">' . $language[$subject] . '</option>\r\n';

            while ($regions = mysql_fetch_array($building_region_query)) {

                $current_lang_options_array = mysql_query("select " . $region_area . " from buildings where building_code ='" . $regions['building_code'] . "' and lang=" . $lang_id) or die("Regional query, current languages options error: " . mysql_error());
                ;

                $current_lang_options_result = mysql_fetch_array($current_lang_options_array);

                if ($regions[$region_area] != "") {

                    if ($regions[$region_area] == $region_selected) {
                        $HTML .= '<option selected="yes" value="' . $regions[$region_area] . '">' . $current_lang_options_result[$region_area] . '</option>' . "\r\n";
                    } else {
                        $HTML .= '<option value="' . $regions[$region_area] . '">' . $current_lang_options_result[$region_area] . '</option>' . "\r\n";
                    }
                }
            }
        }
        return $HTML;
    }

    /*
     * This method gives the suites query string.
     * Accesses either the abstract or its overriden method from the classes it extends.
     * <p>
     * @param   {Integer} $lang_id
     * @param 	{Integer} $building_code
     * @param	{String} $size_selected
     * @param	{String} $availability_notbefore,
     * @param	{String} $availability_notafter
     * @returns {String} $suites_query_string
     * @see /main_abstract/Queries_abstract.php
     * @see region.php
     * @see sub-region.php
     * */

    public function get_suites_query_string($lang_id, $building_code, $size_selected, $sizeFrom, $sizeTo, $suitetype_selected, $notag, $availability_notbefore, $availability_notafter, $industrial_office_space_selected, $industrial_clear_height_selected, $industrial_shipping_doors_selected, $industrial_drivein_doors_selected, $industrial_electrical_volts_selected, $industrial_electrical_amps_selected, $industrial_parking_stalls_selected, $industrial_trucktrailer_parking_selected, $industrialsearchelement, $advanced_search, $db) {
        
        $suite_paramtypes = array();
        $suite_bindvars = array();
        

        if (isset($industrialsearchelement) && $industrialsearchelement != '') {
            
        } else {
            $industrialsearchelement = 0;
        }
        $suites_query_string = "select * from suites alls";
//        $suites_query_string = "select alls.suite_index, alls.lang, alls.building_code, alls.building_id, alls.floor_name, alls.floor_number, alls.suite_id, alls.suite_name, alls.suite_type, alls.net_rentable_area, alls.contiguous_area, alls.min_divisible_area, alls.taxes_and_operating, alls.availability, alls.notice_period, alls.description, alls.net_rent, alls.add_rent_total, alls.presentation_link, alls.presentation_name, alls.presentation_description, alls.pdf_plan, alls.spp_link, alls.model, alls.new, alls.leased, alls.promoted, alls.marketing_text, alls.record_date, alls.record_source, alls.dirty, alls.bu_flag from suites alls";

        
        
        if ($industrialsearchelement > 0 && $advanced_search > 0) {
            $suites_query_string .= ", suites_specifications allss";
        }
        
        $suites_query_string .= " where alls.lang=?";
        array_push($suite_paramtypes, 'i');
        array_push($suite_bindvars, $lang_id);

        
        if ($industrialsearchelement > 0 && $advanced_search > 0) {
            $suites_query_string .= " and allss.lang=? and alls.suite_id = allss.suite_id";
            array_push($suite_paramtypes, 'i');
            array_push($suite_bindvars, $lang_id);
        }
        
        
        $suites_query_string .= " and alls.building_code =?";
        array_push($suite_paramtypes, 's');
        array_push($suite_bindvars, $building_code);
        

        
        if ($size_selected == "Custom") {
            $size_row[0] = $sizeFrom;
            if ($sizeTo == '') {
                $size_row[1] = 2147483635;
            } else {
                $size_row[1] = $sizeTo;
            }
        } else if ($size_selected != "All" && $size_selected != "Tous") {
//            $size_result = mysql_query("SELECT minimum,maximum FROM sizes WHERE size ='" . $size_selected . "'")or die("min max query string error #9: " . mysql_error());
//            $size_row = mysql_fetch_array($size_result, MYSQL_NUM);
            
//            $size_row = array();
            $size_row = $this->minMaxQuery("sizes", $size_selected, $db);
            
        } else {
            $size_row[0] = -1;
            $size_row[1] = 2147483647;
        }
        
        
        if ($size_row[0] != '-1' && $size_row[1] != '2147483647') {
            $suites_query_string .= ' AND ((alls.net_rentable_area BETWEEN ? and ?) OR (alls.contiguous_area BETWEEN ? and ?) OR (alls.min_divisible_area BETWEEN ? and ?))';
            array_push($suite_paramtypes, 's');
            array_push($suite_bindvars, $size_row[0]);
            array_push($suite_paramtypes, 's');
            array_push($suite_bindvars, $size_row[1]);
            array_push($suite_paramtypes, 's');
            array_push($suite_bindvars, $size_row[0]);
            array_push($suite_paramtypes, 's');
            array_push($suite_bindvars, $size_row[1]);
            array_push($suite_paramtypes, 's');
            array_push($suite_bindvars, $size_row[0]);
            array_push($suite_paramtypes, 's');
            array_push($suite_bindvars, $size_row[1]);
        }
        

        $switchSuiteType = "";
        if ($suitetype_selected == 'OFFICE') {
            $switchSuiteType = "BUREAU";
        } else if ($suitetype_selected == 'BUREAU') {
            $switchSuiteType = "OFFICE";
        } else if ($suitetype_selected == 'INDUSTRIAL') {
            $switchSuiteType = "INDUSTRIEL";
        } else if ($suitetype_selected == 'INDUSTRIEL') {
            $switchSuiteType = "INDUSTRIAL";
        } else if ($suitetype_selected == 'RETAIL') {
            $switchSuiteType = "DÉTAIL";
        } else if ($suitetype_selected == 'DÉTAIL') {
            $switchSuiteType = "RETAIL";
        } 



        if ($suitetype_selected != 'All') {
            $suites_query_string .= " and (alls.suite_type = ?   OR   alls.suite_type = ?)";
            array_push($suite_paramtypes, 's');
            array_push($suite_bindvars, $suitetype_selected);
            array_push($suite_paramtypes, 's');
            array_push($suite_bindvars, $switchSuiteType);
        }


        if ($availability_notbefore != '0' && $availability_notafter != '2147483640') {
            $suites_query_string .= " and alls.availability > ? and alls.availability < ? ";
            array_push($suite_paramtypes, 'i');
            array_push($suite_bindvars, ''.$availability_notbefore.'');
            array_push($suite_paramtypes, 'i');
            array_push($suite_bindvars, ''.$availability_notafter.'');
        }


if ($industrialsearchelement > 0 && $advanced_search > 0) {

        
 $industrial_search = 0;
        if ($industrial_office_space_selected !='All' && $industrial_office_space_selected !='') {
//            $industrial_unit_office_space_result = mysql_query("SELECT minimum,maximum FROM office_space WHERE size ='" . $industrial_office_space_selected . "'")or die("industrial office space min max query string error #10: " . mysql_error());
//            $industrial_unit_office_space_row = mysql_fetch_array($industrial_unit_office_space_result, MYSQL_NUM);
            $industrial_unit_office_space_row = $this->minMaxQuery("office_space", $industrial_office_space_selected, $db);
            $industrial_search =1;
        }
        

        if (isset($industrial_clear_height_selected) && $industrial_clear_height_selected !='All' && $industrial_clear_height_selected !='') {
//            $industrial_clear_height_result = mysql_query("SELECT minimum,maximum FROM clear_height_ranges WHERE size ='" . $industrial_clear_height_selected . "'")or die("industrial clear_height min max query string error #11: " . mysql_error());
//            $industrial_clear_height_row = mysql_fetch_array($industrial_clear_height_result, MYSQL_NUM);
            $industrial_clear_height_row = $this->minMaxQuery("clear_height_ranges", $industrial_clear_height_selected, $db);
        }
        
        
        if ($industrial_electrical_volts_selected !='All' && $industrial_electrical_volts_selected !='') {
//            $industrial_unit_electrical_volts_result = mysql_query("SELECT minimum,maximum FROM electrical_volts WHERE size ='" . $industrial_electrical_volts_selected . "'")or die("$ndustrial electrical_volts min max query string error #12: " . mysql_error());
//            $industrial_unit_electrical_volts_row = mysql_fetch_array($industrial_unit_electrical_volts_result, MYSQL_NUM);
            $industrial_unit_electrical_volts_row = $this->minMaxQuery("electrical_volts", $industrial_electrical_volts_selected, $db);
            $industrial_search=3;
        }
        
        if ($industrial_electrical_amps_selected !='All' && $industrial_electrical_amps_selected !='') {
//            $industrial_unit_electrical_amps_result = mysql_query("SELECT minimum,maximum FROM electrical_amps WHERE size ='" . $industrial_electrical_amps_selected . "'")or die("industrial electrical_amps min max query string error #13: " . mysql_error());
//            $industrial_unit_electrical_amps_row = mysql_fetch_array($industrial_unit_electrical_amps_result, MYSQL_NUM);
            $industrial_unit_electrical_amps_row = $this->minMaxQuery("electrical_amps", $industrial_electrical_amps_selected, $db);
            $industrial_search=4;
        }
        
        if ($industrial_shipping_doors_selected !='All' && $industrial_shipping_doors_selected !='') {
//        $industrial_unit_shipping_doors_result = mysql_query("SELECT minimum,maximum FROM num_shipping_doors WHERE size ='" . $industrial_shipping_doors_selected . "'")or die("industrial shipping_doors min max query string error #14: " . mysql_error());
//        $industrial_unit_shipping_doors_row = mysql_fetch_array($industrial_unit_shipping_doors_result, MYSQL_NUM);
            $industrial_unit_shipping_doors_row = $this->minMaxQuery("num_shipping_doors", $industrial_shipping_doors_selected, $db);
            $industrial_search=5;
        }
        
        if ($industrial_drivein_doors_selected !='All' && $industrial_drivein_doors_selected !='') {
//            $industrial_unit_drivein_doors_result = mysql_query("SELECT minimum,maximum FROM num_drivein_doors WHERE size ='" . $industrial_drivein_doors_selected . "'")or die("industrial drivein_doors min max query string error #15: " . mysql_error());
//            $industrial_unit_drivein_doors_row = mysql_fetch_array($industrial_unit_drivein_doors_result, MYSQL_NUM);   
            $industrial_unit_drivein_doors_row = $this->minMaxQuery("num_drivein_doors", $industrial_drivein_doors_selected, $db);
            $industrial_search=6;
        }
        

        
        if (isset($industrial_parking_stalls_selected) && $industrial_parking_stalls_selected !='All' && $industrial_parking_stalls_selected !='') {
//            $industrial_unit_parking_stalls_result = mysql_query("SELECT minimum,maximum FROM surface_stalls_all WHERE size ='" . $industrial_parking_stalls_selected . "'")or die("industrial surface_stalls min max query string error #16: " . mysql_error());
//            $industrial_unit_parking_stalls_row = mysql_fetch_array($industrial_unit_parking_stalls_result, MYSQL_NUM); 
            $industrial_unit_parking_stalls_row = $this->minMaxQuery("surface_stalls_all", $industrial_parking_stalls_selected, $db);
            $industrial_search=7;
        }
        
        
        if ($industrial_trucktrailer_parking_selected !='All' && $industrial_trucktrailer_parking_selected !='') {
            $industrial_search=8;
        }
        
        
        if ($size_selected == "Custom") {
            $size_row[0] = $sizeFrom;
            if ($sizeTo == '') {
                $size_row[1] = 2147483635;
            } else {
                $size_row[1] = $sizeTo;
            }
        } else if ($size_selected != "All" && $size_selected != "Tous") {
//            $size_result = mysql_query("SELECT minimum,maximum FROM sizes WHERE size ='" . $size_selected . "'")or die("min max query string error #17: " . mysql_error());
//            $size_row = mysql_fetch_array($size_result, MYSQL_NUM);
            $size_row = $this->minMaxQuery("sizes", $size_selected, $db);
        } else {
            $size_row[0] = -1;
            $size_row[1] = 2147483647;
        }
        
        
        
        // industrial suite advanced search elements
        
 



        if (($industrial_unit_office_space_row[0] != '-1' && $industrial_unit_office_space_row[0] != '') && ($industrial_unit_office_space_row[1] != '100' && $industrial_unit_office_space_row[1] != '')) {
            
            // ONLY active if SIZE is selected in main search
            if ($size_row[0] != '-1' && $size_row[1] != '2147483647') {

                if ($size_row[0] == 0){
                    $min_total_space = 1;
                    $max_total_space = $size_row[1];
                } else {
                    $min_total_space = $size_row[0];
                    $max_total_space = $size_row[1];
                }

                $minpercentage = $industrial_unit_office_space_row[0] / 100;
                $maxpercentage = $industrial_unit_office_space_row[1] / 100;
                
                $min_office_space = $min_total_space * $maxpercentage;
                $max_office_space = $max_total_space * $maxpercentage;


                $suites_query_string .= ' AND (allss.office_space BETWEEN ? AND ?)';
                array_push($suite_paramtypes, 's');
                array_push($suite_bindvars, $min_office_space);
                array_push($suite_paramtypes, 's');
                array_push($suite_bindvars, $max_office_space);
            }
        }

        
        if (isset($industrial_clear_height_row[0]) && ($industrial_clear_height_row[0] != '-1' && $industrial_clear_height_row[0] != '') && ($industrial_clear_height_row[1] != '201' && $industrial_clear_height_row[1] != '')) {
            $suites_query_string .= ' AND (allss.clear_height_sort BETWEEN ? and ?)';
            array_push($suite_paramtypes, 's');
            array_push($suite_bindvars, $industrial_clear_height_row[0]);
            array_push($suite_paramtypes, 's');
            array_push($suite_bindvars, $industrial_clear_height_row[1]);
        }  



        if (isset($industrial_unit_electrical_volts_row[0]) && ($industrial_unit_electrical_volts_row[0] != '-1' && $industrial_unit_electrical_volts_row[0] != '') && ($industrial_unit_electrical_volts_row[1] != '2147483647' && $industrial_unit_electrical_volts_row[1] != '')) {
            $suites_query_string .= ' AND (allss.available_electrical_volts BETWEEN ? and ?)';
            array_push($suite_paramtypes, 's');
            array_push($suite_bindvars, $industrial_unit_electrical_volts_row[0]);
            array_push($suite_paramtypes, 's');
            array_push($suite_bindvars, $industrial_unit_electrical_volts_row[1]);
        }    

        if (isset($industrial_unit_electrical_amps_row[0]) && ($industrial_unit_electrical_amps_row[0] != '-1' && $industrial_unit_electrical_amps_row[0] != '') && ($industrial_unit_electrical_amps_row[1] != '2147483647' && $industrial_unit_electrical_amps_row[1] != '')) {
            $suites_query_string .= ' AND (allss.available_electrical_amps BETWEEN ? and ?)';
            array_push($suite_paramtypes, 's');
            array_push($suite_bindvars, $industrial_unit_electrical_amps_row[0]);
            array_push($suite_paramtypes, 's');
            array_push($suite_bindvars, $industrial_unit_electrical_amps_row[1]);
        }    

        if (isset($industrial_unit_shipping_doors_row[0]) && ($industrial_unit_shipping_doors_row[0] != '-1' && $industrial_unit_shipping_doors_row[0] != '') && ($industrial_unit_shipping_doors_row[1] != '2147483647' && $industrial_unit_shipping_doors_row[1] != '')) {
            $suites_query_string .= ' AND (allss.shipping_doors_truck BETWEEN ? and ?)';
            array_push($suite_paramtypes, 's');
            array_push($suite_bindvars, $industrial_unit_shipping_doors_row[0]);
            array_push($suite_paramtypes, 's');
            array_push($suite_bindvars, $industrial_unit_shipping_doors_row[1]);
        }    

        if (isset($industrial_unit_drivein_doors_row[0]) && ($industrial_unit_drivein_doors_row[0] != '-1' && $industrial_unit_drivein_doors_row[0] != '') && ($industrial_unit_drivein_doors_row[1] != '2147483647' && $industrial_unit_drivein_doors_row[1] != '')) {
            $suites_query_string .= ' AND (allss.shipping_doors_drive_in BETWEEN ? and ?)';
            array_push($suite_paramtypes, 's');
            array_push($suite_bindvars, $industrial_unit_drivein_doors_row[0]);
            array_push($suite_paramtypes, 's');
            array_push($suite_bindvars, $industrial_unit_drivein_doors_row[1]);
        }    

        if (isset($industrial_unit_parking_stalls_row[0]) && ($industrial_unit_parking_stalls_row[0] != '-1' && $industrial_unit_parking_stalls_row[0] != '') && ($industrial_unit_parking_stalls_row[1] != '2147483647' && $industrial_unit_parking_stalls_row[1] != '')) {
            $suites_query_string .= ' AND (allss.surface_stalls BETWEEN ? and ?)';
            array_push($suite_paramtypes, 's');
            array_push($suite_bindvars, $industrial_unit_parking_stalls_row[0]);
            array_push($suite_paramtypes, 's');
            array_push($suite_bindvars, $industrial_unit_parking_stalls_row[1]);
        }    

        if ($industrial_trucktrailer_parking_selected !='All' && $industrial_trucktrailer_parking_selected !='') {
            if ($industrial_trucktrailer_parking_selected =='Yes') {
                $suites_query_string .= ' AND (allss.truck_trailer_parking > 0)';
            } else if ($industrial_trucktrailer_parking_selected =='No') {
                $suites_query_string .= ' AND (allss.truck_trailer_parking < 1)';
            }
        } 
        
// END industrial unit advanced search elements

} //  ENDs  if ($industrialsearchelement > 0 && $advanced_search > 0) {
        

        $suites_query_string .= " order by alls.floor_number desc, alls.suite_name asc";
        
        // build array for use with call_user_func_array 
        // for binding values to prepared statement
        $suite_output = array();
        array_push($suite_output, $suites_query_string);
        array_push($suite_output, $suite_paramtypes);
        array_push($suite_output, $suite_bindvars);
        

        
        
        
        
        return $suite_output;

    }
    

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    /*
     * This method Is the AR4 Index Page Search Query String
     * Accesses either the abstract or its overriden method from the classes it extends.
     * <p>
     * @param   {Integer} $lang_id
     * @param 	{Integer} $building_code

     * @see /main_abstract/Queries_abstract.php
     * @see region.php
     * @see sub-region.php
     * */

    public function get_suites_query_string_ar4_index($lang_id, $building_code, $db) {
        
        $suite_paramtypes = array();
        $suite_bindvars = array();

        $suites_query_string = "select * from suites alls";
//        $suites_query_string = "select alls.suite_index, alls.lang, alls.building_code, alls.building_id, alls.floor_name, alls.floor_number, alls.suite_id, alls.suite_name, alls.suite_type, alls.net_rentable_area, alls.contiguous_area, alls.min_divisible_area, alls.taxes_and_operating, alls.availability, alls.notice_period, alls.description, alls.net_rent, alls.add_rent_total, alls.presentation_link, alls.presentation_name, alls.presentation_description, alls.pdf_plan, alls.spp_link, alls.model, alls.new, alls.leased, alls.promoted, alls.marketing_text, alls.record_date, alls.record_source, alls.dirty, alls.bu_flag from suites alls";

        $suites_query_string .= ", suites_specifications allss";
        
        $suites_query_string .= " where alls.lang=?";
        array_push($suite_paramtypes, 'i');
        array_push($suite_bindvars, $lang_id);

        $suites_query_string .= " and allss.lang=? and alls.suite_id = allss.suite_id";
        array_push($suite_paramtypes, 'i');
        array_push($suite_bindvars, $lang_id);        
        
        $suites_query_string .= " and alls.building_code =?";
        array_push($suite_paramtypes, 's');
        array_push($suite_bindvars, $building_code);
                

        $suites_query_string .= " order by alls.floor_number desc, alls.suite_name asc";
        
        // build array for use with call_user_func_array 
        // for binding values to prepared statement
        $suite_output = array();
        array_push($suite_output, $suites_query_string);
        array_push($suite_output, $suite_paramtypes);
        array_push($suite_output, $suite_bindvars);
 
        
        return $suite_output;

    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    


    /*
     * This method gives the suites query string.
     * Accesses either the abstract or its overriden method from the classes it extends.
     * <p>
     * @param   {Integer} $lang_id
     * @param 	{Integer} $building_code
     * @param	{String} $size_selected
     * @param	{String} $availability_notbefore,
     * @param	{String} $availability_notafter
     * @returns {String} $suites_query_string
     * @see /main_abstract/Queries_abstract.php
     * @see region.php
     * @see sub-region.php
     * */

    public function get_suites_count_noleased($lang_id, $building_code, $size_selected, $sizeFrom, $sizeTo, $suitetype_selected, $notag, $availability_notbefore, $availability_notafter, $industrial_office_space_selected, $industrial_clear_height_selected, $industrial_shipping_doors_selected, $industrial_drivein_doors_selected, $industrial_electrical_volts_selected, $industrial_electrical_amps_selected, $industrial_parking_stalls_selected, $industrial_trucktrailer_parking_selected, $industrialsearchelement, $advanced_search, $db) {
        $suite_paramtypes = array();
        $suite_bindvars = array();

        
        if (isset($industrialsearchelement) && $industrialsearchelement != '') {
            
        } else {
            $industrialsearchelement = 0;
        }
        
        $suites_query_string = "select count(*) as realsuitecount from suites alls";

        
        
        if ($industrialsearchelement > 0 && $advanced_search > 0) {
            $suites_query_string .= ", suites_specifications allss";
        }
        
        $suites_query_string .= " where alls.lang=?";
        array_push($suite_paramtypes, 'i');
        array_push($suite_bindvars, $lang_id);

        
        if ($industrialsearchelement > 0 && $advanced_search > 0) {
            $suites_query_string .= " and allss.lang=? and alls.suite_id = allss.suite_id";
            array_push($suite_paramtypes, 'i');
            array_push($suite_bindvars, $lang_id);
        }
        
        
        $suites_query_string .= " and alls.building_code =?";
        array_push($suite_paramtypes, 's');
        array_push($suite_bindvars, $building_code);
        

        
        if ($size_selected == "Custom") {
            $size_row[0] = $sizeFrom;
            if ($sizeTo == '') {
                $size_row[1] = 2147483635;
            } else {
                $size_row[1] = $sizeTo;
            }
        } else if ($size_selected != "All" && $size_selected != "Tous") {
//            $size_result = mysql_query("SELECT minimum,maximum FROM sizes WHERE size ='" . $size_selected . "'")or die("min max query string error #9: " . mysql_error());
//            $size_row = mysql_fetch_array($size_result, MYSQL_NUM);
            
//            $size_row = array();
            $size_row = $this->minMaxQuery("sizes", $size_selected, $db);
            
        } else {
            $size_row[0] = -1;
            $size_row[1] = 2147483647;
        }
        
        
        if ($size_row[0] != '-1' && $size_row[1] != '2147483647') {
            $suites_query_string .= ' AND ((alls.net_rentable_area BETWEEN ? and ?) OR (alls.contiguous_area BETWEEN ? and ?) OR (alls.min_divisible_area BETWEEN ? and ?))';
            array_push($suite_paramtypes, 's');
            array_push($suite_bindvars, $size_row[0]);
            array_push($suite_paramtypes, 's');
            array_push($suite_bindvars, $size_row[1]);
            array_push($suite_paramtypes, 's');
            array_push($suite_bindvars, $size_row[0]);
            array_push($suite_paramtypes, 's');
            array_push($suite_bindvars, $size_row[1]);
            array_push($suite_paramtypes, 's');
            array_push($suite_bindvars, $size_row[0]);
            array_push($suite_paramtypes, 's');
            array_push($suite_bindvars, $size_row[1]);
        }
        

        $switchSuiteType = "";
        if ($suitetype_selected == 'OFFICE') {
            $switchSuiteType = "BUREAU";
        } else if ($suitetype_selected == 'BUREAU') {
            $switchSuiteType = "OFFICE";
        } else if ($suitetype_selected == 'INDUSTRIAL') {
            $switchSuiteType = "INDUSTRIEL";
        } else if ($suitetype_selected == 'INDUSTRIEL') {
            $switchSuiteType = "INDUSTRIAL";
        } else if ($suitetype_selected == 'RETAIL') {
            $switchSuiteType = "DÉTAIL";
        } else if ($suitetype_selected == 'DÉTAIL') {
            $switchSuiteType = "RETAIL";
        } 



        if ($suitetype_selected != 'All') {
            $suites_query_string .= " and (alls.suite_type = ?   OR   alls.suite_type = ?)";
            array_push($suite_paramtypes, 's');
            array_push($suite_bindvars, $suitetype_selected);
            array_push($suite_paramtypes, 's');
            array_push($suite_bindvars, $switchSuiteType);
        }


        if ($availability_notbefore != '0' && $availability_notafter != '2147483640') {
            $suites_query_string .= " and alls.availability > ? and alls.availability < ? ";
            array_push($suite_paramtypes, 'i');
            array_push($suite_bindvars, ''.$availability_notbefore.'');
            array_push($suite_paramtypes, 'i');
            array_push($suite_bindvars, ''.$availability_notafter.'');
        }


if ($industrialsearchelement > 0 && $advanced_search > 0) {

        
 $industrial_search = 0;
        if ($industrial_office_space_selected !='All' && $industrial_office_space_selected !='') {
                // industrial unit office space query
//            $industrial_unit_office_space_result = mysql_query("SELECT minimum,maximum FROM office_space WHERE size ='" . $industrial_office_space_selected . "'")or die("industrial office space min max query string error #19: " . mysql_error());
//            $industrial_unit_office_space_row = mysql_fetch_array($industrial_unit_office_space_result, MYSQL_NUM);
            $industrial_unit_office_space_row = $this->minMaxQuery("office_space", $industrial_office_space_selected, $db);
            $industrial_search =1;
        }

        if (isset($industrial_clear_height_selected) && $industrial_clear_height_selected !='All' && $industrial_clear_height_selected !='') {
//            $industrial_clear_height_result = mysql_query("SELECT minimum,maximum FROM clear_height_ranges WHERE size ='" . $industrial_clear_height_selected . "'")or die("industrial clear_height min max query string error: #20 " . mysql_error());
//            $industrial_clear_height_row = mysql_fetch_array($industrial_clear_height_result, MYSQL_NUM);
            $industrial_clear_height_row = $this->minMaxQuery("clear_height_ranges", $industrial_clear_height_selected, $db);
        }
        
        
        if ($industrial_electrical_volts_selected !='All' && $industrial_electrical_volts_selected !='') {
//            $industrial_unit_electrical_volts_result = mysql_query("SELECT minimum,maximum FROM electrical_volts WHERE size ='" . $industrial_electrical_volts_selected . "'")or die("$ndustrial electrical_volts min max query string error #21: " . mysql_error());
//            $industrial_unit_electrical_volts_row = mysql_fetch_array($industrial_unit_electrical_volts_result, MYSQL_NUM);
            $industrial_unit_electrical_volts_row = $this->minMaxQuery("electrical_volts", $industrial_electrical_volts_selected, $db);
            $industrial_search=3;
        }
        
        if ($industrial_electrical_amps_selected !='All' && $industrial_electrical_amps_selected !='') {
//            $industrial_unit_electrical_amps_result = mysql_query("SELECT minimum,maximum FROM electrical_amps WHERE size ='" . $industrial_electrical_amps_selected . "'")or die("industrial electrical_amps min max query string error #22: " . mysql_error());
//            $industrial_unit_electrical_amps_row = mysql_fetch_array($industrial_unit_electrical_amps_result, MYSQL_NUM);
            $industrial_unit_electrical_amps_row = $this->minMaxQuery("electrical_amps", $industrial_electrical_amps_selected, $db);
            $industrial_search=4;
        }
        
        if ($industrial_shipping_doors_selected !='All' && $industrial_shipping_doors_selected !='') {
//            $industrial_unit_shipping_doors_result = mysql_query("SELECT minimum,maximum FROM num_shipping_doors WHERE size ='" . $industrial_shipping_doors_selected . "'")or die("industrial shipping_doors min max query string error #23: " . mysql_error());
//            $industrial_unit_shipping_doors_row = mysql_fetch_array($industrial_unit_shipping_doors_result, MYSQL_NUM);
            $industrial_unit_shipping_doors_row = $this->minMaxQuery("num_shipping_doors", $industrial_shipping_doors_selected, $db);
            $industrial_search=5;
        }
        
        if ($industrial_drivein_doors_selected !='All' && $industrial_drivein_doors_selected !='') {
//            $industrial_unit_drivein_doors_result = mysql_query("SELECT minimum,maximum FROM num_drivein_doors WHERE size ='" . $industrial_drivein_doors_selected . "'")or die("industrial drivein_doors min max query string error #24: " . mysql_error());
//            $industrial_unit_drivein_doors_row = mysql_fetch_array($industrial_unit_drivein_doors_result, MYSQL_NUM);
            $industrial_unit_drivein_doors_row = $this->minMaxQuery("num_drivein_doors", $industrial_drivein_doors_selected, $db);
            $industrial_search=6;
        }

        if (isset($industrial_parking_stalls_selected) && $industrial_parking_stalls_selected !='All' && $industrial_parking_stalls_selected !='') {
//            $industrial_unit_parking_stalls_result = mysql_query("SELECT minimum,maximum FROM surface_stalls_all WHERE size ='" . $industrial_parking_stalls_selected . "'")or die("industrial surface_stalls min max query string error #25: " . mysql_error());
//            $industrial_unit_parking_stalls_row = mysql_fetch_array($industrial_unit_parking_stalls_result, MYSQL_NUM);
            $industrial_unit_parking_stalls_row = $this->minMaxQuery("surface_stalls_all", $industrial_parking_stalls_selected, $db);
            $industrial_search=7;
        }
        
        
        
        
        if ($industrial_trucktrailer_parking_selected !='All' && $industrial_trucktrailer_parking_selected !='') {
             
            $industrial_search=8;
        }
        
        
        if ($size_selected == "Custom") {
            $size_row[0] = $sizeFrom;
            if ($sizeTo == '') {
                $size_row[1] = 2147483635;
            } else {
                $size_row[1] = $sizeTo;
            }
        } else if ($size_selected != "All" && $size_selected != "Tous") {
//            $size_result = mysql_query("SELECT minimum,maximum FROM sizes WHERE size ='" . $size_selected . "'")or die("min max query string error #17: " . mysql_error());
//            $size_row = mysql_fetch_array($size_result, MYSQL_NUM);
            $size_row = $this->minMaxQuery("sizes", $size_selected, $db);
        } else {
            $size_row[0] = -1;
            $size_row[1] = 2147483647;
        }
        
        
        
        // industrial suite advanced search elements
        
 



        if (($industrial_unit_office_space_row[0] != '-1' && $industrial_unit_office_space_row[0] != '') && ($industrial_unit_office_space_row[1] != '100' && $industrial_unit_office_space_row[1] != '')) {
            
            // ONLY active if SIZE is selected in main search
            if ($size_row[0] != '-1' && $size_row[1] != '2147483647') {

                if ($size_row[0] == 0){
                    $min_total_space = 1;
                    $max_total_space = $size_row[1];
                } else {
                    $min_total_space = $size_row[0];
                    $max_total_space = $size_row[1];
                }

                $minpercentage = $industrial_unit_office_space_row[0] / 100;
                $maxpercentage = $industrial_unit_office_space_row[1] / 100;
                
                $min_office_space = $min_total_space * $maxpercentage;
                $max_office_space = $max_total_space * $maxpercentage;


                $suites_query_string .= ' AND (allss.office_space BETWEEN ? AND ?)';
                array_push($suite_paramtypes, 's');
                array_push($suite_bindvars, $min_office_space);
                array_push($suite_paramtypes, 's');
                array_push($suite_bindvars, $max_office_space);
            }
        }

        
        if (isset($industrial_clear_height_row[0]) && ($industrial_clear_height_row[0] != '-1' && $industrial_clear_height_row[0] != '') && ($industrial_clear_height_row[1] != '201' && $industrial_clear_height_row[1] != '')) {
            $suites_query_string .= ' AND (allss.clear_height_sort BETWEEN ? and ?)';
            array_push($suite_paramtypes, 's');
            array_push($suite_bindvars, $industrial_clear_height_row[0]);
            array_push($suite_paramtypes, 's');
            array_push($suite_bindvars, $industrial_clear_height_row[1]);
        }  



        if (isset($industrial_unit_electrical_volts_row[0]) && ($industrial_unit_electrical_volts_row[0] != '-1' && $industrial_unit_electrical_volts_row[0] != '') && ($industrial_unit_electrical_volts_row[1] != '2147483647' && $industrial_unit_electrical_volts_row[1] != '')) {
            $suites_query_string .= ' AND (allss.available_electrical_volts BETWEEN ? and ?)';
            array_push($suite_paramtypes, 's');
            array_push($suite_bindvars, $industrial_unit_electrical_volts_row[0]);
            array_push($suite_paramtypes, 's');
            array_push($suite_bindvars, $industrial_unit_electrical_volts_row[1]);
        }    

        if (isset($industrial_unit_electrical_amps_row[0]) && ($industrial_unit_electrical_amps_row[0] != '-1' && $industrial_unit_electrical_amps_row[0] != '') && ($industrial_unit_electrical_amps_row[1] != '2147483647' && $industrial_unit_electrical_amps_row[1] != '')) {
            $suites_query_string .= ' AND (allss.available_electrical_amps BETWEEN ? and ?)';
            array_push($suite_paramtypes, 's');
            array_push($suite_bindvars, $industrial_unit_electrical_amps_row[0]);
            array_push($suite_paramtypes, 's');
            array_push($suite_bindvars, $industrial_unit_electrical_amps_row[1]);
        }    

        if (isset($industrial_unit_shipping_doors_row[0]) && ($industrial_unit_shipping_doors_row[0] != '-1' && $industrial_unit_shipping_doors_row[0] != '') && ($industrial_unit_shipping_doors_row[1] != '2147483647' && $industrial_unit_shipping_doors_row[1] != '')) {
            $suites_query_string .= ' AND (allss.shipping_doors_truck BETWEEN ? and ?)';
            array_push($suite_paramtypes, 's');
            array_push($suite_bindvars, $industrial_unit_shipping_doors_row[0]);
            array_push($suite_paramtypes, 's');
            array_push($suite_bindvars, $industrial_unit_shipping_doors_row[1]);
        }    

        if (isset($industrial_unit_drivein_doors_row[0]) && ($industrial_unit_drivein_doors_row[0] != '-1' && $industrial_unit_drivein_doors_row[0] != '') && ($industrial_unit_drivein_doors_row[1] != '2147483647' && $industrial_unit_drivein_doors_row[1] != '')) {
            $suites_query_string .= ' AND (allss.shipping_doors_drive_in BETWEEN ? and ?)';
            array_push($suite_paramtypes, 's');
            array_push($suite_bindvars, $industrial_unit_drivein_doors_row[0]);
            array_push($suite_paramtypes, 's');
            array_push($suite_bindvars, $industrial_unit_drivein_doors_row[1]);
        }    

        if (isset($industrial_unit_parking_stalls_row[0]) && ($industrial_unit_parking_stalls_row[0] != '-1' && $industrial_unit_parking_stalls_row[0] != '') && ($industrial_unit_parking_stalls_row[1] != '2147483647' && $industrial_unit_parking_stalls_row[1] != '')) {
            $suites_query_string .= ' AND (allss.surface_stalls BETWEEN ? and ?)';
            array_push($suite_paramtypes, 's');
            array_push($suite_bindvars, $industrial_unit_parking_stalls_row[0]);
            array_push($suite_paramtypes, 's');
            array_push($suite_bindvars, $industrial_unit_parking_stalls_row[1]);
        }    

        if ($industrial_trucktrailer_parking_selected !='All' && $industrial_trucktrailer_parking_selected !='') {
            if ($industrial_trucktrailer_parking_selected =='Yes') {
                $suites_query_string .= ' AND (allss.truck_trailer_parking > 0)';
            } else if ($industrial_trucktrailer_parking_selected =='No') {
                $suites_query_string .= ' AND (allss.truck_trailer_parking < 1)';
            }
        } 
        
// END industrial unit advanced search elements

} //  ENDs  if ($industrialsearchelement > 0 && $advanced_search > 0) {
        
//        $suites_query_string .= " AND leased='false'";
        $suites_query_string .= " order by alls.floor_number desc, alls.suite_name asc";
        // build array for use with call_user_func_array 
        // for binding values to prepared statement
        $suite_output = array();
        array_push($suite_output, $suites_query_string);
        array_push($suite_output, $suite_paramtypes);
        array_push($suite_output, $suite_bindvars);


        return $suite_output;
    }










    /*
     * This method COUNTS the suites query string, excluding leased properties
     * Accesses either the abstract or its overriden method from the classes it extends.
     * <p>
     * @param   {Integer} $lang_id
     * @param 	{Integer} $building_code
     * @param	{String} $size_selected
     * @param	{String} $availability_notbefore,
     * @param	{String} $availability_notafter
     * @returns {String} $suites_query_string
     * @see /main_abstract/Queries_abstract.php
     * @see region.php
     * @see sub-region.php
     * */

    public function get_suites_count_noleased_june19($lang_id, $building_code, $size_selected, $suitetype_selected, $notag, $availability_notbefore, $availability_notafter, $industrial_office_space_selected, $industrial_warehouse_space_selected, $industrial_electrical_volts_selected, $industrial_electrical_amps_selected, $industrial_shipping_doors_selected, $industrial_drivein_doors_selected, $industrial_parking_selected, $industrial_trucktrailer_parking_selected, $industrialsearchelement, $advanced_search) {
        
        
        
        if (isset($industrialsearchelement) && $industrialsearchelement != '') {
            
        } else {
            $industrialsearchelement = 0;
        }
        
        
        
        
        $suites_query_string = "select count(*) as realsuitecountxxx from suites alls";

        
        
        if ($industrialsearchelement > 0 && $advanced_search > 0) {
            $suites_query_string .= ", suites_specifications allss";
        }
        
        $suites_query_string .= " where alls.lang=" . $lang_id . "";
        
        if ($industrialsearchelement > 0 && $advanced_search > 0) {
            $suites_query_string .= " and allss.lang=" . $lang_id . " and alls.suite_id = allss.suite_id";
        }
        
        
        $suites_query_string .= " and alls.building_code ='" . $building_code . "'";
        
        
        
        switch ($size_selected) {
            case '5,000 and Under':
                $suites_query_string .= " and ((alls.contiguous_area between 0 and 5001) or (alls.net_rentable_area between 0 and 5001)) ";
                break;
            case '5,000 - 10,000':
                $suites_query_string .= " and ((alls.contiguous_area between 4999 and 10001) or (alls.net_rentable_area between 4999 and 10001)) ";
                break;
            case '10,000 - 20,000':
                $suites_query_string .= " and ((alls.contiguous_area between 9999 and 20001) or (alls.net_rentable_area between 9999 and 20001)) ";
                break;
            case '20,000 - 40,000':
                $suites_query_string .= " and ((alls.contiguous_area between 19999 and 40001) or (alls.net_rentable_area between 19999 and 40001)) ";
                break;
            case '40,000 and Above':
                $suites_query_string .= " and (alls.contiguous_area > 39999 or alls.net_rentable_area > 39999) ";
                break;
        }


        $switchSuiteType = "";
        if ($suitetype_selected == 'OFFICE') {
            $switchSuiteType = "BUREAU";
        } else if ($suitetype_selected == 'BUREAU') {
            $switchSuiteType = "OFFICE";
        } else if ($suitetype_selected == 'INDUSTRIAL') {
            $switchSuiteType = "INDUSTRIEL";
        } else if ($suitetype_selected == 'INDUSTRIEL') {
            $switchSuiteType = "INDUSTRIAL";
        } else if ($suitetype_selected == 'RETAIL') {
            $switchSuiteType = "DÉTAIL";
        } else if ($suitetype_selected == 'DÉTAIL') {
            $switchSuiteType = "RETAIL";
        } 






        if ($suitetype_selected != 'All') {
            $suites_query_string .= " and (alls.suite_type = '" . $suitetype_selected . "'   OR   alls.suite_type = '" . $switchSuiteType . "')";
        }


        if ($availability_notbefore != '0' && $availability_notafter != '2147483640') {
            $suites_query_string .= " and alls.availability > " . $availability_notbefore . " and alls.availability < " . $availability_notafter;
        }


if ($industrialsearchelement > 0 && $advanced_search > 0) {

// industrial unit advanced search elements


        
 $industrial_search = 0;
        if ($industrial_office_space_selected !='All' && $industrial_office_space_selected !='') {
                // industrial unit office space query
//            $industrial_unit_office_space_result = mysql_query("SELECT minimum,maximum FROM office_space WHERE size ='" . $industrial_office_space_selected . "'")or die("industrial office space min max query string error #26: " . mysql_error());
//            $industrial_unit_office_space_row = mysql_fetch_array($industrial_unit_office_space_result, MYSQL_NUM);
            $industrial_unit_office_space_row = $this->minMaxQuery("office_space", $industrial_office_space_selected, $db);
            
            $industrial_search =1;
        }
        

        
        if ($industrial_electrical_volts_selected !='All' && $industrial_electrical_volts_selected !='') {
//            $industrial_unit_electrical_volts_result = mysql_query("SELECT minimum,maximum FROM electrical_volts WHERE size ='" . $industrial_electrical_volts_selected . "'")or die("$ndustrial electrical_volts min max query string error #27: " . mysql_error());
//            $industrial_unit_electrical_volts_row = mysql_fetch_array($industrial_unit_electrical_volts_result, MYSQL_NUM);
            $industrial_unit_electrical_volts_row = $this->minMaxQuery("electrical_volts", $industrial_electrical_volts_selected, $db);
            $industrial_search=3;
        }
        
        if ($industrial_electrical_amps_selected !='All' && $industrial_electrical_amps_selected !='') {
//            $industrial_unit_electrical_amps_result = mysql_query("SELECT minimum,maximum FROM electrical_amps WHERE size ='" . $industrial_electrical_amps_selected . "'")or die("industrial electrical_amps min max query string error #28: " . mysql_error());
//            $industrial_unit_electrical_amps_row = mysql_fetch_array($industrial_unit_electrical_amps_result, MYSQL_NUM);
            $industrial_unit_electrical_amps_row = $this->minMaxQuery("electrical_amps", $industrial_electrical_amps_selected, $db);
            $industrial_search=4;
        }
        
        if ($industrial_shipping_doors_selected !='All' && $industrial_shipping_doors_selected !='') {
//            $industrial_unit_shipping_doors_result = mysql_query("SELECT minimum,maximum FROM num_shipping_doors WHERE size ='" . $industrial_shipping_doors_selected . "'")or die("industrial shipping_doors min max query string error #29: " . mysql_error());
//            $industrial_unit_shipping_doors_row = mysql_fetch_array($industrial_unit_shipping_doors_result, MYSQL_NUM);
            $industrial_unit_shipping_doors_row = $this->minMaxQuery("num_shipping_doors", $industrial_shipping_doors_selected, $db);
            $industrial_search=5;
        }
        
        if ($industrial_drivein_doors_selected !='All' && $industrial_drivein_doors_selected !='') {
//            $industrial_unit_drivein_doors_result = mysql_query("SELECT minimum,maximum FROM num_drivein_doors WHERE size ='" . $industrial_drivein_doors_selected . "'")or die("industrial drivein_doors min max query string error #30: " . mysql_error());
//            $industrial_unit_drivein_doors_row = mysql_fetch_array($industrial_unit_drivein_doors_result, MYSQL_NUM);
            $industrial_unit_drivein_doors_row = $this->minMaxQuery("num_drivein_doors", $industrial_drivein_doors_selected, $db);
            $industrial_search=6;
        }
        
        if ($industrial_parking_selected !='All' && $industrial_parking_selected !='') {
//            $industrial_unit_parking_result = mysql_query("SELECT minimum,maximum FROM num_parking WHERE size ='" . $industrial_parking_selected . "'")or die("industrial parking min max query string error #31: " . mysql_error());
//            $industrial_unit_parking_row = mysql_fetch_array($industrial_unit_parking_result, MYSQL_NUM);
            $industrial_unit_parking_row = $this->minMaxQuery("num_parking", $industrial_parking_selected, $db);
            $industrial_search=7;
        }
        
        if ($industrial_trucktrailer_parking_selected !='All' && $industrial_trucktrailer_parking_selected !='') {

            $industrial_search=8;
        }
        
        
		if ($size_selected != "All" && $size_selected != "Tous") {
//			$size_result = mysql_query("SELECT minimum,maximum FROM sizes WHERE size ='" . $size_selected . "'")or die("min max query string error #32: " . mysql_error());
//			$size_row = mysql_fetch_array($size_result, MYSQL_NUM);
                        $size_row = $this->minMaxQuery("sizes", $size_selected, $db);
		} else {
			$size_row[0] = -1;
			$size_row[1] = 2147483647;
		}
        
        
        
        // industrial suite advanced search elements

        if (($industrial_unit_office_space_row[0] != '-1' && $industrial_unit_office_space_row[0] != '') && ($industrial_unit_office_space_row[1] != '100' && $industrial_unit_office_space_row[1] != '')) {
            
            // ONLY active if SIZE is selected in main search
            if ($size_row[0] != '-1' && $size_row[1] != '2147483647') {

                if ($size_row[0] == 0){
                    $min_total_space = 1;
                    $max_total_space = $size_row[1];
                } else {
                    $min_total_space = $size_row[0];
                    $max_total_space = $size_row[1];
                }

                $minpercentage = $industrial_unit_office_space_row[0] / 100;
                $maxpercentage = $industrial_unit_office_space_row[1] / 100;
                
                $min_office_space = $min_total_space * $maxpercentage;
                $max_office_space = $max_total_space * $maxpercentage;
  
                $suites_query_string .= ' AND (allss.office_space BETWEEN "' . $min_office_space . '" AND "' . $max_office_space . '")';
            }
        }

        
        if ($industrial_clear_height_selected !='All' && $industrial_clear_height_selected !='') {
//            $industrial_clear_height_result = mysql_query("SELECT minimum,maximum FROM clear_height_ranges WHERE size ='" . $industrial_clear_height_selected . "'")or die("$ industrial clear_height min max query string error #33: " . mysql_error());
//            $industrial_clear_height_row = mysql_fetch_array($industrial_clear_height_result, MYSQL_NUM);
            $industrial_clear_height_row = $this->minMaxQuery("clear_height_ranges", $industrial_clear_height_selected, $db);            
        }


        if (isset($industrial_unit_electrical_volts_row[0]) && ($industrial_unit_electrical_volts_row[0] != '-1' && $industrial_unit_electrical_volts_row[0] != '') && ($industrial_unit_electrical_volts_row[1] != '2147483647' && $industrial_unit_electrical_volts_row[1] != '')) {
            $suites_query_string .= ' AND (allss.available_electrical_volts BETWEEN "' . $industrial_unit_electrical_volts_row[0] . '" and "' . $industrial_unit_electrical_volts_row[1] . '")';
        }    

        if (isset($industrial_unit_electrical_amps_row[0]) && ($industrial_unit_electrical_amps_row[0] != '-1' && $industrial_unit_electrical_amps_row[0] != '') && ($industrial_unit_electrical_amps_row[1] != '2147483647' && $industrial_unit_electrical_amps_row[1] != '')) {
            $suites_query_string .= ' AND (allss.available_electrical_amps BETWEEN "' . $industrial_unit_electrical_amps_row[0] . '" and "' . $industrial_unit_electrical_amps_row[1] . '")';
        }    

        if (isset($industrial_unit_shipping_doors_row[0]) && ($industrial_unit_shipping_doors_row[0] != '-1' && $industrial_unit_shipping_doors_row[0] != '') && ($industrial_unit_shipping_doors_row[1] != '2147483647' && $industrial_unit_shipping_doors_row[1] != '')) {
            $suites_query_string .= ' AND (allss.shipping_doors_truck BETWEEN "' . $industrial_unit_shipping_doors_row[0] . '" and "' . $industrial_unit_shipping_doors_row[1] . '")';
        }    

        if (isset($industrial_unit_drivein_doors_row[0]) && ($industrial_unit_drivein_doors_row[0] != '-1' && $industrial_unit_drivein_doors_row[0] != '') && ($industrial_unit_drivein_doors_row[1] != '2147483647' && $industrial_unit_drivein_doors_row[1] != '')) {
            $suites_query_string .= ' AND (allss.shipping_doors_drive_in BETWEEN "' . $industrial_unit_drivein_doors_row[0] . '" and "' . $industrial_unit_drivein_doors_row[1] . '")';
        }    

        if (isset($industrial_unit_parking_stalls_row[0]) && ($industrial_unit_parking_stalls_row[0] != '-1' && $industrial_unit_parking_stalls_row[0] != '') && ($industrial_unit_parking_stalls_row[1] != '2147483647' && $industrial_unit_parking_stalls_row[1] != '')) {
            $suites_query_string .= ' AND (allss.surface_stalls BETWEEN "' . $industrial_unit_parking_stalls_row[0] . '" and "' . $industrial_unit_parking_stalls_row[1] . '")';
        }    

        if ($industrial_trucktrailer_parking_selected !='All' && $industrial_trucktrailer_parking_selected !='') {
            if ($industrial_trucktrailer_parking_selected =='Yes') {
                $suites_query_string .= ' AND (allss.truck_trailer_parking > 0)';
            } else if ($industrial_trucktrailer_parking_selected =='No') {
                $suites_query_string .= ' AND (allss.truck_trailer_parking < 1)';
            }
        } 
        
// END industrial unit advanced search elements

} //  ENDs  if ($industrialsearchelement > 0 && $advanced_search > 0) {
        
        $suites_query_string .= " AND leased='false'";
        $suites_query_string .= " order by alls.floor_number desc, alls.suite_name asc";
//	echo "Suites Query String: " . $suites_query_string . "<br><br>";
        return $suites_query_string;
    }



    
    
    
    /*
     * This method gives the suites query string.
     * Accesses either the abstract or its overriden method from the classes it extends.
     * <p>
     * @param   {Array} $lang
     * @param 	{Array} $row
     * @returns {String} $suite_net_rentable_area
     * @see /main_abstract/Queries_abstract.php
     * @see /main_abstract/FormatData_abstract.php
     * @see suite.php
     * */

    public function get_suite_net_rentable_area($lang, $row) {
        $suite_net_rentable_area = '';
        if ($lang == "en_CA") {
            $suite_net_rentable_area = $row['net_rentable_area'];
            $suite_net_rentable_area = number_format($suite_net_rentable_area);
        }
        if ($lang == "fr_CA") {
            $suite_net_rentable_area = $row['net_rentable_area'];
            $suite_net_rentable_area = number_format($suite_net_rentable_area, 0, ',', ' ');
        }
        return $suite_net_rentable_area;
    }
    
    
    
    public function get_building_net_rentable_area($lang, $row) {
        $suite_net_rentable_area = '';
        if ($lang == "en_CA") {
            $suite_net_rentable_area = $row['net_rentable_area'];
            $suite_net_rentable_area = number_format($suite_net_rentable_area);
        }
        if ($lang == "fr_CA") {
            $suite_net_rentable_area = $row['net_rentable_area'];
            $suite_net_rentable_area = number_format($suite_net_rentable_area, 0, ',', ' ');
        }
        return $suite_net_rentable_area;
    }
    
    
    

    /*
     * This method gives the suites contiguous area query string.
     * Accesses either the abstract or its overriden method from the classes it extends.
     * <p>
     * @param   {Array} $lang
     * @param 	{Array} $row
     * @returns {String} $suite_contiguous_area
     * @see /main_abstract/Queries_abstract.php
     * @see /main_abstract/FormatData_abstract.php
     * @see suite.php
     * */

    public function get_suite_contiguous_area($lang, $row) {
        $suite_contiguous_area = '';
        if ($lang == "en_CA") {
            $suite_contiguous_area = $row['contiguous_area'];
            $suite_contiguous_area = number_format($suite_contiguous_area);
        }
        if ($lang == "fr_CA") {
            $suite_contiguous_area = $row['contiguous_area'];
            $suite_contiguous_area = number_format($suite_contiguous_area, 0, ',', ' ');
        }
        return $suite_contiguous_area;
    }

    public function get_suite_contiguous_area_noformat($lang, $row) {
        $suite_contiguous_area = $row['contiguous_area'];
        return $suite_contiguous_area;
    }
    
    
    public function get_suite_min_divisible($lang, $Suites) {
        
        
        $suite_min_divisible_area = '';
        if ($lang == "en_CA") {
            $suite_min_divisible_area = $Suites['min_divisible_area'];
            $suite_min_divisible_area = number_format($suite_min_divisible_area);
        }
        if ($lang == "fr_CA") {
            $suite_min_divisible_area = $Suites['min_divisible_area'];
            $suite_min_divisible_area = number_format($suite_min_divisible_area, 0, ',', ' ');
        }
        return $suite_min_divisible_area;
    }
    
    
    
    /*
     * This method gets the net rent for the suite.
     * Accesses either the abstract or its overriden method from the classes it extends.
     * <p>
     * @param 	{Array} $row
     * @returns {Double} $suite_net_rent
     * @see /main_abstract/Queries_abstract.php
     * @see /main_abstract/FormatData_abstract.php
     * */

    public function get_suite_net_rent($language, $row) {
        $suite_net_rent = $language['negotiable_text'];
        if ($row['net_rent'] != null && (string) $row['net_rent'] != "0.00") {
            if ($row['net_rent'] != '') {
                $suite_net_rent = $row['net_rent'];
                $suite_net_rent = money_format('%.2n', (double) $suite_net_rent);
            }
        }
        return $suite_net_rent;
    }

    /*
     * This method gets the availability for the suite.
     * Accesses either the abstract or its overriden method from the classes it extends.
     * <p>
     * @param 	{Array} $language
     * @param 	{Array} $row
     * @returns {String} $suite_availability
     * @see /main_abstract/Queries_abstract.php
     * @see /main_abstract/FormatData_abstract.php
     * */

    public function get_suite_availability($language, $row) {
        $suite_availability_raw = $row['availability'];
        $suite_availability = $language['immediately_building'];
        $suite_notice_period = $row['notice_period'];
        
        if ($suite_notice_period != "no") {
            switch ($suite_notice_period) {
                case "30d":
                    $suite_availability = $language['30days_text'];
                    break;
                case "60d":
                    $suite_availability = $language['60days_text'];
                    break;
                case "90d":
                    $suite_availability = $language['90days_text'];
                    break;
                case "120d":
                    $suite_availability = $language['120days_text'];
                    break;
            }
        } else if ($suite_availability_raw != "" && $suite_availability_raw >= time()) {
            $suite_availability = strftime('%b. %Y', $suite_availability_raw);
        } else {
            // no availability
        }       
        return $suite_availability;
    }
    
    
    
    
    
    

    /*
     * This method gets the cross-out data.
     * Accesses either the abstract or its overriden method from the classes it extends.
     * <p>
     * @param 	{String} $suite_leased
     * @returns {String} $cross_out
     * @see /main_abstract/Queries_abstract.php
     * @see /main_abstract/FormatData_abstract.php
     * */

    public function get_cross_out($suite_leased) {
        $cross_out = ' style="text-decoration:line-through;"';
        if ($suite_leased != "true") {

            $cross_out = '';
        }

        return $cross_out;
    }

    public function output_row_format($suite_leased, $suite_promoted) {
        $row_format = '';
        
        if ($suite_leased == "true") {
            $row_format = ' style="text-decoration:line-through;"';
        }
        
        if ($suite_promoted == "true") {
            $row_format = ' style="font-style: italic;font-weight: bold;"';
        }
        
        return $row_format;
    }
    
    
    
    
    
    /*
     * This method does the building region query call to MySQL and returns the SQL query:
     * SELECT DISTINCT b.building_code, region FROM buildings b INNER JOIN suites s ON b.building_code=s.building_code where province=...
     * Accesses either the abstract or its overriden method from the classes it extends.
     * <p>
     * @param 	{String} $province_selected
     * @param 	{Integer} $lang_id
     * @returns {mysql_query} $result
     * @see /main_abstract/Queries_abstract.php
     * @see /main_abstract/DisplayHTML_abstract.php
     * */

    public function buildingRegionQuery($province_selected, $lang_id) {

        $result = mysql_query("SELECT DISTINCT b.building_code, region FROM buildings b where province='" . mysql_real_escape_string($province_selected) . "' and region != '' and b.lang= " . $lang_id . " group by region") or die("Building region query error: " . mysql_error());
        ;
        return $result;
    }

    /*
     * This method does the building sub-region query call to MySQL and returns the SQL query:
     * SELECT DISTINCT b.building_code, sub_region FROM buildings b INNER JOIN suites s ON b.building_code=s.building_code where region=....
     * Accesses either the abstract or its overriden method from the classes it extends.
     * <p>
     * @param 	{String} $region_selected
     * @param 	{Integer} $lang_id
     * @returns {mysql_query} $result 
     * @see /main_abstract/Queries_abstract.php
     * @see /main_abstract/DisplayHTML_abstract.php
     * */

    public function buildingsubRegionQuery($region_selected, $lang_id) {

        $result = mysql_query("SELECT DISTINCT b.building_code, sub_region FROM buildings b where region='" . mysql_real_escape_string($region_selected) . "' and sub_region != '' and b.lang = 1 group by sub_region")
                or die("Building sub-region query error: " . mysql_error());
				
				
        ;
        return $result;
    }

    /*
     * This method does the file-extension query call to MySQL and returns the SQL query:
     * SELECT * from documents where building_code =... and extension=...
     * Accesses either the abstract or its overriden method from the classes it extends.
     * <p>
     * @param 	{String} $building_code
     * @param 	{String} $extension
     * @param 	{Integer} $lang_id
     * @returns {mysql_query} $result 
     * @see building.php
     * @see /main_abstract/Queries_abstract.php
     * */

//    public function file_extension_query($building_code, $extension, $lang_id) {
//        $result = mysql_query("select * from documents where building_code ='" . $building_code . "' and extension='" . $extension . "'") or die("documents query error: " . mysql_error());
//        return $result;
//    }
    
    
    public function file_extension_query($building_code, $extension, $lang_id, $db) {
//        $result = mysql_query("select * from documents where building_code ='" . $building_code . "' and extension='" . $extension . "'") or die("document ext query error: " . mysql_error());
        $extension_query_string = "select document_index, lang, building_id, building_code, file_name, display_filename, description, uri, thumb, lastModifiedTime, extension, dirty from documents where building_code=? and extension=? order by building_featured_plan DESC";
        if ($extension_query_statement = $db->prepare($extension_query_string)) {
            // carry on
        } else {
            trigger_error('Wrong extension_query_string SQL: ' . $extension_query_string . ' -- Extension: ' . $extension . ' -- Error: ' . $db->error, E_USER_ERROR);
        }
        $extension_query_statement->bind_param('ss', mysqli_real_escape_string($db, $building_code), $extension);
        $results = array();
//        while($row = $extension_query_statement->fetch_array(MYSQLI_ASSOC)){
//            $result[] = $row;
//        }
        if ($extension_query_statement->execute()){
            $result = $extension_query_statement->get_result();
            while($row = $result->fetch_array(MYSQLI_ASSOC)){ 
                $results[] = $row;
            }
        } else {
            trigger_error('Wrong contact statement: ' . $result_string . ' Error: ' . $db->error, E_USER_ERROR);
        }
        $extension_query_statement->close();
        return $results;
    }
    
    
    
    
    
    
    
    

    public function unusual_document_extensions($building_code, $extensions, $lang_id, $db) {
//        $queryStr = "select * from documents where building_code ='" . $building_code . "'";
//        foreach ($extensions as $extension) {
//            $queryStr .= " and NOT extension ='" . $extension . "'";
//        }
//        $result = mysql_query($queryStr)
//                or die("documents query error: " . mysql_error());
//        return $result;

        $queryStr = "select document_index, lang, building_id, building_code, file_name, display_filename, description, uri, thumb, lastModifiedTime, extension, dirty from documents where building_code =?";
        foreach ($extensions as $extension) {
            $queryStr .= " and NOT extension ='" . $extension . "'";
        }
//        $extension_query_string = "select * from documents where building_code =? and extension=?";
        if ($extension_query_statement = $db->prepare($queryStr)) {
            // carry on
        } else {
            trigger_error('Wrong contact SQL: ' . $queryStr . ' -- Extension: ' . $extension . ' -- Error: ' . $db->error, E_USER_ERROR);
        }
        $extension_query_statement->bind_param('s', mysqli_real_escape_string($db, $building_code));
        $extension_query_statement->execute();
        $results = array();
        if ($extension_query_statement->execute()){
            $result = $extension_query_statement->get_result();
            while($row = $result->fetch_array(MYSQLI_ASSOC)){ 
                $results[] = $row;
            }
        } else {
            trigger_error('Wrong contact statement: ' . $result_string . ' Error: ' . $db->error, E_USER_ERROR);
        }
        $extension_query_statement->close();
        return $results;
    }
    
    
    

    /*
     * This method does the contacts query call to MySQL and returns the SQL query:
     * SELECT * from contacts where building_code =...
     * Accesses either the abstract or its overriden method from the classes it extends.
     * <p>
     * @param 	{String} $building_code
     * @param 	{Integer} $lang_id
     * @returns {mysql_query} $result 
     * @see index.php
     * @see suite.php
     * @see /main_abstract/DisplayHTML_abstract.php
     * */

    public function contacts_query($building_selected, $lang_id, $db) {
//        $result = mysql_query("select * from contacts where building_code ='" . $building_selected . "' and lang=" . $lang_id . " order by contact_index asc") or die("Contacts query error: " . mysql_error());
//        return $result;
        
        
        $results = array();  
        $result_string = "select contact_index, lang, priority, building_id, building_code, type, name, title, company, email, phone from contacts where building_code=? and lang=? order by contact_index asc ";
        if ($result_statement = $db->prepare($result_string)) {
            // carry on
        } else {
            trigger_error('Wrong contact SQL: ' . $result_string . ' Error: ' . $db->error, E_USER_ERROR);
        }
        
        if ($result_statement->bind_param('si', mysqli_real_escape_string($db, $building_selected), mysqli_real_escape_string($db, $lang_id))) {
            
        } else {
            trigger_error('Wrong contact binding: ' . $result_string . ' Error: ' . $db->error, E_USER_ERROR);
        } 
        if ($result_statement->execute()){
            $result = $result_statement->get_result();
            while($row = $result->fetch_array(MYSQLI_ASSOC)){ 
                $results[] = $row;
            }
        } else {
            trigger_error('Wrong contact statement: ' . $result_string . ' Error: ' . $db->error, E_USER_ERROR);
        }
        $result_statement->close();
        return $results;
    }

    
    
    public function contacts_tabfooter_query($building_selected, $lang_id, $db) {
//        $result = mysql_query("select * from contacts where building_code ='" . $building_selected . "' and lang=" . $lang_id . " order by contact_index asc limit 2") or die("Contacts query error tabfooter: " . mysql_error());
//        return $result;
        
        $results = array();  
        $result_string = "select contact_index, lang, priority, building_id, building_code, type, name, title, company, email, phone from contacts where building_code=? and lang=? order by contact_index asc limit 2 ";
        if ($result_statement = $db->prepare($result_string)) {
            // carry on
        } else {
            trigger_error('Wrong contact SQL: ' . $result_string . ' Error: ' . $db->error, E_USER_ERROR);
        }
        
        if ($result_statement->bind_param('si', mysqli_real_escape_string($db, $building_selected), mysqli_real_escape_string($db, $lang_id))) {
            
        } else {
            trigger_error('Wrong contact binding: ' . $result_string . ' Error: ' . $db->error, E_USER_ERROR);
        } 
        if ($result_statement->execute()){
            $result = $result_statement->get_result();
            while($row = $result->fetch_array(MYSQLI_ASSOC)){ 
                $results[] = $row;
            }
        } else {
            trigger_error('Wrong contact statement: ' . $result_string . ' Error: ' . $db->error, E_USER_ERROR);
        }
        $result_statement->close();
        return $results;
        
    }
    
    
    
    
    public function contacts_tabfooter_emailaddress_query($building_selected, $lang_id, $db) {
//        $result = mysql_query("select email as allemail from contacts where building_code ='" . $building_selected . "' and lang=" . $lang_id . " order by contact_index") or die("AllContacts query error: " . mysql_error());
//        return $result;
        
        
        
        $results = array();  
        $result_string = "select email as allemail from contacts where building_code=? and lang=? order by contact_index ";
        if ($result_statement = $db->prepare($result_string)) {
            // carry on
        } else {
            trigger_error('Wrong contact SQL: ' . $result_string . ' Error: ' . $db->error, E_USER_ERROR);
        }
        
        if ($result_statement->bind_param('si', mysqli_real_escape_string($db, $building_selected), mysqli_real_escape_string($db, $lang_id))) {
            
        } else {
            trigger_error('Wrong contact binding: ' . $result_string . ' Error: ' . $db->error, E_USER_ERROR);
        } 
        if ($result_statement->execute()){
            $result = $result_statement->get_result();
            while($row = $result->fetch_array(MYSQLI_ASSOC)){ 
                $results[] = $row;
            }
        } else {
            trigger_error('Wrong contact statement: ' . $result_string . ' Error: ' . $db->error, E_USER_ERROR);
        }
        $result_statement->close();
        return $results;
        
        
        
        
        
        
    }

    /*
    

    /*
     * This method is used to make the next and previous links
     * Accesses either the abstract or its overriden method from the classes it extends.
     * <p>
     * @param 	{Array} $language
     * @param 	{Integer} $lang_id
     * @returns {String} $prev_next_query
     * @see /main_abstract/Queries_abstract.php
     * */

    public function prev_next_query($language, $lang_id) {
        //make the previous and next links
//        $prev_next_query = "select building_code from buildings where lang=" . $lang_id;
//
//        if (isset($_REQUEST['province']) && $_REQUEST['province'] != "All") {
//            $prev_next_query .= " and province='" . $_REQUEST['province'] . "'";
//        } else {
//            if ($language['default_province'] != 'All') {
//                if ($lang_id == 1) {
//                    $prev_next_query .= " and province='" . $language['default_province'] . "'";
//                } else {
//                    $prev_next_query .= " and province='" . $language['default_province'] . "'";
//                }
//            }
//        }
//        if (isset($_REQUEST['region']) && $_REQUEST['region'] != "All") {
//            $prev_next_query .= " and region='" . $_REQUEST['region'] . "'";
//        }
//        if (isset($_REQUEST['sub-region']) && $_REQUEST['sub-region'] != "All") {
//            $prev_next_query .= " and sub_region='" . $_REQUEST['sub-region'] . "'";
//        }
//        if (isset($_REQUEST['BuildingType']) && $_REQUEST['BuildingType'] != "All") {
//            $prev_next_query .= " and building_type='" . $_REQUEST['BuildingType'] . "'";
//        }
//
//        $prev_next_query .= " order by building_code asc";

        
        
        $prev_next_query = "select building_code from buildings where lang=?";
        $prev_next_bindvars = array();
        array_push($prev_next_bindvars, $lang_id);
        $prev_next_paramtypes = array();
        array_push($prev_next_paramtypes,"i");
        if (isset($_REQUEST['province']) && $_REQUEST['province'] != "All") {
            
            $prev_next_query .= " and province=?";
            array_push($prev_next_paramtypes,"s");
            array_push($prev_next_bindvars, $_REQUEST['province']);
            
        } else {
            if ($language['default_province'] != 'All') {
                if ($lang_id == 1) {
                    $prev_next_query .= " and province=?";
                    array_push($prev_next_paramtypes,"s");
                    array_push($prev_next_bindvars, $language['default_province']);
                } else {
                    $prev_next_query .= " and province=?";
                    array_push($prev_next_paramtypes,"s");
                    array_push($prev_next_bindvars, $language['default_province']);
                }
            }
        }
        if (isset($_REQUEST['region']) && $_REQUEST['region'] != "All") {
            $prev_next_query .= " and region=?";
            array_push($prev_next_paramtypes,"s");
            array_push($prev_next_bindvars, $_REQUEST['region']);
        }
        if (isset($_REQUEST['sub-region']) && $_REQUEST['sub-region'] != "All") {
            $prev_next_query .= " and sub_region=?";
            array_push($prev_next_paramtypes,"s");
            array_push($prev_next_bindvars, $_REQUEST['sub-region']);
        }
        if (isset($_REQUEST['BuildingType']) && $_REQUEST['BuildingType'] != "All") {
            $prev_next_query .= " and building_type=?";
            array_push($prev_next_paramtypes,"s");
            array_push($prev_next_bindvars, $_REQUEST['BuildingType']);
        }
        $prev_next_query .= " order by building_code asc";
//        $prev_next_paramtypes .= "'";
        // build array for use with call_user_func_array 
        // for binding values to prepared statement
        $output = array();
        array_push($output, $prev_next_query);
        array_push($output, $prev_next_paramtypes);
        array_push($output, $prev_next_bindvars);
        return $output;
    }

    
    
    
    
    
    
    
    
    
    
    
    
    
    /*
     * This method does the sections query call to MySQL and returns the SQL query:
     * SELECT * from sections where building_code =...
     * Accesses either the abstract or its overriden method from the classes it extends.
     * <p>
     * @param 	{String} $building_code
     * @param 	{Integer} $lang_id
     * @returns {mysql_query} $result 
     * @see building.php
     * @see /main_abstract/DisplayHTML_abstract.php
     * @see /main_abstract/Queries_abstract.php
     * @see sections.php
     * */

    public function sections_query($building_code, $lang_id) {
        $result_query_string = "select * from sections where section_content != '' AND building_code ='" . $building_code . "' AND lang=" . $lang_id . " order by section_index asc";
        $result = mysql_query($result_query_string) or die("Sections query error: " . mysql_error());
        return $result;
    }

    /*
     * This method does the preview images query call to MySQL and returns the SQL query:
     * SELECT * from preview_images where building_code =...
     * Accesses either the abstract or its overriden method from the classes it extends.
     * <p>
     * @param 	{String} $building_code
     * @param 	{Integer} $lang_id
     * @returns {mysql_query} $result 
     * @see building.php
     * @see /main_abstract/DisplayHTML_abstract.php
     * @see /main_abstract/Queries_abstract.php
     * */

    public function preview_images_query($building_code, $lang_id, $db) {
//        $result = mysql_query("select * from preview_images where building_code ='" . $building_code . "' and thumbnail=0 order by list_index asc") or die("Preview images query error: " . mysql_error());
//        return $result;
        
        $preview_images_query = array();  
        $preview_images_query_string = "select preview_index, lang, building_id, building_code, file_name, display_file_name, extension, list_index, description, uri, last_modified_time, thumbnail, dirty from preview_images where building_code =? and thumbnail=0 order by list_index asc";
        if ($preview_images_query_statement = $db->prepare($preview_images_query_string)) {
            // carry on
        } else {
            trigger_error('Wrong SQL: ' . $preview_images_query_string . ' Error: ' . $db->error, E_USER_ERROR);
        }
        
        $preview_images_query_statement->bind_param('s', mysqli_real_escape_string($db, $building_code));
        if ($preview_images_query_statement->execute()){
            $result = $preview_images_query_statement->get_result();
            while($row = $result->fetch_array(MYSQLI_ASSOC)){ 
                $preview_images_query[] = $row;
            }
        } else {
            trigger_error('Wrong SQL: ' . $preview_images_query_string . ' Error: ' . $db->error, E_USER_ERROR);
        }
        $preview_images_query_statement->close();
        return $preview_images_query;
    }

    /*
     * This method does the suites query call to MySQL and returns the SQL query:
     * SELECT * from suites where building_code =...
     * Accesses either the abstract or its overriden method from the classes it extends.
     * <p>
     * @param 	{String} $building_selected
     * @param 	{String} $suite_selected
     * @param 	{Integer} $lang_id
     * @returns {mysql_query} $suites_query
     * @see building.php
     * @see index.php
     * @see printpdfbuildingpackage.php
     * @see /main_abstract/Queries_abstract.php
     * */

    public function suites_query($building_selected, $suite_selected, $lang_id, $db) {
//        $suites_query = mysql_query("select * from suites where building_code = '" . $building_selected . "' and suite_id = '" . $suite_selected . "' and lang=" . $lang_id)
//                or die("Suites query error: " . mysql_error());
//        return $suites_query;
        
        $suites_query = array();  
        $suites_query_string = "Select suite_index, lang, building_code, building_id, floor_name, floor_number, suite_id, suite_name, suite_type, net_rentable_area, contiguous_area, min_divisible_area, taxes_and_operating, availability, notice_period, description, net_rent, add_rent_total, presentation_link, presentation_name, presentation_description, pdf_plan, spp_link, model, new, leased, promoted, marketing_text, record_date, record_source, dirty, bu_flag from suites where building_code = ? and suite_id = ? and lang=? ";
        if ($suites_query_statement = $db->prepare($suites_query_string)) {
            // carry on
        } else {
            trigger_error('Wrong SQL: ' . $suites_query_string . ' Error: ' . $db->error, E_USER_ERROR);
        }
        
        $suites_query_statement->bind_param('ssi', mysqli_real_escape_string($db, $building_selected), mysqli_real_escape_string($db, $suite_selected), mysqli_real_escape_string($db, $lang_id));
        if ($suites_query_statement->execute()){
            $result = $suites_query_statement->get_result();
            while($row = $result->fetch_array(MYSQLI_ASSOC)){ 
                $suites_query[] = $row;
            }
        } else {
            trigger_error('Wrong SQL: ' . $suites_query_string . ' Error: ' . $db->error, E_USER_ERROR);
        }
        $suites_query_statement->close();
        return $suites_query;
        
        
        
        
        
        
        
    }

    /*
     * This method does the suite documents query call to MySQL and returns the SQL query:
     * SELECT * from suite_documents where suite_name =...
     * Accesses either the abstract or its overriden method from the classes it extends.
     * <p>
     * @param 	{String} $suite_selected
     * @param 	{String} $building_code
     * @returns {mysql_query} $suite_documents_query
     * @see /main_abstract/Queries_abstract.php
     * @see /main_abstract/DisplayHTML_abstract.php
     * */

    public function suite_documents_query($suite_selected, $building_code, $lang_id, $db) {
//        $suite_documents_query_string = "select * from suite_documents where suite_id = '" . $suite_selected . "' and building_code ='" . $building_code . "' and extension != 'mp4'";
//        $suite_documents_query = mysql_query($suite_documents_query_string) or die("suite_documents_query error: " . mysql_error());
//        return $suite_documents_query;

        $suite_documents_query = array();  
        $suite_documents_query_string = "select document_index, lang, building_id, building_code, suite_id, suite_name, file_name, display_filename, description, uri, thumb, lastModifiedTime, extension, dirty from suite_documents where suite_id =? and building_code =? and extension != 'mp4'";
        if ($suite_documents_query_statement = $db->prepare($suite_documents_query_string)) {
            // carry on
        } else {
            trigger_error('Wrong SQL: ' . $suite_documents_query_string . ' Error: ' . $db->error, E_USER_ERROR);
        }
        
        $suite_documents_query_statement->bind_param('ss', mysqli_real_escape_string($db, $suite_selected), mysqli_real_escape_string($db, $building_code));
        if ($suite_documents_query_statement->execute()){
            $result = $suite_documents_query_statement->get_result();
            while($row = $result->fetch_array(MYSQLI_ASSOC)){ 
                $suite_documents_query[] = $row;
            }
        } else {
            trigger_error('Wrong SQL: ' . $suite_documents_query_string . ' Error: ' . $db->error, E_USER_ERROR);
        }
        $suite_documents_query_statement->close();
        return $suite_documents_query;

    }

    /*
     * This method does the ssuite and a preview image query call to MySQL and returns the SQL query:
     * SELECT * from suite_preview_images where suite_name =...
     * Accesses either the abstract or its overriden method from the classes it extends.
     * <p>
     * @param 	{String} $suite_selected
     * @param 	{String} $building_code
     * @returns {mysql_query} $preview_images_query
     * @see /main_abstract/Queries_abstract.php
     * */

    public function suite_preview_images_query($suite_selected, $building_code, $lang_id, $db) {
//        $preview_images_query_string = "select * from suite_preview_images where suite_id = '" . $suite_selected . "' and building_code ='" . $building_code . "' and thumbnail=0 order by list_index asc";
//        $preview_images_query = mysql_query($preview_images_query_string) or die ("Preview images query error: " . mysql_error());
//        return $preview_images_query;
        
        $suite_preview_images_query = array();  
        $suite_preview_images_query_string = "select preview_index, lang, building_id, building_code, suite_id, suite_name, list_index, file_name, display_file_name, extension, description, uri, last_modified_time, thumbnail, dirty from suite_preview_images where suite_id =? and building_code =? and thumbnail=0 order by list_index asc";
        if ($suite_preview_images_query_statement = $db->prepare($suite_preview_images_query_string)) {
            // carry on
        } else {
            trigger_error('Wrong SQL: ' . $suite_preview_images_query_string . ' Error: ' . $db->error, E_USER_ERROR);
        }
        
        $suite_preview_images_query_statement->bind_param('ss', mysqli_real_escape_string($db, $suite_selected), mysqli_real_escape_string($db, $building_code));
        if ($suite_preview_images_query_statement->execute()){
            $result = $suite_preview_images_query_statement->get_result();
            while($row = $result->fetch_array(MYSQLI_ASSOC)){ 
                $suite_preview_images_query[] = $row;
            }
        } else {
            trigger_error('Wrong SQL: ' . $suite_preview_images_query_string . ' Error: ' . $db->error, E_USER_ERROR);
        }
        $suite_preview_images_query_statement->close();
        return $suite_preview_images_query;
    }

    /*
     * This method does the 3D query call to MySQL and returns the SQL query:
     * SELECT * from files_3d where suite_name =...
     * Accesses either the abstract or its overriden method from the classes it extends.
     * <p>
     * @param 	{String} $building_selected
     * @param 	{String} $suite_selected
     * @param 	{Integer} $lang_id
     * @returns {mysql_query} $threedee_query
     * @see /main_abstract/Queries_abstract.php
     * */

    public function threedee_query($building_selected, $suite_selected, $lang_id, $db) {
//        $threedee_query_string = "Select * from files_3d where suite_id = '" . $suite_selected . "' and building_code = '" . $building_selected . "'";
//        $threedee_query = mysql_query($threedee_query_string) or die ("Threedee query string error: " . mysql_error());
//        return $threedee_query;
        
        $threedee_query = array();  
        $threedee_query_string = "Select threedee_id, suite_id, suite_name, building_id, building_code, presentation_link, presentation_description, presentation_name, presentation_sppfile, presentation_thumb, lastModifiedTime, sketchfabEmbedCode, lang, threedee_request_id, dirty from files_3d where suite_id = ? and building_code = ?";
        if ($threedee_query_statement = $db->prepare($threedee_query_string)) {
            // carry on
        } else {
            trigger_error('Wrong SQL: ' . $threedee_query_string . ' Error: ' . $db->error, E_USER_ERROR);
        }
        
        $threedee_query_statement->bind_param('ss', mysqli_real_escape_string($db, $suite_selected), mysqli_real_escape_string($db, $building_selected));
        if ($threedee_query_statement->execute()){
            $result = $threedee_query_statement->get_result();
            while($row = $result->fetch_array(MYSQLI_ASSOC)){ 
                $threedee_query[] = $row;
            }
        } else {
            trigger_error('Wrong SQL: ' . $threedee_query_string . ' Error: ' . $db->error, E_USER_ERROR);
        }
        $threedee_query_statement->close();
        return $threedee_query;
    }




    /*
     * This method does the 3D query call to MySQL and returns the SQL query:
     * SELECT * from files_3d where suite_name =...
     * Accesses either the abstract or its overriden method from the classes it extends.
     * <p>
     * @param 	{String} $building_selected
     * @param 	{String} $suite_selected
     * @param 	{Integer} $lang_id
     * @returns {mysql_query} $threedee_query
     * @see /main_abstract/Queries_abstract.php
     * */

    public function threedeeSketchfab_query($building_selected, $suite_selected, $lang_id, $db) {
//        $threedee_query_string = "Select * from files_3d where suite_id = '" . $suite_selected . "' and building_code = '" . $building_selected . "' and sketchfabEmbedCode != ''";
//        $threedee_query = mysql_query($threedee_query_string) or die ("Threedee sketchfab query string error: " . mysql_error());
//        return $threedee_query;
    

    
        $threedee_query = array();  
        $threedee_query_string = "Select threedee_id, suite_id, suite_name, building_id, building_code, presentation_link, presentation_description, presentation_name, presentation_sppfile, presentation_thumb, lastModifiedTime, sketchfabEmbedCode, lang, threedee_request_id, dirty from files_3d where suite_id = ? and building_code = ? and sketchfabEmbedCode != '' ";
        if ($threedee_query_statement = $db->prepare($threedee_query_string)) {
            // carry on
        } else {
            trigger_error('Wrong SQL: ' . $threedee_query_string . ' Error: ' . $db->error, E_USER_ERROR);
        }
        
        $threedee_query_statement->bind_param('ss', mysqli_real_escape_string($db, $suite_selected), mysqli_real_escape_string($db, $building_selected));
        if ($threedee_query_statement->execute()){
            $result = $threedee_query_statement->get_result();
            while($row = $result->fetch_array(MYSQLI_ASSOC)){ 
                $threedee_query[] = $row;
            }
        } else {
            trigger_error('Wrong SQL: ' . $threedee_query_string . ' Error: ' . $db->error, E_USER_ERROR);
        }
        $threedee_query_statement->close();
        return $threedee_query;
    
    
    }





     /*
     * This method does the 3D query call to MySQL and returns the SQL query:
     * SELECT * from files_3d where building_code =...
     * Accesses either the abstract or its overriden method from the classes it extends.
     * <p>
     * @param 	{String} $building_selected
     * @param 	{Integer} $lang_id
     * @returns {mysql_query} $threedee_query
     * @see /main_abstract/Queries_abstract.php
     * */

    public function threedee_query_building($building_selected, $lang_id, $db) {
//        $threedee_query_string = "Select * from files_3d where suite_id is NULL and building_code = '" . $building_selected . "'";
//        $threedee_query = mysql_query($threedee_query_string) or die("Building Threedee query string error: " . mysql_error());
//        return $threedee_query;
        
        $threedee_query = array();  
        $threedee_query_string = "Select threedee_id, suite_id, suite_name, building_id, building_code, presentation_link, presentation_description, presentation_name, presentation_sppfile, presentation_thumb, type_3d, lastModifiedTime, sketchfabEmbedCode, lang, threedee_request_id, dirty from files_3d where suite_id IS NULL OR suite_id = '' and building_code = ?";
        if ($threedee_query_statement = $db->prepare($threedee_query_string)) {
            // carry on
        } else {
            trigger_error('Wrong SQL: ' . $threedee_query_string . ' Error: ' . $db->error, E_USER_ERROR);
        }
        
        $threedee_query_statement->bind_param('s', mysqli_real_escape_string($db, $building_selected));
        if ($threedee_query_statement->execute()){
            $result = $threedee_query_statement->get_result();
            while($row = $result->fetch_array(MYSQLI_ASSOC)){ 
                $threedee_query[] = $row;
            }
        } else {
            trigger_error('Wrong SQL: ' . $threedee_query_string . ' Error: ' . $db->error, E_USER_ERROR);
        }
        $threedee_query_statement->close();
        return $threedee_query;
        
        
        
    }

    /*
     * This method does the PDF query call to MySQL and returns the SQL query:
     * SELECT * from files_2d where suite_name=...
     * Accesses either the abstract or its overriden method from the classes it extends.
     * <p>
     * @param 	{String} $building_code
     * @param 	{String} $suite_selected
     * @param 	{Integer} $lang_id
     * @returns {mysql_query} $pdf_query
     * @see building.php
     * @see /main_abstract/Queries_abstract.php
     * @see /main_abstract/DisplayHTML_abstract.php
     * */

    public function pdf_query($building_code, $suite_selected, $lang_id, $db) {
////        $pdf_query_string = "select * from files_2d where suite_id = '" . $suite_selected . "' and building_code ='" . $building_code . "'";
////        $pdf_query = mysql_query($pdf_query_string) or die("PDF query error: " . mysql_error());
//        
//        $pdf_query_string = "select twodeeid, suite_id, suite_name, suite_floor, building_id, building_code, plan_name, display_filename, plan_description, plan_uri, plan_thumb, lastModifiedTime, lang, dirty from files_2d where suite_id =? and building_code =?";
//        $pdf_query_statement = $db->prepare($pdf_query_string);
//        $pdf_query_statement->bind_param('ss', mysqli_real_escape_string($db, $suite_selected), mysqli_real_escape_string($db, $building_code));
//        $pdf_query_statement->execute();
////        $result = $pdf_query_statement->get_result();
////        $pdf_query = $result->fetch_array(MYSQLI_ASSOC);
//        $pdf_query_statement->bind_result($twodeeid, $suite_id, $suite_name, $suite_floor, $building_id, $building_code, $plan_name, $display_filename, $plan_description, $plan_uri, $plan_thumb, $lastModifiedTime, $lang, $dirty);
//        $pdf_query = array();
//        while($row = $pdf_query_statement->fetch()){
//            $pdf_query[] = $row;
//        }
        
        $pdf_query = array();  
        $pdf_query_string = "select twodeeid, suite_id, suite_name, suite_floor, building_id, building_code, plan_name, display_filename, plan_description, plan_uri, plan_thumb, lastModifiedTime, lang, dirty from files_2d where suite_id =? and building_code =? ORDER BY suite_featured_plan DESC";
        if ($pdf_query_statement = $db->prepare($pdf_query_string)) {
            // carry on
        } else {
            trigger_error('Wrong pdf_query_string SQL: ' . $pdf_query_string . ' Error: ' . $db->error, E_USER_ERROR);
        }
        $pdf_query_statement->bind_param('ss', mysqli_real_escape_string($db, $suite_selected), mysqli_real_escape_string($db, $building_code));
        if ($pdf_query_statement->execute()){
            $result = $pdf_query_statement->get_result();
            while($row = $result->fetch_array(MYSQLI_ASSOC)){ 
                $pdf_query[] = $row;
            };
        } 
        $pdf_query_statement->close();
        return $pdf_query;
    }
    
     /*
     * This method does the PDF query call to MySQL and returns the SQL query:
     * SELECT * from files_2d where building_code=...
     * Accesses either the abstract or its overriden method from the classes it extends.
     * <p>
     * @param 	{String} $building_code
     * @param 	{String} $suite_selected
     * @param 	{Integer} $lang_id
     * @returns {mysql_query} $pdf_query
     * @see building.php
     * @see /main_abstract/Queries_abstract.php
     * @see /main_abstract/DisplayHTML_abstract.php
     * This method is used to get all as-built PDF's for a building regardless of suite id.
     * */   
    public function pdf_asbuilt_query($building_code, $lang_id, $db) {
//        $pdf_asbuilt_query_string = "select * from files_2d where building_code ='" . $building_code . "'";
//        $pdf_asbuilt_query = mysql_query($pdf_asbuilt_query_string) or die("PDF asbuilt query error: " . mysql_error());
//        return $pdf_asbuilt_query;
        
        $pdf_asbuilt_query = array();  
        $pdf_asbuilt_query_string = "select twodeeid, suite_id, suite_name, suite_floor, building_id, building_code, plan_name, display_filename, plan_description, plan_uri, plan_thumb, lastModifiedTime, lang, dirty from files_2d where building_code =? ORDER BY suite_featured_plan DESC";
        if ($pdf_asbuilt_query_statement = $db->prepare($pdf_asbuilt_query_string)) {
            // carry on
        } else {
            trigger_error('Wrong SQL: ' . $pdf_asbuilt_query_string . ' Error: ' . $db->error, E_USER_ERROR);
        }
        
        $pdf_asbuilt_query_statement->bind_param('s', mysqli_real_escape_string($db, $building_code));
        if ($pdf_asbuilt_query_statement->execute()){
            $result = $pdf_asbuilt_query_statement->get_result();
            while($row = $result->fetch_array(MYSQLI_ASSOC)){ 
                $pdf_asbuilt_query[] = $row;
            }
        } else {
            trigger_error('Wrong SQL: ' . $pdf_asbuilt_query_string . ' Error: ' . $db->error, E_USER_ERROR);
        }
        $pdf_asbuilt_query_statement->close();
        return $pdf_asbuilt_query;
    }

    /*
     * This method does the video query call to MySQL and returns the SQL query:
     * SELECT * from suite_documents where suite_name =...
     * Accesses either the abstract or its overriden method from the classes it extends.
     * <p>
     * @param 	{String} $building_code
     * @param 	{String} $suite_selected
     * @param 	{Integer} $lang_id
     * @returns {mysql_query} $video_query
     * @see building.php
     * @see /main_abstract/Queries_abstract.php
     * @see /main_abstract/DisplayHTML_abstract.php
     * */
    
    public function video_query($building_code, $suite_selected, $lang_id, $db) {
////        $video_query_string = "select * from suite_documents where suite_id = '" . $suite_selected . "' and building_code ='" . $building_code . "' and extension='mp4'";
//        $video_query_string = "select * from suite_documents where suite_id = ? and building_code =? and extension='mp4'";
//        $video_query_statement = $db->prepare($video_query_string);
//        $video_query_statement->bind_param('ss', mysqli_real_escape_string($db, $building_code), mysqli_real_escape_string($db, $suite_selected));
//        $video_query_statement->execute();
//        $video_query = array();
//        while($row = $video_query_statement->fetch()){
//            $video_query[] = $row;
//        }
//        return $video_query;
        
        
        $video_query = array();  
        $video_query_string = "select document_index, lang, building_id, building_code, suite_id, suite_name, file_name, display_filename, description, uri, thumb, lastModifiedTime, extension, dirty from suite_documents where suite_id = ? and building_code =? and extension='mp4'";
        $video_query_statement = $db->prepare($video_query_string);
        $video_query_statement->bind_param('ss', mysqli_real_escape_string($db, $suite_selected), mysqli_real_escape_string($db, $building_code));
        if ($video_query_statement->execute()){
            $result = $video_query_statement->get_result();
            while($row = $result->fetch_array(MYSQLI_ASSOC)){ 
                $video_query[] = $row;
            }
        }
        $video_query_statement->close();
        return $video_query;
    }

    
    
    
    public function regional_branding_query($province_selected, $db) {
        // REGIONAL BRANDING QUERY
//        $rbq_string = "select * from branding where region = '". $province_selected ."'";
//        $rbq = mysql_query($rbq_string) or die ("regional branding query error: " . mysql_error());
//        return $rbq;
        
        $regional_branding_query = array();  
        $regional_branding_query_string = "select branding_id, region, company_name, street_address, city, prov_state, postal_zip, phone, fax, tollfree from branding where region = ?";
        if ($regional_branding_query_statement = $db->prepare($regional_branding_query_string)) {
            // carry on
        } else {
            trigger_error('Wrong SQL: ' . $regional_branding_query_string . ' Error: ' . $db->error, E_USER_ERROR);
        }
        
        $regional_branding_query_statement->bind_param('s', mysqli_real_escape_string($db, $province_selected));
        if ($regional_branding_query_statement->execute()){
            $result = $regional_branding_query_statement->get_result();
            while($row = $result->fetch_array(MYSQLI_ASSOC)){ 
                $regional_branding_query[] = $row;
            }
        } else {
            trigger_error('Wrong SQL: ' . $regional_branding_query_string . ' Error: ' . $db->error, E_USER_ERROR);
        }
        $regional_branding_query_statement->close();
        return $regional_branding_query;
    }
    

    /*
     * This method gets the building query string from $pdf_query.
     * Accesses either the abstract or its overriden method from the classes it extends.
     * <p>
     * @param 	{mysql_query} $filename_query
     * @returns {String} $buildings_query_string
     * @see DisplayHTML.php

     * */

    public function query_fetch_array($filename_query, $graphics) {
//include("assets/php/krumo/class.krumo.php");
//krumo($filename_query);

        $HTML = "";
        foreach ($filename_query as $row) {
//print_r($row);
            $file_name = $row['display_filename'];
            if (strlen($file_name) > 20) {
                $file_name = substr($file_name, 0, 18) . '...';
            }

            if (strpos(strtoupper($row['extension']), 'MP4') !== false || strpos(strtoupper($row['extension']), 'PDF') !== false) {
				
			if ($row['extension'] == 'pdf') {
				$thumb = 'images/documents/thumb_'.$row['thumb'];
			} else {
				$thumb = 'images/documents/'.$row['thumb'];
			}

                $HTML .= '<div class="col fs_gallery-thumbnail">'
                        . '<a data-fancybox="gallery" class="featuredocclick" href="images/documents/' . $row['file_name'] . '" title="' . $row['display_filename'] . ' "  target="_blank" ><img src="' . $thumb . '" class="img-fluid" /></a>'
                        . '</div>';
                
                
            } elseif (strpos(strtoupper($row['extension']), 'PNG') !== false) { 
				
			if (($row['extension'] == 'png') || ($row['extension'] == 'jpg') || ($row['extension'] == 'gif')) {
				$thumb = 'images/documents/'.$row['file_name'];
			}

                $HTML .= '<div class="col fs_gallery-thumbnail">'
                        . '<a data-fancybox="gallery" class="featuredocclick" href="' . $row['uri'] . '" title="' . $row['display_filename'] . ' "  target="_blank" ><img src="' . $thumb . '" class="img-fluid" /></a>'
                        . '</div';
            } else {

                $pth = $graphics->addTextToPNG($row['extension'], "images/newFileExtensions/icon.png", "images/newFileExtensions/");

                $HTML .= '<div class="col fs_gallery-thumbnail">'
                        . '<a class="featuredocclick" href="' . $row['uri'] . '" title="' . $row['file_name'] . ' "  target="_blank" ><img src="' . $pth . '" class="img-fluid" /></a>'
                        . '</div>';
            }
        }
        return $HTML;
    }

  
}

?>
