<style type="text/css">

    body{
        font-family: "Arial";
    }

</style>


<?php
$lang = "en_CA";
$lang_id = 1;
if (isset($_REQUEST['lang'])) {
    if ($_REQUEST['lang'] == "fr_CA") {
        //	
        $lang = "fr_CA";
        $lang_id = 0;
    }
}

if ($lang_id == '0') {
    $squarefoot = "pi&sup2;";
} else {
    $squarefoot = "sf";
}


include('../theme/db.php');
include('../objects/Queries.php');
include('../objects/ErrorMessages.php');
include('../objects/SearchData.php');
include('../objects/LanguageQuery.php');
include('../objects/FormatData.php');
include('../objects/DisplayHTML.php');


$testmode = 0;
if (isset($_REQUEST['testing']) && $_REQUEST['testing'] == "testing") {
    $testmode = 1;
} else {
    $testmode = 0;
}


$clientname = '';
$clientname = $_REQUEST['clientname'];

// Language Query
$languageQuery = new LanguageQuery();
$langArray = $languageQuery->getLanguageAndID();
$lang = $langArray[0];
$lang_id = $langArray[1];
$language = array();
$language_query_string = "select * from languages_dynamic ld, languages l, languages2 l2 where ld.lang_id =? and l.lang_id =? and l2.lang_id =?";
$language_query_statement = $db->prepare($language_query_string);
$language_query_statement->bind_param('iii', mysqli_real_escape_string($db, $lang_id), mysqli_real_escape_string($db, $lang_id), mysqli_real_escape_string($db, $lang_id));
if ($language_query_statement->execute()) {
    $result = $language_query_statement->get_result();
    while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
        $language = $row;
    }
}
$language_query_statement->close();

// Theme Query
$theme_query = mysql_query("select * from theme");
$theme = mysql_fetch_array($theme_query);

$email_link_url = '';

if ($language['bu_email_link_url'] != '') {
    $email_link_url = $language['bu_email_link_url'];
} else {
    $email_link_url = $language['vacancy_report_website'];
}




//setting locale for time, date, money, etc
date_default_timezone_set('America/New_York');
setlocale(LC_ALL, $language['locale_string']);
$timestamp = time();

function get_query_str() {
    if ($_SERVER['QUERY_STRING'] == "") {
        return "";
    } else {
        return '&' . $_SERVER['QUERY_STRING'];
    }
}

function get_query_str_print() {
    if ($_SERVER['QUERY_STRING'] == "") {
        return "";
    } else {
        return $_SERVER['QUERY_STRING'];
    }
}

function showRed($suiteleased, $value) {
    if ($suiteleased != 'true') {
        $redspan = '<span style="font-weight:bold;color:red;">' . $value . '</span>';
    } else {
        $redspan = $value;
    }
    return $redspan;
}

function translateProvince($province_selected) {
    $provTrans_query_string = "select french as fprovince from provinces where english = '" . $province_selected . "'";
    $provTrans_query = mysql_query($provTrans_query_string) or die("prov trans query error: " . mysql_error());
    $provTrans = mysql_fetch_array($provTrans_query);
    return $provTrans['fprovince'];
}

function translateType($SuiteType_selected) {
    $typeTrans_query_string = "SELECT building_type AS fsuitetype FROM building_types WHERE lang=0 AND sort_order = (SELECT sort_order FROM building_types WHERE building_type = '" . $SuiteType_selected . "' AND lang=1)";
    $typeTrans_query = mysql_query($typeTrans_query_string) or die("suitetype trans query error: " . mysql_error());
    $typeTrans = mysql_fetch_array($typeTrans_query);
    return $typeTrans['fsuitetype'];
}

function translateRegion($region_selected) {
    $regionTrans_query_string = "SELECT region AS fregion FROM buildings WHERE building_id = (SELECT building_id FROM buildings WHERE region = '" . $region_selected . "' AND lang = 1 LIMIT 1) AND lang=0";
    $regionTrans_query = mysql_query($regionTrans_query_string) or die("region trans query error: " . mysql_error());
    $regionTrans = mysql_fetch_array($regionTrans_query);
    return $regionTrans['fregion'];
}

function translateSubRegion($sub_region_selected) {
    $subregionTrans_query_string = "SELECT sub_region AS fsubregion FROM buildings WHERE building_id = (SELECT building_id FROM buildings WHERE sub_region = '" . $sub_region_selected . "' AND lang = 1 LIMIT 1) AND lang=0";
    $subregionTrans_query = mysql_query($subregionTrans_query_string) or die("subregion trans query error: " . mysql_error());
    $subregionTrans = mysql_fetch_array($subregionTrans_query);
    return $subregionTrans['fsubregion'];
}

function translateSize($size_selected) {
    $sizeTrans_query_string = "SELECT size_display AS fsize FROM sizes WHERE maximum = (SELECT maximum FROM sizes WHERE size_display = '" . $size_selected . "' AND lang=1) AND lang=0";
    $sizeTrans_query = mysql_query($sizeTrans_query_string) or die("size trans query error: " . mysql_error());
    $sizeTrans = mysql_fetch_array($sizeTrans_query);
    return $sizeTrans['fsize'];
}

function translateAvailability($availability_selected) {
    $availabilityTrans_query_string = "SELECT availabilities_display AS favailability FROM availabilities WHERE lang = 0 AND availability_index = (SELECT availability_index FROM availabilities WHERE availabilities_display = '" . $availability_selected . "' AND lang=1)";
    $availabilityTrans_query = mysql_query($availabilityTrans_query_string) or die("availability trans query error: " . mysql_error());
    $availabilityTrans = mysql_fetch_array($availabilityTrans_query);
    return $availabilityTrans['favailability'];
}

function buOnTheFlyTitle($province_selected, $SuiteType_selected, $region_selected, $sub_region_selected, $size_selected, $availability_selected, $lang) {

    $title_province = '';
    $title_type = '';
    $title_region = '';
    $title_subregion = '';
    $title_size = '';
    $title_availability = '';



    if ($province_selected != "All" && $province_selected != '') {
        $title_province = $province_selected;
        if ($lang == "fr_CA") {
            $title_province = translateProvince($province_selected);
        } else {
            $title_province = $province_selected;
        }
    }

    if ($SuiteType_selected != "All" && $SuiteType_selected != '') {
        if ($title_province != '') {
            $title_type .= " | ";
        }
        if ($lang == "fr_CA") {
            $title_type .= translateType($SuiteType_selected);
        } else {
            $title_type .= $SuiteType_selected;
        }
    }

    if ($region_selected != "All" && $region_selected != '') {
        if ($title_province != '') {
            $title_region .= " | ";
        }
        if ($lang == "fr_CA") {
            $title_region .= translateRegion($region_selected);
        } else {
            $title_region .= $region_selected;
        }
    }

    if ($sub_region_selected != "All" && $sub_region_selected != '') {
        if ($title_province != '') {
            $title_subregion .= " | ";
        }
        if ($lang == "fr_CA") {
            $title_subregion .= translateSubRegion($sub_region_selected);
        } else {
            $title_subregion .= $sub_region_selected;
        }
    }

    if ($size_selected != "All" && $size_selected != '') {
        if ($title_province != '') {
            $title_size .= " | ";
        }
        if ($lang == "fr_CA") {
            $title_size .= translateSize($size_selected);
        } else {
            $title_size .= $size_selected;
        }
    }

    if ($availability_selected != "All" && $availability_selected != '') {
        if ($title_province != '') {
            $title_availability .= " | ";
        }
        if ($lang == "fr_CA") {
            $title_availability .= translateAvailability($availability_selected);
        } else {
            $title_availability .= $availability_selected;
        }
    }

    $onthefly_title = $title_province . $title_type . $title_region . $title_subregion . $title_size . $title_availability;

    if ($onthefly_title == '') {
        if ($lang == "fr_CA") {
            $onthefly_title = "Toutes les inscriptions";
        } else {
            $onthefly_title = "All Listings";
        }
    }
    return $onthefly_title;
}

// get RGB numbers from hex colour
function getRGB($hex) {
    list($r, $g, $b) = sscanf($hex, "%02x%02x%02x");
    return $r . ", " . $g . ", " . $b;
}

// for reporting
$today = date("Y/m/d");

$recordDate = $today;

// Market Pulse BU Tracking
function updateMarketPulse($clientname, $building_code, $suite_id, $bu_reason, $mpSuiteArea, $suiteType, $bCity, $recordDate, $db) {
//    $mpQueryString = "replace into bu_records (clientname, building_code, suite_id, bu_reason, bu_sqft, bu_type, bu_city, recordDate) VALUES ('" . $clientname . "', '" . $building_code . "', '" . $suite_id . "', '" . $bu_reason . "', '" . $mpSuiteArea . "', '" . $suiteType . "', '" . $bCity . "', '" . $recordDate . "')";

    $mp_query = "replace into bu_records (clientname, building_code, suite_id, bu_reason, bu_sqft, bu_type, bu_city, recordDate) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
    $mp_statement = $db->prepare($mp_query);
    if ($mp_statement === false) {
        trigger_error('Wrong SQL: ' . $contactquery . ' Error: ' . $db->error, E_USER_ERROR);
    }
    //bind parameters for markers, where (s = string, i = integer, d = double,  b = blob)
    $mp_statement->bind_param('ssssssss', $clientname, $building_code, $suite_id, $bu_reason, $mpSuiteArea, $suiteType, $bCity, $recordDate);
    if ($mp_statement->execute()) {
        //        print 'Success! ID of last inserted record is : ' .$statement->insert_id .'<br />'; 
    } else {
        die('Error : (' . $db->errno . ') ' . $db->error);
    }
    $mp_statement->close();

//    $mpQuery = mysql_query($mpQueryString) or die("MP Query String error: " . mysql_error());
}

$bu_email = '';
// get submitted email address
if (isset($_REQUEST['email'])) {
    $bu_email = $_REQUEST['email'];
}
$bu_email_forquery = str_replace('+', '%2b', $bu_email);



$bu_freq = "Daily";
if (isset($_REQUEST['freq'])) {
    $bu_freq = $_REQUEST['freq'];
}


// need to switch the 'diff' table we're comparing against based on the frequency of the subscription.
$freq_diff_table = '';
$freq_yesterday_table = '';
if ($bu_freq == "Daily") {
    $freq_diff_table = "suites_diff";
    $freq_yesterday_table = "suites_yesterday";
} else if ($bu_freq == "Weekly") {
    $freq_diff_table = "suites_diff_lastweek";
    $freq_yesterday_table = "suites_lastweek";
} else if ($bu_freq == "Monthly") {
    $freq_diff_table = "suites_diff_lastmonth";
    $freq_yesterday_table = "suites_lastmonth";
}




$eol = "\n\r";
$eol2 = "\n\r\n\r";
$eolhtml = "<br>";
$eolhtml2 = "<br><br>";

$subscriptionsresults = '';




// BROKER UPDATES QUERY
//    $bu_query_string = "select * from bu_subscriptions where email = '" . $bu_email . "' and active='1' and subscription_frequency='" . $bu_freq . "' and subscription_language = '".$lang."'";
//    $bu_query = mysql_query($bu_query_string);

$bu_query = array();
$bu_query_string = "select * from bu_subscriptions where email =? and subscription_frequency=? and subscription_language =? and active='1'";
$bu_query_statement = $db->prepare($bu_query_string);
$bu_query_statement->bind_param('ssi', mysqli_real_escape_string($db, $bu_email), mysqli_real_escape_string($db, $bu_freq), mysqli_real_escape_string($db, $lang));
if ($bu_query_statement->execute()) {
    $result = $bu_query_statement->get_result();
    while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
        $bu_query[] = $row;
    }
}
$bu_query_statement->close();


//include("../assets/php/krumo/class.krumo.php");
//krumo($bu_query);
// START PER SUBSCRIPTION LOOP
foreach ($bu_query as $bu) {

    $subscription_title = $bu['title'];
    $subscription_email = $bu['email'];
    $subscription_querystring = $bu['bu_query_string'];
    $subscription_language = '';
    $subscription_unique_id = $bu['unique_id'];

    unset($buildingdriver);
    unset($suitedriver);

    // put subscription's querystring into an array so we can use it
    parse_str($subscription_querystring, $querystring);

    if (isset($querystring['province'])) {
        $province_selected = $querystring['province'];
    } else {
        if (isset($querystring['lang']) && $querystring['lang'] == 'fr_CA') {
            $province_selected = $default_province_frc;
        } else {
            $province_selected = $default_province_eng;
        }
    }

    if (isset($querystring['SuiteType'])) {
        $SuiteType_selected = $querystring['SuiteType'];
    } else {
        $SuiteType_selected = "All";
    }

    if (isset($querystring['region'])) {
        $region_selected = $querystring['region'];
    } else {
        $region_selected = "All";
    }

    if (isset($querystring['sub-region'])) {
        $sub_region_selected = $querystring['sub-region'];
    } else {
        $sub_region_selected = "All";
    }

    if (isset($querystring['Size'])) {
        $size_selected = $querystring['Size'];
    } else {
        $size_selected = "All";
    }


    if (isset($querystring['Availability'])) {
        $availability_selected = $querystring['Availability'];

        switch ($availability_selected) {
            case 'All':
                $availability_notafter = 2147483640;
                $availability_notbefore = 0;
                break;
            case 'Immediately':
                $availability_notafter = time();
                $availability_notbefore = 1;
                break;
            case 'Under 3 Months':
                $availability_notafter = strtotime('+3 Month');
                $availability_notbefore = 1;
                break;
            case '3-6 Months':
                $availability_notafter = strtotime('+6 Month');
                $availability_notbefore = strtotime('+3 Month');
                break;
            case '6-12 Months':
                $availability_notafter = strtotime('+1 Year');
                $availability_notbefore = strtotime('+6 Month');
                break;
            case 'Over 12 Months':
                $availability_notafter = 2147483639;
                $availability_notbefore = strtotime('+1 Year');
                break;
            case 'Tous':
                $availability_notafter = 2147483640;
                $availability_notbefore = 0;
                break;
            case 'Immédiatement':
                $availability_notafter = time();
                $availability_notbefore = 1;
                break;
            case 'Moins de 3 mois':
                $availability_notafter = strtotime('+3 Month');
                $availability_notbefore = 1;
                break;
            case '3-6 mois':
                $availability_notafter = strtotime('+6 Month');
                $availability_notbefore = strtotime('+3 Month');
                break;
            case '6-12 mois':
                $availability_notafter = strtotime('+1 Year');
                $availability_notbefore = strtotime('+6 Month');
                break;
            case 'Plus de 12 mois':
                $availability_notafter = 2147483639;
                $availability_notbefore = strtotime('+1 Year');
                break;
        }
    } else {
        $availability_notafter = 2147483640;
        $availability_notbefore = 0;
    }



    // BUILDING QUERY STRING
    $buildings_query_string = '';
    $buildings_query_string_add = '';

    $buildings_query_string .= "select distinct allb.*, alls.* from buildings allb INNER JOIN suites alls USING (building_code) WHERE allb.building_code IN (SELECT building_code FROM " . $freq_diff_table . " where record_source = 'today' and lang=" . $lang_id . ") AND alls.suite_id IN (SELECT suite_id FROM " . $freq_diff_table . " where record_source = 'today' and lang=" . $lang_id . " AND bu_flag='true') AND allb.lang=" . $lang_id . " and alls.lang=" . $lang_id . " and alls.building_code = allb.building_code ";

    if ($SuiteType_selected != "All" && $SuiteType_selected != "Tous") {
        $buildings_query_string_add .= " and alls.suite_type='" . $SuiteType_selected . "' ";
    }
    if ($province_selected != "All" && $province_selected != "Tous") {
        $buildings_query_string_add .= " and allb.province='" . $province_selected . "' ";
    }
    if ($region_selected != "All" && $region_selected != "Tous") {
        $buildings_query_string_add .= " and allb.region='" . mysql_real_escape_string($region_selected) . "' ";
    }
    if ($sub_region_selected != "All" && $sub_region_selected != "Tous") {
        $buildings_query_string_add .= " and allb.sub_region='" . mysql_real_escape_string($sub_region_selected) . "' ";
    }

    switch ($size_selected) {
        case '2,500 and Under':
            $buildings_query_string_add .= " and (alls.contiguous_area < 2501 or alls.net_rentable_area < 2501) ";
            break;
        case '2,500 - 5,000':
            $buildings_query_string_add .= " and (alls.contiguous_area > 2499 or alls.net_rentable_area > 2499) and (alls.contiguous_area < 5001 or alls.net_rentable_area < 5001) ";
            break;
        case '5,000 - 10,000':
            $buildings_query_string_add .= " and (alls.contiguous_area > 4999 or alls.net_rentable_area > 4999) and (alls.contiguous_area < 10001 or alls.net_rentable_area < 10001) ";
            break;
        case '10,000 - 20,000':
            $buildings_query_string_add .= " and (alls.contiguous_area > 9999 or alls.net_rentable_area > 9999) and (alls.contiguous_area < 20001 or alls.net_rentable_area < 20001) ";
            break;
        case '20,000 - 40,000':
            $buildings_query_string_add .= " and (alls.contiguous_area > 19999 or alls.net_rentable_area > 19999) and (alls.contiguous_area < 40001 or alls.net_rentable_area < 40001) ";
            break;
        case '40,000 and Above':
            $buildings_query_string_add .= " and (alls.contiguous_area > 39999 or alls.net_rentable_area > 39999) ";
            break;
    }

    $buildings_query_string_add .= " and alls.availability > " . $availability_notbefore . " and alls.availability < " . $availability_notafter;

    $buildings_query_string_add .= " order by allb.country asc, allb.province asc, allb.city asc, allb.sort_streetname asc, allb.sort_streetnumber asc, alls.suite_name desc";


    $buildings_query_string = $buildings_query_string . $buildings_query_string_add;
    $buildings_query = mysql_query($buildings_query_string) or die("buildings query error: " . mysql_error());
    $one_result = false;
    $output = '';
    $suitedifftop = '';
    $suitediff = '';

    unset($buildingdriver);
    unset($suitedriver);

    $buildingtitle = '';
    $buildingdrivertemp = array();
    $buildingdriver = array();
    $suitedriver = array();
    $initial_suites_count = mysql_num_rows($buildings_query);




    $there_yesterday_buildings_query_string = "SELECT DISTINCT 
        sd.building_code AS privatesuite_building_code,
        sd.suite_id AS privatesuite_id 
        FROM
        " . $freq_diff_table . " sd,
        buildings allb,
        " . $freq_yesterday_table . " sy 
        WHERE sd.suite_id NOT IN
        (SELECT suite_id FROM suites) 
        AND allb.building_code = sd.building_code 
        AND sy.suite_id = sd.suite_id";


    $there_yesterday_buildings_query_string = $there_yesterday_buildings_query_string . str_replace('alls', 'sy', $buildings_query_string_add);

    $there_yesterday_buildings_query = mysql_query($there_yesterday_buildings_query_string) or die("there yesterday query error: " . mysql_error());
    $there_yesterday_count = mysql_num_rows($there_yesterday_buildings_query);
    $initial_suites_count = $initial_suites_count + $there_yesterday_count;




    $colouredbackground = getRGB(str_replace("#", "", $theme['secondary_colour_hex']));


    // BUILD SUBSCRIPTION TABS
    $subscription_tab_html = '<h3 style="text-transform: uppercase;"><a href="' . $email_link_url . '/?' . $bu['bu_query_string'] . '">' . buOnTheFlyTitle($province_selected, $SuiteType_selected, $region_selected, $sub_region_selected, $size_selected, $availability_selected, $lang) . '</a>' . '</h3><hr />';
    // start per building loop
    if ($initial_suites_count > 0) {
        while ($driver = mysql_fetch_array($buildings_query)) {
            array_push($buildingdrivertemp, $driver['building_code']);
            array_push($suitedriver, $driver['suite_id']);
        }


        // need to look for and count up any suites that have 'yesterday' records but no 'today' records. 
        // need to add their building codes to the main driver here before we search for dupes.
        if ($there_yesterday_count > 0) {
            while ($there_yesterday_buildings = mysql_fetch_array($there_yesterday_buildings_query)) {
                array_push($buildingdrivertemp, $there_yesterday_buildings['privatesuite_building_code']);
                array_push($suitedriver, $there_yesterday_buildings['privatesuite_id']);
            }
        }

        // remove doubles from buildingdriver array. we only want unique building codes.
        $buildingdriver = array_unique($buildingdrivertemp);


        $marketingchange = 0;
        $realchangecount = 0;
        $realsuitescount = 0;

        foreach ($buildingdriver as $code_building) {
            $marketingchange = 0;
            $realchangecount = 0;
            $realsuitescount = 0;
            $nobuilding = 0;
            $suitedifftop1 = '';
            $suitedifftop2 = '';
            $suitediffbottom = '';
            $suitediffbottomcontacts = '';
            $realbuildingsquery_string = "select * from buildings b where b.lang = '" . $lang_id . "' and b.building_code = '" . $code_building . "'";
            $realbuildingsquery = mysql_query($realbuildingsquery_string);

            $building_specs_query_string = "select website from building_specifications where building_code='" . $code_building . "' and lang = " . $lang_id . "";
            $building_specs_query = mysql_query($building_specs_query_string);
            $building_specs = mysql_fetch_array($building_specs_query);

            while ($Building = mysql_fetch_array($realbuildingsquery)) {
                $building_type = $Building['building_type'];
                $bCity = $Building['city'];

                $suite_count_query_string = "select * from suites where building_code = '" . $Building['building_code'] . "' and lang = '" . $lang_id . "'";
                $suite_count_query = mysql_query($suite_count_query_string);
                $thisbuilding_suites_count = mysql_num_rows($suite_count_query);

                // Contacts Query
                $contacts_query_string = "Select * from contacts where building_code = '" . $Building['building_code'] . "' and type ='LEASING' and lang ='" . $lang_id . "' order by priority asc";
                $contacts_query = mysql_query($contacts_query_string);

                $suitedifftop1 .= '<div style="background-color:#EEF0F2"><table width="100%" class="suitedifftable" style="cellspacing:0;cellpadding:0;border-spacing:0;padding:5px;">' . $eol;
                $suitedifftop1 .= '<tr><td colspan="8" style="margin:0;padding:5px;font-weight:bold;">' . $eol;

                if ($theme['result_links_to_corp_website'] == 1) {
                    $suitedifftop1 .= '<a target="_blank" href="' . $building_specs['website'] . '">' . $Building['building_name'] . '</a><br /><span style="font-weight:normal;color:#888888;">' . $Building['street_address'] . ', ' . $Building['city'] . ', ' . $Building['province'] . '</span>' . $eol;
                } else {
                    $suitedifftop1 .= '<a target="_blank" href="' . $email_link_url . '/building.php?building=' . $Building['building_code'] . '&lang=' . $lang . '">' . $Building['building_name'] . '</a><br /><span style="font-weight:normal;color:#888888;">' . $Building['street_address'] . ', ' . $Building['city'] . ', ' . $Building['province'] . '</span>' . $eol;
                }



                $suitedifftop1 .= '</td></tr>' . $eol;






                // suitedifftop2 contains the suite table header row
                $suitedifftop2 .= '<tr class="bu_headerrow" style="background-color:#fff;">' . $eol;
                $suitedifftop2 .= '<td width=50px style="margin:0;padding:5px;font-weight:bold;">' . $language['brokerupdates_table_header_status'] . '</td>' . $eol;
                $suitedifftop2 .= '<td style="margin:0;padding:5px;font-weight:bold;">' . $language['brokerupdates_table_header_suite_text'] . '</td>' . $eol;
                $suitedifftop2 .= '<td style="margin:0;padding:5px;font-weight:bold;">' . $language['brokerupdates_table_header_area_text'] . '<br> (' . $language['brokerupdates_table_header_min_divisible_text'] . ' | ' . $language['brokerupdates_table_header_contig_area_text'] . ')</td>' . $eol;
                $suitedifftop2 .= '<td style="margin:0;padding:5px;font-weight:bold;">' . $language['brokerupdates_table_header_availability_text'] . '</td>' . $eol;
                $suitedifftop2 .= '<td style="margin:0;padding:5px;font-weight:bold;">' . $language['brokerupdates_table_header_add_rent_total_text'] . '</td>' . $eol;
                $suitedifftop2 .= '<td style="margin:0;padding:5px;font-weight:bold;">' . $language['brokerupdates_table_header_net_rent_text'] . '</td>' . $eol;
                if ($building_type == "Industrial" || $building_type == "Industriel") {
                    $suitedifftop2 .= '<td style="margin:0;padding:5px;font-weight:bold;">' . $language['ceiling_text'] . '</td>' . $eol;
                    $suitedifftop2 .= '<td style="margin:0;padding:5px;font-weight:bold;">' . $language['loading_text'] . '</td>' . $eol;
                    $suitedifftop2 .= '<td style="margin:0;padding:5px;font-weight:bold;">' . $language['power_text'] . '</td>' . $eol;
                }
                $suitedifftop2 .= '</tr>' . $eol;

                $changecount = 0;
                $changedsuite = 0;
                $newsuite = 0;
                $marketingchange = 0;
                $marketingtext = 0;


                $bu_marketing_reason = '';
                $suitechanges_thisbuilding = 0;
                $suitediffshowallsuites = '';


                foreach ($suitedriver as $code_suite) {
                    $changecount = 0;
                    $bu_reason = '';


                    $suites_query_string = "SELECT sd.suite_id, sd.lang, sd.building_code, sd.building_id, s.suite_name, s.suite_type, sd.net_rentable_area, sd.contiguous_area, sd.availability, s.notice_period, sd.net_rent, sd.add_rent_total, s.pdf_plan, sd.leased, s.promoted, sd.record_date, sd.record_source, s.description FROM " . $freq_diff_table . " sd, suites s WHERE sd.building_code = '" . $Building['building_code'] . "' and s.building_code = '" . $Building['building_code'] . "' and sd.lang = '" . $lang_id . "' and s.lang = '" . $lang_id . "' and sd.suite_id = '" . $code_suite . "' and s.suite_id = '" . $code_suite . "' and sd.record_source = 'today' and s.record_source = 'today'";
                    $suites_query = mysql_query($suites_query_string) or die("Suites query error: " . mysql_error());
                    $suitescount = mysql_num_rows($suites_query);
                    $private_suites_query_string = "SELECT distinct sd.suite_id, sd.lang, sd.building_code, s.suite_name, sd.net_rentable_area, sd.contiguous_area, sd.availability, s.notice_period, sd.net_rent, sd.add_rent_total, s.pdf_plan, sd.leased, s.promoted, sd.record_date, sd.record_source, s.description FROM " . $freq_diff_table . " sd, " . $freq_yesterday_table . " s, suites su WHERE sd.building_code = '" . $Building['building_code'] . "' and s.building_code = '" . $Building['building_code'] . "' and sd.lang = '" . $lang_id . "' and s.lang = '" . $lang_id . "' and sd.suite_id = '" . $code_suite . "' and s.suite_id = '" . $code_suite . "' AND sd.suite_id NOT IN (SELECT suite_id FROM suites WHERE lang = '" . $lang_id . "'AND record_source = 'today')";
                    $private_suites_query = mysql_query($private_suites_query_string) or die("Private suites query error: " . mysql_error());
                    $private_suitescount = mysql_num_rows($private_suites_query);

                    if ($suitescount == 0 && $private_suitescount > 0) {
                        $suites_query = $private_suites_query;
                    }

                    $leasedsuite = 0;


                    while ($Suites = mysql_fetch_array($suites_query)) {
                        $bu_reason = '';
                        $yesterday_suites_query_string = "SELECT * FROM " . $freq_diff_table . " sd WHERE sd.lang = '" . $lang_id . "' and sd.suite_id = '" . $Suites['suite_id'] . "' and sd.record_source = 'yesterday'";
                        $yesterday_suites_query = mysql_query($yesterday_suites_query_string) or die("Yesterday Suites query error: " . mysql_error());
                        $yesterday_suites = mysql_fetch_array($yesterday_suites_query);
                        //
                        // ####################
                        // Diffing queries
                        // ####################
                        $mktgchange = 0;
                        //// Broker Announcements / Marketing Text
                        //
                            //                            
                        //
                            $marketing_text_output = '';
                        $marketing_text_today = '';
                        $marketing_text_yesterday = '';
                        $marketing_removed = 0;
                        $bu_marketing_new = 0;
                        $bu_marketing_change = 0;
                        $onlymktg = 0;
                        $suite_notice_period = 0;

                        $suite_marketing_text_query_string = "				
                            SELECT suites.suite_id, suites.suite_name, suites.marketing_text
                            FROM suites
                            JOIN " . $freq_yesterday_table . " ON suites.suite_id = " . $freq_yesterday_table . ".suite_id
                            WHERE suites.marketing_text <> " . $freq_yesterday_table . ".marketing_text AND " . $freq_yesterday_table . ".building_id	= '" . $Building['building_id'] . "'
                            AND " . $freq_yesterday_table . ".suite_id	= '" . $code_suite . "' and suites.lang = '" . $lang_id . "' and " . $freq_yesterday_table . ".lang = '" . $lang_id . "'
                            ";
                        $suite_marketing_text_query = mysql_query($suite_marketing_text_query_string) or die("suite mktg text query error: " . mysql_error());
                        $suite_marketing_text_change = mysql_fetch_array($suite_marketing_text_query);
                        $marketing_text = $suite_marketing_text_change['marketing_text'];
                        $suite_marketing_text_count = mysql_num_rows($suite_marketing_text_query);

                        if ($suite_marketing_text_count > 0) {
                            if ($marketing_text == '') {
                                if ($yesterday_suites['marketing_text'] != '') {
                                    $changecount = $changecount + 1;
                                    $mktgchange = $mktgchange + 1;
                                    $onlymktg = 1;
                                    $bu_marketing_reason = $language['expired_text'];
                                    $marketing_removed = 1;
                                } else {
                                    
                                }
                            } else {
                                if ($yesterday_suites['marketing_text'] != '') {
                                    $changecount = $changecount + 1;
                                    $onlymktg = 1;
                                    $mktgchange = $mktgchange + 1;
                                    $bu_marketing_reason = $language['update_text'];
                                    $bu_marketing_change = 1;
                                } else {
                                    $changecount = $changecount + 1;
                                    $onlymktg = 1;
                                    $mktgchange = $mktgchange + 1;
                                    $bu_marketing_reason = $language['new_text'];
                                    $bu_marketing_new = 1;
                                }
                            }
                        }
//                            

                        $suite_net_rentable_query_string = "	
                            SELECT suites.suite_id, suites.suite_name, suites.net_rentable_area
                            FROM suites
                            JOIN " . $freq_yesterday_table . " ON suites.suite_id = " . $freq_yesterday_table . ".suite_id
                            WHERE suites.net_rentable_area <> " . $freq_yesterday_table . ".net_rentable_area AND " . $freq_yesterday_table . ".building_id	= '" . $Building['building_id'] . "' 
                            AND " . $freq_yesterday_table . ".suite_id	= '" . $code_suite . "'
                            ";
                        $suite_net_rentable_query = mysql_query($suite_net_rentable_query_string) or die("net rentable query error: " . mysql_error());
                        $suite_net_rentable_change = mysql_fetch_array($suite_net_rentable_query);
                        $net_rentable_area = $suite_net_rentable_change['net_rentable_area'];
                        $suite_net_rentable_count = mysql_num_rows($suite_net_rentable_query);
                        if ($suite_net_rentable_count > 0) {
                            $changecount = $changecount + 1;
                            $onlymktg = 0;
                            $suitechanges_thisbuilding = $suitechanges_thisbuilding + 1;
                            $bu_reason = $language['update_text'];
                        }

                        if ($net_rentable_area != '') {
                            $compare_with_contig = $net_rentable_area;
                        } else {
                            $compare_with_contig = $yesterday_suites['net_rentable_area'];
                        }

                        //confirm if the $compare_with_contig is empty. Then if it is empty, do not include it in a query.
                        $add_contig_query = '';
                        $contiguous_removed = 0;

                        $suite_contiguous_area_query_string = "				
                            SELECT suites.suite_id, suites.suite_name, suites.contiguous_area
                            FROM suites
                            JOIN " . $freq_yesterday_table . " ON suites.suite_id = " . $freq_yesterday_table . ".suite_id
                            WHERE suites.contiguous_area <> " . $freq_yesterday_table . ".contiguous_area AND " . $freq_yesterday_table . ".building_id	= '" . $Building['building_id'] . "'
                            AND " . $freq_yesterday_table . ".suite_id	= '" . $code_suite . "'
                            ";

                        $suite_contiguous_area_query = mysql_query($suite_contiguous_area_query_string) or die("suite contig query error: " . mysql_error());
                        $suite_contiguous_area_change = mysql_fetch_array($suite_contiguous_area_query);
                        $contiguous_area = $suite_contiguous_area_change['contiguous_area'];
                        $suite_contiguous_area_count = mysql_num_rows($suite_contiguous_area_query);
                        if ($suite_contiguous_area_count > 0) {
                            if ($contiguous_area == 0) {
                                if ($yesterday_suites['contiguous_area'] != 0) {
                                    $changecount = $changecount + 1;
                                    $onlymktg = 0;
                                    $suitechanges_thisbuilding = $suitechanges_thisbuilding + 1;
                                    $bu_reason = $language['update_text'];
                                    $contiguous_removed = 1;
                                } else {
                                    
                                }
                            } else {
                                $changecount = $changecount + 1;
                                $onlymktg = 0;
                                $suitechanges_thisbuilding = $suitechanges_thisbuilding + 1;
                                $bu_reason = $language['update_text'];
                            }
                        }


                        $min_divisible_removed = 0;

                        $suite_mindiv_area_query_string = "				
                            SELECT suites.suite_id, suites.suite_name, suites.min_divisible_area
                            FROM suites
                            JOIN " . $freq_yesterday_table . " ON suites.suite_id = " . $freq_yesterday_table . ".suite_id
                            WHERE suites.min_divisible_area <> " . $freq_yesterday_table . ".min_divisible_area AND " . $freq_yesterday_table . ".building_id	= '" . $Building['building_id'] . "'
                            AND " . $freq_yesterday_table . ".suite_id	= '" . $code_suite . "'
                            ";
//echo $suite_mindiv_area_query_string . "<br><br>";
                        $suite_mindiv_area_query = mysql_query($suite_mindiv_area_query_string) or die("suite min divisible area query error: " . mysql_error());
                        $suite_mindiv_area_change = mysql_fetch_array($suite_mindiv_area_query);
                        $min_divisible_area = $suite_mindiv_area_change['min_divisible_area'];
                        $suite_min_divisible_area_count = mysql_num_rows($suite_mindiv_area_query);
                        if ($suite_min_divisible_area_count > 0) {
                            if ($min_divisible_area == 0) {
                                if ($yesterday_suites['min_divisible_area'] != 0) {
                                    $changecount = $changecount + 1;
                                    $onlymktg = 0;
                                    $suitechanges_thisbuilding = $suitechanges_thisbuilding + 1;
                                    $bu_reason = $language['update_text'];
                                    $min_divisible_removed = 1;
                                } else {
                                    
                                }
                            } else {
                                $changecount = $changecount + 1;
                                $onlymktg = 0;
                                $suitechanges_thisbuilding = $suitechanges_thisbuilding + 1;
                                $bu_reason = $language['update_text'];
                            }
                        }













                        // notice period trumps anything to do with Availability. Just show the notice period.
                        // DO not track changes as of Nov 18/2015.
//                            if ($Suites['notice_period'] != "no" && $Suites['notice_period'] != '') {
//                                
//                                $suite_availability = $Suites['notice_period'];
//                                $suite_notice_period = 1;
//                                
//                            } else {
                        // begin tracking notice period changes: Feb 2017

                        $suite_availability_query_string = "				
                                SELECT suites.suite_id, suites.suite_name, suites.availability, suites.notice_period
                                FROM suites
                                JOIN " . $freq_yesterday_table . " ON suites.suite_id = " . $freq_yesterday_table . ".suite_id
                                WHERE ((" . $freq_yesterday_table . ".availability < " . $timestamp . " AND suites.availability > " . $timestamp . ")
                                OR (suites.availability  < " . $timestamp . " AND " . $freq_yesterday_table . ".availability > " . $timestamp . ")
                                OR (suites.availability  > " . $timestamp . " AND " . $freq_yesterday_table . ".availability > " . $timestamp . "))
                                AND suites.availability <> " . $freq_yesterday_table . ".availability
                                AND " . $freq_yesterday_table . ".building_id= '" . $Building['building_id'] . "'
                                AND " . $freq_yesterday_table . ".suite_id= '" . $code_suite . "' AND suites.notice_period NOT IN ('30d', '60d', '90d', '120d')";

                        $suite_availability_query = mysql_query($suite_availability_query_string) or die("suite availability query error: " . mysql_error());
                        $suite_availability_change = mysql_fetch_array($suite_availability_query);
                        $availability = $suite_availability_change['availability'];
                        $suite_availability_count = mysql_num_rows($suite_availability_query);
                        if ($suite_availability_count > 0) {
                            $changecount = $changecount + 1;
                            $onlymktg = 0;
                            $suitechanges_thisbuilding = $suitechanges_thisbuilding + 1;
                            $bu_reason = $language['update_text'];
                        }

                        $suite_availability_raw = $availability;

                        if ($suite_availability_raw != "") {
                            //make old dates show as 'immediately'
                            if ($suite_availability_raw < time()) {
                                //$suite_availability = "Immediate";
                                $suite_availability = $language['immediately_text'];
                            } else {
                                //show the actual availability date - its in the future
                                $suite_availability = strftime('%b %Y', $suite_availability_raw);
                            }
                        } else {
                            $suite_availability = $language['immediately_text'];
                        }
                        $nochange_suite_availability_raw = $Suites['availability'];

                        if ($nochange_suite_availability_raw != "") {
                            //make old dates show as 'immediately'
                            if ($nochange_suite_availability_raw < time()) {
                                $nochange_suite_availability = $language['immediately_text'];
                            } else {
                                //show the actual availability date - its in the future
                                $nochange_suite_availability = strftime('%b %Y', $nochange_suite_availability_raw);
                            }
                        } else {
                            $nochange_suite_availability = $language['immediately_text'];
                        }

//                            }

                        $suite_notice_period_query_string = "				
                            SELECT suites.suite_id, suites.suite_name, suites.notice_period
                            FROM suites
                            JOIN " . $freq_yesterday_table . " ON suites.suite_id = " . $freq_yesterday_table . ".suite_id
                            WHERE suites.notice_period <> " . $freq_yesterday_table . ".notice_period AND " . $freq_yesterday_table . ".building_id	= '" . $Building['building_id'] . "'
                            AND " . $freq_yesterday_table . ".suite_id	= '" . $code_suite . "'
                            ";
                        $suite_notice_period_query = mysql_query($suite_notice_period_query_string) or die("suite addrenttotal query error: " . mysql_error());
                        $suite_notice_period_change = mysql_fetch_array($suite_notice_period_query);
                        $suite_notice_period = $suite_notice_period_change['notice_period'];
                        $suite_notice_period_count = mysql_num_rows($suite_notice_period_query);
                        if ($suite_notice_period_count > 0) {
                            $changecount = $changecount + 1;
                            $onlymktg = 0;
                            $suitechanges_thisbuilding = $suitechanges_thisbuilding + 1;
                            $bu_reason = $language['update_text'];
                        }





                        $suite_add_rent_total_query_string = "				
                            SELECT suites.suite_id, suites.suite_name, suites.add_rent_total
                            FROM suites
                            JOIN " . $freq_yesterday_table . " ON suites.suite_id = " . $freq_yesterday_table . ".suite_id
                            WHERE suites.add_rent_total <> " . $freq_yesterday_table . ".add_rent_total AND " . $freq_yesterday_table . ".building_id	= '" . $Building['building_id'] . "'
                            AND " . $freq_yesterday_table . ".suite_id	= '" . $code_suite . "'
                            ";
                        $suite_add_rent_total_query = mysql_query($suite_add_rent_total_query_string) or die("suite addrenttotal query error: " . mysql_error());
                        $suite_add_rent_total_change = mysql_fetch_array($suite_add_rent_total_query);
                        $add_rent_total = $suite_add_rent_total_change['add_rent_total'];
                        $suite_add_rent_total_count = mysql_num_rows($suite_add_rent_total_query);
                        if ($suite_add_rent_total_count > 0) {
                            $changecount = $changecount + 1;
                            $onlymktg = 0;
                            $suitechanges_thisbuilding = $suitechanges_thisbuilding + 1;
                            $bu_reason = $language['update_text'];
                        }

                        $suite_net_rent_query_string = "				
                            SELECT suites.suite_id, suites.suite_name, suites.net_rent
                            FROM suites
                            JOIN " . $freq_yesterday_table . " ON suites.suite_id = " . $freq_yesterday_table . ".suite_id
                            WHERE suites.net_rent <> " . $freq_yesterday_table . ".net_rent AND " . $freq_yesterday_table . ".building_id	= '" . $Building['building_id'] . "'
                            AND " . $freq_yesterday_table . ".suite_id	= '" . $code_suite . "'
                            ";
                        $suite_net_rent_query = mysql_query($suite_net_rent_query_string) or die("suite net rent query error: " . mysql_error());
                        $suite_net_rent_change = mysql_fetch_array($suite_net_rent_query);
                        $net_rent = $suite_net_rent_change['net_rent'];
                        $suite_net_rent_count = mysql_num_rows($suite_net_rent_query);
                        if ($suite_net_rent_count > 0) {
                            $changecount = $changecount + 1;
                            $onlymktg = 0;
                            $suitechanges_thisbuilding = $suitechanges_thisbuilding + 1;
                            $bu_reason = $language['update_text'];
                        }

                        $suite_notnew = '';
                        $suite_new_flag_query_string = "				
                            SELECT suites.suite_id, suites.suite_name, suites.new
                            FROM suites
                            JOIN " . $freq_yesterday_table . " ON suites.suite_id = " . $freq_yesterday_table . ".suite_id
                            WHERE suites.new <> " . $freq_yesterday_table . ".new AND " . $freq_yesterday_table . ".building_id = '" . $Building['building_id'] . "'
                            AND " . $freq_yesterday_table . ".suite_id = '" . $code_suite . "'
                            ";

                        $suite_new_flag_query = mysql_query($suite_new_flag_query_string) or die("suite new flag query error: " . mysql_error());
                        $suite_new_flag_change = mysql_fetch_array($suite_new_flag_query);
                        $new_flag = $suite_new_flag_change['new'];
                        $suite_new_flag_count = mysql_num_rows($suite_new_flag_query);
                        if ($suite_new_flag_count > 0) {
                            if ($new_flag != 'false') {
//                                    echo "changed<br>";
                                $changecount = $changecount + 1;
                                $onlymktg = 0;
                                $suitechanges_thisbuilding = $suitechanges_thisbuilding + 1;
                                $bu_reason = $language['new_text'];
                            } else {
                                $suite_notnew = 1;
                            }
                        }

                        // private suite? set changecount and call it leased
                        if ($private_suitescount > 0) {
                            $changecount = $changecount + 1;
                            $onlymktg = 0;
                            $suitechanges_thisbuilding = $suitechanges_thisbuilding + 1;
                            $bu_reason = $language['leased_text'];
                        }



                        $leasedsuite = 0;
                        $reallyleased = 0;

                        $suite_leased_flag_query_string = "				
                            SELECT suites.suite_id, suites.suite_name, suites.leased
                            FROM suites
                            JOIN " . $freq_yesterday_table . " ON suites.suite_id = " . $freq_yesterday_table . ".suite_id
                            WHERE suites.leased <> " . $freq_yesterday_table . ".leased AND " . $freq_yesterday_table . ".building_id	= '" . $Building['building_id'] . "'
                            AND " . $freq_yesterday_table . ".suite_id	= '" . $code_suite . "'
                            ";

                        $suite_leased_flag_query = mysql_query($suite_leased_flag_query_string) or die("suite leased flag query error: " . mysql_error());
                        $suite_leased_flag_change = mysql_fetch_array($suite_leased_flag_query);
                        $leased_flag = $suite_leased_flag_change['leased'];
                        $suite_leased_flag_count = mysql_num_rows($suite_leased_flag_query);
                        if ($suite_leased_flag_count > 0) {
                            if ($leased_flag != 'false') {
                                $changecount = $changecount + 1;
                                $onlymktg = 0;
                                $suitechanges_thisbuilding = $suitechanges_thisbuilding + 1;
                                $leasedsuite = 1;
                                $bu_reason = $language['leased_text'];
                            } else if ($leased_flag != 'true') {
                                $leasedsuite = 0;
                                $suite_leased_flag_count = 0;
                            }
                        }

                        if ($Suites['leased'] == "true" && $yesterday_suites['leased'] == "true") {
                            $reallyleased = 1;
                            // do not increment $changecount -- this is not a change.
                            // This is an already-leased suite and we do not report changes for leased suites
                            // $reallyleased = 1 will skip all output
                            // $changecount not being incremented will skip all output below the tests.
                            // It's like this suite doesn't exist.
                        }




                        $promotedsuite = 0;
                        $reallypromoted = 0;

                        $suite_promoted_flag_query_string = "				
                            SELECT suites.suite_id, suites.suite_name, suites.promoted
                            FROM suites
                            JOIN " . $freq_yesterday_table . " ON suites.suite_id = " . $freq_yesterday_table . ".suite_id
                            WHERE suites.promoted <> " . $freq_yesterday_table . ".promoted AND " . $freq_yesterday_table . ".building_id	= '" . $Building['building_id'] . "'
                            AND " . $freq_yesterday_table . ".suite_id	= '" . $code_suite . "'
                            ";

                        $suite_promoted_flag_query = mysql_query($suite_promoted_flag_query_string) or die("suite promoted flag query error: " . mysql_error());
                        $suite_promoted_flag_change = mysql_fetch_array($suite_promoted_flag_query);
                        $promoted_flag = $suite_promoted_flag_change['promoted'];
                        $suite_promoted_flag_count = mysql_num_rows($suite_promoted_flag_query);
                        if ($suite_promoted_flag_count > 0) {
                            if ($promoted_flag != 'false') {
                                $changecount = $changecount + 1;
                                $onlymktg = 0;
                                $suitechanges_thisbuilding = $suitechanges_thisbuilding + 1;
                                $promotedsuite = 1;
                                $bu_reason = $language['promoted_text'];
                            } else if ($promoted_flag != 'true') {
                                $promotedsuite = 0;
                                $suite_promoted_flag_count = 0;
                            }
                        }

                        if ($Suites['promoted'] == "false" && $yesterday_suites['promoted'] == "true") {
                            $reallypromoted = 1;
                            // do not increment $changecount -- this is not a change.
                            // This is a suite that has disabled Promoted status and this change should not be reported.
                            // $reallypromoted = 1 will skip all output so we don't want to use that down at line 1041 in case there are other changes
                            // $changecount not being incremented will skip all output below the tests.
                            // It's like this change didn't happen.
                        }



                        // ####################
                        // END diffing queries
                        // ####################


                        $unchanged_suite_area_noformat = '';
                        $unchanged_suite_area = '';
                        $changed_suite_area_noformat = '';
                        $changed_suite_area = '';
                        $unchanged_suite_contiguous_area_noformat = '';
                        $unchanged_suite_contiguous_area = '';
                        $changed_suite_contiguous_area_noformat = '';
                        $changed_suite_contiguous_area = '';
                        $unchanged_suite_min_divisible_area_noformat = '';
                        $unchanged_suite_min_divisible_area = '';
                        $changed_suite_min_divisible_area_noformat = '';
                        $changed_suite_min_divisible_area = '';


                        if ($building_type == "Industrial" || $building_type == "Industriel") {
                            $availability_padding = "margin:0;padding:7px;";
                            $availability_colspan = "9";
                            $marketing_colspan = "9";
                            $marketingtext_colspan = "8";
                        } else {
                            $availability_padding = "margin:0;padding:10px;";
                            $availability_colspan = "5";
                            $marketing_colspan = "6";
                            $marketingtext_colspan = "5";
                        }

                        if ($changecount > 0 && $reallyleased == 0 && $reallypromoted == 0) {

                            if ($changecount == $mktgchange) { // nov18
                                // increment changed suite but do nothing else
                                // only marketing changes for this property
                                $changedsuite++;

                                // Set up English vs French sqft formatting
                                if ($lang == "en_CA") {
                                    if ($yesterday_suites['net_rentable_area'] != '' && $yesterday_suites['net_rentable_area'] != '0') {
                                        $unchanged_suite_area_noformat = $yesterday_suites['net_rentable_area'];
                                        $unchanged_suite_area = number_format($unchanged_suite_area_noformat);
                                    }
                                    if ($net_rentable_area != '' && $net_rentable_area != '0') {
                                        $changed_suite_area_noformat = $net_rentable_area;
                                        $changed_suite_area = number_format($changed_suite_area_noformat);
                                    }
                                    if ($yesterday_suites['contiguous_area'] != '' && $yesterday_suites['contiguous_area'] != '0') {
                                        $unchanged_suite_contiguous_area_noformat = $yesterday_suites['contiguous_area'];
                                        $unchanged_suite_contiguous_area = number_format($unchanged_suite_contiguous_area_noformat);
                                    }
                                    if ($contiguous_area != '' && $contiguous_area != '0') {
                                        $changed_suite_contiguous_area_noformat = $contiguous_area;
                                        $changed_suite_contiguous_area = number_format($changed_suite_contiguous_area_noformat);
                                    }
                                    if ($yesterday_suites['min_divisible_area'] != '' && $yesterday_suites['min_divisible_area'] != '0') {
                                        $unchanged_suite_min_divisible_area_noformat = $yesterday_suites['min_divisible_area'];
                                        $unchanged_suite_min_divisible_area = number_format($unchanged_suite_min_divisible_area_noformat);
                                    }
                                    if ($min_divisible_area != '' && $min_divisible_area != '0') {
                                        $changed_suite_min_divisible_area_noformat = $min_divisible_area;
                                        $changed_suite_min_divisible_area = number_format($changed_suite_min_divisible_area_noformat);
                                    }
                                }
                                if ($lang == "fr_CA") {
                                    if ($yesterday_suites['net_rentable_area'] != '' && $yesterday_suites['net_rentable_area'] != '0') {
                                        $unchanged_suite_area_noformat = $yesterday_suites['net_rentable_area'];
                                        $unchanged_suite_area = number_format($unchanged_suite_area_noformat, 0, ',', ' ');
                                    }
                                    if ($net_rentable_area != '' && $net_rentable_area != '0') {
                                        $changed_suite_area_noformat = $net_rentable_area;
                                        $changed_suite_area = number_format($changed_suite_area_noformat, 0, ',', ' ');
                                    }
                                    if ($yesterday_suites['contiguous_area'] != '' && $yesterday_suites['contiguous_area'] != '0') {
                                        $unchanged_suite_contiguous_area_noformat = $yesterday_suites['contiguous_area'];
                                        $unchanged_suite_contiguous_area = number_format($unchanged_suite_contiguous_area_noformat, 0, ',', ' ');
                                    }
                                    if ($contiguous_area != '' && $contiguous_area != '0') {
                                        $changed_suite_contiguous_area_noformat = $contiguous_area;
                                        $changed_suite_contiguous_area = number_format($changed_suite_contiguous_area_noformat, 0, ',', ' ');
                                    }
                                    if ($yesterday_suites['min_divisible_area'] != '' && $yesterday_suites['min_divisible_area'] != '0') {
                                        $unchanged_suite_min_divisible_area_noformat = $yesterday_suites['min_divisible_area'];
                                        $unchanged_suite_min_divisible_area = number_format($unchanged_suite_min_divisible_area_noformat, 0, ',', ' ');
                                    }
                                    if ($min_divisible_area != '' && $min_divisible_area != '0') {
                                        $changed_suite_min_divisible_area_noformat = $min_divisible_area;
                                        $changed_suite_min_divisible_area = number_format($changed_suite_min_divisible_area_noformat, 0, ',', ' ');
                                    }
                                }



                                if ($suite_leased_flag_count > 0 || $private_suitescount > 0) {
                                    $suitediffshowallsuites .= '<tr style="background-color:#fff;text-decoration:line-through;">';
                                } else if ($Suites['promoted'] == "true") {
                                    $suitediffshowallsuites .= '<tr style="background-color:#fff;font-weight:bold;font-style:italic;">';
                                } else {
                                    $suitediffshowallsuites .= '<tr style="background-color:#fff;">';
                                }
                                if ($bu_reason == $language['new_text']) {
                                    $suitediffshowallsuites .= '<td style="' . $availability_padding . 'color:red;text-align:center;"><span style="margin:10px;padding:5px;border:1px solid red;">';
                                } else {
                                    $suitediffshowallsuites .= '<td style="' . $availability_padding . 'text-align:center;">';
                                }




                                $suitediffshowallsuites .= $bu_reason;
                                $suitediffshowallsuites .= '</td>' . $eol;
                                $suitediffshowallsuites .= '<td style="margin:0;padding:10px;">';
                                if ($theme['result_links_to_corp_website'] == 1) {
                                    $suitediffshowallsuites .= '<a rel="changedsuite">' . $Suites['suite_name'] . '</a>';
                                } else if ($suite_leased_flag_count > 0 || $private_suitescount > 0) {
                                    $suitediffshowallsuites .= '<a rel="changedsuite" href="">' . $Suites['suite_name'] . '</a>';
                                } else {
                                    $suitediffshowallsuites .= '<a rel="changedsuite" href="' . $email_link_url . '/suite.php?suiteid=' . $Suites['suite_id'] . '&building=' . $Building['building_code'] . '&lang=' . $lang . '" target="_blank">' . $Suites['suite_name'] . '</a>';
                                } // END if/else $theme['result_links_to_corp_website'] == 1

                                $suitediffshowallsuites .= '</td>' . $eol;


                                $suiteleased = $Suites['leased'];


//                                    // area output
//                                    $areaoutput1 = '';
//                                    $areaoutput1 .= '<td style="'.$availability_padding.'"> ';
//                                    
//                                    // net rentable area
//                                    if ($suite_net_rentable_count > 0) {
//                                        $areaoutput1 .= showRed($suiteleased, $changed_suite_area);
//                                    } else if ($suite_net_rentable_count == 0) {
//                                        $areaoutput1 .= $unchanged_suite_area;
//                                    }
//                                    
//                                    
//                                    
//                                    
//                                    
//                                    if (($suite_min_divisible_area_count > 0) || ($suite_contiguous_area_count > 0)) {
//                                        $areaoutput1 .= '<br>';
//                                        $areaoutput1 .= '(';
//                                    }
//                                    
//                                    // min divisible area
//                                    if ($suite_min_divisible_area_count > 0) {
//                                        if ($min_divisible_removed == 1) {
//                                            $areaoutput1 .= '<span style="color:red;"><span style="text-decoration: line-through;"> ' . showRed($suiteleased, $unchanged_suite_min_divisible_area) . '</span>';
//                                        } else {
//                                            $areaoutput1 .=  ($changed_suite_min_divisible_area != '' && $changed_suite_min_divisible_area != '0' ? showRed($suiteleased, $changed_suite_min_divisible_area) : ' ');
//                                        }
//                                    } else if ($suite_min_divisible_area_count == 0) {
//                                        $areaoutput1 .= $unchanged_suite_min_divisible_area;
//                                    }
//                                    
//                                    
//                                    
//                                    // divider between mindiv and contig
//                                    // do not show if both are not present
//                                    if (($suite_min_divisible_area_count > 0) && ($suite_contiguous_area_count > 0)) {
//                                        $areaoutput1 .= ' | ';
//                                    }
//                                    
//                                    
//                                    
//                                    // max contiguous area
//                                    if ($suite_contiguous_area_count > 0) {
//                                        if ($contiguous_removed == 1) {
//                                            $areaoutput1 .= '<span style="color:red;"><span style="text-decoration: line-through;"> ' . showRed($suiteleased, $unchanged_suite_contiguous_area) . '</span>';
//                                        } else {
//                                            $areaoutput1 .=  ($changed_suite_contiguous_area != '' && $changed_suite_contiguous_area != '0' ? showRed($suiteleased, $changed_suite_contiguous_area) : ' ');
//                                        }
//                                    } else if ($suite_contiguous_area_count == 0) {
//                                        $areaoutput1 .= $unchanged_suite_contiguous_area;
//                                    }
//
//                                    if (($suite_min_divisible_area_count > 0) || ($suite_contiguous_area_count > 0)) {
//                                        $areaoutput1 .= ')';
//                                    }
//                                    
//                                    $areaoutput1 .= ' ' . $squarefoot . '</td>' . $eol;
//                                    // end area output
//                                    
//                                    
//                                    $suitediffshowallsuites .= $areaoutput1;
                                // area output
                                $areaoutput1 = '';
                                $areaoutput1 .= '<td data-unchangedcontig="' . $unchanged_suite_contiguous_area . '" data-unchangedmindivt="' . $unchanged_suite_min_divisible_area . '" style="' . $availability_padding . '"> ';

                                // net rentable area
                                if ($suite_net_rentable_count > 0) {
                                    $areaoutput1 .= showRed($suiteleased, $changed_suite_area);
                                } else if ($suite_net_rentable_count == 0) {
                                    $areaoutput1 .= $unchanged_suite_area;
                                }

                                // min divisible area
                                if (($suite_min_divisible_area_count > 0) || ($suite_contiguous_area_count > 0) || ($unchanged_suite_contiguous_area != '' && $unchanged_suite_contiguous_area != 0) || ($unchanged_suite_min_divisible_area != '' && $unchanged_suite_min_divisible_area != 0)) {
                                    $areaoutput1 .= '<br>';
                                    $areaoutput1 .= '(';
                                }

                                if ($suite_min_divisible_area_count > 0) {
                                    if ($min_divisible_removed == 1) {
                                        $areaoutput1 .= '<span style="color:red;"><span data-mindiv="' . $unchanged_suite_min_divisible_area . '" style="text-decoration: line-through;">' . showRed($suiteleased, $unchanged_suite_min_divisible_area) . '</span></span>';
                                    } else {
                                        $areaoutput1 .= ($changed_suite_min_divisible_area != '' && $changed_suite_min_divisible_area != '0' ? showRed($suiteleased, $changed_suite_min_divisible_area) : ' ');
                                    }
                                } else if ($suite_min_divisible_area_count == 0) {
                                    $areaoutput1 .= $unchanged_suite_min_divisible_area;
                                }

                                // divider between mindiv and contig
                                // do not show if both are not present
                                if ((($suite_min_divisible_area_count > 0) && ($suite_contiguous_area_count > 0)) || (($unchanged_suite_contiguous_area != '' && $unchanged_suite_contiguous_area != 0) && ($unchanged_suite_min_divisible_area != '' && $unchanged_suite_min_divisible_area != 0))) {
                                    $areaoutput1 .= ' | ';
                                }

                                // max contiguous area
                                if ($suite_contiguous_area_count > 0) {
                                    if ($contiguous_removed == 1) {
                                        $areaoutput1 .= '<span style="color:red;"><span style="text-decoration: line-through;"> ' . showRed($suiteleased, $unchanged_suite_contiguous_area) . '</span></span>';
                                    } else {
                                        $areaoutput1 .= ($changed_suite_contiguous_area != '' && $changed_suite_contiguous_area != '0' ? showRed($suiteleased, $changed_suite_contiguous_area) : ' ');
                                    }
                                } else if ($suite_contiguous_area_count == 0) {
                                    $areaoutput1 .= $unchanged_suite_contiguous_area;
                                }

                                if (($suite_min_divisible_area_count > 0) || ($suite_contiguous_area_count > 0) || ($unchanged_suite_contiguous_area != '' && $unchanged_suite_contiguous_area != 0) || ($unchanged_suite_min_divisible_area != '' && $unchanged_suite_min_divisible_area != 0)) {
                                    $areaoutput1 .= ')';
                                }

                                $areaoutput1 .= ' ' . $squarefoot . '</td>' . $eol;
                                // end area output


                                $suitediffbottom .= $areaoutput1;



//                                    if (($suite_net_rentable_count > 0) && ($suite_contiguous_area_count > 0) && ($suite_min_divisible_area_count > 0)) {
//                                                         if ($contiguous_removed == 1) {
//                                            $suitediffshowallsuites .= '<td style="'.$availability_padding.'"> ( <span style="color:red;"><span style="text-decoration: line-through;"> ' . showRed($suiteleased, $unchanged_suite_contiguous_area) . ' </span></span> ) ' . $squarefoot . '</td>' . $eol;
//                                        } else {
//                                            $suitediffshowallsuites .= '<td style="'.$availability_padding.'">' . showRed($suiteleased, $changed_suite_area) . ($changed_suite_contiguous_area != '' && $changed_suite_contiguous_area != '0' ? ' ( ' . showRed($suiteleased, $changed_suite_contiguous_area) . ' ) ' : ' ') . $squarefoot . '</td>' . $eol;
////                                            $suitediffshowallsuites .= '<td style="'.$availability_padding.'">' . showRed($suiteleased, $changed_suite_area) . ($changed_suite_min_divisible_area != '' && $changed_suite_min_divisible_area != '0' ? ' <br>( ' . showRed($suiteleased, $changed_suite_min_divisible_area) .  ' | ' : ' ') . ($changed_suite_contiguous_area != '' && $changed_suite_contiguous_area != '0' ? '' . showRed($suiteleased, $changed_suite_contiguous_area) . ' ) ' : ' ') . $squarefoot . '</td>' . $eol;
//                                        }   
//                                    }
//
//                                    if (($suite_net_rentable_count == 0) && ($suite_contiguous_area_count == 0) && ($suite_min_divisible_area_count == 0)) {
//                                     $suitediffshowallsuites .= '<td style="'.$availability_padding.'">' . $unchanged_suite_area . ($unchanged_suite_contiguous_area != '' && $unchanged_suite_contiguous_area != '0' ? ' (' . $unchanged_suite_contiguous_area . ') ' : ' ') . $squarefoot . '</td>' . $eol;
////                                        $suitediffshowallsuites .= '<td style="'.$availability_padding.'">' . $unchanged_suite_area . ($unchanged_suite_min_divisible_area != '' && $unchanged_suite_min_divisible_area != '0' ? ' (' . $unchanged_suite_min_divisible_area . ' | ' : ' ') . ($unchanged_suite_min_divisible_area != '' && $unchanged_suite_min_divisible_area != '0' ? ' (' . $unchanged_suite_min_divisible_area . ' | ' : ' ') . ($unchanged_suite_contiguous_area != '' && $unchanged_suite_contiguous_area != '0' ? $unchanged_suite_contiguous_area . ') ' : ' ') . $squarefoot . '</td>' . $eol;
//                                    }
//
//
//                                    if (($suite_net_rentable_count == 0) && ($suite_contiguous_area_count > 0) && ($suite_min_divisible_area_count == 0)) {
//                                        if ($contiguous_removed == 1) {
//                                            $suitediffshowallsuites .= '<td style="'.$availability_padding.'">' . $unchanged_suite_area . ' ( <span style="color:red;"><span style="text-decoration: line-through;"> ' . showRed($suiteleased, $unchanged_suite_contiguous_area) . ' </span></span> ) ' . $squarefoot . '</td>' . $eol;
//                                        } else {
//                                            $suitediffshowallsuites .= '<td style="'.$availability_padding.'">' . $unchanged_suite_area . ($changed_suite_contiguous_area != '' && $changed_suite_contiguous_area != '0' ? ' ( ' . showRed($suiteleased, $changed_suite_contiguous_area) . ' ) ' : ' ') . $squarefoot . '</td>' . $eol;
////                                            $suitediffshowallsuites .= '<td style="'.$availability_padding.'">' . $unchanged_suite_area . ($changed_suite_contiguous_area != '' && $changed_suite_contiguous_area != '0' ? ' ( ' . showRed($suiteleased, $changed_suite_contiguous_area) . ' ) ' : ' ') . $squarefoot . '</td>' . $eol;
//                                        }
//                                    }
//
//
//
//                                    if (($suite_net_rentable_count > 0) && ($suite_contiguous_area_count == 0)) {
//                                        $suitediffshowallsuites .= '<td style="'.$availability_padding.'">' . showRed($suiteleased, $changed_suite_area) . ($unchanged_suite_contiguous_area != '' && $unchanged_suite_contiguous_area != '0' ? ' ( ' . $unchanged_suite_contiguous_area . ' ) ' : ' ') . $squarefoot . '</td>' . $eol;
//                                    }




                                if ($suite_notice_period != 0) {

                                    $suitediffshowallsuites .= '<td class="spk3" style="margin:0;padding:10px;">' . $Suites['notice_period'] . '</td>' . $eol;
                                } else {
                                    if (($suite_availability_count > 0) && ($availability != '')) {
                                        $suitediffshowallsuites .= '<td class="spk4" style="' . $availability_padding . '">' . showRed($suiteleased, $suite_availability) . '</td>' . $eol;
                                    } else {
                                        $suitediffshowallsuites .= '<td class="spk5" style="' . $availability_padding . '">' . $nochange_suite_availability . '</td>' . $eol;
                                    }
                                }





                                if (($suite_add_rent_total_count > 0) && ($add_rent_total != '')) {
                                    $suitediffshowallsuites .= '<td style="' . $availability_padding . '">' . showRed($suiteleased, money_format('%.2n', (double) $add_rent_total)) . '</td>' . $eol;
                                } else {
                                    $suitediffshowallsuites .= '<td style="' . $availability_padding . '">' . money_format('%.2n', (double) $Suites['add_rent_total']) . '</td>' . $eol;
                                }

                                if ($net_rent == "0.00" || $net_rent == "" || $net_rent == "$0.00") {
                                    $net_rent_output = $language['negotiable_text'];
                                } else {
                                    $net_rent_output = money_format('%.2n', (double) $net_rent);
                                }

                                if ($Suites['net_rent'] == "0.00" || $Suites['net_rent'] == "" || $Suites['net_rent'] == "$0.00") {
                                    $suites_net_rent_output = $language['negotiable_text'];
                                } else {
                                    $suites_net_rent_output = money_format('%.2n', (double) $Suites['net_rent']);
                                }



                                if (($suite_net_rent_count > 0)) {
                                    $suitediffshowallsuites .= '<td style="' . $availability_padding . '">' . showRed($suiteleased, $net_rent_output) . '</td>' . $eol;
                                } else {
                                    $suitediffshowallsuites .= '<td style="' . $availability_padding . '">' . $suites_net_rent_output . '</td>' . $eol;
                                }


                                if ($building_type == "Industrial" || $building_type == "Industriel") {


                                    $suite_specs_query_string = "Select clear_height, shipping_doors_drive_in, shipping_doors_truck, available_electrical_volts, available_electrical_amps from suites_specifications where suite_id = '" . $Suites['suite_id'] . "'";
                                    $suite_specs_query = mysql_query($suite_specs_query_string) or die("suite specs query error: " . mysql_error());
                                    $suite_specs = mysql_fetch_array($suite_specs_query);

                                    $shippingDI = $suite_specs['shipping_doors_drive_in'];
                                    $shippingTL = $suite_specs['shipping_doors_truck'];
                                    $poweramps = $suite_specs['available_electrical_amps'];
                                    $powervolts = $suite_specs['available_electrical_volts'];
                                    $ceiling = $suite_specs['clear_height'];
                                    $loadingoutputDI = '';
                                    $loadingoutputTL = '';
                                    $loadingoutput = '';

                                    if ($shippingDI != '' && $shippingDI != 0) {
                                        $loadingoutputDI = $shippingDI . " " . $language['drivein_loading_abbreviation'];
                                    }
                                    if ($shippingTL != '' && $shippingTL != 0) {
                                        $loadingoutputTL = $shippingTL . " " . $language['truck_loading_abbreviation'];
                                    }
                                    if ($loadingoutputDI != '' && $loadingoutputTL != '') {
                                        $loadingoutput = $loadingoutputDI . "<br />" . $loadingoutputTL;
                                    } else if ($loadingoutputDI != '' && $loadingoutputTL == '') {
                                        $loadingoutput = $loadingoutputDI;
                                    } else if ($loadingoutputDI == '' && $loadingoutputTL != '') {
                                        $loadingoutput = $loadingoutputTL;
                                    } else {
                                        // both are empty leave a space
                                        $loadingoutput = "&nbsp;";
                                    }

                                    $poweroutputAmps = '';
                                    $poweroutputVolts = '';
                                    if ($poweramps != '' && $poweramps != 0) {
                                        $poweroutputAmps = $poweramps . " A";
                                    }
                                    if ($powervolts != '' && $powervolts != 0) {
                                        $poweroutputVolts = $powervolts . " V";
                                    }
                                    if ($poweroutputAmps != '' && $poweroutputVolts != '') {
                                        $poweroutput = $poweroutputAmps . "<br />" . $poweroutputVolts;
                                    } else if ($poweroutputAmps != '' && $poweroutputVolts == '') {
                                        $poweroutput = $poweroutputAmps;
                                    } else if ($poweroutputAmps == '' && $poweroutputVolts != '') {
                                        $poweroutput = $poweroutputVolts;
                                    } else {
                                        // both are empty leave a space
                                        $poweroutput = "&nbsp;";
                                    }


                                    $suitediffshowallsuites .= '<td style="' . $availability_padding . '">' . $suite_specs['clear_height'] . '</td>' . $eol;
                                    $suitediffshowallsuites .= '<td style="' . $availability_padding . '">' . $loadingoutput . '</td>' . $eol;
                                    $suitediffshowallsuites .= '<td style="' . $availability_padding . '">' . $poweroutput . '</td>' . $eol;
                                    $suitediffshowallsuites .= '</tr>' . $eol;
                                } else {
                                    $suitediffshowallsuites .= '</tr>' . $eol;
                                }


                                $suitediffshowallsuites .= '<tr style="background-color:#fff;">';



                                $suitediffshowallsuites .= '<td>&nbsp;</td>';


                                if ($building_type == "Industrial" || $building_type == "Industriel") {
                                    $notes_colspan = $availability_colspan - 1;
                                } else {
                                    $notes_colspan = $availability_colspan;
                                }



                                if ($suite_leased_flag_count > 0 || $private_suitescount > 0) {
                                    $suitediffshowallsuites .= '<td colspan="' . $notes_colspan . '" style="margin:0;padding: 0px 0px 0px 10px;text-decoration:line-through;">';
                                } else if ($Suites['promoted'] == "true") {
                                    $suitediffshowallsuites .= '<td colspan="' . $notes_colspan . '" style="margin:0;padding: 0px 0px 0px 10px;font-weight:bold;font-style:italic;">';
                                } else {
                                    $suitediffshowallsuites .= '<td colspan="' . $notes_colspan . '" style="margin:0;padding: 0px 0px 0px 10px">';
                                }

                                if ($Suites['promoted'] == "true") {
                                    $suitediffshowallsuites .= '<span style="margin-right:5px;text-transform:uppercase;">' . $language['promoted_suite_text'] . '</span>';
                                }

                                $suitediffshowallsuites .= $Suites['description'] . '</td>' . $eol;
                                $suitediffshowallsuites .= "</tr>" . $eol;
                            } else {



                                // Output changed suite
                                $changedsuite++;


                                // Set up English vs French sqft formatting
                                if ($lang == "en_CA") {
                                    if ($yesterday_suites['net_rentable_area'] != '' && $yesterday_suites['net_rentable_area'] != '0') {
                                        $unchanged_suite_area_noformat = $yesterday_suites['net_rentable_area'];
                                        $unchanged_suite_area = number_format($unchanged_suite_area_noformat);
                                    }
                                    if ($net_rentable_area != '' && $net_rentable_area != '0') {
                                        $changed_suite_area_noformat = $net_rentable_area;
                                        $changed_suite_area = number_format($changed_suite_area_noformat);
                                    }
                                    if ($yesterday_suites['contiguous_area'] != '' && $yesterday_suites['contiguous_area'] != '0') {
                                        $unchanged_suite_contiguous_area_noformat = $yesterday_suites['contiguous_area'];
                                        $unchanged_suite_contiguous_area = number_format($unchanged_suite_contiguous_area_noformat);
                                    }
                                    if ($contiguous_area != '' && $contiguous_area != '0') {
                                        $changed_suite_contiguous_area_noformat = $contiguous_area;
                                        $changed_suite_contiguous_area = number_format($changed_suite_contiguous_area_noformat);
                                    }
                                    if ($yesterday_suites['min_divisible_area'] != '' && $yesterday_suites['min_divisible_area'] != '0') {
                                        $unchanged_suite_min_divisible_area_noformat = $yesterday_suites['min_divisible_area'];
                                        $unchanged_suite_min_divisible_area = number_format($unchanged_suite_min_divisible_area_noformat);
                                    }
                                    if ($min_divisible_area != '' && $min_divisible_area != '0') {
                                        $changed_suite_min_divisible_area_noformat = $min_divisible_area;
                                        $changed_suite_min_divisible_area = number_format($changed_suite_min_divisible_area_noformat);
                                    }
                                }
                                if ($lang == "fr_CA") {
                                    if ($yesterday_suites['net_rentable_area'] != '' && $yesterday_suites['net_rentable_area'] != '0') {
                                        $unchanged_suite_area_noformat = $yesterday_suites['net_rentable_area'];
                                        $unchanged_suite_area = number_format($unchanged_suite_area_noformat, 0, ',', ' ');
                                    }
                                    if ($net_rentable_area != '' && $net_rentable_area != '0') {
                                        $changed_suite_area_noformat = $net_rentable_area;
                                        $changed_suite_area = number_format($changed_suite_area_noformat, 0, ',', ' ');
                                    }
                                    if ($yesterday_suites['contiguous_area'] != '' && $yesterday_suites['contiguous_area'] != '0') {
                                        $unchanged_suite_contiguous_area_noformat = $yesterday_suites['contiguous_area'];
                                        $unchanged_suite_contiguous_area = number_format($unchanged_suite_contiguous_area_noformat, 0, ',', ' ');
                                    }
                                    if ($contiguous_area != '' && $contiguous_area != '0') {
                                        $changed_suite_contiguous_area_noformat = $contiguous_area;
                                        $changed_suite_contiguous_area = number_format($changed_suite_contiguous_area_noformat, 0, ',', ' ');
                                    }
                                    if ($yesterday_suites['min_divisible_area'] != '' && $yesterday_suites['min_divisible_area'] != '0') {
                                        $unchanged_suite_min_divisible_area_noformat = $yesterday_suites['min_divisible_area'];
                                        $unchanged_suite_min_divisible_area = number_format($unchanged_suite_min_divisible_area_noformat, 0, ',', ' ');
                                    }
                                    if ($min_divisible_area != '' && $min_divisible_area != '0') {
                                        $changed_suite_min_divisible_area_noformat = $min_divisible_area;
                                        $changed_suite_min_divisible_area = number_format($changed_suite_min_divisible_area_noformat, 0, ',', ' ');
                                    }
                                }

                                $suite_twodee_query_string = "Select * from files_2d where suite_id ='" . $code_suite . "'";
                                $suite_twodee_query = mysql_query($suite_twodee_query_string);
                                $suite_twodee = mysql_fetch_array($suite_twodee_query);
                                $twodee_count = mysql_num_rows($suite_twodee_query);

                                $suite_threedee_query_string = "Select * from files_3d where suite_id ='" . $code_suite . "'";
                                $suite_threedee_query = mysql_query($suite_threedee_query_string);
                                $suite_threedee = mysql_fetch_array($suite_threedee_query);
                                $threedee_count = mysql_num_rows($suite_threedee_query);

                                if ($suite_leased_flag_count > 0 || $private_suitescount > 0) {
                                    $suitediffbottom .= '<tr style="background-color:#fff;text-decoration:line-through;">';
                                } else if ($Suites['promoted'] == "true") {
                                    $suitediffbottom .= '<tr style="background-color:#fff;font-weight:bold;font-style:italic;">';
                                } else {
                                    $suitediffbottom .= '<tr style="background-color:#fff;">';
                                }
                                if ($bu_reason == $language['new_text']) {
//                                    if ($bu_reason != '') {
                                    $suitediffbottom .= '<td style="' . $availability_padding . 'color:red;text-align:center;"><span style="margin:10px;padding:5px;border:1px solid red;">';
                                } else {
                                    $suitediffbottom .= '<td style="' . $availability_padding . 'text-align:center;">';
                                }

                                $suitediffbottom .= $bu_reason;
                                $suitediffbottom .= '</td>' . $eol;
                                $suitediffbottom .= '<td style="' . $availability_padding . '">';
                                if ($theme['result_links_to_corp_website'] == 1) {
                                    $suitediffbottom .= '<a rel="changedsuite">' . $Suites['suite_name'] . '</a>';
                                } else if ($suite_leased_flag_count > 0 || $private_suitescount > 0) {
                                    $suitediffbottom .= '<a rel="changedsuite" href="">' . $Suites['suite_name'] . '</a>';
                                } else {
                                    $suitediffbottom .= '<a rel="changedsuite" href="' . $email_link_url . '/suite.php?suiteid=' . $Suites['suite_id'] . '&building=' . $Building['building_code'] . '&lang=' . $lang . '" target="_blank">' . $Suites['suite_name'] . '</a>';
                                } // END if/else $theme['result_links_to_corp_website'] == 1

                                $suitediffbottom .= '</td>' . $eol;


                                $suiteleased = $Suites['leased'];



                                // area output
                                $areaoutput2 = '';
                                $areaoutput2 .= '<td data-unchangedcontig="' . $unchanged_suite_contiguous_area . '" data-unchangedmindivt="' . $unchanged_suite_min_divisible_area . '" style="' . $availability_padding . '"> ';

                                // net rentable area
                                if ($suite_net_rentable_count > 0) {
                                    $areaoutput2 .= showRed($suiteleased, $changed_suite_area);
                                } else if ($suite_net_rentable_count == 0) {
                                    $areaoutput2 .= $unchanged_suite_area;
                                }

                                // min divisible area
                                if (($suite_min_divisible_area_count > 0) || ($suite_contiguous_area_count > 0) || ($unchanged_suite_contiguous_area != '' && $unchanged_suite_contiguous_area != 0) || ($unchanged_suite_min_divisible_area != '' && $unchanged_suite_min_divisible_area != 0)) {
                                    $areaoutput2 .= '<br>';
                                    $areaoutput2 .= '(';
                                }

                                if ($suite_min_divisible_area_count > 0) {
                                    if ($min_divisible_removed == 1) {
                                        $areaoutput2 .= '<span style="color:red;"><span data-mindiv="' . $unchanged_suite_min_divisible_area . '" style="text-decoration: line-through;">' . showRed($suiteleased, $unchanged_suite_min_divisible_area) . '</span></span>';
                                    } else {
                                        $areaoutput2 .= ($changed_suite_min_divisible_area != '' && $changed_suite_min_divisible_area != '0' ? showRed($suiteleased, $changed_suite_min_divisible_area) : ' ');
                                    }
                                } else if ($suite_min_divisible_area_count == 0) {
                                    $areaoutput2 .= $unchanged_suite_min_divisible_area;
                                }

                                // divider between mindiv and contig
                                // do not show if both are not present
                                if ((($suite_min_divisible_area_count > 0) && ($suite_contiguous_area_count > 0)) || (($unchanged_suite_contiguous_area != '' && $unchanged_suite_contiguous_area != 0) && ($unchanged_suite_min_divisible_area != '' && $unchanged_suite_min_divisible_area != 0))) {
                                    $areaoutput2 .= ' | ';
                                }

                                // max contiguous area
                                if ($suite_contiguous_area_count > 0) {
                                    if ($contiguous_removed == 1) {
                                        $areaoutput2 .= '<span style="color:red;"><span style="text-decoration: line-through;"> ' . showRed($suiteleased, $unchanged_suite_contiguous_area) . '</span></span>';
                                    } else {
                                        $areaoutput2 .= ($changed_suite_contiguous_area != '' && $changed_suite_contiguous_area != '0' ? showRed($suiteleased, $changed_suite_contiguous_area) : ' ');
                                    }
                                } else if ($suite_contiguous_area_count == 0) {
                                    $areaoutput2 .= $unchanged_suite_contiguous_area;
                                }

                                if (($suite_min_divisible_area_count > 0) || ($suite_contiguous_area_count > 0) || ($unchanged_suite_contiguous_area != '' && $unchanged_suite_contiguous_area != 0) || ($unchanged_suite_min_divisible_area != '' && $unchanged_suite_min_divisible_area != 0)) {
                                    $areaoutput2 .= ')';
                                }

                                $areaoutput2 .= ' ' . $squarefoot . '</td>' . $eol;
                                // end area output


                                $suitediffbottom .= $areaoutput2;


                                // previous area output
                                // before adding min divisible area
                                // Feb 2017
//                                    if (($suite_net_rentable_count > 0) && ($suite_contiguous_area_count > 0)) {
//                                        if ($contiguous_removed == 1) {
//                                            $suitediffbottom .= '<td style="'.$availability_padding.'"> ( <span style="color:red;"><span style="text-decoration: line-through;"> ' . showRed($suiteleased, $unchanged_suite_contiguous_area) . ' </span></span> ) ' . $squarefoot . '</td>' . $eol;
//                                        } else {
//                                            $suitediffbottom .= '<td style="'.$availability_padding.'">' . showRed($suiteleased, $changed_suite_area) . ($changed_suite_contiguous_area != '' && $changed_suite_contiguous_area != '0' ? ' ( ' . showRed($suiteleased, $changed_suite_contiguous_area) . ' ) ' : ' ') . $squarefoot . '</td>' . $eol;
//                                        }   
//                                    }
//
//                                    if (($suite_net_rentable_count == 0) && ($suite_contiguous_area_count == 0)) {
//                                        $suitediffbottom .= '<td style="'.$availability_padding.'">' . $unchanged_suite_area . ($unchanged_suite_contiguous_area != '' && $unchanged_suite_contiguous_area != '0' ? ' (' . $unchanged_suite_contiguous_area . ') ' : ' ') . $squarefoot . '</td>' . $eol;
//                                    }
//
//                                    if (($suite_net_rentable_count == 0) && ($suite_contiguous_area_count > 0)) {
//                                        if ($contiguous_removed == 1) {
//                                            $suitediffbottom .= '<td style="'.$availability_padding.'">' . $unchanged_suite_area . ' ( <span style="color:red;"><span style="text-decoration: line-through;"> ' . showRed($suiteleased, $unchanged_suite_contiguous_area) . ' </span></span> ) ' . $squarefoot . '</td>' . $eol;
//                                        } else {
//                                            $suitediffbottom .= '<td style="'.$availability_padding.'">' . $unchanged_suite_area . ($changed_suite_contiguous_area != '' && $changed_suite_contiguous_area != '0' ? ' ( ' . showRed($suiteleased, $changed_suite_contiguous_area) . ' ) ' : ' ') . $squarefoot . '</td>' . $eol;
//                                        }
//                                    }
//
//                                    if (($suite_net_rentable_count > 0) && ($suite_contiguous_area_count == 0)) {
//                                        $suitediffbottom .= '<td style="'.$availability_padding.'">' . showRed($suiteleased, $changed_suite_area) . ($unchanged_suite_contiguous_area != '' && $unchanged_suite_contiguous_area != '0' ? ' ( ' . $unchanged_suite_contiguous_area . ' ) ' : ' ') . $squarefoot . '</td>' . $eol;
//                                    }


                                if ($Suites['notice_period'] != 0) {
                                    if ($suite_notice_period_count > 0) {
                                        $suitediffbottom .= '<td class="spk1" style="' . $availability_padding . '">' . showRed($suiteleased, $Suites['notice_period']) . '</td>' . $eol;
                                    } else {
                                        $suitediffbottom .= '<td class="spk2" style="' . $availability_padding . '">' . $Suites['notice_period'] . '</td>' . $eol;
                                    }
                                } else {
                                    if (($suite_availability_count > 0) && ($availability != '')) {
                                        $suitediffbottom .= '<td class="spk6" style="' . $availability_padding . '">' . showRed($suiteleased, $suite_availability) . '</td>' . $eol;
                                    } else {
                                        $suitediffbottom .= '<td class="spk7" style="' . $availability_padding . '">' . $nochange_suite_availability . '</td>' . $eol;
                                    }
                                }


                                if (($suite_add_rent_total_count > 0) && ($add_rent_total != '')) {
                                    $suitediffbottom .= '<td style="' . $availability_padding . '">' . showRed($suiteleased, money_format('%.2n', (double) $add_rent_total)) . '</td>' . $eol;
                                } else {
                                    $suitediffbottom .= '<td style="' . $availability_padding . '">' . money_format('%.2n', (double) $Suites['add_rent_total']) . '</td>' . $eol;
                                }

                                if ($net_rent == "0.00" || $net_rent == "" || $net_rent == "$0.00") {
                                    $net_rent_output = $language['negotiable_text'];
                                } else {
                                    $net_rent_output = money_format('%.2n', (double) $net_rent);
                                }

                                if ($Suites['net_rent'] == "0.00" || $Suites['net_rent'] == "" || $Suites['net_rent'] == "$0.00") {
                                    $suites_net_rent_output = $language['negotiable_text'];
                                } else {
                                    $suites_net_rent_output = money_format('%.2n', (double) $Suites['net_rent']);
                                }



                                if (($suite_net_rent_count > 0)) {
                                    $suitediffbottom .= '<td style="' . $availability_padding . '">' . showRed($suiteleased, $net_rent_output) . '</td>' . $eol;
                                } else {
                                    $suitediffbottom .= '<td style="' . $availability_padding . '">' . $suites_net_rent_output . '</td>' . $eol;
                                }



                                if ($building_type == "Industrial" || $building_type == "Industriel") {


                                    $suite_specs_query_string = "Select clear_height, shipping_doors_drive_in, shipping_doors_truck, available_electrical_volts, available_electrical_amps from suites_specifications where suite_id = '" . $Suites['suite_id'] . "'";
                                    $suite_specs_query = mysql_query($suite_specs_query_string) or die("suite specs query error: " . mysql_error());
                                    $suite_specs = mysql_fetch_array($suite_specs_query);

                                    $shippingDI = $suite_specs['shipping_doors_drive_in'];
                                    $shippingTL = $suite_specs['shipping_doors_truck'];
                                    $poweramps = $suite_specs['available_electrical_amps'];
                                    $powervolts = $suite_specs['available_electrical_volts'];
                                    $ceiling = $suite_specs['clear_height'];
                                    $loadingoutputDI = '';
                                    $loadingoutputTL = '';
                                    $loadingoutput = '';

                                    if ($shippingDI != '' && $shippingDI != 0) {
                                        $loadingoutputDI = $shippingDI . " " . $language['drivein_loading_abbreviation'];
                                    }
                                    if ($shippingTL != '' && $shippingTL != 0) {
                                        $loadingoutputTL = $shippingTL . " " . $language['truck_loading_abbreviation'];
                                    }
                                    if ($loadingoutputDI != '' && $loadingoutputTL != '') {
                                        $loadingoutput = $loadingoutputDI . "<br />" . $loadingoutputTL;
                                    } else if ($loadingoutputDI != '' && $loadingoutputTL == '') {
                                        $loadingoutput = $loadingoutputDI;
                                    } else if ($loadingoutputDI == '' && $loadingoutputTL != '') {
                                        $loadingoutput = $loadingoutputTL;
                                    } else {
                                        // both are empty leave a space
                                        $loadingoutput = "&nbsp;";
                                    }

                                    $poweroutputAmps = '';
                                    $poweroutputVolts = '';
                                    if ($poweramps != '' && $poweramps != 0) {
                                        $poweroutputAmps = $poweramps . " A";
                                    }
                                    if ($powervolts != '' && $powervolts != 0) {
                                        $poweroutputVolts = $powervolts . " V";
                                    }
                                    if ($poweroutputAmps != '' && $poweroutputVolts != '') {
                                        $poweroutput = $poweroutputAmps . "<br />" . $poweroutputVolts;
                                    } else if ($poweroutputAmps != '' && $poweroutputVolts == '') {
                                        $poweroutput = $poweroutputAmps;
                                    } else if ($poweroutputAmps == '' && $poweroutputVolts != '') {
                                        $poweroutput = $poweroutputVolts;
                                    } else {
                                        // both are empty leave a space
                                        $poweroutput = "&nbsp;";
                                    }




                                    $suitediffbottom .= '<td style="' . $availability_padding . '">' . $suite_specs['clear_height'] . '</td>' . $eol;
                                    $suitediffbottom .= '<td style="' . $availability_padding . '">' . $loadingoutput . '</td>' . $eol;
                                    $suitediffbottom .= '<td style="' . $availability_padding . '">' . $poweroutput . '</td>' . $eol;
                                    $suitediffbottom .= '</tr>' . $eol;
                                } else {
                                    $suitediffbottom .= '</tr>' . $eol;
                                }

                                $suitediffbottom .= '<tr style="background-color:#fff;">';



                                $suitediffbottom .= '<td>&nbsp;</td>';



                                if ($building_type == "Industrial" || $building_type == "Industriel") {
                                    $notes_colspan = $availability_colspan - 1;
                                } else {
                                    $notes_colspan = $availability_colspan;
                                }




                                if ($suite_leased_flag_count > 0 || $private_suitescount > 0) {
                                    $suitediffbottom .= '<td colspan="' . $notes_colspan . '" style="margin:0;padding: 0px 0px 0px 10px;text-decoration:line-through;">';
                                } else if ($Suites['promoted'] == "true") {
//                                        $suitediffbottom .='<tr style="background-color:#fff;font-weight:bold;font-style:italic;">';
                                    $suitediffbottom .= '<td colspan="' . $notes_colspan . '" style="margin:0;padding: 0px 0px 0px 10px;font-weight:bold;font-style:italic;">';
                                } else {
                                    $suitediffbottom .= '<td colspan="' . $notes_colspan . '" style="margin:0;padding: 0px 0px 0px 10px">';
                                }

                                if ($Suites['promoted'] == "true") {
                                    $suitediffbottom .= '<span style="margin-right:5px;text-transform:uppercase;">' . $language['promoted_suite_text'] . '</span>';
                                }

                                $suitediffbottom .= $Suites['description'] . '</td>' . $eol;
                                $suitediffbottom .= "</tr>" . $eol;



                                if ($changed_suite_area != '') {
                                    $mpSuiteArea = $changed_suite_area_noformat;
                                } else {
                                    $mpSuiteArea = $unchanged_suite_area_noformat;
                                }

                                if ($reallyleased != 1) {
                                    $suiteType = $Suites['suite_type'];
                                    if ($suiteType == '') {
                                        $suiteType = $suite_selected;
                                    }
                                    $suite_id = $Suites['suite_id'];
                                    $building_code = $Suites['building_code'];
                                    $building_id = $Suites['building_id'];
//echo "building code: " . $building_code . "<br><br>";
                                    updateMarketPulse($clientname, $building_code, $suite_id, $bu_reason, $mpSuiteArea, $suiteType, $bCity, $recordDate, $db);
                                }
                            }
                        } else {  // (changecount = 0)
                            // check suites_diff - if there is a "yesterday" record for any given suite then it is NOT A NEW SUITE
                            // this means it should not be outputted in this NEW SUITE part of the code.
                            $yesterdaybuilding_query_string = "SELECT record_source FROM " . $freq_diff_table . " WHERE suite_id = '" . $Suites['suite_id'] . "' AND record_source = 'yesterday'";
                            $yesterdaybuilding_query = mysql_query($yesterdaybuilding_query_string) or die("yesterday query failed: " . mysql_error());
                            $yesterdaybuilding_count = mysql_num_rows($yesterdaybuilding_query);



                            if ($yesterdaybuilding_count > 0) {
                                // this is not a new suite.
                                // Need to prevent the building header from displaying
                                $nobuilding++;
                            } else {
                                $newsuite++;
                                // Output NEW SUITE 
                                // Set up English vs French sqft formatting
                                if ($lang == "en_CA") {
                                    $new_suite_area = number_format($Suites['net_rentable_area']);
                                    $new_contiguous_area = number_format($Suites['contiguous_area']);
                                    $new_min_divisible_area = number_format($Suites['min_divisible_area']);
                                }
                                if ($lang == "fr_CA") {
                                    $new_suite_area = number_format($Suites['net_rentable_area'], 0, ',', ' ');
                                    $new_contiguous_area = number_format($Suites['contiguous_area'], 0, ',', ' ');
                                    $new_min_divisible_area = number_format($Suites['min_divisible_area'], 0, ',', ' ');
                                }
                                if ($Suites['availability'] != "") {
                                    //make old dates show as 'immediately'
                                    if ($Suites['availability'] < time()) {
                                        $new_suite_availability = $language['immediately_text'];
                                    } else {
                                        //show the actual availability date - its in the future
                                        $new_suite_availability = strftime('%b %Y', $Suites['availability']);
                                    }
                                } else {
                                    $new_suite_availability = $language['immediately_text'];
                                }

                                $suitediffbottom .= '<tr style="background-color:#fff;">';
                                $suitediffbottom .= '<td style="' . $availability_padding . 'color:red;text-align:center"><span style="margin:10px;padding:5px;color:red;border:1px solid red;">' . $language['new_text'] . '</span></td>' . $eol;
                                $suitediffbottom .= '<td style="' . $availability_padding . '">';

                                if ($theme['result_links_to_corp_website'] == 1) {
                                    $suitediffbottom .= '<a rel="newsuite" href="">' . $Suites['suite_name'] . '</a>';
                                } else {
                                    $suitediffbottom .= '<a rel="newsuite" href="' . $email_link_url . '/suite.php?suiteid=' . $Suites['suite_id'] . '&building=' . $Building['building_code'] . '&lang=' . $lang . '" target="_blank">' . $Suites['suite_name'] . '</a>';
                                } // END if/else $theme['result_links_to_corp_website'] == 1

                                $suitediffbottom .= '</td>' . $eol;





//                                    if ($Suites['contiguous_area'] != '' && $Suites['contiguous_area'] != '0') {
//                                        $suitediffbottom .= ' (' .$new_contiguous_area . ')';
//                                    }
//
//                                    $suitediffbottom .= ' ' .$squarefoot . ' </td>' . $eol;
//                                    if (($Suites['net_rentable_area'] != '' && $Suites['net_rentable_area'] != '0') && ($Suites['contiguous_area'] != '' && $Suites['contiguous_area'] != '0') && ($Suites['min_divisible_area'] != '' && $Suites['min_divisible_area'] != '0')) {
//                                        $suitediffbottom .= '<td style="'.$availability_padding.'">';
//                                        $suitediffbottom .= $new_suite_area;
//                                        $suitediffbottom .= '<br>';
//                                        $suitediffbottom .= '(' . $new_min_divisible_area . ' | ' . $new_contiguous_area . ')' . $squarefoot;
//                                        $suitediffbottom .= '</td>';
//                                    }
                                // area output
                                $areaoutput3 = '';
                                $areaoutput3 .= '<td data-sqft="' . $new_suite_area . '" data-mindiv="' . $new_min_divisible_area . '" data-contig="' . $new_contiguous_area . '" style="' . $availability_padding . '"> ';

                                // net rentable area
                                $areaoutput3 .= $new_suite_area;

                                if (($new_min_divisible_area != 0 && $new_min_divisible_area != '') || ($new_contiguous_area != 0 && $new_contiguous_area != '')) {
                                    $areaoutput3 .= '<br>';
                                    $areaoutput3 .= '(';
                                }

                                // min divisible area
                                if ($new_min_divisible_area != 0 && $new_min_divisible_area != '') {
                                    $areaoutput3 .= $new_min_divisible_area;
                                }



                                // divider between mindiv and contig
                                // do not show if both are not present
                                if (($new_min_divisible_area != 0 && $new_min_divisible_area != '') && ($new_contiguous_area != 0 && $new_contiguous_area != '')) {
                                    $areaoutput3 .= ' | ';
                                }



                                // max contiguous area
                                if ($new_contiguous_area != 0 && $new_contiguous_area != '') {
                                    $areaoutput3 .= $new_contiguous_area;
                                }

                                if (($new_min_divisible_area != 0 && $new_min_divisible_area != '') || ($new_contiguous_area != 0 && $new_contiguous_area != '')) {
                                    $areaoutput3 .= ')';
                                }

                                $areaoutput3 .= ' ' . $squarefoot . '</td>' . $eol;
                                // end area output


                                $suitediffbottom .= $areaoutput3;























                                if ($Suites['net_rent'] == "0.00" || $Suites['net_rent'] == "" || $Suites['net_rent'] == "$0.00") {
                                    $suites_net_rent_output = $language['negotiable_text'];
                                } else {
                                    $suites_net_rent_output = money_format('%.2n', (double) $Suites['net_rent']);
                                }

                                $suitediffbottom .= '<td style="' . $availability_padding . '">' . $new_suite_availability . '</td>' . $eol;
                                $suitediffbottom .= '<td style="' . $availability_padding . '">' . money_format('%.2n', (double) $Suites['add_rent_total']) . '</td>' . $eol;
                                $suitediffbottom .= '<td style="' . $availability_padding . '">' . $suites_net_rent_output . '</td>' . $eol;








                                if ($building_type == "Industrial" || $building_type == "Industriel") {


                                    $suite_specs_query_string = "Select clear_height, shipping_doors_drive_in, shipping_doors_truck, available_electrical_volts, available_electrical_amps from suites_specifications where suite_id = '" . $Suites['suite_id'] . "'";
                                    $suite_specs_query = mysql_query($suite_specs_query_string) or die("suite specs query error: " . mysql_error());
                                    $suite_specs = mysql_fetch_array($suite_specs_query);

                                    $shippingDI = $suite_specs['shipping_doors_drive_in'];
                                    $shippingTL = $suite_specs['shipping_doors_truck'];
                                    $poweramps = $suite_specs['available_electrical_amps'];
                                    $powervolts = $suite_specs['available_electrical_volts'];
                                    $ceiling = $suite_specs['clear_height'];
                                    $loadingoutputDI = '';
                                    $loadingoutputTL = '';
                                    $loadingoutput = '';

                                    if ($shippingDI != '' && $shippingDI != 0) {
                                        $loadingoutputDI = $shippingDI . " " . $language['drivein_loading_abbreviation'];
                                    }
                                    if ($shippingTL != '' && $shippingTL != 0) {
                                        $loadingoutputTL = $shippingTL . " " . $language['truck_loading_abbreviation'];
                                    }
                                    if ($loadingoutputDI != '' && $loadingoutputTL != '') {
                                        $loadingoutput = $loadingoutputDI . "<br />" . $loadingoutputTL;
                                    } else if ($loadingoutputDI != '' && $loadingoutputTL == '') {
                                        $loadingoutput = $loadingoutputDI;
                                    } else if ($loadingoutputDI == '' && $loadingoutputTL != '') {
                                        $loadingoutput = $loadingoutputTL;
                                    } else {
                                        // both are empty leave a space
                                        $loadingoutput = "&nbsp;";
                                    }

                                    $poweroutputAmps = '';
                                    $poweroutputVolts = '';
                                    if ($poweramps != '' && $poweramps != 0) {
                                        $poweroutputAmps = $poweramps . " A";
                                    }
                                    if ($powervolts != '' && $powervolts != 0) {
                                        $poweroutputVolts = $powervolts . " V";
                                    }
                                    if ($poweroutputAmps != '' && $poweroutputVolts != '') {
                                        $poweroutput = $poweroutputAmps . "<br />" . $poweroutputVolts;
                                    } else if ($poweroutputAmps != '' && $poweroutputVolts == '') {
                                        $poweroutput = $poweroutputAmps;
                                    } else if ($poweroutputAmps == '' && $poweroutputVolts != '') {
                                        $poweroutput = $poweroutputVolts;
                                    } else {
                                        // both are empty leave a space
                                        $poweroutput = "&nbsp;";
                                    }




                                    $suitediffbottom .= '<td style="' . $availability_padding . '">' . $suite_specs['clear_height'] . '</td>' . $eol;
                                    $suitediffbottom .= '<td style="' . $availability_padding . '">' . $loadingoutput . '</td>' . $eol;
                                    $suitediffbottom .= '<td style="' . $availability_padding . '">' . $poweroutput . '</td>' . $eol;
                                    $suitediffbottom .= '</tr>' . $eol;
                                    $suitediffbottom .= '<tr style="background-color:#fff;"><td>&nbsp;</td><td colspan="' . $availability_colspan . '" style="' . $availability_padding . '">' . $Suites['description'] . '</td></tr>' . $eol;
                                } else {
                                    $suitediffbottom .= '</tr>' . $eol;
                                    $suitediffbottom .= '<tr style="background-color:#fff;"><td>&nbsp;</td><td colspan="' . $availability_colspan . '" style="' . $availability_padding . '">' . $Suites['description'] . '</td></tr>' . $eol;
                                }


                                $suitediffbottom .= "</tr>";
                            } // end if $yesterdaybuilding_count > 0 ymt 
//                    if ($reallyleased != 1 && $suite_notice_period != 1){
                            if ($reallyleased != 1) {
                                $suiteType = $Suites['suite_type'];
                                if ($suiteType == '') {
                                    $suiteType = $suite_selected;
                                }
                                $suite_id = $Suites['suite_id'];
                                $building_code = $Suites['building_code'];
                                $building_id = $Suites['building_id'];
                                updateMarketPulse($clientname, $building_code, $suite_id, "New", $Suites['net_rentable_area'], $suiteType, $bCity, $recordDate, $db);
                            }
                        }  // end if ($changecount

                        $realchangecount = $realchangecount + $changecount;
                    } // end while $suites 
                    $realsuitescount = $realsuitescount + $suitescount;
                } // end foreach suitedriver

                $suitediffbottomcontacts .= '<tr><td colspan="' . $availability_colspan . '">';
                $suitediffbottomcontacts .= '<table width="100%"><tr>';
                while ($contacts = mysql_fetch_array($contacts_query)) {
                    $suitediffbottomcontacts .= '<td style="padding-right: 10px;" >' . $contacts['name'] . '<br />' . $contacts['title'] . '<br />' . $contacts['email'] . '<br />' . $contacts['phone'] . '</td>' . $eol;
                }

                $suitediffbottomcontacts .= '</tr></table>';
                $suitediffbottomcontacts .= '</td></tr>';
                $suitediffbottomcontacts .= '<tr><td colspan="' . $availability_colspan . '">&nbsp;</td></tr>' . $eol;
                $suitediffbottomcontacts .= '<tr><td colspan="' . $availability_colspan . '">&nbsp;</td></tr>' . $eol;
                $suitediffbottomcontacts .= '<tr><td colspan="' . $availability_colspan . '">&nbsp;</td></tr>' . $eol;
                $suitediffbottomcontacts .= "</table></div>" . $eol;

                if ($suite_marketing_text_count > 0) {
                    $marketing_style = '';
                    $marketing_reason_border = "border:1px solid red;";
                    $marketing_reason_color = "color:red;";

                    if ($marketing_removed > 0) {
                        $marketing_style = "text-decoration:line-through;";
                        $marketing_reason_border = "";
                        $marketing_reason_color = "";
                        $marketing_announcement_text = $yesterday_suites['marketing_text'];
                    } else {
                        $marketing_style = "";
                        $marketing_announcement_text = $Building['marketing_text'];
                    }

                    $marketing_text_output = '<tr class="bu_brokerannouncement" style="background-color:#fff;padding:10px;">' . $eol;
                    $marketing_text_output .= '<td colspan="' . $marketing_colspan . '" style="margin:0;padding:5px;text-align:left;font-weight:bold;">' . $language['marketing_announcement_text'] . '</td></tr>';
                    $marketing_text_output .= '<tr class="bu_brokerannouncement" style="background-color:#fff;padding:10px;">' . $eol;
                    $marketing_text_output .= '<td style="margin:0;padding:10px;' . $marketing_reason_color . 'text-align:center;"><span style="margin:10px;padding:5px;' . $marketing_reason_border . $marketing_style . '">' . $bu_marketing_reason . '</span></td>';
                    $marketing_text_output .= '<td colspan="' . ($marketing_colspan - 1) . '" style="margin:0;padding:5px;' . $marketing_style . '">' . stripslashes($marketing_announcement_text) . '</td></tr>';
                    $marketing_text_output .= '<tr class="spacer" style="background-color:#fff;"><td colspan="' . $marketing_colspan . '"><hr style="border-top: 1px solid #eef0f2;"></td></tr>';
                } else {
                    $marketing_announcement_text = $Building['marketing_text'];
                    if ($marketing_announcement_text != '') {
                        $marketing_text_output = '<tr class="bu_brokerannouncement" style="background-color:#fff;padding:10px;">' . $eol;
                        $marketing_text_output .= '<td colspan="' . $marketing_colspan . '" style="margin:0;padding:5px;text-align:left;font-weight:bold;">' . $language['marketing_announcement_text'] . '</td></tr>';
                        $marketing_text_output .= '<tr class="bu_brokerannouncement" style="background-color:#fff;padding:10px;">' . $eol;
                        $marketing_text_output .= '<td style="margin:0;padding:10px;">&nbsp;</td>';
                        $marketing_text_output .= '<td colspan="' . ($marketing_colspan - 1) . '" style="margin:0;padding:5px;">' . stripslashes($marketing_announcement_text) . '</td></tr>';
                        $marketing_text_output .= '<tr class="spacer" style="background-color:#fff;"><td colspan="' . $marketing_colspan . '"><hr style="border-top: 1px solid #eef0f2;"></td></tr>';
                    }
                }





// this filters out notice_period buildings that have changed availabilities that we aren't showing by clearing the suitedifftop elements
// this also is where we show all suites for buildings where just a marketing announcement has changed using $suitediffshowallsuites

                if (($suitechanges_thisbuilding == 0) && $newsuite == 0) {
                    if ($onlymktg == 1) {
                        $suitediffbottom = $suitediffshowallsuites;
                    } else {
                        $suitedifftop1 = '';
                        $suitedifftop2 = '';
                    }
                }
            } // end while $building



            if ($suitediffbottom != '') {
                if ($building_type == "Industrial" || $building_type == "Industriel") {
                    $suitediffbottom .= '<tr style="background-color:#fff;"><td colspan="9" style="background-color:#fff;">&nbsp;</td></tr>' . $eol;
                } else {
                    $suitediffbottom .= '<tr style="background-color:#fff;"><td colspan="7" style="background-color:#fff;">&nbsp;</td></tr>' . $eol;
                }

                $output .= $suitedifftop1 . $marketing_text_output . $suitedifftop2 . $suitediffbottom . $suitediffbottomcontacts;
            }


            $suitedifftop1 = '';
            $suitedifftop2 = '';
            $suitediffbottom = '';
            $suitediffbottomcontacts = '';
            $marketing_text_output = '';
        }  //end foreach building driver		
    } //end ($initial_suites_count > 0)
    // end per building loop


    if ($output != '') {
        $subscriptionsresults .= $subscription_tab_html . $output;
    }
}// END PER SUBSCRIPTION LOOP HERE

$email_subject_text = $language['email_subject_text'];

if ($bu_freq == 'Weekly') {
    $email_subject_text = $language['email_subject_text_weekly'];
} else if ($bu_freq == 'Monthly') {
    $email_subject_text = $language['email_subject_text_monthly'];
} else {
    $email_subject_text = $language['email_subject_text'];
}



if ($subscriptionsresults != '') {

    $emaildomain = str_replace('http://', '', $email_link_url);
    $to_email = $bu_email;

    $subject = $emaildomain . ' ' . $email_subject_text;

    $messagetop = '';
    $messagetop .= '<html><head>' . $eol;
    $messagetop .= '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />' . $eol;
    $messagetop .= '<title> ' . $email_subject_text . ' ' . date('Y-m-d') . '</title>' . $eol;


    $messagetop .= '</head><body>' . $eol;

    $messagetop .= '<div><img src="' . $language['vacancy_report_website'] . '/' . $language['share_this_image'] . '" alt="companylogo" title="companylogo"></div>';
    $messagetop .= $eol . '<div><h3><div style="padding:5px 5px 5px 0px;color: #000;font-family: sans-serif;border-radius: 4px 4px 4px 4px;">' . $email_subject_text . ' ' . date('Y-m-d') . '</div></div>';
    $messagebottom = '';
//        $messagebottom .='<p style = "font-size:13px;">' . $language['email_first_paragraph_text'] . '</p>' . $eol; //just replaced the red frame string, for now.

    $messagebottom .= $subscriptionsresults;

    $messagebottom .= '<table style="width:100%"><tr>';
    $link = $language['vacancy_report_website'] . '/index.php?unique=' . $subscription_unique_id . '&email=' . $bu_email . $bu['bu_query_string'] . '&oo_manage=all&lang=' . $lang;
    $messagebottom .= '<td><span style="margin: 0px auto;font-size: 15px;"><a target="_blank" href="' . $link .
            '" style="text-decoration: none;color:#A1A9B4;"> ' . $language['manage_subscriptions'] . '</a></span></td>';
    $messagebottom .= '<td align="right">
        <span style="margin: 110px auto;font-size: 15px;">
        <span style="color:#000000">' . $language['powered_by'] . '</span>
        <a target="_blank" href = "http://www.arcestra.com" style="text-decoration: none; white-space: nowrap;">
        <span style="color:#4B5F78;letter-spacing: -3px;">A</span>
        <span style="color:#4B5F78;letter-spacing: -3px;">R</span>
        <span style="color:#4B5F78;letter-spacing: -3px;">C</span>
        <span style="color:#EBD93D;letter-spacing: -3px; font-size: 21px;">e</span>
        <span style="color:#40ABBB;letter-spacing: -3px;">S</span>
        <span style="color:#584787;letter-spacing: -3px;">T</span>
        <span style="color:#38B456;letter-spacing: -3px;">R</span>
        <span style="color:#CF2C57;letter-spacing: -3px;">A</span>
        </a>
        </span>
        </td>
        </tr>
        </table>
        <br><br>';


    $messagebottom .= "______________________________________________________<br />
        " . $language['email_do_not_reply_text1'] . "<br />
        " . $language['email_do_not_reply_text2'] . "<br />
        </body></html>" . $eol;

    $headers = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
    $headers .= 'From: ' . ucwords($clientname) . ' Broker Updates <updates@' . $emaildomain . '>' . $eol;



    $specialmessage = '';
    if ($language['bu_special_message'] != '') {
        $specialmessage = '<p style="margin:0;padding:0;color:red;text-align:left;">' . $language['bu_special_message'] . '</p>';
    }



    if ($language['bu_special_message_toggle'] == 1) {
        // we have a special message
        $message = $messagetop . $specialmessage . $messagebottom;
    } else {
        // there is no special message
        $message = $messagetop . $messagebottom;
    }

    if ($testmode == 0) {
//  Send normal BU email
        @mail($to_email, $subject, $message, $headers, '-f updates@' . $emaildomain . '');

//  Send 2nd email to RR for archive/debug/admin purposes.
        $headers2 = 'MIME-Version: 1.0' . "\r\n";
        $headers2 .= 'Content-type: text/html; charset=utf-8' . "\r\n";
        $headers2 .= 'From: ' . ucwords($clientname) . ' Broker Updates <updates@' . $emaildomain . '>' . $eol;
        $test_email = 'admin+bu@rationalroot.com';
        $subject = $to_email . ' ' . $emaildomain . ' ' . $email_subject_text;
        @mail($test_email, $subject, $message, $headers2, '-f updates@' . $emaildomain . '');


//  BU REPORTING
//  Check reporting db for a 'today' entry
        $today_report_check_query_string = "select * from stats_bu_sent_mail where datesent = '" . $today . "'";
        $today_report_check_query = mysql_query($today_report_check_query_string) or die("today query error: " . mysql_error());
        $today_report_check = mysql_num_rows($today_report_check_query);

        if ($today_report_check == 0) {
//  Add a new row for today
            $bu_stats_newday_query_string = "INSERT INTO stats_bu_sent_mail (clientname, datesent) VALUES ('" . $clientname . "', '" . $today . "')";
            $bu_stats_newday_query = mysql_query($bu_stats_newday_query_string) or die("newday query error: " . mysql_error());

            $bu_stats_counter_query_string = "UPDATE stats_bu_sent_mail SET num_emails_sent=num_emails_sent+1 WHERE datesent = '" . $today . "'";
            $bu_stats_counter_query = mysql_query($bu_stats_counter_query_string) or die("bu stats counter error: " . mysql_error());
        } else {
//  Increment num_emails_sent
            $bu_stats_counter_query_string = "UPDATE stats_bu_sent_mail SET num_emails_sent=num_emails_sent+1 WHERE datesent = '" . $today . "'";
            $bu_stats_counter_query = mysql_query($bu_stats_counter_query_string) or die("bu stats counter error: " . mysql_error());
        } // end if today_report_check
// create HTML record
        $path = "bu_storage/";
        $filename = $to_email . "_" . str_replace("/", "-", $today) . "_" . $bu_freq . ".html";
        file_put_contents($path . $filename, $message);
    } else {
        // TEST MODE ENABLED
        // Hijack BU email so it comes to us
        // No reporting
        // No need for a CC'd email
        $test_email = "admin+testingbu@rationalroot.com";
        $subject = $to_email . ' ' . $emaildomain . ' ' . $email_subject_text;
        @mail($test_email, $subject, $message, $headers, '-f updates@' . $emaildomain . '');

        // create HTML record
        $path = "bu_storage/";
        $filename = $to_email . "_" . str_replace("/", "-", $today) . "_" . $bu_freq . "_TESTING.html";
        file_put_contents($path . $filename, $message);
    }
} // end if ($output != '')
echo $message;



if ($subscriptionsresults == '') {
    echo '<br><br><p>No changes for this search. <a href="bu_list.php">Go Back to the List</a></p>';
    echo '<p>(this page is only here for demonstration purposes)</p>';
}
