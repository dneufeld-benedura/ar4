<script type="text/javascript">
    
//var num_entries = '';    


// IE fixes for console.log
if (!window.console) console = {log: function() {}}; 
if (!console) console = {log: function() {}};


function addslashes (str) { 
	return (str + '').replace(/[\\"']/g, '\\$&').replace(/\u0000/g, '\\0');
}

   var check_arr = [];
   <?php $suffix=$language['properties_instructions_text'];
   $text = '';
   ?>
   


var hideShowCount = 0 ;
window.addEventListener("pagehide", function() {
    hideShowCount++ ;
//    showEventTime('pagehide') ;
});

window.addEventListener("pageshow", function() {
    hideShowCount++ ;
//    showEventTime('pageshow') ;
});


var timeoutDelay = 1000 ;

function tourbookLink(){
    var hideShowCountAtClick = hideShowCount ;
//    showEventTime('click') ;
    setTimeout(function () {
//               showEventTime('timeout function '+hideShowCountAtClick+','+hideShowCount) ;
               if (hideShowCount == hideShowCountAtClick){
                       //alert('app is not installed') ;
                    window.location = 'https://itunes.apple.com/us/app/tourbook/id557257168?mt=8&uo=4' ;
               }
            }, timeoutDelay);
}

//function currentTime()
//{
// return Date.now()
///1000 ;
//}

//
//function showEventTime(event){
//    var time = currentTime() ;
//    document.body.appendChild(document.createElement('br'));
//    document.body.appendChild(document.createTextNode(time+' '+event));
//}
//

       
           // When document is ready, initialize jquery
           $(document).ready(function(){     


               
 	$('#scroll_content').scrollPagination({

		nop     : 5, // The number of posts per scroll to be loaded
		offset  : 0, // Initial offset, begins at 0 in this case
		error   : '', // When the user reaches the end this is the message that is
                              // displayed.
		delay   : 500, // When you scroll down the posts will load after a delayed amount of time.
		               // This is mainly for usability concerns. You can alter this as you see fit
		scroll  : true // The main bit, if set to false posts will not load as the user scrolls. 
		               // but will still load if the user clicks.
		
	});


                               $('#loader').hide();
   
                               $('#search #province').change(function(){
                                       // type
                                       $('#search #region').fadeOut();
                                       $('#search #sub-region').fadeOut();
                                       $('#search #loader').show();
               
                                       $.post("region.php", {
                                               province: $('#search #province').val()
							<?php
								if (isset($_REQUEST['lang'])) {
									echo ", lang: '" . $_REQUEST['lang'] . "'";
								}
							?>
                                       }, function(response){
                                               setTimeout("finishAjax('#search #region', '"+escape(response)+"')", 400);
                                       });
                                       var slashed = $('#search #region').val();
   						//slashed = escape(slashed);
   						slashed = slashed;
   						/**/
   						$.post("sub-region.php", {
   							province: $('#search #province').val(),
   							region: $('#search #province').val()
   							<?php
								if (isset($_REQUEST['lang'])) {
									echo ", lang: '" . $_REQUEST['lang'] . "'";
								}
							?>				
							 
							 }, function(response){
   							setTimeout("finishAjax('#search #sub-region', '"+escape(response)+"')", 400);
   						});/**/
                                       
                                       return false;
                               });
               
                               $('#search #region').change(function(){
                                       // make
                                       $('#search #sub-region').fadeOut();
                                       $('#search #loader').show();
               
                                       var slashed = $('#search #region').val();
                                       //slashed = escape(slashed);
                                       slashed = slashed;
               
                                       $.post("sub-region.php", {
                                               province: $('#search #province').val(),
                                               region: slashed
                                               <?php
                                                if (isset($_REQUEST['lang'])) {
                                                    echo ", lang: '" . $_REQUEST['lang'] . "'";
                                                }
                                                ?>
                                                                                       }, function(response){
                                               setTimeout("finishAjax('#search #sub-region', '"+escape(response)+"')", 400);
                                       });
                                       return false;
                               });
                               
                               $('img').error(function(){
                                       $(this).attr('src', 'images/no_thumb.jpg');
                               });
							   
							   
							   
							   
                                        $('#modal-broker-registration #province').change(function(){
                                       // type
                                       $('#modal-broker-registration #region').fadeOut();
                                       $('#modal-broker-registration #sub-region').fadeOut();
                                       $('#modal-broker-registration #loader').show();
               
                                       $.post("region.php", {
                                               province: $('#modal-broker-registration #province').val()
                                            <?php
                                                    if (isset($_REQUEST['lang'])) {
                                                            echo ", lang: '" . $_REQUEST['lang'] . "'";
                                                    }
                                            ?>
                                       }, function(response){
                                               setTimeout("finishAjax('#modal-broker-registration #region', '"+escape(response)+"')", 400);
                                       });
                                       var slashed = $('#modal-broker-registration #region').val();
   						//slashed = escape(slashed);
   						slashed = slashed;
   						/**/
   						$.post("sub-region.php", {
   							province: $('#modal-broker-registration #province').val(),
   							region: $('#modal-broker-registration #province').val()
   							<?php
								if (isset($_REQUEST['lang'])) {
									echo ", lang: '" . $_REQUEST['lang'] . "'";
								}
							?>				
							 
							 }, function(response){
   							setTimeout("finishAjax('#modal-broker-registration #sub-region', '"+escape(response)+"')", 400);
   						});/**/
                                       
                                       return false;
                               });
               
                               $('#modal-broker-registration #region').change(function(){
                                       // make
                                       $('#modal-broker-registration #sub-region').fadeOut();
                                       $('#modal-broker-registration #loader').show();
               
                                       var slashed = $('#modal-broker-registration #region').val();
                                       //slashed = escape(slashed);
                                       slashed = slashed;
               
                                       $.post("sub-region.php", {
                                               province: $('#modal-broker-registration #province').val(),
                                               region: slashed
                                               <?php
                                                if (isset($_REQUEST['lang'])) {
                                                    echo ", lang: '" . $_REQUEST['lang'] . "'";
                                                }
                                                ?>
                                       }, function(response){
                                               setTimeout("finishAjax('#modal-broker-registration #sub-region', '"+escape(response)+"')", 400);
                                       });
                                       return false;
                               });
                               
                               $('img').error(function(){
                                       $(this).attr('src', 'images/no_thumb.jpg');
                               });
							   
							   
							   
							   
							   
							   
							   
                                        $('#broker-edit-buildingsearch #province').change(function(){
                                       // type
                                       $('#broker-edit-buildingsearch #region').fadeOut();
                                       $('#broker-edit-buildingsearch #sub-region').fadeOut();
                                       $('#broker-edit-buildingsearch #loader').show();
               
                                       $.post("region.php", {
                                               province: $('#broker-edit-buildingsearch #province').val()
							<?php
								if (isset($_REQUEST['lang'])) {
									echo ", lang: '" . $_REQUEST['lang'] . "'";
								}
							?>
                                       }, function(response){
                                               setTimeout("finishAjax('#broker-edit-buildingsearch #region', '"+escape(response)+"')", 400);
                                       });
                                       var slashed = $('#broker-edit-buildingsearch #region').val();
   						//slashed = escape(slashed);
   						slashed = slashed;
   						/**/
   						$.post("sub-region.php", {
   							province: $('#broker-edit-buildingsearch #province').val(),
   							region: $('#broker-edit-buildingsearch #province').val()
   							<?php
								if (isset($_REQUEST['lang'])) {
									echo ", lang: '" . $_REQUEST['lang'] . "'";
								}
							?>				
							 
							 }, function(response){
   							setTimeout("finishAjax('#broker-edit-buildingsearch #sub-region', '"+escape(response)+"')", 400);
   						});/**/
                                       
                                       return false;
                               });
               
                               $('#broker-edit-buildingsearch #region').change(function(){
                                       // make
                                       $('#broker-edit-buildingsearch #sub-region').fadeOut();
                                       $('#broker-edit-buildingsearch #loader').show();
               
                                       var slashed = $('#broker-edit-buildingsearch #region').val();
                                       //slashed = escape(slashed);
                                       slashed = slashed;
               
                                       $.post("sub-region.php", {
                                               province: $('#broker-edit-buildingsearch #province').val(),
                                               region: slashed
                                               <?php
if (isset($_REQUEST['lang'])) {
    echo ", lang: '" . $_REQUEST['lang'] . "'";
}
?>
                                       }, function(response){
                                               setTimeout("finishAjax('#broker-edit-buildingsearch #sub-region', '"+escape(response)+"')", 400);
                                       });
                                       return false;
                               });
                               
                               $('img').error(function(){
                                       $(this).attr('src', 'images/no_thumb.jpg');
                               });
							   
							   
							   
							   
							   
							   
                               
   
   
   
   
   
               $('.search-results .cust_check').css('display','block');
               
               var num_inputs = $('.search-results .search-result').length;
               for(i=0;i<num_inputs;i++) {
                       $('.search-results .search-result:eq('+i+') input:checkbox').css('display','none');
               }
               
   
   
   
                <?php  $text = ""; ?>


	   
				var fadeAlert;
    
               $('.search-results').on("click", "a.add-remove", function(e) {
 					e.preventDefault();
					
					
					clearTimeout(fadeAlert);
					
                       //console.log("input clicked");
                       if($(this).hasClass("isChecked")) {
                               //console.log("is checked");
                               $(this).removeClass("isChecked");
                               //check_value = $(this).parent().attr('id');
                               $(this).find('span.icon-add-remove').removeClass('fa-minus').addClass('fa-plus');

                               check_value = $(this).parent().attr('value');
                               check_arr.splice(check_arr.indexOf(check_value),1);
                               //$(this).html("Add to Compare");
                               var total_selected = check_arr.length;
                               if(total_selected == 0) {
                                    $('.total-selected').html('');
									
                                    $('.user-alert-message').html('No properties selected.');
                                    $('.user-alert').fadeIn( function() {
                                        //animation complete
                                        fadeAlert = setTimeout(function(){ $('.user-alert').stop().fadeOut(); }, 2000);
                                    });
                               }
                               else if(total_selected == 1) {
                                var oneSelected = '<?php echo $language['one_property_selected_text']; ?>';
                                    $('.total-selected').html(oneSelected+' -&nbsp;');
                                    $('.user-alert-message').html(oneSelected);
                                         $('.user-alert').fadeIn( function() {
                                         //animation complete
                                         fadeAlert = setTimeout(function(){ $('.user-alert').stop().fadeOut(); }, 2000);
                                    });        
                               } else {
                                    var moreSelected = '<?php echo $language['properties_selected_text']; ?>'; 
                                    $('.total-selected').html(check_arr.length + ' '+moreSelected+' -&nbsp;');
                                    $('.user-alert-message').html(check_arr.length + '&nbsp;'+moreSelected);
                                    $('.user-alert').fadeIn( function() {
                                         //animation complete

                                         fadeAlert = setTimeout(function(){ $('.user-alert').stop().fadeOut(); }, 2000);

                                    }); 
                               }
   
                       } else {
                                //console.log("not checked");
                                $(this).addClass("isChecked");
                                $(this).find('span.icon-add-remove').removeClass('fa-plus').addClass('fa-minus');

                                //$(this).attr("checked",false);
                                check_value = $(this).find('span.cust_check').attr('value');
                                //console.log("check value: " + check_value);
                                var is_in_array = jQuery.inArray(check_value, check_arr);
                                //console.log(is_in_array);
                                if(is_in_array < 0) {
                                        check_arr.push(check_value);        
                                }
                                //$(this).html("Remove Compare");
                                var total_selected = check_arr.length;
                                if(total_selected == 0) {
                                    $('.total-selected').html('');
									   
                                    $('.user-alert-message').html('No properties selected.');
                                    $('.user-alert').fadeIn( function() {
                                    //animation complete
                                    fadeAlert = setTimeout(function(){ $('.user-alert').stop().fadeOut(); }, 2000);
                                    });
                                } else if(total_selected == 1) {
                                    var oneSelected = '<?php echo $language['one_property_selected_text']; ?>';
                                    $('.total-selected').html(oneSelected+' -&nbsp;');

                                    $('.total-selected').html(oneSelected+' -&nbsp;');

                                    $('.user-alert-message').html(oneSelected);
                                        $('.user-alert').fadeIn( function() {
                                        //animation complete
                                        fadeAlert = setTimeout(function(){ $('.user-alert').stop().fadeOut(); }, 2000);
                                    });                  
                               } else {
                                        var moreSelected = '<?php echo $language['properties_selected_text']; ?>'; 
                                        $('.total-selected').html(check_arr.length + ' '+moreSelected+' -&nbsp;');
                                        $('.user-alert-message').html(check_arr.length + '&nbsp;'+moreSelected);
                                        $('.user-alert').fadeIn( function() {
                                            //animation complete
                                            fadeAlert = setTimeout(function(){ $('.user-alert').stop().fadeOut(); }, 2000);

                                        }); 
                               }
                       }
               });
               
	$('.btn-narrow-results').on("click", function(e) {
		e.preventDefault();
		var lang ='<?php echo $lang; ?>' 
		
		if(check_arr.length < 1) {
			alert("<?php echo $language['how_to_narrow_text'];?>"); 
		}
//		else if(check_arr.length > 10) {
//			alert("<?php //echo $language['limit_narrow_text'];?>"); 	  
//		} 
                else {
		
			<?php
			$currentpage = "";
			if (array_key_exists('CurrentPage', $_REQUEST)) {
				$currentpage = $_REQUEST['CurrentPage'];
			}
                        
                        
                        
                        
                        
			if (isset($_REQUEST['show_no_vacancy'])) {
				$show_no_vacancy_setting = $_REQUEST['show_no_vacancy'];
			} else {
                            if ($theme['no_vacancy_default'] == 1) {
                                $show_no_vacancy_setting = 'on';
                            } else if ($theme['no_vacancy_default'] == 0) {
                                $show_no_vacancy_setting = 'off';
                            }
                        }

			$narrowquerystring = $_SERVER['QUERY_STRING'];

			?>
                                        

		var show_no_vacancy_setting = '<?php echo $show_no_vacancy_setting; ?>';
			<?php if (strpos($narrowquerystring, 'lang=') > -1) { ?>
                                    window.open("index.php?<?php echo $narrowquerystring; ?>&show_no_vacancy="+ show_no_vacancy_setting +"&Address=" + check_arr,"_self");
			<?php 	} else { ?>
                                    window.open("index.php?lang=<?php echo $lang .'&'. $narrowquerystring; ?>&show_no_vacancy="+ show_no_vacancy_setting +"&Address=" + check_arr,"_self");
			<?php 	} ?>
		
		}
	});
   




               $.fn.autosugguest({
                       className: 'ausu-suggest',                                               // user specified - required
                       methodType: 'POST',                                               // 'POST' or 'GET' (set to POST as default)
                       dataFile: 'searchdata.php',                                     // user specified - required
                       minChars: 2,                                                    // How many characters are required before a suggestion appears.
                       FadeTime: 100,                                                  // Time (in milliseconds) it takes for the list to fade out.
                       rtnIDs: false,                                                // true or false (set to false as default) - read more below.
                       addParams: 'lang=<?php echo $lang_id; ?>'   // User specified parameters, separated by ampersand ( & )
               });
       
//console.log("show no vacancy: "+<?php // echo $show_no_vacancy; ?>);   
 	<?php 
        if ($show_no_vacancy == 0) { ?>
			$('.vacancycheckbox').removeAttr('checked');
	<?php } else { ?>
			$('.vacancycheckbox').attr('checked','checked');
	<?php } ?>  



    $("a.threedee_link").on('click', function (e) {
       alert('clicked');
	   e.preventDefault();
       var href = $(this).href;
       $("#threedee_image").attr("src", href);
    });






	//END DOCUMENT READY        
	});
					   
					   


//function tourbookLink() {
//	setTimeout(function () { window.location = "https://itunes.apple.com/us/app/tourbook/id557257168?mt=8&uo=4"; }, 25);
////	window.location = arcestratourbook://;
//}




                              
                       function finishAjax(id, response){
                               $('#loader').hide();
                               $(id).html(unescape(response));
                               $(id).fadeIn();
                       }
                       
     $('.search-result').jscroll();
     
</script>
