<?php

/**
 * @author Rational Root
 * @see ../main_abstract/SearchData_abstract.php
 * @since March 2014
 * @version 1.6
 * */
class FormatData {

    /**
     * This method removes the parameter from an inputted String.
     * Accesses either the abstract or its overriden method from the classes it extends.
     * <p>
     * @param  {String} $string_with_param_to_remove
     * @param  {String} $param
     * @returns {String} $final_query_string
     * @see building.php
     * @see index.php
     * @see suite.php
     * */
    public function removeParam($string_with_param_to_remove, $param) {

        $string_with_param_to_remove = $string_with_param_to_remove . "&";
        $begin = stripos($string_with_param_to_remove, $param);
        $substring = substr($string_with_param_to_remove, $begin);
        $end = stripos($substring, "&");
        $substring = substr($string_with_param_to_remove, $begin, $end);
        $ret = str_replace($substring, "", $string_with_param_to_remove);
        while (strpos($ret, "&&") !== false) {
            $ret = str_replace("&&", "&", $ret);
        }
        while (strpos($ret, "??") !== false) {
            $ret = str_replace("??", "?", $ret);
        }
        if (!is_bool($begin) && !is_bool($end)) {
            return trim($ret, '&');
        }
        return trim($string_with_param_to_remove, '&');
    }

    /**
     * This method removes the concentates the strings for the Suite HTML.
     * Accesses either the abstract or its overriden method from the classes it extends.
     * <p>
     * @param  {String} $building_code
     * @param  {Queries} $queries
     * @param  {Array} $row
     * @param  {Array} $lang
     * @param  {Array} $language
     * @returns {String} $suites_html
     * @see building.php
     * @see index.php
     * */
    public function concatSuiteHTML($building_code, $queries, $row, $lang, $language, $theme) {
        $suite_description = strip_tags($row['description'], '<p>,<br>,<br />,<strong>,<u>,<b>,<i>,<ul>,<li>,<ol>,<a>');
        $suite_availability = $queries->get_suite_availability($language, $row);
        $suite_net_rentable_area = $queries->get_suite_net_rentable_area($lang, $row);
        $suite_contiguous_area = $queries->get_suite_contiguous_area($lang, $row);
        $suite_net_rent = $queries->get_suite_net_rent($language, $row);
        $suite_new = $row['new'];
        $suite_model = $row['model'];
        $suite_leased = $row['leased'];
        $suite_promoted = $row['promoted'];
        $row_format = $queries->output_row_format($suite_leased, $suite_promoted);
        $suite_name = $row['suite_name'];
		$suite_id = $row['suite_id'];


	 
        $fixStr = str_replace('&building_name=' . $building_code , '', $_SERVER['QUERY_STRING']);
        $ahrefStr = 'suite.php?suiteid=' . $suite_id . '&building=' . $building_code . '&' . $fixStr;
        $ahrefStr = rtrim($ahrefStr, "&");


        $suites_html = "";


        $suites_html .= '<td class="suite">';
        $suites_html .= '<span' . $row_format . '>';
        
        
        
        if ($theme['result_links_to_corp_website'] == 1) {
            $suites_html .= $suite_name;
        } else {
            $suites_html .= '<a href="' . $ahrefStr . '">' . $suite_name . '</a>';
        }
        
        if ($suite_new == 'true') {
            $suites_html .= '<span class="label label-danger">';
            $suites_html .= $language['new_text'];
            $suites_html .= '</span></span></td>';
        } else if ($suite_model == 'true') {
            $suites_html .= '<span class="label label-warning">';
            $suites_html .= $language['model_text'];
            $suites_html .= '</span></span></td>';
        } else if ($suite_leased == 'true') {
            $suites_html .= '<span class="label label-primary">';
            $suites_html .= $language['leased_text'];
            $suites_html .= '</span></span></td>';
        } else if ($suite_promoted == 'true') {
            $suites_html .= '<span class="label label-promoted">';
            $suites_html .= $language['promoted_text'];
            $suites_html .= '</span></span></td>';
        } else {
            $suites_html .= '</span></td>';
        }
        

        $suites_html .= '<td' . $row_format . ' class="area">' . $suite_net_rentable_area . '</td>';

        if (intval($suite_contiguous_area) > intval($suite_net_rentable_area)) {
            $suites_html .= '<td' . $row_format . ' class="area-contiguous"><span class="contiguous">' . $suite_contiguous_area . '</span></td>';
        } else {
            $suites_html .= '<td' . $row_format . ' class="area-contiguous">&nbsp;</td>';
        }

        $suites_html .= '<td' . $row_format . ' class="rent">' . $suite_net_rent . '</td>';

        if ($suite_availability != "0000-00-00") {
            $suites_html .= '<td' . $row_format . ' class="availability">' . $suite_availability . '</td>';
        } else {
            $suites_html .= '<td' . $row_format . ' class="immediate">' . $immediately . '</td>';
        }

        $suites_html .= '<td' . $row_format . ' class="notes hidden-sm">' . strip_tags($suite_description, '<br>,<br />,<strong>,<u>,<b>,<i>,<ul>,<li>,<ol>,<a>') . "</td>";

        $suites_html .= "</tr>";

        return $suites_html;
    }













    public function format_numbers($value, $language) {
        if ($language['lang_id'] == "1") {
            $formatted_number = number_format((double) $value);
        }
        if ($language['lang_id'] == "0") {
            $formatted_number = number_format((double) $value, 0, ',', ' ');
        }
        return $formatted_number;
    }







   /**
     * This method removes the concentates the strings for the Suite HTML.
     * Accesses either the abstract or its overriden method from the classes it extends.
     * <p>
     * @param  {String} $building_code
     * @param  {Queries} $queries
     * @param  {Array} $row
     * @param  {Array} $lang
     * @param  {Array} $language
     * @returns {String} $suites_html
     * @see building.php
     * @see index.php
     * */
    public function concatSuiteHTMLNEW($building_type, $building_id, $building_code, $queries, $row, $lang, $language, $theme, $mobileview) {
        $suite_description = strip_tags($row['description'], '<p>,<br>,<br />,<strong>,<u>,<b>,<i>,<ul>,<li>,<ol>,<a>');
        $suite_availability = $queries->get_suite_availability($language, $row);
        $suite_net_rentable_area = $queries->get_suite_net_rentable_area($lang, $row);
        $suite_contiguous_area = $queries->get_suite_contiguous_area_noformat($lang, $row);
        $suite_min_divisible_area = $queries->get_suite_min_divisible($lang, $row);
        $suite_net_rent = $queries->get_suite_net_rent($language, $row);
        $suite_new = $row['new'];
        $suite_model = $row['model'];
        $suite_leased = $row['leased'];
        $suite_promoted = $row['promoted'];
        $row_format = $queries->output_row_format($suite_leased, $suite_promoted);
        $suite_name = $row['suite_name'];
        $suite_id = $row['suite_id'];



        $search = $_REQUEST['webaddr'];  //-!!!!!
        parse_str($search, $output);

        $querystring_rebuilt = http_build_query($output);
        $querystring_rebuilt = str_replace('%3F','',$querystring_rebuilt);
 
        

        $ahrefStr = 'suite.php?suiteid=' . $suite_id . '&building=' . $building_code . '&' . $querystring_rebuilt;


        $suites_html = "";


if ($building_type == "Industrial" || $building_type == "Industriel") {
    

        $suites_html .= '<td class="industrialtabletd" colspan="7">';
        
        $suites_html .='<table class="industrialsearchresults availabilities"><tr class="inforow">';
        
        
        $suites_html .= '<td class="industrialsuite">';
        
        
        
        $suites_html .= '<span' . $row_format . '>';
        
        if ($theme['result_links_to_corp_website'] == 1) {
            if ($mobileview == 1) {
                // normal links to AR suite page if mobile
                $suites_html .= '<a href="' . $ahrefStr . '">' . $suite_name . '</a>';
            } else {
                // not mobile - no site link
                $suites_html .= $suite_name;
            }

        } else {
            // normal links to AR suite page if mobile
            $suites_html .= '<a href="' . $ahrefStr . '">' . $suite_name . '</a>';
        }
        
        if ($suite_new == 'true') {
            $suites_html .= '<span class="label label-danger">';
            $suites_html .= $language['new_text'];
            $suites_html .= '</span></span></td>';
        } else if ($suite_model == 'true') {
            $suites_html .= '<span class="label label-warning">';
            $suites_html .= $language['model_text'];
            $suites_html .= '</span></span></td>';
        } else if ($suite_leased == 'true') {
            $suites_html .= '<span class="label label-primary">';
            $suites_html .= $language['leased_text'];
            $suites_html .= '</span></span></td>';
        } else if ($suite_promoted == 'true') {
            $suites_html .= '<span class="label label-promoted">';
            $suites_html .= $language['promoted_text'];
            $suites_html .= '</span></span></td>';
        } else  {
            $suites_html .= '</span></td>';
        }
        


        
        $suite_contiguous_area_formatted = '';
        if ($lang == "en_CA") {
            $suite_contiguous_area_formatted = number_format($suite_contiguous_area);
        }
        if ($lang == "fr_CA") {
            $suite_contiguous_area_formatted = number_format($suite_contiguous_area, 0, ',', ' ');
        }

        
        
        
        $suites_html .= '<td' . $row_format . ' class="industrialarea">' . $suite_net_rentable_area . ' ' . $language['building_information_sq_ft_trans'];
        
        if ((intval($suite_contiguous_area) != 0) && (intval($suite_min_divisible_area) != 0)) {
            $suites_html .= '<br> (' . $suite_min_divisible_area . ' | ' . $suite_contiguous_area_formatted . ')</td>';
        } else if ((intval($suite_contiguous_area) == 0) && (intval($suite_min_divisible_area) != 0)) {
            $suites_html .= '<br> (' . $suite_min_divisible_area . ')</td>';
        } else if ((intval($suite_contiguous_area) != 0) && (intval($suite_min_divisible_area) == 0)) {
            $suites_html .= '<br> (' . $suite_contiguous_area_formatted . ')</td>';
        } else {
            $suites_html .= '</td>';
        }
        


            $rentclass = "industrialrent";
            $availclass = "industrialavailability";


        $suites_html .= '<td' . $row_format . ' class="'.$rentclass.'">' . $suite_net_rent . '</td>';

        if ($suite_availability != "0000-00-00") {
            $suites_html .= '<td' . $row_format . ' class="'.$availclass.'">' . $suite_availability . '</td>';
        } else {
            $suites_html .= '<td' . $row_format . ' class="'.$availclass.'">' . $immediately . '</td>';
        }


            $suite_specs_query_string = "select shipping_doors_drive_in, shipping_doors_truck, available_electrical_amps, available_electrical_volts, clear_height from suites_specifications where building_id = '".$building_id."' and suite_id = '".$row['suite_id']."'";
            $suite_specs_query = mysql_query($suite_specs_query_string) or die ("Suite specs query error: " . mysql_error());
            $suite_specs = mysql_fetch_array($suite_specs_query);


            $shippingDI = $suite_specs['shipping_doors_drive_in'];
            $shippingTL = $suite_specs['shipping_doors_truck'];
            $poweramps = $suite_specs['available_electrical_amps'];
            $powervolts = $suite_specs['available_electrical_volts'];
            $ceiling = $suite_specs['clear_height'];
            $loadingoutputDI = '';
            $loadingoutputTL = '';
            $loadingoutput = '';

            if ($shippingDI != '' && $shippingDI != 0) {
                $loadingoutputDI = $shippingDI . " " . $language['drivein_loading_abbreviation'];
            } 
            if ($shippingTL != '' && $shippingTL != 0) {
                $loadingoutputTL = $shippingTL . " " . $language['truck_loading_abbreviation'];
            }
            if ($loadingoutputDI != '' && $loadingoutputTL != '') {
                $loadingoutput = $loadingoutputDI . "<br />" . $loadingoutputTL;
            } else if ($loadingoutputDI != '' && $loadingoutputTL == '') {
                $loadingoutput = $loadingoutputDI;
            } else if ($loadingoutputDI == '' && $loadingoutputTL != '') {
                $loadingoutput = $loadingoutputTL;
            } else {
                // both are empty leave a space
                $loadingoutput = "&nbsp;";
            }
            
            $poweroutputAmps = '';
            $poweroutputVolts = '';
            if ($poweramps != '' && $poweramps != 0) {
                $poweroutputAmps = $poweramps . " A";
            } 
            if ($powervolts != '' && $powervolts != 0) {
                $poweroutputVolts = $powervolts . " V";
            }
            if ($poweroutputAmps != '' && $poweroutputVolts != '') {
                $poweroutput = $poweroutputAmps . "<br />" . $poweroutputVolts;
            } else if ($poweroutputAmps != '' && $poweroutputVolts == '') {
                $poweroutput = $poweroutputAmps;
            } else if ($poweroutputAmps == '' && $poweroutputVolts != '') {
                $poweroutput = $poweroutputVolts;
            } else {
                // both are empty leave a space
                $poweroutput = "&nbsp;";
            }
            
            
            
            
    
            $suites_html .= '<td' . $row_format . ' class="industrialloading">' . $loadingoutput . '</td>';
            $suites_html .= '<td' . $row_format . ' class="industrialpower">' . $poweroutput . '</td>';
            $suites_html .= '<td' . $row_format . ' class="industrialceiling">' . ($ceiling !=0 ? $ceiling : '&nbsp;') . '</td>';
            
            $suites_html .= '</td><tr>';
            
            
            
            
            $suites_html .= '<tr class="notesrow"><td colspan="7"' . $row_format . ' class="notes hidden-sm">' . strip_tags($suite_description, '<br>,<br />,<strong>,<u>,<b>,<i>,<ul>,<li>,<ol>,<a>') . "</td></tr></table>";
            
            
            
        

        $suites_html .= "</td></tr>";
        
        

    
    
} else { // retail or office building
    

        $suites_html .= '<td class="suite">';
        $suites_html .= '<span' . $row_format . '>';

        if ($theme['result_links_to_corp_website'] == 1) {
            if ($mobileview == 1) {
                // normal links to AR suite page if mobile
                $suites_html .= '<a href="' . $ahrefStr . '">' . $suite_name . '</a>';
            } else {
                // not mobile - no site link
                $suites_html .= $suite_name;
            }
            
        } else {
            // normal links to AR suite page if mobile
            $suites_html .= '<a href="' . $ahrefStr . '">' . $suite_name . '</a>';
        }
        
        if ($suite_new == 'true') {
            $suites_html .= '<span class="label label-danger">';
            $suites_html .= $language['new_text'];
            $suites_html .= '</span></span></td>';
        } else if ($suite_model == 'true') {
            $suites_html .= '<span class="label label-warning">';
            $suites_html .= $language['model_text'];
            $suites_html .= '</span></span></td>';
        } else if ($suite_leased == 'true') {
            $suites_html .= '<span class="label label-primary">';
            $suites_html .= $language['leased_text'];
            $suites_html .= '</span></span></td>';
        } else if ($suite_promoted == 'true') {
            $suites_html .= '<span class="label label-promoted">';
            $suites_html .= $language['promoted_text'];
            $suites_html .= '</span></span></td>';
        } else  {
            $suites_html .= '</span></td>';
        }
        


        
        $suite_contiguous_area_formatted = '';
        if ($lang == "en_CA") {
            $suite_contiguous_area_formatted = number_format($suite_contiguous_area);
        }
        if ($lang == "fr_CA") {
            $suite_contiguous_area_formatted = number_format($suite_contiguous_area, 0, ',', ' ');
        }
        
        
        
        
        $suites_html .= '<td' . $row_format . ' class="area">' . $suite_net_rentable_area . ' ' . $language['building_information_sq_ft_trans'];
        
        if ((intval($suite_contiguous_area) != 0) && (intval($suite_min_divisible_area) != 0)) {
            $suites_html .= '<br> (' . $suite_min_divisible_area . ' | ' . $suite_contiguous_area_formatted . ')</td>';
        } else if ((intval($suite_contiguous_area) == 0) && (intval($suite_min_divisible_area) != 0)) {
            $suites_html .= '<br> (' . $suite_min_divisible_area . ')</td>';
        } else if ((intval($suite_contiguous_area) != 0) && (intval($suite_min_divisible_area) == 0)) {
            $suites_html .= '<br> (' . $suite_contiguous_area_formatted . ')</td>';
        } else {
            $suites_html .= '</td>';
        }
        
        
        
        
        
            $rentclass = "rent";
            $availclass = "availability";
        
        
        $suites_html .= '<td' . $row_format . ' class="'.$rentclass.'">' . $suite_net_rent . '</td>';

        if ($suite_availability != "0000-00-00") {
            $suites_html .= '<td' . $row_format . ' class="'.$availclass.'">' . $suite_availability . '</td>';
        } else {
            $suites_html .= '<td' . $row_format . ' class="'.$availclass.'">' . $immediately . '</td>';
        }


        

        $suites_html .= '<td' . $row_format . ' class="notes hidden-sm">' . strip_tags($suite_description, '<br>,<br />,<strong>,<u>,<b>,<i>,<ul>,<li>,<ol>,<a>') . "</td>";

        

        $suites_html .= "</tr>";

        
        
        
        
} // end if/else if ($building_type == "Industrial" || $building_type == "Industriel") {
        
        

        return $suites_html;
    }






















	    /**
     * This method removes the concentates the strings for the Suite HTML.
     * Accesses either the abstract or its overriden method from the classes it extends.
     * <p>
     * @param  {String} $building_code
     * @param  {Queries} $queries
     * @param  {Array} $row
     * @param  {Array} $lang
     * @param  {Array} $language
     * @returns {String} $suites_html
     * @see building.php
     * @see index.php
     * */
    public function concatSuiteHTML_buildingpage($building_type, $building_id, $building_code, $queries, $row, $lang, $language, $theme) {

        $suite_description = strip_tags($row['description'], '<p>,<br>,<br />,<strong>,<u>,<b>,<i>,<ul>,<li>,<ol>,<a>');
        $suite_availability = $queries->get_suite_availability($language, $row);
        $suite_net_rentable_area = $queries->get_suite_net_rentable_area($lang, $row);
        $suite_contiguous_area = $queries->get_suite_contiguous_area($lang, $row);
        $suite_min_divisible_area = $queries->get_suite_min_divisible($lang, $row);
        $suite_net_rent = $queries->get_suite_net_rent($language, $row);
        $suite_new = $row['new'];
        $suite_model = $row['model'];
        $suite_leased = $row['leased'];
        $suite_promoted = $row['promoted'];
        $row_format = $queries->output_row_format($suite_leased, $suite_promoted);
        $suite_name = $row['suite_name'];
        $suite_id = $row['suite_id'];


        $fixStr = str_replace('&building_name=' . $building_code , '', $_SERVER['QUERY_STRING']);
        $ahrefStr = 'suite.php?suiteid=' . $suite_id . '&building=' . $building_code . '&' . $fixStr;
        $ahrefStr = rtrim($ahrefStr, "&");


        $suites_html = "";

        $suites_html .= '<td>';
        
        $suites_html .= '<span' . $row_format . '>';
        if ($theme['result_links_to_corp_website'] == 1) {
            $suites_html .= $suite_name;
        } else {
            $suites_html .= '<a href="' . $ahrefStr . '">' . $suite_name . '</a>';
        }
        
        if ($suite_new == 'true') {
            $suites_html .= '<span class="label label-danger">';
            $suites_html .= $language['new_text'];
            $suites_html .= '</span></span></td>';
        } else if ($suite_model == 'true') {
            $suites_html .= '<span class="label label-warning">';
            $suites_html .= $language['model_text'];
            $suites_html .= '</span></span></td>';
        } else if ($suite_leased == 'true') {
            $suites_html .= '<span class="label label-primary">';
            $suites_html .= $language['leased_text'];
            $suites_html .= '</span></span></td>';
        } else if ($suite_promoted == 'true') {
            $suites_html .= '<span class="label label-promoted">';
            $suites_html .= $language['promoted_text'];
            $suites_html .= '</span></span></td>';
        } else {
            $suites_html .= '</span></td>';
        }
		

        $suites_html .= '<td' . $row_format . ' class="area">' . $suite_net_rentable_area . ' ' . $language['building_information_sq_ft_trans'];
        
        if ((intval($suite_contiguous_area) != 0) && (intval($suite_min_divisible_area) != 0)) {
            $suites_html .= '<br> (' . $suite_min_divisible_area . ' | ' . $suite_contiguous_area . ')</td>';
        } else if ((intval($suite_contiguous_area) == 0) && (intval($suite_min_divisible_area) != 0)) {
            $suites_html .= '<br> (' . $suite_min_divisible_area . ')</td>';
        } else if ((intval($suite_contiguous_area) != 0) && (intval($suite_min_divisible_area) == 0)) {
            $suites_html .= '<br> (' . $suite_contiguous_area . ')</td>';
        } else {
            $suites_html .= '</td>';
        }
            
            
        $suites_html .= '<td' . $row_format . ' class="rent">' . $suite_net_rent . '</td>';




        if ($suite_availability != "0000-00-00") {
            $suites_html .= '<td' . $row_format . ' class="availability">' . $suite_availability . '</td>';
        } else {
            $suites_html .= '<td' . $row_format . ' class="immediate">' . $immediately . '</td>';
        }



        
        if ($building_type == "Industrial" || $building_type == "Industriel") {
        
            
            $suite_specs_query_string = "select shipping_doors_drive_in, shipping_doors_truck, available_electrical_amps, available_electrical_volts, clear_height from suites_specifications where building_id = '".$building_id."' and suite_id = '".$suite_id."'";
            $suite_specs_query = mysql_query($suite_specs_query_string) or die ("Suite specs query error: " . mysql_error());
            $suite_specs = mysql_fetch_array($suite_specs_query);


            $shippingDI = $suite_specs['shipping_doors_drive_in'];
            $shippingTL = $suite_specs['shipping_doors_truck'];
            $poweramps = $suite_specs['available_electrical_amps'];
            $powervolts = $suite_specs['available_electrical_volts'];
            $ceiling = $suite_specs['clear_height'];
            $loadingoutputDI = '';
            $loadingoutputTL = '';
            $loadingoutput = '';

            if ($shippingDI != '' && $shippingDI != 0) {
                $loadingoutputDI = $shippingDI . " " . $language['drivein_loading_abbreviation'];
            } 
            if ($shippingTL != '' && $shippingTL != 0) {
                $loadingoutputTL = $shippingTL . " " . $language['truck_loading_abbreviation'];
            }
            if ($loadingoutputDI != '' && $loadingoutputTL != '') {
                $loadingoutput = $loadingoutputDI . "<br />" . $loadingoutputTL;
            } else if ($loadingoutputDI != '' && $loadingoutputTL == '') {
                $loadingoutput = $loadingoutputDI;
            } else if ($loadingoutputDI == '' && $loadingoutputTL != '') {
                $loadingoutput = $loadingoutputTL;
            } else {
                // both are empty leave a space
                $loadingoutput = '&nbsp;';
            }
    
            $poweroutputAmps = '';
            $poweroutputVolts = '';
            if ($poweramps != '' && $poweramps != 0) {
                $poweroutputAmps = $poweramps . " A";
            } 
            if ($powervolts != '' && $powervolts != 0) {
                $poweroutputVolts = $powervolts . " V";
            }
            if ($poweroutputAmps != '' && $poweroutputVolts != '') {
                $poweroutput = $poweroutputAmps . "<br />" . $poweroutputVolts;
            } else if ($poweroutputAmps != '' && $poweroutputVolts == '') {
                $poweroutput = $poweroutputAmps;
            } else if ($poweroutputAmps == '' && $poweroutputVolts != '') {
                $poweroutput = $poweroutputVolts;
            } else {
                // both are empty leave a space
                $poweroutput = "&nbsp;";
            }
            
            $suites_html .= '<td' . $row_format . ' class="loading">' . $loadingoutput . '</td>';
            $suites_html .= '<td' . $row_format . ' class="power">' . $poweroutput . '</td>';
            $suites_html .= '<td' . $row_format . ' class="ceiling">' . $ceiling . '</td>';
            
            $suites_html .= '<td colspan="7"' . $row_format . ' class="notes hidden-sm">' . strip_tags($suite_description, '<br>,<br />,<strong>,<u>,<b>,<i>,<ul>,<li>,<ol>,<a>') . "</td>";
            
            
            
        } else {
        $suites_html .= '<td' . $row_format . ' class="notes hidden-sm">' . strip_tags($suite_description, '<br>,<br />,<strong>,<u>,<b>,<i>,<ul>,<li>,<ol>,<a>') . "</td>";
        }








        $suites_html .= "</tr>";


        return $suites_html;
    }
}

?>
