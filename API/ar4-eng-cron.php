<?php

// Need this connection to access the 'crons' database
mysql_connect('localhost', 'dbmaster', 'zu:b@px', true);
$link = mysql_connect('localhost', 'dbmaster', 'zu:b@px', true);
mysql_select_db('crons');
mysql_set_charset('utf8', $link);

$clientname = $_REQUEST['clientname'];




// Cron is now controlled by the DB
// Client specific data is stored in the 'crons' database
// "source_account" is for data scraping.
// value 1 means direct-scraped account and do not populate from CoRE.
$cron_query_statement = "Select * from crondata where client_name = '" . $clientname . "' and active=1 and ar4=1 and source_account!=1";
$cron_query = mysql_query($cron_query_statement)or die("cron query error: " . mysql_error());

$cron = mysql_fetch_array($cron_query);
$base_file_path = $cron['base_file_path'];
$dbuser = trim($cron['db_username']);
$dbpass = trim($cron['db_password']);
$dbname = trim($cron['db_name']);
$clientname = $cron['client_name'];


$client_official_name = $cron['official_name'];

require('HTTPRequestString.php');

// change this line if moving servers/domains
$base_proxy_url = $cron['vacancy_report_website'] . "/proxy/php/proxy.php?clientname=" . $clientname . "&proxy_url=";


// Initialize errorcode
$errorcode = 0;
// Initialize email line break variable
$eol = "\n";

//ENGLISH
$lang_id = 1;

//set time in motion
date_default_timezone_set("Canada/Eastern");


$apiuser = $cron['api_username'];
$apipass = $cron['api_password'];
$apiurl = str_replace('https://', '', $cron['api_hostname']);
if ($errorcode != 0) {
    echo "Errorcode start: " . $errorcode . "\n\r\n\r";
}
//	$url = 'https://'. $apiuser .':'.$apipass.'@'.$apiurl.'/api/buildingsAndSuites';
////	$url = 'https://'.$apiurl.'/api/buildingsAndSuites';
////	$url = 'https://'.$apiurl;
//	echo "URL: " . $url . "\n\r\n\r";
//	$request = new HttpRequest($url);
//	$request->send();
//	$httpresponse = $request->getResponseCode();
//		echo "httpresponse: " . $httpresponse ."\n\r\n\r";
//	if (($httpresponse != '200') && ($httpresponse != '302')) {
//		// THE checking above does not work. Disabling this one for the time being.
//		//$errorcode = 1;
//	} 

if ($errorcode != 0) {
    echo "(disabled-ignore) Errorcode after first: " . $errorcode . "\n\r\n\r";
}
$url2 = 'https://core.arcestra.com';
$headers = get_headers($url2, 1);

if (($headers[0] == 'HTTP/1.1 200 OK') || ($headers[0] == 'HTTP/1.1 302 Found')) {
    //valid 
//		echo "headers OK: " . $headers[0] . "\n\r\n\r";
} else {
    //return error code.
    if ($errorcode != 1) {
        $errorcode = 2;
    }
}

if ($errorcode != 0) {
    echo "Errorcode after second: " . $errorcode . "\n\r\n\r";
}

// DB Reporting function
// For testing/reporting/etcetera-ing
function db_report($function_name, $event, $note) {
    $db_report_query_string = "insert into cronreports set function_name = '" . $function_name . "', event = '" . $event . "', note = '" . $note . "'";
    $db_report_query = mysql_query($db_report_query_string) or die("db report error: " . mysql_error());
}

// Function to scrape XML from Arcestra
function getXMLObject($url_suffix, $base) {
    $url = $base . $url_suffix;
    $r = new HTTPRequestString($url);
    $string_to_parse = $r->DownloadToString();

// Check for XML stream
    if (strpos($string_to_parse, 'Stream Error:')) {
// return error code.
    }
// Must remove inappropriate dashes from element names
    $string_to_parse = str_replace("postal-code", "postalcode", $string_to_parse);
    $string_to_parse = str_replace("total-space", "totalspace", $string_to_parse);
    $string_to_parse = str_replace("parking-area", "parkingarea", $string_to_parse);
    $string_to_parse = str_replace("parking-ratio", "parkingratio", $string_to_parse);
    $string_to_parse = str_replace("occupied-space", "occupiedspace", $string_to_parse);
    $string_to_parse = str_replace("building-type", "buildingtype", $string_to_parse);
    $string_to_parse = str_replace("flyer-header", "flyerheader", $string_to_parse);
    $string_to_parse = str_replace("preview-images", "previewimages", $string_to_parse);
    $string_to_parse = str_replace("plans-2d", "plans2d", $string_to_parse);
    $string_to_parse = str_replace("suite-id", "suiteid", $string_to_parse);

    //NEW API names...
    $string_to_parse = str_replace("site-description", "sitedescription", $string_to_parse);
    $string_to_parse = str_replace("building-size", "buildingsize", $string_to_parse);
    $string_to_parse = str_replace("safety-access", "safetyaccess", $string_to_parse);
    $string_to_parse = str_replace("tenant-costs", "tenantcosts", $string_to_parse);
    $string_to_parse = str_replace("office-parking", "officeparking", $string_to_parse);
    $string_to_parse = str_replace("office-tenants", "officetenants", $string_to_parse);
    $string_to_parse = str_replace("public-transport", "publictransport", $string_to_parse);
    $string_to_parse = str_replace("hvac-distribution-system-description", "hvacdistributionsystemdescription", $string_to_parse);
    $string_to_parse = str_replace("parking-description", "parkingdescription", $string_to_parse);
    $string_to_parse = str_replace("plans-3d", "plans3d", $string_to_parse);
    $string_to_parse = str_replace("company-id", "companyid", $string_to_parse);
    /*
      $string_to_parse = str_replace("&lt;![CDATA[", "", $string_to_parse);
      $string_to_parse = str_replace("]]&gt;", "", $string_to_parse);
      $string_to_parse = str_replace("<![CDATA[", "", $string_to_parse);
      $string_to_parse = str_replace("]]>", "", $string_to_parse);
      $string_to_parse = str_replace("&", "&amp;", $string_to_parse);
     */


//return simplexml_load_string($string_to_parse);
    return simplexml_load_string($string_to_parse, null, LIBXML_NOCDATA);
}

//building details here:		
$building_xml = getXMLObject("api/buildingsAndSuites", $base_proxy_url);

// Get CoRE ID from root XML node, write to crons.crondata before connecting to client db
$client_core_id = $building_xml['companyid'];
// update crondata with CoRE Client ID
$update_clientID_query_string = "update crondata set core_id = '" . $client_core_id . "' where client_name = '" . $clientname . "'";
$update_clientID_query = mysql_query($update_clientID_query_string) or die("CoRE ID Update Failed: " . mysql_error());






// connect with current client's db credentials
mysql_connect('localhost', $dbuser, $dbpass, true);
$link = mysql_connect('localhost', $dbuser, $dbpass, true);
mysql_select_db($dbname);
mysql_set_charset('utf8', $link);


// Assign CoRE ID in an accessible spot in each client's database
// languages_dynamic.coreID

$updateCoREID_query_String = "update languages_dynamic set coreID = '" . $client_core_id . "'";
$updateCoREID_query = mysql_query($updateCoREID_query_String) or die("update CoRE ID Error: " . mysql_error());


//mark all the buildings dirty
$dirty_buildings = mysql_query("UPDATE buildings SET dirty=1 WHERE building_index > 0 and lang=" . $lang_id);

// TourBook
//Mark all buildings' accounts as dirty
$dirty_building_accounts = mysql_query("UPDATE temp_accounts SET dirty=1");
//END TourBook

$building_id_num = 0;
$new_tempacct_created = 0;
//echo "Building counter start: " . $building_id_num . "\n\r\n\r";


function convert_smart_quotes($string) 

{ 
    $search = array(chr(145), 
                    chr(146), 
                    chr(147), 
                    chr(148), 
                    chr(151)); 

    $replace = array("'", 
                     "'", 
                     '"', 
                     '"', 
                     '-'); 

    return str_replace($search, $replace, $string); 
} 


// Main Building Loop Starts
foreach ($building_xml as $Building) {
//echo "\n\r\n\r\n\r\n\r==================NEW BUILDING====================\n\r\n\r";
    $suitecount = 0;
// TourBook
    $building_id = "{$Building['building-id']}";
//END TourBook

    $building_id_num = $building_id_num + 1;

    

    
 
// Create variables from XML
    $building_name = "{$Building['name']}";
//        echo "Building Name: --|".$building_name."|--\n\r\n\r";
    $building_name = trim($building_name);
    $building_name = rtrim($building_name);
    
    // unsure if this really works or not
    $building_name = convert_smart_quotes($building_name);
    $building_name = str_replace("’", "'", $building_name);
    
//        $building_name = preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $building_name);
    $building_code = "{$Building['building-code']}";
    $building_type = "{$Building->specifications->buildingType['primary-purpose']}";
    $building_street = "{$Building['street']}";
    $building_city = "{$Building['city']}";
    $building_country = "{$Building['country']}";
    $building_region = "{$Building['region']}";
    $building_sub_region = "{$Building['regionSub']}";
    $building_province = "{$Building['longProvince']}";
    $building_province_code = "{$Building['province']}";
    $building_postal_code = "{$Building['postalcode']}";
    $building_total_space = "{$Building['totalspace']}";
    $building_office_area = "{$Building['office-area']}";
    $building_retail_area = "{$Building['retail-area']}";
    $building_industrial_area = "{$Building['industrial-area']}";
    $building_year = "{$Building->specifications->general['year-built']}";
    $building_floors_above_ground = "{$Building['floorsAboveGround']}";
    $building_floors_below_ground = "{$Building['floorsBelowGround']}";

    $building_measurement_unit = "{$Building->specifications->buildingSize['measurement-units']}";
    $building_desc = "{$Building->description}";
    $building_contact = "{$Building->contactInfo}";
    $building_header = "{$Building->flyerHeader}";
    $building_featured_plan = "{$Building->plan}";
    $building_url = "{$Building['externalUrl']}";
    $building_thumbnail = '';
    $building_thumbnail = "{$Building->thumbnail}";
//        if ($building_thumbnail == '') {
//            $building_thumbnail = "EMPTY";
//        }
//echo "BT: " . $building_thumbnail . "\n\r\n\r";
    
    
    $building_specs_boma = "{$Building->specifications->buildingType['boma-best']}";
    $building_specs_leed = "{$Building->specifications->buildingType['building-leed']}";
    
    
    $building_parking_stalls = "{$Building->specifications->parking['total-parking-stalls']}";
    $building_parking_ratio = "{$Building->specifications->parking['surface-ratio']}";
    if ($building_parking_ratio == "1/" || $building_parking_ratio == "/" || $building_parking_ratio == '\\') {
        $building_parking_ratio = "";
    }
    $building_available_space = "{$Building['available-space']}";
    $building_typical_floor_size = "{$Building['typicalFloorSize']}";
    $building_orientation = "{$Building['orientation']}";
    $building_modification_date = "{$Building['modification-date']}";
    $building_add_rent_operating = "{$Building->specifications->tenantCosts['operating-costs']}";
    $building_add_rent_realty = "{$Building->specifications->tenantCosts['realty-tax']}";
    $building_add_rent_power = "{$Building->specifications->tenantCosts['power']}";
    $building_add_rent_total = "{$Building->specifications->tenantCosts['additional-rent-total']}";
    $building_power_text = "{$Building->specifications->tenantCosts['typical-power-watts']}";
    $building_truck_doors = "{$Building->specifications->technical['shipping-doors-truck']}";
    $building_drivein_doors = "{$Building->specifications->technical['shipping-doors-drivein']}";
    $building_clear_height = "{$Building->specifications->technical['ceiling-height']}";
    $building_new_development = "{$Building->specifications->buildingType['new-development']}";
    
    $building_fund_name = "{$Building->specifications->buildingType['fund-name']}";
    $building_measurement_units = "{$Building->specifications->buildingType['measurement-units']}";
    $building_internal_identifier = "{$Building->specifications->buildingType['internal-identifier']}";
    $building_mall_type = "{$Building->specifications->buildingType['mall-type']}";
    $building_costs_posted_net_rate = "{$Building->specifications->tenantCosts['posted-net-rate']}";
    $building_costs_realty = "{$Building->specifications->tenantCosts['realty-tax']}";
    $building_costs_power = "{$Building->specifications->tenantCosts['power']}";
    $building_costs_operating = "{$Building->specifications->tenantCosts['operating-costs']}";
//	$building_costs_power = "{$Building->specifications->tenantCosts['utilities']}";
    $building_costs_add_rent_total = "{$Building->specifications->tenantCosts['additional-rent']}";
    if ($building_costs_add_rent_total == "") {
        $building_costs_add_rent_total = "{$Building->specifications->tenantCosts['additional-rent-total']}";
    }
    $building_costs_other = "{$Building->specifications->tenantCosts['other']}";
    $building_costs_power_watts = "{$Building->specifications->tenantCosts['typical-power-watts']}";
    $building_costs_power_lighting = "{$Building->specifications->tenantCosts['lighting']}";
    $building_zoned = "{$Building->specifications->general['building-zoned']}";
    $building_renovated = "{$Building->specifications->general['last-renovated']}";
    $building_owned = "{$Building->specifications->general['owned-building']}";
    $building_managed = "{$Building->specifications->general['managed-building']}";
    $building_management = "{$Building->specifications->general['building-management']}";
    $building_website = "{$Building->specifications->general['website']}";
//only contained in industrial api results
    $building_average_office = "{$Building->specifications->general['average-office']}";
    $building_number_of_buildings = "{$Building->specifications->general['number-of-buildings']}";
    $building_number_of_units = "{$Building->specifications->general['number-of-units']}";
    $building_primary_usage = "{$Building->specifications->general['primary-usage']}";
    $building_secondary_usage = "{$Building->specifications->general['secondary-usage']}";
    $building_floors = "{$Building->specifications->buildingSize['number-of-floors']}";
    if ($building_floors == "") {
        $building_floors = "{$Building->specifications->buildingSize['number-of-levels']}";
    }
    $building_retail_units = "{$Building->specifications->buildingSize['number-of-retail-units']}";
    if ($building_retail_units == "") {
        $building_retail_units = "{$Building->specifications->buildingSize['number-of-stores']}";
    }
    $building_retail_unit_size = "{$Building->specifications->buildingSize['retail-unit-size']}";
    $building_highrise_floor_plate = "{$Building->specifications->buildingSize['high-rise-floor-plate']}";
    $building_lowrise_floor_plate = "{$Building->specifications->buildingSize['low-rise-floor-plate']}";
    $building_highrise_gross_up = "{$Building->specifications->buildingSize['highrise-gross-up']}";
    $building_lowrise_gross_up = "{$Building->specifications->buildingSize['lowrise-gross-up']}";
//only in retail api results
    $building_retail_food_court = "{$Building->specifications->buildingSize['food-court']}";
    $building_retail_num_food_units = "{$Building->specifications->buildingSize['number-of-food-units']}";
    $building_retail_food_court_seating = "{$Building->specifications->buildingSize['food-court-seating']}";
    $building_total_office_space = "{$Building->specifications->buildingSize['total-office-space']}";
    $building_total_retail_space = "{$Building->specifications->buildingSize['total-retail-space']}";
//industrial only fields
    $building_site_coverage = "{$Building->specifications->buildingSize['site-coverage']}";
    $building_total_hectares = "{$Building->specifications->buildingSize['total-hectares']}";
    $building_total_acreage = "{$Building->specifications->buildingSize['total-acreage']}";
    $building_total_industrial_space = "{$Building->specifications->buildingSize['total-industrial-space']}";
    $building_available_industrial_space = "{$Building->specifications->buildingSize['available-industrial-space']}";
    $building_manufacturing_size = "{$Building->specifications->buildingSize['manufacturing-size']}";
    $building_warehouse_size = "{$Building->specifications->buildingSize['warehouse-size']}";
    $building_available_office_space = "{$Building->specifications->buildingSize['available-office-space']}";
    $building_available_retail_space = "{$Building->specifications->buildingSize['available-retail-space']}";
    $building_occupied_space = "{$Building->specifications->buildingSize['occupied-space']}";
    $building_largest_contiguous_available = "{$Building->specifications->buildingSize['largest-contiguous-available']}";
    $building_const_typical_power = "{$Building->specifications->construction['typical-power']}";
    $building_const_lighting = "{$Building->specifications->construction['lighting']}";
    $building_const_ceiling_height = "{$Building->specifications->construction['ceiling-height']}";
    if ($building_const_ceiling_height == "") {
        $building_const_ceiling_height = "{$Building->specifications->technical['ceiling-height']}";
    }
    $building_const_interior_wall_type = "{$Building->specifications->construction['interior-wall-type']}";
    $building_const_exterior_finish = "{$Building->specifications->construction['exterior-finish']}";
    $building_const_washrooms_per_floor = "{$Building->specifications->construction['washrooms-per-floor']}";
    $building_const_satellite_dish_capability = "{$Building->specifications->construction['satellite-dish-capability']}";
    $building_const_fibre_optic_capability = "{$Building->specifications->construction['fibre-optic-capability']}";
    $building_const_shipping_receiving = "{$Building->specifications->construction['shipping-receiving']}";
    $building_const_emergency_generator = "{$Building->specifications->construction['emergency-generator']}";
    $building_const_after_hours_hvac = "{$Building->specifications->construction['after-hours-hvac']}";
    $building_const_hvac_hours_of_operation = "{$Building->specifications->construction['hvac-hours-of-operation']}";
    $building_const_hvac_description = "{$Building->specifications->construction->hvacDistributionSystemDescription}";
//only industrial in technical
    $building_const_max_door_height = "{$Building->specifications->technical['max-door-height']}";
    $building_const_shipping_doors_drivein = "{$Building->specifications->technical['shipping-doors-drivein']}";
    $building_const_shipping_doors_truck = "{$Building->specifications->technical['shipping-doors-truck']}";
    $building_const_rail_loading = "{$Building->specifications->technical['rail-loading']}";
    $building_const_building_plan = "{$Building->specifications->technical['building-plan']}";
    $building_const_peer_review = "{$Building->specifications->technical['peer-review']}";
    $building_const_survey = "{$Building->specifications->technical['survey']}";
    $building_const_drycleaning_solvents = "{$Building->specifications->technical['dry-cleaning-solvents']}";
    $building_const_asbestos_free = "{$Building->specifications->technical['asbestos-free']}";
//only industrial in construction
    $building_const_roofing_type = "{$Building->specifications->construction['roofing-type']}";
    $building_const_available_electrical_volts = "{$Building->specifications->construction['available-electrical-volts']}";
    $building_const_available_electrical_amps = "{$Building->specifications->construction['available-electrical-amps']}";
    $building_const_power_text = "{$Building->specifications->construction['powerText']}";
    $building_elevators_hirise = "{$Building->specifications->elevators['high-rise']}";
    $building_elevators_midrise = "{$Building->specifications->elevators['mid-rise']}";
    $building_elevators_lowrise = "{$Building->specifications->elevators['low-rise']}";
    $building_elevators_parking = "{$Building->specifications->elevators['parking']}";
    $building_elevators_freight = "{$Building->specifications->elevators['freight']}";
    $building_safety_fire_detection = "{$Building->specifications->safetyAccess['fire-detection-system']}";
    $building_safety_sprinkler_system = "{$Building->specifications->safetyAccess['sprinkler-system']}";
    $building_safety_manned_security = "{$Building->specifications->safetyAccess['manned-security']}";
    $building_safety_security_system = "{$Building->specifications->safetyAccess['security-system']}";
    $building_safety_barrier_free = "{$Building->specifications->safetyAccess['barrier-free-access']}";
    $building_parking_surface_stalls = "{$Building->specifications->parking['surface-stalls']}";
    $building_parking_surface_ratio = "{$Building->specifications->parking['surface-ratio']}";
    $building_parking_above_stalls = "{$Building->specifications->parking['above-ground-stalls']}";
    $building_parking_above_ratio = "{$Building->specifications->parking['above-ground-ratio']}";
    $building_parking_below_stalls = "{$Building->specifications->parking['below-ground-stalls']}";
    $building_parking_below_ratio = "{$Building->specifications->parking['below-ground-ratio']}";
    $building_parking_total_stalls = "{$Building->specifications->parking['total-parking-stalls']}";
    $building_parking_cost_day = "{$Building->specifications->parking['parking-cost-per-day']}";
    $building_parking_cost_month = "{$Building->specifications->parking['parking-cost-per-month']}";
    $building_parking_description = "{$Building->specifications->parking->parkingDescription}";
    $building_anchor_tenant1 = "{$Building->specifications->anchorTenants['anchor-tenant-1']}";
    $building_anchor_tenant2 = "{$Building->specifications->anchorTenants['anchor-tenant-2']}";
    $building_anchor_tenant3 = "{$Building->specifications->anchorTenants['anchor-tenant-3']}";
    $building_anchor_tenant4 = "{$Building->specifications->anchorTenants['anchor-tenant-4']}";
    $building_anchor_tenant5 = "{$Building->specifications->anchorTenants['anchor-tenant-5']}";
    $building_anchor_tenant6 = "{$Building->specifications->anchorTenants['anchor-tenant-6']}";
    $building_public_surface_route = "{$Building->specifications->publicTransport['transit-surface-route']}";
    $building_public_subway_access = "{$Building->specifications->publicTransport['direct-subway-access']}";
    $building_operating_hours_M = "{$Building->specifications->operatingHours['monday']}";
    $building_operating_hours_T = "{$Building->specifications->operatingHours['tuesday']}";
    $building_operating_hours_W = "{$Building->specifications->operatingHours['wednesday']}";
    $building_operating_hours_R = "{$Building->specifications->operatingHours['thursday']}";
    $building_operating_hours_F = "{$Building->specifications->operatingHours['friday']}";
    $building_operating_hours_S = "{$Building->specifications->operatingHours['saturday']}";
    $building_operating_hours_D = "{$Building->specifications->operatingHours['sunday']}";
    $building_operating_hours_notes = "{$Building->specifications->operatingHours->specialNotes}";
    $building_primary_population = "{$Building->specifications->demographics->primaryTrade['population']}";
    $building_primary_number_of_households = "{$Building->specifications->demographics->primaryTrade['number-of-households']}";
    $building_primary_average_household_income = "{$Building->specifications->demographics->primaryTrade['average-household-income']}";
    $building_secondary_population = "{$Building->specifications->demographics->secondaryTrade['population']}";
    $building_secondary_number_of_households = "{$Building->specifications->demographics->secondaryTrade['number-of-households']}";
    $building_secondary_average_household_income = "{$Building->specifications->demographics->secondaryTrade['average-household-income']}";
    $building_customer_household_income = "{$Building->specifications->demographics->customerProfile['average-household-income']}";
    $building_customer_median_age = "{$Building->specifications->demographics->customerProfile['median-age']}";
    $building_customer_person_per_household = "{$Building->specifications->demographics->customerProfile['person-per-household']}";
    $building_traffic_pedestrian_traffic = "{$Building->specifications->demographics->trafficProfile['annual-pedestrian-traffic']}";
    $building_traffic_demographic_source = "{$Building->specifications->demographics->trafficProfile->demographicSource}";
    $building_number_of_stores = "{$Building->specifications->buildingSize['number-of-stores']}";

    $building_marshalling_area = "{$Building->specifications->technical['marshalling_area']}";
    $building_dolly_pad = "{$Building->specifications->technical['dolly_pad']}";
    $building_environmental_certification_notes = "{$Building->specifications->buildingType['certification']}";
    $building_outside_storage = "{$Building->specifications->technical['outside_storage']}";
    $building_storage_notes = "{$Building->specifications->technical->storageDescription}";






    $building_marketing_text = "{$Building->marketingText}";
    $building_marketing_text = addslashes($building_marketing_text);
    $building_latitude = "{$Building['latitude']}";
    $building_longitude = "{$Building['longitude']}";
    $building_address_number = '';
    $building_address_street = '';
    list($building_address_number, $building_address_street) = explode(' ', $building_street, 2);

//**************************************************************//
//**************************************************************//
//**************************************************************//
//**************************************************************//
// THIS IS WHERE WE FILTER THE CRON
//EG: 
// WE ONLY WANT TORONTO OFFICES. NOTHING ELSE
//This worked for CBRE:
//if ($building_city == "Toronto" && $building_type == "Office") {
// This is both database-driven AND optional.
//**************************************************************//
//**************************************************************//
//**************************************************************//
//**************************************************************//
    $n = false; // import this building
    if ($cron['limit_data_scrape'] == 1) {
        // this feature is clearly incomplete. Currently we only need to filter by spacetype for the dream AR3 accounts.
        // this is sufficient for now but once we add elements to filter by this will become complicated quickly.

        $n = true; // Default: Do not import this building
        // limit by spacetype
        if ($cron['limit_spacetype_eng'] != null) {
            if ($building_type == $cron['limit_spacetype_eng']) {
                $n = false; // import this building
            }
        }

        // limit by province
        if ($cron['limit_province_eng'] != null) {
            if ($building_province == $cron['limit_province_eng']) {
                $n = false; // import this building
            }
        }



        if ($cron['limit_spacetype_newdevelopments'] != 'false') {
            if ($building_new_development == $cron['limit_spacetype_newdevelopments']) {
                $n = false; // import this building
            }
        }

        if ($cron['limit_fundname'] != null) {
            if ($building_fund_name == $cron['limit_fundname']) {
                $n = false; // import this building
            }
        }



//		if (($building_city == $cron['limit_city_eng']) && ($building_type == $cron['limit_spacetype_eng'])) {
//			$n = false; // import this building
//		}
    }

// carry on with filtered (or not) data.
    if ($n == false) {



// only make temp accounts if the source 
// is a real landlord and not a scraped account.
        if ($cron['source_account'] == '0') {

//TourBook
            $temp_account_qy_string = "select account_index from temp_accounts where building_id=" . $building_id;
//	echo $temp_account_qy_string . "/n/r";
            $temp_account_qy = mysql_query($temp_account_qy_string) or die("temp acct query error: " . mysql_error());
            $tempexists = mysql_num_rows($temp_account_qy);

            $exists = 0;
            if ($tempexists > 0) {
                $exists = 1;
                $undirty_query_string = "update temp_accounts set dirty=0 where building_id='" . $building_id . "'";
                $undirty_query = mysql_query($undirty_query_string) or die('Undirty Temp Account Query error: ' . mysql_error());
            } else {
                $exists = 0;
                $api_string = urlencode("api/temporaryAccount/BUILDING/" . $building_id . "/2023-01-01 11:00:00 EST");
                $temp_account_xml = getXMLObject($api_string, $base_proxy_url);
                $account_user = $temp_account_xml->attributes()->username;
                $account_pass = $temp_account_xml->attributes()->password;
                $expiry_date = $temp_account_xml->attributes()->expiredTime;
                $download_link = "arcestratourbook://%7B%22url%22%3A%22http%3A%2F%2Fapi.arcestra.com%2Fapi%2Frpc%2Fbuilding%2F" . $building_id . "%22%2C%22RPC%22%3A%22syncBuilding%22%2C%22expiryTime%22%3A%22" . '2023-01-01 11:00:00 EST' . "%22%2C%22password%22%3A%22" . $account_pass . "%22%2C%22suite%22%3A%22%22%2C%22resourceType%22%3A%22BUILDING%22%2C%22username%22%3A%22" . $account_user . "%22%7D";


                $newtempacct_query_string = "INSERT INTO temp_accounts (building_id, building_code, temp_account_user, temp_account_pass, expiry_time, download_link, dirty) VALUES('" . $building_id . "', '" . $building_code . "', '" . $account_user . "', '" . $account_pass . "', '" . $expiry_date . "', '" . $download_link . "', 0)";
                mysql_query($newtempacct_query_string) or die("New Temp Account Insert error: " . mysql_error());

                $new_tempacct_created++;
            }
// END TourBook		
        }




////THE GOOGLE MAPS IMAGE DOWNLOAD FOR ENGLISH BEGINS HERE
        $static_map_name = $building_id . ".png";
        $static_map_11_name = $building_id . "_zoom11.png";
        $static_map_13_name = $building_id . "_zoom13.png";

//$static_map_name  = str_replace(" ", "_", $static_map_name );
        $path = $base_file_path . "images/twodee_files/" . $static_map_name;
        $path_11 = $base_file_path . "images/twodee_files/" . $static_map_11_name;
        $path_13 = $base_file_path . "images/twodee_files/" . $static_map_13_name;
        $staticMapKey = "AIzaSyB2OmSI_tgDizQlc_YDb0I4JI34Sj21aXo";
        if (!file_exists($path)) {

            $maplink = "http://maps.googleapis.com/maps/api/staticmap?size=490x211&zoom=13&format=png32&markers=color:blue|" . urlencode($building_street) . "%2C" . urlencode($building_city) . "%2C" . urlencode($building_province) . "&sensor=false&key=" . $staticMapKey;
            $maplink_11 = "http://maps.googleapis.com/maps/api/staticmap?size=800x600&zoom=11&format=png32&markers=color:blue|" . urlencode($building_street) . "%2C" . urlencode($building_city) . "%2C" . urlencode($building_province) . "&sensor=false&key=" . $staticMapKey;
            $maplink_13 = "http://maps.googleapis.com/maps/api/staticmap?size=800x600&zoom=13format=png32&markers=color:blue|" . urlencode($building_street) . "%2C" . urlencode($building_city) . "%2C" . urlencode($building_province) . "&sensor=false&key=" . $staticMapKey;


            $newfname = $path;
            $file = fopen($maplink, "rb");
            if ($file) {
                $newf = fopen($newfname, "wb");

                if ($newf)
                    while (!feof($file)) {
                        fwrite($newf, fread($file, 1024 * 8), 1024 * 8);
                    }
            }

            if (is_resource($file)) {
                fclose($file);
            }

            if (is_resource($newf)) {
                fclose($newf);
            }


            $newfname_11 = $path_11;
            $file_11 = fopen($maplink_11, "rb");
            if ($file_11) {
                $newf_11 = fopen($newfname_11, "wb");

                if ($newf_11)
                    while (!feof($file_11)) {
                        fwrite($newf_11, fread($file_11, 1024 * 8), 1024 * 8);
                    }
            }

            if (is_resource($file_11)) {
                fclose($file_11);
            }

            if (is_resource($newf_11)) {
                fclose($newf_11);
            }


            $newfname_13 = $path_13;
            $file_13 = fopen($maplink_13, "rb");
            if ($file_13) {
                $newf_13 = fopen($newfname_13, "wb");

                if ($newf_13)
                    while (!feof($file_13)) {
                        fwrite($newf_13, fread($file_13, 1024 * 8), 1024 * 8);
                    }
            }

            if (is_resource($file_13)) {
                fclose($file_13);
            }

            if (is_resource($newf_13)) {
                fclose($newf_13);
            }
        }

//THE GOOGLE MAPS IMAGE DOWNLOAD FOR ENGLISH ENDS HERE

        
        
        
        
// DOWNLOAD BUILDING FLYER HEADER
        $header_file_name = "";
        $hero_file_name = "";
        $hero_cropped_name = "";
        $hero_file_name_small = "";
        
                if ($building_header != '') {
//                    echo $eol . 'BUILDING HEADER NOT EMPTY' . $eol.$eol;
                    $header_file_name = "building_header_".$building_code.".jpg";
                    $hero_file_name = "building_hero_".$building_code.".jpg";
                    $hero_cropped_name = "building_hero_crop_".$building_code.".jpg";
                    $hero_file_name_small = "building_hero_small_".$building_code.".jpg";
                    $hero_url = $building_header . "?width=1920&resizeMode=INNER";
                    $hero_url_small = $building_header . "?width=640&height=360&resizeMode=CROP";
                    
                    
//                    echo "header file name: " . $header_file_name . $eol;
//                    echo "hero_file_name: " . $hero_file_name . $eol;
//                    echo "hero_cropped_name: " . $hero_cropped_name . $eol;
//                    echo "hero_file_name_small: " . $hero_file_name_small . $eol. $eol;
                    
                    // Check for new uri
                    $buildingflyerheader_check_query_string = "Select flyer_header from buildings where building_code = '" . $building_code . "' and flyer_header != '' and lang = '" . $lang_id . "'";
//                    echo "flyerheader querystring: " . $buildingflyerheader_check_query_string . "\n\r\n\r";
                    $buildingflyerheader_check_query = mysql_query($buildingflyerheader_check_query_string) or die("document check error: " . mysql_error());
                    $buildingflyerheader_check = mysql_fetch_array($buildingflyerheader_check_query);
                    $buildingflyerheader_check_count = mysql_num_rows($buildingflyerheader_check_query);
                    
                    $newdocument_uri = $building_header;
                    $existingdocument_uri = $buildingflyerheader_check['flyer_header'];
                    
//                    echo "uri from xml: " . $newdocument_uri . $eol;
//                    echo "uri from db : " . $existingdocument_uri . $eol;
//                    
//                    echo "buildingflyerheader_check_count: " . $buildingflyerheader_check_count . $eol . $eol;
                    
                    if ($buildingflyerheader_check_count > 0) {
//                        echo "flyer header check count > 0" . $eol . $eol;
                        
                        if ($newdocument_uri !== $existingdocument_uri) {
                            
//                            echo "URI's don't match" . $eol . $eol;
                            
                            // delete existing file and thumbnail		
//                            if (is_file($base_file_path . "images/preview_images/" . $header_file_name)) {
//                                echo "Deleted existing flyer_header files " . $eol;
                                unlink($base_file_path . "images/preview_images/" . $header_file_name); // delete old (existing) file
                                unlink($base_file_path . "images/preview_images/" . $hero_file_name); // delete old (existing) file
                                unlink($base_file_path . "images/preview_images/" . $hero_cropped_name); // delete old (existing) file
                                unlink($base_file_path . "images/preview_images/" . $hero_file_name_small); // delete old (existing) file
//                            }
                            // DOWNLOAD Featured Plan and then create a jpg thumbnail from it
//                            if (!is_file($base_file_path . "images/preview_images/" . $hero_file_name_small)) {
                                if (file_put_contents($base_file_path . 'images/preview_images/' . $hero_file_name_small, file_get_contents($hero_url_small))) {
//                                    echo "small hero downloaded: " . $hero_file_name_small . $eol;
        //                            file_put_contents($base_file_path . 'images/preview_images/' . $hero_file_name, file_get_contents($hero_url));
        //                            file_put_contents($base_file_path . 'images/preview_images/' . $hero_file_name_small, file_get_contents($hero_url_small));

                                    if (file_put_contents($base_file_path . 'images/preview_images/' . $hero_file_name, file_get_contents($hero_url))) {
//                                        echo "full hero downloaded: " . $hero_file_name . $eol;
                                        exec("/usr/local/bin/convert " . $base_file_path . "images/preview_images/" . $hero_file_name . " -gravity Center -crop  '1920x540+0+0' +repage " . $base_file_path . "images/preview_images/" . $hero_cropped_name . "");
                                    }


                                } else {
                                    echo "Could not download flyer header for ". $building_code . " with url: " . $building_header . $eol . $eol;
                                }
//                            }
                            
                                // document is already present, update record //
                            $updateFlyerheaderQuery = "update buildings set flyer_header = '".$building_header."' where building_code = '" . $building_code . "' and lang = '" . $lang_id . "'";
            //                echo "update flyer header query: " . $updateFlyerheaderQuery . $eol . $eol . $eol; 
                            $buildingflyerheader_update = mysql_query($updateFlyerheaderQuery) or die("buildingUpdate Building flyer header error: " . mysql_error());
                            
                            
                        }

                        
                    } 
                    else {
                        
                        $header_file_name = "building_header_".$building_code.".jpg";
                        $hero_file_name = "building_hero_".$building_code.".jpg";
                        $hero_cropped_name = "building_hero_crop_".$building_code.".jpg";
                        $hero_file_name_small = "building_hero_small_".$building_code.".jpg";
                        $hero_url = $building_header . "?width=1920&height=1080&resizeMode=INNER";
                        $hero_url_small = $building_header . "?width=640&height=360&resizeMode=CROP";
                                
                        // DOWNLOAD flyer header large size and make a hero from it
                            if (!is_file($base_file_path . "images/preview_images/" . $hero_file_name_small)) {
                                if (file_put_contents($base_file_path . 'images/preview_images/' . $hero_file_name_small, file_get_contents($hero_url_small))) {
//                                    echo "small hero downloaded: " . $hero_file_name_small . $eol;
        //                            file_put_contents($base_file_path . 'images/preview_images/' . $hero_file_name, file_get_contents($hero_url));
        //                            file_put_contents($base_file_path . 'images/preview_images/' . $hero_file_name_small, file_get_contents($hero_url_small));

                                    if (file_put_contents($base_file_path . 'images/preview_images/' . $hero_file_name, file_get_contents($hero_url))) {
//                                        echo "full hero downloaded: " . $hero_file_name . $eol;
                                        exec("/usr/local/bin/convert " . $base_file_path . "images/preview_images/" . $hero_file_name . " -gravity Center -crop  '1920x540+0+0' +repage " . $base_file_path . "images/preview_images/" . $hero_cropped_name . "");
                                    }

                                } else {
                                    echo "Could not download flyer header for ". $building_code . " with url: " . $building_header . $eol . $eol;
                                }
                            }
                        
                        $updateFlyerheaderQuerynewfile = "update buildings set flyer_header = '".$building_header."' where building_code = '" . $building_code . "' and lang = '" . $lang_id . "'";
        //                echo "update flyer header query: " . $updateFlyerheaderQuery . $eol . $eol . $eol; 
                        $buildingflyerheader_update = mysql_query($updateFlyerheaderQuerynewfile) or die("buildingUpdate Building flyer header brand new header error: " . mysql_error());
                    }

            //        $preview_thumb_name_start = str_ireplace("." . $preview_extension, '', $preview_name);
            //        $preview_thumb_name = $preview_thumb_name_start . "_thumb." . $preview_extension;
            //        $preview_thumb_name_carousel = $preview_thumb_name_start . "_hero." . $preview_extension;
//        
//        
//                
//                    if (!is_file($base_file_path . "images/preview_images/" . $hero_file_name_small)) {
//                        if (file_put_contents($base_file_path . 'images/preview_images/' . $hero_file_name_small, file_get_contents($hero_url_small))) {
////                            file_put_contents($base_file_path . 'images/preview_images/' . $hero_file_name, file_get_contents($hero_url));
////                            file_put_contents($base_file_path . 'images/preview_images/' . $hero_file_name_small, file_get_contents($hero_url_small));
//
//                            if (file_put_contents($base_file_path . 'images/preview_images/' . $hero_file_name, file_get_contents($hero_url))) {
//                                exec("/usr/local/bin/convert " . $base_file_path . "images/preview_images/" . $hero_file_name . " -gravity Center -crop  '1920x540+0+0' +repage " . $base_file_path . "images/preview_images/" . $hero_cropped_name . "");
//                            }
//                            
//                            
//                        } else {
//                            echo "Could not download flyer header for ". $building_code . " with url: " . $building_header . $eol . $eol;
//                        }       ;
//                    }else {
////                        echo "Flyer  header already present. Need to get filename from core to determine if it has changed.".$eol;
//                    }
////echo "\n\rDL: " . $file_name . " video downloaded successfully \n\r";
////                    $video_thumbnail_url = $file_link . '?width=700&height=500&resizeMode=INNER"';
////                    $video_thumbnail_name = str_replace('.' . $file_ext, "_thumb.jpg", $file_name);
////echo $video_thumbnail_name . " is the thumbnail \n\r\n\r";
////                    file_put_contents($base_file_path . "images/preview_images/" . $video_thumbnail_name, file_get_contents($video_thumbnail_url));
////echo "video thumb downloaded\n\r\n\r";
//                } else {
////                    echo "There is no flyer header for ". $building_code . $eol;
//                }

                    } // end if building_header !=''
                
                else { // if building header is empty
//                    echo $eol . 'BUILDING HEADER IS EMPTY' . $eol.$eol;
                    $header_file_name = "building_header_".$building_code.".jpg";
                    $hero_file_name = "building_hero_".$building_code.".jpg";
                    $hero_cropped_name = "building_hero_crop_".$building_code.".jpg";
                    $hero_file_name_small = "building_hero_small_".$building_code.".jpg";
//                    echo "header file name: " . $header_file_name . $eol;
//                    echo "hero_file_name: " . $hero_file_name . $eol;
//                    echo "hero_cropped_name: " . $hero_cropped_name . $eol;
//                    echo "hero_file_name_small: " . $hero_file_name_small . $eol;
                    
//                    if (is_file($base_file_path . "images/preview_images/" . $header_file_name)) {
                        //echo "Deleted existing flyer_header files " . $eol;
                        unlink($base_file_path . "images/preview_images/" . $header_file_name); // delete old (existing) file
                        unlink($base_file_path . "images/preview_images/" . $hero_file_name); // delete old (existing) file
                        unlink($base_file_path . "images/preview_images/" . $hero_cropped_name); // delete old (existing) file
                        unlink($base_file_path . "images/preview_images/" . $hero_file_name_small); // delete old (existing) file
//                    }
                    
                    
                    $updateFlyerheaderQuerynewfile = "update buildings set flyer_header = '' where building_code = '" . $building_code . "' and lang = '" . $lang_id . "'";
        //                echo "update flyer header query: " . $updateFlyerheaderQuery . $eol . $eol . $eol; 
                    $buildingflyerheader_update = mysql_query($updateFlyerheaderQuerynewfile) or die("buildingUpdate Building flyer header empty header error: " . mysql_error());
                    
                }

        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
// Remove building record
        $del_building = mysql_query("DELETE FROM buildings WHERE building_code ='" . $building_code . "' and lang=" . $lang_id);
// insert updated building record
        $qy = "insert into buildings (building_internal_identifier, lang, building_code, building_id, building_type, city, contact_info, country, description, flyer_header,street_address,modification_date,number_of_floors,floors_above_ground,floors_below_ground,available_space,office_area, industrial_area,postal_code,province,province_code,region,sub_region,leasing_node,renovated, retail_area,building_name,measurement_unit,thumbnail,static_map,total_space,uri,year_of_building, parking_stalls,parking_ratio,typical_floor_size,file_name,file_uri,dirty,orientation,add_rent_operating, add_rent_realty,add_rent_power,add_rent_total,truck_doors,drivein_doors,clear_height,power_text,marketing_text,sort_streetnumber,sort_streetname, latitude, longitude, client_name) values ( '" . addslashes($building_internal_identifier) . "','" . addslashes($lang_id) . "', '" . addslashes($building_code) . "','" . addslashes($building_id) . "', '" . ucwords(strtolower($building_type)) . "', '" . addslashes($building_city) . "', '', '" . addslashes($building_country) . "', '" . addslashes($building_desc) . "', '" . addslashes($building_header) . "', '" . addslashes($building_street) . "', '" . $building_modification_date . "', '" . $building_floors . "' ,'" . addslashes($building_floors_above_ground) . "','" . addslashes($building_floors_below_ground) . "','" . addslashes($building_available_space) . "','" . addslashes($building_office_area) . "' ,'" . $building_industrial_area . "', '" . addslashes($building_postal_code) . "', '" . $building_province . "', '" . $building_province_code . "', '" . addslashes($building_region) . "', '" . addslashes($building_sub_region) . "', '' , '', '" . $building_retail_area . "', '" . addslashes($building_name) . "', '', '" . $building_thumbnail . "', '" . addslashes($static_map_name) . "', '" . $building_total_space . "', '" . $building_url . "', '" . addslashes($building_year) . "','" . addslashes($building_parking_stalls) . "','" . addslashes($building_parking_ratio) . "','" . addslashes($building_typical_floor_size) . "', '', '',0,'" . addslashes($building_orientation) . "','" . addslashes($building_add_rent_operating) . "','" . addslashes($building_add_rent_realty) . "','" . addslashes($building_add_rent_power) . "','" . addslashes($building_add_rent_total) . "','" . addslashes($building_truck_doors) . "','" . addslashes($building_drivein_doors) . "','" . addslashes($building_clear_height) . "','" . addslashes($building_power_text) . "','" . addslashes($building_marketing_text) . "','" . addslashes($building_address_number) . "','" . addslashes($building_address_street) . "','" . addslashes($building_latitude) . "','" . addslashes($building_longitude) . "', '" . addslashes($client_official_name) . "')";
//echo "updated building insert: " . $qy . "\n\r\n\r";
//echo "Building Name: --|".$building_name."|--\n\r\n\r";

        $insert_building = mysql_query($qy) or die("insert building error: " . mysql_error());

// Building Flyer Header
        
        





// Building Featured PDF
        if ($building_featured_plan != ''){
            $file_name = $building_code."_featured_building_plan.pdf";
            $file_thumb = str_replace('.pdf', '.jpg', $file_name);
            $file_link = $building_featured_plan;
            
            
//            echo $file_name . $eol . $eol;

// Check for new uri
            $buildingfeaturedplan_check_query_string = "Select * from documents where building_code = '" . $building_code . "' and building_featured_plan = '1' and lang = '" . $lang_id . "'";
//echo "docheck querystring: " . $buildingfeaturedplan_check_query_string . "\n\r\n\r";
            $buildingfeaturedplan_check_query = mysql_query($buildingfeaturedplan_check_query_string) or die("document check error: " . mysql_error());
            $buildingfeaturedplan_check = mysql_fetch_array($buildingfeaturedplan_check_query);
            $buildingfeaturedplan_check_count = mysql_num_rows($buildingfeaturedplan_check_query);

            
//            echo "existing uri: " . $eol;
//            echo $buildingfeaturedplan_check['uri'] . $eol . $eol;
//            echo "NEW uri: " . $eol;
//            echo $file_link . $eol . $eol;
//            
//            echo "buildingfeaturedplan_check_count: " . $eol;
//            echo $buildingfeaturedplan_check_count . $eol . $eol;
            
//echo "filelastmod: " . $file_last_modified . "\n\r";
//echo "db  lastmod: " . $buildingfeaturedplan_check['lastModifiedTime'] . "\n\r";

            $newdocument_uri = $file_link;
            $existingdocument_uri = $buildingfeaturedplan_check['uri'];

            if ($buildingfeaturedplan_check_count > 0) {
                if ($newdocument_uri != $existingdocument_uri) {
                    // delete existing file and thumbnail		
                    if (is_file($base_file_path . "images/documents/" . $file_name)) {
//                        echo "Deleted existing building plan files " . $eol;
                        unlink($base_file_path . "images/documents/" . $file_name); // delete old (existing) file
                        unlink($base_file_path . "images/documents/" . $file_thumb); // delete old (existing) file
                        unlink($base_file_path . "images/documents/thumb_" . $file_thumb); // delete old (existing) file
                    }
                    // DOWNLOAD Featured Plan and then create a jpg thumbnail from it
                        if (file_put_contents($base_file_path . "images/documents/" . $file_name, file_get_contents($file_link))) {
//                            echo "\n\rDL: building plan downloaded\n\r\n\r";                            
                            try {
                                // using Ghostscript to create one page jpg thumbnail of PDF front page
                                // and then a thumbnail of that jpg below
                                exec('/usr/local/bin/gs -sDEVICE=jpeg -r300 -o ' . $base_file_path . 'images/documents/' . $file_thumb . ' -dFirstPage=1 -dLastPage=1 ' . $base_file_path . 'images/documents/' . $file_name . '');
//                                echo "JPG created from PDF " . $eol;
                            } catch (Exception $e) {
                                echo "\nCannot create jpg from PDF for " . $file_name . " -- " . $clientname . "\n\r";
                            }

                            try {
                                exec("/usr/local/bin/convert " . $base_file_path . "images/documents/" . $file_thumb . " -thumbnail '180x' " . $base_file_path . "images/documents/thumb_" . $file_thumb . "");
//                                echo "Thumb created from JPG " . $eol;
                            } catch (Exception $e) {
                                echo "\nCannot create thumbnail for " . $file_name . " -- " . $clientname . "\n\r";
                            }
                        }
                }
                
                // document is already present, update record //
                $updateFeaturedplanQuery = "update documents set lang=" . $lang_id . ", building_id='" . $building_id . "', building_code='" . $building_code . "', file_name='" . $file_name ."', uri='" . $file_link . "', thumb='".$file_thumb."', lastModifiedTime='". date('Y-m-d H:i:s') ."',extension='pdf', dirty='0' where building_code = '" . $building_code . "' and building_featured_plan = 1 and lang = '" . $lang_id . "'";
//                echo "update featured plan query: " . $updateFeaturedplanQuery . $eol . $eol . $eol; 
                $buildingfeaturedplan_update = mysql_query($updateFeaturedplanQuery) or die("buildingUpdate Building featured plan error: " . mysql_error());
            } else {
                // DOWNLOAD Featured Plan and then create a jpg thumbnail from it
                        if (file_put_contents($base_file_path . "images/documents/" . $file_name, file_get_contents($file_link))) {
//                            echo "\n\rDL: building plan downloaded\n\r\n\r";                            
                            try {
                                // using Ghostscript to create one page jpg thumbnail of PDF front page
                                // and then a thumbnail of that jpg below
                                exec('/usr/local/bin/gs -sDEVICE=jpeg -r300 -o ' . $base_file_path . 'images/documents/' . $file_thumb . ' -dFirstPage=1 -dLastPage=1 ' . $base_file_path . 'images/documents/' . $file_name . '');
//                                echo "JPG created from PDF " . $eol;
                            } catch (Exception $e) {
                                echo "\nCannot create jpg from PDF for " . $file_name . " -- " . $clientname . "\n\r";
                            }

                            try {
                                exec("/usr/local/bin/convert " . $base_file_path . "images/documents/" . $file_thumb . " -thumbnail '180x' " . $base_file_path . "images/documents/thumb_" . $file_thumb . "");
//                                echo "Thumb created from JPG " . $eol;
                            } catch (Exception $e) {
                                echo "\nCannot create thumbnail for " . $file_name . " -- " . $clientname . "\n\r";
                            }
                        }
                // new document
                $insertFeaturedplanQuery = "insert into documents (lang, building_id, building_code, file_name, uri, thumb, lastModifiedTime, extension, dirty, building_featured_plan) values (" . $lang_id . ", '" . $building_id . "', '" . $building_code . "', '" . $file_name . "', '" . $file_link . "', '". $file_thumb ."', '".date('Y-m-d H:i:s')."', 'pdf', '0', '1')";
//                echo "insert featuredplan query: " . $insertFeaturedplanQuery . $eol . $eol . $eol; 
                $buildingfeaturedplan_insert = mysql_query($insertFeaturedplanQuery) or die("buildingInsert Building featured plan error: " . mysql_error());
                
            }
// If file is named the same but newer	
//echo "building2d - newdoc: " . $file_name . ' time: ' . $newdocument_modified_time . "\n\r";
//echo "building2d - existdoc  : " . $file_name . ' time: ' . $existingdocument_modified_time . "\n\r\n\r\n\r";

        } //end if $building_featured_plan



//$building_featured_plan;






























//echo "Building counter middle: " . $building_id_num . "\n\r\n\r";		
// HANDLE THE "SECTIONS"
        $del_sections = mysql_query("DELETE FROM sections WHERE lang=" . $lang_id . " and building_code='" . $building_code . "'");

        foreach ($Building->sections->section as $section) {

            $section_index = "{$section['index']}";
            $section_title = "{$section['title']}";
            $section_content = "{$section}";

            $section_query = mysql_query("insert into sections (lang, section_index, building_id, building_code, section_title, section_content) values (" . $lang_id . "," . addslashes($section_index) . ", '" . $building_id . "', '" . addslashes($building_code) . "', '" . addslashes($section_title) . "', '" . addslashes($section_content) . "')") or die("insert sections error: " . mysql_error());
        }

// Handle the Building Specifications
// HANDLE BUILDINGS SPECS
        $del_building_specs = mysql_query("DELETE FROM building_specifications WHERE building_code ='" . $building_code . "' and lang=" . $lang_id);

        $specs_qy = mysql_query("INSERT INTO building_specifications 
	(building_code, 
	building_id,
	lang, 
	primary_purpose, 
	new_development, 
	fund_name, 
	measurement_units, 
	internal_identifier, 
	mall_type, 
	costs_posted_net_rate, 
	costs_realty, 
	costs_power, 
	costs_operating, 
	costs_utilities, 
	costs_add_rent_total,
        costs_other,
	costs_power_watts, 
	costs_power_lighting, 
	zoned, 
	year_built, 
	renovated, 
	owned, 
	managed, 
	management, 
	website, 
	average_office, 
	number_of_buildings, 
	number_of_units, 
	primary_usage, 
	secondary_usage, 
	floors, 
	retail_units, 
	retail_unit_size, 
	highrise_floor_plate, 
	lowrise_floor_plate, 
	highrise_gross_up, 
	lowrise_gross_up, 
	retail_food_court, 
	retail_num_food_units, 
	retail_food_court_seating, 
	total_office_space, 
	total_retail_space, 
	total_space, 
	building_available_space,
	site_coverage, 
	total_hectares, 
	total_acreage, 
	total_industrial_space, 
	available_industrial_space, 
	manufacturing_size, 
	warehouse_size, 
	available_office_space, 
	available_retail_space, 
	available_space, 
	occupied_space, 
	largest_contiguous_available, 
	const_typical_power, 
	const_lighting, 
	const_ceiling_height, 
	const_interior_wall_type, 
	const_exterior_finish, 
	const_washrooms_per_floor, 
	const_satellite_dish_capability, 
	const_fibre_optic_capability, 
	const_shipping_receiving, 
	const_emergency_generator, 
	const_after_hours_hvac, 
	const_hvac_hours_of_operation, 
	const_hvac_description, 
	const_max_door_height, 
	const_shipping_doors_drivein, 
	const_shipping_doors_truck, 
	const_rail_loading, 
	const_building_plan, 
	const_peer_review, 
	const_survey, 
	const_drycleaning_solvents, 
	const_asbestos_free, 
	const_roofing_type, 
	const_available_electrical_volts, 
	const_available_electrical_amps, 
	const_power_text, 
	elevators_hirise, 
	traffic_demographic_source,
	traffic_pedestrian_traffic,
	customer_persons_per_household,
	customer_median_age,
	customer_household_income,
	secondary_household_income,
	primary_household_income,
	secondary_number_of_households,
	primary_number_of_households,
	secondary_population,
	primary_population,
	number_of_stores,
	elevators_midrise, 
	elevators_lowrise, 
	elevators_parking, 
	elevators_freight, 
	safety_fire_detection, 
	safety_sprinkler_system, 
	safety_manned_security, 
	safety_security_system, 
	safety_barrier_free, 
	parking_surface_stalls, 
	parking_surface_ratio, 
	parking_above_stalls, 
	parking_above_ratio, 
	parking_below_stalls, 
	parking_below_ratio, 
	parking_total_stalls, 
	parking_cost_day, 
	parking_cost_month, 
	parking_description, 
        marshalling_area, 
        dolly_pad, 
        environmental_certification_notes, 
        outside_storage, 
        storage_notes, 
        anchor_tenant1, 
	anchor_tenant2, 
	anchor_tenant3, 
	anchor_tenant4, 
	anchor_tenant5, 
	anchor_tenant6, 
	public_surface_route, 
	public_subway_access, 
	operating_hours_M, 
	operating_hours_T, 
	operating_hours_W, 
	operating_hours_R, 
	operating_hours_F, 
	operating_hours_S, 
	operating_hours_D, 
	operating_hours_notes,
        boma,
        leed
	)
	VALUES
	('" . $building_code . "',
	" . $building_id . ",
	" . $lang_id . ", 
	'" . addslashes($building_type) . "', 
	'" . addslashes($building_new_development) . "', 
	'" . addslashes($building_fund_name) . "', 
	'" . addslashes($building_measurement_units) . "', 
	'" . addslashes($building_internal_identifier) . "', 
	'" . addslashes($building_mall_type) . "', 
	'" . addslashes($building_costs_posted_net_rate) . "', 
	'" . addslashes($building_costs_realty) . "', 
	'" . addslashes($building_costs_power) . "', 
	'" . addslashes($building_costs_operating) . "', 
	'" . addslashes($building_costs_other) . "', 
	'" . addslashes($building_costs_add_rent_total) . "', 
        '" . addslashes($building_costs_other) . "', 
	'" . addslashes($building_costs_power_watts) . "', 
	'" . addslashes($building_costs_power_lighting) . "', 
	'" . addslashes($building_zoned) . "', 
	'" . addslashes($building_year) . "', 
	'" . addslashes($building_renovated) . "', 
	'" . addslashes($building_owned) . "', 
	'" . addslashes($building_managed) . "', 
	'" . addslashes($building_management) . "', 
	'" . addslashes($building_website) . "', 
	'" . addslashes($building_average_office) . "', 
	'" . addslashes($building_number_of_buildings) . "', 
	'" . addslashes($building_number_of_units) . "', 
	'" . addslashes($building_primary_usage) . "', 
	'" . addslashes($building_secondary_usage) . "', 
	'" . addslashes($building_floors) . "', 
	'" . addslashes($building_retail_units) . "', 
	'" . addslashes($building_retail_unit_size) . "', 
	'" . addslashes($building_highrise_floor_plate) . "', 
	'" . addslashes($building_lowrise_floor_plate) . "', 
	'" . addslashes($building_highrise_gross_up) . "', 
	'" . addslashes($building_lowrise_gross_up) . "', 
	'" . addslashes($building_retail_food_court) . "', 
	'" . addslashes($building_retail_num_food_units) . "', 
	'" . addslashes($building_retail_food_court_seating) . "', 
	'" . addslashes($building_total_office_space) . "', 
	'" . addslashes($building_total_retail_space) . "', 
	'" . addslashes($building_total_space) . "', 
	'" . addslashes($building_available_space) . "', 
	'" . addslashes($building_site_coverage) . "', 
	'" . addslashes($building_total_hectares) . "', 
	'" . addslashes($building_total_acreage) . "', 
	'" . addslashes($building_total_industrial_space) . "', 
	'" . addslashes($building_available_industrial_space) . "', 
	'" . addslashes($building_manufacturing_size) . "', 
	'" . addslashes($building_warehouse_size) . "', 
	'" . addslashes($building_available_office_space) . "', 
	'" . addslashes($building_available_retail_space) . "', 
	'" . addslashes($building_available_space) . "', 
	'" . addslashes($building_occupied_space) . "', 
	'" . addslashes($building_largest_contiguous_available) . "', 
	'" . addslashes($building_const_typical_power) . "', 
	'" . addslashes($building_const_lighting) . "', 
	'" . addslashes($building_const_ceiling_height) . "', 
	'" . addslashes($building_const_interior_wall_type) . "', 
	'" . addslashes($building_const_exterior_finish) . "', 
	'" . addslashes($building_const_washrooms_per_floor) . "', 
	'" . addslashes($building_const_satellite_dish_capability) . "', 
	'" . addslashes($building_const_fibre_optic_capability) . "', 
	'" . addslashes($building_const_shipping_receiving) . "', 
	'" . addslashes($building_const_emergency_generator) . "', 
	'" . addslashes($building_const_after_hours_hvac) . "', 
	'" . addslashes($building_const_hvac_hours_of_operation) . "', 
	'" . addslashes($building_const_hvac_description) . "', 
	'" . addslashes($building_const_max_door_height) . "', 
	'" . addslashes($building_const_shipping_doors_drivein) . "', 
	'" . addslashes($building_const_shipping_doors_truck) . "', 
	'" . addslashes($building_const_rail_loading) . "', 
	'" . addslashes($building_const_building_plan) . "', 
	'" . addslashes($building_const_peer_review) . "', 
	'" . addslashes($building_const_survey) . "', 
	'" . addslashes($building_const_drycleaning_solvents) . "', 
	'" . addslashes($building_const_asbestos_free) . "', 
	'" . addslashes($building_const_roofing_type) . "', 
	'" . addslashes($building_const_available_electrical_volts) . "', 
	'" . addslashes($building_const_available_electrical_amps) . "', 
	'" . addslashes($building_const_power_text) . "', 
	'" . addslashes($building_elevators_hirise) . "', 
	'" . addslashes($building_traffic_demographic_source) . "', 
	'" . addslashes($building_traffic_pedestrian_traffic) . "', 
	'" . addslashes($building_customer_person_per_household) . "',
	'" . addslashes($building_customer_median_age) . "',
	'" . addslashes($building_customer_household_income) . "', 
	'" . addslashes($building_secondary_average_household_income) . "', 
	'" . addslashes($building_primary_average_household_income) . "', 
	'" . addslashes($building_secondary_number_of_households) . "', 
	'" . addslashes($building_primary_number_of_households) . "', 
	'" . addslashes($building_secondary_population) . "', 
	'" . addslashes($building_primary_population) . "', 
	'" . addslashes($building_number_of_stores) . "', 
	'" . addslashes($building_elevators_midrise) . "', 
	'" . addslashes($building_elevators_lowrise) . "', 
	'" . addslashes($building_elevators_parking) . "', 
	'" . addslashes($building_elevators_freight) . "', 
	'" . addslashes($building_safety_fire_detection) . "', 
	'" . addslashes($building_safety_sprinkler_system) . "', 
	'" . addslashes($building_safety_manned_security) . "', 
	'" . addslashes($building_safety_security_system) . "', 
	'" . addslashes($building_safety_barrier_free) . "', 
	'" . addslashes($building_parking_surface_stalls) . "', 
	'" . addslashes($building_parking_surface_ratio) . "', 
	'" . addslashes($building_parking_above_stalls) . "', 
	'" . addslashes($building_parking_above_ratio) . "', 
	'" . addslashes($building_parking_below_stalls) . "', 
	'" . addslashes($building_parking_below_ratio) . "', 
	'" . addslashes($building_parking_total_stalls) . "', 
	'" . addslashes($building_parking_cost_day) . "', 
	'" . addslashes($building_parking_cost_month) . "', 
	'" . addslashes($building_parking_description) . "', 
        '" . addslashes($building_marshalling_area) . "', 
        '" . addslashes($building_dolly_pad) . "', 
        '" . addslashes($building_environmental_certification_notes) . "', 
        '" . addslashes($building_outside_storage) . "', 
        '" . addslashes($building_storage_notes) . "', 
	'" . addslashes($building_anchor_tenant1) . "', 
	'" . addslashes($building_anchor_tenant2) . "', 
	'" . addslashes($building_anchor_tenant3) . "', 
	'" . addslashes($building_anchor_tenant4) . "', 
	'" . addslashes($building_anchor_tenant5) . "', 
	'" . addslashes($building_anchor_tenant6) . "', 
	'" . addslashes($building_public_surface_route) . "', 
	'" . addslashes($building_public_subway_access) . "', 
	'" . addslashes($building_operating_hours_M) . "', 
	'" . addslashes($building_operating_hours_T) . "', 
	'" . addslashes($building_operating_hours_W) . "', 
	'" . addslashes($building_operating_hours_R) . "', 
	'" . addslashes($building_operating_hours_F) . "', 
	'" . addslashes($building_operating_hours_S) . "', 
	'" . addslashes($building_operating_hours_D) . "', 
	'" . addslashes($building_operating_hours_notes) . "',
        '" . addslashes($building_specs_boma) . "',
        '" . addslashes($building_specs_leed) . "')") or die("insert building specs error: " . mysql_error());




// HANDLE THE CONTACTS
        $del_contacts = mysql_query("DELETE FROM contacts WHERE lang=" . $lang_id . " and building_code='" . $building_code . "'");

        foreach ($Building->contacts->contact as $contact) {
            $contact_type = "{$contact['type']}";
            $contact_name = "{$contact['name']}";
            $contact_priority = "{$contact['index']}";
            $contact_title = "{$contact['title']}";
            $contact_company = "{$contact['company']}";
            $contact_email = "{$contact['email']}";
            $contact_phone = "{$contact['phone']}";
            $contact_phone = str_replace('\'', '', $contact_phone);

            $contact_query = mysql_query("insert into contacts (lang, priority, building_id, building_code, type, name, title, company, email, phone) values (" . addslashes($lang_id) . ", " . addslashes($contact_priority) . ", '" . $building_id . "', '" . addslashes($building_code) . "', '" . addslashes($contact_type) . "', '" . addslashes($contact_name) . "', '" . addslashes($contact_title) . "', '" . addslashes($contact_company) . "', '" . addslashes($contact_email) . "', '" . addslashes($contact_phone) . "')") or die("insert contact error: " . mysql_error());
        }

//mark all the documents dirty
        $dirty_documents = mysql_query("UPDATE documents SET dirty=1 WHERE building_code = '" . $building_code . "' and building_featured_plan !='1' and lang=" . $lang_id);

// Building Level 2D
        foreach ($Building->plans2d->file as $file) {
//echo $building_code . " -----building > plans2d > file : filehere\n\r\n\r";
            $file_name = "{$file['name']}";
            $file_name = str_replace(' ', '_', $file_name);
            $file_name = str_replace('(', '', $file_name);
            $file_name = str_replace(')', '', $file_name);
            $file_name = str_replace('*', '', $file_name);
            $file_name = str_replace('\\', '', $file_name);
            $file_name = str_replace('/', '', $file_name);

            $file_desc = "{$file['description']}";
            $file_desc = str_replace(' ', '_', $file_desc);
            $file_desc = str_replace('(', '', $file_desc);
            $file_desc = str_replace(')', '', $file_desc);
            $file_desc = str_replace('*', '', $file_desc);
            $file_desc = str_replace('\\', '', $file_desc);
            $file_desc = str_replace('/', '', $file_desc);

            $file_link = "{$file->uri}";
            $file_ext = "{$file['extension']}";

            if (strpos($file_name, '.pdf') != FALSE) {
                // add file description to name
                $file_name = str_replace('.pdf', '_' . $file_desc . '.pdf', $file_name);
            } else {
                // add file description and '.file_ext' to name
                $file_name = $file_name . '_' . $file_desc . '.' . $file_ext;
            }

            $file_last_modified = "{$file['lastModifiedTime']}";
            $file_thumb = str_replace('.pdf', '.jpg', $file_name);

// Check for newer file versions
            $buildingdocument_check_query_string = "Select * from documents where building_code = '" . $building_code . "' and file_name = '" . $file_name . "' and lang = '" . $lang_id . "'";
//echo "docheck querystring: " . $buildingdocument_check_query_string . "\n\r\n\r";
            $buildingdocument_check_query = mysql_query($buildingdocument_check_query_string) or die("document check error: " . mysql_error());
            $buildingdocument_check = mysql_fetch_array($buildingdocument_check_query);
            $buildingdocument_check_count = mysql_num_rows($buildingdocument_check_query);

//echo "filelastmod: " . $file_last_modified . "\n\r";
//echo "db  lastmod: " . $buildingdocument_check['lastModifiedTime'] . "\n\r";

            $newdocument_modified_time = strtotime($file_last_modified);
            $existingdocument_modified_time = strtotime($buildingdocument_check['lastModifiedTime']);

            if ($buildingdocument_check_count > 0) {
                // document is already present, update record //
                $buildingdocument_update = mysql_query("update documents set lang=" . $lang_id . ", building_id='" . $building_id . "', building_code='" . $building_code . "', file_name='" . addslashes($file_name) . "', description='" . addslashes($file_desc) . "', uri='" . $file_link . "', thumb='" . addslashes($file_thumb) . "', lastModifiedTime='" . addslashes($file_last_modified) . "', extension='" . $file_ext . "', dirty='0' where building_code = '" . $building_code . "' and file_name = '" . $file_name . "' and lang = '" . $lang_id . "'") or die("buildingUpdate Buildings plans2d error: " . mysql_error());
            } else {
                // new document
                $buildingdocument_insert = mysql_query("insert into documents (lang, building_id, building_code, file_name, description, uri, thumb, lastModifiedTime, extension, dirty) values (" . $lang_id . ", '" . $building_id . "', '" . $building_code . "', '" . addslashes($file_name) . "', '" . addslashes($file_desc) . "', '" . $file_link . "', '" . addslashes($file_thumb) . "', '" . addslashes($file_last_modified) . "', '" . $file_ext . "', '0')") or die("buildingInsert Building plans2d error: " . mysql_error());
            }
// If file is named the same but newer	
//echo "building2d - newdoc: " . $file_name . ' time: ' . $newdocument_modified_time . "\n\r";
//echo "building2d - existdoc  : " . $file_name . ' time: ' . $existingdocument_modified_time . "\n\r\n\r\n\r";


            if ($newdocument_modified_time > $existingdocument_modified_time) {
                // delete existing file and thumbnail		
                if (is_file($base_file_path . "images/documents/" . $file_name)) {
                    unlink($base_file_path . "images/documents/" . $file_name); // delete old (existing) file
                }
                if (is_file($base_file_path . "images/documents/" . $file_thumb)) {
                    unlink($base_file_path . "images/documents/" . $file_thumb); // delete old (existing) thumb
                }

// DOWNLOAD BUILDING LEVEL 2D FILES FOR THUMBNAILING
                if (file_put_contents($base_file_path . "images/documents/" . $file_name, file_get_contents($file_link))) {
//echo "\n\rDL: building 2d downloaded\n\r\n\r";                            
                    try {
// using Ghostscript to create one page jpg thumbnail of PDF front page
                        exec('/usr/local/bin/gs -sDEVICE=jpeg -r300 -o ' . $base_file_path . 'images/documents/' . $file_thumb . ' -dFirstPage=1 -dLastPage=1 ' . $base_file_path . 'images/documents/' . $file_name . '');
                    } catch (Exception $e) {
                        echo "\nCannot create jpg from PDF for " . $file_name . "\n\r";
                    }

                    try {
                        exec("/usr/local/bin/convert " . $base_file_path . "images/documents/" . $file_thumb . " -thumbnail '180x' " . $base_file_path . "images/documents/thumb_" . $file_thumb . "");
                    } catch (Exception $e) {
                        echo "\nCannot create thumbnail for " . $file_name . "\n\r";
                    }
                }
            } //end time check
        } //end foreach $Building->plans2d->file as $file
// Building Level Video
        foreach ($Building->buildingVideos->file as $file) {
            $file_name = "{$file['name']}";
//echo "raw filename: " . $file_name . "\n\r";

            $file_name = str_replace(' ', '_', $file_name);
            $file_name = str_replace('(', '', $file_name);
            $file_name = str_replace(')', '', $file_name);
            $file_name = str_replace('*', '', $file_name);
            $file_name = str_replace('\\', '', $file_name);
            $file_name = str_replace('/', '', $file_name);
//echo "cleaned filename: " . $file_name . "\n\r";

            $file_desc = "{$file['description']}";
            $file_desc = str_replace(' ', '_', $file_desc);
            $file_desc = str_replace('(', '', $file_desc);
            $file_desc = str_replace(')', '', $file_desc);
            $file_desc = str_replace('*', '', $file_desc);
            $file_desc = str_replace('\\', '', $file_desc);
            $file_desc = str_replace('/', '', $file_desc);

            $file_link = "{$file->uri}";
            $file_last_modified = "{$file['lastModifiedTime']}";
            $file_ext = "{$file['extension']}";

//		if (strpos($file_name, $file_ext) > -1) {
//			// add file description to name
//			$file_name = str_replace('.' . $file_ext, '_' . $file_desc . '.' . $file_ext, $file_name);
//		} else {
//			// add file description and '.file_ext' to name
//			$file_name = $file_name . '_' . $file_desc . '.' . $file_ext;
//		}

            $document_check_query_string = "Select * from documents where building_code = '" . $building_code . "' and file_name = '" . $file_name . "' and lang = '" . $lang_id . "'";
            $document_check_query = mysql_query($document_check_query_string) or die("document check error: " . mysql_error());
            $document_check = mysql_fetch_array($document_check_query);
            $document_check_count = mysql_num_rows($document_check_query);

            $newdocument_modified_time = '';
            $existingdocument_modified_time = '';
            $newdocument_modified_time = strtotime($file_last_modified);
            $existingdocument_modified_time = strtotime($document_check['lastModifiedTime']);


            if ($document_check_count > 0) {
                // document is already present, update record //
                $document_update = mysql_query("update documents set lang=" . $lang_id . ", building_id='" . $building_id . "', building_code='" . $building_code . "', file_name='" . addslashes($file_name) . "', description='" . addslashes($file_desc) . "', uri='" . $file_link . "', lastModifiedTime='" . $file_last_modified . "', extension='" . $file_ext . "', dirty='0' where building_code = '" . $building_code . "' and file_name = '" . $file_name . "' and lang = '" . $lang_id . "'") or die("Update Buildings Video Error: " . mysql_error());
            } else {
                // new document
                $document_insert = mysql_query("insert into documents (lang, building_id, building_code, file_name, description, uri, lastModifiedTime, extension, dirty) values (" . $lang_id . ", '" . $building_id . "', '" . $building_code . "', '" . addslashes($file_name) . "', '" . addslashes($file_desc) . "', '" . $file_link . "', '" . $file_last_modified . "', '" . $file_ext . "', '0')") or die("Insert Building Video Error: " . mysql_error());
            }


            // if file is newer than existing
            if ($newdocument_modified_time > $existingdocument_modified_time) {
                // delete existing
                if (is_file($base_file_path . "images/video_files/" . $file_name)) {
                    unlink($base_file_path . "images/video_files/" . $file_name); // delete file
                }

// DOWNLOAD BUILDING LEVEL VIDEO FILES
                if (file_put_contents($base_file_path . "images/video_files/" . $file_name, file_get_contents($file_link))) {
//echo "\n\rDL: " . $file_name . " video downloaded successfully \n\r";
                    $video_thumbnail_url = $file_link . '?width=700&height=500&resizeMode=INNER"';
                    $video_thumbnail_name = str_replace('.' . $file_ext, "_thumb.jpg", $file_name);
//echo $video_thumbnail_name . " is the thumbnail \n\r\n\r";
                    file_put_contents($base_file_path . "images/video_files/" . $video_thumbnail_name, file_get_contents($video_thumbnail_url));
//echo "video thumb downloaded\n\r\n\r";
                }
            }
        }

// Building Level Documents
        foreach ($Building->documents->file as $file) {
            $file_name = '';
            $file_name = "{$file['name']}";
            $file_name = str_replace(' ', '_', $file_name);
            $file_name = str_replace('(', '', $file_name);
            $file_name = str_replace(')', '', $file_name);
            $file_name = str_replace('*', '', $file_name);
            $file_name = str_replace('\\', '', $file_name);
            $file_name = str_replace('/', '', $file_name);

            $file_desc = "{$file['description']}";
            $file_desc = str_replace(' ', '_', $file_desc);
            $file_desc = str_replace('(', '', $file_desc);
            $file_desc = str_replace(')', '', $file_desc);
            $file_desc = str_replace('*', '', $file_desc);
            $file_desc = str_replace('\\', '', $file_desc);
            $file_desc = str_replace('/', '', $file_desc);

            $file_link = "{$file->uri}";
            $file_last_modified = "{$file['lastModifiedTime']}";
            $file_ext = "{$file['extension']}";

            if (strpos($file_name, $file_ext) != FALSE) {
                // add file description to name
                $file_name = str_replace('.' . $file_ext, '_' . $file_desc . '.' . $file_ext, $file_name);
            } else {
                // add file description and '.file_ext' to name
                $file_name = $file_name . '_' . $file_desc . '.' . $file_ext;
            }

            $document_check_query_string = "Select * from documents where building_code = '" . $building_code . "' and file_name = '" . $file_name . "' and lang = '" . $lang_id . "'";
//echo "buildingdocuments check query string: " . $document_check_query_string . "\n\r\n\r";
            $document_check_query = mysql_query($document_check_query_string) or die("document check error: " . mysql_error());
            $document_check = mysql_fetch_array($document_check_query);
            $document_check_count = mysql_num_rows($document_check_query);

            $newdocument_modified_time = '';
            $existingdocument_modified_time = '';
            $newdocument_modified_time = strtotime($file_last_modified);
            $existingdocument_modified_time = strtotime($document_check['lastModifiedTime']);

//echo "buildingdocs - newdoc: " . $file_name . ' time: ' . $newdocument_modified_time . "\n\r";
//echo "buildingdocs - existdoc  : " . $file_name . ' time: ' . $existingdocument_modified_time . "\n\r\n\r\n\r";

            if ($document_check_count > 0) {
                // document is already present, update record //
                $document_update = mysql_query("update documents set lang=" . $lang_id . ", building_id='" . $building_id . "', building_code='" . $building_code . "', file_name='" . addslashes($file_name) . "', description='" . addslashes($file_desc) . "', uri='" . $file_link . "', lastModifiedTime='" . $file_last_modified . "', extension='" . $file_ext . "', dirty='0' where building_code = '" . $building_code . "' and file_name = '" . $file_name . "' and lang = '" . $lang_id . "'") or die("Update Buildings Documents Error: " . mysql_error());
            } else {
                // new document
                $document_insert = mysql_query("insert into documents (lang, building_id, building_code, file_name, description, uri, lastModifiedTime, extension, dirty) values (" . $lang_id . ", '" . $building_id . "', '" . $building_code . "', '" . addslashes($file_name) . "', '" . addslashes($file_desc) . "', '" . $file_link . "', '" . $file_last_modified . "', '" . $file_ext . "', '0')") or die("Insert Building Documents Error: " . mysql_error());
            }


            // if file is newer than existing
            if ($newdocument_modified_time > $existingdocument_modified_time) {
                // delete existing
                if (is_file($base_file_path . "images/documents/" . $file_name)) {
                    unlink($base_file_path . "images/documents/" . $file_name); // delete file
                }

                // DOWNLOAD BUILDING LEVEL Documents
                if (file_put_contents($base_file_path . "images/documents/" . $file_name, file_get_contents($file_link))) {
//echo "building doc downloaded\n\r\n\r";
                    //				echo $file_name . " downloaded successfully \n\r";
                }
            }
        }

// building 3d Presentations
//	$del_3dpres2 = mysql_query("DELETE FROM files_3d WHERE lang=" . $lang_id . " and building_code='" . $building_code . "'") or die ("3d delete2 error: " . mysql_error());
        $dirty_3d = mysql_query("UPDATE files_3d SET dirty=1 WHERE type_3d='pres' AND building_code = '" . $building_code . "' and lang=" . $lang_id . " and suite_name = ''");

        foreach ($Building->presentations->presentation as $PresentationB) {
            $building_presentation_link = "{$PresentationB->uri}";
            $building_presentation_name = "{$PresentationB['name']}";
            $building_presentation_name = str_replace(' ', '_', $building_presentation_name);
            $building_presentation_description = "{$PresentationB['description']}";
            $building_presentation_spp_link = "{$PresentationB->sppUri}";
            $building_presentation_lastModifiedTime = "{$PresentationB['lastModifiedTime']}";
            $building_presentation_sketchfabEmbedCode = "{$PresentationB->sketchfabEmbedCode}";
            $threedee_request_id = "0";
            $id_pos = strpos($building_presentation_link, '=');
            $building3d_id = substr($building_presentation_link, $id_pos + 1);
            $building3d_image_url = "http://www.arcestra.com/fileDl/fetchPresentationFile.action?id=" . $building3d_id . "&fileAccessMethod=PUBLIC_FILE_LINK&presentationContent=presentation%2FmasterImage.png";
            $building_3dimage_name = $building_code . '_' . $building_presentation_name . '.png';

            $building3d_check_query_string = "Select * from files_3d where building_code = '" . $building_code . "' and suite_name = '' and presentation_name = '" . $building_presentation_name . "' and type_3d='pres' and lang = '" . $lang_id . "'";
//echo "\n\rbuilding 3d check qs: " . $building3d_check_query_string . "\n\r\n\r";
            $building3d_check_query = mysql_query($building3d_check_query_string) or die("building3d_check error: " . mysql_error());
            $building3d_check = mysql_fetch_array($building3d_check_query);
            $building3d_check_count = mysql_num_rows($building3d_check_query);
//echo "\n\rbuilding 3d check count: " . $building3d_check_count . "\n\r\n\r";
            $newbuilding3d_modified_time = '';
            $existingbuilding3d_modified_time = '';


//echo "raw lastmodified xml: " . $building_presentation_lastModifiedTime . "\n\r";
//echo "raw lastmodified db : " . $building3d_check['lastModifiedTime'] . "\n\r\n\r";

            $newbuilding3d_modified_time = strtotime($building_presentation_lastModifiedTime);
            $existingbuilding3d_modified_time = strtotime($building3d_check['lastModifiedTime']);


            if ($building3d_check_count > 0) {
                // existing 3d
                $update_pres_query_string = "update files_3d set suite_id = '', suite_name = '', building_id = '" . $building_id . "', building_code = '" . $building_code . "', presentation_link = '" . addslashes($building_presentation_link) . "', presentation_description = '" . addslashes($building_presentation_description) . "', presentation_name = '" . addslashes($building_presentation_name) . "', presentation_sppfile = '" . addslashes($building_presentation_spp_link) . "', presentation_thumb = '" . addslashes($building_3dimage_name) . "', lastModifiedTime = '" . addslashes($building_presentation_lastModifiedTime) . "', sketchfabEmbedCode = '" . addslashes($building_presentation_sketchfabEmbedCode) . "', type_3d='pres', lang = " . $lang_id . ", threedee_request_id = '" . $threedee_request_id . "', dirty = 0 WHERE building_code = '" . $building_code . "' and lang=" . $lang_id . " and presentation_name = '" . $building_presentation_name . "'";
//echo "update building presentation query string: " . $update_pres_query_string . "\n\r\n\r";
                $update_pres_query = mysql_query($update_pres_query_string) or die("update building presentation_query error: " . mysql_error());
            } else {
                // new 3d
                $insert_pres_query_string = "insert into files_3d (suite_id, suite_name, building_id, building_code, presentation_link, presentation_description, presentation_name, presentation_sppfile, presentation_thumb, lastModifiedTime, sketchfabEmbedCode, type_3d, lang, threedee_request_id) values ('','','" . $building_id . "', '" . $building_code . "', '" . addslashes($building_presentation_link) . "', '" . addslashes($building_presentation_description) . "', '" . addslashes($building_presentation_name) . "', '" . addslashes($building_presentation_spp_link) . "', '" . addslashes($building_3dimage_name) . "', '" . addslashes($building_presentation_lastModifiedTime) . "', '" . addslashes($building_presentation_sketchfabEmbedCode) . "', 'pres', " . $lang_id . ", '" . $threedee_request_id . "')";
//echo "insert building presentation query string: " . $insert_pres_query_string . "\n\r\n\r";
                $insert_pres_query = mysql_query($insert_pres_query_string) or die("insert building presentation_query error: " . mysql_error());
            }

//echo "new building lvl 3d modified: " . $newbuilding3d_modified_time . "\n\r";
//echo "existing bld lvl 3d modified: " . $existingbuilding3d_modified_time . "\n\r";
            // if 3d pres is named the same and newer than existing
            if ($newbuilding3d_modified_time > $existingbuilding3d_modified_time) {
                // delete existing
                if (is_file($base_file_path . "images/threedee_files/" . $building3d_check['presentation_thumb'])) {
                    unlink($base_file_path . "images/threedee_files/" . $building3d_check['presentation_thumb']); // delete file
//echo "\n\rexisting building 3d file deleted: " . $building3d_check['presentation_thumb'] . "\n\r\n\r";
                }
                file_put_contents($base_file_path . 'images/threedee_files/' . $building_3dimage_name, file_get_contents($building3d_image_url));
//echo "\n\rDL: building 3d downloaded\n\r\n\r";
                //		echo "\n 3D Presentation Thumbnail ".$suite_3dimage_name." Successfully downloaded \n";				
            } // end if newbuilding3d > existingbuilding3d
        } // end foreach as presentationE
// building 3d FILES (panoramas)
//	$del_3dpres2 = mysql_query("DELETE FROM files_3d WHERE lang=" . $lang_id . " and building_code='" . $building_code . "'") or die ("3d delete2 error: " . mysql_error());
        $dirty_3d = mysql_query("UPDATE files_3d SET dirty=1 WHERE building_code = '" . $building_code . "' and type_3d='pano' and lang=" . $lang_id . " and suite_name = ''");

        foreach ($Building->presentations->file as $panoramaFile) {
            $arcestra_file_link = "{$panoramaFile->uri}";
            
            
            $building_panorama_name = "{$panoramaFile['name']}";
            
            $building_panorama_link = $cron['vacancy_report_website'] . "/pannellum.htm?panorama=" . $cron['vacancy_report_website'] . "/images/threedee_files/".$building_panorama_name;
            
            $building_panorama_name = str_replace(' ', '_', $building_panorama_name);
            $building_panorama_description = "{$panoramaFile['description']}";
//            $building_panorama_spp_link = "{$panoramaFile->sppUri}";
            $building_panorama_lastModifiedTime = "{$panoramaFile['lastModifiedTime']}";
//            $building_panorama_sketchfabEmbedCode = "{$panoramaFile->sketchfabEmbedCode}";
            $building_panorama_sketchfabEmbedCode = '';
            $threedee_request_id = "0";
//            $id_pos = strpos($arcestra_file_link, '=');
            $building3dPano_id = substr($arcestra_file_link, $id_pos + 1);
//            $building3dPano_image_url = "http://www.arcestra.com/fileDl/fetchPresentationFile.action?id=" . $building3dPano_id . "&fileAccessMethod=PUBLIC_FILE_LINK&presentationContent=presentation%2FmasterImage.png";
            $building_3dPanoimage_name = $building_panorama_name;
            $building_3dPanoThumb_name = "thumb_".$building_panorama_name;
            

            $building3dPano_check_query_string = "Select * from files_3d where building_code = '" . $building_code . "' and suite_name = '' and presentation_name = '" . $building_panorama_name . "' and type_3d='pano' and lang = '" . $lang_id . "'";
//echo "\n\rbuilding 3d check qs: " . $building3dPano_check_query_string . "\n\r\n\r";
            $building3dPano_check_query = mysql_query($building3dPano_check_query_string) or die("building3dPano_check error: " . mysql_error());
            $building3dPano_check = mysql_fetch_array($building3dPano_check_query);
            $building3dPano_check_count = mysql_num_rows($building3dPano_check_query);
//echo "\n\rbuilding 3d check count: " . $building3dPano_check_count . "\n\r\n\r";
            $newbuilding3dPano_modified_time = '';
            $existingbuilding3dPano_modified_time = '';


//echo "raw lastmodified xml: " . $building_panorama_lastModifiedTime . "\n\r";
//echo "raw lastmodified db : " . $building3dPano_check['lastModifiedTime'] . "\n\r\n\r";

            $newbuilding3dPano_modified_time = strtotime($building_panorama_lastModifiedTime);
            $existingbuilding3dPano_modified_time = strtotime($building3dPano_check['lastModifiedTime']);


            if ($building3dPano_check_count > 0) {
                // existing 3d
                $update_pano_query_string = "update files_3d set suite_id = '', suite_name = '', building_id = '" . $building_id . "', building_code = '" . $building_code . "', presentation_link = '" . addslashes($building_panorama_link) . "', presentation_description = '" . addslashes($building_panorama_description) . "', presentation_name = '" . addslashes($building_panorama_name) . "', presentation_sppfile = '', presentation_thumb = '" . addslashes($building_3dPanoThumb_name) . "', lastModifiedTime = '" . addslashes($building_panorama_lastModifiedTime) . "', sketchfabEmbedCode = '', panorama_filename='" . addslashes($building_panorama_name) . "', type_3d='pano', lang = " . $lang_id . ", threedee_request_id = '" . $threedee_request_id . "', dirty = 0 WHERE building_code = '" . $building_code . "' and lang=" . $lang_id . " and presentation_name = '" . $building_panorama_name . "'";
//echo "update building presentation query string: " . $update_pano_query_string . "\n\r\n\r";
                $update_pano_query = mysql_query($update_pano_query_string) or die("update building presentation_query error: " . mysql_error());
            } else {
                // new 3d
                $insert_pano_query_string = "insert into files_3d (suite_id, suite_name, building_id, building_code, presentation_link, presentation_description, presentation_name, presentation_sppfile, presentation_thumb, lastModifiedTime, sketchfabEmbedCode, panorama_filename, type_3d, lang, threedee_request_id) values ('','','" . $building_id . "', '" . $building_code . "', '" . addslashes($building_panorama_link) . "', '" . addslashes($building_panorama_description) . "', '" . addslashes($building_panorama_name) . "', '', '" . addslashes($building_3dPanoThumb_name) . "', '" . addslashes($building_panorama_lastModifiedTime) . "', '" . addslashes($building_panorama_sketchfabEmbedCode) . "', '" . $building_panorama_name . "', 'pano', " . $lang_id . ", '" . $threedee_request_id . "')";
//echo "insert building presentation query string: " . $insert_pano_query_string . "\n\r\n\r";
                $insert_pano_query = mysql_query($insert_pano_query_string) or die("insert building presentation_query error: " . mysql_error());
            }


            if ($newbuilding3dPano_modified_time > $existingbuilding3dPano_modified_time) {
                // delete existing
                if (is_file($base_file_path . "images/threedee_files/" . $building3dPano_check['presentation_thumb'])) {
                    unlink($base_file_path . "images/threedee_files/" . $building3dPano_check['presentation_thumb']); // delete file
                    unlink($base_file_path . "images/threedee_files/" . $building3dPano_check['panorama_filename']); // delete file
//echo "\n\rexisting building 3d file deleted: " . $building3d_check['presentation_thumb'] . "\n\r\n\r";
                }
//                file_put_contents($base_file_path . 'images/threedee_files/' . $building_3dPanoimage_name, file_get_contents($building3d_image_url));
                
                
                if (file_put_contents($base_file_path . 'images/threedee_files/' . $building_3dPanoimage_name, file_get_contents($arcestra_file_link))) {
//echo "DL: suite 2d downloaded\n\r\n\r";
//                    echo "pano downloaded\n\r\n\r";
                    try {
//                        exec('/usr/local/bin/gs -sDEVICE=jpeg -r300 -o ' . $base_file_path . 'images/twodee_files/' . $plan_thumb . ' -dFirstPage=1 -dLastPage=1 ' . $base_file_path . 'images/twodee_files/' . $plan_name . '');
//                        echo "thumb attempt\n\r\n\r";
                        exec("/usr/local/bin/convert " . $base_file_path . "images/threedee_files/" . $building_3dPanoimage_name . " -thumbnail '180x' " . $base_file_path . "images/threedee_files/" . $building_3dPanoThumb_name . "");
                    } catch (Exception $e) {
                        echo "\nCannot create Pano thumbnail for " . $plan_name . "\n";
                        echo 'Caught exception: ', $e->getMessage(), "\n";
                    }
                } else {
                    // download failed
                    echo "Panorama Download Failed\r\n\r\n";
                }
                
                
                
                
//echo "\n\rDL: building 3d downloaded\n\r\n\r";
                //		echo "\n 3D Presentation Thumbnail ".$suite_3dimage_name." Successfully downloaded \n";				
            } // end if newbuilding3d > existingbuilding3d
        }






























//
// HANDLE THE PREVIEW IMAGES (GALLERY IMAGES)
//$del_previewimages = mysql_query("DELETE FROM preview_images WHERE building_code='" . $building_code . "' and lang = '".$lang_id."'");
        $dirty_previewimages = mysql_query("UPDATE preview_images SET dirty=1 WHERE building_code = '" . $building_code . "'");


//foreach ($Building->previewimages->image as $preview) {

        foreach ($Building->previewImages->file as $buildingpreview) {
            $buildingpreview_name = "{$buildingpreview['name']}";
            $buildingpreview_name = $building_id . $buildingpreview_name;
            $buildingpreview_displayname = "{$buildingpreview['name']}";
            $buildingpreview_name = str_replace(' ', '_', $buildingpreview_name);
            $buildingpreview_name = str_replace('\'', '', $buildingpreview_name);
            $buildingpreview_list_index = "{$buildingpreview['listIndex']}";
            $buildingpreview_description = "{$buildingpreview['description']}";
            $buildingpreview_image_uri = "{$buildingpreview->uri}";
            $buildingpreview_list_index = "{$buildingpreview['listIndex']}";
            $buildingpreview_extension = "{$buildingpreview['extension']}";
            $buildingpreview_last_modified = "{$buildingpreview['lastModifiedTime']}";



            if (stripos($buildingpreview_name, $buildingpreview_extension) > -1) {
                $buildingpreview_name = str_ireplace("." . $buildingpreview_extension, $buildingpreview_list_index . "." . $buildingpreview_extension, $buildingpreview_name);
            } else {
                $buildingpreview_name = $buildingpreview_name . $buildingpreview_list_index . "." . $buildingpreview_extension;
            }

//echo "BUILDING LEVEL IMAGE FOUND: " . $building_code . " --- " . $preview_name . "\n\r";
//		$preview_insert = mysql_query("replace into preview_images (lang, building_id, building_code, file_name, list_index, uri) values ('" . addslashes($lang_id) . "', '". addslashes($building_id) . "', '" . addslashes($building_code) . "','" . addslashes($preview_name) . "', '" . addslashes($preview_list_index) . "', '" . addslashes($preview_image) . "')") or die ("insert previewimages error: ".mysql_error());

            $buildingpreview_check_query_string = "Select * from preview_images where building_code = '" . $building_code . "' and file_name = '" . $buildingpreview_name . "' and uri = '" . $buildingpreview_image_uri . "'";
//echo "buildingpreview images check query: " . $buildingpreview_check_query_string . "\r\n\r\n";
            $buildingpreview_check_query = mysql_query($buildingpreview_check_query_string) or die("buildingpreview check error: " . mysql_error());
            $buildingpreview_check = mysql_fetch_array($buildingpreview_check_query);
            $buildingpreview_check_count = mysql_num_rows($buildingpreview_check_query);
            $newbuildingpreview_modified_time = '';
            $existingbuildingpreview_modified_time = '';
            $newbuildingpreview_modified_time = strtotime($buildingpreview_last_modified);
            $existingbuildingpreview_modified_time = strtotime($buildingpreview_check['last_modified_time']);

//echo "buildingpreview check count: " . $buildingpreview_check_count . "\n\r\n\r";


            if ($buildingpreview_check_count > 0) {
// document is already present, update record //
//echo "existing building preview image \n\r";

                $buildingpreviewimages_query_string = "update preview_images set building_id='" . $building_id . "',  building_code='" . $building_code . "', file_name='" . addslashes($buildingpreview_name) . "', display_file_name = '" . $buildingpreview_displayname . "', extension='" . addslashes($buildingpreview_extension) . "', list_index='" . addslashes($buildingpreview_list_index) . "', description='" . addslashes($buildingpreview_description) . "', uri='" . addslashes($buildingpreview_image_uri) . "', last_modified_time='" . $buildingpreview_last_modified . "', dirty = '0' where building_code = '" . $building_code . "' and file_name = '" . $buildingpreview_name . "' and uri = '" . $buildingpreview_image_uri . "'";

//echo "UPDATE buildingpreviewimages query string: " . $buildingpreviewimages_query_string . "\n\r\n\r";

                $buildingpreviewimages_query = mysql_query($buildingpreviewimages_query_string) or die("Update buildingPreview Images Error: " . mysql_error());
            } else {
//// new document
//echo "NEW building preview image \n\r";

                $buildingpreviewimages_query_string = "insert into preview_images (building_id, building_code, file_name, display_file_name, extension, list_index, description, uri, last_modified_time, dirty) values ('" . addslashes($building_id) . "', '" . addslashes($building_code) . "', '" . addslashes($buildingpreview_name) . "', '" . $buildingpreview_displayname . "', '" . addslashes($buildingpreview_extension) . "', '" . addslashes($buildingpreview_list_index) . "', '" . addslashes($buildingpreview_description) . "', '" . addslashes($buildingpreview_image_uri) . "', '" . $buildingpreview_last_modified . "', '0')";


//echo "INSERT buildingpreviewimages query string: " . $buildingpreviewimages_query_string . "\n\r\n\r";


                $buildingpreviewimages_query = mysql_query($buildingpreviewimages_query_string) or die("Insert Building Preview Images Error: " . mysql_error());
            }



//echo "last modified XML: " . $buildingpreview_last_modified ."\n\r";
//echo "last modified DB : " . $buildingpreview_check['last_modified_time'] ."\n\r";
//echo "new building preview modified time: " . $newbuildingpreview_modified_time ."\n\r";
//echo "existing building preview modified: " . $existingbuildingpreview_modified_time ."\n\r\n\r";
// if file is newer than existing
            if ($newbuildingpreview_modified_time > $existingbuildingpreview_modified_time) {
                // delete existing
                if (is_file($base_file_path . "images/preview_images/" . $buildingpreview_name)) {
                    unlink($base_file_path . "images/preview_images/" . $buildingpreview_name); // delete file
//echo "deleted existing file\n\r";
                }

                if (file_put_contents($base_file_path . 'images/preview_images/' . $buildingpreview_name, file_get_contents($buildingpreview_image_uri . "?width=1000&height=750&resizeMode=OUTER"))) {
//echo "\nDL: ". $buildingpreview_name ." has been downloaded successfully. " . $building_code . " -- Buildingpreview.\n";
                } else {
//echo "download failed for ". $preview_name ." \n\r\n\r";
                }

                // GET THUMBNAIL FOR PREVIEW IMAGE
                $buildingpreview_thumb_uri = $buildingpreview_image_uri . "?width=700&height=500&resizeMode=INNER";
                $buildingpreview_thumb_uri2 = $buildingpreview_image_uri . "?width=320&height=240&resizeMode=INNER";
                $buildingpreview_thumb_name_start = str_ireplace("." . $buildingpreview_extension, '', $buildingpreview_name);
                $buildingpreview_thumb_name = $buildingpreview_thumb_name_start . "_thumb." . $buildingpreview_extension;
                $buildingpreview_thumb_name2 = $buildingpreview_thumb_name_start . "_thumbc." . $buildingpreview_extension;

                if (file_put_contents($base_file_path . 'images/preview_images/' . $buildingpreview_thumb_name, file_get_contents($buildingpreview_thumb_uri))) {
//echo "\nDL: ". $buildingpreview_name ." THUMBNAIL has been downloaded successfully. " . $building_code . " -- Building preview thumb.\n";
                    file_put_contents($base_file_path . 'images/preview_images/' . $buildingpreview_thumb_name2, file_get_contents($buildingpreview_thumb_uri2));
                    
                } else {
//echo "THUMBNAIL download failed for ". $preview_name ." \n\r\n\r";
                }
            }
        } //end foreach $building->previewImages->file as $preview		






        if ($building_thumbnail != '') {
//echo "\n\r\n\rbuilding thumbnail before check: " . $building_thumbnail . "\n\r\n\r";
            // CHECK FOR BUILDING THUMBNAIL
            $building_thumbnail_name = $building_code . "_thumbnail.jpg";
            $building_thumbnail_thumb_name = str_replace('.jpg', '_thumb.jpg', $building_thumbnail_name);
            $building_thumbnail_thumb_name_carousel = str_replace('.jpg', '_thumbc.jpg', $building_thumbnail_name);
            $building_thumbnail_thumb_name_list = str_replace('.jpg', '_listthumb.jpg', $building_thumbnail_name);
            $building_thumbnail_thumb_name_grid = str_replace('.jpg', '_gridthumb.jpg', $building_thumbnail_name);
            $building_thumbnail_check_query = "select * from preview_images where building_code = '" . $building_code . "' and file_name = '" . $building_thumbnail_name . "' and thumbnail = 1";
//echo "building thumbnail check query: " . $building_thumbnail_check_query . "\n\r\n\r";
            $building_thumbnail_check = mysql_query($building_thumbnail_check_query) or die("building thumbnail check error: " . mysql_error());
            $building_thumbnail_check_results = mysql_fetch_array($building_thumbnail_check);
            $building_thumbnail_check_count = mysql_num_rows($building_thumbnail_check);


            $newbuildingthumbnail_modified_time = '';
            $existingbuildingthumbnail_modified_time = '';
            $newbuildingthumbnail_url = $building_thumbnail;
            $existingbuildingthumbnail_url = $building_thumbnail_check_results['uri'];

            // ADD BUILDING THUMBNAIL TO DB
            // need last modified time check added here.



            if ($building_thumbnail_check_count > 0) {
                $building_thumbnail_query_string = "update preview_images set building_id='" . $building_id . "',  building_code='" . $building_code . "', file_name='" . addslashes($building_thumbnail_name) . "', extension='jpg', uri='" . addslashes($building_thumbnail) . "', thumbnail='1', dirty = '0' where building_code = '" . $building_code . "' and file_name = '" . $building_thumbnail_name . "' and thumbnail=1";
            } else {
                $building_thumbnail_query_string = "insert into preview_images (building_id, building_code, file_name, uri, thumbnail, dirty) values ('" . addslashes($building_id) . "', '" . addslashes($building_code) . "', '" . addslashes($building_thumbnail_name) . "', '" . addslashes($building_thumbnail) . "', '1', '0')";
            }

            $building_thumbnail_query = mysql_query($building_thumbnail_query_string) or die("insert building thumbnail error: " . mysql_error());


            // BUILDING "THUMBNAIL" DOWNLOAD
            $buildingthumbnail_url = '';
            $buildingthumbnail_url = $building_thumbnail . "?width=700&height=700&resizeMode=INNER";
            $buildingthumbnail_thumb_url = $building_thumbnail . "?width=700&height=500&resizeMode=INNER";
            $buildingthumbnail_thumb_url2 = $building_thumbnail . "?width=320&height=240&resizeMode=INNER";
            $buildingthumbnail_thumb_url_list = $building_thumbnail . "?width=100&height=100&resizeMode=INNER";
            $buildingthumbnail_thumb_url_grid = $building_thumbnail . "?width=520&height=200&resizeMode=INNER";
//echo "Thumb URL: " . $thumbnail_url . "\n\r\n\r";
//echo "New url: " . $newbuildingthumbnail_url . "\n\r\n\r";
//echo "old url: " . $existingbuildingthumbnail_url . "\n\r\n\r";
// if file is newer than existing
            if ($newbuildingthumbnail_url != $existingbuildingthumbnail_url) {

                // delete existing
                if (is_file($base_file_path . 'images/preview_images/' . $building_thumbnail_name)) {
                    unlink($base_file_path . 'images/preview_images/' . $building_thumbnail_name); // delete file
                }

                if (file_put_contents($base_file_path . 'images/preview_images/' . $building_thumbnail_name, file_get_contents($buildingthumbnail_url))) {
//echo "\nDL: ". $building_thumbnail_name ." has been downloaded successfully. " . $building_code . " -- Building Thumbnail.\n";
                    file_put_contents($base_file_path . 'images/preview_images/' . $building_thumbnail_thumb_name, file_get_contents($buildingthumbnail_thumb_url));
                    file_put_contents($base_file_path . 'images/preview_images/' . $building_thumbnail_thumb_name_carousel, file_get_contents($buildingthumbnail_thumb_url2));
                    file_put_contents($base_file_path . 'images/preview_images/' . $building_thumbnail_thumb_name_list, file_get_contents($buildingthumbnail_thumb_url_list));
                    file_put_contents($base_file_path . 'images/preview_images/' . $building_thumbnail_thumb_name_grid, file_get_contents($buildingthumbnail_thumb_url_grid));
                } else {
//        echo "download failed for ". $preview_name ." \n\r\n\r";
                }
            }
        }










        $del_suites = mysql_query("DELETE FROM suites WHERE lang=" . $lang_id . " and building_code='" . $building_code . "'") or die("suite delete error: " . mysql_error());

// HANDLE THE SUITES	

        foreach ($Building->suites->suite as $Suite) {
//echo "\n\r\n\r==================NEW SUITE====================\n\r\n\r";
            $suite_id = "{$Suite['suiteid']}";
            $suite_name = "{$Suite['name']}";
            $suite_name_clean = str_replace(' ', '', $suite_name);
            $suite_name_clean = str_replace('\'', '', $suite_name_clean);
            $suite_name_clean = str_replace('&amp;', '', $suite_name_clean);
            $suite_name_clean = str_replace('&', '', $suite_name_clean);
            $suite_name_clean = str_replace('/', '-', $suite_name_clean);
            $suite_name_clean = str_replace('\\', '-', $suite_name_clean);
            $suite_name_clean = str_replace(',', '-', $suite_name_clean);
            $suite_net_rentable_area = "{$Suite['net-rentable-area']}";
            $suite_contiguous_area = "{$Suite['contiguousArea']}";
            $suite_minimum_divisible = "{$Suite['minDivisible']}";
//			if ($suite_net_rentable_area > $suite_contiguous_area){
//				$suite_contiguous_area = $suite_net_rentable_area;
//			}
            $suite_floor_name = "{$Suite->floor['name']}";
            $suite_floor = "{$Suite->floor['floor-number']}";
            $suite_type = "{$Suite['suiteType']}";
            $suite_taxes_and_operating = "{$Suite['taxes-and-operating']}";
            $suite_availability = "{$Suite['availability']}";

            // set notice period default to "no" for easy checking later on
            $suite_notice_period = "no";
            if ($Suite['noticePeriod'] != '') {
//                        (30d, 60d, 90d, 120d)
                $suite_notice_period = "{$Suite['noticePeriod']}";
            }
            $bu_flag = "{$Suite['sendBrokerUpdates']}";



            if ($suite_notice_period != 'no') {

                switch ($suite_notice_period) {
                    case "30d":
                        $suite_availability = strtotime(date('Y-m-d', strtotime("+31 days")));
//                                    echo "30: " . strtotime(date('Y-m-d', strtotime("+30 days"))) . "\n\r";
                        break;

                    case "60d":
                        $suite_availability = strtotime(date('Y-m-d', strtotime("+62 days")));
//                                    echo "60: " . strtotime(date('Y-m-d', strtotime("+60 days"))) . "\n\r";
                        break;

                    case "90d":
                        $suite_availability = strtotime(date('Y-m-d', strtotime("+93 days")));
//                                    echo "90: " . strtotime(date('Y-m-d', strtotime("+90 days"))) . "\n\r";
                        break;

                    case "120d":
                        $suite_availability = strtotime(date('Y-m-d', strtotime("+124 days")));
//                                    echo "120: " . strtotime(date('Y-m-d', strtotime("+120 days"))) . "\n\r";
                        break;
                }
            } else {

                // turn it into a timestamp
                $suite_availability = strtotime($suite_availability);



                //if $suite_availability is blank, this means that the dates were blanked at Arcestra's CoRE.
                //Get today's date
                if (is_nan($suite_availability) || $suite_availability == '' || is_null($suite_availability)) {
                    $date = date("d-m-Y", time()); //this will avoid the hours
                    //                        $suite_availability = strtotime(date("d-m-Y"));
                    //                        $suite_availability = $suite_availability + 86400;
                    $suite_availability = 2;
                }
            }








            $suite_description = "{$Suite->description}";
            $suite_net_rent = "{$Suite['netRent']}";
            $suite_add_rent_total = "{$Suite['addrentTotal']}";
            $suite_new = "{$Suite['newSuite']}";
            $suite_model = "{$Suite['modelSuite']}";
            $suite_leased = "{$Suite['justLeased']}";
            $suite_promoted = "{$Suite['promotedSuite']}";


//                        if ($building_code == "duke") {
//                            echo "suitename: " . $suite_name . "\n\r";
//                            echo "Promoted: " . $suite_promoted . "\n\r\n\r";
//                        }




            if (strpos($suite_net_rent, '$') > -1) {
                $suite_net_rent = str_replace('$', '', $suite_net_rent);
            }

            if (strpos($suite_taxes_and_operating, '$') > -1) {
                $suite_taxes_and_operating = str_replace('$', '', $suite_taxes_and_operating);
            }



//			$del_3dpres = mysql_query("DELETE FROM files_3d WHERE lang=" . $lang_id . " and building_code='" . $building_code . "' and suite_id = '".$suite_id."'") or die ("3d delete error: " . mysql_error());
//			$del_suitefiles2 = mysql_query("DELETE FROM suite_preview_images WHERE lang=" . $lang_id . " and building_code='" . $building_code . "' and suite_id = '".$suite_id."'") or die ("suitedocs2 delete error: " . mysql_error());
            $dirty_suite3d = mysql_query("UPDATE files_3d SET dirty=1 WHERE building_code = '" . $building_code . "' and lang='" . $lang_id . "' and suite_id = '" . $suite_id . "'");


            // ONLY import public suites		
            if ("{$Suite['public']}" == "true") {
//                if ($suite_leased != "true") {
                    $suitecount++;
//                }
                $i = 0;
                $randnum = uniqid();

                foreach ($Suite->presentations->presentation as $PresentationE) {
//echo "\n\r\n\r==================NEW SUITE 3D====================\n\r\n\r";
//		echo "in presentations\n\r\n\r";

                    $suite_presentation_link = "{$PresentationE->uri}";
                    $suite_presentation_name = "{$PresentationE['name']}";

//		echo "Presentation name: " . $suite_presentation_name . "\n\r\n\r";

                    $suite_presentation_name = str_replace(' ', '_', $suite_presentation_name);
                    $suite_presentation_description = "{$PresentationE['description']}";
                    $suite_presentation_spp_link = "{$PresentationE->sppUri}";
                    $suite_presentation_lastModifiedTime = "{$PresentationE['lastModifiedTime']}";
                    $suite_presentation_sketchfabEmbedCode = "{$PresentationE->sketchfabEmbedCode}";
                    $threedee_request_id = "0";
                    $id_pos = strpos($suite_presentation_link, '=');
                    $suite3d_id = substr($suite_presentation_link, $id_pos + 1);
                    $suite3d_image_url = "http://www.arcestra.com/fileDl/fetchPresentationFile.action?id=" . $suite3d_id . "&fileAccessMethod=PUBLIC_FILE_LINK&presentationContent=presentation%2FmasterImage.png";
                    $suite_3dimage_name = $building_code . '_' . $suite_name_clean . '_' . $suite_presentation_name . '.png';
                    $suitethreed_check_query_string = "Select * from files_3d where building_code = '" . $building_code . "' and suite_id = '" . $suite_id . "' and presentation_name = '" . $suite_presentation_name . "' and presentation_link = '" . $suite_presentation_link . "' and lang = '" . $lang_id . "'";
//echo "\n\r" . $suite_name_clean . " suitethreed check querystring: " . $suitethreed_check_query_string . "\n\r\n\r";
                    $suitethreed_check_query = mysql_query($suitethreed_check_query_string) or die("suitethreed check error: " . mysql_error());
                    $suitethreed_check = mysql_fetch_array($suitethreed_check_query);
                    $suitethreed_check_count = mysql_num_rows($suitethreed_check_query);
                    $newsuite3d_modified_time = '';
                    $existingsuite3d_modified_time = '';


//echo $suite_name_clean . " --start suite dump\n\r\n\r";
//echo $suitethreed_check['suite_id'] . "\n\r";
//echo $suitethreed_check['suite_name'] . "\n\r";
//echo $suitethreed_check['building_code'] . "\n\r";
//echo $suitethreed_check['presentation_name'] . "\n\r";
//echo $suitethreed_check['lastModifiedTime'] . "\n\r";
//echo "end dump\n\r\n\r";





                    $newsuite3d_modified_time = strtotime($suite_presentation_lastModifiedTime);
                    $existingsuite3d_modified_time = strtotime($suitethreed_check['lastModifiedTime']);


//echo "suite3d check count : " . $suitethreed_check_count . "\n\r\n\r";

                    if ($suitethreed_check_count > 0) {
                        // existing 3d
                        $update_pres_query_string = "update files_3d set suite_id = '" . $suite_id . "', suite_name = '" . addslashes($suite_name) . "', building_id = '" . $building_id . "', building_code = '" . $building_code . "', presentation_link = '" . addslashes($suite_presentation_link) . "', presentation_description = '" . addslashes($suite_presentation_description) . "', presentation_name = '" . addslashes($suite_presentation_name) . "', presentation_sppfile = '" . addslashes($suite_presentation_spp_link) . "', presentation_thumb = '" . addslashes($suite_3dimage_name) . "', lastModifiedTime = '" . addslashes($suite_presentation_lastModifiedTime) . "', sketchfabEmbedCode = '" . addslashes($suite_presentation_sketchfabEmbedCode) . "', lang = " . $lang_id . ", threedee_request_id = '" . $threedee_request_id . "', dirty = 0 WHERE building_code = '" . $building_code . "' and lang=" . $lang_id . " and presentation_name = '" . $suite_presentation_name . "' and presentation_link = '" . $suite_presentation_link . "' and suite_id = '" . $suite_id . "'";
                        $update_pres_query = mysql_query($update_pres_query_string) or die("update pres_query error: " . mysql_error());
//echo "update presentation query string: " . $update_pres_query_string	. "\n\r\n\r";
                    } else {
                        // new 3d
                        $insert_pres_query_string = "insert into files_3d (suite_id, suite_name, building_id, building_code, presentation_link, presentation_description, presentation_name, presentation_sppfile, presentation_thumb, lastModifiedTime, sketchfabEmbedCode, lang, threedee_request_id) values ('" . $suite_id . "', '" . addslashes($suite_name) . "', '" . $building_id . "', '" . $building_code . "', '" . addslashes($suite_presentation_link) . "', '" . addslashes($suite_presentation_description) . "', '" . addslashes($suite_presentation_name) . "', '" . addslashes($suite_presentation_spp_link) . "', '" . addslashes($suite_3dimage_name) . "', '" . addslashes($suite_presentation_lastModifiedTime) . "', '" . addslashes($suite_presentation_sketchfabEmbedCode) . "', " . $lang_id . ", '" . $threedee_request_id . "')";
                        $insert_pres_query = mysql_query($insert_pres_query_string) or die("insert pres_query error: " . mysql_error());

//$function_name = $suite_name_clean . " - Suite 3d";
//$event = $suitethreed_check['presentation_name'] . " -- file downloaded";
//$note = "File shouldnt be downloading";
//db_report($function_name, $event, $note);
//echo "insert presentation query string: " . $insert_pres_query_string	. "\n\r\n\r";
                    }

//echo "\n\rxml lastmodified suitepres: " . $suite_presentation_lastModifiedTime . "\n\r";
//echo "db  lastmodified suitepres: " . $suitethreed_check['lastModifiedTime'] . "\n\r\n\r";
//echo "new suite lvl 3d modified: " . $newsuite3d_modified_time . "\n\r";
//echo "existg suite 3d modified : " . $existingsuite3d_modified_time . "\n\r";



                    if ($newsuite3d_modified_time > $existingsuite3d_modified_time) {
                        if (is_file($base_file_path . "images/threedee_files/" . $suite_3dimage_name)) {
                            unlink($base_file_path . "images/threedee_files/" . $suite_3dimage_name); // delete file
//echo "Existing suite 3d deleted\n\r\n\r";                            
                        }

                        file_put_contents($base_file_path . 'images/threedee_files/' . $suite_3dimage_name, file_get_contents($suite3d_image_url));
//echo "DL: suite 3d downloaded\n\r\n\r";
                        $i++;
                    } // end if new modified time > existing modified time
                } // end foreach suite presentation








                $p = 0;

                $dirty_2d = mysql_query("UPDATE files_2d SET dirty=1 WHERE building_code = '" . $building_code . "' and lang=" . $lang_id . " and suite_id = '" . $suite_id . "' and suite_featured_plan != 1");

                foreach ($Suite->plans2d->file as $twodee) {
                    $display_filename = "{$twodee['name']}";
                    $plan_name = "{$twodee['name']}";
                    $plan_name = $building_code . $p . $plan_name;
                    $plan_name = str_replace(' ', '_', $plan_name);
                    $plan_name = str_replace('(', '', $plan_name);
                    $plan_name = str_replace(')', '', $plan_name);
                    $plan_name = str_replace('*', '', $plan_name);
                    $plan_name = str_replace('\\', '', $plan_name);
                    $plan_name = str_replace('/', '', $plan_name);

                    $plan_description = "{$twodee['description']}";
                    $plan_description = str_replace(' ', '_', $plan_description);
                    $plan_description = str_replace('(', '', $plan_description);
                    $plan_description = str_replace(')', '', $plan_description);
                    $plan_description = str_replace('*', '', $plan_description);
                    $plan_description = str_replace('\\', '', $plan_description);
                    $plan_description = str_replace('/', '', $plan_description);

                    $plan_ext = "{$twodee['extension']}";

                    if (strpos($plan_name, $plan_ext) != FALSE) {
                        // add file description to name
                        $plan_name = str_replace('.' . $plan_ext, '_' . $plan_description . '.' . $plan_ext, $plan_name);
                    } else {
                        // add file description and '.plan_ext' to name
                        $plan_name = $plan_name . '_' . $plan_description . '.' . $plan_ext;
                    }

//echo "Plan Name: " . $plan_name . "\n\r\n\r";

                    $plan_uri = "{$twodee->uri}";
                    //$plan_thumb = str_replace('.pdf','.'.$p.'.jpg',$plan_name);
                    $plan_thumb = str_replace('.pdf', '.jpg', $plan_name);
                    $file_last_modified = "{$twodee['lastModifiedTime']}";

                    $document_check_query_string = "Select * from files_2d where building_code = '" . $building_code . "' and suite_id = '" . $suite_id . "' and plan_name = '" . $plan_name . "' and lang = '" . $lang_id . "'";

                    $document_check_query = mysql_query($document_check_query_string) or die("document check error: " . mysql_error());
                    $document_check = mysql_fetch_array($document_check_query);
                    $document_check_count = mysql_num_rows($document_check_query);
                    $newdocument_modified_time = '';
                    $existingdocument_modified_time = '';
                    $newdocument_modified_time = strtotime($file_last_modified);
                    $existingdocument_modified_time = strtotime($document_check['lastModifiedTime']);


                    if ($document_check_count > 0) {
                        // document is already present, update record //
//            echo "existing doc\n\r\n\r";
                        $twodee_query_string = "update files_2d set suite_id='" . addslashes($suite_id) . "', suite_name='" . addslashes($suite_name) . "', suite_floor='" . $suite_floor . "', building_id='" . $building_id . "', building_code='" . $building_code . "', plan_name='" . addslashes($plan_name) . "', display_filename='" . addslashes($display_filename) . "', plan_description='" . addslashes($plan_description) . "', plan_uri='" . addslashes($plan_uri) . "', plan_thumb='" . addslashes($plan_thumb) . "', lastModifiedTime='" . $file_last_modified . "', lang='" . $lang_id . "', dirty='0' where building_code = '" . $building_code . "' and plan_name = '" . $plan_name . "' and lang = '" . $lang_id . "'";
//            echo $twodee_query_string . "\n\r\n\r";
                        $twodee_query = mysql_query($twodee_query_string) or die("Update Suite Files2d Error: " . mysql_error());
                    } else {
                        // new document
//            echo "new doc\n\r\n\r";
                        $twodee_query_string = "insert into files_2d (suite_id, suite_name, suite_floor, building_id, building_code, plan_name, display_filename, plan_description, plan_uri, plan_thumb, lastModifiedTime, lang, dirty) values ('" . addslashes($suite_id) . "', '" . addslashes($suite_name) . "', '" . $suite_floor . "', '" . $building_id . "', '" . $building_code . "', '" . addslashes($plan_name) . "', '" . addslashes($display_filename) . "', '" . addslashes($plan_description) . "', '" . addslashes($plan_uri) . "', '" . addslashes($plan_thumb) . "', '" . $file_last_modified . "', '" . $lang_id . "', '0')";
//            echo $twodee_query_string . "\n\r\n\r";
                        $twodee_query = mysql_query($twodee_query_string) or die("Insert Suite Files2d Error: " . mysql_error());
                    }
//echo "twodee query string: " . $twodee_query_string . "\n\r\n\r";
//echo "twodeestuff\n\r\n\r";
                    // if file is newer than existing file
                    if ($newdocument_modified_time > $existingdocument_modified_time) {
// delete existing file and thumbnail
//echo "updatedfile\n\r\n\r";
                        if (is_file($base_file_path . "images/twodee_files/" . $plan_name)) {
                            unlink($base_file_path . "images/twodee_files/" . $plan_name); // delete file
                        }
                        if (is_file($base_file_path . "images/twodee_files/" . $plan_thumb)) {
                            unlink($base_file_path . "images/twodee_files/" . $plan_thumb); // delete thumb
                        }

// download new file and make new thumbnail
//echo "newdownload\n\r\n\r";
                        if (file_put_contents($base_file_path . 'images/twodee_files/' . $plan_name, file_get_contents($plan_uri))) {
//echo "DL: suite 2d downloaded\n\r\n\r";
                            try {
                                exec('/usr/local/bin/gs -sDEVICE=jpeg -r300 -o ' . $base_file_path . 'images/twodee_files/' . $plan_thumb . ' -dFirstPage=1 -dLastPage=1 ' . $base_file_path . 'images/twodee_files/' . $plan_name . '');

                                exec("/usr/local/bin/convert " . $base_file_path . "images/twodee_files/" . $plan_thumb . " -thumbnail '180x' " . $base_file_path . "images/twodee_files/thumb_" . $plan_thumb . "");
                                
                                exec("/usr/local/bin/convert " . $base_file_path . "images/twodee_files/" . $plan_thumb . " -thumbnail 'x240' " . $base_file_path . "images/twodee_files/thumbc_" . $plan_thumb . "");
                            } catch (Exception $e) {
                                echo "\nCannot create PDF thumbnail for " . $plan_name . "\n";
                                echo 'Caught exception: ', $e->getMessage(), "\n";
                            }
                        }
                    }
                    $p++;
                }// end foreach 2d













                $suitedocs_dirty = mysql_query("UPDATE suite_documents SET dirty=1 WHERE building_code = '" . $building_code . "' and suite_id = '" . $suite_id . "'");
                // HANDLE SUITE VIDEOS
                $q = 0;

                foreach ($Suite->videos->file as $suitevid) {
//	echo "in suite videos\n\r\n\r";			
                    $file_name = '';
                    $file_name = "{$suitevid['name']}";
                    $file_name = str_replace(' ', '_', $file_name);
                    $file_name = str_replace('(', '', $file_name);
                    $file_name = str_replace(')', '', $file_name);
                    $file_name = str_replace('*', '', $file_name);
                    $file_name = str_replace('\\', '', $file_name);
                    $file_name = str_replace('/', '', $file_name);

                    $file_desc = "{$suitevid['description']}";
                    $file_desc = str_replace(' ', '_', $file_desc);
                    $file_desc = str_replace('(', '', $file_desc);
                    $file_desc = str_replace(')', '', $file_desc);
                    $file_desc = str_replace('*', '', $file_desc);
                    $file_desc = str_replace('\\', '', $file_desc);
                    $file_desc = str_replace('/', '', $file_desc);

                    $file_link = "{$suitevid->uri}";
                    $file_ext = "{$suitevid['extension']}";

                    if (strpos($file_name, $file_ext) != FALSE) {
                        // add file description to name
                        $file_name = str_replace('.' . $file_ext, '_' . $file_desc . '.' . $file_ext, $file_name);
                    } else {
                        // add file description and '.pdf' to name
                        $file_name = $file_name . '_' . $file_desc . '.' . $file_ext;
                    }

//			echo "video file name: " . $file_name . "\n\r\n\r";

                    $file_last_modified = "{$suitevid['lastModifiedTime']}";

                    $suiteviddocument_check_query_string = "Select * from suite_documents where building_code = '" . $building_code . "' and file_name = '" . $file_name . "'";

//		echo "document check query string: " . $document_check_query_string . "\n\r\n\r";

                    $suiteviddocument_check_query = mysql_query($suiteviddocument_check_query_string) or die("suiteviddocument check error: " . mysql_error());
                    $suiteviddocument_check = mysql_fetch_array($suiteviddocument_check_query);
                    $suiteviddocument_check_count = mysql_num_rows($suiteviddocument_check_query);
                    $newdocument_modified_time = '';
                    $existingdocument_modified_time = '';
                    $newdocument_modified_time = strtotime($file_last_modified);
                    $existingdocument_modified_time = strtotime($suiteviddocument_check['lastModifiedTime']);

//		echo "Document check count: " . $document_check_count . "\n\r\n\r";			


                    if ($suiteviddocument_check_count > 0) {
                        // document is already present, update record //
//		echo "existing video\n\r\n\r";

                        $suitevid_query_string = "update suite_documents set building_id='" . $building_id . "',  building_code='" . addslashes($building_code) . "', suite_id='" . addslashes($suite_id) . "', suite_name='" . addslashes($suite_name) . "', file_name='" . addslashes($file_name) . "', description='" . addslashes($file_desc) . "', uri='" . addslashes($file_link) . "', lastModifiedTime='" . $file_last_modified . "', extension='" . addslashes($file_ext) . "', dirty='0' where building_code = '" . $building_code . "' and file_name = '" . $file_name . "' and lang = '" . $lang_id . "'";

//		echo "suitevid query string: " . $suitevid_query_string . "\n\r\n\r";

                        mysql_query($suitevid_query_string) or die("Update Suite Video Error: " . mysql_error());
                    } else {
                        // new document
//		echo "new video\n\r\n\r";

                        $suitevid_query_string = "insert into suite_documents (building_id, building_code, suite_id, suite_name, file_name, description, uri, lastModifiedTime, extension, dirty) values ('" . $building_id . "', '" . addslashes($building_code) . "','" . addslashes($suite_id) . "', '" . addslashes($suite_name) . "', '" . addslashes($file_name) . "', '" . addslashes($file_desc) . "', '" . addslashes($file_link) . "', '" . $file_last_modified . "', '" . addslashes($file_ext) . "', '0')";

//		echo "suitevid query string: " . $suitevid_query_string . "\n\r\n\r";

                        mysql_query($suitevid_query_string) or die("Insert Suite Video Error: " . mysql_error());
                    }

                    // if file is newer than existing
                    if ($newdocument_modified_time > $existingdocument_modified_time) {
                        // delete existing
                        if (is_file($base_file_path . "images/video_files/" . $file_name)) {
                            unlink($base_file_path . "images/video_files/" . $file_name); // delete file
                        }

                        // DOWNLOAD Suite LEVEL Video
                        if (file_put_contents($base_file_path . 'images/video_files/' . $file_name, file_get_contents($file_link))) {
//echo "\nDL: ". $file_name ." has been downloaded \n";
                        }
                    }
                }// end Suite videos

                $dirty_suitepreviewimages = mysql_query("UPDATE suite_preview_images SET dirty=1 WHERE building_code = '" . $building_code . "' and suite_id = '" . $suite_id . "'");


                // SUITE PREVIEW IMAGES
                foreach ($Suite->previewImages->file as $preview) {
//echo "\n\r\n\r==================NEW SUITE PREVIEW IMAGE====================\n\r\n\r";
                    $preview_name = "{$preview['name']}";
                    $preview_file_displayname = "{$preview['name']}";
                    $preview_name = str_replace(' ', '_', $preview_name);
                    $preview_name = str_replace('\'', '', $preview_name);
                    $preview_description = "{$preview['description']}";
                    $preview_extension = "{$preview['extension']}";
                    $preview_uri = "{$preview->uri}";
                    $preview_list_index = "{$preview['listIndex']}";
                    $preview_last_modified = "{$preview['lastModifiedTime']}";


                    if (stripos($preview_name, $preview_extension) > -1) {
                        $preview_name = $suite_id . '_' . str_ireplace($preview_extension, $preview_list_index . '.' . $preview_extension, $preview_name);
                    } else {
                        $preview_name = $suite_id . '_' . $preview_name . $preview_list_index . "." . $preview_extension;
                    }


//					$suitepreview_query_string = "insert into suite_preview_images (lang, building_id, building_code, suite_id, suite_name, list_index, file_name, extension, description, uri, last_modified_time) values ('" . $lang_id . "', '" . $building_id . "', '" . $building_code . "', '" . addslashes($suite_id) . "', '" . addslashes($suite_name) . "', '" . addslashes($preview_list_index) . "', '" . addslashes($preview_name) . "', '" . addslashes($preview_extension) . "', '" . addslashes($preview_description) . "', '" . addslashes($preview_uri) . "', '" . addslashes($preview_last_modified) . "')";
//					$suitepreview_query = mysql_query($suitepreview_query_string) or die ("suitepreview_query error: " . mysql_error());
//                                        
//                                        


                    $suitepreview_check_query_string = "Select * from suite_preview_images where suite_id = '" . $suite_id . "' and file_name = '" . $preview_name . "'";
//echo "suite preview check querystring: " . $suitepreview_check_query_string . "\n\r\n\r";
                    $suitepreview_check_query = mysql_query($suitepreview_check_query_string) or die("suitepreview check error: " . mysql_error());
                    $suitepreview_check = mysql_fetch_array($suitepreview_check_query);
                    $suitepreview_check_count = mysql_num_rows($suitepreview_check_query);
                    $newsuitepreview_modified_time = '';
                    $existingpreview_modified_time = '';

//echo "\n\rxml lastmodified suitepreviewimage: " . $preview_last_modified . "\n\r";
//echo "db  lastmodified suitepreviewimage: " . $suitepreview_check['last_modified_time'] . "\n\r\n\r";

                    $newsuitepreview_modified_time = strtotime($preview_last_modified);
                    $existingpreview_modified_time = strtotime($suitepreview_check['last_modified_time']);


//echo "suitepreview check count: " . $suitepreview_check_count . "\n\r\n\r";


                    if ($suitepreview_check_count > 0) {
                        // document is already present, update record //
//echo "existing document\n\r\n\r";

                        $suitepreviewimages_query_string = "update suite_preview_images set building_id='" . $building_id . "',  building_code='" . $building_code . "', suite_id='" . addslashes($suite_id) . "', suite_name='" . addslashes($suite_name) . "', list_index='" . addslashes($preview_list_index) . "', file_name='" . addslashes($preview_name) . "', display_file_name = '" . $preview_file_displayname . "', extension='" . addslashes($preview_extension) . "', description='" . addslashes($preview_description) . "', uri='" . addslashes($preview_uri) . "', last_modified_time='" . $preview_last_modified . "', dirty='0' where suite_id = '" . $suite_id . "' and file_name = '" . $preview_name . "'";

//echo "UPDATE suite previewimages query string: " . $suitepreviewimages_query_string . "\n\r\n\r";

                        $suitepreviewimages_query = mysql_query($suitepreviewimages_query_string) or die("Update Suite Preview Images Error: " . mysql_error());
                    } else {
                        // new document
//echo "NEW document\n\r\n\r";

                        $suitepreviewimages_query_string = "insert into suite_preview_images (building_id, building_code, suite_id, suite_name, list_index, file_name, display_file_name, extension, description, uri, last_modified_time, dirty) values ('" . addslashes($building_id) . "', '" . addslashes($building_code) . "', '" . addslashes($suite_id) . "', '" . addslashes($suite_name) . "', '" . addslashes($preview_list_index) . "', '" . addslashes($preview_name) . "', '" . $preview_file_displayname . "', '" . addslashes($preview_extension) . "', '" . addslashes($preview_description) . "', '" . addslashes($preview_uri) . "', '" . $preview_last_modified . "', 0)";


//echo "INSERT suite previewimages query string: " . $suitepreviewimages_query_string . "\n\r\n\r";


                        $suitepreviewimages_query = mysql_query($suitepreviewimages_query_string) or die("Insert Suite Preview Images Error: " . mysql_error());
                    }



//echo "new suitepreview modified: " . $newsuitepreview_modified_time . "\n\r";
//echo "exist suitepview modified: " . $existingpreview_modified_time . "\n\r\n\r";
                    // if file is newer than existing
                    if ($newsuitepreview_modified_time > $existingpreview_modified_time) {
                        // delete existing
                        if (is_file($base_file_path . "images/suite_preview_images/" . $preview_name)) {
                            unlink($base_file_path . "images/suite_preview_images/" . $preview_name); // delete file
                        }

                        if (file_put_contents($base_file_path . 'images/suite_preview_images/' . $preview_name, file_get_contents($preview_uri . "?width=1000&height=750&resizeMode=OUTER"))) {
//echo "\n\rDL: ". $preview_name ." has been downloaded successfully.  " . $suite_id . " -- Suitepreviewimage\r\n";
                            // GET THUMBNAIL FOR PREVIEW IMAGE
                            $preview_thumb_uri = $preview_uri . "?width=700&height=500&resizeMode=INNER";
                            $preview_thumb_uri_carousel = $preview_uri . "?width=320&height=240&resizeMode=INNER";
                            $preview_thumb_name_start = str_ireplace("." . $preview_extension, '', $preview_name);
                            $preview_thumb_name = $preview_thumb_name_start . "_thumb." . $preview_extension;
                            $preview_thumb_name_carousel = $preview_thumb_name_start . "_thumbc." . $preview_extension;

                            if (file_put_contents($base_file_path . 'images/suite_preview_images/' . $preview_thumb_name, file_get_contents($preview_thumb_uri))) {
//echo "\n\rDL: ". $preview_thumb_name ." has been downloaded successfully. " . $building_code . " --suitepreview thumb\r\n";
                                file_put_contents($base_file_path . 'images/suite_preview_images/' . $preview_thumb_name_carousel, file_get_contents($preview_thumb_uri_carousel));
                            } else {
                                //        echo "download failed for ". $preview_name ." \n\r\n\r";
                            }
                        } else {
//echo "\n\r Download Problem for ". $preview_name ."\n\r";
                        }
                    }
                }// end foreach preview images

















$pq = 0;
foreach ($Suite->plan->file as $suitePlan) {
                    $display_filename = "{$suitePlan['name']}";
                    $suiteFeaturedPlan_name = "{$suitePlan['name']}";
                    $suiteFeaturedPlan_name = $building_code . $pq . $suiteFeaturedPlan_name;
                    $suiteFeaturedPlan_name = str_replace(' ', '_', $suiteFeaturedPlan_name);
                    $suiteFeaturedPlan_name = str_replace('(', '', $suiteFeaturedPlan_name);
                    $suiteFeaturedPlan_name = str_replace(')', '', $suiteFeaturedPlan_name);
                    $suiteFeaturedPlan_name = str_replace('*', '', $suiteFeaturedPlan_name);
                    $suiteFeaturedPlan_name = str_replace('\\', '', $suiteFeaturedPlan_name);
                    $suiteFeaturedPlan_name = str_replace('/', '', $suiteFeaturedPlan_name);

                    $suiteFeaturedPlan_description = "{$suitePlan['description']}";
                    $suiteFeaturedPlan_description = str_replace(' ', '_', $suiteFeaturedPlan_description);
                    $suiteFeaturedPlan_description = str_replace('(', '', $suiteFeaturedPlan_description);
                    $suiteFeaturedPlan_description = str_replace(')', '', $suiteFeaturedPlan_description);
                    $suiteFeaturedPlan_description = str_replace('*', '', $suiteFeaturedPlan_description);
                    $suiteFeaturedPlan_description = str_replace('\\', '', $suiteFeaturedPlan_description);
                    $suiteFeaturedPlan_description = str_replace('/', '', $suiteFeaturedPlan_description);

                    $suiteFeaturedPlan_ext = "{$suitePlan['extension']}";

                    if (strpos($suiteFeaturedPlan_name, $suiteFeaturedPlan_ext) != FALSE) {
                        // add file description to name
                        $suiteFeaturedPlan_name = str_replace('.' . $suiteFeaturedPlan_ext, '_' . $suiteFeaturedPlan_description . '.' . $suiteFeaturedPlan_ext, $suiteFeaturedPlan_name);
                    } else {
                        // add file description and '.plan_ext' to name
                        $suiteFeaturedPlan_name = $suiteFeaturedPlan_name . '_' . $suiteFeaturedPlan_description . '.' . $suiteFeaturedPlan_ext;
                    }

//echo "Plan Name: " . $suiteFeaturedPlan_name . "\n\r\n\r";

                    $suiteFeaturedPlan_uri = "{$suitePlan->uri}";
                    //$suiteFeaturedPlan_thumb = str_replace('.pdf','.'.$pq.'.jpg',$suiteFeaturedPlan_name);
                    $suiteFeaturedPlan_thumb = str_replace('.pdf', '.jpg', $suiteFeaturedPlan_name);
                    $file_last_modified = "{$suitePlan['lastModifiedTime']}";

                    $document_check_query_string = "Select * from files_2d where building_code = '" . $building_code . "' and suite_id = '" . $suite_id . "' and suite_featured_plan = '1' and lang = '" . $lang_id . "'";

                    $document_check_query = mysql_query($document_check_query_string) or die("document check error: " . mysql_error());
                    $document_check = mysql_fetch_array($document_check_query);
                    $document_check_count = mysql_num_rows($document_check_query);
                    $newdocument_modified_time = '';
                    $existingdocument_modified_time = '';
                    $newdocument_modified_time = strtotime($file_last_modified);
                    $existingdocument_modified_time = strtotime($document_check['lastModifiedTime']);


                    if ($document_check_count > 0) {
                        // document is already present, update record //
//            echo "existing doc\n\r\n\r";
                        $suitePlan_query_string = "update files_2d set suite_id='" . addslashes($suite_id) . "', suite_name='" . addslashes($suite_name) . "', suite_floor='" . $suite_floor . "', building_id='" . $building_id . "', building_code='" . $building_code . "', plan_name='" . addslashes($suiteFeaturedPlan_name) . "', display_filename='" . addslashes($display_filename) . "', plan_description='" . addslashes($suiteFeaturedPlan_description) . "', plan_uri='" . addslashes($suiteFeaturedPlan_uri) . "', plan_thumb='" . addslashes($suiteFeaturedPlan_thumb) . "', lastModifiedTime='" . $file_last_modified . "', lang='" . $lang_id . "', dirty='0' where building_code = '" . $building_code . "' and suite_featured_plan = '1' and lang = '" . $lang_id . "'";
//            echo $suitePlan_query_string . "\n\r\n\r";
                        $suitePlan_query = mysql_query($suitePlan_query_string) or die("Update Suite Files2d Error: " . mysql_error());
                    } else {
                        // new document
//            echo "new doc\n\r\n\r";
                        $suitePlan_query_string = "insert into files_2d (suite_id, suite_name, suite_floor, building_id, building_code, plan_name, display_filename, plan_description, plan_uri, plan_thumb, lastModifiedTime, lang, dirty, suite_featured_plan) values ('" . addslashes($suite_id) . "', '" . addslashes($suite_name) . "', '" . $suite_floor . "', '" . $building_id . "', '" . $building_code . "', '" . addslashes($suiteFeaturedPlan_name) . "', '" . addslashes($display_filename) . "', '" . addslashes($suiteFeaturedPlan_description) . "', '" . addslashes($suiteFeaturedPlan_uri) . "', '" . addslashes($suiteFeaturedPlan_thumb) . "', '" . $file_last_modified . "', '" . $lang_id . "', '0', '1')";
//            echo $suitePlan_query_string . "\n\r\n\r";
                        $suitePlan_query = mysql_query($suitePlan_query_string) or die("Insert Suite Files2d Error: " . mysql_error());
                    }
//echo "twodee query string: " . $suitePlan_query_string . "\n\r\n\r";
//echo "twodeestuff\n\r\n\r";
                    // if file is newer than existing file
                    if ($newdocument_modified_time > $existingdocument_modified_time) {
// delete existing file and thumbnail
//echo "updatedfile\n\r\n\r";
                        if (is_file($base_file_path . "images/twodee_files/" . $suiteFeaturedPlan_name)) {
                            unlink($base_file_path . "images/twodee_files/" . $suiteFeaturedPlan_name); // delete file
                        }
                        if (is_file($base_file_path . "images/twodee_files/" . $suiteFeaturedPlan_thumb)) {
                            unlink($base_file_path . "images/twodee_files/" . $suiteFeaturedPlan_thumb); // delete thumb
                        }

// download new file and make new thumbnail
//echo "newdownload\n\r\n\r";
                        if (file_put_contents($base_file_path . 'images/twodee_files/' . $suiteFeaturedPlan_name, file_get_contents($suiteFeaturedPlan_uri))) {
//echo "DL: suite 2d downloaded\n\r\n\r";
                            try {
                                exec('/usr/local/bin/gs -sDEVICE=jpeg -r300 -o ' . $base_file_path . 'images/twodee_files/' . $suiteFeaturedPlan_thumb . ' -dFirstPage=1 -dLastPage=1 ' . $base_file_path . 'images/twodee_files/' . $suiteFeaturedPlan_name . '');

                                exec("/usr/local/bin/convert " . $base_file_path . "images/twodee_files/" . $suiteFeaturedPlan_thumb . " -thumbnail '180x' " . $base_file_path . "images/twodee_files/thumb_" . $suiteFeaturedPlan_thumb . "");
                                
                                exec("/usr/local/bin/convert " . $base_file_path . "images/twodee_files/" . $suiteFeaturedPlan_thumb . " -thumbnail 'x240' " . $base_file_path . "images/twodee_files/thumbc_" . $suiteFeaturedPlan_thumb . "");
                            } catch (Exception $e) {
                                echo "\nCannot create PDF thumbnail for " . $suiteFeaturedPlan_name . "\n";
                                echo 'Caught exception: ', $e->getMessage(), "\n";
                            }
                        }
                    }
                    $pq++;
                }// end foreach 2d



































                // SUITE DOCUMENTS

                foreach ($Suite->documents->file as $document) {
//		echo "in suite documents\n\r\n\r";

                    $document_name = '';
                    $document_name = "{$document['name']}";
                    $document_name = str_replace(' ', '_', $document_name);
                    $document_name = str_replace('(', '', $document_name);
                    $document_name = str_replace(')', '', $document_name);
                    $document_name = str_replace('*', '', $document_name);
                    $document_name = str_replace('\\', '', $document_name);
                    $document_name = str_replace('/', '', $document_name);

                    $document_description = "{$document['description']}";
                    //$document_description = "{$suitevid['description']}";
                    $document_description = str_replace(' ', '_', $document_description);
                    $document_description = str_replace('(', '', $document_description);
                    $document_description = str_replace(')', '', $document_description);
                    $document_description = str_replace('*', '', $document_description);
                    $document_description = str_replace('\\', '', $document_description);
                    $document_description = str_replace('/', '', $document_description);


                    $document_uri = "{$document->uri}";
                    $document_extension = "{$document['extension']}";

//					$file_ext = "{$suitevid['extension']}";

                    if (strpos($document_name, $document_extension) != FALSE) {
                        // add file description to name
                        $document_name = str_replace('.' . $document_extension, '_' . $document_description . '.' . $document_extension, $document_name);
                    } else {
                        // add file description and '.document_extension' to name
                        $document_name = $document_name . '_' . $document_description . '.' . $document_extension;
                    }
//		echo "document name: " . $document_name . "\n\r\n\r";

                    $file_last_modified = "{$document['lastModifiedTime']}";

                    $document_check_query_string = "Select * from suite_documents where building_code = '" . $building_code . "' and file_name = '" . $document_name . "' and lang = '" . $lang_id . "'";
                    $document_check_query = mysql_query($document_check_query_string) or die("document check error: " . mysql_error());
                    $document_check = mysql_fetch_array($document_check_query);
                    $document_check_count = mysql_num_rows($document_check_query);
                    $newdocument_modified_time = '';
                    $existingdocument_modified_time = '';
                    $newdocument_modified_time = strtotime($file_last_modified);
                    $existingdocument_modified_time = strtotime($document_check['lastModifiedTime']);

//echo "doc check query string: " . $document_check_query_string . "\n\r\n\r";
//echo "document check count: " . $document_check_count . "\n\r\n\r";


                    if ($document_check_count > 0) {
                        // document is already present, update record //
//echo "existing document\n\r\n\r";

                        $suitedocument_query_string = "update suite_documents set lang='" . $lang_id . "', building_id='" . $building_id . "',  building_code='" . $building_code . "', suite_id='" . addslashes($suite_id) . "', suite_name='" . addslashes($suite_name) . "', file_name='" . addslashes($document_name) . "', description='" . addslashes($document_description) . "', uri='" . addslashes($document_uri) . "', thumb='', lastModifiedTime='" . $file_last_modified . "', extension='" . addslashes($document_extension) . "', dirty='0' where building_code = '" . $building_code . "' and file_name = '" . $document_name . "' and lang = '" . $lang_id . "'";

//echo "suite doc query string: " . $suitedocument_query_string . "\n\r\n\r";

                        $suitedocument_query = mysql_query($suitedocument_query_string) or die("Update Suite Document Error: " . mysql_error());
                    } else {
                        // new document
//echo "NEW document\n\r\n\r";

                        $suitedocument_query_string = "insert into suite_documents (lang, building_id, building_code, suite_id, suite_name, file_name, description, uri, thumb, lastModifiedTime, extension, dirty) values ('" . $lang_id . "', '" . $building_id . "', '" . $building_code . "', '" . addslashes($suite_id) . "', '" . addslashes($suite_name) . "', '" . addslashes($document_name) . "', '" . addslashes($document_description) . "', '" . addslashes($document_uri) . "', '', '" . $file_last_modified . "', '" . addslashes($document_extension) . "', '0')";


//echo "suite doc query string: " . $suitedocument_query_string . "\n\r\n\r";


                        $suitedocument_query = mysql_query($suitedocument_query_string) or die("Insert Suite Document Error: " . mysql_error());
                    }






                    // if file is newer than existing
                    if ($newdocument_modified_time > $existingdocument_modified_time) {
                        // delete existing
                        if (is_file($base_file_path . "images/suite_files/" . $document_name)) {
                            unlink($base_file_path . "images/suite_files/" . $document_name); // delete file
                        }

                        if (file_put_contents($base_file_path . 'images/suite_files/' . $document_name, file_get_contents($document_uri))) {
//echo "\nDL: ". $document_name ." has been downloaded successfully.\n";
                        }
                    }
                }// end foreach Suite->documents
// SUITES SPECIFICATIONS
                $del_suite_specs = mysql_query("DELETE FROM suites_specifications WHERE suite_id ='" . $suite_id . "' and lang=" . $lang_id) or die("Delete suite specs error" . mysql_error());

//                if ($suite_type == "INDUSTRIAL" || $suite_type == "INDUSTRIEL") {




                    //$building_costs_add_rent_total = "{$Building->specifications->tenantCosts['additional-rent']}";
                    $suite_spec_office_space = "{$Suite['office-space']}";
                    $suite_spec_warehouse_space = "{$Suite['warehouse-space']}";

                    $suite_spec_clear_height = "{$Suite['clear-height']}";
                    $suite_spec_clear_height_sort = substr($suite_spec_clear_height, 0, strpos($suite_spec_clear_height, '\''));

                    $suite_spec_available_electrical_volts = "{$Suite['available-electrical-volts']}";
                    $suite_spec_available_electrical_amps = "{$Suite['available-electrical-amps']}";
                    $suite_spec_shipping_doors_drive_in = "{$Suite['shipping-doors-drive-in']}";
                    $suite_spec_shipping_doors_truck = "{$Suite['shipping-doors-truck']}";
                    $suite_spec_bay_size_x = "{$Suite['bay-size-x']}";
                    $suite_spec_bay_size_y = "{$Suite['bay-size-y']}";
                    $suite_spec_surface_stalls = "{$Suite['surface-stalls']}";
                    $suite_spec_truck_trailer_parking = "{$Suite['truck-trailer-parking']}";

                    $suite_spec_surface_stalls_ratio = "{$Suite['surface-stalls-ratio']}";
                    $suite_spec_surface_stalls_ratio_sort = str_replace('/1000', '', $suite_spec_surface_stalls_ratio);

                    $suite_spec_clear_height_notes = "{$Suite->clearHeightNotes}";
                    $suite_spec_heating_hvac_notes = "{$Suite->heatingHvacNotes}";
                    $suite_spec_lighting_notes = "{$Suite->lightingNotes}";
                    $suite_spec_electrical_notes = "{$Suite->electricalNotes}";
                    $suite_spec_sprinkler_system_notes = "{$Suite->sprinklerSystemNotes}";
                    $suite_spec_shipping_door_notes = "{$Suite->shippingDoorNotes}";
                    $suite_spec_bay_size_notes = "{$Suite->bayNotes}";
                    $suite_spec_slab_notes = "{$Suite->slabNotes}";
                    $suite_spec_parking_notes = "{$Suite->parkingNotes}";



                    // INSERT SUITE SPECS

                    $insert_suite_specs_query_string = "replace into suites_specifications (lang, building_id, suite_id, suite_type, office_space, warehouse_space, clear_height, clear_height_sort, clear_height_notes, shipping_doors_drive_in, shipping_doors_truck, shipping_doors_notes, bay_size_x, bay_size_y, bay_size_notes, slab_notes, sprinkler_notes, available_electrical_volts, available_electrical_amps, available_electrical_notes, heating_hvac_notes, lighting_notes, surface_stalls_per_unit, surface_stalls_per_unit_sort, surface_stalls, truck_trailer_parking, parking_notes) values (" . $lang_id . ",'" . $building_id . "', '" . $suite_id . "', '" . addslashes($suite_type) . "', '" . addslashes($suite_spec_office_space) . "', '" . addslashes($suite_spec_warehouse_space) . "', '" . addslashes($suite_spec_clear_height) . "', '" . addslashes($suite_spec_clear_height_sort) . "', '" . addslashes($suite_spec_clear_height_notes) . "', '" . addslashes($suite_spec_shipping_doors_drive_in) . "', '" . addslashes($suite_spec_shipping_doors_truck) . "', '" . addslashes($suite_spec_shipping_door_notes) . "', '" . addslashes($suite_spec_bay_size_x) . "', '" . addslashes($suite_spec_bay_size_y) . "', '" . addslashes($suite_spec_bay_size_notes) . "', '" . addslashes($suite_spec_slab_notes) . "', '" . addslashes($suite_spec_sprinkler_system_notes) . "', '" . addslashes($suite_spec_available_electrical_volts) . "', '" . addslashes($suite_spec_available_electrical_amps) . "', '" . addslashes($suite_spec_electrical_notes) . "', '" . addslashes($suite_spec_heating_hvac_notes) . "', '" . addslashes($suite_spec_lighting_notes) . "', '" . addslashes($suite_spec_surface_stalls_ratio) . "', '" . addslashes($suite_spec_surface_stalls_ratio_sort) . "', '" . addslashes($suite_spec_surface_stalls) . "', '" . addslashes($suite_spec_truck_trailer_parking) . "', '" . addslashes($suite_spec_parking_notes) . "')";

                    $insert_suite_specs_query = mysql_query($insert_suite_specs_query_string) or die("Suite Specs Insert Error: " . mysql_error());
//                } // end if suite_type == Industrial
// END SUITE SPECIFICATIONS
                //	echo "\n suite is public, insert to db.\n";
                // INSERT SUITE RECORD	
                $suite_query_string = "replace into suites (lang, building_code, building_id, floor_name, floor_number, suite_id, suite_name, suite_type, net_rentable_area, contiguous_area, min_divisible_area, taxes_and_operating, availability, notice_period, description, net_rent, add_rent_total, model, new, leased, promoted, marketing_text, record_date, record_source, dirty, bu_flag) values (" . $lang_id . ",'" . $building_code . "', '" . $building_id . "', '" . addslashes($suite_floor_name) . "', '" . addslashes($suite_floor) . "', '" . addslashes($suite_id) . "', '" . addslashes($suite_name) . "', '" . addslashes($suite_type) . "', '" . addslashes($suite_net_rentable_area) . "', '" . $suite_contiguous_area . "', '" . $suite_minimum_divisible . "', '" . $suite_taxes_and_operating . "', '" . $suite_availability . "', '" . $suite_notice_period . "', '" . addslashes($suite_description) . "', '" . $suite_net_rent . "', '" . $suite_add_rent_total . "', '" . $suite_model . "', '" . $suite_new . "', '" . $suite_leased . "', '" . $suite_promoted . "', '" . $building_marketing_text . "', '" . time() . "', 'today', 0, '" . $bu_flag . "')";

                $suite_query = mysql_query($suite_query_string) or die("Suite query error: " . mysql_error());
            } // end if public == true
        } // end foreach Suite
//echo "building code: " . $building_code . "\n\r\n\r";
//echo "suitecount: " . $suitecount . "\n\r\n\r";
//echo "building type: " . $building_type . "\n\r\n\r";

        $is_retail = 0;
        if ($building_type == "Retail") {
            $is_retail = 1;
        }
        if ($building_type == "Détail") {
            $is_retail = 1;
        }
//echo "is retail: " . $is_retail . "\n\r\n\r";	
        // seif ($building_type != "Retail") {t no_vacancy flag if there are no public suites for this building.
        if ($is_retail == 0) {
            if ($suitecount == 0) {
                // set no vacancies flag on current building
                $no_vacancies_query_string = "update buildings set no_vacancies = 1 where building_code = '" . $building_code . "'";
                $no_vacancies_query = mysql_query($no_vacancies_query_string) or die("no_vacancies_query error" . mysql_query());
            }
        }
    } // end if filtered
}//end for each BUILDING
//echo "DIRTY\n\r\n\r";
// DELETE THE DIRTY BUILDINGS AND THEIR DATA!!!!
//echo "getting dirties for: " . $clientname . "\n\r\n\r";
$get_dirties_q_query = "select building_code from buildings where dirty=1 and lang=" . $lang_id or die("get dirties error: " . mysql_error());
$get_dirties_q = mysql_query($get_dirties_q_query);

//echo "dirties q query: " . $get_dirties_q_query . "\n\r\n\r";
//echo "getting cleans for: " . $clientname . "\n\r\n\r";
$get_cleans_q = mysql_query("select building_code from buildings where dirty=0 and lang=" . $lang_id) or die("get cleans error for : " . $clientname . " --: " . mysql_error());
$get_totals_q = mysql_query("select building_code from buildings where lang=" . $lang_id) or die("get totals error: " . mysql_error());



$num_dirties = mysql_num_rows($get_dirties_q);
$num_cleans = mysql_num_rows($get_cleans_q);
$num_totals = mysql_num_rows($get_totals_q);

//echo "num dirties: " . $num_dirties . "\n\r";
//echo "num cleans: " . $num_cleans . "\n\r";
//echo "num totals: " . $num_totals . "\n\r";


if ($num_dirties == 0) {

//echo "num_dirties is empty \n\r\n\r";
} else {

//echo "Number of dirties: " . $num_dirties . "\n\r";
//echo "Number of cleans: " . $num_cleans . "\n\r";


    if ($num_dirties > 50) {
        if (($errorcode != 1) && ($errorcode != 2) && ($errorcode != 33) && ($errorcode != 666)) {
            $errorcode = 3;
        }

        if ($errorcode != 0) {
            echo "Errorcode after third: " . $errorcode . "\n\r\n\r";
        }
    } else {



        if ($num_cleans > 0 || $num_totals == 0) {

//echo "delete the dirties\n\r\n\r";

            while ($get_dirties_r = mysql_fetch_array($get_dirties_q)) {

                //echo "in dirty loop\n\r\n\r";

                $del_building = mysql_query("DELETE FROM buildings WHERE building_code ='" . $get_dirties_r['building_code'] . "' and lang=" . $lang_id);
                $del_sections = mysql_query("DELETE FROM sections WHERE building_code ='" . $get_dirties_r['building_code'] . "' and lang=" . $lang_id);
                $del_contacts = mysql_query("DELETE FROM contacts WHERE building_code ='" . $get_dirties_r['building_code'] . "' and lang=" . $lang_id);
                $del_buildingspecs_dirty = mysql_query("DELETE FROM building_specifications WHERE lang=" . $lang_id . " and building_code='" . $get_dirties_r['building_code'] . "'");
                $del_suites = mysql_query("DELETE FROM suites WHERE building_code ='" . $get_dirties_r['building_code'] . "' and lang=" . $lang_id);


//  2015-08-17
//  document dirty elements below must be replaced by a full document clearing
//  as preview images and suite preview images (further below) are doing
//  2d and documents have been roughed in.
//		$del_preview_images = mysql_query("DELETE FROM preview_images WHERE building_code ='" . $get_dirties_r['building_code'] . "' and lang=" . $lang_id);
//		$del_suitefiles2dirty = mysql_query("DELETE FROM suite_preview_images WHERE lang=" . $lang_id . " and building_code='" . $get_dirties_r['building_code'] . "'");
                $del_documents = mysql_query("DELETE FROM documents WHERE building_code ='" . $get_dirties_r['building_code'] . "' and lang=" . $lang_id);
                $del_3ddirty = mysql_query("DELETE FROM files_3d WHERE lang=" . $lang_id . " and building_code='" . $get_dirties_r['building_code'] . "'");
//                    $del_2ddirty_query_string = "DELETE FROM files_2d WHERE lang=" . $lang_id . " and building_code='" . $get_dirties_r['building_code'] . "'";
//                    $del_2ddirty = mysql_query($del_2ddirty_query_string);
                $del_suitefiles1dirty = mysql_query("DELETE FROM suite_documents WHERE lang=" . $lang_id . " and building_code='" . $get_dirties_r['building_code'] . "'");
            }
        } else {
            $errorcode = 33;
            if ($errorcode != 0) {
                echo "Errorcode after Dirties: " . $errorcode . "\n\r\n\r";
            }
        }
    }
}




// any leftover dirty documents and files - ****outside of dirty buildings****?
// check to ensure that there are no problems up until this point first before deleting
//echo "Final errorcode check: " . $errorcode . "\n\r\n\r";

if ($errorcode == 0) {

// FILE CLEANUPS
// 
// Building Preview Image Cleanup
// add dirty files for any that slipped through missing their building getting deleted. 
// apparently this happens.
    $building_preview_dirtydoublecheck_querystring = "UPDATE preview_images SET dirty=1 WHERE building_id NOT IN (SELECT building_id FROM buildings)";
    $building_preview_dirtydoublecheck_query = mysql_query($building_preview_dirtydoublecheck_querystring) or die("building documents dirty doublecheck query error: " . mysql_error());
//    echo "new dirties found: " . mysql_affected_rows() . "\n\r\n\r";
// get names of dirty files
    $building_previewimage_dirtycheck_query_string = "select file_name from preview_images where dirty = 1";
    $building_previewimage_dirtycheck_query = mysql_query($building_previewimage_dirtycheck_query_string) or die("building previewimage dirtycheck error: " . mysql_error());

    $building_previewcheck_dirtyimagenames = array();

    while ($building_previewimage_dirtycheck = mysql_fetch_array($building_previewimage_dirtycheck_query)) {
        // Array of dirty imagenames
        array_push($building_previewcheck_dirtyimagenames, $building_previewimage_dirtycheck['file_name']);
    }
    // and delete/unlink them
    foreach ($building_previewcheck_dirtyimagenames as $bpi_deleteme) {
        if (is_file($base_file_path . "images/preview_images/" . $bpi_deleteme)) {
            unlink($base_file_path . "images/preview_images/" . $bpi_deleteme); // delete file
//echo "DELETEME: images/preview_images/".$bpi_deleteme."\n\r";
        }
    }
    // THEN remove them from the db. So dirty.
    $del_building_preview_images_cleanup_querystring = "DELETE FROM preview_images WHERE dirty = 1";
//echo "delbuilding preview image: " . $del_building_preview_images_cleanup_querystring . "\n\r\n\r";
    $del_building_preview_images_cleanup = mysql_query($del_building_preview_images_cleanup_querystring) or die("del_building_preview_images_cleanup error: " . mysql_error());
// end Building Preview Image Cleanup
// Suite Preview Image Cleanup
// add dirty files for any that slipped through missing their building getting deleted. 
// apparently this happens.
    $suite_preview_dirtydoublecheck_querystring = "UPDATE suite_preview_images SET dirty=1 WHERE building_id NOT IN (SELECT building_id FROM buildings)";
    $suite_preview_dirtydoublecheck_query = mysql_query($suite_preview_dirtydoublecheck_querystring) or die("building documents dirty doublecheck query error: " . mysql_error());
//echo "new dirties found: " . mysql_affected_rows() . "\n\r\n\r";
// get names of dirty files
    $suite_previewimage_dirtycheck_query_string = "select file_name from suite_preview_images where dirty = '1'";
    $suite_previewimage_dirtycheck_query = mysql_query($suite_previewimage_dirtycheck_query_string) or die("building previewimage dirtycheck error: " . mysql_error());

    $suite_previewcheck_dirtyimagenames = array();

    while ($suite_previewimage_dirtycheck = mysql_fetch_array($suite_previewimage_dirtycheck_query)) {
        // Array of dirty imagenames
        array_push($suite_previewcheck_dirtyimagenames, $suite_previewimage_dirtycheck['file_name']);
    }
    // and delete/unlink them
    foreach ($suite_previewcheck_dirtyimagenames as $bpi_deleteme) {
        if (is_file($base_file_path . "images/suite_preview_images/" . $bpi_deleteme)) {
            unlink($base_file_path . "images/suite_preview_images/" . $bpi_deleteme); // delete file
//echo "DELETEME: images/preview_images/".$bpi_deleteme."\n\r";
        }
    }
    // THEN remove them from the db. So dirty.
    $del_suite_preview_images_cleanup_querystring = "DELETE FROM suite_preview_images WHERE dirty = '1'";
//echo "delsuitepreview querystring: " . $del_suite_preview_images_cleanup_querystring . "\n\r\n\r";
    $del_suite_preview_images_cleanup = mysql_query($del_suite_preview_images_cleanup_querystring) or die("del_suite_preview_images_cleanup error: " . mysql_error());
// end Suite Preview Image Cleanup   
// Building Documents Cleanup
// add dirty files for any that slipped through missing their building getting deleted. 
// apparently this happens.
    $building_documents_dirtydoublecheck_querystring = "UPDATE documents SET dirty=1 WHERE building_id NOT IN (SELECT building_id FROM buildings)";
    $building_documents_dirtydoublecheck_query = mysql_query($building_documents_dirtydoublecheck_querystring) or die("building documents dirty doublecheck query error: " . mysql_error());
//echo "new dirties found: " . mysql_affected_rows() . "\n\r\n\r";
// get names of dirty files
    $building_documents_dirtycheck_query_string = "select file_name from documents where dirty = 1";
//echo "BDDQS: " . $building_documents_dirtycheck_query_string . "\n\r";
    $building_documents_dirtycheck_query = mysql_query($building_documents_dirtycheck_query_string) or die("building document dirtycheck error: " . mysql_error());
    $building_documents_dirtyfilenames = array();
    while ($building_documents_dirtycheck = mysql_fetch_array($building_documents_dirtycheck_query)) {
        // Array of dirty imagenames
        array_push($building_documents_dirtyfilenames, $building_documents_dirtycheck['file_name']);
    }
//var_dump($building_documents_dirtyfilenames);
    // and delete/unlink them
    foreach ($building_documents_dirtyfilenames as $bdoc_deleteme) {
        if (is_file($base_file_path . "images/documents/" . $bdoc_deleteme)) {
            unlink($base_file_path . "images/documents/" . $bdoc_deleteme); // delete file
//echo "DELETEME: images/documents/".$bdoc_deleteme."\n\r";
        }
    }
    // THEN remove them from the db. So dirty.
    $del_building_documents_cleanup_querystring = "DELETE FROM documents WHERE dirty='1'";
    $del_building_documents_cleanup = mysql_query($del_building_documents_cleanup_querystring) or die("del_building_documents_cleanup error: " . mysql_error());
// end Building Documents Cleanup
//// Suite Documents Cleanup
//// get names of dirty files
//    $suite_documents_dirtycheck_query_string = "select file_name from suite_documents where dirty = 1";
//    $suite_documents_dirtycheck_query = mysql_query($suite_documents_dirtycheck_query_string) or die ("suite document dirtycheck error: " . mysql_error());
//    $suite_documents_dirtyfilenames = array();
//    while ($suite_documents_dirtycheck = mysql_fetch_array($suite_documents_dirtycheck_query)) {
//        // Array of dirty imagenames
//        array_push($suite_documents_dirtyfilenames, $suite_documents_dirtycheck['file_name']);
//    }
//    // and delete/unlink them
//    foreach ($suite_documents_dirtyfilenames as $sdoc_deleteme) {
//        if(is_file($base_file_path."images/documents/".$sdoc_deleteme)){
//            unlink($base_file_path."images/documents/".$sdoc_deleteme); // delete file
////echo "DELETEME: images/documents/".$bdoc_deleteme."\n\r";
//        }
//    }
//    // THEN remove them from the db. So dirty.
//    $del_suite_documents_cleanup_querystring = "DELETE FROM suite_documents WHERE dirty='1'";
//    $del_suite_documents_cleanup = mysql_query($del_suite_documents_cleanup_querystring) or die ("del_suite_documents_cleanup error: " . mysql_error());
//// end Suite Documents Cleanup
// 2D Files Cleanup
// add dirty files for any that slipped through missing their building getting deleted. 
// apparently this happens.
    $files2d_dirtydoublecheck_querystring = "UPDATE files_2d SET dirty=1 WHERE building_id NOT IN (SELECT building_id FROM buildings)";
    $files2d_dirtydoublecheck_query = mysql_query($files2d_dirtydoublecheck_querystring) or die("2d dirty doublecheck query error: " . mysql_error());
//    echo "new dirties found: " . mysql_affected_rows() . "\n\r\n\r";
// 
// get names of dirty files
    $files2d_dirtycheck_query_string = "select plan_name from files_2d where dirty = 1";
    $files2d_dirtycheck_query = mysql_query($files2d_dirtycheck_query_string) or die("building 2d dirtycheck error: " . mysql_error());
    $files2d_dirtyimagenames = array();
    while ($files2d_dirtycheck = mysql_fetch_array($files2d_dirtycheck_query)) {
        // Array of dirty imagenames
        array_push($files2d_dirtyimagenames, $files2d_dirtycheck['plan_name']);
    }
    // and delete/unlink them
    foreach ($files2d_dirtyimagenames as $bpi_deleteme) {
        if (is_file($base_file_path . "images/twodee_files/" . $bpi_deleteme)) {
            unlink($base_file_path . "images/twodee_files/" . $bpi_deleteme); // delete file
//echo "DELETEME: images/twodee_files/".$bpi_deleteme."\n\r";
        }
    }
    // THEN remove them from the db. So dirty.
    $del_files2d_cleanup_querystring = "DELETE FROM files_2d WHERE dirty='1'";
    $del_files2d_cleanup = mysql_query($del_files2d_cleanup_querystring) or die("del_files3d cleanup error: " . mysql_error());
// end 2D Files Cleanup     
// 3D Files Cleanup
// add dirty files for any that slipped through missing their building getting deleted. 
// apparently this happens.
    $files3d_dirtydoublecheck_querystring = "UPDATE files_3d SET dirty=1 WHERE building_id NOT IN (SELECT building_id FROM buildings)";
    $files3d_dirtydoublecheck_query = mysql_query($files3d_dirtydoublecheck_querystring) or die("3d dirty doublecheck query error: " . mysql_error());
//    echo "new dirties found: " . mysql_affected_rows() . "\n\r\n\r";
// get names of dirty files
    $files3d_dirtycheck_query_string = "select presentation_thumb, panorama_filename from files_3d where dirty = 1";
    $files3d_dirtycheck_query = mysql_query($files3d_dirtycheck_query_string) or die("building 3d dirtycheck error: " . mysql_error());
    $files3d_dirtyimagenames = array();
    while ($files3d_dirtycheck = mysql_fetch_array($files3d_dirtycheck_query)) {
        // Array of dirty imagenames
        array_push($files3d_dirtyimagenames, $files3d_dirtycheck['presentation_thumb']);
        if ($files3d_dirtycheck['panorama_filename'] != '') {
            array_push($files3d_dirtyimagenames, $files3d_dirtycheck['panorama_filename']);
        }
    }
    // and delete/unlink them
    foreach ($files3d_dirtyimagenames as $bpi_deleteme) {
        if (is_file($base_file_path . "images/threedee_files/" . $bpi_deleteme)) {
            unlink($base_file_path . "images/threedee_files/" . $bpi_deleteme); // delete file
//echo "DELETEME: images/twodee_files/".$bpi_deleteme."\n\r";
        }
    }
    // THEN remove them from the db. So dirty.
    $del_files3d_cleanup_querystring = "DELETE FROM files_3d WHERE dirty='1'";
    $del_files3d_cleanup = mysql_query($del_files3d_cleanup_querystring) or die("del_files3d cleanup error: " . mysql_error());
// end 3D Files Cleanup     
//  END FILE CLEANUPS
}








// Temp Account Sanity Check
//echo "Building counter end: " . $building_id_num . "\n\r\n\r";

$totalbuildings = $building_id_num;
$totaltempaccounts = $new_tempacct_created;
$min = $totalbuildings - ($totalbuildings * (20 / 100));
$max = $totalbuildings + ($totalbuildings * (20 / 100));

//echo "total temp accounts created: " . $totaltempaccounts . "\n\r\n\r";
//echo "total buildings: " . $totalbuildings . "\n\r\n\r";


if ($cron['source_account'] == '0') {
    $emailmessage = "";
    if (($min <= $totaltempaccounts) && ($totaltempaccounts <= $max)) {
        if (($errorcode != 1) && ($errorcode != 2) && ($errorcode != 3) && ($errorcode != 33) && ($errorcode != 666)) {
            $errorcode = 4;
        }
    }

    if ($errorcode != 0) {
        echo "Errorcode after fourth: " . $errorcode . "\n\r\n\r";
    }
}

// errorcode 1 = XML feed not present
// errorcode 2 = Arcestra.com unavailable
// errorcode 3 = > 50 dirty
// errorcode 4 = temp accounts problem
$emaildomain = str_replace('http://', '', $cron['vacancy_report_website']);
$to_email = "support@rationalroot.com";
$from_email = ' root@';
$from_email .= $emaildomain;

if ($errorcode > 0) {

    switch ($errorcode) {
        case 1:
            $emailsubject = $cron['vacancy_report_website'] . " XML Authentication Problem";
            $emailmessage .= '<p>There was an Authentication problem accessing the Arcestra XML Feed</p>' . $eol;
            break;
        case 2:
            $emailsubject = $cron['vacancy_report_website'] . " Arcestra.com unavailable";
            $emailmessage .= '<p>The Arcestra website is currently unavailable</p>' . $eol;
            break;
        case 3:
            $emailsubject = $cron['vacancy_report_website'] . " More than 50 Dirty Accounts";
            $emailmessage .= '<p>The cron file has registered more than 50 dirty accounts for ' . $cron['vacancy_report_website'] . '. Please investigate.</p>' . $eol;
            break;
        case 33:
            $emailsubject = $cron['vacancy_report_website'] . " NO Clean Buildings";
            $emailmessage .= '<p>The cron file has registered NO Clean Buildings for ' . $cron['vacancy_report_website'] . '. Please investigate.</p>' . $eol;
            $emailmessage .= '<p>The cron for ' . $cron['vacancy_report_website'] . '. has been aborted.</p>' . $eol;
            $emailmessage .= '<p>This usually means that the arcestra API is not available.</p>' . $eol;
            break;

        case 666:
            $emailsubject = $cron['vacancy_report_website'] . " API Scrape Failed!";
            $emailmessage .= '<p>The cron file is unable to gather XML data for ' . $cron['vacancy_report_website'] . '. Please investigate.</p>' . $eol;
            $emailmessage .= '<p>The cron for ' . $cron['vacancy_report_website'] . '. has been aborted.</p>' . $eol;
            $emailmessage .= '<p>This usually means that the arcestra API is not available.</p>' . $eol;
            break;

        case 4:
            $emailsubject = $cron['vacancy_report_website'] . " Temp Account Problem";
            $emailmessage .= '<p>Total Buildings: ' . $totalbuildings . '</p>' . $eol;
            $emailmessage .= '<p>New Temp Accounts just created: ' . $totaltempaccounts . '</p>' . $eol;
            $emailmessage .= '<p>Email triggered because this is' . (($totaltempaccounts / $totalbuildings) * 100) . '% of the total buildings. </p>' . $eol;
            $emailmessage .= '<p>Email threshhold is +/- 20%</p>' . $eol;
            break;
    }

    $subject = $emailsubject;

    $message = '';
    $message .= '<html><head>' . $eol;
    $message .= '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />' . $eol;
    $message .= '</head><body>' . $eol;

    $message .= $emailmessage;

    $message .= "</body></html>" . $eol;
    $headers = 'MIME-Version: 1.0' . $eol;
    $headers .= 'Content-type: text/html; charset=utf-8' . $eol;
    $headers .= 'From: ' . $from_email . $eol;
    $headers .= 'X-Mailer: PHP/' . phpversion() . $eol;
    @mail($to_email, $subject, $message, $headers);
} // end if ($errorcode > 0) {
// End Temp Account Sanity Check




echo "Success: ENG " . $clientname . "\n\r";
error_log($clientname . ": Cron Complete");
?>
