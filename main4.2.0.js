var fshare_captcha;
var CaptchaCallback = function() {
    // Renders the HTML element with id 'example1' as a reCAPTCHA widget.
    // The id of the reCAPTCHA widget is assigned to 'widgetId1'.
//    tourcaptcha = grecaptcha.render('tourcaptcha', {
//        'sitekey' : '6LfgPBoUAAAAAHYde0AUWfiCTJlKXhyUTAcon9ko'
//    });
//    aitccaptcha = grecaptcha.render('aitccaptcha', {
//        'sitekey' : '6LfgPBoUAAAAAHYde0AUWfiCTJlKXhyUTAcon9ko'
//    });
    fshare_captcha = grecaptcha.render('fshare_captcha', {
        'sitekey' : '6LcsOjkUAAAAAOMAVbfLS3o8KUDhisF1yr4MvLZj'
    });
    
//    bshare_captcha = grecaptcha.render('bshare_captcha', {
//        'sitekey' : '6LcsOjkUAAAAAOMAVbfLS3o8KUDhisF1yr4MvLZj'
//    });
};


$(document).ready(function () {
//    console.log("Document.ready starts");

//var url = '';
    var url = new URL(window.location.href);
//console.log("url");
//console.log(url);
    //console.log(isMobile);
    var isMobile;
    var viewportwidth;
    var viewportheight;
    var favCount = 0;
    var owl;

    function updateMap() {
        setTimeout(function () {
////            theMap.refresh();
//            // nudge the map to fix the markerClusterer bug
//            // https://github.com/googlemaps/v3-utility-library/issues/252
////            theMap.setCenter((mapcenterLa + 0.0000001), mapcenterLg);
//            console.log("resize triggered");
////            google.maps.event.trigger(map,'resize');
////            google.maps.event.trigger(google.maps.map , 'resize')
            var center = map.getCenter();
            google.maps.event.trigger(map, "resize");
            map.setCenter(center); 
//
        }, 500);

//google.maps.event.trigger(map, 'resize');
    }


    //carousel mobile controls
    // Go to the next item
    $('body').on('click', '.fs_carousel-next', function (e) {
        e.preventDefault();
        owl.trigger('next.owl.carousel');
    });
    // Go to the previous item
    $('body').on('click', '.fs_carousel-prev', function (e) {
        e.preventDefault();
        owl.trigger('prev.owl.carousel');
    });

   $('#fs_favourites').mouseover(function() {
        if (!isMobile) {
            $('body').addClass('fs_view-favourites');
        }
    }).mouseout(function() {
        if (!isMobile) {
            $('body').removeClass('fs_view-favourites');
        }
    });

    function updateFavourites() {
        console.log('update favourites');
        console.log(isMobile);
        console.log(favCount);

        if (isMobile) {
            if (favCount == 0) {
                $('body').removeClass('fs_favourites-active fs_view-favourites fs_view-favourites-full fs_has-favourites');
            } else {
                //remove favourites-carousel and return markup to initial state
                $("#fs_favourites-carousel").sortable("destroy");

                //intialize carousel
                owl = $('.owl-carousel');
                owl.owlCarousel({
                    loop: false,
                    margin: 10,
                    nav: false,
                    dots: false,
                    slideBy: 1,
                    responsive: {
                        0: {
                            items: 1,
                            slideBy: 1
                        },
                        640: {
                            items: 2,
                            slideBy: 1
                        }
                    }
                });
            }

        } else {
            if (favCount != 0) {
                //these 3 lines kill owl carousel, and returns the markup to the initial state
                owl.trigger('destroy.owl.carousel');
                owl.find('.owl-stage-outer').children().unwrap();
                owl.removeClass("owl-center owl-loaded owl-text-select-on");

                //intialize favourites-carousel
                $("#fs_favourites-carousel").sortable();
            }
        }
    }

    //get viewport width and height, set isMobile
    if (typeof window.innerWidth != 'undefined') {
        viewportwidth = window.innerWidth;
        viewportheight = window.innerHeight;

        if (viewportwidth > 991) {
            isMobile = false;
        } else {
            isMobile = true;
        }
    }

    var addEvent = function (object, type, callback) {
        if (object == null || typeof (object) == 'undefined')
            return;
        if (object.addEventListener) {
            object.addEventListener(type, callback, false);
        } else if (object.attachEvent) {
            object.attachEvent("on" + type, callback);
        } else {
            object["on" + type] = callback;
        }
    };

    //window resize listener
    addEvent(window, "resize", function (event) {
        if (typeof window.innerWidth != 'undefined') {
            viewportwidth = window.innerWidth,
                    viewportheight = window.innerHeight

            if (viewportwidth < 992 && isMobile == false) {
                isMobile = true;

                //switch from desktop to mobile viewport, update favourites layout/functionality

                updateFavourites();


            } else if (viewportwidth > 992 && isMobile == true) {
                isMobile = false;
                //switch from mobile to desktop viewport, update favourites layout/functionality

                updateFavourites();

            }
            //window.location.reload(false); 
        }
    });
    //document.write('<p>Your viewport width is '+viewportwidth+'x'+viewportheight+'</p>');

    $('.fs_toggle-filter').on('click', function (e) {
        e.preventDefault();
        $('body').toggleClass('fs_view-filter');
        updateMap();
    });

    $('.close-filter').on('click', function (e) {
        $('body').removeClass('fs_view-filter');
        updateMap();
    });

    $('.fs_toggle-results').on('click', function (e) {
        e.preventDefault();
        $('body').toggleClass('fs_view-map-full');
        $('.fs_toggle-map').toggleClass('d-none');

        if ($('body').hasClass('fs_view-map-full')) {
            $(this).find('.fs_nav-link-icon').text('chevron_left');
        } else {
            $(this).find('.fs_nav-link-icon').text('chevron_right');
        }

        updateMap();
    });

    $('.fs_toggle-results-mobile').on('click', function (e) {
        e.preventDefault();
        $('body').toggleClass('fs_view-map-full');
        if ($('body').hasClass('fs_view-map-full')) {
            $(this).html('<span class="fs_nav-link-text">Show Results</span> <i class="material-icons fs_nav-link-icon fs_icon-expand">expand_more</i>');
        } else {
            $(this).html('<span class="fs_nav-link-text">Hide Results</span> <i class="material-icons fs_nav-link-icon fs_icon-expand">expand_less</i>');
        }
        updateMap();
    });

    $('.fs_toggle-map').on('click', function (e) {
        e.preventDefault();

        $('body').toggleClass('fs_view-map').removeClass('fs_view-map-full');

        if ($('body').hasClass('fs_view-map')) {
            $(this).html('<i class="material-icons fs_nav-link-icon fs_icon-map">map</i> <span class="fs_nav-link-text d-none d-md-inline">Hide</span> <span class="fs_nav-link-text">Map</span>');
            $('#fs_filter, .fs_search-results').mCustomScrollbar("update");
        } else {
            $(this).html('<i class="material-icons fs_nav-link-icon fs_icon-map">map</i> <span class="fs_nav-link-text d-none d-md-inline">Show</span> <span class="fs_nav-link-text">Map</span>');
            $('.fs_toggle-results-mobile').html('<span class="fs_nav-link-text">Hide Results</span> <i class="material-icons fs_nav-link-icon fs_icon-expand">expand_less</i>');
            $('#fs_filter, .fs_search-results').mCustomScrollbar("disable");
        }



        updateMap();
    });

    $('.fs_toggle-view').on('click', function (e) {
        e.preventDefault();
        $('body').toggleClass('fs_view-grid');

        if ($('body').hasClass('fs_view-grid')) {
            $(this).html('<i class="material-icons fs_nav-link-icon fs_icon-view">view_list</i> <span class="d-none d-md-inline fs_nav-link-text">View as</span> <span class="fs_nav-link-text">List</span></a>');

        } else {

            $(this).html('<i class="material-icons fs_nav-link-icon fs_icon-view">view_module</i> <span class="d-none d-md-inline fs_nav-link-text">View as</span> <span class="fs_nav-link-text">Grid</span></a>');

        }
        updateMap();
    });

    $('.fs_filter-list-toggle').on('click', function (e) {
        e.preventDefault();

        $(this).toggleClass('collapsed');

        if ($(this).hasClass('collapsed')) {
            $(this).find('.fs_nav-link-icon').text('expand_more');
        } else {
            $(this).find('.fs_nav-link-icon').text('expand_less');
        }
    });

    $('body').on('click', '.fs_collapse-toggle', function (e) {
        e.preventDefault();
//       console.log('clicked');
        $(this).toggleClass('collapsed');

        if ($(this).hasClass('collapsed')) {
            $(this).find('.fs_nav-link-icon').text('expand_more');
        } else {
            $(this).find('.fs_nav-link-icon').text('expand_less');
        }
    });

    $('body').on('click', '.fs_remove-favourite', function (e) {
        e.preventDefault();
        var suiteId = $(this).closest('.fs_favourite').attr('data-suite-id');
        $(this).closest('.fs_favourite').remove();
        //remove fs_selected class from search results table favourite button
        $('button[data-suite-id=' + suiteId + ']').removeClass('fs_selected');

        favCount--;



        if (favCount > 0) {
            $('.fs_count-favourites-text').html('<a href="#" class="btn-favourites-toggle-mobile">' + favCount + favouritesTranslated + '</a>');
            $('body').addClass('fs_favourites-active');
        } else {
            $('.fs_count-favourites-text').html(favCount + favouritesTranslated);
            $('body').removeClass('fs_favourites-active fs_has-favourites fs_view-favourites fs_view-favourites-full');
        }
    });


    function faveButton() {

        $(this).toggleClass('fs_selected');

        var suiteId = $(this).attr('data-suite-id');

        var suiteThumbnail = $(this).closest('.fs_search-result-property').find('.fs_search-result-thumbnail img').attr('src');
        console.log(suiteThumbnail);

        var suiteBuildingName = $(this).closest('.fs_search-result-property').find('.fs_search-result-title').text();
        console.log(suiteBuildingName);

        var suiteAddress = $(this).closest('.fs_search-result-property').find('.fs_search-result-address').text();
        console.log(suiteAddress);

        var parentRow = $(this).closest('tr');

        var suiteName = $(parentRow).find('.fs_result-suite-name a').text();
        console.log(suiteName);

        var suiteArea = $(parentRow).find('.fs_result-suite-area').text();
        console.log(suiteArea);

        var suiteAvailability = $(parentRow).find('.fs_result-suite-availability').text();
        console.log(suiteAvailability);

        var suiteRent = $(parentRow).find('.fs_result-suite-rent').text();
        console.log(suiteRent);

        var suiteLoading = $(parentRow).find('.fs_result-suite-loading').html();

        var suitePower = $(parentRow).find('.fs_result-suite-power').html();

        var suiteCeiling = $(parentRow).find('.fs_result-suite-ceiling').text();

        if (suiteCeiling == '') {
            suiteCeiling = '12ft';
        }

        var suiteAdditionalRent = $(this).closest('.fs_search-result-collapse').find('.fs_other-costs .fs_result-additional-rent .fs_result-value').text();

        var suiteOperatingCosts = $(this).closest('.fs_search-result-collapse').find('.fs_other-costs .fs_result-operating-costs .fs_result-value').text();

        var suiteRealtyTax = $(this).closest('.fs_search-result-collapse').find('.fs_other-costs .fs_result-realty-tax .fs_result-value').text();

        var htmlOutput = '<div data-suite-id="' + suiteId + '" class="fs_favourite fs_favourite-' + suiteId + '">' +
                '<div class="fs_favourite-header row">' +
                '<div class="col fs_favourite-thumbnail">' +
                '<img src="' + suiteThumbnail + '" alt="thumbnail" class="img-fluid">' +
                '<div class="fs_favourite-remove"><a href="#" class="fs_remove-favourite"><i class="material-icons fs_icon-clear">clear</i></a></div>' +
                '</div>' +
                '<div class="col d-lg-none fs_favourite-details-header">' +
                '<h5 class="fs_favourite-title">' + suiteBuildingName + '</h5>' +
                '<h6 class="fs_favourite-address">' + suiteAddress + '</h6>' +
                '</div>' +
                '</div>' +
                '<ul class="fs_favourite-details-list">' +
                '<li class="row d-none d-lg-flex"><div class="col-6 d-lg-none"></div><div class="col-6 col-lg-12"><h5 class="fs_favourite-title">' + suiteBuildingName + '</h5></div></li>' +
                '<li class="row"><div class="col-6 d-lg-none">Suite #</div><div class="col-6 col-lg-12">' + suiteName + '</div></li>' +
                '<li class="row"><div class="col-6 d-lg-none">Area (Div/Contig)</div><div class="col-6 col-lg-12">' + suiteArea + '</div></li>' +
                '<li class="row"><div class="col-6 d-lg-none">Rent (PSF)</div><div class="col-6 col-lg-12">' + suiteRent + '</div></li>' +
                '<li class="row"><div class="col-6 d-lg-none">Availability</div><div class="col-6 col-lg-12">' + suiteAvailability + '</div></li>' +
                '<li class="row"><div class="col-6 d-lg-none">Loading</div><div class="col-6 col-lg-12">' + suiteLoading + '</div></li>' +
                '<li class="row"><div class="col-6 d-lg-none">Power</div><div class="col-6 col-lg-12">' + suitePower + '</div></li>' +
                '<li class="row"><div class="col-6 d-lg-none">Ceiling</div><div class="col-6 col-lg-12">' + suiteCeiling + '</div></li>' +
                '</ul>' +
                '<ul class="fs_favourite-details-other-costs">' +
                '<li class="row"><div class="col-6 d-lg-none">Total Additional Rent</div><div class="col-6 col-lg-12">' + suiteAdditionalRent + '</div></li>' +
                '<li class="row"><div class="col-6 d-lg-none">Operating Costs</div><div class="col-6 col-lg-12">' + suiteOperatingCosts + '</div></li>' +
                '<li class="row"><div class="col-6 d-lg-none">Realty Tax</div><div class="col-6 col-lg-12">' + suiteRealtyTax + '</div></li>' +
                '<li class="row d-lg-none"><div class="col-6"><a href="#" class="btn btn-primary btn-block fs_favourite-btn-share"><i class="material-icons fs_btn-icon fs_btn-icon-share">share</i> <span class="fs_btn-text">Share</span></a></div><div class="col-6"><a href="#" class="btn btn-primary btn-block fs_favourite-btn-details"><i class="material-icons fs_btn-icon fs_btn-icon-share">info</i> <span class="fs_btn-text">Details</span></a></div></li>' +
                '</ul>' +
                '</div>';

        if ($(this).hasClass('fs_selected')) {
            if (favCount == 0) {
                //favourites is empty

                //remove message
                $('#fs_favourites-carousel').empty();

                //add favourite
                $('#fs_favourites #fs_favourites-carousel').append(htmlOutput);

                if (isMobile) {
                    //intialize carousel
                    owl = $('.owl-carousel');
                    owl.owlCarousel({
                        loop: false,
                        margin: 10,
                        nav: false,
                        dots: false,
                        slideBy: 1,
                        responsive: {
                            0: {
                                items: 1,
                                slideBy: 1
                            },
                            640: {
                                items: 2,
                                slideBy: 1
                            }
                        }
                    });

                } else {
                    //initialize favourites-carousel
                    $("#fs_favourites-carousel").sortable();
                }
            } else {
                //favourites not empty
                if (isMobile) {
                    //these 3 lines kill owl carousel, and returns the markup to the initial state
                    owl.trigger('destroy.owl.carousel');
                    owl.find('.owl-stage-outer').children().unwrap();
                    owl.removeClass("owl-center owl-loaded owl-text-select-on");

                    $('#fs_favourites #fs_favourites-carousel').append(htmlOutput);

                    //intialize carousel
                    owl = $('.owl-carousel');
                    owl.owlCarousel({
                        loop: false,
                        margin: 10,
                        nav: false,
                        dots: false,
                        slideBy: 1,
                        responsive: {
                            0: {
                                items: 1,
                                slideBy: 1
                            },
                            640: {
                                items: 2,
                                slideBy: 1
                            }
                        }
                    });


                } else {
                    $("#fs_favourites-carousel").sortable("destroy");
                    //add item 
                    $('#fs_favourites #fs_favourites-carousel').append(htmlOutput);
                    //initialize favourites-carousel
                    $("#fs_favourites-carousel").sortable();
                }
            }
            favCount++;
        } else {
            //remove item from favourites

            if (isMobile) {
                //these 3 lines kill owl carousel, and returns the markup to the initial state
                owl.trigger('destroy.owl.carousel');
                owl.find('.owl-stage-outer').children().unwrap();
                owl.removeClass("owl-center owl-loaded owl-text-select-on");

                $('#fs_favourites').find('.fs_favourite-' + suiteId).remove();

                if (favCount > 1) {
                    //favourites not empty intialize carousel
                    owl = $('.owl-carousel');
                    owl.owlCarousel({
                        loop: false,
                        margin: 10,
                        nav: false,
                        dots: false,
                        slideBy: 1,
                        responsive: {
                            0: {
                                items: 1,
                                slideBy: 1
                            },
                            640: {
                                items: 2,
                                slideBy: 1
                            }
                        }
                    });

                } else {
                    //favourites now empty
                    $('#fs_favourites-carousel').empty();
                    $('#fs_favourites-carousel').append('<p class="fs_favourites-message">'+noFavouritesMessage+'</p>');
                }
            } else {
                $("#fs_favourites-carousel").sortable("destroy");
                //remove from favourites-carousel
                $('#fs_favourites').find('.fs_favourite-' + suiteId).remove();
                if (favCount > 1) {
                    //intialize favourites-carousel
                    $("#fs_favourites-carousel").sortable();
                }
            }
            favCount--;
        }

        if (favCount > 0) {
            $('.fs_count-favourites-text').html('<a href="#" class="btn-favourites-toggle-mobile">' + favCount + favouritesTranslated +'</a>');
            $('body').addClass('fs_favourites-active fs_has-favourites');
        } else {
            $('.fs_count-favourites-text').html(favCount + favouritesTranslated);
            $('body').removeClass('fs_favourites-active fs_has-favourites fs_view-favourites fs_view-favourites-full');
        }
        //suiteId = $(this).attr('data-suite-id');
        //buildingId = $(this).attr('data-building-id');
    }

    $('.fs_favourites-toggle').on('click', function (e) {
        e.preventDefault();
        if ($('body').hasClass('fs_view-favourites')) {
            if ($('body').hasClass('fs_view-favourites-full')) {
                $('body').removeClass('fs_view-favourites fs_view-favourites-full');
                $(this).find('.fs_icon-expand').text('expand_more');
            } else {
                $('body').addClass('fs_view-favourites-full');
                $(this).find('.fs_icon-expand').text('expand_less');
            }
        } else {

        }
    });

    $('body').on('click', '.btn-favourites-toggle-mobile', function (e) {
        e.preventDefault();
        $('body').toggleClass('fs_view-favourites');
    });

    $('.fs_favourites-clear').on('click', function (e) {
        e.preventDefault();
        $('#fs_favourites-carousel').sortable('destroy');
        $('#fs_favourites-carousel').empty();
        //clear selected favourite buttons
        $('#fs_results .fs_btn-favourite.fs_selected').removeClass('fs_selected');
        //reset favourite count and state
        $('.fs_count-favourites-text').empty().text('0 '+favouritesTranslated);
        $('body').removeClass('fs_has-favourites fs_favourites-active fs_view-favourites fs_view-favourites-full');
        $('#fs_favourites-carousel').append('<p class="fs_favourites-message">'+noFavouritesMessage+'</p>');
        favCount = 0;
    });

    $('.fs_filter-list-toggle').on('click', function (e) {
        if ($(this).hasClass('collapsed')) {
            $(this).find('.fs_icon_filter').text('expand_less');
        } else {
            $(this).find('.fs_icon_filter').text('expand_more');
        }

    });

//    $("#slider-range").slider({
//        range: true,
//        min: 0,
//        max: 5000,
//        values: [500, 1500],
//        slide: function (event, ui) {
//            $("#amount").val(ui.values[ 0 ] + " - " + ui.values[ 1 ] + "sqft");
//        }
//    });
//    $("#amount").val($("#slider-range").slider("values", 0) +
//            " - " + $("#slider-range").slider("values", 1) + "sqft");













//--------------------
// Map, Filter and Template code below
//--------------------
//
// Global Variables
    var markersToRemove = []; // array
    var selectedFilters = {}; // object
    var previousLocation = []; // array


    var urlFilters = {}; // object
    urlFilters['suite_type'] = []; // array
    urlFilters['suite_size'] = [];
    urlFilters['location'] = [];
    urlFilters['mapFilter'] = [];
    urlFilters['suite_availability'] = [];
    urlFilters['lang'] = [];
    urlFilters['SuiteOfficeSize'] = [];
    urlFilters['SuiteClearHeight'] = [];
    urlFilters['SuiteShippingDoors'] = [];
    urlFilters['SuiteDriveInDoors'] = [];
    urlFilters['SuiteAvailElecAmps'] = [];
    urlFilters['SuiteAvailElecVolts'] = [];
    urlFilters['SuiteParkingSurfaceStalls'] = [];
    urlFilters['SuiteTruckTrailerParking'] = [];
    var urlFilterSet = 0;
    var originalUrl = ''; // string
    var pdfURL = ""; // string
    
    
    var availArray = [];
    var date = moment();
    var dateTime = moment().unix(date);

    var map;
//    var bounds = new google.maps.LatLngBounds();
    var bounds;
    var mapcenterLa = null;
    var mapcenterLg = null;
    
    // we might want to look into pulling the JSON from an ajax call
    // instead of embedding it into the HTML
    // but as discussed with KK this is still open to someone scraping it either way.
    var buildingListAll = JSON.parse($("#theBuildings").text());
    var suiteListAll = JSON.parse($("#theSuites").text());

    var buildingList = [];
    buildingList.push(buildingListAll);
    buildingList.push(suiteListAll);
    

    var suiteMin;
    var suiteMax;
    
//    var startUrl = "http://"+ url_domain(window.location.pathname) +"/index.php?" ;
//    var startUrl = url_domain(window.location.pathname) +"/index.php?" ;
    
    
//    var infoWindow = new google.maps.InfoWindow({});
    var infoWindow;

    

    var startUrl = "/index4.2.0.php";
    var queryString = '';
    
    var filtersNotEmpty = 0;
    var changedMarkers = [];
    
    var theLanguage = "en_CA";

    
    


    // controlling this with a checkbox
    // this needs to pick up a default setting
    // from php
    var showVacancies;
    
    var vacancyPassThru = $("#no_vacancy_val").data('value');
//    console.log("vacancyPassThru");
//    console.log(vacancyPassThru);
    if (vacancyPassThru == 1) {
        showVacancies = true;
    } else {
        showVacancies = false;
    }
    
    

// Functions at the top 
// 
// "isInteger" Polyfill for browsers that don't support it (IE and Edge and maybe Safari)
    Number.isInteger = Number.isInteger || function (value) {
        return typeof value === "number" &&
                isFinite(value) &&
                Math.floor(value) === value;
    };

    function inArrayCaseInsensitive(needle, haystackArray) {
//Iterates over an array of items to return the index of the first item that matches the provided val ('needle') in a case-insensitive way.  Returns -1 if no match found.
        var defaultResult = -1;
        var result = defaultResult;
        $.each(haystackArray, function (index, value) {
            if (Number.isInteger(value)) {
//                console.log(value);
            } else
            if (result == defaultResult && value.toLowerCase() == needle.toLowerCase()) {
                result = index;
            }
        });
        return result;
    }




var availabilityFrenchText = "";

    function processAvailability(availabilitySet) {


        switch (availabilitySet) {

            case 'All':
                availArray.push(2147483640);
                availArray.push(0);
                break;
            case 'Immediately':
                availArray.push(dateTime);
                availArray.push(1);
                break;
            case 'Under 3 Months':
                var threeMonths = moment(date).add(3, 'months');
                var threeMonthsUnix = moment(threeMonths).unix();
                availArray.push(threeMonthsUnix);
                availArray.push(1);
                break;
            case '3-6 Months':
                var threeMonths = moment(date).add(3, 'months');
                var threeMonthsUnix = moment(threeMonths).unix();
                var sixMonths = moment(date).add(6, 'months');
                var sixMonthsUnix = moment(sixMonths).unix();
                availArray.push(sixMonthsUnix);
                availArray.push(threeMonthsUnix);
                break;
            case '6-12 Months':
                var sixMonths = moment(date).add(6, 'months');
                var sixMonthsUnix = moment(sixMonths).unix();
                var twelveMonths = moment(date).add(12, 'months');
                var twelveMonthsUnix = moment(twelveMonths).unix();
                availArray.push(twelveMonthsUnix);
                availArray.push(sixMonthsUnix);
                break;
            case 'Over 12 Months':
                var twelveMonths = moment(date).add(12, 'months');
                var twelveMonthsUnix = moment(twelveMonths).unix();
                availArray.push(2147483639);
                availArray.push(twelveMonthsUnix);
                break;
            case 'Tous':
                availArray.push(2147483640);
                availArray.push(0);
                break;
            case 'Immédiatement':
                availArray.push(dateTime);
                availArray.push(1);
                break;
            case 'Moins de 3 mois':
                var threeMonths = moment(date).add(3, 'months');
                var threeMonthsUnix = moment(threeMonths).unix();
                availArray.push(threeMonthsUnix);
                availArray.push(1);
                break;
            case '3-6 mois':
                var threeMonths = moment(date).add(3, 'months');
                var threeMonthsUnix = moment(threeMonths).unix();
                var sixMonths = moment(date).add(6, 'months');
                var sixMonthsUnix = moment(sixMonths).unix();
                availArray.push(sixMonthsUnix);
                availArray.push(threeMonthsUnix);
                break;
            case '6-12 mois':
                var sixMonths = moment(date).add(6, 'months');
                var sixMonthsUnix = moment(sixMonths).unix();
                var twelveMonths = moment(date).add(12, 'months');
                var twelveMonthsUnix = moment(twelveMonths).unix();
                availArray.push(twelveMonthsUnix);
                availArray.push(sixMonthsUnix);
                break;
            case 'Plus de 12 mois':
                var twelveMonths = moment(date).add(12, 'months');
                var twelveMonthsUnix = moment(twelveMonths).unix();
                availArray.push(2147483639);
                availArray.push(twelveMonthsUnix);
                break;
            default:
                availArray.push(2147483640);
                availArray.push(0);
        }
//        } else {
//            availArray.push(2147483640);
//            availArray.push(0);
//        }
        return availArray;

    } // end processAvailability
 
    
    
    
    function translateAvailability(availabilityFrenchText) {
        switch (availabilityFrenchText) {
            case 'Immediately':
                availabilityFrenchText = 'Immédiatement';
                break;
            case 'Under 3 Months':
                availabilityFrenchText = 'Moins de 3 mois';
                break;
            case '3-6 Months':
                availabilityFrenchText = '3-6 mois';
                break;
            case '6-12 Months':
                availabilityFrenchText = '6-12 mois';
                break;
            case 'Over 12 Months':
                availabilityFrenchText = 'Plus de 12 mois';
                break;
        }
        return availabilityFrenchText;
    }
    
    function typeLanguage(theSuiteType) {
        
        switch (theSuiteType) {
            case "office":
                theSuiteType = "bureau";
                break;
            case "industrial":
                theSuiteType = "industriel";
                break;
            case "retail":
                theSuiteType = "détail";
                break;
        }
        return theSuiteType;
    }
    
    function chipLanguage(theChipValue) {
        
        switch (theChipValue) {
            case "bureau":
                theChipValue = "office";
                break;
            case "industriel":
                theChipValue = "industrial";
                break;
            case "détail":
                theChipValue = "retail";
                break;
        }
        return theChipValue;
    }


//--------------------
// Part One: Process the query string, if present
//--------------------

    function processUrl() {
//        console.log("processUrl url begins");
        var url = new URL(window.location.href);
//        console.log("processUrl starting url:");
//        console.log(url);
    urlFilters = {}; // object
    urlFilters['suite_type'] = []; // array
    urlFilters['suite_size'] = [];
    urlFilters['location'] = [];
    urlFilters['mapFilter'] = [];
    urlFilters['suite_availability'] = [];
    urlFilters['lang'] = [];
    urlFilters['SuiteOfficeSize'] = [];
    urlFilters['SuiteClearHeight'] = [];
    urlFilters['SuiteShippingDoors'] = [];
    urlFilters['SuiteDriveInDoors'] = [];
    urlFilters['SuiteAvailElecAmps'] = [];
    urlFilters['SuiteAvailElecVolts'] = [];
    urlFilters['SuiteParkingSurfaceStalls'] = [];
    urlFilters['SuiteTruckTrailerParking'] = [];
    urlFilterSet = 0;

        
        originalUrl = url;
        var queryParams = window.location.search.substr(1).split('&').reduce(function (q, query) {
            var chunks = query.split('=');
            var key = chunks[0];
            var value = chunks[1];
            return (q[key] = value, q);
        }, {});

//console.log("queryParams");
//console.log(queryParams);


            if ((typeof queryParams["province"] !== 'undefined') && (queryParams["province"] !== '') && (queryParams["province"] !== "All")) {
//                console.log("province is set");
                urlFilters['location'].push("province");
                urlFilters['location'].push(queryParams["province"]);
                urlFilterSet++;
                var theText = queryParams["province"].toString().replace(/%20/g, " ");
                $("#property-search").val(theText);
            }

            if ((typeof queryParams["city"] !== 'undefined') && (queryParams["city"] !== '') && (queryParams["city"] !== "All")) {
//                console.log("city is set");
                urlFilters['location'].push("city");
                urlFilters['location'].push(queryParams["city"]);
                urlFilterSet++;
                var theText = queryParams["city"].toString().replace(/%20/g, " ");
                $("#property-search").val(theText);
            }

            if ((typeof queryParams["region"] !== 'undefined') && (queryParams["region"] !== '') && (queryParams["region"] !== "All")) {
//                console.log("region is set");
                urlFilters['location'].push("city");
                urlFilters['location'].push(queryParams["region"]);
                urlFilterSet++;
                var theText = queryParams["region"].toString().replace(/%20/g, " ");
                $("#property-search").val(theText);
            }

            if ((typeof queryParams["pc"] !== 'undefined') && (queryParams["pc"] !== '') && (queryParams["pc"] !== "All")) {
//                console.log("pc is set");
                urlFilters['location'].push("pc");
                urlFilters['location'].push(queryParams["pc"]);
                urlFilterSet++;
                var theText = queryParams["pc"].toString().replace(/%20/g, " ");
                $("#property-search").val(theText);
            }

            if ((typeof queryParams["street_address"] !== 'undefined') && (queryParams["street_address"] !== '') && (queryParams["street_address"] !== "All")) {
//                console.log("street_address is set");
                urlFilters['location'].push("street_address");
                urlFilters['location'].push(queryParams["street_address"]);
                urlFilterSet++;
                var theText = queryParams["street_address"].toString().replace(/%20/g, " ");
                $("#property-search").val(theText);
            }

            if ((typeof queryParams["building_name"] !== 'undefined') && (queryParams["building_name"] !== '') && (queryParams["building_name"] !== "All")) {
//                console.log("building_name is set");
                var theText = queryParams["building_name"].toString().replace(/%20/g, " ");
                urlFilters['location'].push("building_name");
                urlFilters['location'].push(theText);
                urlFilterSet++;
                $("#property-search").val(theText);
            }

            if ((typeof queryParams["SuiteType"] !== 'undefined') && (queryParams["SuiteType"] !== '') && (queryParams["SuiteType"] !== "All")) {
//                console.log("SuiteType is set");
                var lowerCaseVal = queryParams["SuiteType"].toLowerCase();
                urlFilters['suite_type'].push(lowerCaseVal);
                urlFilterSet++;
                $("#" + lowerCaseVal + "").prop("checked", true);


            if (lowerCaseVal !== "industrial") {
//                    console.log("clear advanced search processUrl - industrial not matched");
                    $("#collapseExample5 select").val("All");
                    $("#industrialFilters").addClass("d-none");
                } else if (lowerCaseVal === "industrial") {
                    $("#industrialFilters").removeClass("d-none");
                }
            } else {
//                console.log("No Type Filter in URL - Uncheck All Three boxes");
                $("#industrial").prop("checked", false);
                $("#office").prop("checked", false);
                $("#retail").prop("checked", false);

//                console.log("clear advanced search processUrl - no SuiteType set");
                $("#collapseExample5 select").val("All");
                $("#industrialFilters").addClass("d-none");
            }

        
            if ((typeof queryParams["Size"] !== 'undefined') && (queryParams["Size"] !== '') && (queryParams["Size"] !== "All")) {
//                console.log("size is set");
                var sizeVals = queryParams["Size"];
//                console.log("sizeVals");
//                console.log(sizeVals);
//                    sizeVals = sizeVals.replace(/%2C/g, "").replace(/\+/g,"");
                if ((sizeVals.indexOf("+-+") > -1) || (sizeVals.indexOf("+and+") > -1) || (sizeVals.indexOf("%20-%20") > -1)) {
                    sizeVals = queryParams["Size"].toString().replace(/ /g, '').replace(/,/g, '').replace(/%2C/g, "").replace(/%20/g, "").replace('-', ',').replace(/\+/g, "");
                }
                if (sizeVals.indexOf("Under") > -1) {
                    sizeVals = "0,5000";
                } else if (sizeVals.indexOf("above") > -1) {
                    sizeVals = "200000,2147483646";
                }

                switch(sizeVals) {
                    case "0,5000":
                        $("select#Size").prop('selectedIndex', 1);
                        break;
                    case "5000,10000":
                        $("select#Size").prop('selectedIndex', 2);
                        break;
                    case "10000,20000":
                        $("select#Size").prop('selectedIndex', 3);
                        break;
                    case "20000,40000":
                        $("select#Size").prop('selectedIndex', 4);
                        break;
                    case "40000,80000":
                        $("select#Size").prop('selectedIndex', 5);
                        break;
                    case "80000,120000":
                        $("select#Size").prop('selectedIndex', 6);
                        break;
                    case "120000,200000":
                        $("select#Size").prop('selectedIndex', 7);
                        break;
                    case "200000,2147483646":
                        $("select#Size").prop('selectedIndex', 8);
                        break;
                    default:
                        $("select#Size").prop('selectedIndex', 0);                            
                }
//                var sizeVals = queryParams["Size"].replace("%2C", "").replace("+","");
                var sizeStrings = sizeVals.split(',');
                urlFilters['suite_size'].push(parseInt(sizeStrings[0]));
                urlFilters['suite_size'].push(parseInt(sizeStrings[1]));
                urlFilterSet++;
            } 
//            else if (k === "Size" && v === "All") {
//                urlFilters['suite_size'].push(suiteMin);
//                urlFilters['suite_size'].push(suiteMax);
//                urlFilterSet++;
//            }


            if ((typeof queryParams["Availability"] !== 'undefined') && (queryParams["Availability"] !== '') && (queryParams["Availability"] !== "All")) {
//                console.log("Availability is set");
                // clean up urlencoded text coming in from url
                var theText = queryParams["Availability"].toString().replace(/%20/g, " ").replace(/\+/g, " ");
//            console.log("theText - availability url");    
//            console.log(theText);
                    $("#Availability").val(theText);
//                    console.log("availArray urlfilter");
//                    console.log(availArray);
                    urlFilters['suite_availability'].push(theText);
                    urlFilterSet++;
                }

            if ((typeof queryParams["lang"] !== 'undefined') && (queryParams["lang"] !== '') && (queryParams["lang"] !== "All")) {
                console.log("Lang is set");
                urlFilters['lang'].push(queryParams["lang"]);
                urlFilterSet++;
                theLanguage = queryParams["lang"];
            }

            if ((typeof queryParams["SuiteOfficeSize"] !== 'undefined') && (queryParams["SuiteOfficeSize"] !== '') && (queryParams["SuiteOfficeSize"] !== "All")) {
//                console.log("SuiteOfficeSize is set");
                var officeDropVal = queryParams['SuiteOfficeSize'];
                $("#SuiteOfficeSize").val(officeDropVal);
                urlFilters['SuiteOfficeSize'].push(officeDropVal);

            }

            if ((typeof queryParams["SuiteClearHeight"] !== 'undefined') && (queryParams["SuiteClearHeight"] !== '') && (queryParams["SuiteClearHeight"] !== "All")) {
//                console.log("SuiteClearHeight is set");
                // set the dropdown before processing value
                $("#SuiteClearHeight").val(queryParams["SuiteClearHeight"]);
                // process the value
                if (queryParams["SuiteClearHeight"].indexOf("below") > -1) {
                    urlFilters['SuiteClearHeight'].push(0);
                } else {
                    var heightVal = queryParams["SuiteClearHeight"].toString().replace(/ /g, '').replace(/,/g, '').replace(/'/g, '').replace('min', '');
                    urlFilters['SuiteClearHeight'].push(heightVal);
            }

                urlFilterSet++;
                
            }

            if ((typeof queryParams["SuiteShippingDoors"] !== 'undefined') && (queryParams["SuiteShippingDoors"] !== '') && (queryParams["SuiteShippingDoors"] !== "All")) {
//                console.log("SuiteShippingDoors is set");
                // set the dropdown before processing value
                $("#SuiteShippingDoors").val(queryParams["SuiteShippingDoors"]);
                // process the value

                    var heightVal = queryParams["SuiteShippingDoors"].toString().replace(/ /g, '').replace(/,/g, '').replace(/'/g, '').replace('min', '');
                    urlFilters['SuiteShippingDoors'].push(heightVal);
                                
                urlFilterSet++;
            }

            if ((typeof queryParams["SuiteDriveInDoors"] !== 'undefined') && (queryParams["SuiteDriveInDoors"] !== '') && (queryParams["SuiteDriveInDoors"] !== "All")) {
//                console.log("SuiteDriveInDoors is set");
                // set the dropdown before processing value
                $("#SuiteDriveInDoors").val(queryParams["SuiteDriveInDoors"]);
                // process the value

                    var heightVal = queryParams["SuiteDriveInDoors"].toString().replace(/ /g, '').replace(/,/g, '').replace(/'/g, '').replace('min', '');
                    urlFilters['SuiteDriveInDoors'].push(heightVal);
                                
                urlFilterSet++;
            }

            if ((typeof queryParams["SuiteAvailElecAmps"] !== 'undefined') && (queryParams["SuiteAvailElecAmps"] !== '') && (queryParams["SuiteAvailElecAmps"] !== "All")) {
//                console.log("SuiteAvailElecAmps is set");
                // set the dropdown before processing value
                var dropVal = queryParams["SuiteAvailElecAmps"].toString().replace(/\+/g, ' ');
                $("#SuiteAvailElecAmps").val(dropVal);
                // process the value
                if (queryParams["SuiteAvailElecAmps"].indexOf("less") > -1) {
                    urlFilters['SuiteAvailElecAmps'].push(0);
                } else {
                    var ampsVal = queryParams["SuiteAvailElecAmps"].toString().replace(/ /g, '').replace(/A/g, '').replace('min', '');
                    urlFilters['SuiteAvailElecAmps'].push(ampsVal);
            }

                urlFilterSet++;
            }

            if ((typeof queryParams["SuiteAvailElecVolts"] !== 'undefined') && (queryParams["SuiteAvailElecVolts"] !== '') && (queryParams["SuiteAvailElecVolts"] !== "All")) {
//                console.log("SuiteAvailElecVolts is set");
                // set the dropdown before processing value
                var dropVal = queryParams["SuiteAvailElecVolts"];
//            console.log("dropVal to replace");    
//            console.log(dropVal);
                dropVal = dropVal.replace("+", " ");            
//                dropVal = dropVal.toString();
//            console.log("dropVal to set");    
//            console.log(dropVal);
            $("#SuiteAvailElecVolts").val(dropVal);
            var voltsVal = '';
                // process the value
                if (queryParams["SuiteAvailElecVolts"].indexOf("Max") > -1) {
                    voltsVal = 0;
                } else {
                    voltsVal = dropVal.toString().replace(/ /g, '').replace(/V/g, '').replace("+", " ").replace('min', '').replace('Max', '');
            }
                urlFilters['SuiteAvailElecVolts'].push(parseInt(voltsVal));

                urlFilterSet++;
            }

            if ((typeof queryParams["SuiteParkingSurfaceStalls"] !== 'undefined') && (queryParams["SuiteParkingSurfaceStalls"] !== '') && (queryParams["SuiteParkingSurfaceStalls"] !== "All")) {
//                console.log("SuiteParkingSurfaceStalls is set");
                var parkVal = queryParams["SuiteParkingSurfaceStalls"];
                $("#SuiteParkingSurfaceStalls").val(parkVal);
                urlFilters['SuiteParkingSurfaceStalls'].push(parkVal);

            }

            if ((typeof queryParams["SuiteTruckTrailerParking"] !== 'undefined') && (queryParams["SuiteTruckTrailerParking"] !== '') && (queryParams["SuiteTruckTrailerParking"] !== "All")) {
//                console.log("SuiteTruckTrailerParking is set");
                var truckParkVal = queryParams['SuiteTruckTrailerParking'];
                $("#SuiteTruckTrailerParking").val(truckParkVal);
                urlFilters['SuiteTruckTrailerParking'].push(parkVal);
            }


//console.log("urlFilters");
//console.log(urlFilters);

    }// end function processUrl()







//--------------------
// Part Two: Feed the processed query string to filterFunc()
// Or Run filterFunc() with no set filters to get a default list of buildings
//--------------------



    function filterFunc() {
//        console.log("filter Func is running");
//        console.log("This");
//        console.log($(this));
//        console.log("locationFilter");
//        console.log(locationFilter);

        var searchBox = locationFilter;
//        var searchBox = $("#property-search").getSelectedItemData();
        var bCode = $(this).data("buildingcode");
        if (bCode) {
            markersToRemove.push(bCode.toString());
        }
//        console.log("search box");
//        console.log(searchBox);
//        console.log("box value");
//        console.log($("#property-search").val());
//        console.log(Object.keys(selectedFilters).length);
//        console.log("markers to remove top of filterfunc");
//        console.log(markersToRemove);

        if (searchBox.length === 0 && $("#property-search").val() === '') {
//            there is no location search set
        }
        if (searchBox.length === 0 && $("#property-search").val() !== '') {
//            there is a previous search location set
            previousLocation = [];
            _.each(selectedFilters['location'], function (i) {
                previousLocation.push(i);
            });
        }
        if (searchBox.length !== 0 && $("#property-search").val() !== '') {
            // there is a NEW location search set
            previousLocation = [];
        }

        selectedFilters = {};
        selectedFilters['suite_type'] = [];
        selectedFilters['suite_size'] = [];
        selectedFilters['location'] = [];
        selectedFilters['mapFilter'] = [];
        selectedFilters['suite_availability'] = [];
        selectedFilters['lang'] = [];
        selectedFilters['SuiteOfficeSize'] = [];
        selectedFilters['SuiteClearHeight'] = [];
        selectedFilters['SuiteShippingDoors'] = [];
        selectedFilters['SuiteDriveInDoors'] = [];
        selectedFilters['SuiteAvailElecAmps'] = [];
        selectedFilters['SuiteAvailElecVolts'] = [];
        selectedFilters['SuiteParkingSurfaceStalls'] = [];
        selectedFilters['SuiteTruckTrailerParking'] = [];


//console.log("selectedFilters after initializing in filterFunc");
//console.log(selectedFilters);

//console.log("urlFilterSet before test in filterFunc");
//console.log(urlFilterSet);


        if (urlFilterSet === 0) {
//        console.log("urlFilters empty");
//        console.log(urlFilters);


        if (theLanguage !== "") {
            selectedFilters['lang'] = theLanguage;
        }

        

// apply filter from autocomplete input
            if ($("#property-search").val().length > 0) {
//            console.log("search box detected");
                var code = "";
                var name = "";
                var type = "";
                if (previousLocation.length !== 0) {
                    code = previousLocation[0];
                    name = previousLocation[1];
                    type = "location";
                } else
                {
                    code = searchBox['code'];
                    name = searchBox['value'];
                    type = searchBox['type'];
                }
                selectedFilters["location"].push(code);
                selectedFilters["location"].push(name);
            } // end if ($("#property-search").val().length > 0) {

        
        // if mouse has not left the map
        // do not reset mapLiveInteraction
        if (mouseOut === 1) {
            mapLiveInteraction = 0;
        }
        
        // process Map Filter
        if (typeof mapFilter !== "undefined") {
            _.each(mapFilter, function(v,k) {
                selectedFilters['mapFilter'].push(v);
            });
        }


//console.log("new filters loc");
//console.log(selectedFilters);


            // GET VALUES FROM SUITE TYPE CHECKBOXES
            // THIS SHOULD PROBABLY BE A DROPDOWN LIKE AR3 TO MATCH THE REST OF THE SITE
            // enumerate checkbox filter items
            $suiteFilterCheckboxes.filter(':checked').each(function () {
                if (!selectedFilters.hasOwnProperty(this.name)) {
                    selectedFilters[this.name] = [];
                }
                selectedFilters[this.name].push(this.value);
            });



            // Manage show/don't show and resetting of advanced select boxes
            // before processing the filters
            if (selectedFilters['suite_type'][0] !== "industrial") {
                $("#industrialFilters").addClass("d-none");
//                console.log("clear advanced search processUrl - no industrial in selectedFilters[suite_type] ");
                $("#collapseExample5 select").val("All");
            } 
            else {
                $("#industrialFilters").removeClass("d-none");
                //reset all industrial filters if industrial is not selected
//                $("#collapseExample5 select").val("All");
            }
 
//        else {
////            $("#industrialFilters").addClass("d-none");
////            $("#industrial").prop("checked", false);
////            $("#office").prop("checked", false);
////            $("#retail").prop("checked", false);
//            $("#industrial").removeAttr('checked');
//            $("#office").removeAttr('checked');
//            $("#retail").removeAttr('checked');
//            //reset all industrial filters if industrial is not selected
//            $("#collapseExample5 select").val("All");
//        }



            // GET VALUES FROM SIZE DROPDOWN
            var sizeDrop = $("#Size").val();
            var sizeStrings = [];
            if ((sizeDrop !== '') && (sizeDrop !== 'All')){
                if (sizeDrop.indexOf("Under") > -1 || sizeDrop.indexOf("moins") > -1) {
                    sizeStrings.push(0);
                    sizeStrings.push(5000);
                } else if (sizeDrop.indexOf("Above") > -1 || sizeDrop.indexOf("plus") > -1) {
                    sizeStrings.push(200000);
                    sizeStrings.push(2147483646);
                } else {
                    var sizeVals = sizeDrop.replace(/ /g, '').replace(/,/g, '').replace('-', ',');
                    sizeStrings = sizeVals.split(',');
                }
                if (sizeStrings[0] !== "All") {
                    _.each(sizeStrings, function (v) {
                        selectedFilters['suite_size'].push(v);
                    });
                }
            }





            // GET VALUES FROM availability DROPDOWN
            var availabilityDrop = $("#Availability").val();
            availArray = [];
            // processAvailability converts availability dropdown values to unix Timestamps and populates availArray
            processAvailability(availabilityDrop);
//            console.log("availArray");
//            console.log(availArray);

            if (availArray[0] !== 2147483640) {
                _.each(availArray, function (v) {
                    selectedFilters['suite_availability'].push(v);
                });
            }


            // GET VALUES FROM SuiteOfficeSize
            var officeSpaceDrop = $("#SuiteOfficeSize").val();
            var officeSpaceStrings = [];
//            console.log("officeSpaceDrop");
//            console.log(officeSpaceDrop);
            
            if (officeSpaceDrop !== '') {
                if (officeSpaceDrop.indexOf("less") > -1) {
                    officeSpaceStrings.push(-1);
                    officeSpaceStrings.push(5);
                } else if (officeSpaceDrop.indexOf("more") > -1) {
                    officeSpaceStrings.push(75);
                    officeSpaceStrings.push(200);
                } else {
                    var sizeVals = officeSpaceDrop.replace(/ /g, '').replace(/,/g, '').replace(/%/g, '').replace('-', ',');
                    officeSpaceStrings = sizeVals.split(',');
                }
                if (officeSpaceStrings[0] !== "All") {
                    _.each(officeSpaceStrings, function (v) {
                        selectedFilters['SuiteOfficeSize'].push(v);
                    });
                }
            }
            

            // GET VALUES FROM SuiteClearHeight
            var clearHeightDrop = $("#SuiteClearHeight").val();
            var clearHeightStrings = [];
//            console.log("clearHeightDrop");
//            console.log(clearHeightDrop);
            
            if (clearHeightDrop !== '') {
                if (clearHeightDrop.indexOf("below") > -1) {
                    clearHeightStrings.push(0);
                } else {
                    var sizeVals = clearHeightDrop.replace(/ /g, '').replace(/,/g, '').replace(/'/g, '').replace('min', '');
                    clearHeightStrings = sizeVals.split(',');
                }
                if (clearHeightStrings[0] !== "All") {
                    _.each(clearHeightStrings, function (v) {
                        selectedFilters['SuiteClearHeight'].push(parseInt(v));
                    });
                }
            }


            // GET VALUES FROM SuiteShippingDoors
            var shippingDoorsDrop = $("#SuiteShippingDoors").val();
            var shipVal = [];
//            console.log("shippingDoorsDrop");
//            console.log(shippingDoorsDrop);
            
            if ((shippingDoorsDrop !== '') && (shippingDoorsDrop !== "All")) {
                
                var shipVal = shippingDoorsDrop.replace(/ /g, '').replace('min', '');

                if (shipVal !== "All") {
                        selectedFilters['SuiteShippingDoors'].push(parseInt(shipVal));
                }
            }
            
            
            
            // GET VALUES FROM SuiteDriveInDoors
            var driveInDoorsDrop = $("#SuiteDriveInDoors").val();
            var driveVal = [];
//            console.log("driveInDoorsDrop");
//            console.log(driveInDoorsDrop);
            
            if ((driveInDoorsDrop !== '') && (driveInDoorsDrop !== "All")) {
                
                var driveVal = driveInDoorsDrop.replace(/ /g, '').replace('min', '');

                if (driveVal !== "All") {
                        selectedFilters['SuiteDriveInDoors'].push(parseInt(driveVal));
                }
            }
            
            
            
            // GET VALUES FROM SuiteAvailElecAmps
            var elecAmpsDrop = $("#SuiteAvailElecAmps").val();
            var ampVal = [];
//            console.log("elecAmpsDrop");
//            console.log(elecAmpsDrop);
            
            if ((elecAmpsDrop !== '') && (elecAmpsDrop !== "All")) {
                if (elecAmpsDrop.indexOf("less") > -1) {
                    ampVal = 0;
                } else {
                    ampVal = elecAmpsDrop.replace(/ /g, '').replace(/A/g, '').replace('min', ''); 
                }
                selectedFilters['SuiteAvailElecAmps'].push(parseInt(ampVal));
            }
            
            
            
            // GET VALUES FROM SuiteAvailElecVolts
            var elecVoltsDrop = $("#SuiteAvailElecVolts").val();
            var voltVal = [];
//            console.log("elecVoltsDrop");
//            console.log(elecVoltsDrop);
            
            if ((elecVoltsDrop !== '') && (elecVoltsDrop !== "All")) {
                if (elecVoltsDrop.indexOf("Max") > -1) {
                    voltVal = 0;
                } else {
                    voltVal = elecVoltsDrop.replace(/ /g, '').replace(/V/g, '').replace('min', ''); 
                }
                selectedFilters['SuiteAvailElecVolts'].push(parseInt(voltVal));
            }
            
            
            
            
            
            // GET VALUES FROM SuiteParkingSurfaceStalls
            var surfaceStallsDrop = $("#SuiteParkingSurfaceStalls").val();
            var surfaceStallsStrings = [];
//            console.log("surfaceStallsDrop");
//            console.log(surfaceStallsDrop);
            
            if (surfaceStallsDrop !== '') {
                if (surfaceStallsDrop.indexOf("75") > -1) {
                    surfaceStallsStrings.push(50);
                    surfaceStallsStrings.push(20000);
                } else {
                    var sizeVals = surfaceStallsDrop.replace(/ /g, '').replace('-', ',');
                    surfaceStallsStrings = sizeVals.split(',');
                }
                if (surfaceStallsStrings[0] !== "All") {
                    _.each(surfaceStallsStrings, function (v) {
                        selectedFilters['SuiteParkingSurfaceStalls'].push(v);
                    });
                }
            }
            
            
            // GET VALUES FROM SuiteTruckTrailerParking
            var truckTrailerDrop = $("#SuiteTruckTrailerParking").val();
            var tTval = [];
//            console.log("truckTrailerDrop");
//            console.log(truckTrailerDrop);
            
            if ((truckTrailerDrop !== '') && (truckTrailerDrop !== "All")) {
                if (truckTrailerDrop.indexOf("Yes") > -1) {
                    tTval = 1;
                } else {
                    tTval = 0;
                }
                selectedFilters['SuiteTruckTrailerParking'].push(tTval);
            }
        } // end if (urlFilterset === 0)


        if (urlFilterSet > 0) {
//        console.log("urlFilters not empty");
//        console.log(urlFilters);
// push urlFilters into selectedFilters
            selectedFilters = urlFilters;
// reset url filter flag
            urlFilterSet = 0;
//console.log("selectedFilters after urlfilter");
//console.log(selectedFilters);
        }
//        console.log("new filters");
//        console.log(selectedFilters);
        
//            console.log("selectedFilters fully populated");
//            console.log(selectedFilters);
        
        // create PROPER OBJECT COPIES of the buildings and suite
        // so we filter these instead of the base/root JSON
        // because we use that every time a filter changes
        var filteredBuildings = null;
        var filteredSuites = null;
//        filteredBuildings = buildingListAll;
//        filteredSuites = suiteListAll;
        filteredBuildings = jQuery.extend(true, {}, buildingListAll);
        filteredSuites = jQuery.extend(true, {}, suiteListAll);



        // filter buildings
        // create empty OBJECT
        var buildingOutput = {};
        // 
        // Building LOCATION
        // this handles Building name, street address, postal code, city and province
        // entries from the search box
        // 
        // check if building location filter is set
        if (selectedFilters['location'].length !== 0) {
            // get which type of location has been chosen
            var locationVar = selectedFilters['location'][0];
            // loop through object
            for (var key in filteredBuildings) {
                if (filteredBuildings.hasOwnProperty(key)) {
                    // filter each building against the building type array
                    if (inArrayCaseInsensitive(filteredBuildings[key][locationVar], selectedFilters['location']) === -1) {
                        // if the type is a match, add a new key to buildingOutput array (key = the building_code)
                        // and add the entire object to that key like it is in the original array
//                        console.log("filtered");
//                        buildingOutput[key] = filteredBuildings[key];
                        delete filteredBuildings[key];
                    }
                }
            }
            
        }
        
        
        if (selectedFilters['mapFilter'].length !== 0) {
            // loop through object
            for (var key in filteredBuildings) {
                if (filteredBuildings.hasOwnProperty(key)) {
//                    console.log("filteredBuildings[key]['building_code']");
//                    console.log(filteredBuildings[key]['building_code']);
//                    console.log("selectedFilters['mapFilter']");
//                    console.log(selectedFilters['mapFilter']);
                    // filter each building against the building type array
                    if (inArrayCaseInsensitive(filteredBuildings[key]['building_code'], selectedFilters['mapFilter']) === -1) {

                        delete filteredBuildings[key];
                    }
                }
            }
            
        }
        
//        console.log("suiteListAll before type filter");
//        console.log(suiteListAll);
//        console.log("selectedFilters['suite_type'] before type filter");
//        console.log(selectedFilters['suite_type']);
        

        // suite type filter
        if (selectedFilters['suite_type'].length !== 0) {
            // SUITE TYPE FILTER
            for (var key in filteredSuites) {
                //loop through object
                _.each(filteredSuites[key], function (skey) {
                    if (inArrayCaseInsensitive(skey.suite_type, selectedFilters['suite_type']) === -1) {
                        delete filteredSuites[key][skey.suite_id];
                    }
                });
            }
        }
        
//        console.log("filteredSuites after type filter");
//        console.log(filteredSuites);


        var filterMin = '';
        var filterMax = '';

        if (selectedFilters['suite_size'].length !== 0) {
            // SUITE SIZE FILTER
//            console.log("selectedFilters['suite_size']");
//            console.log(selectedFilters['suite_size']);

            for (var key in filteredSuites) {
                //loop through object
                _.each(filteredSuites[key], function (skey) {
                    // our filter values aren't integers yet
                    filterMin = parseInt(selectedFilters['suite_size'][0]);
                    filterMax = parseInt(selectedFilters['suite_size'][1]);
                    // filter suites by size
                    
                    var theArea = parseInt(skey.rawArea);
                    if (isNaN(theArea)){
                        theArea = 0;
                    }
                    var theContig = parseInt(skey.rawContigArea);
                    if (isNaN(theContig)){
                        theContig = 0;
                    }
                    var theDivis = parseInt(skey.rawDivisArea);
                    if (isNaN(theDivis)){
                        theDivis = 0;
                    }
                    
                    var areaArray = [parseInt(theArea),parseInt(theContig),parseInt(theDivis)];
                    var noZeroes = areaArray.filter(function(d) { return parseInt(d) !== 0; });
                                        
                    var suiteMinArea = Math.min.apply(Math, noZeroes);
                    var suiteMaxArea = Math.max.apply(Math, noZeroes);
                    
//                    console.log("Suite");
//                    console.log(skey);
//                    console.log("filterMin");
//                    console.log(filterMin);
//                    console.log("suiteMinArea");
//                    console.log(suiteMinArea);
//                    console.log("suiteMaxArea");
//                    console.log(suiteMaxArea);
//                    console.log("filterMax");
//                    console.log(filterMax);
//                    console.log("---------------");
//                    console.log("---------------");
                    
//                    ((alls.net_rentable_area BETWEEN ? and ?) OR (alls.contiguous_area BETWEEN ? and ?) OR (alls.min_divisible_area BETWEEN ? and ?))
//                    ((skey.net_rentable_area <= filterMin && skey.net_rentable_area >= filterMin ) || (skey.rawContigArea <= filterMin && skey.rawContigArea >= filterMin ) || (skey.rawDivisArea BETWEEN ? and ?))
//                    if ((suiteMinArea <= filterMin) || (suiteMaxArea >= filterMax)) {
                    if ((suiteMinArea <= filterMin || suiteMinArea >= filterMax) && (suiteMaxArea <= filterMin || suiteMaxArea >= filterMax)) {
                        delete filteredSuites[key][skey.suite_id];
                    }
                });
            }
            // only show office % filter if a size range has been selected
            $("#SuiteOfficeSize").removeClass("d-none");
        } else {
//                console.log("sizeDrop empty");
            $("#SuiteOfficeSize").addClass("d-none");
            $("#Size").val("All");
        }


        if (selectedFilters['suite_availability'].length !== 0) {
            // SUITE AVAILABILITY
            for (var key in filteredSuites) {
                //loop through object
                _.each(filteredSuites[key], function (skey) {
                    // our filter values aren't integers yet
                    var availMax = parseInt(selectedFilters['suite_availability'][0]);
                    var availMin = parseInt(selectedFilters['suite_availability'][1]);
                    // filter suites by availability
                    if ((skey.rawAvailability <= availMin) || (skey.rawAvailability >= availMax)) {
                        delete filteredSuites[key][skey.suite_id];
                    }
                });
            }
        } else {
            $("#Availability").val("All");
        }




        // THIS filter is only active and available when suite size has been selected
        // this is the same as AR3 and it DOES NOT work 100%
        if (selectedFilters['SuiteOfficeSize'].length !== 0) {
            // SUITE SIZE FILTER
//            console.log("selectedFilters['suite_size']");
//            console.log(selectedFilters['suite_size']);

            for (var key in filteredSuites) {
                //loop through object
                _.each(filteredSuites[key], function (skey) {
                    // our filter values aren't integers yet
                    var officeMin = parseInt(selectedFilters['SuiteOfficeSize'][0]);
                    var officeMax = parseInt(selectedFilters['SuiteOfficeSize'][1]);

                    // create decimal percentages of selected values 
                    var minpercentage = officeMin / 100;
                    var maxpercentage = officeMax / 100;
//                    console.log("minpercentage");
//                    console.log(minpercentage);
//                    console.log("maxpercentage");
//                    console.log(maxpercentage);

                    // determine actual values to test based on selected size filter
                    var min_office_space = filterMin * maxpercentage;
                    var max_office_space = filterMax * maxpercentage;
//                    console.log("min_office_space");
//                    console.log(min_office_space);
//                    console.log("max_office_space");
//                    console.log(max_office_space);
//                    
//                    
//                    console.log("skey.office_space");
//                    console.log(skey.office_space);
        
                    // filter suites by size
                    if ((skey.office_space <= min_office_space) || (skey.office_space >= max_office_space)) {
                        delete filteredSuites[key][skey.suite_id];
                    }
                });
            }
        }





        var clearHeightMin = '';
        var clearHeightMax = '';

        if (selectedFilters['SuiteClearHeight'].length !== 0) {
            // SUITE SIZE FILTER
//            console.log("selectedFilters['SuiteClearHeight']");
//            console.log(selectedFilters['SuiteClearHeight']);

            for (var key in filteredSuites) {
                //loop through object
                _.each(filteredSuites[key], function (skey) {
                    // our clearHeight values aren't integers yet
                    clearHeightMin = parseInt(selectedFilters['SuiteClearHeight'][0]);
                    // clearHeight suites by size
                var theClearHeight = skey.clear_height.replace("'","").replace("\"","");

                    if (selectedFilters['SuiteClearHeight'][0] === 0){
                        if (parseInt(theClearHeight) >= 10) {
                            delete filteredSuites[key][skey.suite_id];
                        }
                    } else {
                        if ((parseInt(theClearHeight) < clearHeightMin) || (theClearHeight === 'n/a')) {
                            delete filteredSuites[key][skey.suite_id];
                        }
                    }
                });
            }

        } else {
            $("#SuiteClearHeight").val("All");
        }




        var shippingDoorsMin = '';

        if (selectedFilters['SuiteShippingDoors'].length !== 0) {
            // SUITE SIZE FILTER
//            console.log("selectedFilters['SuiteShippingDoors']");
//            console.log(selectedFilters['SuiteShippingDoors']);

            for (var key in filteredSuites) {
                //loop through object
                _.each(filteredSuites[key], function (skey) {
                    // our shippingDoors values aren't integers yet
                    shippingDoorsMin = parseInt(selectedFilters['SuiteShippingDoors'][0]);
                    // shippingDoors suites by size
//                    console.log("skey.shipping_doors_truck");
//                    console.log(skey.shipping_doors_truck);
                    
                var theShippingDoors = skey.shipping_doors_truck;

                    if (selectedFilters['SuiteShippingDoors'][0] === 0){
                        if (parseInt(theShippingDoors) !== 0) {
                            delete filteredSuites[key][skey.suite_id];
                        }
                    } else {
                        if ((parseInt(theShippingDoors) < shippingDoorsMin) || (theShippingDoors === 'n/a')) {
                            delete filteredSuites[key][skey.suite_id];
                        }
                    }
                });
            }

        } else {
            $("#SuiteShippingDoors").val("All");
        }
        
        
        
        
        var driveinDoorsMin = '';

        if (selectedFilters['SuiteDriveInDoors'].length !== 0) {
            // SUITE SIZE FILTER
//            console.log("selectedFilters['SuiteDriveInDoors']");
//            console.log(selectedFilters['SuiteDriveInDoors']);

            for (var key in filteredSuites) {
                //loop through object
                _.each(filteredSuites[key], function (skey) {
                    // our driveinDoors values aren't integers yet
                    driveinDoorsMin = parseInt(selectedFilters['SuiteDriveInDoors'][0]);
                    // driveinDoors suites by size
//                    console.log("skey.shipping_doors_drive_in");
//                    console.log(skey.shipping_doors_drive_in);
                    
                var theDriveInDoors = skey.shipping_doors_drive_in;

                    if (selectedFilters['SuiteDriveInDoors'][0] === 0){
                        if (parseInt(theDriveInDoors) !== 0) {
                            delete filteredSuites[key][skey.suite_id];
                        }
                    } else {
                        if ((parseInt(theDriveInDoors) < driveinDoorsMin) || (theDriveInDoors === 'n/a')) {
                            delete filteredSuites[key][skey.suite_id];
                        }
                    }
                });
            }

        } else {
            $("#SuiteDriveInDoors").val("All");
        }


        var elecAmpsMin = '';
        var elecAmpsMax = '';

        if (selectedFilters['SuiteAvailElecAmps'].length !== 0) {
            // SUITE SIZE FILTER
//            console.log("selectedFilters['SuiteClearHeight']");
//            console.log(selectedFilters['SuiteClearHeight']);

            for (var key in filteredSuites) {
                //loop through object
                _.each(filteredSuites[key], function (skey) {
                    // our elecAmps values aren't integers yet
                    elecAmpsMin = parseInt(selectedFilters['SuiteAvailElecAmps'][0]);
                    // elecAmps suites by size
                var theElecAmps = skey.available_electrical_amps;

                    if (selectedFilters['SuiteAvailElecAmps'][0] === 0){
                        if (parseInt(theElecAmps) > 50) {
                            delete filteredSuites[key][skey.suite_id];
                        }
                    } else {
                        if ((parseInt(theElecAmps) < elecAmpsMin) || (theElecAmps === 'n/a')) {
                            delete filteredSuites[key][skey.suite_id];
                        }
                    }
                });
            }

        } else {
            $("#SuiteAvailElecAmps").val("All");
        }
        
        
        
        
        
        var elecVoltsMin = '';
        var elecVoltsMax = '';

        if (selectedFilters['SuiteAvailElecVolts'].length !== 0) {
            // SUITE SIZE FILTER
//            console.log("selectedFilters['SuiteClearHeight']");
//            console.log(selectedFilters['SuiteClearHeight']);

            for (var key in filteredSuites) {
                //loop through object
                _.each(filteredSuites[key], function (skey) {
                    // our elecVolts values aren't integers yet
                    elecVoltsMin = parseInt(selectedFilters['SuiteAvailElecVolts'][0]);
                    // elecVolts suites by size
                var theElecVolts = skey.available_electrical_volts;

                    if (selectedFilters['SuiteAvailElecVolts'][0] === 0){
                        if (parseInt(theElecVolts) > 120) {
                            delete filteredSuites[key][skey.suite_id];
                        }
                    } else {
                        if ((parseInt(theElecVolts) < elecVoltsMin) || (theElecVolts === 'n/a')) {
                            delete filteredSuites[key][skey.suite_id];
                        }
                    }
                });
            }

        } else {
            $("#SuiteAvailElecVolts").val("All");
        }
        
        
        
        
        
        
        
        var surfaceStallsMin = '';
        var surfaceStallsMax = '';

        if (selectedFilters['SuiteParkingSurfaceStalls'].length !== 0) {
            // SUITE SIZE FILTER
//            console.log("selectedFilters['SuiteClearHeight']");
//            console.log(selectedFilters['SuiteClearHeight']);

            for (var key in filteredSuites) {
                //loop through object
                _.each(filteredSuites[key], function (skey) {
                    // our surfaceStalls values aren't integers yet
                    surfaceStallsMin = parseInt(selectedFilters['SuiteParkingSurfaceStalls'][0]);
                    surfaceStallsMax = parseInt(selectedFilters['SuiteParkingSurfaceStalls'][1]);
                    // surfaceStalls suites by size
                var theSurfaceStalls = skey.surface_stalls;
                    if ((parseInt(theSurfaceStalls) < surfaceStallsMin) || (parseInt(theSurfaceStalls) > surfaceStallsMax) || (theSurfaceStalls === '')) {
                        delete filteredSuites[key][skey.suite_id];
                    }
                });
            }

        } else {
            $("#SuiteParkingSurfaceStalls").val("All");
        }
        



// clean buildings with no suites out of filteredSuites
_.each(filteredSuites, function(v,k) {
    if (_.isEmpty(v)) {
        delete filteredSuites[k];
    }
});

//console.log("filteredSuites after emptying");
//console.log(filteredSuites);

filtersNotEmpty = 0;

_.each(selectedFilters, function(v,k) {
    if (v.length !== 0) {
        filtersNotEmpty++;
    }
});



        // Build new array of objects to pass to the rest of the page
        changedMarkers = [];
        changedMarkers.push(filteredBuildings);
        changedMarkers.push(filteredSuites);



//console.log("selectedFilters");
//console.log(selectedFilters);
//
//console.log("changedMarkers");
//console.log(changedMarkers);


        // UPDATED OCT 23: If no changes, then send an empty list to be dealt with by the template
        // if no changes
        if (filtersNotEmpty === 0) {
            // load full list of results
            $(".fs_search-results").empty();
            generateHTML(buildingList);
//            theMap.removeMarkers();
//            addPoints(buildingList[0], theMap);
            mapMarkers(buildingList[0]);
            filterChips(selectedFilters);
//            updateMap();
            updateUrl();
            
//            updateSizeRange(buildingList, "first");
        } else {
            // load changed list of results
            $(".fs_search-results").empty();
            generateHTML(changedMarkers);
//            theMap.removeMarkers();
            // do not add markers if there are no markers
            if (!_.isEmpty(changedMarkers[0])) {
//                addPoints(changedMarkers[0], theMap);
                if (mapLiveInteraction === 0) {
                    mapMarkers(changedMarkers[0]);
                }
            }
            filterChips(selectedFilters);
//            updateMap();
            updateUrl();
            
//            updateSizeRange(changedMarkers, "notFirst");
        }
    }


//--------------------
// Part Three: Take filtered JSON and feed it to the html template 
//--------------------

// push the JSON to the html template
    function generateHTML(theMarkers) {

//    console.log("theMarkers top");
//    console.log(theMarkers);



        // Process markersToRemove every time the HTML is loaded
        // this ensures that any X'd out buildings are still out of the results until a full reset
        if (markersToRemove.length !== 0) {
            
            var z = 0;
            _.each(theMarkers[0], function (i) {
              //  console.log("i");
               // console.log(i);
                // remove any results that have been X'd out of the listings
                if (markersToRemove.indexOf(i.building_code) > -1) {
                    delete theMarkers[0][i.building_code];
                }
                z++;
            });
        }




        // "showVacancies" is set at page load time in the global variables section
        // based on the ar's theme database flag
        // the checkbox in the filter flips this between true and false
        // works great.
//        console.log("showVacancies inside template before test");
//        console.log(showVacancies);
        if (!showVacancies) {
        
            var tempSuites = [];
            // get all keys (building_codes) from the suite records
            _.each(theMarkers[1], function (v, k) {
                tempSuites.push(k);
            });

            // loop through the buildings
            _.each(theMarkers[0], function (i) {
                // IF a building_code doesn't match up with the suite keys (building_codes)
                if (tempSuites.indexOf(i.building_code) === -1) {
                    // remove the building from the building side of the object
                    delete theMarkers[0][i.building_code];
                }
            });
        
        }

//        console.log("theMarkers before template prep");
//        console.log(theMarkers);

        // prepare data for template
        var data = {target: theMarkers};

        // This needs to be CSS'd
        var noSuitesFound;
        
        if (theLanguage == "en_CA"){
            noSuitesFound = '<div><h2>No buildings match your search!</h2>';
            noSuitesFound += '<p>Please broaden your search or try another combination.</p></div>';
        } else {
            noSuitesFound = "<div><h2>Pas de bàtiment correspondant à votre recherche.</h2>";
            noSuitesFound += "<p>S'il vous plaît élargir votre recherche ou essayez une autre combinaison.</p></div>";        
        }


        if (_.isEmpty(data['target'][0])) {
            // push No Suites Found message
            $(".fs_search-results").append(noSuitesFound);
            
            
            // intialize clicks
            $('.fs_btn-favourite').on('click', faveButton);
            $('.fs_search-result-remove').on('click', filterFunc);
            $('.fs_search-result-remove-grid').on('click', filterFunc);
            // count the suites and update the results number
            var suiteCount = $("tr.suitesList").length;
            $(".suitescounted").empty();
            $(".suitescounted").text(suiteCount);
        } else {
            // send data to template
            var template = _.template($("#ourHTML").text());
            $(".fs_search-results").append(template(data));


            // intialize clicks
            $('.fs_btn-favourite').on('click', faveButton);
            $('.fs_search-result-remove').on('click', filterFunc);
            $('.fs_search-result-remove-grid').on('click', filterFunc);
            // count the suites and update the results number
            var suiteCount = $("tr.suitesList").length;
            $(".suitescounted").empty();
            $(".suitescounted").text(suiteCount);

        }

    }


//--------------------
// Part Four: Build the map and feed the same filtered JSON to it - it only needs building level info
// 
//--------------------    
//    It's maptime
// count suites for map markers
function suiteCount(buildingCode) {

    var numSuites;
    
    if (filtersNotEmpty === 1) {
//        buildingList[1]
        var theObject = buildingList[1][buildingCode];
        if (typeof theObject !== "undefined") {
            numSuites = Object.keys(theObject).length;
        } else {
            numSuites = 0;
        }
    } 
    else {
////        changedMarkers[1]
        var theObject = changedMarkers[1][buildingCode];
        if (typeof theObject !== "undefined") {
            numSuites = Object.keys(theObject).length;
        } else {
            numSuites = 0;
        }
    }
    return numSuites;
}

function initialize() {
    
    // add "opacity" to the map
    // we really just add gamma.
    // Need JY to review this
    var styledMapType = new google.maps.StyledMapType(
        [
            {
                "featureType": "all",
                "elementType": "all",
                "stylers": [
                    {
                        "gamma": "2.50"
                    }
                ]
            }
        ],
        {name: 'Styled Map'}
    );    
    

    
  map = new google.maps.Map(document.getElementById('themap'), {
    center: {lat: 58.665904, lng: -97.216165},
    zoom: 3
  });
  
    map.mapTypes.set('styled_map', styledMapType);
    map.setMapTypeId('styled_map');
  
}
initialize();

var markers = [];
var theMarkerArray = [];
var markerCluster;
    
    function mapMarkers(data) {
//        console.log("mapMarkers triggered");
        var bounds = new google.maps.LatLngBounds();
//        setMapOnAll(null);
//        console.log("mapMarkers data");
//        console.log(data);
//        console.log("markers");
//        console.log(theMarkerArray);
        for (var i = 0; i < theMarkerArray.length; i++) {
          theMarkerArray[i].setMap(null);
        }
        markers = [];
        theMarkerArray = [];
        var infoWindowContent = [];
        

        $.each(data, function (k, v) {
            var buildingName = v['building_name'];
            var buildingType = v['building_type'];
            var streetAddress = v['street_address'];
            var city = v['city'];
            var province = v['province'];
            var postal = v['pc'];
            var buildingCode = v['building_code'];
            var thumbnail = v['thumbnailList'];
            var latitude = v['latitude'];
            var longitude = v['longitude'];
            
            var suiteTextOutput = '';
            
            
            var numSuites = suiteCount(buildingCode);
            
            if (theLanguage === "fr_CA") {
                if (numSuites === 0) {
                    if (buildingType === "Retail" || buildingType == "Détail"){
                        suiteTextOutput = "Appelez-nous pour connaître les espaces disponible";
                    } else {
                        suiteTextOutput = "Ce bâtiment est actuellement loué à 100%";
                    }
                } else {
                    suiteTextOutput = numSuites + " Bureaux disponibles";
                }
            } else {
                if (numSuites === 0) {
                    if (buildingType === "Retail" || buildingType == "Détail"){
                        suiteTextOutput = "Call For Availability";
                    } else {
                        suiteTextOutput = "This building is currently 100% leased";
                    }
                } else {
                    suiteTextOutput = numSuites + " Suites Available";
                }
            }
            
            
            markers.push([buildingName, latitude, longitude, buildingCode, numSuites]);
            
            if (buildingName === streetAddress) {
                buildingName = "";
            } else {
                buildingName = '<span class="fs_iw-building-name">' + buildingName + '</span><br>';
            }
            infoWindowContent.push(['<div class="fs_infoWindow">' +
                    '<span class="fs_iw-close"><button type="button" class="fs_btn-iw-close"><i class="material-icons fs_icon-iw-close">clear</i></button></span><div class="fs_iw-image"><a class="fs_building-link" href="building-integrated.php?building='+buildingCode+'"><img alt="" src="' + thumbnail + '"></a></div>' +
                    '<div class="fs_iw-building">' + buildingName + '<span class="fs_iw-building-address">' + streetAddress + '</span><br><span class="fs_iw-building-city">' + city + '</span>, <span class="fs_iw-building-province">' + province + '</span><br><span class="fs_iw-building-postal-code">' + postal + '</span></div>' +
                    '<div class="fs_iw-available"><a class="fs_map-suites-link" href="building-integrated.php?building='+buildingCode+'">'+suiteTextOutput+'</div>' +
                    '</div>']);
        });
        
//        console.log("Markers array is built");
//        console.log(markers);
        
        
    // Multiple Markers
//    var markers = [
//        ['London Eye, London', 51.503454,-0.119562],
//        ['Palace of Westminster, London', 51.499633,-0.124755]
//    ];
                        
    // Info Window Content
//    var infoWindowContent = ['<div class="infoWindow">' +
//                    '<div class="iwImage"><img alt="thumnail" src="' + thumbnail + '"></div>' +
//                    '<div class="iwBuilding">' + buildingName + '<span class="iwBuildingAddress">' + streetAddress + '</span><br><span class="iwBuildingCity">' + city + '</span>, <span class="iwBuildingProvince">' + province + '</span><br><span class="iwBuildingPostalCode">' + postal + '</span></div>' +
//                    '<div class="iwAvailable">Test</div>' +
//                    '</div>'];
        
    // Display multiple markers on a map
    infoWindow = new google.maps.InfoWindow();
    var marker;
    var i;
    
    // Loop through our array of markers & place each one on the map  
    for( i = 0; i < markers.length; i++ ) {
        var markerNumber = markers[i][4].toString();
        if (markerNumber == '0') {
            markerNumber = ' ';
        }
        var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
        bounds.extend(position);
        marker = new google.maps.Marker({
            position: position,
            map: map,
            title: markers[i][0],
            bcode: markers[i][3],
            icon: 'theme/fs_marker-icon.png',
//            label: markers[i][4].toString()
            label: {
                text: markerNumber,
                color: "#fff",
                fontSize: "12px",
                fontWeight: "bold"
            }
        });
    theMarkerArray.push(marker);
        // Allow each marker to have an info window    
        google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
                infoWindow.setContent(infoWindowContent[i][0]);
                infoWindow.open(map, marker);
                // scroll results div to show the
                // clicked building
                //$('.fs_search-results').animate({scrollTop: $('.fs_search-results').scrollTop() + ($('.fs_search-result[data-buildingcode="'+marker.bcode+'"]').offset().top - $('.fs_search-results').offset().top)});
                //if(!isMobile) {
                    //$('.fs_search-results').mCustomScrollbar("scrollTo",'.fs_search-result[data-buildingcode="'+marker.bcode+'"]');
                //}
                //else {
                    $('.fs_search-results').animate({scrollTop: $('.fs_search-results').scrollTop() + ($('.fs_search-result[data-buildingcode="'+marker.bcode+'"]').offset().top - $('.fs_search-results').offset().top)});
                //}
            };
        })(marker, i));

        
    }
//    console.log("typeof markerCluster");
//    console.log(typeof markerCluster);
//    var markerCluster
           if (typeof markerCluster === "undefined") {
//               console.log("markerCluster undefined");
           } else {
//               console.log("clearing markers");
               markerCluster.clearMarkers();
           }
    
   
    
        
//        // Add a marker clusterer to manage the markers.
  markerCluster = new MarkerClusterer(map, theMarkerArray, {
    imagePath: 'theme/m'
  });
//    
    
                /*
            * The google.maps.event.addListener() event waits for
            * the creation of the infowindow HTML structure 'domready'
            * and before the opening of the infowindow defined styles
            * are applied.
            */
//    map.addListener('domready', function() {
           google.maps.event.addListener(infoWindow, 'domready', function() {
              
              // Reference to the DIV which receives the contents of the infowindow using jQuery
              var iwOuter = $('.gm-style-iw');
              
              //reset width and height of parent div to better position assets (eg. close button)
              //iwOuter.parent().css({'width' : '225px', 'height' : '125px'});
              
              //iwOuter.parent().addClass('infowindow-parent');

              /* The DIV we want to change is above the .gm-style-iw DIV.
               * So, we use jQuery and create a iwBackground variable,
               * and took advantage of the existing reference to .gm-style-iw for the previous DIV with .prev().
               */
              var iwBackground = iwOuter.prev();

              // Remove the background shadow DIV
              iwBackground.children(':nth-child(2)').css({'display' : 'none'});

              // Remove the white background DIV
              iwBackground.children(':nth-child(4)').css({'display' : 'none'});
              
              
              
              // Moves the infowindow 115px to the right.
              //iwOuter.parent().parent().css({left: '115px'});
              
              // Moves the shadow of the arrow 76px to the left margin 
                //iwBackground.children(':nth-child(1)').attr('style', function(i,s){ return s + 'left: 76px !important;'});
                
                //hide the shadow of the arrow
                iwBackground.children(':nth-child(1)').css({'display' : 'none'});
                
                //hide the arrow
                iwBackground.children(':nth-child(3)').css({'display' : 'none'});

                // Moves the arrow 76px to the left margin 
                //iwBackground.children(':nth-child(3)').attr('style', function(i,s){ return s + 'left: 76px !important;'});
                
                
                // selector for info window close button
                var iwCloseBtn = iwOuter.next();
                
                //hide the close button since we are adding our own custom close to infowindow
                iwCloseBtn.css('display', 'none');

                // Apply the desired effect to the close button
                //iwCloseBtn.css({
                 // opacity: '1', // by default the close button has an opacity of 0.7
                  //right: '0px', top: '0px' // button repositioning
                  //border: '7px solid #48b5e9', // increasing button border and new color
                  //'border-radius': '13px', // circular effect
                  //'box-shadow': '0 0 5px #3990B9' // 3D effect to highlight the button
                  //});

                // The API automatically applies 0.7 opacity to the button after the mouseout event.
                // This function reverses this event to the desired value.
                //iwCloseBtn.mouseout(function(){
                //  $(this).css({opacity: '1'});
                //});

           });
    
    
    
    
    
    
    if (mapLiveInteraction === 0) {
//        console.log("mapLiveInteraction in mapMarkers");
//        console.log(mapLiveInteraction);
//        console.log("mouseIn in mapMarkers");
//        console.log(mouseIn);
//        console.log("mouseOut in mapMarkers");
//        console.log(mouseOut);
        // Automatically center the map fitting all markers on the screen
//        console.log("Fitbounds");
        map.fitBounds(bounds);
    }
}


// manipulating the map
$('body').on('click', '.fs_btn-iw-close', function(e) {
//    map.hideInfoWindows();
    infoWindow.close();
    
});


var mapLiveInteraction = 0;
var mouseOut = 1;
var mouseIn = 0;
    map.addListener('mouseover', function() {
//        console.log("mouseover");
        mapLiveInteraction = 1;
        mouseOut = 0;
        mouseIn = 1;
    });
    
    map.addListener('mousemove', function() {
//        console.log("mousemove");
        mapLiveInteraction = 1;
    });

//    map.addListener('mouseout', function() {
//        console.log("mouseout");
//        mapLiveInteraction = 0;
//    });
    
    // In their infinite wisdom, Google triggers "mouseout"
    // when you click on any map control. Zoom in/out for eg.
    // so we have to resort to a DOM event "mouseleave" when the mouse
    // leaves the map's canvas div (#themap)
    $(map.getDiv()).mouseleave(function(){
//        console.log('mouseleave');
        mapLiveInteraction = 0;
        mouseOut = 1;
        mouseIn = 0;
    });

    map.addListener('dragend', function() {
        if (mapLiveInteraction === 1) {
//            console.log('dragend');
            mapMoved();
        }

    });

    map.addListener('zoom_changed', function() {
        if (mapLiveInteraction === 1 && mouseOut === 0) {
//            console.log('zoom_changed');
            setTimeout(function () {
//            theMap.setCenter((mapcenterLa + 0.0000001), mapcenterLg);
                mapMoved();
            }, 50);
        }

    });
    
//    var mapHash;
    map.addListener('bounds_changed', function() {
//        if (mapLiveInteraction === 1) {
//            console.log('bounds_changsed');
//            mapHash ='lat='+this.getCenter().lat().toFixed(6)+
//                    '&lng='+this.getCenter().lng().toFixed(6)+
//                    '&zoom='+this.getZoom();
////            history.pushState(null, null, mapHash);
//            
//            if (manualStateChange === false) {
//                manualStateChange = false;
//        //            console.log("updateURL History.pushState FIRES NOW");
//        //            history.pushState(null, null, newUrl);
//                var mapUrlStart = new URL(window.location.href);
//                var mapUrl = mapUrlStart +'/'+ mapHash;
//                History.pushState(null, null, mapUrl);
//                
//            }
//        }
        
    });




///*
// * The google.maps.event.addListener() event waits for
// * the creation of the infowindow HTML structure 'domready'
// * and before the opening of the infowindow defined styles
// * are applied.
// */

//google.maps.event.addListener(infoWindow, 'domready', function() {
////    console.log('gmaps domready');
//   // Reference to the DIV which receives the contents of the infowindow using jQuery
//   var iwOuter = $('.gm-style-iw');
//
//   /* The DIV we want to change is above the .gm-style-iw DIV.
//    * So, we use jQuery and create a iwBackground variable,
//    * and took advantage of the existing reference to .gm-style-iw for the previous DIV with .prev().
//    */
//   var iwBackground = iwOuter.prev();
//
//   // Remove the background shadow DIV
//   iwBackground.children(':nth-child(2)').css({'display' : 'none'});
//
//   // Remove the white background DIV
//   iwBackground.children(':nth-child(4)').css({'display' : 'none'});
//
//});











//--------------------
// Part Five: Set the filter chips
//--------------------


    function filterChips(selectedFilters) {
//        updateSizeRange(changedMarkers);
//        console.log("selected filters filterChips");
//        console.log(selectedFilters);
        var newChip = '';
        _.each(selectedFilters, function (v, k) {
//        console.log("each selected filter k");
//        console.log(k);
//        console.log("each selected filter v");
//        console.log(v);
            if (v != '') {
                if (k === "suite_size") {
                    // test filter and remove 'default' size filter if present
                    // create a temp array of the min and max size values currently active
//                    console.log("the suite_size V");
//                    console.log(v);
                    if (v[0] === 200000) {
                        v[0] = 200000;
                        if (theLanguage === "fr_CA"){
                            v[1] = " et plus"
                        } else {
                            v[1] = " and above";
                        }
                        newChip += '<div class="fs_chip"><span class="fs_chip-text">' + v[0] + ' - ' + v[1] + '</span> <button type="button" class="fs_chip-remove" data-filtertype="' + k + '" data-filterval="' + v + '" aria-label="Close"><i class="material-icons fs_icon-chip">clear</i></button></div>';
                    } else {
                        newChip += '<div class="fs_chip"><span class="fs_chip-text">' + v[0] + ' - ' + v[1] + ' sqft</span> <button type="button" class="fs_chip-remove" data-filtertype="' + k + '" data-filterval="' + v + '" aria-label="Close"><i class="material-icons fs_icon-chip">clear</i></button></div>';
                    }
                    

                } else if (k === "suite_type") {
                    var typeChip = v.toString().toLowerCase();
                    if (theLanguage === "fr_CA"){
                        typeChip = typeLanguage(typeChip);
                    }
                    newChip += '<div class="fs_chip"><span class="fs_chip-text">' + typeChip + '</span> <button type="button" class="fs_chip-remove" data-filtertype="' + k + '" data-filterval="' + typeChip + '" aria-label="Close"><i class="material-icons fs_icon-chip">clear</i></button></div>';

                } else if (k === "location") {
//                    console.log("the location V");
//                    console.log(v);
                    var theText = v[1];
                    theText = theText.replace(/%20/g, " ");
                    newChip += '<div class="fs_chip"><span class="fs_chip-text">' + theText + '</span> <button type="button" class="fs_chip-remove" data-filtertype="' + k + '" data-filterval="' + v + '" aria-label="Close"><i class="material-icons fs_icon-chip">clear</i></button></div>';
                } else if (k === "suite_availability") {
//                    console.log("the availability V");
//                    console.log(v);
                    var theSetAvail = $("#Availability").val();
//                    console.log("theSetAvail");
//                    console.log(theSetAvail);
                    var theText = theSetAvail.toString();
                    theText = theText.replace(/%20/g, " ");
                    if (theLanguage === "fr_CA"){
                        theText = translateAvailability(theText)
                    }
                    newChip += '<div class="fs_chip"><span class="fs_chip-text">' + theText + '</span> <button type="button" class="fs_chip-remove" data-filtertype="' + k + '" data-filterval="' + v + '" aria-label="Close"><i class="material-icons fs_icon-chip">clear</i></button></div>';
                } else if (k === "SuiteOfficeSize") {
                    
                } else if (k === "SuiteClearHeight") {
//                    console.log("the SuiteClearHeight V");
//                    console.log(v);
                    var chOutput = '';
                    if (parseInt(v) === 0){
                        if (theLanguage === "fr_CA"){
                            chOutput = "moins de 10'";
                        } else {
                            chOutput = "Below 10'";
                        }
                    } else {
                        chOutput = "Min " + v.toString()+ "'";
//                console.log("chOutput");
//                console.log(chOutput);
                        
                }
                    newChip += '<div class="fs_chip"><span class="fs_chip-text">' + chOutput + '</span> <button type="button" class="fs_chip-remove" data-filtertype="' + k + '" data-filterval="' + v + '" aria-label="Close"><i class="material-icons fs_icon-chip">clear</i></button></div>';
                
                } else if (k === "SuiteShippingDoors") {
//                    console.log("the SuiteShippingDoors V");
//                    console.log(v);
                    var sdOutput = '';
                    var sdSuffix = '';
                    if (parseInt(v) === 0){
                        sdOutput = "No Doors";
                        if (theLanguage === "fr_CA") {
                            sdOutput = "pas de portes de livraison";
                        }
                    } else {
                        if (theLanguage === "fr_CA") {
                            sdSuffix = " portes de livraison";
                        } else {
                            sdSuffix = " Shipping Doors";
                        }
                        sdOutput = "Min " + v.toString() + sdSuffix;
//                console.log("sdOutput");
//                console.log(sdOutput);
                        
            }
                    newChip += '<div class="fs_chip"><span class="fs_chip-text">' + sdOutput + '</span> <button type="button" class="fs_chip-remove" data-filtertype="' + k + '" data-filterval="' + v + '" aria-label="Close"><i class="material-icons fs_icon-chip">clear</i></button></div>';        
                    
                    
                    
                } else if (k === "SuiteDriveInDoors") {
//                    console.log("the SuiteDriveInDoors V");
//                    console.log(v);
                    var diOutput = '';
                    var diSuffix = '';
                    if (parseInt(v) === 0){
                        if (theLanguage === "fr_CA") {
                            diOutput = " Pas de portes au sol";
                        } else {
                            diOutput     = "No Drive-In Doors";
                        }
                    } else {
                        if (theLanguage === "fr_CA") {
                            diSuffix = " Portes au sol";
                        } else {
                            diSuffix = " Drive-in doors";
                        }
                        diOutput = "Min " + v.toString() + diSuffix;
//                console.log("diOutput");
//                console.log(diOutput);
                        
                    }
                    newChip += '<div class="fs_chip"><span class="fs_chip-text">' + diOutput + '</span> <button type="button" class="fs_chip-remove" data-filtertype="' + k + '" data-filterval="' + v + '" aria-label="Close"><i class="material-icons fs_icon-chip">clear</i></button></div>';        

                } else if (k === "SuiteAvailElecAmps") {
//                    console.log("the SuiteAvailElecAmps V");
//                    console.log(v);
                    var ampOutput = '';
                    if (parseInt(v) === 0){
                        if (theLanguage === "fr_CA"){
                            ampOutput = "moins de 50A";
                        } else {
                            ampOutput = "Less than 50A";
                        }
                    } else {
                        ampOutput = "Min " + v.toString()+ "A";
//                console.log("ampOutput");
//                console.log(ampOutput);
                        
                    }
                    newChip += '<div class="fs_chip"><span class="fs_chip-text">' + ampOutput + '</span> <button type="button" class="fs_chip-remove" data-filtertype="' + k + '" data-filterval="' + v + '" aria-label="Close"><i class="material-icons fs_icon-chip">clear</i></button></div>';        
                } else if (k === "SuiteAvailElecVolts") {
//                    console.log("the SuiteAvailElecVolts V");
//                    console.log(v);
                    var voltOutput = '';
                    if (parseInt(v) === 0){
                        voltOutput = "Max 120V";
                    } else {
                        voltOutput = "Min " + v.toString()+ "V";
//                console.log("voltOutput");
//                console.log(voltOutput);
                        
                    }
                    newChip += '<div class="fs_chip"><span class="fs_chip-text">' + voltOutput + '</span> <button type="button" class="fs_chip-remove" data-filtertype="' + k + '" data-filterval="' + v + '" aria-label="Close"><i class="material-icons fs_icon-chip">clear</i></button></div>';        
                    
                } else if (k === "SuiteParkingSurfaceStalls") {
                    var ssSuffix = '';
                    if (theLanguage === "fr_CA") {
                        ssSuffix = " Places de parking";
                    } else {
                        ssSuffix = " Surface Stalls";
                    }

                    newChip += '<div class="fs_chip"><span class="fs_chip-text">' + v[0] + ' - ' + ssSuffix + '</span> <button type="button" class="fs_chip-remove" data-filtertype="' + k + '" data-filterval="' + v + '" aria-label="Close"><i class="material-icons fs_icon-chip">clear</i></button></div>';

                    
                } else if (k === "SuiteTruckTrailerParking") {
//                    console.log("SuiteTruckTrailerParking V");
//                    console.log(v);
                    if (parseInt(v) === 1){
                        var ttParking = '';
                        if (theLanguage === "fr_CA") {
                            ttParking = "Parking de camions disponible";
                        } else {
                            ttParking = "Truck parking available";
                        }
                        newChip += '<div class="fs_chip"><span class="fs_chip-text">'+ttParking+'</span> <button type="button" class="fs_chip-remove" data-filtertype="' + k + '" data-filterval="Yes" aria-label="Close"><i class="material-icons fs_icon-chip">clear</i></button></div>';
                    } else {
                        var ttParking = '';
                        if (theLanguage === "fr_CA") {
                            ttParking = "Pas de parking de camion disponible";
                        } else {
                            ttParking = "No truck parking available";
                        }
                        newChip += '<div class="fs_chip"><span class="fs_chip-text">'+ttParking+'</span> <button type="button" class="fs_chip-remove" data-filtertype="' + k + '" data-filterval="No" aria-label="Close"><i class="material-icons fs_icon-chip">clear</i></button></div>';
                    }
                }
                
                
                
                
                
                
            }
        });

//    var newChip = '<div class="chip"><span class="chip-text">Office</span> <button type="button" class="chip-close close" aria-label="Close"><i class="material-icons chip-icon">clear</i></button></div>';
        $("div.fs_thechips").empty();
        $("div.fs_thechips").html(newChip);
        $('.fs_chip-remove').on('click', closeChip);
        $(".fs_chip-text").css("text-transform", "capitalize");
    } // end function filterChips


//--------------------
// Filter Chip X button
//--------------------

    function closeChip() {
//        console.log("filtertype");
//        console.log($(this).data("filtertype"));
//        console.log("filterval");
//        console.log($(this).data("filterval"));
//        console.log("selected filters");
//        console.log(selectedFilters);
        var filterType = $(this).data("filtertype");
        var filterValue = $(this).data("filterval");
//        console.log("filterType");
//        console.log(filterType);
//        console.log("filterValue tolowercase");
//        console.log(filterValue.toLowerCase());

        manualStateChange = false;
        // uncheck a checkbox
        if (filterType === "suite_type") {
            // fire a click event on the checkbox that's set
            // the checkbox code takes care of everything else
            var lowercaseFilterValue = filterValue.toLowerCase();
            
            if (theLanguage === "fr_CA"){
                lowercaseFilterValue = chipLanguage(lowercaseFilterValue);
            }
            
            $('#' + lowercaseFilterValue + '').trigger('click');

        }
        if (filterType === "suite_size") {
//            console.log("size chip clicked");
            // clear the size sorting from the filter
            selectedFilters['suite_size'] = [];
            // reset the sliders
            $("#Size").val("All");
            // run filterFunc()
            filterFunc();
        }
        if (filterType === "location") {
//            console.log("location chip clicked");
            selectedFilters['location'] = [];
            $("#property-search").val("");
            filterFunc();
        }
        if (filterType === "suite_availability") {
//            console.log("location chip clicked");
            selectedFilters['suite_availability'] = [];
            $("#Availability").val("All");
            filterFunc();
        }
        if (filterType === "SuiteClearHeight") {
//            console.log("location chip clicked");
            selectedFilters['SuiteClearHeight'] = [];
            $("#SuiteClearHeight").val("All");
            filterFunc();
    }
        if (filterType === "SuiteShippingDoors") {
//            console.log("location chip clicked");
            selectedFilters['SuiteShippingDoors'] = [];
            $("#SuiteShippingDoors").val("All");
            filterFunc();
        }
        if (filterType === "SuiteDriveInDoors") {
//            console.log("location chip clicked");
            selectedFilters['SuiteDriveInDoors'] = [];
            $("#SuiteDriveInDoors").val("All");
            filterFunc();
        }
        if (filterType === "SuiteAvailElecAmps") {
//            console.log("location chip clicked");
            selectedFilters['SuiteAvailElecAmps'] = [];
            $("#SuiteAvailElecAmps").val("All");
            filterFunc();
        }
        if (filterType === "SuiteAvailElecVolts") {
//            console.log("location chip clicked");
            selectedFilters['SuiteAvailElecVolts'] = [];
            $("#SuiteAvailElecVolts").val("All");
            filterFunc();
        }
        if (filterType === "SuiteParkingSurfaceStalls") {
//            console.log("location chip clicked");
            selectedFilters['SuiteParkingSurfaceStalls'] = [];
            $("#SuiteParkingSurfaceStalls").val("All");
            filterFunc();
        }
        if (filterType === "SuiteTruckTrailerParking") {
//            console.log("location chip clicked");
            selectedFilters['SuiteTruckTrailerParking'] = [];
            $("#SuiteTruckTrailerParking").val("All");
            filterFunc();
        }
    }



//--------------------
// Part Six: Process and push the new filtered URL
//--------------------




// Update the URL
// 

    function url_domain(data) {
        var a = document.createElement('a');
        a.href = data;
        return a.hostname;
    }
    // pretty it up

    function updateUrl() {
//console.log("update url has begun");
//console.log(selectedFilters);

        queryString = '';
        var x = 0;

            if (selectedFilters['location'].length !== 0) {
                if (x > 0) {
                    queryString += "&";
                } else if (x === 0) {
                    queryString += "?";
                }
                queryString += selectedFilters['location'][0] + '=' + selectedFilters['location'][1];
                x++;
            }
            
            if (selectedFilters['suite_type'].length !== 0) {
                if (x > 0) {
                    queryString += "&";
                } else if (x === 0) {
                    queryString += "?";
                }
                var urlType = selectedFilters['suite_type'].toString().toLowerCase();
                queryString += 'SuiteType=' + urlType;
                x++;
            } 
            
            if (selectedFilters['suite_size'].length !== 0) {
                if (x > 0) {
                    queryString += "&";
                } else if (x === 0) {
                    queryString += "?";
                }
//                if (selectedFilters['suite_size'][1] == 2147483646) {
//                    selectedFilters['suite_size'][0] = 200000;
//                    selectedFilters['suite_size'][1] = " and above";
//                }
//                
//                console.log("selectedFilters['suite_size'][0]");
//                console.log(selectedFilters['suite_size'][0]);
//                
//                var sizeMin = selectedFilters['suite_size'][0].toLocaleString(
//                    "en", // use a string like 'en-US' to override browser locale
//                    { minimumFractionDigits: 0 }
//                );
//        
//                console.log("sizeMin");
//                console.log(sizeMin);
//        
//                var sizeMax = selectedFilters['suite_size'][1].toLocaleString(
//                    "en", // use a string like 'en-US' to override browser locale
//                    { minimumFractionDigits: 0 }
//                );
                
        
                var theSize = $("select#Size").val();
            console.log("theSize");    
            console.log(theSize);
                queryString += 'Size=' + theSize;
                
//                queryString += 'Size=' + sizeMin + ',' + sizeMax;
                x++;
            } 
            
            if (selectedFilters['suite_availability'].length !== 0) {
                if (x > 0) {
                    queryString += "&";
                } else if (x === 0) {
                    queryString += "?";
                }
                var theSetAvail = $("#Availability").val();
                var theText = theSetAvail;
                theText = theText.replace(/%20/g, " ");
                queryString += 'Availability=' + theText;
                x++;
            } 
            
            if (selectedFilters['lang'].length !== 0) {
                if (x > 0) {
                    queryString += "&";
                } else if (x === 0) {
                    queryString += "?";
                }
                queryString += 'lang=' + selectedFilters['lang'];
                x++;
            } 
            
            if (selectedFilters['SuiteClearHeight'].length !== 0) {
                if (x > 0) {
                    queryString += "&";
                } else if (x === 0) {
                    queryString += "?";
                }
                if (parseInt(selectedFilters['SuiteClearHeight']) === 0){
                    queryString += 'SuiteClearHeight=' + "below10";
            } else {
                    queryString += 'SuiteClearHeight=' + "min"+selectedFilters['SuiteClearHeight'];
                }
                x++;
            } 

            if (selectedFilters['SuiteShippingDoors'].length !== 0) {
                if (x > 0) {
                    queryString += "&";
                } else if (x === 0) {
                    queryString += "?";
            }
                if (parseInt(selectedFilters['SuiteShippingDoors']) === 0) {
                    queryString += 'SuiteShippingDoors='+selectedFilters['SuiteShippingDoors'];
                } else {
                    queryString += 'SuiteShippingDoors=min'+selectedFilters['SuiteShippingDoors'];
                }
                x++;
            } 

            if (selectedFilters['SuiteDriveInDoors'].length !== 0) {
                if (x > 0) {
                    queryString += "&";
                } else if (x === 0) {
                    queryString += "?";
                }
                if (parseInt(selectedFilters['SuiteDriveInDoors']) === 0) {
                    queryString += 'SuiteDriveInDoors='+selectedFilters['SuiteDriveInDoors'];
                } else {
                    queryString += 'SuiteDriveInDoors=min'+selectedFilters['SuiteDriveInDoors'];
                }
                x++;
            } 
            
            if (selectedFilters['SuiteAvailElecAmps'].length !== 0) {
                if (x > 0) {
                    queryString += "&";
                } else if (x === 0) {
                    queryString += "?";
                }
                if (parseInt(selectedFilters['SuiteAvailElecAmps']) === 0) {
                    queryString += 'SuiteAvailElecAmps=less+50A';
                } else {
                    queryString += 'SuiteAvailElecAmps=min+'+selectedFilters['SuiteAvailElecAmps']+"A";
                }
                x++;
            } 
            
            if (selectedFilters['SuiteAvailElecVolts'].length !== 0) {
                if (x > 0) {
                    queryString += "&";
                } else if (x === 0) {
                    queryString += "?";
                }
                if (parseInt(selectedFilters['SuiteAvailElecVolts']) === 0) {
                    queryString += 'SuiteAvailElecVolts=Max+120V';
                } else {
                    queryString += 'SuiteAvailElecVolts=min+'+selectedFilters['SuiteAvailElecVolts']+"V";
                }
                x++;
            }
            
            if (selectedFilters['SuiteParkingSurfaceStalls'].length !== 0) {
                if (x > 0) {
                    queryString += "&";
                } else if (x === 0) {
                    queryString += "?";
                }
                    queryString += 'SuiteParkingSurfaceStalls=' + selectedFilters['SuiteParkingSurfaceStalls'][0] + '-' + selectedFilters['SuiteParkingSurfaceStalls'][1];
                x++;
            }
            
            if (selectedFilters['SuiteTruckTrailerParking'].length !== 0) {
                if (x > 0) {
                    queryString += "&";
                } else if (x === 0) {
                    queryString += "?";
                }
                if (parseInt(selectedFilters['SuiteTruckTrailerParking']) === 1){
                    queryString += 'SuiteTruckTrailerParking=Yes';
                } else {
                    queryString += 'SuiteTruckTrailerParking=No';
                }
                x++;
            }

        var newUrl = startUrl + queryString;
//        newUrl = encodeURI(newUrl);
//        console.log("newUrl");
//        console.log(newUrl);
    var currentState = History.getState();
//    console.log("currentState just before updateUrl fired");
//    console.log(currentState);

    var numberOfEntries = window.history.length;
//    console.log("History Length just before updateUrl");
//    console.log(numberOfEntries);

    if (manualStateChange === false) {
        manualStateChange = false;
//            console.log("updateURL History.pushState FIRES NOW");
//            history.pushState(null, null, newUrl);
        History.pushState(null, null, newUrl);
    } else {
//        console.log("updateURL History.pushState DID NOT FIRE");
    }
}




//--------------------
// Part Seven: Set up the autocomplete bar
//--------------------



// Autocomplete 
// (jQuery UI Autocomplete)
//
// create array to use for autocompleting
    var searchJson = [];
    var u = 0;
    var bName = '';
    _.each(buildingList[0], function (i) {
        searchJson.push({value: "" + i.province + "", code: "province", type: "location"});
        searchJson.push({value: "" + i.city + "", code: "city", type: "location"});
        searchJson.push({value: "" + i.street_address + "", code: "street_address", type: "location"});
        searchJson.push({value: "" + i.pc + "", code: "pc", type: "location"});
        if (i.building_name.indexOf("'") > -1) {
            bName = i.building_name.replace("\'", "'");
        } else {
            bName = i.building_name;
        }
        searchJson.push({value: "" + bName + "", code: "building_name", type: "location"});
        u++;
    });
    
    
// beautiful function that removes duplicates from an array
// IE10 / EDGE SUPPORTED
// https://stackoverflow.com/questions/36032179/remove-duplicates-in-an-object-array-javascript
function dedupe(arr) {
  return arr.reduce(function (p, c) {
    // create an identifying id from the object values
//    var id = [c.value];
    var id = [c.value, c.code, c.type].join('|');
    // if the id is not found in the temp array
    // add the object to the output array
    // and add the key to the temp array
    if (p.temp.indexOf(id) === -1) {
      p.out.push(c);
      p.temp.push(id);
    }
    return p;
  // return the deduped array
  }, { temp: [], out: [] }).out;
}    

var distinctJson = dedupe(searchJson);

// flush accents out of search results for bilingually named buildings
var accentMap = {
'ẚ':'a', 'Á':'a', 'á':'a', 'À':'a', 'à':'a', 'Ă':'a', 'ă':'a', 'Ắ':'a', 'ắ':'a', 'Ằ':'a', 'ằ':'a', 'Ẵ':'a', 'ẵ':'a', 'Ẳ':'a', 'ẳ':'a', 'Â':'a', 'â':'a', 'Ấ':'a', 'ấ':'a', 'Ầ':'a', 'ầ':'a', 'Ẫ':'a', 'ẫ':'a', 'Ẩ':'a', 'ẩ':'a', 'Ǎ':'a', 'ǎ':'a', 'Å':'a', 'å':'a', 'Ǻ':'a', 'ǻ':'a', 'Ä':'a', 'ä':'a', 'Ǟ':'a', 'ǟ':'a', 'Ã':'a', 'ã':'a', 'Ȧ':'a', 'ȧ':'a', 'Ǡ':'a', 'ǡ':'a', 'Ą':'a', 'ą':'a', 'Ā':'a', 'ā':'a', 'Ả':'a', 'ả':'a', 'Ȁ':'a', 'ȁ':'a', 'Ȃ':'a', 'ȃ':'a', 'Ạ':'a', 'ạ':'a', 'Ặ':'a', 'ặ':'a', 'Ậ':'a', 'ậ':'a', 'Ḁ':'a', 'ḁ':'a', 'Ⱥ':'a', 'ⱥ':'a', 'Ǽ':'a', 'ǽ':'a', 'Ǣ':'a', 'ǣ':'a', 'Ḃ':'b', 'ḃ':'b', 'Ḅ':'b', 'ḅ':'b', 'Ḇ':'b', 'ḇ':'b', 'Ƀ':'b', 'ƀ':'b', 'ᵬ':'b', 'Ɓ':'b', 'ɓ':'b', 'Ƃ':'b', 'ƃ':'b', 'Ć':'c', 'ć':'c', 'Ĉ':'c', 'ĉ':'c', 'Č':'c', 'č':'c', 'Ċ':'c', 'ċ':'c', 'Ç':'c', 'ç':'c', 'Ḉ':'c', 'ḉ':'c', 'Ȼ':'c', 'ȼ':'c', 'Ƈ':'c', 'ƈ':'c', 'ɕ':'c', 'Ď':'d', 'ď':'d', 'Ḋ':'d', 'ḋ':'d', 'Ḑ':'d', 'ḑ':'d', 'Ḍ':'d', 'ḍ':'d', 'Ḓ':'d', 'ḓ':'d', 'Ḏ':'d', 'ḏ':'d', 'Đ':'d', 'đ':'d', 'ᵭ':'d', 'Ɖ':'d', 'ɖ':'d', 'Ɗ':'d', 'ɗ':'d', 'Ƌ':'d', 'ƌ':'d', 'ȡ':'d', 'ð':'d', 'É':'e', 'Ə':'e', 'Ǝ':'e', 'ǝ':'e', 'é':'e', 'È':'e', 'è':'e', 'Ĕ':'e', 'ĕ':'e', 'Ê':'e', 'ê':'e', 'Ế':'e', 'ế':'e', 'Ề':'e', 'ề':'e', 'Ễ':'e', 'ễ':'e', 'Ể':'e', 'ể':'e', 'Ě':'e', 'ě':'e', 'Ë':'e', 'ë':'e', 'Ẽ':'e', 'ẽ':'e', 'Ė':'e', 'ė':'e', 'Ȩ':'e', 'ȩ':'e', 'Ḝ':'e', 'ḝ':'e', 'Ę':'e', 'ę':'e', 'Ē':'e', 'ē':'e', 'Ḗ':'e', 'ḗ':'e', 'Ḕ':'e', 'ḕ':'e', 'Ẻ':'e', 'ẻ':'e', 'Ȅ':'e', 'ȅ':'e', 'Ȇ':'e', 'ȇ':'e', 'Ẹ':'e', 'ẹ':'e', 'Ệ':'e', 'ệ':'e', 'Ḙ':'e', 'ḙ':'e', 'Ḛ':'e', 'ḛ':'e', 'Ɇ':'e', 'ɇ':'e', 'ɚ':'e', 'ɝ':'e', 'Ḟ':'f', 'ḟ':'f', 'ᵮ':'f', 'Ƒ':'f', 'ƒ':'f', 'Ǵ':'g', 'ǵ':'g', 'Ğ':'g', 'ğ':'g', 'Ĝ':'g', 'ĝ':'g', 'Ǧ':'g', 'ǧ':'g', 'Ġ':'g', 'ġ':'g', 'Ģ':'g', 'ģ':'g', 'Ḡ':'g', 'ḡ':'g', 'Ǥ':'g', 'ǥ':'g', 'Ɠ':'g', 'ɠ':'g', 'Ĥ':'h', 'ĥ':'h', 'Ȟ':'h', 'ȟ':'h', 'Ḧ':'h', 'ḧ':'h', 'Ḣ':'h', 'ḣ':'h', 'Ḩ':'h', 'ḩ':'h', 'Ḥ':'h', 'ḥ':'h', 'Ḫ':'h', 'ḫ':'h', 'H':'h', '̱':'h', 'ẖ':'h', 'Ħ':'h', 'ħ':'h', 'Ⱨ':'h', 'ⱨ':'h', 'Í':'i', 'í':'i', 'Ì':'i', 'ì':'i', 'Ĭ':'i', 'ĭ':'i', 'Î':'i', 'î':'i', 'Ǐ':'i', 'ǐ':'i', 'Ï':'i', 'ï':'i', 'Ḯ':'i', 'ḯ':'i', 'Ĩ':'i', 'ĩ':'i', 'İ':'i', 'i':'i', 'Į':'i', 'į':'i', 'Ī':'i', 'ī':'i', 'Ỉ':'i', 'ỉ':'i', 'Ȉ':'i', 'ȉ':'i', 'Ȋ':'i', 'ȋ':'i', 'Ị':'i', 'ị':'i', 'Ḭ':'i', 'ḭ':'i', 'I':'i', 'ı':'i', 'Ɨ':'i', 'ɨ':'i', 'Ĵ':'j', 'ĵ':'j', 'J':'j', '̌':'j', 'ǰ':'j', 'ȷ':'j', 'Ɉ':'j', 'ɉ':'j', 'ʝ':'j', 'ɟ':'j', 'ʄ':'j', 'Ḱ':'k', 'ḱ':'k', 'Ǩ':'k', 'ǩ':'k', 'Ķ':'k', 'ķ':'k', 'Ḳ':'k', 'ḳ':'k', 'Ḵ':'k', 'ḵ':'k', 'Ƙ':'k', 'ƙ':'k', 'Ⱪ':'k', 'ⱪ':'k', 'Ĺ':'a', 'ĺ':'l', 'Ľ':'l', 'ľ':'l', 'Ļ':'l', 'ļ':'l', 'Ḷ':'l', 'ḷ':'l', 'Ḹ':'l', 'ḹ':'l', 'Ḽ':'l', 'ḽ':'l', 'Ḻ':'l', 'ḻ':'l', 'Ł':'l', 'ł':'l', 'Ł':'l', '̣':'l', 'ł':'l', '̣':'l', 'Ŀ':'l', 'ŀ':'l', 'Ƚ':'l', 'ƚ':'l', 'Ⱡ':'l', 'ⱡ':'l', 'Ɫ':'l', 'ɫ':'l', 'ɬ':'l', 'ɭ':'l', 'ȴ':'l', 'Ḿ':'m', 'ḿ':'m', 'Ṁ':'m', 'ṁ':'m', 'Ṃ':'m', 'ṃ':'m', 'ɱ':'m', 'Ń':'n', 'ń':'n', 'Ǹ':'n', 'ǹ':'n', 'Ň':'n', 'ň':'n', 'Ñ':'n', 'ñ':'n', 'Ṅ':'n', 'ṅ':'n', 'Ņ':'n', 'ņ':'n', 'Ṇ':'n', 'ṇ':'n', 'Ṋ':'n', 'ṋ':'n', 'Ṉ':'n', 'ṉ':'n', 'Ɲ':'n', 'ɲ':'n', 'Ƞ':'n', 'ƞ':'n', 'ɳ':'n', 'ȵ':'n', 'N':'n', '̈':'n', 'n':'n', '̈':'n', 'Ó':'o', 'ó':'o', 'Ò':'o', 'ò':'o', 'Ŏ':'o', 'ŏ':'o', 'Ô':'o', 'ô':'o', 'Ố':'o', 'ố':'o', 'Ồ':'o', 'ồ':'o', 'Ỗ':'o', 'ỗ':'o', 'Ổ':'o', 'ổ':'o', 'Ǒ':'o', 'ǒ':'o', 'Ö':'o', 'ö':'o', 'Ȫ':'o', 'ȫ':'o', 'Ő':'o', 'ő':'o', 'Õ':'o', 'õ':'o', 'Ṍ':'o', 'ṍ':'o', 'Ṏ':'o', 'ṏ':'o', 'Ȭ':'o', 'ȭ':'o', 'Ȯ':'o', 'ȯ':'o', 'Ȱ':'o', 'ȱ':'o', 'Ø':'o', 'ø':'o', 'Ǿ':'o', 'ǿ':'o', 'Ǫ':'o', 'ǫ':'o', 'Ǭ':'o', 'ǭ':'o', 'Ō':'o', 'ō':'o', 'Ṓ':'o', 'ṓ':'o', 'Ṑ':'o', 'ṑ':'o', 'Ỏ':'o', 'ỏ':'o', 'Ȍ':'o', 'ȍ':'o', 'Ȏ':'o', 'ȏ':'o', 'Ơ':'o', 'ơ':'o', 'Ớ':'o', 'ớ':'o', 'Ờ':'o', 'ờ':'o', 'Ỡ':'o', 'ỡ':'o', 'Ở':'o', 'ở':'o', 'Ợ':'o', 'ợ':'o', 'Ọ':'o', 'ọ':'o', 'Ộ':'o', 'ộ':'o', 'Ɵ':'o', 'ɵ':'o', 'Ṕ':'p', 'ṕ':'p', 'Ṗ':'p', 'ṗ':'p', 'Ᵽ':'p', 'Ƥ':'p', 'ƥ':'p', 'P':'p', '̃':'p', 'p':'p', '̃':'p', 'ʠ':'q', 'Ɋ':'q', 'ɋ':'q', 'Ŕ':'r', 'ŕ':'r', 'Ř':'r', 'ř':'r', 'Ṙ':'r', 'ṙ':'r', 'Ŗ':'r', 'ŗ':'r', 'Ȑ':'r', 'ȑ':'r', 'Ȓ':'r', 'ȓ':'r', 'Ṛ':'r', 'ṛ':'r', 'Ṝ':'r', 'ṝ':'r', 'Ṟ':'r', 'ṟ':'r', 'Ɍ':'r', 'ɍ':'r', 'ᵲ':'r', 'ɼ':'r', 'Ɽ':'r', 'ɽ':'r', 'ɾ':'r', 'ᵳ':'r', 'ß':'s', 'Ś':'s', 'ś':'s', 'Ṥ':'s', 'ṥ':'s', 'Ŝ':'s', 'ŝ':'s', 'Š':'s', 'š':'s', 'Ṧ':'s', 'ṧ':'s', 'Ṡ':'s', 'ṡ':'s', 'ẛ':'s', 'Ş':'s', 'ş':'s', 'Ṣ':'s', 'ṣ':'s', 'Ṩ':'s', 'ṩ':'s', 'Ș':'s', 'ș':'s', 'ʂ':'s', 'S':'s', '̩':'s', 's':'s', '̩':'s', 'Þ':'t', 'þ':'t', 'Ť':'t', 'ť':'t', 'T':'t', '̈':'t', 'ẗ':'t', 'Ṫ':'t', 'ṫ':'t', 'Ţ':'t', 'ţ':'t', 'Ṭ':'t', 'ṭ':'t', 'Ț':'t', 'ț':'t', 'Ṱ':'t', 'ṱ':'t', 'Ṯ':'t', 'ṯ':'t', 'Ŧ':'t', 'ŧ':'t', 'Ⱦ':'t', 'ⱦ':'t', 'ᵵ':'t', 'ƫ':'t', 'Ƭ':'t', 'ƭ':'t', 'Ʈ':'t', 'ʈ':'t', 'ȶ':'t', 'Ú':'u', 'ú':'u', 'Ù':'u', 'ù':'u', 'Ŭ':'u', 'ŭ':'u', 'Û':'u', 'û':'u', 'Ǔ':'u', 'ǔ':'u', 'Ů':'u', 'ů':'u', 'Ü':'u', 'ü':'u', 'Ǘ':'u', 'ǘ':'u', 'Ǜ':'u', 'ǜ':'u', 'Ǚ':'u', 'ǚ':'u', 'Ǖ':'u', 'ǖ':'u', 'Ű':'u', 'ű':'u', 'Ũ':'u', 'ũ':'u', 'Ṹ':'u', 'ṹ':'u', 'Ų':'u', 'ų':'u', 'Ū':'u', 'ū':'u', 'Ṻ':'u', 'ṻ':'u', 'Ủ':'u', 'ủ':'u', 'Ȕ':'u', 'ȕ':'u', 'Ȗ':'u', 'ȗ':'u', 'Ư':'u', 'ư':'u', 'Ứ':'u', 'ứ':'u', 'Ừ':'u', 'ừ':'u', 'Ữ':'u', 'ữ':'u', 'Ử':'u', 'ử':'u', 'Ự':'u', 'ự':'u', 'Ụ':'u', 'ụ':'u', 'Ṳ':'u', 'ṳ':'u', 'Ṷ':'u', 'ṷ':'u', 'Ṵ':'u', 'ṵ':'u', 'Ʉ':'u', 'ʉ':'u', 'Ṽ':'v', 'ṽ':'v', 'Ṿ':'v', 'ṿ':'v', 'Ʋ':'v', 'ʋ':'v', 'Ẃ':'w', 'ẃ':'w', 'Ẁ':'w', 'ẁ':'w', 'Ŵ':'w', 'ŵ':'w', 'W':'w', '̊':'w', 'ẘ':'w', 'Ẅ':'w', 'ẅ':'w', 'Ẇ':'w', 'ẇ':'w', 'Ẉ':'w', 'ẉ':'w', 'Ẍ':'x', 'ẍ':'x', 'Ẋ':'x', 'ẋ':'x', 'Ý':'y', 'ý':'y', 'Ỳ':'y', 'ỳ':'y', 'Ŷ':'y', 'ŷ':'y', 'Y':'y', '̊':'y', 'ẙ':'y', 'Ÿ':'y', 'ÿ':'y', 'Ỹ':'y', 'ỹ':'y', 'Ẏ':'y', 'ẏ':'y', 'Ȳ':'y', 'ȳ':'y', 'Ỷ':'y', 'ỷ':'y', 'Ỵ':'y', 'ỵ':'y', 'ʏ':'y', 'Ɏ':'y', 'ɏ':'y', 'Ƴ':'y', 'ƴ':'y', 'Ź':'z', 'ź':'z', 'Ẑ':'z', 'ẑ':'z', 'Ž':'z', 'ž':'z', 'Ż':'z', 'ż':'z', 'Ẓ':'z', 'ẓ':'z', 'Ẕ':'z', 'ẕ':'z', 'Ƶ':'z', 'ƶ':'z', 'Ȥ':'z', 'ȥ':'z', 'ʐ':'z', 'ʑ':'z', 'Ⱬ':'z', 'ⱬ':'z', 'Ǯ':'z', 'ǯ':'z', 'ƺ':'z', 
// Roman fullwidth ascii equivalents: 0xff00 to 0xff5e
'２':'2', '６':'6', 'Ｂ':'B', 'Ｆ':'F', 'Ｊ':'J', 'Ｎ':'N', 'Ｒ':'R', 'Ｖ':'V', 'Ｚ':'Z', 'ｂ':'b', 'ｆ':'f', 'ｊ':'j', 'ｎ':'n', 'ｒ':'r', 'ｖ':'v', 'ｚ':'z', '１':'1', '５':'5', '９':'9', 'Ａ':'A', 'Ｅ':'E', 'Ｉ':'I', 'Ｍ':'M', 'Ｑ':'Q', 'Ｕ':'U', 'Ｙ':'Y', 'ａ':'a', 'ｅ':'e', 'ｉ':'i', 'ｍ':'m', 'ｑ':'q', 'ｕ':'u', 'ｙ':'y', '０':'0', '４':'4', '８':'8', 'Ｄ':'D', 'Ｈ':'H', 'Ｌ':'L', 'Ｐ':'P', 'Ｔ':'T', 'Ｘ':'X', 'ｄ':'d', 'ｈ':'h', 'ｌ':'l', 'ｐ':'p', 'ｔ':'t', 'ｘ':'x', '３':'3', '７':'7', 'Ｃ':'C', 'Ｇ':'G', 'Ｋ':'K', 'Ｏ':'O', 'Ｓ':'S', 'Ｗ':'W', 'ｃ':'c', 'ｇ':'g', 'ｋ':'k', 'ｏ':'o', 'ｓ':'s', 'ｗ':'w'};

var normalize = function( term ) {
      var ret = "";
      for ( var i = 0; i < term.length; i++ ) {
        ret += accentMap[ term.charAt(i) ] || term.charAt(i);
      }
      return ret;
    };


var locationFilter = {};

    $("#property-search").autocomplete({
        minLength: 3,
        delay: 600,
        // jquery ui autocomplete "custom source"
        source: function( request, response ) {
        var matcher = new RegExp( $.ui.autocomplete.escapeRegex( request.term ), "i" );
        response( $.grep( distinctJson, function( value ) {
          value = value.label || value.value || value;
          return matcher.test( value ) || matcher.test( normalize( value ) );
        }) );
        },
        autoFocus: true,
        focus: function (event, ui) {
            $("#property-search").val(ui.item.value);
            return false;
        },
        select: function (event, ui) {
            locationFilter = ui.item;
            filterFunc();
        }
    });
    


    // Clear Filters link
    // DISABLED. LINK CURRENTLY JUST RELOADS THE PAGE 
//    $('.fs_clear-filters').on('click', function (e) {
//        e.preventDefault();
////    console.log(selectedFilters);
//        $('input:checkbox').prop('checked', false);
//        $(':input').val('');
//        $("div.fs_thechips").empty();
//        selectedFilters = {};
//        markersToRemove = [];
//        $(".fs_search-results").empty();
//        generateHTML(buildingList);
//        theMap.removeMarkers();
//        addPoints(buildingList[0], theMap);
//        updateMap();
//    });





// prepare various inputs that call filterFunc

    var $suiteFilterCheckboxes = $('#collapseFilter1 input[type="checkbox"]');
    $suiteFilterCheckboxes.on('change', filterFunc);
    
    
    $('input[id="show_no_vacancy"]').on("change", function() {
        // This is value xor-equals true, which will flip the boolean value
        // https://stackoverflow.com/questions/2479058/how-to-make-a-boolean-variable-switch-between-true-and-false-every-time-a-method
        showVacancies ^= true;
        filterFunc();
    });

    if (showVacancies) {
        $("#show_no_vacancy").prop("checked", true);
    }

// Initialize all select boxes (filters)
    $('select').change(function () {
        filterFunc();
    });

    $('.fs_clear-filters, .fs_reset-search').on('click', function(e) {
        e.preventDefault();
        if (selectedFilters['lang'] == "fr_CA"){
        window.location.href = startUrl+"?lang=fr_CA";
        } else {
        window.location.href = startUrl;
        }        
    });

    // hey look the languages work
    $(".langclick").on('click', function(e) {
        e.preventDefault();
        // get the language clicked from data-language
        var selectedLang = $(this).data('language');
        // take current URL
        var urlNow = window.location.href;
        // remove any language reference
        urlNow = urlNow.replace(/&lang=en_CA/g,"").replace(/&lang=fr_CA/g,"").replace(/\?lang=en_CA/g,"").replace(/\?lang=fr_CA/g,"").replace(/#/g,"");
        // add new language reference
        if (urlNow.indexOf("?") > -1) {
            var langUrl = urlNow+"&lang="+selectedLang;
        } else {
            var langUrl = urlNow+"?lang="+selectedLang;
        }
        // reload the page with new url
        window.location.href = langUrl;
    });
    
    
    
    



    // deprec
//    updateSizeRange(buildingList, "first");
    // call the URL Reader function
    processUrl();
    

    // theLanguage gets changed to French during processUrl() if french is set
    // need to do this translating after that happens. Please don't move this.
    var favouritesTranslated = '';
    var noFavouritesMessage = '';
    if (theLanguage == "fr_CA") {
        favouritesTranslated = " Options";
        noFavouritesMessage = "Vous n'avez pas d'options. Sélectionnez des suites pour comparer les options.";
    } else {
        favouritesTranslated = " Options";
        noFavouritesMessage = "You have no options. Select suites to compare options.";
    }
    
    
    
    
    
    
    
    // first load of template
//    generateHTML(buildingList);
    filterFunc();
//    updateMap();

    $('.fs_toolbar-link').on('click', function (e) {
        e.preventDefault();
        $('#fs_toolbar').addClass('open');
    });

    $('#fs_toolbar .close').on('click', function (e) {
        e.preventDefault();
        $('#fs_toolbar').removeClass('open');
    });

    $("body").on('click', '.fs_building-link, .fs_available-suites-link, .fs_map-suites-link', function (e) {

//    $(".fs_building-link, .fs_available-suites-link").on('click', function (e) {
        e.preventDefault();
        var newQstring;
        if (this.href.indexOf("?") > -1) {
            newQstring = queryString.replace("?", "&");
        } else {
            newQstring = queryString;
        }
        if (this.className === "fs_available-suites-link" || this.className === "fs_map-suites-link") {
           newQstring = newQstring+"#tab_fs-suites";
        }
        
//        newQstring = encodeURI(newQstring);
//        console.log("this.href+newQstring");
//        console.log(this.href+newQstring);
//        window.location.href = this.href+newQstring;
        var win = window.open(this.href+newQstring, '_blank');
        win.focus();
    });
    
    
    
//    $(".suiteLink").on('click', function (e) {
    $("body").on('click', ".suiteLink", function (e) {
        e.preventDefault();
        var newsuiteQstring;
        if (this.href.indexOf("?") > -1) {
            newsuiteQstring = queryString.replace("?", "&");
        } else {
            newsuiteQstring = queryString;
        }
//        window.location.href = this.href+newsuiteQstring;
        var win2 = window.open(this.href+newsuiteQstring, '_blank');
        win2.focus();
    });






// pdf Printing index page


$('.printSelect').on('click', function(e) {
    e.preventDefault();
    console.log("this");
    console.log(this);
    $('.printSelect').removeClass('active');
    $(this).addClass('active');
});


$('#fs_modal-print').on("shown.bs.modal", function () { 
    $('.printSelect').removeClass('active');
});


    $('.btn-modal-print').on('click', function (e) {
        var href = url ? url : window.location.href;

        if (!showVacancies) {
            queryString = queryString+"&show_no_vacancy=off";
        }

        var config_selection = $('a.nav-link.printSelect.active').data('pdftype');
        
console.log("config_selection");
console.log(config_selection);
        var pdf_url = 'printpdf.php' + queryString;
        var pp_url = pdf_url.replace('printpdf.php', 'printpdfpropertypages.php');

console.log("pdf_url");
console.log(pdf_url)
console.log("pp_url");
console.log(pp_url);

        if (config_selection == "Condensed") {
            window.open(pdf_url + "&config=1", "_self");
        } else if (config_selection == "Properties") {
//            var propertycounter = 0;
//            propertycounter = suiteCount;
//            console.log("propertycounter");
//            console.log(propertycounter);
//            if (propertycounter < 11) {
                window.open(pp_url, "_self");
//            } else {
//                alert("Please select 10 buildings or fewer.");
//            }
        } else {
            // PRINT THE PDF
            window.open(pdf_url, "_self");
        }

        $('#modal-print-pdf').modal('hide');
    });




var prevNumberEntries = 0;
var manualStateChange = false;
//
History.Adapter.bind(window,'statechange',function(event){
    var currentState = History.getState();
//    console.log("currentState change state fired");
//    console.log(currentState);

    var numberOfEntries = window.history.length;

    if (prevNumberEntries != numberOfEntries) {
        //we've added to the history so a back or forward button was not pushed
        prevNumberEntries = numberOfEntries;
        manualStateChange = false;
//        console.log('update history');
    }
    else {
        //a back or forward button was pressed
        manualStateChange = true;
//        console.log('do NOT update history');
    }
//    console.log("History Length in history");
//    console.log(numberOfEntries);

//    manualStateChange = false;
//    console.log("manualStateChange inside statechange");
//    console.log(manualStateChange);


    if(manualStateChange == true){
     // BACK BUTTON WAS PRESSED
//        console.log("original state change event");
//        console.log(event);
//        console.log("location: " + document.location + ", state: " + JSON.stringify(event.state));

//        console.log("selectedFilters back button");
//        console.log(selectedFilters);
//        console.log("urlFilters back button");
//        console.log(urlFilters);
        
        processUrl();
        filterFunc();
    }
   // manualStateChange = true;
});





//
//window.onpopstate = function(event) {
//    
//    console.log('on popstate fired');
//    manualStateChange = true;
//    
//    console.log(manualStateChange);
//
//};
//

var currentState = History.getState();
//console.log("currentState at page load");
//console.log(currentState);



var numberOfEntries = window.history.length;
//console.log("History Length at page load");
//console.log(numberOfEntries);



// Searching by Map


var mapFilter = [];

    function mapMoved() {
        var newBounds = map.getBounds();
//        console.log("bounds in mapMoved()");
//        console.log(bounds);\
        var markersToFilter = [];
        markersToFilter = buildingListAll;
        
        
        console.log("markersToFilter in mapMoved");
        console.log(markersToFilter);
        
        mapFilter = [];
        selectedFilters['mapFilter'] = [];
    
//        selectedFilters['location'] = [];
//        locationFilter = {};
    
        $.each(markersToFilter, function (k, v) {
            var buildingCode = v['building_code'];
            var latitude = v['latitude'];
            var longitude = v['longitude'];
            
          var latlng = new google.maps.LatLng(latitude,longitude);
//                console.log("latlng inside loop");
//                console.log(latlng);
//          theMap.addMarker({
//            lat: latitude,
//            lng: lng,
//            title: buildingCode,    
//          });

          if (newBounds.contains(latlng)) {
            // add the details into an array here.  After the loop, output the contents to a div
//                console.log("latlng after moved");
//                console.log(latlng);
//                console.log("buildingCode after moved");
//                console.log(buildingCode);
                mapFilter.push(buildingCode);
                
          }
        });
//        console.log("mapFilter");
//        console.log(mapFilter);
        
        filterFunc();
    }




$(window).on("load",function(){
    if(!isMobile) {
        $("#fs_filter").mCustomScrollbar({
            theme: "minimal-dark",
            scrollInertia: 500,
            scrollEasing: 'linear'
        });
    }
    
});



$('#fs_modal-share').on('hidden.bs.modal', function (e) {
    $('#fs_modal-share input').parent().removeClass('alert alert-danger');
});

    $('#fs_modal-share .btn-modal-share-submit').on('click', function(e) {
        $('#fs_modal-share input').parent().removeClass('alert alert-danger');

        var urlToShare = window.location.origin+"/favourites.php";

        var uu=0;
        _.each($("#fs_favourites-carousel").children("div"), function(i) {            
            if (uu===0){
                urlToShare += "?suites[]="+$(i).data("suite-id");
            } else {
                urlToShare += "&suites[]="+$(i).data("suite-id");
            }
            uu++;
        });

        
        
        
//        console.log("urlToShare");
//        console.log(urlToShare);

        var tname = $('#fs_modal-share #recipient-name').val();
        var temail = $('#fs_modal-share #recipient-email').val();
        var fname = $('#fs_modal-share #sender-name').val();
        var femail = $('#fs_modal-share #sender-email').val();
        var fcomments = $('#fs_modal-share #bshare-comments').val();


        var googleinfo = grecaptcha.getResponse(fshare_captcha);

        var error = 0;

        if(tname === '') {
            $('#fs_modal-share #recipient-name').parent().addClass('alert alert-danger');
//            $('#fs_modal-share .alert-danger').removeClass('d-none');
            error = error+1;
        }

        if(temail === '') {
            $('#fs_modal-share #recipient-email').parent().addClass('alert alert-danger');
//            $('#fs_modal-share .alert-danger').removeClass('d-none');
            error = error+1;
        }

        if(fname === '') {
            $('#fs_modal-share #sender-name').parent().addClass('alert alert-danger');
//            $('#fs_modal-share .alert-danger').removeClass('d-none');
            error = error+1;
        }

        if(femail === '') {
            $('#fs_modal-share #sender-email').parent().addClass('alert alert-danger');
//            $('#fs_modal-share .alert-danger').removeClass('d-none');
            error = error+1;
        }
        
//console.log("error");
//console.log(error);
        if (error === 0) {            
            $.ajax({
                type: "GET",
                url: "verifyemail.php",
                data: {fromemail:femail,toemail:temail},
                beforeSend: function() {
                    $('#fs_modal-share .modal-body').prepend('<div class="loader"><img src="images/loader.svg"></div>');
                },
                success: function(data){
                    
                },
                error: function(){
                    
                }
            }).done( function(data) {
                if (data === '66') {
                    $('.loader').remove();
                    $('#fs_modal-share #recipient-email').parent().addClass('alert alert-danger');
                    $('#fs_modal-share .alert-danger').removeClass('hidden');
                    error = error+1;
                } else if (data === '77') {
                    $('.loader').remove();
                    $('#fs_modal-share #sender-email').parent().addClass('alert alert-danger');
                    $('#fs_modal-share .alert-danger').removeClass('hidden');
                    error = error+1;
                } else if (data === '88') {
                    $('.loader').remove();
                    $('#fs_modal-share #recipient-email').parent().addClass('alert alert-danger');
                    $('#fs_modal-share #sender-email').parent().addClass('alert alert-danger');
                    $('#fs_modal-share .alert-danger').removeClass('hidden');
                    error = error+1;
                } else {
                    $('#fs_modal-share input').parent().removeClass('alert alert-danger');
                    // email verified in PHP OK - start recaptcha
                    // console.log("recaptcha started");
                    var googleinfo = grecaptcha.getResponse(fshare_captcha);
                    var ipaddress = $("#jsuseripaddress").val();

                    $.ajax({
                        type: "GET",
                        url: "docaptcha.php",
                        data: {response: googleinfo, remoteip:ipaddress},
                        success: function(data){

                        },
                        error: function(){

                        }
                    }).done(function(data) {

                        var newdata = JSON.parse(data);
                        var reply = newdata[0][0];

                        if (newdata[0].length === 2) {
                            var error = newdata[0][1][0];
                        }

                        if (reply === '66') {
                            $('.loader').remove();
                            alert("Are you sure you're not a robot? Please try again.")
                            error = error+1;
                        } else {
                            var ccme = false;
                            if ($("#cc-me").is(':checked')) {
                                ccme = true;
                            }
                            // captcha succes, send email
                            $.ajax({
                                type: "GET",
                                url: "share_a_link.php",
                                data: {toname:tname,toemail:temail,fromname:fname,fromemail:femail,fromcomments:fcomments,urltoshare:urlToShare,requesttype:"Favourite",ccme:ccme},
                                    beforeSend: function() {

                                    },
                                    complete: function(){
                                        //hide preloader
                                        $('.loader').remove();
                                    },
                                    success: function(data){

                                    },
                                    error: function(){

                                    }
                            }).done(function(data) {
                                // console.log("data returned from sentctcrequest.php: ");
                                // console.log(data);
                                $('#fs_modal-share #sender-email, #fs_modal-share #sender-name').parent().removeClass('alert alert-danger');
                                if(data == 66) {
                                    //email error
                                    $('#fs_modal-share #sender-email').parent().addClass('alert alert-danger');
                                    $('#fs_modal-share .alert-danger').removeClass('hidden');
                                }
                                else {
                                    $('#fs_modal-share #sender-email, #fs_modal-share #sender-name').parent().removeClass('alert alert-danger');
                                    $('#fs_modal-share .alert-success').removeClass('hidden');
                                    $('#fs_modal-share .alert-danger').addClass('hidden');
                                    $('textarea, :input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
                                    $('#fs_modal-share').modal('toggle');
                                }
                            });
                        } // end else
                    });
                } //end else 
            });
        }
    });


$('.fs_favourites-print').on('click', function(e) {
     
        var favePdfLink = 'printpdfFavourites.php';
        var uu=0;
        _.each($("#fs_favourites-carousel").children("div"), function(i) {            
            if (uu===0){
                favePdfLink += "?suites[]="+$(i).data("suite-id");
            } else {
                favePdfLink += "&suites[]="+$(i).data("suite-id");
            }
            uu++;
        });
        
//        console.log("favePdfLink");
//        console.log(favePdfLink);
        
//        var win = window.open(this.href+newQstring, '_blank');
//        win.focus();
        window.open(favePdfLink);
        
});













});//eo document.ready 