<?php
$spec_qy = mysql_query("select * from building_specifications where building_code='" . $building_code . "' and lang = " . $lang_id . "");
$row = mysql_fetch_array($spec_qy);


$lang = "en_CA";
$lang_id = 1;
if (isset($_REQUEST['lang'])) {
    if ($_REQUEST['lang'] == "fr_CA") {

        $lang = "fr_CA";
        $lang_id = 0;
    } else {
        
    }
} else {
    
}

setlocale(LC_ALL, $lang . '.utf8');



$suitecount_query_string = 'SELECT COUNT(*) AS realsuitecount FROM suites alls WHERE building_code = ? AND lang=1 AND leased="false"';
// Preparing the statement
$stmt = $db->prepare($suitecount_query_string);
// binding the parameters
$stmt->bind_param('s', $building_code);
// Executing the query
if (!$stmt->execute()) {
    trigger_error('The query execution failed; MySQL said (' . $stmt->errno . ') ' . $stmt->error, E_USER_ERROR);
}
$col1 = null;
$stmt->bind_result($suiteCount);
while ($stmt->fetch()) {
    // having to use this while makes no sense at all but it doesn't work w/o it.
}
$stmt->close();





$shippingDI = $row['const_shipping_doors_drivein'];
$shippingTL = $row['const_shipping_doors_truck'];
$shippingRL = $row['const_rail_loading'];
$ceiling = $row2['clear_height'];
$loadingoutputDI = '';
$loadingoutputTL = '';
$loadingoutputRL = '';
$loadingoutput = '';


if ($shippingDI !== '' && $shippingDI !== "0") {
    $loadingoutputDI = $shippingDI . " " . $language['drivein_loading_abbreviation'];
}
if ($shippingTL !== '' && $shippingTL !== "0") {
    $loadingoutputTL = $shippingTL . " " . $language['truck_loading_abbreviation'];
}
if ($shippingRL !== "false") {
    $loadingoutputRL = $language['rail_loading_abbreviation'];
}


if ($loadingoutputDI != '' && $loadingoutputTL != '' && $loadingoutputRL != '') {
    $loadingoutput = $loadingoutputDI . ", " . $loadingoutputTL . ", " . $loadingoutputRL;
} else if ($loadingoutputDI != '' && $loadingoutputTL != '' && $loadingoutputRL == '') {
    $loadingoutput = $loadingoutputDI . ", " . $loadingoutputTL;
} else if ($loadingoutputDI != '' && $loadingoutputTL == '' && $loadingoutputRL == '') {
    $loadingoutput = $loadingoutputDI;
} 

else if ($loadingoutputDI == '' && $loadingoutputTL != '' && $loadingoutputRL != '') {
    $loadingoutput = $loadingoutputTL . ", " . $loadingoutputRL;
} else if ($loadingoutputDI == '' && $loadingoutputTL == '' && $loadingoutputRL != '') {
    $loadingoutput = $loadingoutputRL;
} 

 else if ($loadingoutputDI == '' && $loadingoutputTL != '' && $loadingoutputRL == '') {
    $loadingoutput = $loadingoutputTL;
} else if ($loadingoutputDI != '' && $loadingoutputTL == '' && $loadingoutputRL != '') {
    $loadingoutput = $loadingoutputDI . ", " . $loadingoutputRL;
} else {
    // all are empty. n/a
    $loadingoutput = 'n/a';
}



$poweroutputAmps = '';
$poweroutputVolts = '';
if ($poweramps != '' && $poweramps != 0) {
    $poweroutputAmps = $poweramps . " A";
}
if ($powervolts != '' && $powervolts != 0) {
    $poweroutputVolts = $powervolts . " V";
}
if ($poweroutputAmps != '' && $poweroutputVolts != '') {
    $poweroutput = $poweroutputAmps . ", " . $poweroutputVolts;
} else if ($poweroutputAmps != '' && $poweroutputVolts == '') {
    $poweroutput = $poweroutputAmps;
} else if ($poweroutputAmps == '' && $poweroutputVolts != '') {
    $poweroutput = $poweroutputVolts;
} else {
    // both are empty. leave a space
    $poweroutput = "&nbsp;";
}

?>



<?php if ($theme['featured_specs_visible'] == 1) { ?>
<section class="fs_section fs_building-features">
    <div class="row">
        <div class="col">
            <div class="card fs_card-features">
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <div class="row">
                                <div class="col fs_col-feature-icon">
                                    <i class="material-icons fs_icon-feature">business</i>
                                </div>
                                <div class="col fs_col-feature-value">
                                    <h3 class="card-title fs_card-feature-title"><?php echo $language['size_text']; ?> / <?php echo $language['building_information_sq_ft_trans']; ?></h3>
                                    <?php $total_space = format_numbers($row['total_space'], $language); ?>
                                    <p class="fs_feature-value"><?php echo ($total_space !== '0' ? $total_space : $language['na_text']); ?></p>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="row">
                                <div class="col fs_col-feature-icon">
                                    <i class="material-icons fs_icon-feature">store</i>
                                </div>
                                <div class="col fs_col-feature-value">
                                    <h3 class="card-title fs_card-feature-title"><?php echo $language['units_available_text']; ?></h3>
                                    <p class="fs_feature-value"><?php echo $suiteCount; ?></p>                                                                
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="row">
                                <div class="col fs_col-feature-icon">
                                    <i class="material-icons fs_icon-feature">attach_money</i>
                                </div>
                                <div class="col fs_col-feature-value">
                                    <h3 class="card-title fs_card-feature-title"><?php echo $language['price_text']; ?> / <?php echo $language['month_abbr']; ?></h3>
                                    <p class="fs_feature-value"><?php echo $language['negotiable_text']; ?></p>                                                                
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="row">
                                <div class="col fs_col-feature-icon">
                                    <i class="material-icons fs_icon-feature">local_shipping</i>
                                </div>
                                <div class="col fs_col-feature-value">
                                    <h3 class="card-title fs_card-feature-title"><?php echo $language['loading_text']; ?></h3>
                                    <p class="fs_feature-value"><?php echo $loadingoutput; ?></p>
                                    </div>
                            </div>
                        </div>
<!--                        <div class="col">
                            <div class="row">
                                <div class="col fs_col-feature-icon">
                                    <i class="material-icons fs_icon-feature">flash_on</i>
                                </div>
                                <div class="col fs_col-feature-value">
                                    <h3 class="card-title fs_card-feature-title"><?php // echo $language['power_text']; ?></h3>
                                    <p class="fs_feature-value">
                                        <?php
//                                        if ($row['const_available_electrical_volts'] !== '' || $row['const_available_electrical_volts'] !== ''){
//                                            echo ($row['const_available_electrical_volts'] !== '' ? $row['const_available_electrical_volts'].'v' : $language['na_text']); ?> / <?php echo ($row['const_available_electrical_amps'] !== '' ? $row['const_available_electrical_amps'].'a' : $language['na_text']);
//                                        } else {
//                                            echo $language['na_text'];
//                                        }
                                        ?>
                                    </p>                                                                
                                </div>
                            </div>
                        </div>-->
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="row">
                                <div class="col fs_col-feature-icon">
                                    <i class="material-icons fs_icon-feature">vertical_align_top</i>
                                </div>
                                <div class="col fs_col-feature-value">
                                    <h3 class="card-title fs_card-feature-title"><?php echo $language['ceiling_text']; ?></h3>
                                    <p class="fs_feature-value"><?php echo ($row['const_ceiling_height'] !== '' ? $row['const_ceiling_height'] : $language['na_text']); ?></p>                                                                
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="row">
                                <div class="col fs_col-feature-icon">
                                    <i class="material-icons fs_icon-feature">local_parking</i>
                                </div>
                                <div class="col fs_col-feature-value">
                                    <h3 class="card-title fs_card-feature-title"><?php echo $language['total_stalls_text']; ?></h3>
                                    <p class="fs_feature-value"><?php echo ($row['parking_total_stalls'] !== '0' ? $row['parking_total_stalls'] : $language['na_text']); ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php if ($row['boma'] !== '' || $row['leed'] !== '') { ?>
                    <div class="row">
                        <div class="col">
                            <div class="row">
                                <div class="col fs_col-feature-icon">
                                    <i class="material-icons fs_icon-feature">new_releases</i>
                                </div>
                                <div class="col fs_col-feature-value">
                                    <h3 class="card-title fs_card-feature-title"><?php echo $language['boma_title_text']; ?></h3>
                                    <p class="fs_feature-value"><?php echo ($row['boma'] !== '' ? $row['boma'] : $language['na_text']); ?></p>
                                    </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="row">
                                <div class="col fs_col-feature-icon">
                                    <i class="material-icons fs_icon-feature">spa</i>
                                </div>
                                <div class="col fs_col-feature-value">
                                    <h3 class="card-title fs_card-feature-title"><?php echo $language['leed_title_text']; ?></h3>
                                    <p class="fs_feature-value"><?php echo ($row['leed'] !== '' ? $row['leed'] : $language['na_text']); ?></p>                                                                
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php } ?>

<?php if ($secCount > 0) { ?>
<section class="fs_section building-sections">
    <div class="card fs_card-sections">
        <div class="card-body">
            <div class="card-title"><h4><?php echo $language['features_text']; ?></h4></div>

                <?php echo $features_tab_content; ?>

        </div>
    </div>
</section>
<?php } ?>

<section class="fs_section building-specifications">
<?php if (!checkForEmpty($row['costs_posted_net_rate']) && !checkForEmpty($row['costs_realty']) && !checkForEmpty($row['costs_power']) && !checkForEmpty($row['costs_operating']) && !checkForEmpty($row['costs_other']) && !checkForEmpty($row['costs_add_rent_total'])) {
    // this section is emtpy so skip it
} else { ?>
    <div class="card fs_card-specifications">
        <div class="card-body">
            <div class="card-title"><h3><?php echo $language['tenant_costs_title_text']; ?></h3></div>
        <?php if (checkForEmpty($row['costs_posted_net_rate'])) { ?>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['posted_net_rate_text']; ?></div>
                <div class="col fs_specification-value"><?php echo money_format('%.2n', (double) $row['costs_posted_net_rate']). ' ' . $language['building_information_sq_ft_trans']; ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['costs_realty'])) {
        ?>            
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['realty_tax_text']; ?></div>
                <div class="col fs_specification-value"><?php echo money_format('%.2n', (double) $row['costs_realty']). ' ' . $language['building_information_sq_ft_trans']; ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['costs_power'])) {
        ?>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['utilities_text']; ?></div>
                <div class="col fs_specification-value"><?php echo money_format('%.2n', (double) $row['costs_power']). ' ' . $language['building_information_sq_ft_trans']; ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['costs_operating'])) {
        ?>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['operating_costs_text']; ?></div>
                <div class="col fs_specification-value"><?php echo money_format('%.2n', (double) $row['costs_operating']). ' ' . $language['building_information_sq_ft_trans']; ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['costs_other'])) {
        ?>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['other_text']; ?></div>
                <div class="col fs_specification-value"><?php echo money_format('%.2n', (double) $row['costs_other']). ' ' . $language['building_information_sq_ft_trans']; ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['costs_add_rent_total'])) {
        ?>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['additional_rent_total_text']; ?></div>
                <div class="col fs_specification-value"><?php echo money_format('%.2n', (double) $row['costs_add_rent_total']). ' ' . $language['building_information_sq_ft_trans']; ?></div>
            </div>
        <?php } ?>
        </div>
    </div>
<?php } ?>
    
<?php if (!checkForEmpty($row['year_built']) && !checkForEmpty($row['zoned']) && !checkForEmpty($row['average_office']) && !checkForEmpty($row['number_of_buildings']) && !checkForEmpty($row['number_of_units']) && !checkForEmpty($row['environmental_certification_notes'])) {
    // this section is emtpy so skip it
} else { ?>    
    <div class="card fs_card-specifications">
        <div class="card-body">
            <div class="card-title"><h3><?php echo $language['site_description_text']; ?></h3></div>
        <?php if (checkForEmpty($row['year_built'])) { ?>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['year_built_text_nocolon']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row['year_built'] !== '' ? $row['year_built'] : $language['na_text']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['zoned'])) {
        ?>            
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['building_zoned_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row['zoned'] !== '' ? $row['zoned'] : $language['na_text']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['average_office'])) {
        ?>            
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['avg_office_percent_area_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row['average_office'] !== '' ? $row['average_office'] : $language['na_text']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['number_of_buildings'])) {
        ?>            
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['number_of_buildings_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row['number_of_buildings'] !== '' ? $row['number_of_buildings'] : $language['na_text']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['number_of_units'])) {
        ?>            
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['number_of_units_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row['number_of_units'] !== '' ? $row['number_of_units'] : $language['na_text']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['environmental_certification_notes'])) {
        ?>            
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['environmental_certification_notes_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row['environmental_certification_notes'] !== '' ? $row['environmental_certification_notes'] : $language['na_text']); ?></div>
            </div>
        </div>
        <?php } ?>
    </div>
    
<?php } ?>
    
<?php if (!checkForEmpty($row['total_space']) && !checkForEmpty($row['warehouse_size']) && !checkForEmpty($row['total_industrial_space']) && !checkForEmpty($row['available_industrial_space']) && !checkForEmpty($row['total_office_space']) && !checkForEmpty($row['available_office_space'])) {
    // this section is emtpy so skip it
} else { ?>  
    <div class="card fs_card-specifications">
        <div class="card-body">
            <div class="card-title"><h3><?php echo $language['building_size_title_text']; ?> (<?php echo $language['building_information_sq_ft_trans']; ?>)</h3></div>
        <?php if (checkForEmpty($row['total_space'])) { ?>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['total_space_text_nocolon']; ?></div>
                <?php $total_space = format_numbers($row['total_space'], $language); ?>
                <div class="col fs_specification-value"><?php echo ($total_space !== '0' ? $total_space .' '. $language['building_information_sq_ft_trans']  : $language['na_text']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['warehouse_size'])) {
        ?>            
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['warehouse_size_text']; ?></div>
                <?php $warehouse_size = format_numbers($row['warehouse_size'], $language); ?>
                <div class="col fs_specification-value"><?php echo ($warehouse_size !== '0' ? $warehouse_size : $language['na_text']); ?></div>
            </div>
            
            <div class="card-title-type"><h4><?php echo $language['building_type_industrial']; ?></h4></div>
        <?php }
        
        if (checkForEmpty($row['total_industrial_space'])) {
        ?>            
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['total_industrial_space_text']; ?></div>
                <?php $total_industrial_space = format_numbers($row['total_industrial_space'], $language); ?>
                <div class="col fs_specification-value"><?php echo ($total_industrial_space !== '0' ? $total_industrial_space .' '. $language['building_information_sq_ft_trans'] : $language['na_text']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['available_industrial_space'])) {
        ?>            
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['avl_industrial_space_text']; ?></div>
                <?php $available_industrial_space = format_numbers($row['available_industrial_space'], $language); ?>
                <div class="col fs_specification-value"><?php echo ($available_industrial_space !== '0' ? $available_industrial_space .' '. $language['building_information_sq_ft_trans']  : $language['na_text']); ?></div>
            </div>
            
            <div class="card-title-type"><h4><?php echo $language['building_type_office']; ?></h4></div>
        <?php }
        
        if (checkForEmpty($row['total_office_space'])) {
        ?>            
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['total_office_space_text']; ?></div>
                <?php $total_office_space = format_numbers($row['total_office_space'], $language); ?>
                <div class="col fs_specification-value"><?php echo ($total_office_space !== '0' ? $total_office_space .' '. $language['building_information_sq_ft_trans']  : $language['na_text']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['available_office_space'])) {
        ?>            
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['avl_office_space_text']; ?></div>
                <?php $available_office_space = format_numbers($row['available_office_space'], $language); ?>
                <div class="col fs_specification-value"><?php echo ($available_office_space !== '0' ? $available_office_space .' '. $language['building_information_sq_ft_trans']  : $language['na_text']); ?></div>
            </div>
        <?php } ?>
        </div>
    </div>
<?php } ?>
    
<?php if (!checkForEmpty($row['const_ceiling_height']) && !checkForEmpty($row['const_max_door_height']) && !checkForEmpty($row['const_building_plan']) && !checkForEmpty($row['const_shipping_doors_drivein']) && !checkForEmpty($row['const_shipping_doors_truck']) && !checkForEmpty($row['const_rail_loading']) && !checkForEmpty($row['marshalling_area']) && !checkForEmpty($row['dolly_pad']) && !checkForEmpty($row['outside_storage']) && !checkForEmpty($row['storage_notes'])) {
    // this section is emtpy so skip it
} else { ?>     
    <div class="card fs_card-specifications">
        <div class="card-body">
            <div class="card-title"><h3><?php echo $language['technical_title_text']; ?></h3></div>
        <?php if (checkForEmpty($row['const_ceiling_height'])) { ?>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['ceiling_height_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row['const_ceiling_height'] !== '' ? $row['const_ceiling_height'] : $language['na_text']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['const_max_door_height'])) {
        ?>            
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['max_door_height_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row['const_max_door_height'] !== '' ? $row['const_max_door_height'] : $language['na_text']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['const_building_plan'])) {
        ?>            
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['building_plan_text']; ?></div>
                <div class="col fs_specification-value"><?php checkOrX($row['const_building_plan']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['const_shipping_doors_drivein'])) {
        ?>            
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['shipping_doors_drive_in_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row['const_shipping_doors_drivein'] !== '' ? $row['const_shipping_doors_drivein'] : $language['na_text']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['const_shipping_doors_truck'])) {
        ?>            
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['shipping_doors_truck_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row['const_shipping_doors_truck'] !== '' ? $row['const_shipping_doors_truck'] : $language['na_text']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['const_rail_loading'])) {
        ?>            
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['rail_loading_text']; ?></div>
                <div class="col fs_specification-value"><?php checkOrX($row['const_rail_loading']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['marshalling_area'])) {
        ?>            
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['marshalling_area_text']; ?></div>
                <?php $marshalling_area = format_numbers($row['marshalling_area'], $language); ?>
                <div class="col fs_specification-value"><?php echo ($marshalling_area !== '' ? $marshalling_area : $language['na_text']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['dolly_pad'])) {
        ?>            
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['dolly_pad_text']; ?></div>
                <?php $dolly_pad = format_numbers($row['dolly_pad'], $language); ?>
                <div class="col fs_specification-value"><?php echo ($dolly_pad !== '' ? $dolly_pad . ' ' . $language['feetandinches_text'] : $language['na_text']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['outside_storage'])) {
        ?>            
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['outside_storage_text']; ?></div>
                <div class="col fs_specification-value"><?php checkOrX($row['outside_storage']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['storage_notes'])) {
        ?>            
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['outside_storage_notes_text']; ?></div>
                <div class="col fs_specification-value"><?php checkOrX($row['storage_notes']); ?></div>
            </div>
        <?php } ?>
        </div>
    </div>
<?php } ?>
    
<?php if (!checkForEmpty($row['const_available_electrical_volts']) && !checkForEmpty($row['const_available_electrical_amps']) && !checkForEmpty($row['const_hvac_description']) && !checkForEmpty($row['const_power_text']) && !checkForEmpty($row['const_exterior_finish'])) {
    // this section is emtpy so skip it
} else { ?>      

    <div class="card fs_card-specifications">
        <div class="card-body">
            <div class="card-title"><h3><?php echo $language['construction_title_text']; ?></h3></div>
        <?php if (checkForEmpty($row['const_available_electrical_volts'])) { ?>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['avl_electrical_volts_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row['const_available_electrical_volts'] !== '' ? $row['const_available_electrical_volts'] : $language['na_text']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['const_available_electrical_amps'])) {
        ?>            
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['avl_electrical_amps_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row['const_available_electrical_amps'] !== '' ? $row['const_available_electrical_amps'] : $language['na_text']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['const_hvac_description'])) {
        ?>            
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['heating_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row['const_hvac_description'] !== '' ? $row['const_hvac_description'] : $language['na_text']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['const_power_text'])) {
        ?>            
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['power_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row['const_power_text'] !== '' ? $row['const_power_text'] : $language['na_text']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['const_exterior_finish'])) {
        ?>            
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['exterior_finish_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row['const_exterior_finish'] !== '' ? $row['const_exterior_finish'] : $language['na_text']); ?></div>
            </div>
        <?php } ?>
        </div>
    </div>
    
<?php } ?>
    
<?php if (!checkForEmpty($row['safety_sprinkler_system']) && !checkForEmpty($row['parking_total_stalls']) && !checkForEmpty($row['parking_description'])) {
    // this section is emtpy so skip it
} else { ?>      
    <div class="card fs_card-specifications">
        <div class="card-body">
            <div class="card-title"><h3><?php echo $language['safety_and_parking_title_text']; ?></h3></div>
        <?php if (checkForEmpty($row['safety_sprinkler_system'])) { ?>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['sprinkler_system_text']; ?></div>
                <div class="col fs_specification-value"><?php checkOrX($row['safety_sprinkler_system']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['parking_total_stalls'])) {
        ?>            
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['total_stalls_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row['parking_total_stalls'] !== '' ? $row['parking_total_stalls'] : $language['na_text']); ?></div>
            </div>
        <?php }
        
        if (checkForEmpty($row['parking_description'])) {
        ?>            
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['parking_desc_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row['parking_description'] !== '' ? $row['parking_description'] : $language['na_text']); ?></div>
            </div>
        <?php } ?>
        </div>
    </div>
<?php } ?>
    

    
    

<!--    <div class="card fs_card-specifications">
        <div class="card-body">
            <div class="card-title"><h3><?php // echo $language['safety_and_access_text']; ?></h3></div>
            <div class="row">
                <div class="col fs_specification-label"><?php // echo $language['fire_detection_system_text']; ?></div>
                <div class="col fs_specification-value"><?php // checkOrX($row['safety_fire_detection']); ?></div>
            </div>
            <div class="row">
                <div class="col fs_specification-label"><?php // echo $language['sprinkler_system_text']; ?></div>
                <div class="col fs_specification-value"><?php // checkOrX($row['safety_sprinkler_system']); ?></div>
            </div>
            <div class="row">
                <div class="col fs_specification-label"><?php // echo $language['manned_security_text']; ?></div>
                <div class="col fs_specification-value"><?php // checkOrX($row['safety_manned_security']); ?></div>
            </div>
            <div class="row">
                <div class="col fs_specification-label"><?php // echo $language['security_system_text']; ?></div>
                <div class="col fs_specification-value"><?php // checkOrX($row['safety_security_system']); ?></div>
            </div>
            <div class="row">
                <div class="col fs_specification-label"><?php // echo $language['barrier_free_acccess_text']; ?></div>
                <div class="col fs_specification-value"><?php // checkOrX($row['safety_barrier_free']); ?></div>
            </div>
        </div>
    </div>


    <div class="card fs_card-specifications">
        <div class="card-body">
            <div class="card-title"><h3><?php // echo $language['parking_text']; ?></h3></div>
            <div class="row">
                <div class="col fs_specification-label"><?php // echo $language['surface_stalls_text']; ?></div>
                <div class="col fs_specification-value"><?php // echo ($row['parking_surface_stalls'] !== '' ? $row['parking_surface_stalls'] : $language['na_text']); ?></div>
            </div>
            <div class="row">
                <div class="col fs_specification-label"><?php // echo $language['total_stalls_text']; ?></div>
                <div class="col fs_specification-value"><?php // echo ($row['parking_total_stalls'] !== '' ? $row['parking_total_stalls'] : $language['na_text']); ?></div>
            </div>
            <div class="row">
                <div class="col fs_specification-label"><?php // echo $language['above_gnd_stalls_text']; ?></div>
                <div class="col fs_specification-value"><?php // echo ($row['parking_above_stalls'] !== '' ? $row['parking_above_stalls'] : $language['na_text']); ?></div>
            </div>
            <div class="row">
                <div class="col fs_specification-label"><?php // echo $language['below_gnd_stalls_text']; ?></div>
                <div class="col fs_specification-value"><?php // echo ($row['parking_below_stalls'] !== '' ? $row['parking_below_stalls'] : $language['na_text']); ?></div>
            </div>
            <div class="row">
                <div class="col fs_specification-label"><?php // echo $language['below_gnd_ratio_text']; ?></div>
                <div class="col fs_specification-value"><?php // echo ($row['parking_below_ratio'] !== '' ? str_replace('\\', '/', $row['parking_below_ratio']) . ' ' . $language['building_information_sq_ft_trans'] : $language['na_text']); ?></div>
            </div>
        </div>
    </div>




    <div class="card fs_card-specifications">
        <div class="card-body">
            <div class="card-title"><h3><?php // echo $language['public_transport_title_text']; ?></h3></div>
            <div class="row">
                <div class="col fs_specification-label"><?php // echo $language['transit_surface_route_text']; ?></div>
                <div class="col fs_specification-value"><?php // checkOrX($row['public_surface_route']); ?></div>
            </div>
            <div class="row">
                <div class="col fs_specification-label"><?php // echo $language['direct_subway_access_text']; ?></div>
                <div class="col fs_specification-value"><?php // checkOrX($row['public_subway_access']); ?></div>
            </div>
        </div>
    </div>-->

</section>



<!--<section class="fs_section building-notes">
    <div class="card fs_card-notes">
        <div class="card-body">
            <div class="row">
                <div class="col fs_col-card-notes-icon">
                    <i class="material-icons fs_notes-icon">insert_drive_file</i>
                    <h2 class="fs_notes-title">Notes</h2>
                </div>
                <div class="col fs_col-card-notes-text">
                    <h3 class="fs_notes-title"><?php // echo $language['building_description_text']; ?></h3>
                    <p class="fs_notes-text"><?php // echo $building_desc; ?></p>
                    <h3 class="fs_notes-title">Sublease</h3>
                    <p class="fs_notes-text">Sed tempus sed fermentum hendrerit. Suspendisse ullamcorper euismod dignissim. etiam commodo varius lacus, et laoreet neque tristique. Phasellus eleifend urna sed accumsan scelerisque. Nulla quis semper lacus, eget congue eros.</p>
                </div>
            </div>
        </div>
    </div>
</section>-->
