<?php
//		$lang = "en_CA";
//		$lang_id = 1;
//		if (isset($_REQUEST['lang'])){
//			if ($_REQUEST['lang'] == "fr_CA"){
//			//	
//				$lang = "fr_CA";
//				$lang_id = 0;
//			}else{
//		//				
//			}
//		}else{
//		//	
//		}
		
//		if (file_exists('theme/db.php')) {
//			include('theme/db.php');
//		} else if (file_exists('../theme/db.php')) {
//			include('../theme/db.php');
//		}
		
	// Language Query
//
//	$language_query_string = 'select * from languages_dynamic ld, languages l, languages2 l2 where ld.lang_id = ' . $lang_id . ' and l.lang_id = ' . $lang_id . ' and l2.lang_id = ' . $lang_id;
//
//	$language_query = mysql_query($language_query_string)or die("language query error: ". mysql_error());
//	$language = mysql_fetch_array($language_query);
//	
	// Theme Query
//	$theme_query = mysql_query("select * from theme");
//	$theme = mysql_fetch_array($theme_query);				


	//setting locale for time, date, money, etc
//		setlocale(LC_ALL, $language['locale_string']);

	// Custom Google Font		

	if (isset($theme['google_font_code']) && $theme['google_font_code'] != '') {
		echo $theme['google_font_code'];
	}


?>


<style type="text/css">

/*ELEMENTS WITH DB/Theme values ARE ALL AT THE TOP */


<?php if (isset($theme['fontcolor']) && $theme['fontcolor'] != '') {
	$fontcolor = $theme['fontcolor'];
} else {
	$fontcolor = '#676767';
}
?>

<?php if (isset($theme['body_background_color']) && $theme['body_background_color'] != '') {
	$body_background_color = "background-color: ".$theme['body_background_color'].";";
} else {
	$body_background_color = '';
}
?>



<?php if (isset($theme['site_background_image']) && $theme['site_background_image'] != '') {
	
	if (file_exists("theme/".$theme['site_background_image']."")) {
		$sitebackgroundimage = 'theme/'.$theme['site_background_image'];
	} else if (file_exists("../theme/".$theme['site_background_image']."")) {
		$sitebackgroundimage = "../theme/".$theme['site_background_image'];
	}
	
	$site_background_image = "background: url('".$sitebackgroundimage."')";
} else {
	$site_background_image = '';
}
?>





<?php if (isset($theme['site_background_fixed']) && $theme['site_background_fixed'] != '') {
	$site_background_fixed = $theme['site_background_fixed'].';';
} else {
	$site_background_fixed = ';';
}
?>

<?php if (isset($theme['site_background_repeat']) && $theme['site_background_repeat'] != '') {
	$site_background_repeat = "background-repeat: ".$theme['site_background_repeat'].";";
} else {
	$site_background_repeat = '';
}
?>

<?php if (isset($theme['site_background_position']) && $theme['site_background_position'] != '') {
	$site_background_position = "background-position: ".$theme['site_background_position'].";";
} else {
	$site_background_position = '';
}
?>

<?php if (isset($theme['site_padding_top']) && $theme['site_padding_top'] != '') {
	$site_padding_top = "padding-top: ".$theme['site_padding_top'];
} else {
	$site_padding_top = '';
}
?>

<?php if (isset($theme['secondary_page_background_image']) && $theme['secondary_page_background_image'] != '') {
	$secondary_page_background_image = "background-image: url('theme/".$theme['secondary_page_background_image']."')";
} else {
	$secondary_page_background_image = $site_background_image;
}

?>

<?php if (isset($theme['wrapper_width']) && $theme['wrapper_width'] != '') {
	$wrapper_width = $theme['wrapper_width'];
} else {
	$wrapper_width = '960';
}
?>


/*html, body {
	font-family: <?php echo $theme['font_family']; ?>;
	color: <?php echo $fontcolor; ?>;
	<?php // echo $body_background_color; ?>;
}*/



#outerwrapper {
	width: 100%;
	/*padding-bottom: 20px;*/
	<?php echo $site_background_image .' '. $site_background_fixed; ?>
	<?php echo $site_background_repeat; ?>
	<?php echo $site_background_position; ?>
	<?php echo $site_padding_top.'px;'; ?>
}

#building #outerwrapper, #suite #outerwrapper {
	<?php echo $secondary_page_background_image .' '. $site_background_fixed; ?>
	<?php echo $site_background_repeat; ?>
	<?php echo $site_background_position; ?>
	<?php echo $site_padding_top.'px;'; ?>
}






/*#footer {*/
	/*width: <?php //echo $wrapper_width.'px'; ?>;
	height: 100px;
	background: url(../theme/footer-bg.png) 0 0 no-repeat;*/
	/*margin: 20px 0 10px 0;
	margin: 0 auto 0 auto;*/
/*}*/

<?php 

if (array_key_exists ('innerwrapper_background',$theme)){

if ($theme['innerwrapper_background'] != '') {
	$innerwrapper_background = "background: url('theme/".$theme['innerwrapper_background']."') center top repeat-y;";
} else {
	$innerwrapper_background = '';
}}
else {
	$innerwrapper_background = '';
}



?>





.innerwrapper {
	width: <?php echo $wrapper_width; ?>px;
	<?php echo $innerwrapper_background; ?>;
	margin: 0 auto 0 auto;
}



<?php if (isset($theme['content_width']) && $theme['content_width'] != '') {
	$content_width = $theme['content_width'];
} else {
	$content_width = '960';
}
?>


#container {
	width: <?php echo $content_width.'px'; ?>;
	margin: 0 auto 0 auto;
/*	background-color: #fff;*/
}


#content {
	width: <?php echo $content_width.'px'; ?>;
	margin: 0 auto 0 auto;
}

#tab-content {
	width: <?php echo $content_width.'px'; ?>;
	margin: 0 auto 0 auto;
}


#suite #content {
	/*background: url(../images/suite-content-bg.jpg) top left repeat-y;*/
	border-bottom: 1px solid #fff;
	width: <?php echo $content_width.'px'; ?>;
	height: 250px;
	padding: 0;
	margin: 0;
	/*border-top: 2px solid #58A618;
	border-top: 1px solid #7a7a7a;
	border-bottom: 1px solid #7a7a7a;
	background-color: #fff;*/
	margin: 0 auto 20px auto;
}



<?php if (isset($theme['content_top_left_padding_left']) && $theme['content_top_left_padding_left'] != '') {
	$content_top_left_padding_left = "padding-left: " . $theme['content_top_left_padding_left'] . "px";
} else {
	$content_top_left_padding_left = '';
}
?>

#content-top-left {
	float: left;
	width: 250px;
	height: 250px;
	/*border-right: 1px solid #4b74b2;*/
	/*text-align: center;*/
	display: table-cell;
	/*vertical-align: middle;*/
	padding-right: 20px;
	<?php echo $content_top_left_padding_left;?>;
	
}

<?php if ($theme['content_top_left_padding_left'] != '') { ?>

/* if the building/suite pages have left padding in the area above the tabs, they will now have the same below the tabs.*/
table#availabilities {
	<?php echo $content_top_left_padding_left;?>;
}

div.content-left {
	<?php echo $content_top_left_padding_left;?>;
}

div#t1 div.content-left {
	padding-left: 0px;
}

h3.section-title {
	<?php echo $content_top_left_padding_left;?>;
}

ul.gallery {
	<?php echo $content_top_left_padding_left;?>;
}

ul.gallery li {
	margin: 0 10px 20px 0;
}

table#properties {
	margin-left: <?php echo $theme['content_top_left_padding_left']; ?>px;
	width: 95%;
}

div.content-left-contacts {
	<?php echo $content_top_left_padding_left;
	$newwidth = 290 - $theme['content_top_left_padding_left'];
	echo ";\n\r";
	echo "width: " . $newwidth ."px";
	?>;
}


<?php } ?>






<?php if (isset($theme['content_top_right_width']) && $theme['content_top_right_width'] != '') {
	$content_top_right_width = $theme['content_top_right_width'];
} else {
	$content_top_right_width = '688';
}
?>


#content-top-right {
	float: left;
	width: <?php echo $content_top_right_width.'px'; ?>;
}






#headertop {
	/*background: #fff;*/
	margin: 0 auto 0 auto;
	width: <?php echo $wrapper_width.'px'; ?>;
}


<?php if (isset($theme['wrapper_background_color']) && $theme['wrapper_background_color'] != '') {
	$wrapper_background_color = "background: ".$theme['wrapper_background_color'].";";
} else {
	$wrapper_background_color = '';
}
?>

<?php if (isset($theme['wrapper_min_height']) && $theme['wrapper_min_height'] != '') {
	$wrapper_min_height = "min-height: ".$theme['wrapper_min_height']."px ;";
} else {
	$wrapper_min_height = '';
}
?>



#wrapper {
	width: <?php echo $wrapper_width.'px'; ?>;
	margin: 0 auto 0 auto;
	padding: 10px 0 1px 0;
	<?php echo $wrapper_background_color; ?>;
	<?php echo $wrapper_min_height; ?>;
}

<?php if (isset($theme['wrapper_border']) && $theme['wrapper_border'] == 1) { ?>
	.innerwrapper {
		border: 1px solid <?php echo $fontcolor; ?>;
		margin-top: 20px;
		padding: 0 0 1px 0;
	}

	#building .innerwrapper {
		padding:  0 10px 1px 10px;
	}

	#suite .innerwrapper {
		padding: 0 10px 1px 10px;
	}
<?php } ?>

<?php if (isset($theme['wrapper_border']) && $theme['wrapper_border'] == 2) { ?>

	.innerwrapper {
		border: 5px solid #464a52;
		/*margin-top: 20px;*/
		padding: 0 0 1px 0;
	}

<?php } ?>


<?php if (isset($theme['search_result_right_width']) && $theme['search_result_right_width'] != '') {
	$search_result_right_width = $theme['search_result_right_width'];
} else {
	$search_result_right_width = '733';
}
?>

<?php if (isset($theme['search_result_left_width']) && $theme['search_result_left_width'] != '') {
	$search_result_left_width = $theme['search_result_left_width'];
} else {
	$search_result_left_width = '190';
}
?>

<?php 

if (isset($theme['search_results_width']) && $theme['search_results_width'] != '') {
	$search_result_width = $theme['search_results_width'];
} else {
	$search_result_width = $search_result_right_width + $search_result_left_width;
}
	if ($search_result_width < 960) {
		$search_result_width = 960;
	}

?>

#search-results {
	/*background-color: #fff;*/
	padding: 0;
	width: <?php echo $search_result_width."px"; ?>;
	margin: 0 auto 0 auto;
	
}

.search-result {
	width: <?php echo $search_result_width."px" ?>;
	margin: 0 0 30px 0;
}

.search-result-left {
	float: left;
	width: <?php echo $search_result_left_width.'px'; ?>;
	margin: 0 0 0 0;	
}

.search-result-right {
	float: left;
	width: <?php echo $search_result_right_width.'px'; ?>;
	padding: 0 0 0 15px;
}


<?php if (isset($theme['breadcrumb_padding_left']) && $theme['breadcrumb_padding_left'] != '') {
	$breadcrumb_padding_left = "padding-left: ".$theme['breadcrumb_padding_left']."px;";
} else {
	$breadcrumb_padding_left = '';
}
?>

#breadcrumbs {
	padding-bottom:10px;
	float: left;
	<?php echo $breadcrumb_padding_left; ?>;
}

#building #breadcrumbs, #suite #breadcrumbs {
	padding-bottom:10px;
	float: left;
	<?php echo $breadcrumb_padding_left; ?>;
}



#location-map-map {
	width: <?php echo $content_width.'px'; ?>;
	height: 400px;	
}


.top-shadow-middle {
	position: absolute;
	top: 0;
	background:url(../images/shadow-top-middle.png) repeat-x;
	margin:0px 10px 0px 10px;
	width: <?php echo $content_width.'px'; ?>;
	height: 10px;
}


.bottom-shadow-middle {
	position: absolute;
	bottom: 0;
	background:url(../images/shadow-bottom-middle.png) repeat-x;
	margin:0px 10px 0px 10px;
	width: <?php echo $content_width.'px'; ?>;
	height: 10px;
}

<?php
if (isset($theme['broker_updates_self_register']) && $theme['broker_updates_self_register'] == 1) {
	$searchboxheight = "150px";
} else {
	$searchboxheight = "130px";
}
?>



#search-box {
	clear: both;
	height: <?php echo $searchboxheight; ?>;
	margin: 0 0 20px 0;
	border-top: 2px solid <?php echo $theme['secondary_colour_hex']; ?>;
	border-bottom: 2px solid <?php echo $theme['secondary_colour_hex']; ?>;
}

#building-high-rise .navigation a:link,
#building .navigation a:link,
#building-high-rise .navigation a:visited,
#building .navigation a:visited {
	color: <?php echo $theme['primary_colour_hex']; ?>;
	text-decoration: none;
	
}


.search-result .title-bar {
	float: left;
/*	background-color: #fff;*/
	font-size: 1.6em;
	font-weight: bold;
	color: <?php echo $theme['primary_colour_hex']; ?>;
	/*width: 728px;*/
	width: 100%;
	line-height: 35px;
	margin: 0;
<?php /*?>	border-top: 2px solid <?php echo $theme['primary_colour_hex']; ?>;
	border-bottom: 2px solid <?php echo $theme['primary_colour_hex']; ?>;
	border-bottom: 1px solid <?php echo $theme['secondary_colour_hex']; ?>;<?php */?>
	text-transform: uppercase;
}


<?php if (isset($theme['search_result_property_name_color']) && $theme['search_result_property_name_color'] != '') {
	$search_result_property_name_color = $theme['search_result_property_name_color'];
} else {
	$search_result_property_name_color = $theme['primary_colour_hex'];
}
?>




.property-name a:link, .property-name a:visited{
	color:<?php echo $search_result_property_name_color; ?>;
	text-decoration:underline;
}

.property-name a:hover{
	color:<?php echo $theme['primary_colour_hex']; ?>;
	text-decoration:none;
}

#building-address {
	float: left;
	color: <?php echo $theme['primary_colour_hex']; ?>;	
	margin: 0 0 0 20px;
	font-size: 1.3em;
	line-height: 40px;
}

#suite #building-address {
	float: left;
	color: <?php echo $theme['primary_colour_hex']; ?>;	
	margin: 0 0 0 30px;
	font-size: 1.3em;
	line-height: 40px;
}

#suite #building-map-button a {
	color: <?php echo $theme['primary_colour_hex']; ?>;	
}


<?php if (isset($theme['link_color']) && $theme['link_color'] != '') {
	$link_colour = $theme['link_color'];
} else {
	$link_colour = $theme['primary_colour_hex']; 
}

?>

a:link, a:visited {
	color: <?php echo $link_colour; ?>;
	text-decoration: underline;
}


#utilities a:link, #utilities a:visited {
	color: <?php echo $theme['primary_colour_hex']; ?>;
	text-decoration: underline;
}


#utilities a:hover {
    text-decoration: none;
}



#accessories a:link, #accessories a:visited {
	color: <?php echo $theme['primary_colour_hex']; ?>;
	text-decoration: underline;
}


#accessories a:hover {
    text-decoration: none;
}


a.modal-button {
	background: <?php echo $theme['primary_colour_hex']; ?>;
	border: 1px solid <?php echo $theme['primary_colour_hex']; ?>;
	padding: 5px;
	color: #fff !important;
	text-transform: uppercase;
	text-align: center;
	text-decoration: none;
	width: 50px;
	font-weight: bold;
	margin-right: 5px;
	display: inline-block;
	zoom:1;
	*display:inline;	
}

h1 {
	font-size: 3.6em;
	color: <?php echo $theme['primary_colour_hex']; ?>;
	margin: 0 0 10px 0;
}

#content-right h1 {
	font-size: 3.0em;
	color: <?php echo $theme['primary_colour_hex']; ?>;
	margin: 0;
}


h2 {
	font-size: 1.2em;
	color: <?php echo $theme['primary_colour_hex']; ?>;
	font-weight: bold;
	margin: 0;
	padding: 0 0 5px 0;
	text-transform: uppercase;
	/*border-bottom: 2px solid <?php echo $theme['primary_colour_hex']; ?>;*/
	border-bottom: 1px solid <?php echo $theme['secondary_colour_hex']; ?>;
}


h2.date {
	font-size: 1.2em;
	color: <?php echo $theme['primary_colour_hex']; ?>;
	float: right;
	margin: 0 0 20px 0;
	border: none;
}

#header-inner-top h2 {
	font-size: 1.8em;
	color: <?php echo $theme['primary_colour_hex']; ?>;
	margin: 0;
	padding-right: 15px;
	display: inline-block;	
}

#building-information h3 {
	color: <?php echo $theme['primary_colour_hex']; ?>;	
}

#building-gallery h3 {
	color: <?php echo $theme['primary_colour_hex']; ?>;	
}

h3.contacts-title {
	font-size: 20px;
	color: <?php echo $theme['primary_colour_hex']; ?>;
	/*text-transform: uppercase;	*/
}

#message-incentive {
	background: <?php echo $theme['primary_colour_hex']; ?>;	
                padding: 5px;	
                margin: 10px 0 0 0;
}

h3.message-title a {
color: #fff;
}


<?php if (isset($theme['searchbox_label_color']) && $theme['searchbox_label_color'] != '') {
	$searchbox_label_color = $theme['searchbox_label_color'];
} else {
	$searchbox_label_color = $theme['fontcolor'];
}
?>


#search-form td.dropdown-label {
	text-align: right;
	width: 100px;
	padding: 0 10px 0 0;
	color: <?php echo $searchbox_label_color; ?>;
	font-size: 10px;
}

#search-form td.dropdown-label-narrow {
	text-align: right;
	width: 50px;
	padding: 0 10px 0 0;
	color: <?php echo $searchbox_label_color; ?>;
	font-size: 10px;
}

<?php if (isset($theme['searchbox_color']) && $theme['searchbox_color'] != '') {
	$searchbox_color = $theme['searchbox_color'];
} else {
	$searchbox_color = $theme['primary_colour_hex'];
}
?>


#search-form select, .button {
	font-family: <?php echo $theme['font_family']; ?>;	
}



select {
	border: 1px solid <?php echo $searchbox_color; ?>;
	width: 125px;
}


<?php if (isset($theme['button_text_color']) && $theme['button_text_color'] != '') {
	$button_text_color = $theme['button_text_color'];
} else {
	$button_text_color = $fontcolor;
}

if (isset($theme['button_text_color_hover']) && $theme['button_text_color_hover'] != '') {
	$button_text_color_hover = $theme['button_text_color_hover'];
} else {
	$button_text_color_hover = $theme['button_text_color'];
}
?>



<?php if (isset($theme['button_background_color']) && $theme['button_background_color'] != '') {
	$button_background_color = $theme['button_background_color'];
} else {
	$button_background_color = $theme['primary_colour_hex'];
}

if (isset($theme['button_background_color_hover']) && $theme['button_background_color_hover'] != '') {
	$button_background_color_hover = $theme['button_background_color_hover'];
} else {
	$button_background_color_hover = $theme['secondary_colour_hex'];
}
?>


#search-form .button {
	color: <?php echo $button_text_color; ?>;
	background-color: <?php echo $button_background_color; ?>;
	border:	none;
	padding: 3px 11px 3px 12px;
	border-width: 1px;
	font-size: 10px;
	margin: 0 13px 0 0;
	/* height: 20px; */
	cursor: pointer;
	float: right;
}

#search-form .button:hover {
	color: <?php echo $button_text_color_hover; ?>;
	background-color: <?php echo $button_background_color_hover; ?>;
}


#search-form .button-french {
	color: <?php echo $button_text_color; ?>;
	background-color: <?php echo $button_background_color; ?>;
	border:	none;
	padding: 3px 11px 3px 12px;
	border-width: 1px;
	font-size: 10px;
	margin: 0 13px 0 0;
	/* height: 20px; */
	cursor: pointer;
	float: right;
}

#search-form .button-french:hover {
	color: <?php echo $button_text_color_hover; ?>;
	background-color: <?php echo $button_background_color_hover; ?>;
}



#search-form .button_reg {
	color: <?php echo $button_text_color; ?>;
	background-color: <?php echo $button_background_color; ?>;
	border:	none;
	padding: 3px 6px 3px 6px;
	border-width: 1px;
	font-size: 10px;
	margin: 0 13px 0 0;
	/* height: 20px; */
	cursor: pointer;
	float: right;
}

#search-form .button_reg:hover {
	color: <?php echo $button_text_color_hover; ?>;
	background-color: <?php echo $button_background_color_hover; ?>;
}



#search-form .button-french_reg {
	color: <?php echo $button_text_color; ?>;
	background-color: <?php echo $button_background_color; ?>;
	border:	none;
	padding: 3px 14px 3px 15px;
	border-width: 1px;
	font-size: 10px;
	margin: 0 13px 0 0;
	/* height: 20px; */
	cursor: pointer;
	float: right;
}

#search-form .button-french_reg:hover {
	color: <?php echo $button_text_color_hover; ?>;
	background-color: <?php echo $button_background_color_hover; ?>;
}







/* Narrow results */

<?php if (isset($theme['narrow_padding_left']) && $theme['narrow_padding_left'] != '') {
	$narrow_padding_left = "padding-left: ".$theme['narrow_padding_left'].'px'.";";
} else {
	$narrow_padding_left = '';
}
?>

#narrow-results {
	float: left;
	margin: 0 20px 20px 0;	
	<?php echo $narrow_padding_left; ?>;
}


#narrow-results a {
	display: block;
	padding: 5px;
	color: <?php echo $button_text_color; ?>;
	background: <?php echo $button_background_color; ?>;
	border: none;
	text-decoration: none;	
}

#narrow-results a:focus,
#narrow-results a:active,
#narrow-results a:hover {
	color: <?php echo $button_text_color_hover; ?>;
	background-color: <?php echo $button_background_color_hover; ?>;
}

#footer-bottom .navigation .back a:link, #footer-bottom .navigation .back a:visited {
	text-transform: uppercase;
	margin: 0px -18px 10px 0;
	float: left;
        text-decoration:underline;
}

#footer-bottom .navigation .back a:hover {
	text-transform: uppercase;
	margin: 0px -18px 10px 0;
	float: left;
        text-decoration:none;
}




<?php if (isset($theme['accessories_padding_right']) && $theme['accessories_padding_right'] != '') {
	$accessories_padding_right = "padding-right: ".$theme['accessories_padding_right'].'px'.";";
} else {
	$accessories_padding_right = '';
}
?>


#accessories {
	float: right;
	<?php echo $accessories_padding_right; ?>;
}

#building #accessories, #suite #accessories{
	float: right;
	<?php echo $accessories_padding_right; ?>;
}





#content-top h1 {
	font-size: 36px;
	color: <?php echo $theme['primary_colour_hex']; ?>;
	font-weight: bold;	
}

#content-top h3 {
	font-size: 16px;
	color: <?php echo $theme['primary_colour_hex']; ?>;
	font-weight: normal;
	margin: 0;
	padding: 0;
	border: none;
	text-transform: none;	
}

#suite #content h1 {
	font-size: 36px;
	color: <?php echo $theme['primary_colour_hex']; ?>;
	font-weight: bold;	
}

#suite #content h3 {
	font-size: 16px;
	color: <?php echo $theme['primary_colour_hex']; ?>;
	font-weight: normal;
	margin: 0;
	padding: 0;
	border: none;
	text-transform: none;	
}

#suite-info h3 {
	font-size: 16px;
	color: <?php echo $theme['primary_colour_hex']; ?>;
	font-weight: normal;
	margin: 0;
	padding-bottom: 10px;
	border: none;
	text-transform: none;	
}


<?php if (isset($theme['suite_info_title_colour']) && $theme['suite_info_title_colour'] != '') {
	$suite_info_title_colour = $theme['suite_info_title_colour'];
} else {
	$suite_info_title_colour = '#000000';
}
?>



dt.suite-info-title {
	float: left;
	clear: left;
	width: 140px;
	overflow: hidden;
	color: <?php echo $suite_info_title_colour; ?>;
	/*display:inline-block;
	zoom:1;
	*display:inline;*/	
	margin-bottom: 10px;
	font-size: 14px;
}



h3.section-title {
	font-size: 20px;
	color: <?php echo $theme['primary_colour_hex']; ?>;
	margin: 0 0 10px 0;
	text-transform: uppercase;
}



table#properties th {
	color: <?php echo $theme['primary_colour_hex']; ?>;
	text-align: left;
	font-weight: bold;
	text-transform: uppercase;
	font-size: 16px;
	padding-top: 15px;
}

table#availabilities thead {
	background-color: none;
	border-bottom: 1px solid <?php echo $theme['primary_colour_hex']; ?>;
	border-top: 1px solid <?php echo $theme['primary_colour_hex']; ?>;
}


table#availabilities td.icon span.suitemarketing {
	color: <?php echo $theme['primary_colour_hex']; ?>;
	border-style: solid;
	border-color: <?php echo $theme['primary_colour_hex']; ?>;
	border-width: 1px;
	padding: 1px;
	font-size: 8px;
}


<?php if (isset($theme['availabilities_text_color']) && $theme['availabilities_text_color'] != '') {
	$availabilities_text_color = $theme['availabilities_text_color'];
} else {
	$availabilities_text_color = $theme['fontcolor'];
}
?>



table#availabilities td, table#availabilities td p, table#availabilities td span{
	font-size: 14px !important;
	color: <?php echo $theme['availabilities_text_color']; ?> !important;
	
}


<?php if (isset($theme['availabilities_header_text_color']) && $theme['availabilities_header_text_color'] != '') {
	$availabilities_header_text_color = $theme['availabilities_header_text_color'];
} else {
	$availabilities_header_text_color = "#000";
}
?>




table#availabilities th {
	color: <?php echo $availabilities_header_text_color; ?>;
	text-align: center;
	font-weight: bold;
}



<?php if (isset($theme['contiguous_text_colour']) && $theme['contiguous_text_colour'] != '') {
	$contiguous_text_color = $theme['contiguous_text_colour'];
} else {
	$contiguous_text_color = $theme['fontcolor'];
}
?>

span.contiguous {
    color: <?php echo $contiguous_text_color; ?>;    
}







/*-----------------------*/
/*-----------------------*/
/*-----------------------*/
/*-----------------------*/
/*-----------------------*/
/*AR 3.0 CUSTOM THEME COLOURED ELEMENTS*/
/*-----------------------*/
/*-----------------------*/
/*-----------------------*/
/*-----------------------*/
/*-----------------------*/


<?php if (isset($theme['body_background_color']) && $theme['body_background_color'] != '') {
	$body_background_color = "background-color: ".$theme['body_background_color'].";";
} else {
	$body_background_color = '';
}
?>

html, body {
	font-family: <?php echo $theme['font_family']; ?>;
	color: <?php echo $fontcolor; ?>;
	<?php echo $body_background_color; ?>;
}


body {
    	<?php echo $site_background_image .' '. $site_background_fixed; ?>
	<?php echo $site_background_repeat; ?>
	<?php echo $site_background_position; ?>
	<?php echo $site_padding_top.'px;'; ?>
}


        
    <?php if (isset($theme['page_background_color']) && $theme['page_background_color'] != '') {
	$page_background_color = "background: ".$theme['page_background_color'].";";
} else {
	$page_background_color = '';
}
?>

#page {
    <?php echo $page_background_color; ?>;
}







.btn-primary {
	color: #fff !important;
	background-color: <?php echo $theme['primary_colour_hex']; ?>;
	/*border-color: <?php // echo $theme['secondary_colour_hex']; ?>;*/
        border-color: <?php echo $theme['primary_colour_hex']; ?>;
	text-decoration: none !important;
}

.btn-primary:hover, .btn-primary:focus, .btn-primary.focus, .btn-primary:active, .btn-primary.active, .open > .dropdown-toggle.btn-primary {
	color: #fff;
	background-color: <?php echo $theme['secondary_colour_hex']; ?>;
	/*border-color: <?php // echo $theme['primary_colour_hex']; ?>;*/
        border-color: <?php echo $theme['secondary_colour_hex']; ?>;
}

.search-result a.add-remove span.cust_check {
	width: 36px;
	height: 36px;
	margin: 0;
	border: 1px solid <?php echo $theme['secondary_colour_hex']; ?>;
	font-size: 28px;
	padding: 2px 5px 0 6px;
	background: rgba(255,255,255,0.7);
	color: <?php echo $theme['primary_colour_hex']; ?>;
}

.tab-content-footer .icon {
	font-size: 56px;
	text-align: center;
	color: <?php echo $theme['primary_colour_hex']; ?>;
	line-height: 1em;
}

.tab-content-footer .icon-label {
	text-align: center;
	line-height: 1em;
	padding-top: 10px;
	color: <?php echo $theme['primary_colour_hex']; ?>;
}

h3.tab-content-title {
	font-size: 20px;
	color: <?php echo $theme['primary_colour_hex']; ?>;
}

ul.nav.nav-pills li a {
    background-color: <?php echo $theme['primary_colour_hex']; ?>;
}

ul.nav.nav-pills li a:hover,
ul.nav.nav-pills li a:active,
ul.nav.nav-pills li a:focus {
	color: #fff;
	background-color: <?php echo $theme['secondary_colour_hex']; ?>;
}

ul.nav.nav-pills>li.active>a,
ul.nav.nav-pills>li.active>a:hover,
ul.nav.nav-pills>li.active>a:focus {
	color: #fff;
	background-color: <?php echo $theme['secondary_colour_hex']; ?>;
}

.btn-broker-registration {
    margin-bottom: 20px;
    background-color: #fff;
    border-color: <?php echo $theme['secondary_colour_hex']; ?>;
    color: <?php echo $theme['primary_colour_hex']; ?> !important;
}

.btn-broker-registration:hover,
.btn-broker-registration:active,
.btn-broker-registration:focus {
	background-color: #fff;
  	border-color: <?php echo $theme['primary_colour_hex']; ?>;
	color: <?php echo $theme['secondary_colour_hex']; ?> !important;
}
h3.panel-title {
    color: <?php echo $theme['primary_colour_hex']; ?>;
}

h4.panel-title {
    color: <?php echo $theme['primary_colour_hex']; ?>;
}

h4.tab-content-title {
    color: <?php echo $theme['primary_colour_hex']; ?>;
}

/*.btn-languages span.text-disabled {
	color: <?php echo $theme['primary_colour_hex']; ?>;
}
*/
ul.toolbar-nav li .btn-languages span.text-disabled {
    color: <?php echo $theme['secondary_colour_hex']; ?>;
}




ul.toolbar-nav li a:hover,
ul.toolbar-nav li a:active,
ul.toolbar-nav li a:focus {
	background-color: <?php echo $theme['primary_colour_hex']; ?>;
	color: #fff;	
}

div#breadcrumbs a {
    color: <?php echo $fontcolor; ?>;
    text-decoration: none;
    font-weight: 500;
    font-size: 16px;
}

div#breadcrumbs a:hover,
div#breadcrumbs a:active,
div#breadcrumbs a:focus{
    color: <?php echo $theme['primary_colour_hex']; ?>;
    text-decoration: underline;
    font-weight: 500;
    font-size: 16px;
}

.bg-info {
    background-color: <?php echo $theme['tertiary_colour_hex']; ?>;
}


i.fa.fa-fw.fa-toggle-on,
i.fa.fa-fw.fa-toggle-off {
    color: <?php echo $theme['primary_colour_hex']; ?>;
}


<?php

// Function to dynamically adjust the brigtness of a colour
// Used to create a border for the BU alert on the index page out of the tertiary colour in the db.

function colourBrightness($hex, $percent) {
	// Work out if hash given
	$hash = '';
	if (stristr($hex,'#')) {
		$hex = str_replace('#','',$hex);
		$hash = '#';
	}
	/// HEX TO RGB
	$rgb = array(hexdec(substr($hex,0,2)), hexdec(substr($hex,2,2)), hexdec(substr($hex,4,2)));
	//// CALCULATE 
	for ($i=0; $i<3; $i++) {
		// See if brighter or darker
		if ($percent > 0) {
			// Lighter
			$rgb[$i] = round($rgb[$i] * $percent) + round(255 * (1-$percent));
		} else {
			// Darker
			$positivePercent = $percent - ($percent*2);
			$rgb[$i] = round($rgb[$i] * $positivePercent) + round(0 * (1-$positivePercent));
		}
		// In case rounding up causes us to go to 256
		if ($rgb[$i] > 255) {
			$rgb[$i] = 255;
		}
	}
	//// RBG to Hex
	$hex = '';
	for($i=0; $i < 3; $i++) {
		// Convert the decimal digit to hex
		$hexDigit = dechex($rgb[$i]);
		// Add a leading zero if necessary
		if(strlen($hexDigit) == 1) {
		$hexDigit = "0" . $hexDigit;
		}
		// Append to the hex string
		$hex .= $hexDigit;
	}
	return $hash.$hex;
}

$colour = $theme['tertiary_colour_hex'];
$brightness = -0.8; // 10% darker
$newColour = colourBrightness($colour,$brightness);


?>


.alert-info {
    color: <?php echo $theme['primary_colour_hex']; ?>;
    background-color: <?php echo $theme['tertiary_colour_hex']; ?>;
    border-color: <?php echo $newColour; ?>;
}


/* Medium Devices, Desktops */
@media(min-width:768px){
    

    ul.nav.nav-pills li a {
        /*background-color: #ddd;*/
        background-color: <?php echo $theme['primary_colour_hex']; ?>;
    }	








} 
/*// end @media(min-width:768px)*/


@media (min-width: 1200px) {
    <?php if (isset($theme['page_width']) && $theme['page_width'] != '') {
            $page_width = " max-width: " . $theme['page_width'] . "px";
    } else {
            $page_width = '';
    }
    ?>

    .container {
       <?php echo $page_width; ?> ;
    }

<?php if (isset($theme['body_background_color']) && $theme['page_border'] > 0) { ?>
#page {
    border: <?php echo $theme['page_border'] . 'px'; ?> solid <?php echo $theme['primary_colour_hex']; ?>;
}
<?php } ?>


}











/*-----------------------*/
/*-----------------------*/
/*-----------------------*/
/*-----------------------*/
/*-----------------------*/
/*END AR 3.0 CUSTOM THEME COLOURED ELEMENTS*/
/*-----------------------*/
/*-----------------------*/
/*-----------------------*/
/*-----------------------*/
/*-----------------------*/














/*-----------------------*/
/* END COLOURED ELEMENTS */
/*-----------------------*/
/*-----------------------*/
/*-----------------------*/
/*-----------------------*/
/*-----------------------*/


/* JDIALOGUE CSS */

/*Splash screen*/
#jDialogueSplash
{
	background-color: #737373;
}

/*The whole window*/
.jDialogueWindow
{
	background-color: #fff;
	width: 680px;
	min-height: 120px;
	/*border: 1px solid #535156;*/
	box-shadow : 0px 0px 0px 6px rgba(115,115,115,0.8);
	padding: 10px;
	/*margin-top: -60px; 
    margin-left: -225px; */
}

/*Window title*/
.jDialogueTitle
{
	display: block;
	height: 27px;
	line-height: 27px;
	/*background-color: #6d84b5;*/
	font-weight: bold;
	color: <?php echo $theme['primary_colour_hex']; ?>;
	border-top: 10px solid <?php echo $theme['secondary_colour_hex']; ?>;
	border-bottom: 2px solid <?php echo $theme['secondary_colour_hex']; ?>;
	margin: 0 0 20px 0;
	padding: 10px;
	font-size: 18px;
}

#building h1.jDialogueTitle
{
	display: block;
	height: 27px;
	line-height: 27px;
	/*background-color: #6d84b5;*/
	font-weight: bold;
	color: <?php echo $theme['primary_colour_hex']; ?>;
	border-top: 10px solid <?php echo $theme['secondary_colour_hex']; ?>;
	border-bottom: 2px solid <?php echo $theme['secondary_colour_hex']; ?>;
	margin: 0 0 20px 0;
	padding: 10px;
	font-size: 18px;
	text-transform: none;
	float: none;
	text-align: left;
}


/*DIV containing the message*/
.jDialogueMessage
{
	margin: 0;
	font-size: 13px;
	background-color: #fff;
}

#print-pdf-search ul {
	list-style: none;	
}

#print-pdf-search ul li {
	float: left;
	margin-right: 10px;
	list-style: none;
}

#print-pdf-search ul li label {
	float: left;
	margin: 0 5px 10px 0;
	font-size: 1.2em;
}

#print-pdf-search ul li input[type="radio"] {
	float: left;
	margin-right: 10px;
	border: 1px solid #000;
}

.thumbsContainer {
	
}

.thumbsContainer ul {
	clear: both;
	margin: 10px 0 10px 0;	
}

.thumbsContainer ul li {
	float: left;
	list-style: none;
	margin: 0 5px 0 0;
}

/* Div containing the thumbnails */
/*.jConfirmThumbsContainer {
		
}

ul.jConfirmThumbs {
	list-style: none;
	height: 60px;	
}

ul.jConfirmThumbs li {
	float: left;	
}
*/

/*Paragraph containing the buttons*/
.jDialogueButtonsContainer
{
	height: 42px;
	line-height: 42px;
	width: 100%;
	margin: 0px;
	margin-top: -1px;
	padding: 0px;
	/*border-top: 1px solid #e6e6e6;*/
	text-align: right;
	clear: both;
	border-bottom: 1px solid <?php echo $theme['secondary_colour_hex']; ?>;
}

/*Buttons*/
.jDialogueButtonsContainer input
{
	min-width: 64px;
	min-height: 25px;
	font-size: 13px;
	font-weight: bold;
	cursor: pointer;
}

/*Cancel button*/
.jDialogueCancelButton
{
	margin-right: 10px;
	margin-left: 5px;
	color: #fff;
	background-color: <?php echo $theme['primary_colour_hex']; ?>;
	border: 1px solid <?php echo $theme['primary_colour_hex']; ?>;
}

.jDialogueCancelButton:hover
{
	background-color: <?php echo $theme['secondary_colour_hex']; ?>;
	border: 1px solid <?php echo $theme['secondary_colour_hex']; ?>;
}

/*Valid button*/
.jDialogueValidButton
{
	color: #fff;
	background-color: <?php echo $theme['primary_colour_hex']; ?>;
	border: 1px solid <?php echo $theme['primary_colour_hex']; ?>;
}

.jDialogueValidButton:hover
{
	background-color: <?php echo $theme['secondary_colour_hex']; ?>;
	border: 1px solid <?php echo $theme['secondary_colour_hex']; ?>;
}


/*a.bu_popup {
	background: <?php // echo $theme['primary_colour_hex']; ?>;
	border: 1px solid <?php // echo $theme['secondary_colour_hex']; ?>;
	padding: 3px;
	color: #fff !important;
	text-transform: uppercase;
	display: inline-block;
	padding: 3px 5px 3px 5px;
	border-width: 1px;
	font-size: 10px;
	margin: 2px 13px 0 0;
	float: right;
	text-decoration: none;
}*/

a.bu_popup:hover {
	background: <?php echo $theme['secondary_colour_hex']; ?>;
}


h3#broker {
	font-size: 1.2em;
	color: <?php echo $theme['primary_colour_hex']; ?>;
	font-weight: bold;
	margin: 0;
	text-transform: uppercase;
}

a.bttn {
	background: <?php echo $theme['primary_colour_hex']; ?>;
	border: 1px solid <?php echo $theme['secondary_colour_hex']; ?>;
	padding: 3px;
	color: #fff !important;
	text-transform: uppercase;
	display: inline-block;
	zoom: 1;
	text-align: left;
}

a.bttn:hover {
	text-decoration: none;
}
a.bttn-sm {
	background: <?php echo $theme['primary_colour_hex']; ?>;
	border: 1px solid <?php echo $theme['secondary_colour_hex']; ?>;
	padding: 3px;
	color: #fff !important;
	text-transform: uppercase;
	display: inline-block;
	zoom: 1;
	text-align: left;
	font-size: 10px;
}


a.bttn-sm:hover {
	text-decoration: none;
}

a.subscription_title {
	text-transform: uppercase;
	font-weight: bold;
}

a.subscription_title:hover {
	text-decoration: none;
}


ol.subscriptions {
	font-size: 14px;
	float: left;
}

ol.subscriptions li {
	margin-bottom: 10px;
	line-height: 21px;
}

ul.subscriptions-buttons {
	float: left;
	list-style: none;
}

ul.subscriptions-buttons li {
	list-style: none;
	margin-bottom: 14px;
	margin-left: 14px;
}</style>