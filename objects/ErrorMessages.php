<?php

/**
 * @author Rational Root
 * @see ../main_abstract/SearchData_abstract.php
 * @since March 2014
 * @version 1.6
 * */
class ErrorMessages {
    /*
     * This method echoes the HTML for displaying when no search results are found.
     * <p>
     * @param  {Array} $language
     * @see index.php
     * */

    public function echo_no_results_message($language) {
        $HTML = "";
        $HTML .= '
               <div class="result">
					<div class="search-result">
                        <div class="row">
							<div class="col-sm-3 col-md-2">
                           		<div class="thumbnail-image"><img src="images/no_thumb.jpg" class="img-responsive"></div>
							</div><!-- /.col -->
							<div class="col-sm-9 col-md-10">
       							<div class="title-bar">
          							<h3 class="search-result-title">' . $language['no_matches'] . '</h3>
								</div><!-- /.title-bar -->
								<div class="property-contact">
                                	<div class="contact-name">' . $language['no_matches_advice'] . '</div>
                            	</div>
							</div><!-- /.col -->
						</div><!-- /.row -->	
					</div><!-- /.search-result -->
				</div><!-- /.result -->';
        return $HTML;
    }


    public function echo_no_results_message2($language) {
        $HTML = "";
        $HTML .= '<div class="result"><div class="search-result"><div class="row"><div class="col-sm-3 col-md-2"><div class="thumbnail-image"><img src="images/no_thumb_700.png" class="img-responsive"></div></div><!-- /.col --><div class="col-sm-9 col-md-10"><div class="title-bar"><h3 class="search-result-title">' . $language['no_matches'] . '</h3></div><!-- /.title-bar --><div class="property-contact"><div class="contact-name">' . $language['no_matches_advice'] . '</div></div></div><!-- /.col --></div><!-- /.row --></div><!-- /.search-result --></div><!-- /.result -->';
        return $HTML;
    }
}

?>
