<?php

/*
 * This class is used to generate the message that is to be sent to a subscriber.
 * @author RationalRoot
 * @version AR2.0
 * @since December 2014
 */

class CreateHTML {

    private static $language;
    private static $squarefoot;
    private static $lang_id;
    private static $lang;
    private static $queries;
    private static $numberOfSuitesAdded=0;

    /**
     * This is a constructor method for the CreateHTML class.
     * It sets the $language, $lang_id, and the square foot ($sqrt) unit for a language in question.
     * @param type $language
     * @param type $lang_id
     * @param type $sqft
     */
    public final function __construct($language, $lang, $lang_id, $sqft, $queries) {
        self::$language = $language;
        self::$squarefoot = $sqft;
        self::$lang_id = $lang_id;
        self::$lang = $lang;
        self::$queries = $queries;
    }

    /**
     * The purpose of this method is to create the HTML for the suiteDIFF (the top part).
     * This method is used by the forEachBuildingDriver method
     * Gets the suiteDiffTop
     * @param type $language
     * @param type $Building
     * @return string
     */
    public function getSuiteDiffTop($Building) {
        $email_link_url = '';
        if (self::$language['bu_email_link_url'] != '') {
            $email_link_url = self::$language['bu_email_link_url'];
        } else {
            $email_link_url = self::$language['vacancy_report_website'];
        }


        $eol = "\n";
        $suitedifftop = '<div style="background-color: #D1D6DC;"><table width="725px" class="suitedifftable" style="cellspacing:0;cellpadding:0;border-spacing:0;padding:10px;">'. $eol;
        $suitedifftop .= '<tr><td colspan="7" style="margin:0;padding:10;">'. $eol;


        if (strlen(stristr($email_link_url, "cadillac")) > 0 || strlen(stristr($email_link_url, "oxford")) > 0) {
            //get the building link per building ID
            $buildingQueryString = "SELECT * FROM buildings where building_id=" . $Building['building_id'];
            $buildingQuery = mysql_query($buildingQueryString)or die("building query error: " . mysql_error());
            $buildingByID = mysql_fetch_array($buildingQuery);
            $buildingUri = $buildingByID['uri'];
            $suitedifftop .= '<h3 style="margin:10px 0 -5px 0;"><a target="_blank" href="' . $buildingUri . '">' . $Building['building_name'] . '</a></h3>';
        } 
        
        else if (strlen(stristr($email_link_url, "gwlra")) > 0) {
            $suitedifftop .= '<h3 style="margin:10px 0 -5px 0;"><a target="_blank" href="' . $email_link_url . '/building.php?building='
                    . $Building['building_code'] . '&lang=' . self::$lang . '">' . $Building['building_name'] . '</a></h3>';
        } 
        
        else {
            $suitedifftop .= '<h3 style="margin:10px 0 -5px 0;"><a target="_blank" href="' . $email_link_url . '/building.php?building=' . $Building['building_code'] . '&lang=' . self::$lang . '">' . $Building['building_name'] . '</a></h3>'. $eol;
        }

        $suitedifftop .= '<h4 style="margin:10px 0 -5px 0;">' . $Building['street_address'] . ', ' . $Building['city'] . ', ' . $Building['province'] . '</h4>'. $eol;
        $suitedifftop .= '</td></tr>';
        $suitedifftop .= '<tr class="bu_headerrow" style="background-color:#fff;">';
        $suitedifftop .= '<td style="margin:0;padding:10;font-weight:bold;" align="center">' . self::$language['brokerupdates_table_header_status'] . '</td>'. $eol;
        $suitedifftop .= '<td style="margin:0;padding:10;font-weight:bold;">' . self::$language['brokerupdates_table_header_suite_text'] . '</td>'. $eol;
        $suitedifftop .= '<td style="margin:0;padding:10;font-weight:bold;">' . self::$language['brokerupdates_table_header_area_text'] . '</td>'. $eol;
        $suitedifftop .= '<td style="margin:0;padding:10;font-weight:bold;">' . self::$language['brokerupdates_table_header_contig_area_text'] . '</td>'. $eol;
        $suitedifftop .= '<td style="margin:0;padding:10;font-weight:bold;">' . self::$language['brokerupdates_table_header_availability_text'] . '</td>'. $eol;
        $suitedifftop .= '<td style="margin:0;padding:10;font-weight:bold;">' . self::$language['brokerupdates_table_header_add_rent_total_text'] . '</td>'. $eol;
        $suitedifftop .= '<td width="175px" style="margin:0;padding:10;font-weight:bold;">' . self::$language['brokerupdates_table_header_suite_notes'] . '</td>'. $eol;
        $suitedifftop .= '</tr>'. $eol;
        return $suitedifftop;
    }

    private static function formatSubscription($input) {
        $output = $input;
        //remove everything after </span>
        if (strpos($input, '</span>') !== false) {
            $pos = strpos($input, "</span>");
            $output = substr($input, 0, $pos + 7);
        } else if (strpos($input, '<span style="font-family:') !== false) {
            $output = '';
        } else if (strpos($input, '<span style="font:') !== false) {
            $output = '';
        }
        return $output;
    }

    public function getUpdatedSuite($suiteData, $Suites, $Building) {

    $checkIfLeasedQueryString = "SELECT * from suites_diff WHERE building_id='".$suiteData["yesterday_suites"]["building_id"]
            ."' AND suite_id='".$suiteData["yesterday_suites"]["suite_id"]."' AND leased='true' AND record_source='yesterday'";    //Otherwise, it is new, and does not belong to updated!
    $checkIfLeasedQuery = mysql_query($checkIfLeasedQueryString);
    $isLeased = mysql_num_rows($checkIfLeasedQuery);
    if ($isLeased >0){return "";}        
            
        //format the description
        $Suites['description'] = self::formatSubscription($Suites['description']);


        $email_link_url = '';
        if (self::$language['bu_email_link_url'] != '') {
            $email_link_url = self::$language['bu_email_link_url'];
        } else {
            $email_link_url = self::$language['vacancy_report_website'];
        }


        $theme_query = mysql_query("select * from theme");
        $theme = mysql_fetch_array($theme_query);


        $suite_twodee_query_string = "Select * from files_2d where suite_id ='" . $suiteData['yesterday_suites']['suite_id'] . "'";
        $suite_twodee_query = mysql_query($suite_twodee_query_string);
        $suite_twodee = mysql_fetch_array($suite_twodee_query);
        $twodee_count = mysql_num_rows($suite_twodee_query);

        $suite_threedee_query_string = "Select * from files_3d where suite_id ='" . $Suites['suite_id'] . "'";
        $suite_threedee_query = mysql_query($suite_threedee_query_string);
        $threedee_count = mysql_num_rows($suite_threedee_query);


        $net_rentable_area = $suiteData['netRentData']['net_rentable_area'];
        $availability = $suiteData['suiteAvailabilityData']['availability'];
        $suite_net_rentable_count = $suiteData['netRentData']['suite_net_rentable_count'];
        $suite_contiguous_area_count = $suiteData['contiguousData']['suite_contiguous_area_count'];
        $suite_availability_count = $suiteData['suiteAvailabilityData']['suite_availability_count'];
        $suite_add_rent_total_count = $suiteData['addrentTotalData']['suite_add_rent_total_count'];
        $add_rent_total = $suiteData['addrentTotalData']['add_rent_total'];
        $suite_availability = $suiteData['suite_availability'];
        $nochange_suite_availability = $suiteData['nochange_suite_availability'];


        $eol = "\n";
        $suitediffbottom = '';
        $isLeased = $suiteData['justLeasedData']['leased'];
        $isRemoved = $suiteData['removedSuitesData']['removed'];
        if ($suiteData['removedSuitesData']['building_code']!='' && !is_null($suiteData['removedSuitesData']['building_code'])){
            $isRemoved = true;
        }
        

        // Set up English vs French sqft formatting
        if (self::$lang == "en_CA") {
            $unchanged_suite_area_noformat = $suiteData['yesterday_suites']['net_rentable_area'];
            $unchanged_suite_area = number_format($unchanged_suite_area_noformat);
            $changed_suite_area_noformat = $net_rentable_area;
            $changed_suite_area = number_format($changed_suite_area_noformat);
            $unchanged_suite_contiguous_area_noformat = $suiteData['yesterday_suites']['contiguous_area'];
            $unchanged_suite_contiguous_area = number_format($unchanged_suite_contiguous_area_noformat);
            $changed_suite_contiguous_area_noformat = $suiteData['contiguousData']['contiguous_area'];
            $changed_suite_contiguous_area = number_format($changed_suite_contiguous_area_noformat);
        }
        if (self::$lang == "fr_CA") {
            $unchanged_suite_area_noformat = $suiteData['yesterday_suites']['net_rentable_area'];
            $unchanged_suite_area = number_format($unchanged_suite_area_noformat, 0, ',', ' ');
            $changed_suite_area_noformat = $net_rentable_area;
            $changed_suite_area = number_format($changed_suite_area_noformat, 0, ',', ' ');
            $unchanged_suite_contiguous_area_noformat = $suiteData['yesterday_suites']['contiguous_area'];
            $unchanged_suite_contiguous_area = number_format($unchanged_suite_contiguous_area_noformat, 0, ',', ' ');
            $changed_suite_contiguous_area_noformat = $suiteData['contiguousData']['contiguous_area'];
            $changed_suite_contiguous_area = number_format($changed_suite_contiguous_area_noformat, 0, ',', ' ');
        }



        $color_red = 'color:red;'; //use red when red needs to be used, however if it is leased, then use black!
    
        if ($isLeased == 'true' || $isRemoved == 'true') {
            $color_red = 'color:black;';
            $suitediffbottom .= '<tr style="background-color:#fff; text-decoration: line-through;">';
            $suitediffbottom .= '<td style="margin:0;padding:10px;color:black;text-align:center"><span>' . self::$language['leased_text'] . '</span></td>'. $eol;
        } else {
            $suitediffbottom .= '<tr style="background-color:#fff;">';
            $suitediffbottom .= '<td style="margin:0;padding:10px;color:black;text-align:center">' . self::$language['update_text'] . '</td>'. $eol;
        }

        $suitediffbottom .= '<td style="margin:0;padding:10;">';
        //if client is oxford properties or leased, do not link to a suite
        if (strlen(stristr($email_link_url, "oxford")) > 0 || (strlen(stristr($email_link_url, "cadillacfairview")) > 0) || $isLeased == 'true' || $isRemoved == 'true') {
            $suitediffbottom .= '<a rel="changedsuite" target="_blank">' . $Suites['suite_name'] . '</a>';
        }

        //if client is gwlra and if pdfs exist link to the pdfs
        else if (strlen(stristr($email_link_url, "gwlra")) > 0) {
            //if it links to a corporate website
            if ($theme['result_links_to_corp_website'] == 1 && (strlen($Suites['suite_id']) > 0)) {
                $suitediffbottom .= '<a rel="changedsuite" href="' . self::$language['bu_email_link_url'] . '/suite.php?suiteid='
                        . $Suites['suite_id'] . '&building=' . $Building['building_code'] . '&lang=' . self::$lang . '" target="_blank">'
                        . $Suites['suite_name'] . '</a>';
            } else if ($twodee_count > 0) {
                $suitediffbottom .= '<a rel="changedsuite" href="' . $suite_twodee['plan_uri'] . '" target="_blank">' . $Suites['suite_name'] . '</a>';
            } else if ($threedee_count > 0 && (strlen($Suites['suite_id']) > 0)) {
                $suitediffbottom .= '<a rel="changedsuite" href="' . $email_link_url . '/suite.php?suiteid='
                        . $Suites['suite_id'] . '&suitenum=' . $Suites['suite_name'] . '&building=' . $Building['building_code']
                        . '&lang=' . self::$lang . '" target="_blank">' . $Suites['suite_name'] . '</a>';
            }

            //if there are no PDFs, then link to a corporate website  
            else {
                $suitediffbottom .= '<a rel="changedsuite" href="' . self::$language['bu_email_link_url'] . '/building.php?'
                        . 'building=' . $Building['building_code'] . '&lang=' . self::$lang . '" target="_blank">'
                        . $Suites['suite_name'] . '</a>';
            }
        }

        //if client is anyone else or not leased
        else {
            $suitediffbottom .= '<a rel="changedsuite" href="' . self::$language['vacancy_report_website'] . '/suite.php?suiteid='
                    . $Suites['suite_id'] . '&building=' . $Building['building_code'] . '&lang=' . self::$lang . '" target="_blank">' . $Suites['suite_name'] . '</a>';
        }

        $suitediffbottom .= '</td>'. $eol;

        if (($suite_net_rentable_count > 0) && ($net_rentable_area != '')) {
            $suitediffbottom .= '<td style="margin:0;padding:10;' . $color_red . '">' . $changed_suite_area . '&nbsp;' . self::$squarefoot . '</td>'. $eol;
        } else {
            $suitediffbottom .= '<td style="margin:0;padding:10;">' . $unchanged_suite_area . '&nbsp;' . self::$squarefoot . '</td>'. $eol;
        }

        if (($suite_net_rentable_count > 0) && ($suite_contiguous_area_count > 0)) {
            if (($suite_contiguous_area_count > 0) && ($changed_suite_contiguous_area != '') && (intval($changed_suite_contiguous_area_noformat) > intval($changed_suite_area_noformat) )) {
                $suitediffbottom .= '<td style="margin:0;padding:10;' . $color_red . '">' . $changed_suite_contiguous_area . '&nbsp;' . self::$squarefoot . '</td>'. $eol;
            } else {
                $suitediffbottom .= '<td style="margin:0;padding:10;">&nbsp;</td>';
            }
        }

        if (($suite_net_rentable_count == 0) && ($suite_contiguous_area_count == 0)) {
            if (($unchanged_suite_contiguous_area != '') && (intval($unchanged_suite_contiguous_area_noformat)) > (intval($unchanged_suite_area_noformat))) {
                $suitediffbottom .= '<td style="margin:0;padding:10;">' . $unchanged_suite_contiguous_area . '&nbsp;' . self::$squarefoot . '</td>'. $eol;
            } else {
                $suitediffbottom .= '<td style="margin:0;padding:10;">&nbsp;</td>';
            }
        }

        if (($suite_net_rentable_count == 0) && ($suite_contiguous_area_count > 0)) {
            if (($changed_suite_contiguous_area != '') && ((intval($changed_suite_contiguous_area_noformat)) > (intval($unchanged_suite_area_noformat)))) {
                $suitediffbottom .= '<td style="margin:0;padding:10;' . $color_red . '">' . $changed_suite_contiguous_area . '&nbsp;' . self::$squarefoot . '</td>'. $eol;
            } else {
                $suitediffbottom .= '<td style="margin:0;padding:10;">&nbsp;</td>'. $eol;
            }
        }

        if (($suite_net_rentable_count > 0) && ($suite_contiguous_area_count == 0)) {
            if (($unchanged_suite_contiguous_area != '') && ((intval($unchanged_suite_contiguous_area_noformat)) > (intval($changed_suite_area_noformat)))) {
                $suitediffbottom .= '<td style="margin:0;padding:10;">' . $unchanged_suite_contiguous_area . '&nbsp;' . self::$squarefoot . '</td>'. $eol;
            } else {
                $suitediffbottom .= '<td style="margin:0;padding:10;">&nbsp;</td>'. $eol;
            }
        }


        if (($suite_availability_count > 0) && ($availability != '')) {
            $suitediffbottom .= '<td style="margin:0;padding:10;' . $color_red . '">' . $suite_availability . '</td>'. $eol;
        } else {
            $suitediffbottom .= '<td style="margin:0;padding:10;">' . $nochange_suite_availability . '</td>'. $eol;
        }


        if (($suite_add_rent_total_count > 0) && ($add_rent_total != '')) {
            $suitediffbottom .= '<td style="margin:0;padding:10;' . $color_red . '">' . money_format('%.2n', $add_rent_total) . '</td>'. $eol;
        } else if ($Suites['add_rent_total'] == 0) {
            $suitediffbottom .= '<td style="margin:0;padding:10;">' . '</td>'. $eol;
        } else {
            $suitediffbottom .= '<td style="margin:0;padding:10;">' . money_format('%.2n', floatval($Suites['add_rent_total'])) . '</td>'. $eol;
        }

        $suitediffbottom .= '<td style="margin:0;padding:10;">' . $Suites['description'] . '</td>'. $eol;


        $suitediffbottom .= "</tr>";

        //this is done to enable the sorting of the change types. New comes first, Update second, Lease third
        //The typeOf tells us the ['change_type'] and ['html_code'] of the suite.
        $typeOf = array();

        if ($isLeased == 'true') {
            $typeOf['change_type'] = 'lease';
        } else if ($isRemoved == 'true') {
            $typeOf['change_type'] = 'lease';
        } else {
            $typeOf['change_type'] = 'update';
        }


        $typeOf['html_code'] = $suitediffbottom;
        self::$numberOfSuitesAdded = self::$numberOfSuitesAdded+1;
        return $typeOf;
    }

    /**
     * This fucntion gets the new suite row.
     * @param type $Building
     * @param type $Suites
     * @param type $nochange_suite_availability
     * @return string
     */
    public function getNewSuite($Building, $Suites, $nochange_suite_availability) {  
 
            $checkIfLeasedQueryString = "SELECT * from suites_diff WHERE building_code='".$Suites["building_code"]
            ."' AND suite_id='".$Suites["suite_id"]."' AND leased='true' AND record_source='today'";   //if leased today, then not new!
    $checkIfLeasedQuery = mysql_query($checkIfLeasedQueryString);
    $isLeased = mysql_num_rows($checkIfLeasedQuery);
    if ($isLeased >0){return "";}    
        
        
        //format the description
        $Suites['description'] = self::formatSubscription($Suites['description']);

        $email_link_url = '';
        if (self::$language['bu_email_link_url'] != '') {
            $email_link_url = self::$language['bu_email_link_url'];
        } else {
            $email_link_url = self::$language['vacancy_report_website'];
        }

        $theme_query = mysql_query("select * from theme");
        $theme = mysql_fetch_array($theme_query);


        $suite_twodee_query_string = "Select * from files_2d where suite_id ='" . $Suites['suite_id'] . "'";
        $suite_twodee_query = mysql_query($suite_twodee_query_string);
        $suite_twodee = mysql_fetch_array($suite_twodee_query);
        $twodee_count = mysql_num_rows($suite_twodee_query);

        $suite_threedee_query_string = "Select * from files_3d where suite_id ='" . $Suites['suite_id'] . "'";
        $suite_threedee_query = mysql_query($suite_threedee_query_string);
        $threedee_count = mysql_num_rows($suite_threedee_query);

        if (self::$lang == "en_CA") {
            $new_suite_area = number_format($Suites['net_rentable_area']);
            $new_contiguous_area = number_format($Suites['contiguous_area']);
        }
        if (self::$lang == "fr_CA") {
            $new_suite_area = number_format($Suites['net_rentable_area'], 0, ',', ' ');
            $new_contiguous_area = number_format($Suites['contiguous_area'], 0, ',', ' ');
        }

        if ($Suites['availability'] != "") {
            //make old dates show as 'immediately'                          
            if ($Suites['availability'] < time()) {
                $new_suite_availability = self::$language['immediately_text'];
            } else {
                //show the actual availability date - its in the future
                $new_suite_availability = strftime('%b %Y', $Suites['availability']);
            }
        } else {
            $new_suite_availability = self::$language['immediately_text'];
        }



        $eol = "\n";
        $suitediffbottom = '<tr style="background-color:#fff;">';
        $suitediffbottom .= '<td style="margin:0;padding:10px;color:red;text-align:center"><span style="margin:10px;padding:5px;">' . self::$language['new_text']
                . '</span></td>' . $eol;
        $suitediffbottom .= '<td style="margin:0;padding:10;">';

        //if client  properties, no links to a suite.
        if ((strlen(stristr($email_link_url, "oxford")) > 0) || (strlen(stristr($email_link_url, "cadillacfairview")) > 0)) {
            $suitediffbottom .= '<a rel="newsuite" target="_blank">' . $Suites['suite_name'] . '</a>';
        }

        //if client is gwlra and if pdfs exist link to the pdfs
        else if ((strlen(stristr($email_link_url, "gwlra")) > 0)) {
            //if it links to a corporate website
            if ($theme['result_links_to_corp_website'] == 1 && (strlen($Suites['suite_id']) > 0)) {
                $suitediffbottom .= '<a rel="changedsuite" href="' . self::$language['bu_email_link_url'] . '/suite.php?suiteid='
                        . $Suites['suite_id'] . '&building=' . $Building['building_code'] . '&lang=' . self::$lang . '" target="_blank">'
                        . $Suites['suite_name'] . '</a>';
            } else if ($twodee_count > 0) {
                $suitediffbottom .= '<a rel="newsuite" href="' . $suite_twodee['plan_uri'] . '" target="_blank">' . $Suites['suite_name'] . '</a>';
            } else if ($threedee_count > 0 && (strlen($Suites['suite_id']) > 0)) {
                $suitediffbottom .= '<a rel="newsuite" href="' . self::$language['bu_email_link_url'] . '/suite.php?suiteid='
                        . $Suites['suite_id'] . '&suitenum=' . $Suites['suite_name'] . '&building=' . $Building['building_code']
                        . '&lang=' . self::$lang . '" target="_blank">' . $Suites['suite_name'] . '</a>';
            }



            //if there are no PDFs, then link to a corporate website (fix)
            else {
                $suitediffbottom .= '<a rel="newsuite" href="' . self::$language['bu_email_link_url'] . '/building.php?building=' .
                        $Building['building_code'] . '&lang=' . self::$lang . '" target="_blank">'
                        . $Suites['suite_name'] . '</a>'; 
            }
        }

        //if client is anyone else
        else {
            $suitediffbottom .= '<a rel="newsuite" href="' . self::$language['vacancy_report_website'] . '/suite.php?suiteid='
                    . $Suites['suite_id'] . '&building=' . $Building['building_code'] . '&lang=' . self::$lang . '" target="_blank">' . $Suites['suite_name'] . '</a>';
        }

        $suitediffbottom .= '</td>' . $eol; 
        $suitediffbottom .= '<td style="margin:0;padding:10;">' . $new_suite_area . '&nbsp;' . self::$squarefoot . '</td>' . $eol;

        if ($Suites['contiguous_area'] > $Suites['net_rentable_area']) {
            $suitediffbottom .= '<td style="margin:0;padding:10;">' . $new_contiguous_area . '&nbsp;' . self::$squarefoot . '</td>' . $eol;
        } else {
            $suitediffbottom .= '<td style="margin:0;padding:10;"></td>' . $eol;
        }

        $suitediffbottom .= '<td style="margin:0;padding:10;">' . $nochange_suite_availability . '</td>' . $eol;


        if ($Suites['add_rent_total'] > 0) {
            $suitediffbottom .= '<td style="margin:0;padding:10;">' . money_format('%.2n', floatval($Suites['add_rent_total'])) . '</td>' . $eol;
        } else {
            $suitediffbottom .= '<td style="margin:0;padding:10;">' . '</td>'. $eol;
    }
            $suitediffbottom .= '<td style="margin:0;padding:10;">' . $Suites['description'] . '</td>';
        $suitediffbottom .= "</tr>";

        //this is done just to follow the convention that is used in a getUpdatedSuite method
        //The typeOf tells us the ['change_type'] and ['html_code'] of the suite.
        $typeOf = array();
        $typeOf['change_type'] = 'new';
        $typeOf['html_code'] = $suitediffbottom;
        self::$numberOfSuitesAdded = self::$numberOfSuitesAdded+1;
        return $typeOf;
    }

    /**
     * This function gets the contacts and inserts the data into a message to be sent.
     * @param type $contacts_query
     * @return string
     */
    public function getContacts($contacts_query, $uniqueid) {
        $bu_query = self::$queries->getBuQuery($uniqueid); //this is now mainly used just to get the title of a subscription.
        $bu_data = mysql_fetch_array($bu_query);
        $email_pos = strrpos($bu_data['bu_query_string'], "&email=");
        $bu_query_string_filtered = substr($bu_data['bu_query_string'], 0, $email_pos);
        $bu_name = '&bu_name=' . $bu_data['bu_name'];
        $bu_query_string_filtered = str_replace($bu_name, "", $bu_query_string_filtered);
        $viewLink = '<span style="padding: 10px;"><a href="' . self::$language['vacancy_report_website'] . '/index.php?' . $bu_query_string_filtered . '">' . self::$language['view_all_results'] . '</a></span>';

        $eol = "\n";
        $suitediffbottom = '<tr><td colspan="7">&nbsp;</td></tr>' . $eol;

        $suitediffbottom .= '<tr><td colspan="7">';
        $suitediffbottom .= '<table width="100%"><tr>';
        while ($contacts = mysql_fetch_array($contacts_query)) {

            $suitediffbottom .= '<td style="padding-right: 10px;" >' . $contacts['name'] . '<br />' . $contacts['title'] . '<br />' . $contacts['email'] . '<br />' . $contacts['phone'] . '</td>' . $eol;
        }
        $suitediffbottom .= '</tr></table></div>';
        $suitediffbottom .= '</td></tr>';
        $suitediffbottom .= "</table></div>" . $eol;

        return $suitediffbottom;
    }

    /**
     * This function generates the message to be sent.
     * @param type $multiple_emails
     * @param type $clientname
     * @param type $uniqueid
     * @return string
     */
    public function generateMessage($multiple_emails, $uniqueid) {

        if (count($multiple_emails) > 0) {
            $message = '';
            $message .=$this->getEmailHeader();

            foreach ($multiple_emails as $data) {
                $message .= $data;
            }

            $message .=$this->getEmailFooter($uniqueid);
            return $message;
        } else {
            $message = '<br><br><p>No changes for this search. <a href="bu_list.php">Go Back to the List</a></p>';
            $message .= '<p>(this page is only here for demonstration purposes)</p>';
            return $message;
        }
    }

    /**
     *
     * @param type $clientname
     * @param type $emaildomain
     * @return string
     */
    public function getHeaders($clientname, $emaildomain) {
        $eol = "\n";
        $headers = 'MIME-Version: 1.0' . $eol;
        $headers .= 'Content-type: text/html; charset=utf-8' . $eol;
        $headers .= 'From: ' . ucwords($clientname) . ' Broker Updates <updates@' . $emaildomain . '>' . $eol;
        $headers .= 'X-Mailer: PHP/' . phpversion() . $eol;
        return $headers;
    }

    /**
     * Creates and returns the e-mail header
     * @param type $clientname
     * @return string
     */
    private function getEmailHeader() {
        $eol = "\n";
        $message = '';
        $message .= $eol . '<div><h3><div style="padding:5px 5px 5px 5px; background-color: #4B5F78;color: #fff;font-family: sans-serif;border-radius: 4px 4px 4px 4px;">
' . self::$language['trademark_name']. ' ' . self::$language['email_subject_text'] . ' ' . date('Y-m-d') . '
</div></div>';
        $message .='<p style = "font-size:13px;">' . self::$language['email_first_paragraph_text'] . '</p>' . $eol; //just replaced the red frame string, for now.
        return $message;
    }

    /**
     * Creates and returns the e-mail footer
     * @return string
     */
    private function getEmailFooter($uniqueid) {
        $email_link_url = '';
        if (self::$language['bu_email_link_url'] != '') {
            $email_link_url = self::$language['bu_email_link_url'];
        } else {
            $email_link_url = self::$language['vacancy_report_website'];
        }
        $eol = "\n";
        $message = '';
        $bu_query = self::$queries->getBuQuery($uniqueid); 
        $bu_data = mysql_fetch_array($bu_query);
        $message .= '<table style="width:100%"><tr>';
        $link = $email_link_url . '/index.php?unique=' . $uniqueid . '&' . $bu_data['bu_query_string'] . '&oo_manage=all';
        $message .= '<td><span style="margin: 0px auto;font-size: 15px;"><a target="_blank" href="' . $link .
                '" style="text-decoration: none;color:#A1A9B4;"> ' . self::$language['manage_subscriptions'] . '</a></span></td>';
        $message .= '<td align="right">
<span style="margin: 110px auto;font-size: 15px;">
<span style="color:#000000">' . self::$language['powered_by'] . '</span>
<a target="_blank" href = "https://arcestra.com" style="text-decoration: none; white-space: nowrap;">
<span style="color:#4B5F78;letter-spacing: -3px;">A</span>
<span style="color:#4B5F78;letter-spacing: -3px;">R</span>
<span style="color:#4B5F78;letter-spacing: -3px;">C</span>
<span style="color:#EBD93D;letter-spacing: -3px; font-size: 21px;">e</span>
<span style="color:#40ABBB;letter-spacing: -3px;">S</span>
<span style="color:#584787;letter-spacing: -3px;">T</span>
<span style="color:#38B456;letter-spacing: -3px;">R</span>
<span style="color:#CF2C57;letter-spacing: -3px;">A</span>
</a>
</span>
</td>
</tr>
</table>
<br><br>';
        $message .= "
______________________________________________________<br />
" . self::$language['email_do_not_reply_text1'] . "<br />
" . self::$language['email_do_not_reply_text2'] . "<br />
</body></html>" . $eol;
        return $message;
    }

//adds the tabs to a building data
    public function addTab($uniqueid, $message, $language) {
        $email_link_url = '';
        if (self::$language['bu_email_link_url'] != '') {
            $email_link_url = self::$language['bu_email_link_url'];
        } else {
            $email_link_url = self::$language['vacancy_report_website'];
        }
        
        $bu_query = self::$queries->getBuQuery($uniqueid); //this is now mainly used just to get the title of a subscription.
        $bu_data = mysql_fetch_array($bu_query);
        $email_pos = strrpos($bu_data['bu_query_string'], "&email=");
        $bu_query_string_filtered = substr($bu_data['bu_query_string'], 0, $email_pos);
        $bu_name = '&bu_name=' . $bu_data['bu_name'];
        $bu_query_string_filtered = str_replace($bu_name, "", $bu_query_string_filtered);
        $tab = '<br><h3><span style="background-color: #D1D6DC;padding: 20px;"><a href="'. $email_link_url . '/?' . $bu_data['bu_query_string'] . '">' . strtoupper($bu_data['title']) . '</a></span>' . '</h3>';
//THIS IS WHERE TITLE TRANSLATING SHOULD OCCUR!
        $message = $tab . $message;
        return $message;
    }
    
    
    public function getNumberOfSuitesAdded(){return self::$numberOfSuitesAdded;}

}
