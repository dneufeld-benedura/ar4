<?php

$lang = "en_CA";
$lang_id = 1;
if (isset($_REQUEST['lang'])) {
    if ($_REQUEST['lang'] == "fr_CA") {

        $lang = "fr_CA";
        $lang_id = 0;
    } else {
        
    }
} else {
    
}

function GeraHash($qtd) {
    //$Caracteres includes all of the characters to be used in the randomly generated code. 
    $Caracteres = 'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvXwWxYyZz0123456789';
    $QuantidadeCaracteres = strlen($Caracteres);
    $QuantidadeCaracteres--;

    $Hash = NULL;
    for ($x = 1; $x <= $qtd; $x++) {
        $Posicao = rand(0, $QuantidadeCaracteres);
        $Hash .= substr($Caracteres, $Posicao, 1);
    }

    return $Hash;
}

$filename_unique_portion = GeraHash(30);




include('theme/db.php');
include('objects/Queries.php');
// Initiate the Queries class
$queries = new Queries();

// Language Query


$language = array();
$language_query_string = "select * from languages_dynamic ld, languages l, languages2 l2 where ld.lang_id =? and l.lang_id =? and l2.lang_id =?";
$language_query_statement = $db->prepare($language_query_string);
$language_query_statement->bind_param('iii', mysqli_real_escape_string($db, $lang_id), mysqli_real_escape_string($db, $lang_id), mysqli_real_escape_string($db, $lang_id));
if ($language_query_statement->execute()) {
    $result = $language_query_statement->get_result();
    while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
        $language = $row;
    }
}
$language_query_statement->close();


$theme_query_string = 'select * from theme';
$theme_query = mysql_query($theme_query_string);
$theme = mysql_fetch_array($theme_query);



//setting locale for time, date, money, etc
setlocale(LC_ALL, $language['locale_string']);
date_default_timezone_set('America/New_York');
$date_string = date('Y-m-d');



require('HTTPRequestString.php');

function get_query_str() {
    if ($_SERVER['QUERY_STRING'] == "") {
        return "";
    } else {
        return '&' . $_SERVER['QUERY_STRING'];
    }
}

function get_query_str_print() {
    if ($_SERVER['QUERY_STRING'] == "") {
        return "";
    } else {
        return $_SERVER['QUERY_STRING'];
    }
}

$webQuery = '';


$buildingtype_selected = "All";
if (isset($_REQUEST['SuiteType'])) {
    $buildingtype_selected = $_REQUEST['SuiteType'];
    $webQuery .= "&SuiteType=" . $buildingtype_selected;
}


$suitetype_selected = "All";
if (isset($_REQUEST['SuiteType'])) {
    $suitetype_selected = $_REQUEST['SuiteType'];
}


$size_selected = "All";
$sizeFrom = "";
$sizeTo = "";
if (isset($_REQUEST['Size']) && $_REQUEST['Size'] == "Custom") {
    $size_selected = "Custom";
    $sizeFrom = $_REQUEST['sizeFrom'];
    $sizeTo = $_REQUEST['sizeTo'];
    $webQuery .= "&Size=Custom&sizeFrom=" . $_REQUEST['sizeFrom'] . "&sizeTo=" . $_REQUEST['sizeTo'];
} else if (isset($_REQUEST['Size'])) {
    $size_selected = $_REQUEST['Size'];
    $webQuery .= "&Size=" . $size_selected;
}



if (isset($_REQUEST['Availability'])) {
    $webQuery .= "&Availability=" . $_REQUEST['Availability'];
    $availability_selected = $_REQUEST['Availability'];
    switch ($availability_selected) {

        case 'All':
            $availability_notafter = 2147483640;
            $availability_notbefore = 0;
            break;
        case 'Immediately':
            $availability_notafter = time();
            $availability_notbefore = 0;
            break;
        case 'Under 3 Months':
            $availability_notafter = strtotime('+3 Month');
            $availability_notbefore = 0;
            break;
        case '3-6 Months':
            $availability_notafter = strtotime('+6 Month');
            $availability_notbefore = strtotime('+3 Month');
            break;
        case '6-12 Months':
            $availability_notafter = strtotime('+1 Year');
            $availability_notbefore = strtotime('+6 Month');
            break;
        case 'Over 12 Months':
            $availability_notafter = 2147483640;
            $availability_notbefore = strtotime('+1 Year');
            break;
        case 'Tous':
            $availability_notafter = 2147483640;
            $availability_notbefore = 0;
            break;
        case 'Immédiatement':
            $availability_notafter = time();
            $availability_notbefore = 0;
            break;
        case 'Moins de 3 mois':
            $availability_notafter = strtotime('+3 Month');
            $availability_notbefore = 0;
            break;
        case '3-6 mois':
            $availability_notafter = strtotime('+6 Month');
            $availability_notbefore = strtotime('+3 Month');
            break;
        case '6-12 mois':
            $availability_notafter = strtotime('+1 Year');
            $availability_notbefore = strtotime('+6 Month');
            break;
        case 'Plus de 12 mois':
            $availability_notafter = 2147483640;
            $availability_notbefore = strtotime('+1 Year');
            break;
    }
} else {
    $availability_notafter = 2147483640;
    $availability_notbefore = 0;
}



$industrialsearchelement = 0;

// industrial unit advanced search elements    

$industrialsearchelement = 0;
$industrial_office_space_selected = "All";
if (isset($_REQUEST['SuiteOfficeSize']) && $_REQUEST['SuiteOfficeSize'] != "All") {
    $industrial_office_space_selected = $_REQUEST['SuiteOfficeSize'];
    $webQuery .= "&SuiteOfficeSize=" . $industrial_office_space_selected;
    $industrialsearchelement++;
}

$industrial_clear_height_selected = "All";
if (isset($_REQUEST['SuiteClearHeight']) && $_REQUEST['SuiteClearHeight'] != "All") {
    $industrial_clear_height_selected = $_REQUEST['SuiteClearHeight'];
    $webQuery .= "&SuiteClearHeight=" . $industrial_clear_height_selected;
    $industrialsearchelement++;
}

$industrial_parking_stalls_selected = "All";
if (isset($_REQUEST['SuiteParkingSurfaceStalls']) && $_REQUEST['SuiteParkingSurfaceStalls'] != "All") {
    $industrial_parking_stalls_selected = $_REQUEST['SuiteParkingSurfaceStalls'];
    $webQuery .= "&SuiteParkingSurfaceStalls=" . $industrial_parking_stalls_selected;
    $industrialsearchelement++;
}
//echo "ise: " . $industrialsearchelement . "<br><br>";

$industrial_electrical_volts_selected = "All";
if (isset($_REQUEST['SuiteAvailElecVolts']) && $_REQUEST['SuiteAvailElecVolts'] != "All") {
    $industrial_electrical_volts_selected = $_REQUEST['SuiteAvailElecVolts'];
    $webQuery .= "&SuiteAvailElecVolts=" . $industrial_electrical_volts_selected;
    $industrialsearchelement++;
}
//echo "ise: " . $industrialsearchelement . "<br><br>";

$industrial_electrical_amps_selected = "All";
if (isset($_REQUEST['SuiteAvailElecAmps']) && $_REQUEST['SuiteAvailElecAmps'] != "All") {
    $industrial_electrical_amps_selected = $_REQUEST['SuiteAvailElecAmps'];
    $webQuery .= "&SuiteAvailElecAmps=" . $industrial_electrical_amps_selected;
    $industrialsearchelement++;
}
//echo "ise: " . $industrialsearchelement . "<br><br>";

$industrial_shipping_doors_selected = "All";
if (isset($_REQUEST['SuiteShippingDoors']) && $_REQUEST['SuiteShippingDoors'] != "All") {
    $industrial_shipping_doors_selected = $_REQUEST['SuiteShippingDoors'];
    $webQuery .= "&SuiteShippingDoors=" . $industrial_shipping_doors_selected;
    $industrialsearchelement++;
}
//echo "ise: " . $industrialsearchelement . "<br><br>";

$industrial_drivein_doors_selected = "All";
if (isset($_REQUEST['SuiteDriveInDoors']) && $_REQUEST['SuiteDriveInDoors'] != "All") {
    $industrial_drivein_doors_selected = $_REQUEST['SuiteDriveInDoors'];
    $webQuery .= "&SuiteDriveInDoors=" . $industrial_drivein_doors_selected;
    $industrialsearchelement++;
}
//echo "ise: " . $industrialsearchelement . "<br><br>";

$industrial_parking_selected = "All";
if (isset($_REQUEST['SuiteParking']) && $_REQUEST['SuiteParking'] != "All") {
    $industrial_parking_selected = $_REQUEST['SuiteParking'];
    $webQuery .= "&SuiteParking=" . $industrial_parking_selected;
    $industrialsearchelement++;
}
//echo "ise: " . $industrialsearchelement . "<br><br>";

$industrial_trucktrailer_parking_selected = "All";
if (isset($_REQUEST['SuiteTruckTrailerParking']) && $_REQUEST['SuiteTruckTrailerParking'] != "All") {
    $industrial_trucktrailer_parking_selected = $_REQUEST['SuiteTruckTrailerParking'];
    $webQuery .= "&SuiteTruckTrailerParking=" . $industrial_trucktrailer_parking_selected;
    $industrialsearchelement++;
}
//echo "ise: " . $industrialsearchelement . "<br><br>";
// End industrial unit advanced search elements 



$building_code = $_REQUEST['building'];




//if ($industrialsearchelement > 0) {
//echo "insideadvanced<br><br>";
    $suites_query_string_query = $queries->get_suites_query_string($lang_id, $building_code, $size_selected, $sizeFrom, $sizeTo, $suitetype_selected, "notag", $availability_notbefore, $availability_notafter, $industrial_office_space_selected, $industrial_clear_height_selected, $industrial_shipping_doors_selected, $industrial_drivein_doors_selected, $industrial_electrical_volts_selected, $industrial_electrical_amps_selected, $industrial_parking_stalls_selected, $industrial_trucktrailer_parking_selected, $industrialsearchelement, $industrialsearchelement, $db);

    $suitecount_query_string_query = $queries->get_suites_count_noleased($lang_id, $building_code, $size_selected, $sizeFrom, $sizeTo, $suitetype_selected, "notag", $availability_notbefore, $availability_notafter, $industrial_office_space_selected, $industrial_clear_height_selected, $industrial_shipping_doors_selected, $industrial_drivein_doors_selected, $industrial_electrical_volts_selected, $industrial_electrical_amps_selected, $industrial_parking_stalls_selected, $industrial_trucktrailer_parking_selected, $industrialsearchelement, $industrialsearchelement, $db);

//} else {
//    $suites_query_string_query = $queries->get_suites_query_string($lang_id, $building_code, $size_selected, $sizeFrom, $sizeTo, $suitetype_selected, "notag", $availability_notbefore, $availability_notafter, $db);
//
//    $suitecount_query_string_query = $queries->get_suites_count_noleased($lang_id, $building_code, $size_selected, $sizeFrom, $sizeTo, $suitetype_selected, "notag", $availability_notbefore, $availability_notafter, $db);
//    
//}

        $suite_query_string = $suites_query_string_query[0];
        $a_params = array();
        if ($suite_query_statement = $db->prepare($suite_query_string)) {
            // carry on
        } else {
            trigger_error('Wrong SQL: ' . $suite_query_string . ' Error: ' . $db->error, E_USER_ERROR);
        }
        // prep dynamic variable query
        $param_type = '';
        $n = count($suites_query_string_query[1]);
        for($i = 0; $i < $n; $i++) {
          $param_type .= $suites_query_string_query[1][$i];
        }
         /* with call_user_func_array, array params must be passed by reference */
        $a_params[] = & $param_type;
        for($i = 0; $i < $n; $i++) {
          /* with call_user_func_array, array params must be passed by reference */
          $a_params[] = & $suites_query_string_query[2][$i];
        }

        // call_user_func_array is how we can pass dynamic quantities of variables to mysqli prepared statements.
        call_user_func_array(array($suite_query_statement, 'bind_param'), $a_params);
        $suites_query = array();
        if ($suite_query_statement->execute()){
            $result = $suite_query_statement->get_result();
            while($row = $result->fetch_array(MYSQLI_ASSOC)){ 
                $suites_query[] = $row;
            }
        } else {
            trigger_error('Could not execute preview images query statement: Error: ' . $db->error, E_ALL);
        }
        $suite_query_statement->close();
        
        
        
        
        
        

$buildinginfo = mysql_query("select street_address from buildings where building_code = '" . $_REQUEST['building'] . "'");
$building_info = mysql_fetch_array($buildinginfo);

$pdf_name = $language['pdf_building_filename_text'] . str_replace(' ', '_', $building_info['street_address']) . "-" . $date_string . ".pdf";
$pdf_name = str_replace("&", "-", $pdf_name);
$pdf_name = str_replace(',', '-', $pdf_name);

$url = $language['vacancy_report_website'] . '/includes/';


$url .= $theme['building_pdf_filename'];

$url .= '?' . get_query_str_print();

$r = new HTTPRequestString($url);
$string_to_parse = $r->DownloadToString();


require_once("dompdf/dompdf_config.inc.php");

ob_start();
echo $string_to_parse;
$layoutHTML = ob_get_contents();
ob_end_clean();

$dompdf = new DOMPDF();
$dompdf->set_base_path("/var/www/html/");
$dompdf->load_html($layoutHTML);
$dompdf->set_paper("letter", "portrait");
$dompdf->render();



$k = 0;
$fileArray = array();
$datadir = "temppdf/";
if ($_REQUEST['twodee'] == "include") {

    $list_of_suite_ids = '';
    foreach($suites_query as $suites) {
        $list_of_suite_ids .= "'" . $suites['suite_id'] . "', ";
    }

    $list_of_suite_ids = rtrim($list_of_suite_ids, ", ");



    if ($list_of_suite_ids != '') {

        // name the main building pdf
        $main_building_pdf_name = $filename_unique_portion . "-main.pdf";

        // Using DomPDF, output the Building PDF we just built above into the temppdf/ directory
        file_put_contents($datadir . $main_building_pdf_name, $dompdf->output());

        // Put the main building PDF name into the fileArray first
        $fileArray[] .= $datadir . $main_building_pdf_name;

        $building_pdf_query_string = "Select suite_name, plan_uri from files_2d where suite_id IN (" . $list_of_suite_ids . ") order by suite_name ASC";
        echo "PDF Query STring: " . $building_pdf_query_string . "<br><br>";
        $building_pdf_query = mysql_query($building_pdf_query_string) or die("Building PDF query error: " . mysql_error());

        while ($row = mysql_fetch_array($building_pdf_query)) {

            if ($row['plan_uri'] != "") {
                // take any 2D PDF's and copy them into the temppdf/ directory
                file_put_contents($datadir . $filename_unique_portion . "-" . $k . ".pdf", file_get_contents($row['plan_uri']));

                // while doing this, add each file name to the fileArray using $k to keep track of each file.
                $fileArray[] .= $datadir . $filename_unique_portion . "-" . $k . ".pdf";

                $k++;
            }
        }

        $outputName = $datadir . $pdf_name;

        // GhostScript command to append the the 2D files to the Building PDF  
        $cmd = "gs -q -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -sOutputFile=$outputName ";
        //Add each pdf filename to the end of the command

        foreach ($fileArray as $file) {
            if (file_exists($file)) {
                $cmd .= $file . " ";
            }
        }

        // execute the command if file exists!
        $result = shell_exec($cmd);
        // prepare the combined PDF for download
        $data = file_get_contents($outputName);

        // Send download headers
        header("Content-type: application/octet-stream");
        header("Content-disposition: attachment;filename=$pdf_name");

        // output combined PDF
        echo $data;
    }// end ($list_of_suite_ids != '') {
    else {
        $dompdf->stream($pdf_name);
    }
} else {
    // output Building PDF via Dompdf
    $dompdf->stream($pdf_name);
}

// Post-Download cleanup of temp folder used for combining as-built PDF's
reset($fileArray); // array we just used to determine what as-built files are associated with the building
foreach ($fileArray as $file) {
    if (is_file($file)) {
        unlink($file); // delete file
    }
    unlink($outputName); // then delete the finished, combined file
}
?>
