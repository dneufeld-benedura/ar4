<?php
$lang    = "en_CA";
$lang_id = 1;
if (isset($_REQUEST['lang'])) {
    if ($_REQUEST['lang'] == "fr_CA") {
        
        $lang    = "fr_CA";
        $lang_id = 0;
    } else {
        
        
    }
} else {
    
}

include('theme/db.php');

// Language Query

$language_query_string = 'select * from languages_dynamic ld, languages l, languages2 l2 where ld.lang_id = ' . $lang_id . ' and l.lang_id = ' . $lang_id . ' and l2.lang_id = ' . $lang_id;

$language_query = mysql_query($language_query_string) or die("language query error: " . mysql_error());
$language = mysql_fetch_array($language_query);

$theme_query_string = 'select * from theme';
$theme_query = mysql_query($theme_query_string);
$theme = mysql_fetch_array($theme_query);

//setting locale for time, date, money, etc
setlocale(LC_ALL, $language['locale_string']);
date_default_timezone_set('America/New_York');
$date_string = date('Y-m-d');


require('HTTPRequestString.php');
function get_query_str()
{
    if ($_SERVER['QUERY_STRING'] == "") {
        return "";
    } else {
        return '&' . $_SERVER['QUERY_STRING'];
    }
}
function get_query_str_print()
{
    if ($_SERVER['QUERY_STRING'] == "") {
        return "";
    } else {
        return $_SERVER['QUERY_STRING'];
    }
}

$building_name   = $language['pdf_property_pages_filename_text'] . $date_string . ".pdf";
//$url             = $language['vacancy_report_website'] . '/includes/pdf_propertypages.php?' . get_query_str_print();
$url = $language['vacancy_report_website'] . '/eclipse/pdfs/';
//$url = $language['vacancy_report_website'] . '/';
$url .= "pdf_favourites.php?" . get_query_str_print();


$r               = new HTTPRequestString($url);
$string_to_parse = $r->DownloadToString();

require_once("dompdf/dompdf_config.inc.php");
ob_start();
echo $string_to_parse;

$layoutHTML = ob_get_contents();
ob_end_clean();

$dompdf = new DOMPDF();
$dompdf->set_base_path("/var/www/html/");
$dompdf->load_html($layoutHTML);
$dompdf->set_paper("letter", "landscape");
$dompdf->render();
$dompdf->stream($building_name);

?>
