<?php


class MainQueryBuilder {

    private static $suitesToday;
    private static $suitesYesterday;
    private static $lang_id;
    private static $selectedData;

    public final function __construct($selectedData, $lang_id) {
        self::$suitesToday = self::getSuitesToday();
        self::$suitesYesterday = self::getSuitesYesterday();
        self::$lang_id = $lang_id;
        self::$selectedData = $selectedData;
    }

    public function getBuildingUpdateQueryString() {
        $updates = $this->combineAllUpdateTypes();
        $result = $this->addFilter($updates); 
        return $result;
    }

    private static function getSuitesToday() {
        $suitesToday = "SELECT * FROM suites_diff where record_source = 'today'";
        return $suitesToday;
    }

    private static function getSuitesYesterday() {
        $suitesYesterday = "SELECT * FROM suites_diff where record_source = 'yesterday'";
        return $suitesYesterday;
    }

    private static function getTodayButNotYesterday() {
        $fromQuery = self::$suitesToday;
        $without = "yesterday";
        $returnQuery = self::getOuterJoinSelection($fromQuery, $without);
        return $returnQuery;
    }

    private static function getYesterdayButNotToday() {
        $fromQuery = self::$suitesYesterday;
        $without = "today";
        $returnQuery = self::getOuterJoinSelection($fromQuery, $without);
        return $returnQuery;
    }

    private static function getOuterJoinSelection($fromQuery, $without) {
        $outerJoin = " SELECT *
                        FROM   (" . $fromQuery . ") as today
                        WHERE  today.suite_id NOT IN
                               (
                              SELECT suite_id
                              FROM   suites_diff AS yesterday
                              WHERE  yesterday.record_source='" . $without . "')";
        return $outerJoin;
    }

    private static function getUnleasedSuites() {
        $unleased = "
        SELECT DISTINCT t1.*
        FROM (" . self::$suitesToday . ") t1
        INNER JOIN (" . self::$suitesYesterday . ") t2 ON t2.suite_id = t1.suite_id
        WHERE t2.leased='true' AND t1.leased='false'
        ";
        return $unleased;
    }

    private static function getSuitesTodayAndYesterday() {
        $both = "SELECT DISTINCT today.*
        FROM (" . self::$suitesToday . ") as today
        INNER JOIN (" . self::$suitesYesterday . ") as yesterday
        ON today.suite_id=yesterday.suite_id
        WHERE (today.leased='false' AND (yesterday.leased='false' OR yesterday.leased IS NULL))
        OR today.leased IS NULL
        ";
        return $both;
    }

    private static function getNewSutesQueryString() {
        $newSuites = self::getTodayButNotYesterday();
        $unleased = self::getUnleasedSuites();
        $result = "SELECT DISTINCT * FROM ((" . $unleased . ") UNION (" . $newSuites . ")) as new_suites";
        return $result;
    }

    private static function getUpdatedSuites() {
        $updatedSuites = self::getSuitesTodayAndYesterday();
        return $updatedSuites;
    }

    private static function getLeasedSutesQueryString() {
        $leasedSuitesdiff = self::getYesterdayButNotToday();
        $result = "(SELECT * FROM suites_diff where leased='true' AND record_source = 'today') UNION (" . $leasedSuitesdiff . ")";
        return $result;
    }

    private function combineAllUpdateTypes() {
        $newSuites = self::getNewSutesQueryString();
        $updatedSuites = self::getUpdatedSuites();
        $leasedSuites = self::getLeasedSutesQueryString();
        $unioned = self::unionAllUpdateTypes($newSuites, $updatedSuites, $leasedSuites);
        return $unioned;
    }

    private static function unionAllUpdateTypes($newSuites, $updatedSuites, $leasedSuites) {
        $newAndUpdated = self::makeUnion($newSuites, $updatedSuites);
        $newUpdatedAndLeased = self::makeUnion($leasedSuites, $newAndUpdated);
        return $newUpdatedAndLeased;
    }

    private static function makeUnion($first, $second) {
        $union = "(SELECT * FROM (" . $first . ") as first)" . " UNION (SELECT * FROM (" . $second . ") as second)";
        return $union;
    }

    private function addFilter($changes) {
        $joinBuildingsAndSuites = self::joinBuildingsAndSuites(); 
        $relevantData = self::getRelevantBuildingsAndSuitesData($changes, $joinBuildingsAndSuites);
        $withFilters = self::addFilterSet($relevantData, self::$selectedData);
        $relevantIDs = self::getRelevantIDs($changes, $withFilters);
        return $relevantIDs;
    }

//Adds the rest of filters, together
    private static function addFilterSet($new_changes) {
        $filter = "SELECT DISTINCT * FROM (" . $new_changes . ") as filterchanges WHERE lang = " . self::$lang_id;
        $filter .= self::suiteTypeFilter();
        $filter .= self::provinceFilter();
        $filter .= self::regionFilter();
        $filter .= self::subregionFilter();
        $filter .= self::sizeFilter();
        $filter .= self::availabilityFilter();
        return $filter;
    }

    private static function getRelevantIDs($changes, $filter) {
        $result = "SELECT DISTINCT changes.*
        FROM (" . $changes . ") as changes
        LEFT JOIN (" . $filter . ") as filter
        ON filter.suite_id=changes.suite_id
        WHERE filter.suite_id IS NOT NULL AND changes.lang = " . self::$lang_id;
        return $result;
    }

    private static function joinBuildingsAndSuites() {
        $buildingAndSuites = "SELECT DISTINCT b.suite_name as s_suite_name, b.suite_type as s_suite_type, s.province as s_province, 
        s.region as s_region, s.sub_region as s_sub_region, b.net_rentable_area as s_net_rentable_area,
        b.contiguous_area as s_contiguous_area, s.building_id as s_building_id, b.description as s_description
        FROM buildings as s
        INNER JOIN suites as b
        ON b.building_id=s.building_id
        ";

        return $buildingAndSuites;
    }

    
    public static function getRelevantBuildingsAndSuitesData($changes, $buildingAndSuites) {
        $inner = self::getRelevantBuildingsAndSuitesDataInnerJoin($changes, $buildingAndSuites);
        $left = self::getRelevantBuildingsAndSuitesDataLeftJoin($changes, $buildingAndSuites);  
        $new_changes = self::makeUnion($inner, $left);
        return $new_changes;
    }
    
    
    public static function getRelevantBuildingsAndSuitesDataInnerJoin($changes, $buildingAndSuites) {
        $new_changes = "SELECT DISTINCT 
        changes.*, s.s_suite_name as s_suite_name, s.s_suite_type as s_suite_type, s.s_province as s_province, 
        s.s_region as s_region, s.s_sub_region as s_sub_region, s.s_net_rentable_area as s_net_rentable_area,
        s.s_contiguous_area as s_contiguous_area, s.s_description as s_description
        FROM (" . $changes . ") as changes
        INNER JOIN (" . $buildingAndSuites . ") as s
        ON changes.building_id=s.s_building_id
        ";
        return $new_changes;
    }
    
    
        public static function getRelevantBuildingsAndSuitesDataLeftJoin($changes, $buildingAndSuites) {
        $new_changes = "SELECT DISTINCT 
        changes.*, s.s_suite_name as s_suite_name, s.s_suite_type as s_suite_type, s.s_province as s_province, 
        s.s_region as s_region, s.s_sub_region as s_sub_region, s.s_net_rentable_area as s_net_rentable_area,
        s.s_contiguous_area as s_contiguous_area, s.s_description as s_description
        FROM (" . $changes . ") as changes
        LEFT JOIN (" . $buildingAndSuites . ") as s
        ON changes.building_id=s.s_building_id
        ";
        return $new_changes;
    }

    public static function suiteTypeFilter() {
        $SuiteType_selected = self::$selectedData['SuiteType_selected'];
        $buildings_selection = '';
        if ($SuiteType_selected != "All" && $SuiteType_selected != "Tous") {
            $buildings_selection .= " AND s_suite_type='" . $SuiteType_selected . "' ";
        }
        return $buildings_selection;
    }

    public static function provinceFilter() {
        $province_selected = self::$selectedData['province_selected'];
        $buildings_selection = '';
        if ($province_selected != "All" && $province_selected != "Tous") {
            $buildings_selection = " AND s_province='" . $province_selected . "' ";
        }
        return $buildings_selection;
    }

    public static function regionFilter() {
        $region_selected = self::$selectedData['region_selected'];
        $buildings_selection = '';
        if ($region_selected != "All" && $region_selected != "Tous") {
            $buildings_selection = " and s_region='" . mysql_real_escape_string($region_selected) . "' ";
        }
        return $buildings_selection;
    }

    public static function subregionFilter() {
        $sub_region_selected = self::$selectedData['sub_region_selected'];
        $buildings_selection = '';
        if ($sub_region_selected != "All" && $sub_region_selected != "Tous") {
            $buildings_selection = " and s_sub_region='" . mysql_real_escape_string($sub_region_selected) . "' ";
        }
        return $buildings_selection;
    }

    public static function sizeFilter() {
        $size_selected = self::$selectedData['size_selected'];
        $buildings_selection = '';
        switch ($size_selected) {
            case '5,000 and Under':
                $buildings_selection .= " and (contiguous_area < 5001 or net_rentable_area < 5001) ";
                break;
            case '5,000 - 10,000':
                $buildings_selection .= " and (contiguous_area > 4999 or net_rentable_area > 4999) and (contiguous_area < 10001 or net_rentable_area < 10001) ";
                break;
            case '10,000 - 20,000':
                $buildings_selection .= " and (contiguous_area > 9999 or net_rentable_area > 9999) and (contiguous_area < 20001 or net_rentable_area < 20001) ";
                break;
            case '20,000 - 40,000':
                $buildings_selection .= " and (contiguous_area > 19999 or net_rentable_area > 19999) and (contiguous_area < 40001 or net_rentable_area < 40001) ";
                break;
            case '40,000 and Above':
                $buildings_selection .= " and (contiguous_area > 39999 or net_rentable_area > 39999) ";
                break;
        }
        return $buildings_selection;
        
    }
    
    
    public static function availabilityFilter(){
        $availability_notbefore = self::$selectedData['availability_notbefore'];
        $availability_notafter = self::$selectedData['availability_notafter'];
        $buildings_selection = " and availability > " . $availability_notbefore . " and availability < " . $availability_notafter;
        return $buildings_selection;
    }

}
