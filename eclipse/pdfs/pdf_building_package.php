<?php
include('../objects/Queries.php');
include('../objects/SearchData.php');
include('../objects/LanguageQuery.php');
include('../objects/StatsPackage.php');
include('../theme/db.php');

// Initiate the Queries class
$queries = new Queries();
//Initiate the Search Data class
$searchData = new SearchData();
// initiate the StatsPackage class
$stats = new StatsPackage();


$languageQuery = new LanguageQuery();
$langArray = $languageQuery->getLanguageAndID();
$lang = $langArray[0];
$lang_id = $langArray[1];

$language = array();
$language_query_string = "select * from languages_dynamic ld, languages l, languages2 l2 where ld.lang_id =? and l.lang_id =? and l2.lang_id =?";
$language_query_statement = $db->prepare($language_query_string);
$language_query_statement->bind_param('iii', mysqli_real_escape_string($db, $lang_id), mysqli_real_escape_string($db, $lang_id), mysqli_real_escape_string($db, $lang_id));
if ($language_query_statement->execute()) {
    $result = $language_query_statement->get_result();
    while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
        $language = $row;
    }
}
$language_query_statement->close();

//	// Language Query 2
//	$language_query_string2 = 'select * from languages_dynamic ld, languages l, languages2 l2 where ld.lang_id = ' . $lang_id . ' and l.lang_id = ' . $lang_id . ' and l2.lang_id = ' . $lang_id;
//	$language_query2 = mysql_query($language_query_string2)or die("language query error: ". mysql_error());
//	$language2 = mysql_fetch_array($language_query2);
//setting locale for time, date, money, etc
date_default_timezone_set('America/New_York');
setlocale(LC_ALL, $language['locale_string']);


$province_selected = $searchData->get_province_selected($language, $lang, $db);
$buildingtype_selected = $searchData->get_buildingtype_selected();
$region_selected = $searchData->get_region_selected($language, $lang, $db);
$sub_region_selected = $searchData->get_sub_region_selected($language, $lang, $db);
$size_selected = $searchData->get_size_selected();


$sizeFrom = "";
$sizeTo = "";
if (isset($_REQUEST['Size']) && $_REQUEST['Size'] == "Custom") {
    $size_selected = "Custom";
    $sizeFrom = $_REQUEST['sizeFrom'];
    $sizeTo = $_REQUEST['sizeTo'];
}








$availability_selected_array = $searchData->get_availability_selected();
$availability_notafter = $availability_selected_array['availability_notafter'];
$availability_notbefore = $availability_selected_array['availability_notbefore'];

$building_selected = $_REQUEST['building'];




$buildings_query_string = "select building_index, building_internal_identifier, lang, building_code, building_id, building_type, city, contact_info, country, description, flyer_header, modification_date, street_address, number_of_floors, floors_above_ground, floors_below_ground, available_space, office_area, industrial_area, postal_code, province, province_code, region, sub_region, leasing_node, renovated, retail_area, building_name, measurement_unit, thumbnail, static_map, total_space, uri, year_of_building, parking_stalls, parking_ratio, typical_floor_size, file_name, file_uri, dirty, orientation, add_rent_operating, add_rent_realty, add_rent_power, add_rent_total, truck_doors, drivein_doors, clear_height, power_text, marketing_text, sort_streetnumber, sort_streetname, latitude, longitude, client_name, new_development, no_vacancies from buildings where building_code=? and lang=?";
$buildings_query_statement = $db->prepare($buildings_query_string);
//bind parameters for markers, where (s = string, i = integer, d = double,  b = blob)
$buildings_query_statement->bind_param('si', mysqli_real_escape_string($db, $building_selected), mysqli_real_escape_string($db, $lang_id));
//execute query
$buildings_query_statement->execute();
//bind result variables
$buildings_query_statement->bind_result($building_index, $building_internal_identifier, $lang, $building_code, $building_id, $building_type, $city, $contact_info, $country, $description, $flyer_header, $modification_date, $street_address, $number_of_floors, $floors_above_ground, $floors_below_ground, $available_space, $office_area, $industrial_area, $postal_code, $province, $province_code, $region, $sub_region, $leasing_node, $renovated, $retail_area, $building_name, $measurement_unit, $thumbnail, $static_map, $total_space, $uri, $year_of_building, $parking_stalls, $parking_ratio, $typical_floor_size, $file_name, $file_uri, $dirty, $orientation, $add_rent_operating, $add_rent_realty, $add_rent_power, $add_rent_total, $truck_doors, $drivein_doors, $clear_height, $power_text, $marketing_text, $sort_streetnumber, $sort_streetname, $latitude, $longitude, $client_name, $new_development, $no_vacancies);
//must fetch the info after it's bound. Unsure why but there it is.
$buildings_query_statement->fetch();
$buildings_query_statement->close();


$building_id = $building_id;
$building_name = $building_name;
$building_code = $building_code;
$building_type = $building_type;
$building_street = $street_address;
$building_city = $city;
$building_province = $province;
$building_country = $country;
$building_total_space = $total_space;
$building_measurement_unit = $measurement_unit;
$building_desc = $description;
$building_contact = $contact_info;
$building_header = $flyer_header;
/* if ($building_header == ""){
  $building_header = "../images/no_header.png";
  } */
$building_url = $uri;
$building_static_map = $static_map;
//                $building_thumbnail = $thumbnail;
//		if (strlen($building_thumbnail) < 1){
//			 $building_thumbnail = $language['vacancy_report_website.'/images/no_thumb_700.png';
//		}
$building_availability = $modification_date;
$building_postal_code = $postal_code;
$building_total_space = $total_space;
$building_office_area = $office_area;
$building_retail_area = $retail_area;
$building_industrial_area = $industrial_area;
$building_floors = $number_of_floors;
$building_year_built = $year_of_building;
$building_year_renovated = $renovated;
$building_typical_floor_size = $typical_floor_size;
$building_parking_stalls = $parking_stalls;
$building_parking_ratio = $parking_ratio;
$building_available_space = $available_space;
$building_add_rent_operating = $add_rent_operating;
$building_add_rent_realty = $add_rent_realty;
$building_add_rent_power = $add_rent_power;
$building_add_rent_total = $add_rent_total;
$building_truck_doors = $truck_doors;
$building_drivein_doors = $drivein_doors;
$building_clear_height = $clear_height;
$building_power_text = $power_text;
$building_orientation = $orientation;
$building_marketing_text = stripslashes($marketing_text);

if ($building_header == "") {
//			if ($building_orientation == "PORTRAIT"){
    $building_header = "../images/no_header_portrait.png";
//			}else{
//				$building_header = "../images/no_header_landscape.png";
//			}
}



// for reporting
$today = date("Y/m/d");
$todaytimestamp = strtotime($today);
// Statistics Tracking
$stats->trackBuildingPdfPrint($todaytimestamp, $today, $building_id, $building_code);


// Morguard CONTACTS QUERY
//$morguard_contacts_query = mysql_query("select * from contacts where building_code ='" . $building_code . "' and email like '%morguard%' and lang=" . $lang_id . " order by contact_index asc limit 2");
// Listing Broker CONTACTS QUERY
$listingbroker_contacts_query = "select * from contacts where building_code ='" . $building_code . "' and lang=" . $lang_id . " order by contact_index asc limit 2";
$listingbroker_contacts = mysql_query($listingbroker_contacts_query) or die(mysql_error());
;

// CONTACTS QUERY
//$contacts_query = mysql_query("select * from contacts where building_code ='" . $building_code . "' and lang=" . $lang_id . " order by contact_index asc");
// DOCUMENT QUERY
$documents_query = mysql_query("select * from documents where building_code ='" . $building_code . "' and lang=" . $lang_id);

// SECTIONS QUERY
$sections_query = mysql_query("select * from sections where section_content != '' and building_code='" . $building_code . "' and lang=" . $lang_id);
$sections_count = mysql_num_rows($sections_query);



$building_thumbnail_query_string = $queries->get_building_thumbnail_query_string($lang_id, $building_code);
// process buildings query string
$building_thumbnail__string = $building_thumbnail_query_string[0];
//    $building_thumbnail__string .=" LIMIT ".$postnumbers." OFFSET ".$offset;
$pageCurrent = '';
//    $building_thumbnail__string = str_replace("and allb.building_name like '%none%'", "", $building_thumbnail__string);
if ($building_thumbnail__statement = $db->prepare($building_thumbnail__string)) {
    // carry on
} else {
    trigger_error('Wrong SQL: ' . $building_thumbnail__string . ' Error: ' . $db->error, E_USER_ERROR);
}
// prep dynamic variable query
$param_type = '';
$n = count($building_thumbnail_query_string[1]);
for ($i = 0; $i < $n; $i++) {
    $param_type .= $building_thumbnail_query_string[1][$i];
}

/* with call_user_func_array, array params must be passed by reference */
$a_params = array();
$a_params[] = & $param_type;

for ($i = 0; $i < $n; $i++) {
    /* with call_user_func_array, array params must be passed by reference */
    $a_params[] = & $building_thumbnail_query_string[2][$i];
}



// call_user_func_array is how we can pass dynamic quantities of variables to mysqli prepared statements.
call_user_func_array(array($building_thumbnail__statement, 'bind_param'), $a_params);
$building_thumbnail = array();
if ($building_thumbnail__statement->execute()) {
    $result = $building_thumbnail__statement->get_result();
    while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
        $building_thumbnail[] = $row;
    }
} else {
    trigger_error('Could not execute building_thumbnail query statement: Error: ' . $db->error, E_ALL);
}
$building_thumbnail__statement->close();


    $building_thumbnail_image_name = '';
    if (isset($building_thumbnail['file_name']) && $building_thumbnail['file_name'] != '') {
        $building_thumbnail_image_name = "images/preview_images/" . $building_thumbnail['file_name'];
    } else {
        $building_thumbnail_image_name = "images/no_thumb_700.png";
    }

//	$maplink = '';
//	//Initialize the static map link
////	if ($Building['orientation'] == "PORTRAIT") {
//		$maplink .= 'http://maps.googleapis.com/maps/api/staticmap?size=490x190&zoom=13&format=png32';
////	} else {
////		$maplink .= 'http://maps.googleapis.com/maps/api/staticmap?size=230x204&zoom=13&format=png32';
////	}
//	
//	//Build the map image link
//	$maplink .= '&markers=color:blue%7C'. urlencode($building_street) .'%2C'. urlencode($building_city) .'%2C'. urlencode($building_province);
//	
//			
//			//Finish the maplink code
//			$maplink .= '&sensor=false';
//this bad-boy simply adds a variable to the query string gracefully...sort of
function append_to_query_str($query_string, $var, $appendee) {
    if ($query_string == "" || $query_string == "&") {
        return $var . "=" . $appendee;
    } else {
        return $query_string . "&" . $var . "=" . $appendee;
    }
}

date_default_timezone_set('America/New_York');

$as_built_links = array();


// Theme Query
$theme_query = mysql_query("select * from theme");
$theme = mysql_fetch_array($theme_query);

// Branding Query
$branding_name = '';

$branding_query_string = "Select * from branding";
$branding_query = mysql_query($branding_query_string) or die("branding_query error: " . mysql_error());
$branding_count = mysql_num_rows($branding_query);
if ($branding_count == 0) {
    $branding_name = $language['page_title'];
} else {
    while ($branding = mysql_fetch_array($branding_query)) {
        if ($branding['region'] == $province_selected) {
            $branding_name = $branding['company_name'];
            break; // when we find a match, do not continue
        } else {
            $branding_name = $language['page_title'];
        }
    }
}
?>




<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><?php echo $language['page_title']; ?></title>
        <link rel="stylesheet" type="text/css" href="../css/reset.css" />

        <style type="text/css">



            div.building-information h3 {
                margin-top: -4px;
                padding-top: 0px;
            }


            table.portrait td.right {
                margin-top: 0px;
                padding-top: 0px;
                vertical-align: top;
            }



            table.buildinginfopdf,  table.buildinginfopdf tr,  table.buildinginfopdf td {
                padding: 0;
                margin: 0;
            }


            /*COLOURED CSS ELEMENTS*/

            #header {
                width: 760px;
                background: <?php echo $theme['primary_colour_dompdf_hex']; ?>;
                margin: 0 0 5px 0;
                padding: 0;
                color: #fff;
                visibility: hidden;
            }

            table#sub-header h2 {
                font-size: 1.2em;
                color: <?php echo $theme['primary_colour_dompdf_hex']; ?>;
                text-align: right;
                text-transform: uppercase;
            }


            <?php
            $tertiaryColour = '';
            if ($theme['fourth_colour_hex'] != '') {
                $tertiaryColour = $theme['fourth_colour_hex'];
            } else {
                $tertiaryColour = $theme['secondary_colour_dompdf_hex'];
            };
            ?>


            h3 {
                font-size: 1.0em;
                text-transform: uppercase;
                border-bottom: 2px solid <?php echo $tertiaryColour; ?>;
                margin-bottom: 10px;
                color: <?php echo $theme['primary_colour_dompdf_hex']; ?>;
                text-align: left;
            }

            h3.specifications {
                font-size: 1.2em;
                text-transform: uppercase;
                border-bottom: 2px solid <?php echo $tertiaryColour; ?>;
                margin-bottom: 10px;
                color: <?php echo $theme['primary_colour_dompdf_hex']; ?>;
                text-align: left;
            }

            table#availabilities-heading h3 {
                font-size: 1.2em;
                text-transform: uppercase;
                border-bottom: 2px solid <?php echo $tertiaryColour; ?>;
                margin-bottom: 10px;
                color: <?php echo $theme['primary_colour_dompdf_hex']; ?>;
            }

            .header-border {
                background: <?php echo $theme['secondary_colour_dompdf_hex']; ?>;	
            }

            #message-incentive {
                background: <?php echo $theme['primary_colour_dompdf_hex']; ?>;
                padding: 5px;
                margin: 10px 0 0 0;
                color: #fff;
                font-weight: bold;
                /*text-align: center;*/
            }

            h3.message-title {
                color: #fff;
                border-bottom:none;
                text-transform: none;
            }

            h3.message-title a {
                color: #fff;
            }

            h3.message-title ul {
                list-style-type: disc;
                list-style: disc;
                margin-left:10px;
            }

            h3.message-title p {
                font-size: 16pt;
                size: 16pt;
            }


            <?php
            if ($theme['contiguous_text_colour'] != '') {
                $contiguous_text_color = $theme['contiguous_text_colour'];
            } else {
                $contiguous_text_color = $theme['fontcolor'];
            }
            ?>

            span.contiguous {
                color: <?php echo $contiguous_text_color; ?>;    
            }

            <?php
            $specsheaderbg = '';
            if ($theme['pdfspecs_bgcolour_hex'] != '') {
                $specsheaderbg = $theme['pdfspecs_bgcolour_hex'];
            } else {
                $specsheaderbg = '#4c4c4c';
            }

            $specsheadertextcolour = "#fff";

            if ($theme['specsheadertextcolour'] == "dark") {
                $specsheadertextcolour = "#000";
            } else { // "light"
                $specsheadertextcolour = "#fff";
            }
            ?>

            table.properties thead {
                background-color: <?php echo $specsheaderbg; ?>;
                border-bottom: 1px solid #fff;
            }

            table.properties th {
                color: <?php echo $specsheadertextcolour; ?>;
                text-align: left;
                font-weight: bold;
                padding: 2px;
            }

            table#additional-costs {
                width: 760px;
                border-top: 1px solid <?php echo $tertiaryColour; ?>;
                border-bottom: 1px solid <?php echo $tertiaryColour; ?>;
                font-size: 1.2em;	
                margin-bottom: 20px;
                /*page-break-after: auto;*/
            }

            /*---------------------*/
            /*---------------------*/
            /* END COLOUR ELEMENTS */
            /*---------------------*/
            /*---------------------*/
            /*---------------------*/
            /*---------------------*/
            /*---------------------*/





            @page {
                margin: 160px 30px 120px 30px;
                padding: 0;	
            }

            body {
                margin: 0;
                padding: 0;
                font-family: Arial, sans-serif;
                color: #555555;
                font-size: 10px;
            }



            #header td.header-left {
                width: 275px;	
            }



            #logo {
                margin: 20px 0 20px 20px;	
                width: 210px;
                height: 78px;

            }

            table#header h1 {
                font-size: 2.4em;
                color: #fff;
                display: inline-block;
                /*border-top: 2px solid #fff;
                border-bottom: 2px solid #fff;*/
                text-transform: uppercase;
                text-align: left;
                /*margin: 20px 20px 20px 0;*/

            }

            .titleblock-pin-line-top {
                border-bottom: 1px solid #fff;
            }

            .titleblock-pin-line-bottom {
                border-bottom: 1px solid #fff;
            }

            .titleblock-spacer {
                height:25px;
                width: 400px;
                /*width:660px;*/
            }

            table#sub-header {
                width: 760px;
                margin: 0 0 5px 0;	
            }





            table.portrait {
                width: 760px;
                margin: 0 0 10px 0;	
                padding-top: 0px;
            }

            table.portrait td.left {
                /*width: 300px;*/	
            }

            table.portrait td.right {
                /*width: 460px;*/

            }

            table.portrait td.right table {
                width: 450px;
                float: right;	
            }

            table.portrait td.right table td {
                width: 225px;	
            }

            table#landscape td {
                width: 240px;
                padding: 0 10px 20px 0;	
            }

            table#landscape td.secondary {

                padding: 0 10px 20px 0;	
            }

            table#landscape td.tertiary {
                width: 240px;
                padding: 0 0 20px 0;	
            }

            table#landscape td.image {
                width: 480px;
            }



            .box {
                margin: 0 20px 20px 20px;
            }

            table#landscape td.primary .box {
                margin: 0 40px 20px 0;
            }

            table#landscape td.secondary .box {
                margin: 0 30px 20px 10px;
            }


            #building-contact.box {
                margin-right: 0;
            }

            .building-gallery {
                /*margin: 0;*/
                clear: both;
                /*page-break-before: auto;*/
                /*page-break-inside: avoid;*/

            }

            .building-gallery td {
                vertical-align: top;
                padding-top: 15px;
                padding-right: 15px;	
            }

            .gallery {
                margin: 0;
                clear: both;
                overflow: auto;	
            }



            .building-image {

            }

            div.building-information {
                /*width: 200px;*/
                font-size: 1.2em;	
            }

            #building-contact {
                /*width: 200px;*/
                font-size: 1.2em;	
            }

            .building-description {
                /*width: 520px;*/	
            }

            .building-description.center-box {
                /*width: 420px;*/
            }

            .portrait .building-information,
            .portrait .building-description {

            }

            .contact-details {
                margin: 0 5px 0 5px;	
                float:	left;
            }

            .contact-name {
                color: #000;	
            }

            .contact-title {
                color: #000;
            }

            .second-building-page{
                padding-top:20px;
            }

            #breakit {
                page-break-before: always;
            }

            #breakitforsure {
                page-break-before: always;
            }

            table#availabilities-heading {
                width: 760px;	
            }

            table.availabilities {
                width: 760px;
                font-size: 1.2em;
                padding: 0;
                margin: 0 0 10px 0;
            }

            table.availabilities th {
                padding: 5px;
                text-align: center;
                font-weight: bold;
                color: #000;
            }

            table.availabilities td {
                text-align: center;
                padding: 2px 0 2px 0;
                /*vertical-align: top;*/
            }

            table.availabilities td.description{
                text-align: left;
            }



            table#additional-costs td{
                padding: 5px 0 5px 0;	
            }

            #more-info {
                width: 760px;
                font-size: 1.2em;
                page-break-inside:avoid;
                /*	page-break-after: auto;*/
            }

            /*
            table#more-info td {
                    width: 200px;
                    padding-right: 20px;	
            }
            
            */

            #more-info td.left {
                width: 240px;
                padding: 0 20px 20px 0;	
            }

            #more-info td.right {
                width: 500px;
                padding: 0 0 20px 0;	
            }

            .gallery-thumb {
                float: left;
                display: inline-block;
                width: 230px;
                height: 180px;
                overflow: hidden;
                background: #ccc;
                margin: 0 30px 10px 0;	
            }

            .gallery-thumb.last {
                margin: 0;	
            }



            table.properties {
                width: 100%;
                clear: both;
                /*font-size: 1.4em;*/
                /*margin: 0 0 20px 0;*/
            }



            table.properties tr {
                border-bottom: 1px solid #fff;
            }

            table.properties tr.odd {
                background-color: #f2f5f8;

            }

            table.properties tr.even {
                /*background-color: #e5ecf1;*/

            }







            table.properties th.availability {
                text-align: right;
            }

            table.properties th.contiguous {
                text-align: right;
            }



            table.properties td {
                text-align: left;
                padding: 2px;
                width: 60px;
            }

            table.properties td.availability {

                text-align: right;
            }

            table.properties td.contiguous {

                text-align: right;
            }

            table.properties td.contact {

            }

            .greyedout {
                color: #d3d3d3;
            }


            table.features {
                width: 100%;
                font-size: 1.2em;
            }

            /*.features p {
                margin-bottom: 10px;
            }*/


            /*div#building-image img {
              margin-top: 26px;
            }*/

            .result a,
            .result2 a,
            .availabilities a {
                text-decoration: none;
                color: <?php echo $theme['primary_colour_dompdf_hex']; ?>;
            }

        </style>

    </head>
    <body>

        <?php
        global $attachments;
        $attachments = 0;
        ?>



        <?php
        if ($lang_id == 0) {
            $page_text = "Page {PAGE_NUM} de {PAGE_COUNT}";
        } else {
            $page_text = "Page {PAGE_NUM} of {PAGE_COUNT}";
        }
        ?>

        <script type="text/php">
            if ( isset($pdf) ) {
            $font = Font_Metrics::get_font("Arial", "bold");

            $pdf->page_text(520, 765, "<?php echo $page_text; ?>", $font, 10, array(0,0,0));

            }
        </script>



        <script type="text/php">
            date_default_timezone_set('America/New_York'); 
            setlocale(LC_ALL, <?php echo $language['locale_string']; ?>);

            if ( isset($pdf) ) {
            //$pdf->page_script('	

            // start a new object

            $obj3 = $pdf->open_object();

            // set the font

            $w = $pdf->get_width();
            $h = $pdf->get_height();

            $pdf->image("<?php echo $language['pdflogo']; ?>", 10, 10, 592, 55);

            $font = Font_Metrics::get_font("Arial", "bold");
            $fontnobold = Font_Metrics::get_font("Arial", "regular");

            <?php
// HEX ENCODE POTENTIALLY FRENCH TEXT - HEADER TITLE TEXT BIG
            $str = $building_name;
            $building_name_dompdf = '';
            for ($i = 0; $i < strlen($str); $i++) {
                $building_name_dompdf .= sprintf('\\x%lx', ord($str[$i]));
            }
//
//			
// HEX ENCODE POTENTIALLY FRENCH TEXT - DATE
            $str = strftime('%B %Y');
            $datetext = '';
            for ($i = 0; $i < strlen($str); $i++) {
                $datetext .= sprintf('\\x%lx', ord($str[$i]));
            }

// HEX ENCODE POTENTIALLY FRENCH TEXT - Street Address
            $str = $building_street;
            $addresstext = '';
            for ($i = 0; $i < strlen($str); $i++) {
                $addresstext .= sprintf('\\x%lx', ord($str[$i]));
            }

// HEX ENCODE POTENTIALLY FRENCH TEXT - City
            $str = $building_city;
            $citytext = '';
            for ($i = 0; $i < strlen($str); $i++) {
                $citytext .= sprintf('\\x%lx', ord($str[$i]));
            }

// HEX ENCODE POTENTIALLY FRENCH TEXT - Province
            $str = $building_province;
            $provincetext = '';
            for ($i = 0; $i < strlen($str); $i++) {
                $provincetext .= sprintf('\\x%lx', ord($str[$i]));
            }

// HEX ENCODE POTENTIALLY FRENCH TEXT - Country
            $str = $building_country;
            $countrytext = '';
            for ($i = 0; $i < strlen($str); $i++) {
                $countrytext .= sprintf('\\x%lx', ord($str[$i]));
            }



// HEX ENCODE POTENTIALLY FRENCH TEXT - Branding Line
            $str = $branding_name;
            $branding = '';
            for ($i = 0; $i < strlen($str); $i++) {
                $branding .= sprintf('\\x%lx', ord($str[$i]));
            }
            ?>


            $pagetext2 = "<?php echo strtoupper($building_name_dompdf); ?>";
            $pagetext22 = "<?php echo $addresstext . ' ' . $citytext . ' ' . $provincetext . ' ' . $countrytext; ?>";






            $pagetext3 = "<?php echo $branding . ' | ' . $datetext; ?>";

            //	get text width
            $width = Font_Metrics::get_text_width($pagetext2, $font, 18);
            $width2 = Font_Metrics::get_text_width($pagetext3, $font, 10);
            $width22 = Font_Metrics::get_text_width($pagetext22, $font, 8);

            //	Building Name
            $pdf->text(22, 75, $pagetext2, $font, 18, array(<?php echo $theme['primary_colour_dompdf']; ?>));
            //  Building Address
            $pdf->text(22, 95, $pagetext22, $font, 8, array(<?php echo $theme['primary_colour_dompdf']; ?>));	
            // 	Broker and Date line
            $pdf->text(($w-$width2-20), 75, $pagetext3, $font, 10, array(<?php echo $theme['primary_colour_dompdf']; ?>));



            // close the bject
            $pdf->close_object();
            $pdf->add_object($obj3);



            $allcontactsobj = $pdf->open_object();
            $w = $pdf->get_width();

            $listingtext = "<?php echo $language['listing_broker_contact_text']; ?>";
            $pdf->text(20, 685, $listingtext, $font, 10, array(<?php echo $theme['primary_colour_dompdf']; ?>));

            //	$pdf->line(start x, start y, finish x, finish y, colour, size? )	
            $pdf->line(20, 700, ($w-20), 700, array(0.047,0.49,1.0), 1);


            //			$contactXstart = 700;
            $contactYstart = 20;
            $font = Font_Metrics::get_font("Arial", "bold");
            $fontnobold = Font_Metrics::get_font("Arial", "regular");
            <?php
            $k = 1;
            while ($row2 = mysql_fetch_array($listingbroker_contacts)) {

                $leasingcontact_name = $row2['name'];
                $leasingcontact_title = $row2['title'];
                $leasingcontact_company = $row2['company'];
                $leasingcontact_phone = $row2['phone'];
                $leasingcontact_email = $row2['email'];







                // HEX ENCODE POTENTIALLY FRENCH TEXT (makes the accents work with dompdf)
                $str = $leasingcontact_name;
                $leasing_contact_name = '';
                for ($i = 0; $i < strlen($str); $i++) {
                    $leasing_contact_name .= sprintf('\\x%lx', ord($str[$i]));
                }

                $str = $leasingcontact_title;
                $leasing_contact_title = '';
                for ($i = 0; $i < strlen($str); $i++) {
                    $leasing_contact_title .= sprintf('\\x%lx', ord($str[$i]));
                }

                $str = $leasingcontact_company;
                $leasing_contact_company = '';
                for ($i = 0; $i < strlen($str); $i++) {
                    $leasing_contact_company .= sprintf('\\x%lx', ord($str[$i]));
                }

                $str = $leasingcontact_phone;
                $leasing_contact_phone = '';
                for ($i = 0; $i < strlen($str); $i++) {
                    $leasing_contact_phone .= sprintf('\\x%lx', ord($str[$i]));
                }

                $str = $leasingcontact_email;
                $leasing_contact_email = '';
                for ($i = 0; $i < strlen($str); $i++) {
                    $leasing_contact_email .= sprintf('\\x%lx', ord($str[$i]));
                }



                // leave while open

                echo "$" . "contactXstart = 710;\n";
//				echo "$"."contactYstart = 40;\n";


                echo "$" . "contact_name" . $k . " = \"" . $leasing_contact_name . "\";\n";
                echo "$" . "contact_title" . $k . " = \"" . $leasing_contact_title . "\";\n";
                echo "$" . "contact_company" . $k . " = \"" . $leasing_contact_company . "\";\n";
                echo "$" . "contact_phone" . $k . " = \"" . $leasing_contact_phone . "\";\n";
                echo "$" . "contact_email" . $k . " = \"" . $leasing_contact_email . "\";\n";


                echo "$" . "widthname = Font_Metrics::get_text_width(" . "$" . "contact_name" . $k . ", " . "$" . "font, 8)" . ";\n";
                echo "$" . "widthtitle = Font_Metrics::get_text_width(" . "$" . "contact_title" . $k . ", " . "$" . "fontnobold, 8)" . ";\n";
                echo "$" . "widthcompany = Font_Metrics::get_text_width(" . "$" . "contact_company" . $k . ", " . "$" . "fontnobold, 8)" . ";\n";
                echo "$" . "widthphone = Font_Metrics::get_text_width(" . "$" . "contact_phone" . $k . ", " . "$" . "fontnobold, 8)" . ";\n";
                echo "$" . "widthemail = Font_Metrics::get_text_width(" . "$" . "contact_email" . $k . ", " . "$" . "fontnobold, 8)" . ";\n";

                echo "$" . "pdf->text(" . "$" . "contactYstart, " . "$" . "contactXstart, " . "$" . "contact_name" . $k . ", " . "$" . "font, 8, array(0.0,0.0,0.0))" . ";\n";


                echo "$" . "contactXstart = " . "$" . "contactXstart + 10;\n";
                echo "$" . "pdf->text(" . "$" . "contactYstart, " . "$" . "contactXstart, " . "$" . "contact_title" . $k . ", " . "$" . "fontnobold, 8, array(0.33,0.33,0.33))" . ";\n";

                echo "$" . "contactXstart = " . "$" . "contactXstart + 10;\n";
                echo "$" . "pdf->text(" . "$" . "contactYstart, " . "$" . "contactXstart, " . "$" . "contact_company" . $k . ", " . "$" . "fontnobold, 8, array(0.33,0.33,0.33))" . ";\n";
                echo "$" . "contactXstart = " . "$" . "contactXstart + 10;\n";
                echo "$" . "pdf->text(" . "$" . "contactYstart, " . "$" . "contactXstart, " . "$" . "contact_phone" . $k . ", " . "$" . "fontnobold, 8, array(0.33,0.33,0.33))" . ";\n";
                echo "$" . "contactXstart = " . "$" . "contactXstart + 10;\n";
                echo "$" . "pdf->text(" . "$" . "contactYstart, " . "$" . "contactXstart, " . "$" . "contact_email" . $k . ", " . "$" . "fontnobold, 8, array(0.33,0.33,0.33))" . ";\n";

                echo "$" . "contactYstart = " . "$" . "contactYstart + 200;\n";

                $k++;
            }
            ?>

            $pdf->close_object();
            $pdf->add_object($allcontactsobj);




            }
        </script>


        <?php
//					if ($building_orientation == "PORTRAIT"){
        include('pdf_building_portrait.php');
//					}else{
//						include('pdf_building_landscape.php');
//					}
        ?> 

        <?php
        
        $suitetype_selected = $searchData->get_suitetype_selected($language);	
        if (isset($suitetype_selected) && $suitetype_selected == '') {
            $suitetype_selected = "All";
        }
        
        $advanced_search = 0;

        if ($theme['advanced_search'] == 1) {
            $advanced_search = 1;
        } else {
            $advanced_search = 0;
        }

        $industrialsearchelement = 0;
        if ($advanced_search == 1) {

// industrial unit advanced search elements    
            $webQuery = '';
            $industrialsearchelement = 0;
            $industrial_office_space_selected = "All";
            if (isset($_REQUEST['SuiteOfficeSize']) && $_REQUEST['SuiteOfficeSize'] != "All") {
                $industrial_office_space_selected = $_REQUEST['SuiteOfficeSize'];
                $webQuery .= "&SuiteOfficeSize=" . $industrial_office_space_selected;
                $industrialsearchelement++;
            }

            $industrial_clear_height_selected = "All";
            if (isset($_REQUEST['SuiteClearHeight']) && $_REQUEST['SuiteClearHeight'] != "All") {
                $industrial_clear_height_selected = $_REQUEST['SuiteClearHeight'];
                $webQuery .= "&SuiteClearHeight=" . $industrial_clear_height_selected;
                $industrialsearchelement++;
            }



            $industrial_parking_stalls_selected = "All";
            if (isset($_REQUEST['SuiteParkingSurfaceStalls']) && $_REQUEST['SuiteParkingSurfaceStalls'] != "All") {
                $industrial_parking_stalls_selected = $_REQUEST['SuiteParkingSurfaceStalls'];
                $webQuery .= "&SuiteParkingSurfaceStalls=" . $industrial_parking_stalls_selected;
                $industrialsearchelement++;
            }
//echo "ise: " . $industrialsearchelement . "<br><br>";

            $industrial_electrical_volts_selected = "All";
            if (isset($_REQUEST['SuiteAvailElecVolts']) && $_REQUEST['SuiteAvailElecVolts'] != "All") {
                $industrial_electrical_volts_selected = $_REQUEST['SuiteAvailElecVolts'];
                $webQuery .= "&SuiteAvailElecVolts=" . $industrial_electrical_volts_selected;
                $industrialsearchelement++;
            }
//echo "ise: " . $industrialsearchelement . "<br><br>";

            $industrial_electrical_amps_selected = "All";
            if (isset($_REQUEST['SuiteAvailElecAmps']) && $_REQUEST['SuiteAvailElecAmps'] != "All") {
                $industrial_electrical_amps_selected = $_REQUEST['SuiteAvailElecAmps'];
                $webQuery .= "&SuiteAvailElecAmps=" . $industrial_electrical_amps_selected;
                $industrialsearchelement++;
            }
//echo "ise: " . $industrialsearchelement . "<br><br>";

            $industrial_shipping_doors_selected = "All";
            if (isset($_REQUEST['SuiteShippingDoors']) && $_REQUEST['SuiteShippingDoors'] != "All") {
                $industrial_shipping_doors_selected = $_REQUEST['SuiteShippingDoors'];
                $webQuery .= "&SuiteShippingDoors=" . $industrial_shipping_doors_selected;
                $industrialsearchelement++;
            }
//echo "ise: " . $industrialsearchelement . "<br><br>";

            $industrial_drivein_doors_selected = "All";
            if (isset($_REQUEST['SuiteDriveInDoors']) && $_REQUEST['SuiteDriveInDoors'] != "All") {
                $industrial_drivein_doors_selected = $_REQUEST['SuiteDriveInDoors'];
                $webQuery .= "&SuiteDriveInDoors=" . $industrial_drivein_doors_selected;
                $industrialsearchelement++;
            }
//echo "ise: " . $industrialsearchelement . "<br><br>";

            $industrial_parking_selected = "All";
            if (isset($_REQUEST['SuiteParking']) && $_REQUEST['SuiteParking'] != "All") {
                $industrial_parking_selected = $_REQUEST['SuiteParking'];
                $webQuery .= "&SuiteParking=" . $industrial_parking_selected;
                $industrialsearchelement++;
            }
//echo "ise: " . $industrialsearchelement . "<br><br>";

            $industrial_trucktrailer_parking_selected = "All";
            if (isset($_REQUEST['SuiteTruckTrailerParking']) && $_REQUEST['SuiteTruckTrailerParking'] != "All") {
                $industrial_trucktrailer_parking_selected = $_REQUEST['SuiteTruckTrailerParking'];
                $webQuery .= "&SuiteTruckTrailerParking=" . $industrial_trucktrailer_parking_selected;
                $industrialsearchelement++;
            }
//echo "ise: " . $industrialsearchelement . "<br><br>";
// End industrial unit advanced search elements 
        } // end if ($advanced_search == 1) {

        
        
        
//$suites_query_string = $queries->get_suites_query_string($lang_id, $building_code, $size_selected, $sizeFrom, $sizeTo, $suitetype_selected, "notag", $availability_notbefore, $availability_notafter);
        $suites_query_string_query = $queries->get_suites_query_string($lang_id, $building_code, $size_selected, $sizeFrom, $sizeTo, $suitetype_selected, "notag", $availability_notbefore, $availability_notafter, $industrial_office_space_selected, $industrial_clear_height_selected, $industrial_shipping_doors_selected, $industrial_drivein_doors_selected, $industrial_electrical_volts_selected, $industrial_electrical_amps_selected, $industrial_parking_stalls_selected, $industrial_trucktrailer_parking_selected, $industrialsearchelement, $advanced_search, $db);

        $suite_query_string = $suites_query_string_query[0];
        $a_params = array();
        if ($suite_query_statement = $db->prepare($suite_query_string)) {
            // carry on
        } else {
            trigger_error('Wrong SQL: ' . $suite_query_string . ' Error: ' . $db->error, E_USER_ERROR);
        }
        // prep dynamic variable query
        $param_type = '';
        $n = count($suites_query_string_query[1]);
        for ($i = 0; $i < $n; $i++) {
            $param_type .= $suites_query_string_query[1][$i];
        }
        /* with call_user_func_array, array params must be passed by reference */
        $a_params[] = & $param_type;
        for ($i = 0; $i < $n; $i++) {
            /* with call_user_func_array, array params must be passed by reference */
            $a_params[] = & $suites_query_string_query[2][$i];
        }

        // call_user_func_array is how we can pass dynamic quantities of variables to mysqli prepared statements.
        call_user_func_array(array($suite_query_statement, 'bind_param'), $a_params);
        $suites_query = array();
        if ($suite_query_statement->execute()) {
            $result = $suite_query_statement->get_result();
            while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
                $suites_query[] = $row;
            }
        } else {
            trigger_error('Could not execute preview images query statement: Error: ' . $db->error, E_ALL);
        }
        $suite_query_statement->close();


        $i = 1;
//print_r($content_xml);
        $suite_html = "";
        $suite_new = "";
        $suite_model = "";
        $suite_leased = "";
        $suite_promoted = "";

        $spacer = 0;

//foreach($content_xml->xpath('//suites') as $Suites) {
        $contig = 0;

        function get_suite_availability($language, $row) {
            $suite_availability_raw = $row['availability'];
            $suite_availability = $language['immediately_building'];
            $suite_notice_period = $row['notice_period'];

            if ($suite_notice_period != "no") {
                switch ($suite_notice_period) {
                    case "30d":
                        $suite_availability = $language['30days_text'];
                        break;
                    case "60d":
                        $suite_availability = $language['60days_text'];
                        break;
                    case "90d":
                        $suite_availability = $language['90days_text'];
                        break;
                    case "120d":
                        $suite_availability = $language['120days_text'];
                        break;
                }
            } else if ($suite_availability_raw != "" && $suite_availability_raw >= time()) {
                $suite_availability = strftime('%b. %Y', $suite_availability_raw);
            } else {
                // no availability nothin
            }
            return $suite_availability;
        }

        foreach ($suites_query as $row) {

            $spacer = 0;
            $cross_out = '';
//                            $suite_name = $row['suite_name'];
            // fix for left and right quotes in the suite name
            // they break the PDF when dompdf attempts to make an href link out of them.
            $suite_name = str_replace("“", "\"", $row['suite_name']);
            $suite_name = str_replace("”", "\"", $suite_name);
            $suite_name_short = substr($suite_name, 0, 12);

            $suite_id = $row['suite_id'];



            //$suite_floor = substr($suite_name, 0,1);
            // Set up English vs French sqft formatting
            if ($lang == "1") { // English
                $suite_net_rentable_area = $row['net_rentable_area'];
                $suite_net_rentable_area = number_format($suite_net_rentable_area);
                $suite_contiguous_area = $row['contiguous_area'];
                $suite_contiguous_area = number_format($suite_contiguous_area);
                $suite_min_divisible_area = $row['min_divisible_area'];
                $suite_min_divisible_area = number_format($suite_min_divisible_area);
            }
            if ($lang == "0") { // French
                $suite_net_rentable_area = $row['net_rentable_area'];
                $suite_net_rentable_area = number_format($suite_net_rentable_area, 0, ',', ' ');
                $suite_contiguous_area = $row['contiguous_area'];
                $suite_contiguous_area = number_format($suite_contiguous_area, 0, ',', ' ');
                $suite_min_divisible_area = $row['min_divisible_area'];
                $suite_min_divisible_area = number_format($suite_min_divisible_area, 0, ',', ' ');
            }




            $suite_net_rent = $row['net_rent'];

            $suite_add_rent_total = $row['add_rent_total'];



            $suite_description = strip_tags($row['description']);
//$suite_availability_raw = $row['availability'];
////make old dates show as 'immediately'
//if ($suite_availability_raw != ""){
//    if ($suite_availability_raw < time()){
//        $suite_availability = $language['immediately_text'];
//    }else{
//        //show the actual availability date - its in the future
//        if ($lang == "fr_CA") {
//        $suite_availability = strftime('%e %b, %Y', $suite_availability_raw);
//        //	$suite_availability = preg_replace('^[1]','1<sup>er</sup>',$suite_availability);
//        }
//        if ($lang == "en_CA") {
//        $suite_availability = strftime('%b %e, %Y', $suite_availability_raw);
//        }
//    }
//}else{
//$suite_availability = $language['immediately_text'];
//}



            if ($i % 2 == 0) {
                $suite_html .= '<tr class="even">';
            } else {
                $suite_html .= '<tr class="odd">';
            }
            $i = $i + 1;
            //$suite_html .= "<td class=\"floor\">" . $suite_floor . "</td>";
            //$suite_html .= '<td class="suite">' . $suite_name . "</td>";

            $suite_new = $row['new'];
            $suite_model = $row['model'];
            $suite_leased = $row['leased'];
            $suite_promoted = $row['promoted'];


            if ($suite_new == "true") {
                if ($lang == "en_CA") {
                    $suite_html .= '<td class="icon"><img src="' . $language['vacancy_report_website'] . '/images/icon-new_EN.png"></td>';
                } else {
                    $suite_html .= '<td class="icon"><img src="' . $language['vacancy_report_website'] . '/images/icon-new_FR.png"></td>';
                }

                $cross_out = '';
                $spacer = 1;
            }

            if ($suite_model == "true") {
                if ($lang == "en_CA") {
                    $suite_html .= '<td class="icon"><img src="' . $language['vacancy_report_website'] . '/images/icon-model-suite_EN.png"></td>';
                } else {
                    $suite_html .= '<td class="icon"><img src="' . $language['vacancy_report_website'] . '/images/icon-model-suite_FR.png"></td>';
                }
                $cross_out = '';
                $spacer = 1;
            }

            if ($suite_leased == "true") {
                if ($lang == "en_CA") {
                    $suite_html .= '<td class="icon"><img src="' . $language['vacancy_report_website'] . '/images/icon-leased_EN.png"></td>';
                } else {
                    $suite_html .= '<td class="icon"><img src="' . $language['vacancy_report_website'] . '/images/icon-leased_FR.png"></td>';
                }
                $cross_out = ' style="text-decoration:line-through;"';
                $spacer = 1;
            }

            //                        if ($suite_promoted == "true") {
            //                                $suite_html .= '<td class="icon"><img src="'. $language['vacancy_report_website'] . '/images/icon-promoted_EN.png"></td>';
            //                                $cross_out = ' style="font-style: italic;font-weight: bold;"';
            //                                $spacer = 1;
            //                        }

            if ($suite_promoted == "true") {
                if ($lang == "en_CA") {
                    $suite_html .= '<td class="icon"><img src="' . $language['vacancy_report_website'] . '/images/icon-promoted_EN.png"></td>';
                } else {
                    $suite_html .= '<td class="icon"><img src="' . $language['vacancy_report_website'] . '/images/icon-promoted_FR.png"></td>';
                }
                $cross_out = ' style="font-style: italic;font-weight: bold;"';
                $spacer = 1;
            }




            if ($spacer == 0) {
                $suite_html .= '<td class="icon">&nbsp;</td>';
            }


//			$suite_pdf_query_string = "select * from files_2d where building_code = '".$building_code."' and suite_name = '".$row['suite_name']."'";
//			$suite_pdf_query = mysql_query($suite_pdf_query_string) or die ("Suite pdf query error: " . mysql_error());
//			$suite_pdf = mysql_fetch_array($suite_pdf_query);
////			echo $suite_pdf_query_string.'<br><br>';
//			
//				$suite_pdf_plan = $suite_pdf['plan_name'];
//
// //                               echo "suite name: " . $suite_name . "<br><br>";
// //                               echo "suite net rentable area" . $suite_net_rentable_area . "<br><br>";
// //                               echo "suite contig area: " . $suite_contiguous_area . "<br><br>";
//                                
//			// Add suite name
//				if ($suite_pdf_plan != '') {
//					if ($suite_contiguous_area != '0' && $suite_contiguous_area != ''){
//						$contig = 1;
//						$suite_html .= '<td class="contiguous"><span' . $cross_out . '><a target="_blank" class="contiguous" target="_blank" href="'. $language['vacancy_report_website'] .'/images/twodee_files/'. $suite_pdf_plan . '">' . $suite_name . '</a></span>';
//					}else{
//						$suite_html .= '<td class="suite"><span' . $cross_out . '><a target="_blank" class="contiguous" target="_blank" href="'. $language['vacancy_report_website'] .'/images/twodee_files/'. $suite_pdf_plan . '">' . $suite_name . '</a></span></td>';
//					}
//					
//				} else {
//					if ($suite_contiguous_area != '0' && $suite_contiguous_area != ''){
//						$contig = 1;
//						$suite_html .= '<td class="contiguous"><span' . $cross_out . '>' . $suite_name . '</span></td>';
//					}else{
//						$suite_html .= '<td class="suite"><span' . $cross_out . '>' . $suite_name . '</span></td>';
//					}
//				}





            if ($theme['result_links_to_corp_website'] == 1) {

                $suite_html .= '<td class="contiguous"><span' . $cross_out . '>' . $suite_name . '</span></td>';
            } else {

                if ($suite_contiguous_area != 0 && $suite_contiguous_area != '') {
                    $contig = 1;
                    $suite_html .= '<td class="contiguous"><span' . $cross_out . '><a class="contiguous" href="' . $language['vacancy_report_website'] . '/suite.php?building=' . $building_code . '&suiteid=' . $suite_id . $queries->get_query_str() . '">' . addslashes($suite_name_short) . '</a></span></td>';
                } else {
                    $suite_html .= '<td class="suite"><span' . $cross_out . '><a href="' . $language['vacancy_report_website'] . '/suite.php?building=' . $building_code . '&suiteid=' . $suite_id . $queries->get_query_str() . '">' . $suite_name_short . '</a></span></td>';
                }
            }






            // Suite Area
            $suite_html .= '<td class="area"><span' . $cross_out . '>' . $suite_net_rentable_area . "<br />";

//			if (is_string($suite_net_rentable_area)) {
////                            echo "It's a string". "<br><br>";
//                        } else {
////                            echo "It's a number". "<br><br>";
//                        }
            // Suite Contiguous Area
//                            if ($suite_contiguous_area != '0' && $suite_contiguous_area != '' && $suite_contiguous_area != 0) {
//                                    $suite_html .= '(<span class="contiguous"><span' . $cross_out . '>' . $suite_min_divisible_area . ' | ' . $suite_contiguous_area . '</span></span>)';
//                            } else {
////                                    $suite_html .= '(<span class="contiguous"><span' . $cross_out . '> | </span></span>)';
//                            }


            if ((intval($suite_contiguous_area) != 0) && (intval($suite_min_divisible_area) != 0)) {
                $suite_html .= '(' . $suite_min_divisible_area . ' | ' . $suite_contiguous_area . ')</td>';
            } else if ((intval($suite_contiguous_area) == 0) && (intval($suite_min_divisible_area) != 0)) {
                $suite_html .= '(' . $suite_min_divisible_area . ')</td>';
            } else if ((intval($suite_contiguous_area) != 0) && (intval($suite_min_divisible_area) == 0)) {
                $suite_html .= '(' . $suite_contiguous_area . ')</td>';
            } else {
                $suite_html .= '</td>';
            }




//                            $suite_html .= '</td>';
            // Suite Rent
            $suite_html .= '<td class="rent"><span' . $cross_out . '>';
            if ($suite_net_rent == "0" || $suite_net_rent == "") {
                $suite_html .= $language['negotiable_text'] . "</span></td>";
            } else {
                if (strpos($suite_net_rent, '$') > -1) {
                    $suite_html .= money_format('%.2n', $suite_net_rent) . "</span></td>";
                } else {
                    $suite_html .= money_format('%.2n', $suite_net_rent) . "</span></td>";
                }
            }

            // Suite Availability
            $suite_html .= '<td class="availability"><span' . $cross_out . '>' . get_suite_availability($language, $row) . "</span></td>";
            //$suite_html .= '<td class="rent-additional">$' . $suite_add_rent_total . "</td>";
//			// Suite Description
//                            $suite_html .= '<td class="description"><span' . $cross_out . '>' . $suite_description . '</span></td>';


            if ($building_type == "Industrial" || $building_type == "Industriel") {

                $suite_specs_query_string = "select shipping_doors_drive_in, shipping_doors_truck, available_electrical_amps, available_electrical_volts, clear_height from suites_specifications where building_id = '" . $building_id . "' and suite_id = '" . $row['suite_id'] . "'";
                $suite_specs_query = mysql_query($suite_specs_query_string) or die("Suite specs query error: " . mysql_error());
                $suite_specs = mysql_fetch_array($suite_specs_query);


                $shippingDI = $suite_specs['shipping_doors_drive_in'];
                $shippingTL = $suite_specs['shipping_doors_truck'];
                $poweramps = $suite_specs['available_electrical_amps'];
                $powervolts = $suite_specs['available_electrical_volts'];
                $ceiling = $suite_specs['clear_height'];
                $loadingoutputDI = '';
                $loadingoutputTL = '';
                $loadingoutput = '';

                if ($shippingDI != '' && $shippingDI != 0) {
                    $loadingoutputDI = $shippingDI . " " . $language['drivein_loading_abbreviation'];
                }
                if ($shippingTL != '' && $shippingTL != 0) {
                    $loadingoutputTL = $shippingTL . " " . $language['truck_loading_abbreviation'];
                }
                if ($loadingoutputDI != '' && $loadingoutputTL != '') {
                    $loadingoutput = $loadingoutputDI . "<br />" . $loadingoutputTL;
                } else if ($loadingoutputDI != '' && $loadingoutputTL == '') {
                    $loadingoutput = $loadingoutputDI;
                } else if ($loadingoutputDI == '' && $loadingoutputTL != '') {
                    $loadingoutput = $loadingoutputTL;
                } else {
                    // both are empty leave a space
                    $loadingoutput = "&nbsp;";
                }

                $poweroutputAmps = '';
                $poweroutputVolts = '';
                if ($poweramps != '' && $poweramps != 0) {
                    $poweroutputAmps = $poweramps . " A";
                }
                if ($powervolts != '' && $powervolts != 0) {
                    $poweroutputVolts = $powervolts . " V";
                }
                if ($poweroutputAmps != '' && $poweroutputVolts != '') {
                    $poweroutput = $poweroutputAmps . "<br />" . $poweroutputVolts;
                } else if ($poweroutputAmps != '' && $poweroutputVolts == '') {
                    $poweroutput = $poweroutputAmps;
                } else if ($poweroutputAmps == '' && $poweroutputVolts != '') {
                    $poweroutput = $poweroutputVolts;
                } else {
                    // both are empty leave a space
                    $poweroutput = "&nbsp;";
                }

                $suite_html .= '<td class="loading"><span ' . $cross_out . '>' . $loadingoutput . '</span></td>';
                $suite_html .= '<td class="power"><span ' . $cross_out . '>' . $poweroutput . '</span></td>';
                $suite_html .= '<td class="ceiling"><span ' . $cross_out . '>' . $ceiling . '</span></td>';


                $suite_html .= '<td class="description"><span ' . $cross_out . '>' . strip_tags(str_replace('&nbsp;', '', $suite_description)) . '</span></td>';
                $suite_html .= '</tr>';
            } else {
                $suite_html .= '<td class="description"><span ' . $cross_out . '>' . strip_tags(str_replace('&nbsp;', '', $suite_description)) . '</span></td>';
                $suite_html .= '</tr>';
            }
        }


        /*
          if ((mysql_num_rows($suites_query) > 5) && ($building_orientation == "PORTRAIT")) {
          echo '<div id="breakit"></div>';
          }

          if ((mysql_num_rows($suites_query) > 7) && ($building_orientation == "LANDSCAPE")) {
          echo '<div id="breakit"></div>';
          }
         */
        ?>









        <table class="availabilities">
            <thead>
                <tr>
                    <td colspan="9" id="availabilities-heading">
                        <h3><?php echo $language['available_space_text']; ?></h3>
                    </td>
                </tr>
                <tr>
                    <th>&nbsp;</th>
                    <?php
                    if ($building_type == "Industrial" || $building_type == "Industriel") {
                        echo '<th>' . $language['unit_text'] . '</th>';
                    } else {
                        echo '<th>' . $language['suite_text'] . '</th>';
                    }
                    ?>
                    <th><?php echo $language['area_text']; ?><br />
                        (<?php echo $language['min_divisible_text'] ?> | <?php echo $language['contiguous_text']; ?>)
                    </th>
                    <th><?php echo $language['rent_text']; ?></th>
                    <th><?php echo $language['index_table_header_availability_text']; ?></th>
                    <?php if ($building_type == "Industrial" || $building_type == "Industriel") { ?>

                    <?php } else { ?>
                        <th><?php echo $language['notes_text']; ?></th>
                    <?php } ?>
                    <?php
//                        if ($twodee == 1){
//                            echo '<th>' . $language['space_plan_text'] . '</th>';
//                        }else{
//                            echo '<th> </th>';
//                        }
//                        if ($threedee == 1){
//                            echo '<th>' . $language['three_dee_text'] . '</th>';
//                        }else{
//                            echo '<th> </th>';
//                        }



                    if ($building_type == "Industrial" || $building_type == "Industriel") {
                        echo '<th class="description">' . $language['loading_text'] . '</th>';
                        echo '<th class="description">' . $language['power_text'] . '</th>';
                        echo '<th class="description">' . $language['ceiling_text'] . '</th>';
                        echo '<th class="description">' . $language['notes_text'] . '</th>';
                    }
                    ?>  
                </tr> 
            </thead> 



            <?php
            $translateretail = '';

            if ($language['lang_id'] == 1) {
                $translatedretail = "Retail";
            }
            if ($language['lang_id'] == 0) {
                $translatedretail = "Détail";
            }
            if ($suite_html == "" && $building_type != $translatedretail) {
                ?><tr>
                    <td colspan="9">  
                        <?php
//            $main_contact_result = mysql_fetch_array($contacts_query);
                        echo '<br><strong><center>' . $language['all_leased_text'] . '</center></strong>';
                        echo '<p><center>' . $language['no_availabilities_text'] . '</center></p><br><br>';
                        ?></td>
                </tr><?php } else if ($suite_html == "" && $building_type == $translatedretail) {
                        ?>
                <tr>
                    <td colspan="9">

                <?php echo '<br><p><center>' . $language['call_for_availability_text'] . '</center></p><br>' ?>
                    </td>
                </tr>
            <?php } ?>

<?php
echo $suite_html;
?>
        </table>
        <!-- eo availabilities //-->

        <?php
        $additional_costs_html = "";


        if (strlen($building_add_rent_operating) > 0 || strlen($building_add_rent_realty) > 0 || strlen($building_add_rent_power) > 0 || strlen($building_add_rent_total) > 0) {
            //echo '<div class="cost">';
            if (strlen($building_add_rent_operating) > 0) {
                $additional_costs_html .= '<td>' . $language['add_rent_operating_text'] . ': ' . money_format('%.2n', $building_add_rent_operating) . ' ' . $language['per_sqft_text'] . '</td>';
            }
            if (strlen($building_add_rent_realty) > 0) {
                $additional_costs_html .= '<td>' . $language['add_rent_realty_text'] . ': ' . money_format('%.2n', $building_add_rent_realty) . ' ' . $language['per_sqft_text'] . '</td>';
            }
            if (strlen($building_add_rent_power) > 0) {
                if ($building_add_rent_power == "0") {
                    $additional_costs_html .= '<td>' . $language['add_rent_power_text'] . ': Included</td>';
                } else {
                    $additional_costs_html .= '<td>' . $language['add_rent_power_text'] . ': ' . money_format('%.2n', $building_add_rent_power) . ' ' . $language['per_sqft_text'] . '</td>';
                }
            }
            if (strlen($building_add_rent_total) > 0) {
                $additional_costs_html .= '<td>' . $language['add_rent_total_text'] . ': ' . money_format('%.2n', $building_add_rent_total) . ' ' . $language['per_sqft_text'] . '</td>';
            }
            if ($additional_costs_html != "") {
                echo '<table id="additional-costs"><tr>' . $additional_costs_html . '</tr></table>';
            }
        }// end big if strlen...
        ?>
        <!-- eo additional-costs -->



        <?php // if (mysql_num_rows($suites_query) < 6) {
        ?>
        <!--   <div id="breakit"></div>-->
<?php // }  ?>






        <!-- end availabilities & new page -->
        <div id="breakit"></div>

        <?php

        function convert($text) {
            $text = trim($text);
            return '<p>' . preg_replace('/[\r\n]+/', '</p><p>', $text) . '</p>';
        }
        ?>
        <!-- Property Description and Building Features -->

        <table class="features">
            <tr>
                <td colspan="2">
                    <h3><?php echo $language['property_desc_text']; ?></h3>
                    <p><?php echo strip_tags(nl2br($building_desc), '<br>,<br />,<p>'); ?></p>
                </td>
            </tr>

            <?php
            if ($sections_count > 0) {
                echo "<tr>";
            }

            $sec = 1;
            while ($Section = mysql_fetch_array($sections_query)) {
                $section_title = trim($Section['section_title']);
                $section_content = strip_tags(nl2br($Section['section_content']), '<br>,<br />,<p>');
                $section_content = substr($section_content, 0, 1800) . "...";

                echo '<td valign="top">';
                echo "<h3>" . $section_title . "</h3>";
                echo "<p>" . $section_content . "</p>";
                echo "</td>";

                if ($sections_count > 2) {
                    if (($sec % 2 == 0) && ($sec != "4")) {
                        echo "</tr><tr>";
                    }
                }


                $sec++;
            }

            echo "</tr>";
            ?>




        </table>



        <?php
//			   echo $features_section_length.'<br>';
//			   echo $amenities_content_length.'<br>';		
//			   echo $managed_section_length.'<br>';
//			   echo $building_desc_length.'<br>';	
//			   echo $toobig;


        /* 			   if ($toobig >= 2) {
          echo $toobig.'<br>';
          echo $evenbigger.'<br>';

          } else { */
        ?>

        <!--<div id="breakitforsure"></div>-->

        <!-- BEGIN GALLERY -->
        <table class="building-gallery">
            <tr>
            <!-- <h3 align="left" ><?php //echo $gallery_text; ?></h3> -->
                <?php
                /* 	if (mysql_num_rows($suites_query) > 13) {
                  $gallery_limit = 3;
                  } else {
                  $gallery_limit = 6;
                  } */


//	$gallery_query_string = "select distinct * from preview_images where building_code ='" . $building_code . "' limit 6";
//	$gallery_query = mysql_query($gallery_query_string);
                // PREVIEW IMAGES QUERY
                $preview_images_query_string = "select * from preview_images where building_code ='" . $building_code . "' order by preview_index asc limit 6";
                $preview_images_query = mysql_query($preview_images_query_string) or die("preview images query error: " . mysql_error());


//        echo $preview_images_query_string . "<br><br>";

                $preview_images_count = mysql_num_rows($preview_images_query);


                if (mysql_num_rows($preview_images_query) == 0) {
                    //echo '<td><img src="../images/no_thumb.jpg" /></td>';
                } else {


                    $i = 1;
                    while ($row = mysql_fetch_array($preview_images_query)) {
                        $preview_image = $row['file_name'];
                        if ($i % 3 == 0) {
                            echo '<td><img src="' . $language['vacancy_report_website'] . '/images/preview_images/' . $preview_image . '" width="230" height="180" /></td></tr></table><table class="building-gallery"><tr>';
                        } else {
                            echo '<td><img src="' . $language['vacancy_report_website'] . '/images/preview_images/' . $preview_image . '" width="230" height="180" /></td>';
                        }
                        $i++;
                    }
                }
                ?>
            </tr></table>
        <!-- END GALLERY -->

        <div id="breakit"></div>
        <?php
        if ($building_type == "Office" || $building_type == "Bureau") {
            include ('pdf_office_spec.php');
        } else if ($building_type == "Industrial" || $building_type == "Industriel") {
            include ('pdf_industrial_spec.php');
        } else if ($building_type == "Retail" || $building_type == "Détail") {
            include ('pdf_retail_spec.php');
        }
        ?>



    </body>
</html>
