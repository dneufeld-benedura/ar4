<?php


	include('../objects/Queries.php');
	include('../objects/SearchData.php');
        include('../objects/LanguageQuery.php');
        include('../objects/StatsPackage.php');
        include('../theme/db.php');
        
        // Initiate the Queries class
	$queries = new Queries();
        //Initiate the Search Data class
	$searchData = new SearchData();
        // initiate the StatsPackage class
        $stats = new StatsPackage();

                                
	$languageQuery         = new LanguageQuery();
	$langArray             = $languageQuery->getLanguageAndID();
	$lang                  = $langArray[0];
	$lang_id               = $langArray[1];

	// Language Query
	$language_query_string = 'select * from languages_dynamic ld, languages l, languages2 where ld.lang_id = ' . $lang_id . ' and l.lang_id = ' . $lang_id;
	$language_query = mysql_query($language_query_string) or die("language query error: " . mysql_error());
	$language = mysql_fetch_array($language_query);


	// Theme Query
	$theme_query = mysql_query("select * from theme");
	$theme = mysql_fetch_array($theme_query);
				
	if (isset($_REQUEST['show_no_vacancy'])) {
		if ($_REQUEST['show_no_vacancy'] == 'on') {
			$show_no_vacancy = 1;
		} else {
			$show_no_vacancy = 0;
		}
	} else {
		if ($theme['no_vacancy_default'] == 1) {
			$show_no_vacancy = 1;
		} else {
			$show_no_vacancy = 0;
		}
	}
        
    function get_suite_availability($language, $row) {
        $suite_availability_raw = $row['availability'];
        $suite_availability = $language['immediately_building'];
        $suite_notice_period = $row['notice_period'];
        
        if ($suite_notice_period != "no") {
            switch ($suite_notice_period) {
                case "30d":
                    $suite_availability = $language['30days_text'];
                    break;
                case "60d":
                    $suite_availability = $language['60days_text'];
                    break;
                case "90d":
                    $suite_availability = $language['90days_text'];
                    break;
                case "120d":
                    $suite_availability = $language['120days_text'];
                    break;
            }
        } else if ($suite_availability_raw != "" && $suite_availability_raw >= time()) {
            $suite_availability = strftime('%b. %Y', $suite_availability_raw);
        } else {
            // no availability nothin
        }       
        return $suite_availability;
    }
        
        
        
	//setting locale for time, date, money, etc
		setlocale(LC_ALL, $language['locale_string']);
		date_default_timezone_set('America/New_York'); 
                                

                                
$building_name_sterm_array  = $searchData->get_building_name_sterm_array();
$building_name       = $building_name_sterm_array['building_name_sterm'];
$building_name_sterm_prepop = $building_name_sterm_array['building_name_sterm_prepop'];  

$province_selected           = $searchData->get_province_selected($language);
$buildingtype_selected       = $searchData->get_buildingtype_selected(); 
$suitetype_selected = $searchData->get_suitetype_selected($language);	
$region_selected             = $searchData->get_region_selected($language);        
$sub_region_selected         = $searchData->get_sub_region_selected($language);        
$size_selected = $searchData->get_size_selected();    
        $sizeFrom                    = "";
        $sizeTo                      = "";
        if (isset($_REQUEST['Size']) && $_REQUEST['Size'] == "Custom"){
            $size_selected = "Custom";
            $sizeFrom = $_REQUEST['sizeFrom'];
            $sizeTo = $_REQUEST['sizeTo'];
        }
$availability_selected_array = $searchData->get_availability_selected();
$availability_notafter       = $availability_selected_array['availability_notafter'];
$availability_notbefore      = $availability_selected_array['availability_notbefore'];  



	if (isset($_GET['Address'])) {
	$addresses = explode(',',$_GET['Address']);


	} else {
            $building_codes_narrow = '';
$buildings_query_string = $queries->getBuildingsQueryString($size_selected, $sizeFrom, $sizeTo, $suitetype_selected, $buildingtype_selected, $province_selected, $region_selected, $sub_region_selected, $building_codes_narrow, $lang_id, $availability_notafter, $availability_notbefore, $building_name_sterm, $show_no_vacancy, $industrial_office_space_selected, $industrial_clear_height_selected, $industrial_shipping_doors_selected, $industrial_drivein_doors_selected, $industrial_electrical_volts_selected, $industrial_electrical_amps_selected, $industrial_parking_stalls_selected, $industrial_trucktrailer_parking_selected, $industrialsearchelement, $advanced_search);

                $buildings_query = mysql_query($buildings_query_string);

				
	$building_driver = array();
	
	// loop through results and push them in to an array (like the $addresses array we're exploding above)
		while ($row = mysql_fetch_array($buildings_query)) {
				$building_driver[] = $row['building_code'];
		}
		
	}



        
        // Branding Query
	$branding_name = '';

	$branding_query_string = "Select * from branding where region = '" . $province_selected . "'";
	$branding_query = mysql_query($branding_query_string) or die ("branding_query error: " . mysql_error());
        $branding = mysql_fetch_array($branding_query);
	$branding_count = mysql_num_rows($branding_query);
	if ($branding_count == 0) {
		$branding_name = $language['page_title'];
	} else {
		$branding_name = $branding['company_name'];
	}
        
        
        
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $language['page_title']; ?></title>
<link rel="stylesheet" type="text/css" href="../css/reset.css" />

<style type="text/css">

/*---------------------*/
/*COLOURED CSS ELEMENTS*/
/*---------------------*/
/*---------------------*/
/*---------------------*/

#header {
	width: 760px;
	background: <?php echo $theme['primary_colour_dompdf_hex']; ?>;
	margin: 0 0 5px 0;
	padding: 0;
	color: #fff;
	visibility: hidden;
}

.header-border {
	 background: <?php echo $theme['secondary_colour_dompdf_hex']; ?>;	
}

table#sub-header h2 {
	font-size: 1.2em;
	color: <?php echo $theme['primary_colour_dompdf_hex']; ?>;
	text-align: right;
	text-transform: uppercase;
}


<?php 
$tertiaryColour = '';
if ($theme['fourth_colour_hex'] != '') {
    $tertiaryColour = $theme['fourth_colour_hex'];
} else {
    $tertiaryColour = $theme['secondary_colour_dompdf_hex'];
};

?>


h3 {
	font-size: 1.0em;
	text-transform: uppercase;
	border-bottom: 2px solid <?php echo $tertiaryColour; ?>;
	margin-bottom: 10px;
	color: <?php echo $theme['primary_colour_dompdf_hex']; ?>;
	text-align: left;
}

table#availabilities-heading h3 {
	font-size: 1.2em;
	text-transform: uppercase;
	border-bottom: 2px solid <?php echo $tertiaryColour; ?>;
	margin-bottom: 10px;
	color: <?php echo $theme['primary_colour_dompdf_hex']; ?>;
}

table#additional-costs {
	width: 760px;
	border-top: 1px solid <?php echo $tertiaryColour; ?>;
	border-bottom: 1px solid <?php echo $tertiaryColour; ?>;
	font-size: 1.2em;	
	margin-bottom: 20px;
	/*page-break-after: auto;*/
}

.result a {
    text-decoration: none;
    color: <?php echo $theme['primary_colour_dompdf_hex']; ?>;
}

<?php if ($theme['contiguous_text_colour'] != '') {
	$contiguous_text_color = $theme['contiguous_text_colour'];
} else {
	$contiguous_text_color = $theme['fontcolor'];
}
?>

span.contiguous {
    color: <?php echo $contiguous_text_color; ?>;    
}

/*---------------------*/
/*---------------------*/
/* END COLOUR ELEMENTS */
/*---------------------*/
/*---------------------*/
/*---------------------*/
/*---------------------*/
/*---------------------*/







@page {
	margin: 160px 30px 60px 30px;
	padding: 0;	
}

body {
	margin: 0;
	padding: 0;
	font-family: Arial, sans-serif;
	color: #555555;
	font-size: 10px;
}



#header td.header-left {
	width: 275px;	
}



#logo {
	margin: 20px 0 20px 20px;	
	width: 210px;
	height: 78px;
	
}

table#header h1 {
	font-size: 2.4em;
	color: #fff;
	display: inline-block;
	/*border-top: 2px solid #fff;
	border-bottom: 2px solid #fff;*/
	text-transform: uppercase;
	text-align: left;
	/*margin: 20px 20px 20px 0;*/
	
}

.titleblock-pin-line-top {
	border-bottom: 1px solid #fff;
}

.titleblock-pin-line-bottom {
	border-bottom: 1px solid #fff;
}

.titleblock-spacer {
	height:25px;
	width: 400px;
	/*width:660px;*/
}

table#sub-header {
	width: 760px;
	margin: 0 0 5px 0;	
}



table#portrait {
	width: 760px;
	margin: 0 0 10px 0;	
}

table#portrait td.left {
/*	width: 300px;	*/
}

table#portrait td.right {
/*	width: 460px;*/
		
}

table#portrait td.right table {
	width: 450px;
	float: right;	
}

table#portrait td.right table td {
	width: 225px;	
}

table#landscape td {
	width: 240px;
	padding: 0 10px 20px 0;	
}


table#landscape td.secondary {
	
	padding: 0 10px 20px 0;	
}

table#landscape td.tertiary {
	width: 240px;
	padding: 0 0 20px 0;	
}

table#landscape td.image {
	width: 480px;
}



.box {
	margin: 0 20px 20px 20px;
}

table#landscape td.primary .box {
	margin: 0 40px 20px 0;
}

table#landscape td.secondary .box {
	margin: 0 30px 20px 10px;
}


#building-contact.box {
	margin-right: 0;
}

.building-gallery {
	/*margin: 0;*/
	clear: both;
	/*page-break-before: auto;*/
	/*page-break-inside: avoid;*/
	
}

.building-gallery td {
	vertical-align: top;
	padding-top: 15px;
	padding-right: 15px;	
}

.gallery {
	margin: 0;
	clear: both;
	overflow: auto;	
}

#building-image {
	
}

div.building-information {
	/*width: 200px;*/
	font-size: 1.2em;	
}

#building-contact {
	/*width: 200px;*/
	font-size: 1.2em;	
}

#building-description {
	/*width: 520px;*/	
}

#building-description.center-box {
	/*width: 420px;*/
}

#portrait #building-information,
#portrait #building-description {
		
}

.contact-details {
	margin-bottom: 10px;	
}

.contact-name {
	color: #000;	
}

.contact-title {
	color: #000;
}

.second-building-page{
	padding-top:20px;
}

#breakit {
	page-break-before: always;
}

table#availabilities-heading {
	width: 760px;	
}

table.availabilities {
	width: 760px;
	font-size: 1.2em;
	padding: 0;
	margin: 0 0 10px 0;
}

table.availabilities th {
	padding: 5px;
	text-align: center;
	font-weight: bold;
	color: #000;
}

table.availabilities td {
	text-align: center;
	padding: 2px 0 2px 0;
	/*vertical-align: top;*/
}

table.availabilities td.area {
/*width: 55px;*/
}

table.availabilities td.description{
	text-align: left;
}



table#additional-costs td{
	padding: 5px 0 5px 0;	
}

#more-info {
	width: 760px;
	font-size: 1.2em;
	page-break-inside:avoid;
/*	page-break-after: auto;*/
}

/*
table#more-info td {
	width: 200px;
	padding-right: 20px;	
}

*/

#more-info td.left {
	width: 240px;
	padding: 0 20px 20px 0;	
}

#more-info td.right {
	width: 500px;
	padding: 0 0 20px 0;	
}

.gallery-thumb {
	float: left;
	display: inline-block;
	width: 230px;
	height: 180px;
	overflow: hidden;
	background: #ccc;
	margin: 0 30px 10px 0;	
}

.gallery-thumb.last {
	margin: 0;	
}

table.features {
	width: 100%;
}

div.fullyleased1 {
	text-align: center;
	/*font-size: 1.4em;*/
	font-weight: bold;
/*	margin: -10px 0 0 0;*/
}

div.fullyleased2 {
	text-align: center;
	/*font-size: 1.4em;*/
	margin: 5px 0 0 0;
}


</style>

</head>

<?php 

  	// decide which mechanism will drive the page
	

	$buildings_sorted = array();
	// if narrowed results...
	if (isset($_GET['Address'])) {
	
	// make sure they're sorted properly
		$address_sorting_query_string = "SELECT building_code, sort_streetname, sort_streetnumber FROM buildings WHERE building_code IN ('" . str_replace(",", "','", $_GET['Address']) . "') and lang = ".$lang_id." order by province asc, region asc, sort_streetname asc, sort_streetnumber asc";
	$address_sorting_query = mysql_query($address_sorting_query_string);


	// loop through results and push them in to an array (like the $addresses array we're exploding above)
		while ($sortrow = mysql_fetch_array($address_sorting_query)) {
				$buildings_sorted[] = $sortrow['building_code'];
		}

	$driver = $buildings_sorted;	
	} else {
	// if search results
	$driver = $building_driver;
	}


//ENORMOUS FOREACH THAT WRAPS THE ENTIRE PAGE	

	foreach ($driver as $building_selected) {	

	$maplink = '';

		
		$buildings_query_string = "select * from buildings where building_code='" . $building_selected . "' and lang=" . $lang_id;
		$buildings_query = mysql_query($buildings_query_string);
		$Building = mysql_fetch_array($buildings_query);
                
                $building_specs_query_string = "select website from building_specifications where building_code='" . $building_selected . "' and lang = ".$lang_id."";
                $building_specs_query = mysql_query($building_specs_query_string);
                $building_specs = mysql_fetch_array($building_specs_query);
		
		$building_id = $Building['building_index'];
		$building_name = $Building['building_name'];
		$building_code = $building_selected;
		$building_street = $Building['street_address']; 
		$building_city = $Building['city'];
		$building_province = $Building['province'];
                $building_type = $Building['building_type'];
		$building_total_space = $Building['total_space'];
		$building_measurement_unit = $Building['measurement_unit'];
		$building_desc = $Building['description'];
		$building_contact = $Building['contact_info'];
		$building_header = $Building['flyer_header'];
		if ($building_header == ""){
			$building_header = "/var/www/html/newdev/images/no_header.jpg";
		}
		$building_url = $Building['uri'];
		$building_static_map = $Building['static_map'];

		$building_availability = $Building['modification_date'];
		$building_postal_code = $Building['postal_code'];
		$building_total_space = $Building['total_space'];
		$building_office_area = $Building['office_area'];
		$building_retail_area = $Building['retail_area'];
		$building_industrial_area = $Building['industrial_area'];
		$building_floors = $Building['number_of_floors'];
		$building_year_built = $Building['year_of_building'];
		$building_year_renovated = $Building['renovated'];
		$building_typical_floor_size = $Building['typical_floor_size'];
		$building_parking_stalls = $Building['parking_stalls'];
		$building_parking_ratio = $Building['parking_ratio'];
		$building_available_space = $Building['available_space'];
		$building_add_rent_operating = $Building['add_rent_operating'];
		$building_add_rent_realty = $Building['add_rent_realty'];
		$building_add_rent_power = $Building['add_rent_power'];
		$building_add_rent_total = $Building['add_rent_total'];
		$building_truck_doors = $Building['truck_doors'];
		$building_drivein_doors = $Building['drivein_doors'];
		$building_clear_height = $Building['clear_height'];
		$building_power_text = $Building['power_text'];
		$building_orientation = $Building['orientation'];
		
            // for reporting
            $today = date("Y/m/d");
            $todaytimestamp = strtotime($today);
            // Statistics Tracking
            $stats->trackBuildingPdfPrint($todaytimestamp, $today, $building_id, $building_code);  
            

		// Listing Broker CONTACTS QUERY
		$listingbroker_contacts_query = mysql_query("select * from contacts where building_code ='" . $building_code . "' and email not like '%morguard%' and lang=" . $lang_id . " order by contact_index asc limit 2");
		
		// DOCUMENT QUERY
		$documents_query = mysql_query("select * from documents where building_code ='" . $building_code . "' and lang=" . $lang_id);

		// SECTIONS QUERY
	$sections_query = mysql_query("select * from sections where section_content != '' and building_code='" . $building_code . "' and lang=" . $lang_id);
	$sections_count = mysql_num_rows($sections_query);


    $building_thumbnail_query_string = $queries->get_building_thumbnail_query_string($lang_id, $building_code);
    $building_thumbnail_query = mysql_query($building_thumbnail_query_string) or die ("building thumbnail image error: " . mysql_error());
    $building_thumbnail = mysql_fetch_array($building_thumbnail_query);
    $building_thumbnail_image_name = '';
    $building_thumbnail_image_name = "images/preview_images/" . $building_thumbnail['file_name'];

    if ($building_thumbnail_image_name == "") {
            $building_thumbnail_image_name = "images/no_thumb_700.png";
    }
	
	//Initialize the static map link
		$maplink .= 'http://maps.googleapis.com/maps/api/staticmap?size=490x190&zoom=13&format=png32';
	
	
	//Build the map image link
	$maplink .= '&markers=color:blue%7C'. urlencode($building_street) .'%2C'. urlencode($building_city) .'%2C'. urlencode($building_province);
	
			
			//Finish the maplink code
			$maplink .= '&sensor=false';

				$attachments = true;

?>


                    <?php
$suites_query_string = $queries->get_suites_query_string($lang_id, $building_code, $size_selected, $sizeFrom, $sizeTo, $suitetype_selected, "notag", $availability_notbefore, $availability_notafter);

                        $suites_query = mysql_query($suites_query_string);
                        $i = 1;
						$suitecount = mysql_num_rows($suites_query);

		
	
		
		$suitetype_query_string = "select distinct suite_type from suites where lang=" . $lang_id . " and building_code ='" . $building_code . "'";
		$suitetype_query = mysql_query($suitetype_query_string) or die ('suitetype_query error: ' . mysql_error());
		$suitetypearray = array();
		while ($suitetypecheck = mysql_fetch_array($suitetype_query)) {
			array_push($suitetypearray, strtolower($suitetypecheck['suite_type']));
		}
		

		$othersuitetypes_query_string = "select suite_index from suites where lang=" . $lang_id . " and building_code ='" . $building_code . "' and suite_type !='" . $suitetype_selected . "'";
		$othersuitetypes_query = mysql_query($othersuitetypes_query_string) or die ('othersuitetypes_query error: ' . mysql_error());
		$othersuitetypes_count = mysql_num_rows($othersuitetypes_query);
		
		$allsuites_query_string = "select * from suites where lang=" . $lang_id . " and building_code ='" . $building_code . "'";
		$allsuites_query = mysql_query($allsuites_query_string) or die ('allsuites_query error: ' . mysql_error());
		$allsuites_count = mysql_num_rows($allsuites_query);
		
        $suite_type_chosen = false;

  
  
  	if ($lang_id == 1) {
  		$translateretail = "Retail";
	}
	if ($lang_id == 0) {
		$translateretail = "Détail";
	}

	$type_lang_array = '';
	if ($suitetype_selected !=  $language['all']) {	
		if ((strtolower($suitetype_selected) == "office") || (strtolower($suitetype_selected) == "bureau")) {
			$type_lang_array = array('office','bureau');
		}
		if ((strtolower($suitetype_selected) == "industrial") || (strtolower($suitetype_selected) == "industriel")) {
			$type_lang_array = array('industrial','industriel');
		}
		if ((strtolower($suitetype_selected) == "retail") || (strtolower($suitetype_selected) == "détail")) {
			$type_lang_array = array('retail','détail');
		}
	}



	
	$donotshow = 0;
	if ($suitecount == 0) {
		if ((($suitetype_selected != "All") && (!in_array(strtolower($Building['building_type']),$type_lang_array))) ||  ($allsuites_count > $suitecount)) {
			$donotshow = 1;
		}
		if (($Building['no_vacancies'] == 1) && (in_array(strtolower($Building['building_type']),$type_lang_array))) {
			$donotshow = 0;
		}
	}
	




	// conditional to show vacant buildings or not, retail buildings and normal search results.
	if (($suitecount == 0 && $show_no_vacancy == 1 ) || ($suitecount > 0 ) || ($Building['building_type'] == $translateretail) || (in_array($suitetype_selected,$suitetypearray))) {  
	
	if ($donotshow != 1) {

						?>

<body>

<?php 
global $attachments; 
$attachments = 0;
?>



<?php 
		if ($lang_id == 0){
			$page_text = "Page {PAGE_NUM} de {PAGE_COUNT}";
			
		}else{
			$page_text = "Page {PAGE_NUM} of {PAGE_COUNT}";
			
		}
	?>

	<script type="text/php">
		if ( isset($pdf) ) {
			$font = Font_Metrics::get_font("Arial", "bold");
			
			$pdf->page_text(542, 725, "<?php echo $page_text; ?>", $font, 10, array(0,0,0));
			
		}
	</script>
    


<script type="text/php">

	date_default_timezone_set('America/New_York'); 
	setlocale(LC_ALL, <?php echo $language['locale_string']; ?>);


			
if ( isset($pdf) ) {
//	$pdf->page_script('	

			// start a new object
			$obj3 = $pdf->open_object();
		
			// set the font
			$w = $pdf->get_width();
			$h = $pdf->get_height();

			$pdf->image("<?php echo $language['pdflogo']; ?>", 10, 10, 592, 55);

			$font = Font_Metrics::get_font("Arial", "bold");
			
			<?php
			// HEX ENCODE POTENTIALLY FRENCH TEXT - DATE
			$str = strftime('%B %Y');
			$datetext = '';
			for ($i=0;$i<strlen($str);$i++) {
				$datetext .= sprintf('\\x%lx', ord($str[$i]));
			}
                        
                        // HEX ENCODE POTENTIALLY FRENCH TEXT - Branding Line
			$str = $branding_name;
			$branding = '';
			for ($i=0;$i<strlen($str);$i++) {
				$branding .= sprintf('\\x%lx', ord($str[$i]));
			}
			?>

			$pagetext3 = "<?php // echo $language['page_title'] . ' | ' . $datetext; ?>";
                        $pagetext3 = "<?php echo $branding . ' | ' . $datetext; ?>";

		//	get text width
			$width3 = Font_Metrics::get_text_width($pagetext3, $font, 10);

		//  Header line and date on right
			$pdf->text(($w-$width3-20), 75, $pagetext3, $font, 10, array(<?php echo $theme['primary_colour_dompdf']; ?>));

			// close the object
			$pdf->close_object();
			$pdf->add_object($obj3);
//	');
	
			
}
	</script>

<script type="text/php">
	if ( isset($pdf) ) {
	//  Header Building Address on left
			$font = Font_Metrics::get_font("Arial", "bold");
			
			<?php
			// HEX ENCODE POTENTIALLY FRENCH TEXT
			$str = $building_name;
			$building_name_dompdf = '';
			for ($i=0;$i<strlen($str);$i++) {
				$building_name_dompdf .= sprintf('\\x%lx', ord($str[$i]));
			}

			
			// HEX ENCODE POTENTIALLY FRENCH TEXT
			$str = $building_street;
			$streettext = '';
			for ($i=0;$i<strlen($str);$i++) {
				$streettext .= sprintf('\\x%lx', ord($str[$i]));
			}
			
			// HEX ENCODE POTENTIALLY FRENCH TEXT
			$str = $building_city;
			$citytext = '';
			for ($i=0;$i<strlen($str);$i++) {
				$citytext .= sprintf('\\x%lx', ord($str[$i]));
			}
			?>
			
			
			$nametext = "<?php echo strtoupper($building_name_dompdf); ?>";
			$addresstext = "<?php echo $streettext.", ".$citytext; ?>";
			
                        //$pdf->add_link('www.spk.ca', 20, 75, 0, 0);
			$pdf->text(20, 75, $nametext, $font, 18, array(<?php echo $theme['primary_colour_dompdf']; ?>));
			$pdf->text(20, 100, $addresstext, $font, 10, array(<?php echo $theme['primary_colour_dompdf']; ?>));
	}
</script>



<?php


//					if ($building_orientation == "PORTRAIT"){
					include('pdf_building_portrait.php');
//					}else{
//						include('pdf_building_landscape.php');
//					}
?>				
 
                         <?php
                        $suites_query = mysql_query($suites_query_string);
                        $i = 1;
                        //print_r($content_xml);
                        $suite_html = "";
                        $suite_new = "";
						$suite_model = "";
						$suite_leased = "";
                                                $suite_promoted = "";
						$spacer = 0;
						
						//foreach($content_xml->xpath('//suites') as $Suites) {
						$contig = 0;
						





						
                        while($row = mysql_fetch_array($suites_query)){
							$spacer = 0;
							$cross_out = '';
                            $suite_name = $row['suite_name'];
                            $suite_id = $row['suite_id'];
                            //$suite_floor = substr($suite_name, 0,1);
						// Set up English vs French sqft formatting
                                                    if ($lang == "en_CA") {
                                                            $suite_net_rentable_area = $row['net_rentable_area'];
                                                            $suite_net_rentable_area = number_format($suite_net_rentable_area);
                                                            $suite_contiguous_area = $row['contiguous_area'];
                                                            $suite_contiguous_area = number_format($suite_contiguous_area);
                                                            $suite_min_divisible_area = $row['min_divisible_area'];
                                                            $suite_min_divisible_area = number_format($suite_min_divisible_area);
                                                    }
                                                    if ($lang == "fr_CA") {
                                                            $suite_net_rentable_area = $row['net_rentable_area'];
                                                            $suite_net_rentable_area = number_format($suite_net_rentable_area, 0,',',' ');
                                                            $suite_contiguous_area = $row['contiguous_area'];
                                                            $suite_contiguous_area = number_format($suite_contiguous_area, 0, ',', ' ');
                                                            $suite_min_divisible_area = $row['min_divisible_area'];
                                                            $suite_min_divisible_area = number_format($suite_min_divisible_area, 0, ',', ' ');
                                                    }
							
							
							
                            $suite_net_rent = $row['net_rent'];
							
                            $suite_add_rent_total = $row['add_rent_total'];
			
							
                            $suite_description = strip_tags($row['description']);


                            if ($i % 2 == 0){
                                $suite_html .= '<tr class="even">';
                            }else{
                                $suite_html .= '<tr class="odd">';	
                            }      
                            $i = $i + 1;                      
							
                        $suite_new = $row['new'];
                        $suite_model = $row['model'];
                        $suite_leased = $row['leased'];
                        $suite_promoted = $row['promoted'];


                        if ($suite_new == "true") {
                                if ($lang == "en_CA"){
                                        $suite_html .= '<td class="icon"><img src="'. $language['vacancy_report_website'] . '/images/icon-new_EN.png"></td>';
                                }else{
                                        $suite_html .= '<td class="icon"><img src="'. $language['vacancy_report_website'] . '/images/icon-new_FR.png"></td>';								
                                        }
                                $cross_out = '';
                                $spacer = 1;
                        }

                        if ($suite_model == "true") {
                                if ($lang == "en_CA"){							
                                        $suite_html .= '<td class="icon"><img src="'. $language['vacancy_report_website'] . '/images/icon-model-suite_EN.png"></td>';
                                }else{
                                        $suite_html .= '<td class="icon"><img src="'. $language['vacancy_report_website'] . '/images/icon-model-suite_FR.png"></td>';
                                        }
                                $cross_out = '';
                                $spacer = 1;
                        }

                        if ($suite_leased == "true") {
                                if ($lang == "en_CA"){	
                                        $suite_html .= '<td class="icon"><img src="'. $language['vacancy_report_website'] . '/images/icon-leased_EN.png"></td>';
                                }else{
                                        $suite_html .= '<td class="icon"><img src="'. $language['vacancy_report_website'] . '/images/icon-leased_FR.png"></td>';								
                                        }$cross_out = ' style="text-decoration:line-through;"';
                                $spacer = 1;
                        }
                        

                        
                        if ($suite_promoted == "true") {
                            if ($lang == "en_CA"){						
                                    $suite_html .= '<td class="icon"><img src="'. $language['vacancy_report_website'] . '/images/icon-promoted_EN.png"></td>';
                            }else{
                                    $suite_html .= '<td class="icon"><img src="'. $language['vacancy_report_website'] . '/images/icon-promoted_FR.png"></td>';
                            }
                            $cross_out = ' style="font-style: italic;font-weight: bold;"';
                            $spacer = 1;
                        }
                        
                        
                        
                        

                        if ($spacer == 0) {
                                $suite_html .= '<td class="icon">&nbsp;</td>';
                        }



        $suite_pdf_query_string = "select * from files_2d where building_code = '".$building_code."' and suite_name = '".$row['suite_name']."'";
        $suite_pdf_query = mysql_query($suite_pdf_query_string) or die ("Suite pdf query error: " . mysql_error());
        $suite_pdf = mysql_fetch_array($suite_pdf_query);
        $suite_pdf_plan = $suite_pdf['plan_name'];


							
                                if ($theme['result_links_to_corp_website'] == 1) {
                                    
                                    $suite_html .= '<td class="contiguous"><span' . $cross_out . '>' .$suite_name . '</span></td>';
                                    
                                } else {
                                    
                                    if ($suite_contiguous_area != 0 && $suite_contiguous_area != ''){
                                        $contig = 1;
                                        $suite_html .= '<td class="contiguous"><span' . $cross_out . '><a class="contiguous" href="'. $language['vacancy_report_website'] .'/suite.php?building=' . $building_code . '&suiteid=' . $suite_id . $queries->get_query_str() . '">' . addslashes($suite_name) . '</a></span></td>';
                                    }else{
                                        $suite_html .= '<td class="suite"><span' . $cross_out . '><a href="'. $language['vacancy_report_website'] .'/suite.php?building=' . $building_code . '&suiteid=' . $suite_id . $queries->get_query_str() . '">' . $suite_name_short . '</a></span></td>';
                                    }
                                }

			// Suite Area
                            $suite_html .= '<td class="area"><span' . $cross_out . '>' . $suite_net_rentable_area . "<br />";
							
			
			// Suite Contiguous Area
							
            if ((intval($suite_contiguous_area) != 0) && (intval($suite_min_divisible_area) != 0)) {
                $suite_html .= '(' . $suite_min_divisible_area . ' | ' . $suite_contiguous_area . ')</span></td>';
            } else if ((intval($suite_contiguous_area) == 0) && (intval($suite_min_divisible_area) != 0)) {
                $suite_html .= '(' . $suite_min_divisible_area . ')</span></td>';
            } else if ((intval($suite_contiguous_area) != 0) && (intval($suite_min_divisible_area) == 0)) {
                $suite_html .= '(' . $suite_contiguous_area . ')</span></td>';
            } else {
                $suite_html .= '</span></td>';
            }
                            
                            
                            
                            
                            
                            
                            
                            
			// Suite Rent
                            $suite_html .= '<td class="rent"><span' . $cross_out . '>';
                            if ($suite_net_rent == "0" || $suite_net_rent == ""){
                                $suite_html .= $language['negotiable_text']."</span></td>";
                            }else{
                                if (strpos($suite_net_rent, '$') > -1){
                                    $suite_html .= money_format('%.2n',$suite_net_rent) . "</span></td>";
                                }else{
                                    $suite_html .= money_format('%.2n',$suite_net_rent) . "</span></td>";
                                }
                            }
							
			// Suite Availability
						
                            $suite_html .= '<td class="availability"><span' . $cross_out . '>' . get_suite_availability($language, $row) . "</span></td>";
							
                            
                            
if ($building_type == "Industrial" || $building_type == "Industriel") {
    
    $suite_specs_query_string = "select shipping_doors_drive_in, shipping_doors_truck, available_electrical_amps, clear_height from suites_specifications where building_id = '".$building_id."' and suite_id = '".$row['suite_id']."'";
    $suite_specs_query = mysql_query($suite_specs_query_string) or die ("Suite specs query error: " . mysql_error());
    $suite_specs = mysql_fetch_array($suite_specs_query);
    
    
    $shippingDI = $suite_specs['shipping_doors_drive_in'];
    $shippingTL = $suite_specs['shipping_doors_truck'];
    $poweramps = $suite_specs['available_electrical_amps'];
    $ceiling = $suite_specs['clear_height'];
            $loadingoutputDI = '';
            $loadingoutputTL = '';
            $loadingoutput = '';

            if ($shippingDI != '' && $shippingDI != 0) {
                $loadingoutputDI = $shippingDI . " " . $language['drivein_loading_abbreviation'];
            } 
            if ($shippingTL != '' && $shippingTL != 0) {
                $loadingoutputTL = $shippingTL . " " . $language['truck_loading_abbreviation'];
            }
            if ($loadingoutputDI != '' && $loadingoutputTL != '') {
                $loadingoutput = $loadingoutputDI . "<br />" . $loadingoutputTL;
            } else if ($loadingoutputDI != '' && $loadingoutputTL == '') {
                $loadingoutput = $loadingoutputDI;
            } else if ($loadingoutputDI == '' && $loadingoutputTL != '') {
                $loadingoutput = $loadingoutputTL;
            } else {
                // both are empty leave a space
                $loadingoutput = "&nbsp;";
            }
    


    $suite_html .='<td class="loading"><span ' . $cross_out . '>' . $loadingoutput . '</span></td>';
    $suite_html .='<td class="power"><span ' . $cross_out . '>' . ($poweramps != 0 ? $poweramps . " A" : '') . '</span></td>';
    $suite_html .='<td class="ceiling"><span ' . $cross_out . '>' . $ceiling . '</span></td>';
    

    $suite_html .='<td class="description"><span ' . $cross_out . '>' . strip_tags(str_replace('&nbsp;', '', $suite_description)) . '</span></td>';
    $suite_html .='</tr>';
} else {
    $suite_html .='<td class="description"><span ' . $cross_out . '>' . strip_tags(str_replace('&nbsp;', '', $suite_description)) . '</span></td>';
    $suite_html .='</tr>';
}
                            

							
                        } // end while($row = mysql_fetch_array($suites_query)){
						
                        
  ?>



                
<!-- Property Description and Building Features -->

                <table class="features">
                    <tr>
                    	<td colspan="2">
                            <h3><?php echo $language['property_desc_text']; ?></h3>
                            <p><?php echo strip_tags(nl2br($building_desc),'<br>'); ?></p>
                        </td>
                    </tr>
                    
                        <?php if ($sections_count > 0) { 
                        echo "<tr>";
						}
                        
						$sec = 1;
                        while($Section = mysql_fetch_array($sections_query)){ 
							$section_title = trim($Section['section_title']);
							$section_content = strip_tags(nl2br($Section['section_content']),'<br>');
							
						echo '<td valign="top">';
							echo "<h3>".$section_title."</h3>";
							echo "<p>".$section_content."</p>";		
						echo "</td>";  
					
					if ($sections_count > 2) {
						if (($sec % 2 == 0) && ($sec != "4")) {
							echo "</tr><tr>";						
						}
					}
						
						
						$sec++;
						
                        }
                        
                        echo "</tr>";
                        ?>
   


 
                </table>

               
            <div id="breakit"></div>
         
         
 
            
            
            

                <table class="availabilities">
                <thead>
                	<tr>
                    <td colspan="7" id="availabilities-heading">
                    <h3><?php echo $language['available_space_text']; ?></h3>
                    </td>
                 	</tr>
                    <tr>
                    	<th>&nbsp;</th>
                        
                        
                <?php if ($building_type == "Industrial" || $building_type == "Industriel") {
                    echo '<th>' . $language['unit_text'] . '</th>';
                } else {
                    echo '<th>' . $language['suite_text'] . '</th>';
                }     
                ?>
      
                        
                        <th><?php echo $language['area_text']; ?><br />
                        (<?php echo $language['min_divisible_text'] ?> | <?php echo $language['contiguous_text']; ?>)
                        </th>
                        <th><?php echo $language['rent_text']; ?></th>
                        <th><?php echo $language['availability_header_text']; ?></th>
        <?php if ($building_type == "Industrial" || $building_type == "Industriel") { ?>
                        
        <?php } else { ?>
                        <th><?php echo $language['notes_text']; ?></th>
        <?php } ?>
                        <?php 

                        
                if ($building_type == "Industrial" || $building_type == "Industriel") {
                    echo '<th class="description">' . $language['loading_text'] . '</th>';
                    echo '<th class="description">' . $language['power_text'] . '</th>';
                    echo '<th class="description">' . $language['ceiling_text'] . '</th>';
                    echo '<th class="description">' . $language['notes_text'] . '</th>';
                        }
                        ?>  
                    </tr> 
                 </thead> 
                    <?php 

$is_retail = 0;	
if ($Building['building_type'] == "Retail") {
	$is_retail = 1;	
}
if ($Building['building_type'] == "Détail") {
	$is_retail = 1;	
}



if (($suitecount > 0) && ($Building['no_vacancies'] == 0)) {
	// Normal building with available suites
	echo $suite_html;	
	if ($contig == 1){
		echo '<tr><td align="left" class="contiguous-text" colspan="3">** ' . $language['contiguous_premises_text'] . '</td></tr>';
		
	}
} 
// True no vacancy building
// Prevents 'no vacancy' results due to search parameters
elseif (($suitecount == 0) && ($Building['no_vacancies'] == 1) && ($is_retail == 0)) {
	echo "<tr><td colspan=7>";
	echo '<div class="fullyleased1">' . $language['all_leased_text']. '</div>
	<div class="fullyleased2">' . $language['no_availabilities_text'] . '</div>';
	echo "</td></tr>";		
} 
// Retail building
elseif (($suitecount == 0) && ($Building['no_vacancies'] == 0) && ($is_retail == 1)) {
	echo "<tr><td colspan=7>";
	echo '<div class="fullyleased1">' . $language['call_for_availability_text'] . '</div>';
	echo "</td></tr>";	
}


					?>
                </table>
                <!-- eo availabilities //-->
               
               <?php 
			   	$additional_costs_html = "";
			   
			   
	if (strlen($building_add_rent_operating) > 0 || strlen($building_add_rent_realty) > 0 || strlen($building_add_rent_power) > 0 || strlen($building_add_rent_total) > 0) {
			if (strlen($building_add_rent_operating) > 0){
				 $additional_costs_html .= '<td>' . $language['add_rent_operating_text'] . ': ' . money_format('%.2n',$building_add_rent_operating) .' '.$language['per_sqft_text'].'</td>';
			}
			if (strlen($building_add_rent_realty) > 0){
				$additional_costs_html .= '<td>' . $language['add_rent_realty_text'] . ': ' . money_format('%.2n',$building_add_rent_realty) .' '.$language['per_sqft_text'].'</td>';
			}
			if (strlen($building_add_rent_power) > 0){
				if ($building_add_rent_power == "0"){
					$additional_costs_html .= '<td>' . $language['add_rent_power_text'] . ': Included</td>';
				}else{
					$additional_costs_html .= '<td>' . $language['add_rent_power_text'] . ': ' . money_format('%.2n',$building_add_rent_power) .' '.$language['per_sqft_text'].'</td>';
				}
			}
			if (strlen($building_add_rent_total) > 0){
				$additional_costs_html .= '<td>' . $language['add_rent_total_text'] . ': ' . money_format('%.2n',$building_add_rent_total) .' '.$language['per_sqft_text'].'</td>';
			}
			if ($additional_costs_html != ""){
				echo '<table id="additional-costs"><tr>' . $additional_costs_html . '</tr></table>';
			}
			
	}// end big if strlen...
                                ?>
                <!-- eo additional-costs -->


<!-- end availabilities & new page -->
<div id="breakit"></div>


         
                <!-- BEGIN GALLERY -->
                <table class="building-gallery">
                <tr>
                <?php


	// PREVIEW IMAGES QUERY
	$preview_images_query = mysql_query("select * from preview_images where building_code ='" . $building_code . "' and lang='". $lang_id ."' order by preview_index asc limit 6") or die ("preview images query error: " . mysql_error());
	$preview_images_count = mysql_num_rows($preview_images_query);
	
	
	if (mysql_num_rows($preview_images_query) == 0){

        }else{
		
		
				$i=1;
					while ($row = mysql_fetch_array($preview_images_query)) {
						$preview_image = $row['file_name'];
							if($i%3 == 0) {
								echo '<td><img src="' . $language['vacancy_report_website'] . '/images/preview_images/' . $preview_image . '" width="230" height="180" /></td></tr></table><table class="building-gallery"><tr>';
							} else {
								echo '<td><img src="' . $language['vacancy_report_website'] . '/images/preview_images/' . $preview_image . '" width="230" height="180" /></td>';
							}
								$i++;

					}
	}
	?>
            </tr></table>
                <!-- END GALLERY -->


        	
  </body> 
  
<?php  } // closing if suites query ?>

<?php  } // closing if suites query ?>    
            
            
            
	      
                








<?php
} // end foreach ($addresses as $building_code)
?>

</html>

