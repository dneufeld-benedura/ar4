<?php
$spec_qy = mysql_query("select * from building_specifications where building_code='" . $building_code . "' and lang = " . $lang_id . "");
$row = mysql_fetch_array($spec_qy);


$lang = "en_CA";
$lang_id = 1;
if (isset($_REQUEST['lang'])) {
    if ($_REQUEST['lang'] == "fr_CA") {

        $lang = "fr_CA";
        $lang_id = 0;
    } else {
        
    }
} else {
    
}

setlocale(LC_ALL, $lang . '.utf8');



$suitecount_query_string = 'SELECT COUNT(*) AS realsuitecount FROM suites alls WHERE building_code = ? AND lang=1 AND leased="false"';
// Preparing the statement
$stmt = $db->prepare($suitecount_query_string);
// binding the parameters
$stmt->bind_param('s', $building_code);
// Executing the query
if (!$stmt->execute()) {
    trigger_error('The query execution failed; MySQL said (' . $stmt->errno . ') ' . $stmt->error, E_USER_ERROR);
}
$col1 = null;
$stmt->bind_result($suiteCount);
while ($stmt->fetch()) {
    // having to use this while makes no sense at all but it doesn't work w/o it.
}
$stmt->close();


?>





<section class="fs_section fs_building-features">
    <div class="row">
        <div class="col">
            <div class="card fs_card-features">
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <div class="row">
                                <div class="col fs_col-feature-icon">
                                    <i class="material-icons fs_icon-feature">photo_size_select_small</i>
                                </div>
                                <div class="col fs_col-feature-value">
                                    <h3 class="card-title fs_card-feature-title"><?php echo $language['size_text']; ?> / <?php echo $language['building_information_sq_ft_trans']; ?></h3>
                                    <?php $total_space = format_numbers($row['total_space'], $language); ?>
                                    <p class="fs_feature-value"><?php echo $total_space; ?></p>                                                                
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="row">
                                <div class="col fs_col-feature-icon">
                                    <i class="material-icons fs_icon-feature">local_shipping</i>
                                </div>
                                <div class="col fs_col-feature-value">
                                    <h3 class="card-title fs_card-feature-title"><?php echo $language['loading_text']; ?></h3>
                                    <p class="fs_feature-value"><?php echo ($row['const_shipping_doors_drivein'] !== '' ? $row['const_shipping_doors_drivein'] : $language['na_text']); ?> / <?php echo ($row['const_shipping_doors_truck'] !== '' ? $row['const_shipping_doors_truck'] : $language['na_text']); ?></p>
                                    </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="row">
                                <div class="col fs_col-feature-icon">
                                    <i class="material-icons fs_icon-feature">store</i>
                                </div>
                                <div class="col fs_col-feature-value">
                                    <h3 class="card-title fs_card-feature-title"><?php echo $language['units_available_text']; ?></h3>
                                    <p class="fs_feature-value"><?php echo $suiteCount; ?></p>                                                                
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="row">
                                <div class="col fs_col-feature-icon">
                                    <i class="material-icons fs_icon-feature">flash_on</i>
                                </div>
                                <div class="col fs_col-feature-value">
                                    <h3 class="card-title fs_card-feature-title"><?php echo $language['power_text']; ?></h3>
                                    <p class="fs_feature-value"><?php echo ($row['const_available_electrical_volts'] !== '' ? $row['const_available_electrical_volts'] : $language['na_text']); ?>v / <?php echo ($row['const_available_electrical_amps'] !== '' ? $row['const_available_electrical_amps'] : $language['na_text']); ?>a</p>                                                                
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="row">
                                <div class="col fs_col-feature-icon">
                                    <i class="material-icons fs_icon-feature">attach_money</i>
                                </div>
                                <div class="col fs_col-feature-value">
                                    <h3 class="card-title fs_card-feature-title"><?php echo $language['price_text']; ?> / <?php echo $language['month_abbr']; ?></h3>
                                    <p class="fs_feature-value"><?php echo $language['negotiable_text']; ?></p>                                                                
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="row">
                                <div class="col fs_col-feature-icon">
                                    <i class="material-icons fs_icon-feature">vertical_align_top</i>
                                </div>
                                <div class="col fs_col-feature-value">
                                    <h3 class="card-title fs_card-feature-title"><?php echo $language['ceiling_text']; ?></h3>
                                    <p class="fs_feature-value"><?php echo ($row['const_ceiling_height'] !== '' ? $row['const_ceiling_height'] : $language['na_text']); ?></p>                                                                
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>




<section class="fs_section fs_suite-specifications">
    <div class="card fs_card-specifications">
        <div class="card-body">
            <div class="card-title"><h3><?php echo $language['tenant_costs_title_text']; ?></h3></div>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['realty_tax_text']; ?></div>
                <div class="col fs_specification-value"><?php echo money_format('%.2n', (double) $row['costs_realty']); ?></div>
            </div>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['operating_costs_text']; ?></div>
                <div class="col fs_specification-value"><?php echo money_format('%.2n', (double) $row['costs_operating']); ?></div>
            </div>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['additional_rent_total_text']; ?></div>
                <div class="col fs_specification-value"><?php echo money_format('%.2n', (double) $row['costs_add_rent_total']); ?></div>
            </div>
        </div>
    </div>

    <div class="card fs_card-specifications">
        <div class="card-body">
            <div class="card-title"><h3><?php echo $language['building_size_title_text']; ?> (<?php echo $language['building_information_sq_ft_trans']; ?>)</h3></div>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['total_space_text_nocolon']; ?></div>
                <div class="col fs_specification-value"><?php echo $total_space; ?></div>
            </div>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['number_of_floors_text']; ?></div>
                <div class="col fs_specification-value"><?php echo $row['floors']; ?></div>
            </div>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['total_office_space_text']; ?></div>
                <?php $total_office_space = format_numbers($row['total_office_space'], $language); ?>
                <div class="col fs_specification-value"><?php echo $total_office_space; ?></div>
            </div>
        </div>
    </div>

    <div class="card fs_card-specifications">
        <div class="card-body">
            <div class="card-title"><h3><?php echo $language['construction_title_text']; ?></h3></div>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['fibre_optic_capability_text']; ?></div>
                <div class="col fs_specification-value"><?php checkOrX($row['const_fibre_optic_capability']); ?></div>
            </div>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['shipping_receiving_text']; ?></div>
                <div class="col fs_specification-value"><?php checkOrX($row['const_shipping_receiving']); ?></div>
            </div>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['ceiling_height_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row['const_ceiling_height'] !== '' ? $row['const_ceiling_height'] : $language['na_text']); ?></div>
            </div>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['washrooms_per_floor_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row['const_washrooms_per_floor'] !== '' ? $row['const_washrooms_per_floor'] : $language['na_text']); ?></div>
            </div>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['number_of_elevators_text']; ?></div>
                <?php $total = (int) $row['elevators_hirise'] + (int) $row['elevators_midrise'] + (int) $row['elevators_lowrise']; ?>
                <div class="col fs_specification-value"><?php echo $total; ?></div>
            </div>
        </div>
    </div>

    <div class="card fs_card-specifications">
        <div class="card-body">
            <div class="card-title"><h3><?php echo $language['safety_and_access_text']; ?></h3></div>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['fire_detection_system_text']; ?></div>
                <div class="col fs_specification-value"><?php checkOrX($row['safety_fire_detection']); ?></div>
            </div>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['sprinkler_system_text']; ?></div>
                <div class="col fs_specification-value"><?php checkOrX($row['safety_sprinkler_system']); ?></div>
            </div>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['manned_security_text']; ?></div>
                <div class="col fs_specification-value"><?php checkOrX($row['safety_manned_security']); ?></div>
            </div>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['security_system_text']; ?></div>
                <div class="col fs_specification-value"><?php checkOrX($row['safety_security_system']); ?></div>
            </div>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['barrier_free_acccess_text']; ?></div>
                <div class="col fs_specification-value"><?php checkOrX($row['safety_barrier_free']); ?></div>
            </div>
        </div>
    </div>


    <div class="card fs_card-specifications">
        <div class="card-body">
            <div class="card-title"><h3><?php echo $language['parking_text']; ?></h3></div>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['surface_stalls_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row['parking_surface_stalls'] !== '' ? $row['parking_surface_stalls'] : $language['na_text']); ?></div>
            </div>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['total_stalls_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row['parking_total_stalls'] !== '' ? $row['parking_total_stalls'] : $language['na_text']); ?></div>
            </div>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['above_gnd_stalls_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row['parking_above_stalls'] !== '' ? $row['parking_above_stalls'] : $language['na_text']); ?></div>
            </div>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['below_gnd_stalls_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row['parking_below_stalls'] !== '' ? $row['parking_below_stalls'] : $language['na_text']); ?></div>
            </div>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['below_gnd_ratio_text']; ?></div>
                <div class="col fs_specification-value"><?php echo ($row['parking_below_ratio'] !== '' ? str_replace('\\', '/', $row['parking_below_ratio']) . ' ' . $language['building_information_sq_ft_trans'] : $language['na_text']); ?></div>
            </div>
        </div>
    </div>




    <div class="card fs_card-specifications">
        <div class="card-body">
            <div class="card-title"><h3><?php echo $language['public_transport_title_text']; ?></h3></div>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['transit_surface_route_text']; ?></div>
                <div class="col fs_specification-value"><?php checkOrX($row['public_surface_route']); ?></div>
            </div>
            <div class="row">
                <div class="col fs_specification-label"><?php echo $language['direct_subway_access_text']; ?></div>
                <div class="col fs_specification-value"><?php checkOrX($row['public_subway_access']); ?></div>
            </div>
        </div>
    </div>

</section>



<section class="fs_section fs_suite-notes">
    <div class="card fs_card-notes">
        <div class="card-body">
            <div class="row">
                <div class="col fs_col-card-notes-icon">
                    <i class="material-icons fs_notes-icon">insert_drive_file</i>
                    <h2 class="fs_notes-title">Notes</h2>
                </div>
                <div class="col fs_col-card-notes-text">
                    <h3 class="fs_notes-title"><?php echo $language['building_description_text']; ?></h3>
                    <p class="fs_notes-text"><?php echo $building_desc; ?></p>
<!--                    <h3 class="fs_notes-title">Sublease</h3>
                    <p class="fs_notes-text">Sed tempus sed fermentum hendrerit. Suspendisse ullamcorper euismod dignissim. etiam commodo varius lacus, et laoreet neque tristique. Phasellus eleifend urna sed accumsan scelerisque. Nulla quis semper lacus, eget congue eros.</p>-->
                </div>
            </div>
        </div>
    </div>
</section>
