<?php
//This file is used as the means to redirect to all the necessary work that needs to be done for the Broker Updates

	include('../theme/db.php');
        include_once('./Mainscript/Mainscript.php');
	include_once('../objects/LanguageQuery.php');


//do the procedural steps here
$logic = 'multiple';
new Redirect($logic);
//Initiate the Object Oriented approach


/**
 * This class is used as a middle-man between the running cron and the client, to generate and send the e-mail alerts.
 * It "Redirects" the cron, ececuting either the joined or the single e-mail alerts.
 * @author RationalRoot
 * @version AR2.0
 * @since December 2014
 */
class Redirect{
    /**
     * A constructor method, accepting the $logic variable from the $_REQUEST['logic']
     * This is where it is decided whether the emails are single-send or multiple-join 
     * If the $_REQUEST['logic'] is not 'single' or not set, 'multiple' is assumed.
     */
    public function __construct($logic) {
        if ($logic=='multiple'){self::multipleSend();}
    }
    

    
    /**
     * A redirection for sending the joined, multiple e-mail(s).
     */
    private static function multipleSend(){
        $mainscript = new Mainscript(); //var_dump($_REQUEST);
         if (isset($_REQUEST['stored_array'])){ 
         $cluster = unserialize(base64_decode($_REQUEST['stored_array']));         
        
        //For each element in array, add the information using the mainscript
        //This will join all the e-mails together and then at the end, it will send as an e-mail.
        $email = $cluster[0]['email']; //since all e-mails are same, the first e-mail will hold for all cases

        foreach($cluster as $data){
        $mainscript->multiple_main($data);
        }

        
        $mainscript->sendMultipleMessages($email);
        }
   }



}

