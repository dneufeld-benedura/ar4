<?php

	include('../theme/db.php');
	include('../objects/LanguageQuery.php');
	include('../objects/Queries.php');
	// Initiate the Queries class
	$queries               = new Queries();
	date_default_timezone_set("Canada/Eastern");

	
		
		// Language Query
		$languageQuery = new LanguageQuery();
		$langArray = $languageQuery -> getLanguageAndID();
		$lang = $langArray[0];
		$lang_id = $langArray[1];
		$language_query_string = 'select * from languages_dynamic ld, languages l where ld.lang_id = ' . $lang_id . ' and l.lang_id = ' . $lang_id;

		$language_query = mysql_query($language_query_string)or die("language query error: ". mysql_error());
		$language = mysql_fetch_array($language_query);
	
setlocale(LC_ALL, $language['locale_string']);

	// Theme Query
	$theme_query = mysql_query("select * from theme");
	$theme       = mysql_fetch_array($theme_query);


        
        function startsWith($haystack, $needle) {
        // search backwards starting from haystack length characters from the end
        return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== FALSE;
        }

        function endsWith($haystack, $needle) {
            // search forward starting from end minus needle length characters
            return $needle === "" || strpos($haystack, $needle, strlen($haystack) - strlen($needle)) !== FALSE;
        }
        
        
	function get_query_str(){
		if ($_SERVER['QUERY_STRING'] == ""){
			return "";
		}else{
			return $_SERVER['QUERY_STRING'];
		}
	}

		if (isset($_REQUEST['province'])){
			$province_selected = $_REQUEST['province'];
		}else{
			if (isset($_REQUEST['lang']) && $_REQUEST['lang'] == 'fr_CA'){
				$province_selected = $default_province_frc;
			}else{
				$province_selected = $default_province_eng;
			}
		}

		if (isset($_REQUEST['SuiteType'])){
			$SuiteType_selected = $_REQUEST['SuiteType'];
		}else{
			$SuiteType_selected = "Office";
		}

		if (isset($_REQUEST['region'])){				
			$region_selected = $_REQUEST['region'];
		}else{
			$region_selected = "All";
		}
		
		if (isset($_REQUEST['sub-region'])){				
			$sub_region_selected = $_REQUEST['sub-region'];
		}else{
			$sub_region_selected = "All";
		}
		
		if (isset($_REQUEST['Size'])){
			$size_selected = $_REQUEST['Size'];
		}else{
			$size_selected = "All";
		}
		
		if (isset($_REQUEST['Availability'])){
			$availability_selected = $_REQUEST['Availability'];
			switch($availability_selected){
				
				case 'All':
					$availability_notafter = 2147483640;
					$availability_notbefore = 0; 
					break;
				case 'Immediately':
					$availability_notafter = time();
					$availability_notbefore = 0; 
					break;
				case 'Under 3 Months':
					$availability_notafter = strtotime('+3 Month');
					$availability_notbefore = 0; 
					break;
				case '3-6 Months':
					$availability_notafter = strtotime('+6 Month');
					$availability_notbefore = strtotime('+3 Month');
					break;
				case '6-12 Months':
					$availability_notafter = strtotime('+1 Year');
					$availability_notbefore = strtotime('+6 Month');
					break;
				case 'Over 12 Months':
					$availability_notafter = 2147483640;
					$availability_notbefore = strtotime('+1 Year');
					break;
				case 'Tous':
					$availability_notafter = 2147483640;
					$availability_notbefore = 0; 
					break;
				case 'Immédiatement':
					$availability_notafter = time();
					$availability_notbefore = 1; 
					break;
				case 'Moins de 3 mois':
					$availability_notafter = strtotime('+3 Month');
					$availability_notbefore = 1; 
					break;
				case '3-6 mois':
					$availability_notafter = strtotime('+6 Month');
					$availability_notbefore = strtotime('+3 Month');
					break;
				case '6-12 mois':
					$availability_notafter = strtotime('+1 Year');
					$availability_notbefore = strtotime('+6 Month');
					break;
				case 'Plus de 12 mois':
					$availability_notafter = 2147483639;
					$availability_notbefore = strtotime('+1 Year');
					break;
			}
			
		}else{
			$availability_notafter = 2147483640;
			$availability_notbefore = 0; 
		}

		$uniqueid = $_REQUEST['unique'];
		$email = $_REQUEST['email'];
                $lang = $_REQUEST['lang'];
                
                
                // Broker Updates subscription query
		$bu_sub_query_string = "select * from bu_subscriptions where unique_id = '".$uniqueid."' and active = '1'";
		$bu_sub_query = mysql_query($bu_sub_query_string);
		$bu_sub = mysql_fetch_array($bu_sub_query);
                $bu_sub_title = $bu_sub['title'];
                while(startsWith($bu_sub_title," |")){$bu_sub_title = substr($bu_sub_title, 3);}
                if ($bu_sub_title==''){$bu_sub_title=$language['all_listings'];}
                $bu_sub['title'] = $bu_sub_title;
                
                
                

		function append_to_query_str($query_string, $var, $appendee){
		if ($query_string == "" || $query_string == "&"){
			return $var . "=" . $appendee;
		}else{
			return $query_string . "&" . $var . "=" . $appendee;
		}
	}
	
	if ($theme['wrapper_width'] != '') {
	$wrapper_width = $theme['wrapper_width'];
} else {
	$wrapper_width = '960';
}
?>



<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <meta name="viewport" content="width=<?php echo $wrapper_width; ?>" />
      <title><?php echo $language['page_title']; ?></title>
      <link rel="shortcut icon" href="../theme/favicon.ico" type="image/x-icon" />
      <link rel="stylesheet" type="text/css" href="../css/reset.css" />
      <link rel="stylesheet" type="text/css" href="../css/styles.css" />
      <?php include('../css/colour_styles.php'); ?>
<!--      <link rel="stylesheet" type="text/css" href="css/map.css" />
      <link rel="stylesheet" href="css/pagination.css" />
      <link type="text/css" href="css/smoothness/jquery-ui-1.8.13.custom.css" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="css/dialogue-pdf-config.css">
	  <link href="lightbox/css/lightbox.css" rel="stylesheet" />-->
      <script src="../lightbox/js/jquery-1.10.2.min.js"></script>
      <script type="text/javascript" src="../js/jquery-ui-1.8.13.custom.min.js"></script>
      <script type="text/javascript" src="../js/jquery.pagination.js"></script> 
<!--  <script src="js/cufon-yui.js" type="text/javascript"></script> -->
      <script type="text/javascript" src="../js/jquery.tools.min.js"></script>
      <script type="text/javascript" src="../js/jquery.jdialogue.js"></script>
<!--      <script type="text/javascript">var switchTo5x=true;</script>
      <script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>-->

<script type="text/javascript"> 
	function addslashes (str) {
		// Escapes single quote, double quotes and backslash characters in a string with backslashes  
		// 
		// version: 1103.1210
		// discuss at: http://phpjs.org/functions/addslashes    // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
		// +   improved by: Ates Goral (http://magnetiq.com)
		// +   improved by: marrtins
		// +   improved by: Nate
		// +   improved by: Onno Marsman    // +   input by: Denny Wardhana
		// +   improved by: Brett Zamir (http://brett-zamir.me)
		// +   improved by: Oskar Larsson Högfeldt (http://oskar-lh.name/)
		// *     example 1: addslashes("kevin's birthday");
		// *     returns 1: 'kevin\'s birthday'    
		return (str + '').replace(/[\\"']/g, '\\$&').replace(/\u0000/g, '\\0');
	}


	// When document is ready, initialize pagination
	$(document).ready(function(){     
	

		
		$('#loader').hide();

		$('#province').change(function(){
			// type
			$('#region').fadeOut();
			$('#sub-region').fadeOut();
			$('#loader').show();

			$.post("../region.php", {
				province: $('#province').val()
				<?php
				if (isset($_REQUEST['lang'])){
					echo ", lang: '" . $_REQUEST['lang'] . "'"; 
				}
				?>
			}, function(response){
				setTimeout("finishAjax('region', '"+escape(response)+"')", 400);
			});
			
			
			$.post("../sub-region.php", {
				province: $('#province').val(),
				region: ''
				<?php
				if (isset($_REQUEST['lang'])){
					echo ", lang: '" . $_REQUEST['lang'] . "'"; 
				}
				?>
			}, function(response){
				setTimeout("finishAjax('sub-region', '"+escape(response)+"')", 400);
			});
			
			
			return false;
		});


			
		$('#region').change(function(){
		// make
		$('#sub-region').fadeOut();
		$('#loader').show();

		var slashed = $('#region').val();
		//slashed = escape(slashed);
		//slashed = slashed;

		$.post("../sub-region.php", {
			//province: $('#province').val(),
			region: slashed
			<?php
			if (isset($_REQUEST['lang'])){
				echo ", lang: '" . $_REQUEST['lang'] . "'"; 
			}
			?>
		}, function(response){
			setTimeout("finishAjax('sub-region', '"+escape(response)+"')", 400);
		});
		return false;
	});




	//END DOCUMENT READY	
	});
	
		function finishAjax(id, response){
		$('#loader').hide();
		$('#'+id).html(unescape(response));
		$('#'+id).fadeIn();
	}



</script>




<style>
body {
		
}

#search-form {
	top: auto;	
}

.dropdown-label {
	text-align: left;
	padding-left: 0;	
}

input#email {
	margin-left: 10px;	
}

/*a.modal-button {
	background: #608e3a;
	border: 1px solid #608e3a;
	padding: 5px;
	color: #fff !important;
	text-transform: uppercase;
	text-align: center;
	text-decoration: none;
	width: 50px;
	font-weight: bold;
	margin-right: 5px;
	display: inline-block;
	zoom:1;
	*display:inline;	
}*/

table.summarytable {
	margin-bottom: 20px;
	background: #efefef;
	border-top: 1px solid #ccc;	
}

table.summarytable td {
	font-size: 14px;
	padding: 5px;
	width: 120px;
}

table.summarytable td.summarydata {
	font-size: 14px;
	padding: 5px;
	width: 200px;
}

p {
    margin: 0px;
}

</style>


</head>
<body>
 <?php 
 
$uri = 'unique='.$bu_sub['unique_id'].'&'.$bu_sub['bu_query_string'].'&bu_name='.$bu_sub['bu_name'].'&email='.$bu_sub['email'].'&oo_manage=manage'; 
 
 
$url = 'http'.(empty($_SERVER['HTTPS'])?'':'s').'://'.$_SERVER['SERVER_NAME']."/index.php?".$uri;


$englishUrl = $url."&lang=en_CA";
$frenchUrl = $url."&lang=fr_CA";

$switchToEnglish = '<span style="margin: 0px auto;font-size: 15px;"><a target="_top" href="'.$englishUrl.'">English</a> - <span>';
$switchToFrench = '<span style="margin: 0px auto;font-size: 15px;"><a target="_top" href="'.$frenchUrl.'">Français</a><span>';
$switchToEnglishNoLink = '<span style="margin: 0px auto;font-size: 15px;"><a>English</a> - <span>';
$switchToFrenchNoLink = '<span style="margin: 0px auto;font-size: 15px;"><a>Français</a><span>';                                 
?>
 


         <div class="clear"></div>
         <!-- eo clear //-->

	<div class="clearfix">

<h1 class="page-title"><?php echo $language['modify_subscription_title_text']; ?></h1>
<p class="modal-copy"><?php echo $language['modify_subscription_instruction_text'].$bu_sub['title'].'.'; ?></p>
<h2 class="modal-title"><?php echo $language['search_criteria_text']; ?></h2>
<div id="search-form">  
			<form action="bu_manage.php" method="get" name="buildingsearch" id="buildingsearch">
				<table valign="top" cellpadding="0" cellspacing="0" border="0">
					<tr>
<!--FIRST - ROW-->
                        <td class="<?php echo $language['left_dropdown_padding_class']; ?>"><?php echo $language['province_text']; ?></td>
						<td class="search-field">
		
       						<select id="province" class="selectbox" name="province" size="1">
								<?php 
								$building_province_query = mysql_query("select distinct province from buildings where buildings.lang=" . $lang_id . " order by province asc");
								$building_province_option_query = mysql_query("select distinct province from buildings INNER JOIN suites USING(building_code) where buildings.lang=1 order by province asc");
								echo '<option value="All">' . $language['all'] . '</option>';
                                while ($provinces = mysql_fetch_array($building_province_query)){
									//just making sure its not an empty string
									$provinces_options = mysql_fetch_array($building_province_option_query);
									if ($provinces_options['province'] != ""){
										
										if ($provinces_options['province'] == $province_selected){
											echo '<option selected="yes" value="' . $provinces_options['province'] .'">' . $provinces['province'] .'</option>'."\r\n";
										}else{
											echo '<option value="' . $provinces_options['province'] .'">' . $provinces['province'] .'</option>'."\r\n";
										}
									}
                                }
                                ?>
			
							</select>
						</td>
                        <td class="dropdown-label"><?php echo $language['region_text']; ?></td>
						<td colspan="2" class="search-field">
			
                            <select id="region" name="region" class="selectbox" size="1">
                                <?php 
                                
								//now query the english option values for the form selects
								$query = "SELECT DISTINCT b.building_code, region FROM buildings b where province='" . mysql_real_escape_string($province_selected) . "' and region != '' and b.lang=1 group by region";
								$building_region_query = mysql_query($query);
								//echo $query;
								
								//output the default option for the select.
								echo '<option value="All">' . $language['all'] . '</option>\r\n';
								
								//now iterate the english option values
								while ($regions = mysql_fetch_array($building_region_query)){
									
									$current_lang_options_array = mysql_query("select region from buildings where building_code ='" . $regions['building_code'] . "' and lang=" .$lang_id);
									$current_lang_options_result = mysql_fetch_array($current_lang_options_array);
									
									//check that the region isn't just the empty string
									if ($regions['region'] != ""){
										//check if this option value is "selected"
										if ($regions['region'] == $region_selected){
											echo '<option selected="yes" value="' . $regions['region'] .'">' .$current_lang_options_result['region'] .'</option>'."\r\n";
										}else{
											echo '<option value="' . $regions['region'] .'">' . $current_lang_options_result['region'] .'</option>'."\r\n";
										}
									}
								}
                                ?>
			
							</select>
						</td>
                        
                        </tr>
                        <!-- SPACER ROW //-->
<tr>
	<td colspan="8" height="10">&nbsp;</td>
</tr>

<!-- eo SPACER ROW //-->
                        
						<tr>
						  <td class="dropdown-label"><?php echo $language['sub_region_text']; ?></td>
						<td class="search-field">
			
                            <select id="sub-region" name="sub-region" class="selectbox"  size="1">
                            <?php
                                
								//now query the english option values for the form selects
								$query = "SELECT DISTINCT b.building_code, sub_region FROM buildings b where region='" . mysql_real_escape_string($region_selected) . "' and sub_region != '' and b.lang=1 group by sub_region";
								$building_sub_region_query = mysql_query($query);
								//echo $query;
								
								//output the default option for the select.
								echo '<option value="All">' . $language['all'] . '</option>\r\n';
								
								//now iterate the english option values
								while ($sub_regions = mysql_fetch_array($building_sub_region_query)){
									
									//echo "select sub_region from buildings where building_index = " . $sub_regions['building_index'] . " and lang=" .$lang_id;
									$current_lang_options_array = mysql_query("select sub_region from buildings where building_code ='" . $sub_regions['building_code'] . "' and lang=" .$lang_id);
									$current_lang_options_result = mysql_fetch_array($current_lang_options_array);
									
									//check that the region isn't just the empty string
									if ($sub_regions['sub_region'] != ""){
										//check if this option value is "selected"
										if ($sub_regions['sub_region'] == $sub_region_selected){
											echo '<option selected="yes" value="' . $sub_regions['sub_region'] .'">' .$current_lang_options_result['sub_region'] .'</option>'."\r\n";
										}else{
											echo '<option value="' . $sub_regions['sub_region'] .'">' . $current_lang_options_result['sub_region'] .'</option>'."\r\n";
										}
									}
								}
                                ?>
			
							</select>
						</td>
  
						<td class="<?php echo $language['left_dropdown_padding_class']; ?>"><?php echo $language['space_type_text']; ?></td>
						<td colspan="2" class="search-field">
			
            <?php
			
			$building_types_query_string = "select distinct s.suite_type from suites s INNER JOIN building_types bt ON (s.suite_type=bt.building_type) where s.lang='" . $lang_id . "' order by bt.building_type asc";
			$building_types_query = mysql_query($building_types_query_string) or die ("suite type error: " . mysql_error());

			$building_types_option_query_string = "select distinct s.suite_type from suites s INNER JOIN building_types bt ON (s.suite_type=bt.building_type) where s.lang='" . $lang_id . "' order by bt.building_type asc";
			$building_types_option_query = mysql_query($building_types_option_query_string) or die ("suite type option error: " . mysql_error());
					
								
			?>
            
            
            <select class="selectbox" name="SuiteType" id="SuiteType" size="1">
            
				<?php 
                
					echo '<option value="All">' . $language['all'] . '</option>\r\n';
				while ($building_types = mysql_fetch_array($building_types_query)){
				
                                                                            $type = $building_types['suite_type'];
                                        if ($lang_id=='0'){
                                                if ($type == "OFFICE"){
                                                $type = "Bureau";
                                                }else if ($type == "INDUSTRIAL"){
                                                $type = "Industriel";
                                                }else if ($type == "RETAIL"){
                                                $type = "Détail";
                                                }   
                                        }
                                        else{
                                                if ($type == "OFFICE"){
                                                $type = "Office";
                                                }else if ($type == "INDUSTRIAL"){
                                                $type = "Industrial";
                                                }else if ($type == "RETAIL"){
                                                $type = "Retail";
                                                }    
                                        }
                                    
                                    $building_types_options = mysql_fetch_array($building_types_option_query);
					if ($building_types_options['suite_type'] == $SuiteType_selected){
					echo '<option selected="yes" value="' . $building_types_options['suite_type'] .'">' . $type .'</option>'."\r\n";
					}else{
					echo '<option value="' . $building_types_options['suite_type'] .'">' . $type .'</option>'."\r\n";
               		 }
                }
                ?>
            </select>
						</td>
                        
                        </tr>
<!-- SPACER ROW //-->
<tr>
	<td colspan="8" height="10">&nbsp;</td>
</tr>

<!-- eo SPACER ROW //-->
                        <tr>
                        
                        
                        
                        
						<td class="dropdown-label"><?php echo $language['size_text']; ?></td>
						<td class="search-field">
		
							<select class="selectbox" name="Size" id="Size" size="1">
								<?php 
								$sizes_query = mysql_query("select distinct size from sizes where lang=" . $lang_id . " order by size_index asc") or die ("building size error: " . mysql_error());
								$sizes_option_query = mysql_query("select distinct size from sizes where lang=1 order by size_index asc") or die ("building size option error: " . mysql_error());
                                while ($size_row = mysql_fetch_array($sizes_query)){
									$size_row_options = mysql_fetch_array($sizes_option_query);
                                	if ($size_row_options['size'] == $size_selected){
										echo '<option selected="yes" value="' . $size_row_options['size'] .'">' . $size_row['size'] .'</option>'."\r\n";
									}else{
                                    	echo '<option value="' . $size_row_options['size'] .'">' . $size_row['size'] .'</option>'."\r\n";
									}
								}
								?>
							</select>
						</td>
						
                        <td class="dropdown-label"><?php echo $language['availability_text']; ?></td>
						<td colspan="2" class="search-field">
		
							<select class="selectbox" name="Availability" id="Availability" size="1">
								<?php 
								$avail_query = mysql_query("select distinct availability_period from availabilities where lang=" . $lang_id . " order by availability_index asc") or die ("building availability error: " . mysql_error());
								$avail_option_query = mysql_query("select distinct availability_period from availabilities where lang=1 order by availability_index asc") or die ("building availability option error: " . mysql_error());
                                while ($avail_row = mysql_fetch_array($avail_query)){
									$avail_row_options = mysql_fetch_array($avail_option_query);
                                	if ($avail_row_options['availability_period'] == $availability_selected){
										echo '<option selected="yes" value="' . $avail_row_options['availability_period'] .'">' . $avail_row['availability_period'] .'</option>'."\r\n";
									}else{
                                    	echo '<option value="' . $avail_row_options['availability_period'] .'">' . $avail_row['availability_period'] .'</option>'."\r\n";
									}
								}
								?>
							</select>
						</td>
                        
                        
                       </tr>
                       
<!-- SPACER ROW //-->
<tr>
	<td colspan="8" height="10">&nbsp;</td>
</tr>

<!-- eo SPACER ROW //-->


<!-- SPACER ROW //-->
<tr>
	<td colspan="8" height="10">&nbsp;</td>
</tr>





<!-- SPACER ROW //-->
<tr>
    <td colspan="2" height="10">&nbsp;</td>
    <td colspan="2" height="10">
        
        <?php
        if ($lang == "en_CA") {
            //
            $selected_en = 'selected="selected"'; 
        } else {
            //
            $selected_fr = 'selected="selected"';
        }
        
        ?>
        

    </td>
    <td colspan="4" height="10">&nbsp;</td>
</tr>


						<tr> 

                        
                        <td class="search-label" align="left">
                        	
							<!--<input class="<?php //echo $button_class; ?>" name="Submit" type="Submit" value="<?php //echo $search_text; ?>" id="Submit" /> -->

                    <input type="hidden" name="action" value="edit" />
                    <input type="hidden" name="email" value="<?php echo $bu_sub['email']; ?>" />
                    <input type="hidden" name="unique" value="<?php echo $uniqueid; ?>" />
<!--                    <input type="hidden" name="lang" value="<?php echo $lang; ?>" />
                    <input name="lang" type="hidden" value="<?php echo $lang; ?>" id="lang" />-->
						</td>
                        
							
                       <td></td>
                        <td></td>
                        <td align="right" valign="top"><a href="#" class="modal-button" onclick="buildingsearch.submit()">Ok</a></td>
                        <td valign="top"><a href="bu_manage_list.php?unique=<?php echo $uniqueid; ?>&amp;<?php echo get_query_str(); ?>&amp;email=<?php echo $bu_sub['email']; ?>&amp;lang=<?php echo $lang; ?>" class="modal-button"><?php echo $language['cancel_button_text']; ?></a></td>
                        </tr>



<!-- SPACER ROW //-->
<tr>
	<td colspan="8" height="10">&nbsp;</td>
</tr>
<!-- SPACER ROW //-->
<tr>
	<td colspan="8" height="10">&nbsp;</td>
</tr>

<!-- eo SPACER ROW //-->
				</table>

</form>

       	</div>    
       
       
       
       
       
 
    </div>
    <!-- eo content //-->

</body>
</html>