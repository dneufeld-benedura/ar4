<?php

/**
 * @author Rational Root
 * @see ../main_abstract/SearchData_abstract.php
 * @since March 2014
 * @version 1.6
 * */

class Graphics {
    /*
     * This method is meant to take any path to png image, and add any text on top of it.
     * Please make sure that you have all of the necessary priviledges (such as chmod 777) for a directory where new images are to be generated.
     * Currently, the method is using Arial font only, but this should be subjected to a change.
     * Should you like to change the colour of the font, adjust the $textcolor variable (or overload the method to accept the RGB integers).
     * To center the letters (should you change the font or size), adjust the $leftTextPos variable.
     * <p>
     * @param  {String} $text - The text to write on top of PNG icon
     * @param  {String} $iconPath - The path to PNG icon
     * @param  {String} $outputFolder - The path of the folder to output the PNG icon into 
     * @returns {String} $out - The path of the newly created file
     * @see 
     * */

    public function addTextToPNG($text, $iconPath, $outputFolder) {
        try {
            $text = strtoupper($text);
            $out = $outputFolder . $text . ".png";


            $exists = file_exists($out);
            if ($exists) {
                return $out;
            }

// load icon image
            $im = imagecreatefrompng($iconPath);

//for transparency
            imageSaveAlpha($im, true);
            imageAlphaBlending($im, false);



// Gray shadow and blue text
            $textcolor = imagecolorallocate($im, 103, 103, 103);
            $shadow = imagecolorallocate($im, 125, 125, 125);


// get the width and the height of the image
            $width = imagesx($im);
            $height = imagesy($im);

//for transparency
            $transparent = imageColorAllocateAlpha($im, 0, 0, 0, 127);
            imageAlphaBlending($im, true);

//load the font from path
            $font = 'images/newFileExtensions/arial.ttf';

//TO CENTER THE TEXT ON ICON CHANGE THE NUMBER n in n*strlen($text) 
            $leftTextPos = ( $width - 20 * strlen($text) ) / 2;

//imagettftext($im, 20, 0, $leftTextPos+2, ($height/2)+17, $shadow, $font, $text);
            imagettftext($im, 20, 0, $leftTextPos, ($height / 2) + 15, $textcolor, $font, $text);

// Output the image
            copy($iconPath, $out);
            imagepng($im, $out);
            imagedestroy($im);
            return $out;
        } catch (Exception $e) {
            return $iconPath;
        }
    }

}

?>
