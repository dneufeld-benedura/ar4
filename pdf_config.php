<?php
$lang    = "en_CA";
$lang_id = 1;
if (isset($_REQUEST['lang'])) {
    if ($_REQUEST['lang'] == "fr_CA") {
        //	
        $lang    = "fr_CA";
        $lang_id = 0;
    } else {
        //				
    }
} else {
    //	
}
include('theme/db.php');

// Language Query

	$language_query_string = 'select * from languages_dynamic ld, languages l where ld.lang_id = ' . $lang_id . ' and l.lang_id = ' . $lang_id;

$language_query = mysql_query($language_query_string) or die("language query error: " . mysql_error());
$language = mysql_fetch_array($language_query);

// Theme Query
$theme_query = mysql_query("select * from theme");
$theme       = mysql_fetch_array($theme_query);



//setting locale for time, date, money, etc
setlocale(LC_ALL, $language['locale_string']);



?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>PDF Selection</title>
</head>

<body>
<form id="print-pdf-search">
<ul class="clearfix">
	<li>
    	<input id="view-thumb" type="radio" name="view" value="Thumbnail" checked="yes"><label for="view-thumb"><?php
echo $language['thumbnail_view_text'];
?></label><br/>
        <img src="images/dialogue-view-thumbs.jpg" width="125" height="125" />
    </li>
    <li>
    	<input id="view-condensed" type="radio" name="view" value="Condensed"><label for="view-condensed"><?php
echo $language['condensed_view_text'];
?></label><br/>
        <img src="images/dialogue-view-condensed.jpg" width="125" height="125" />
    </li>
    <li>
    	<input id="property-pages" type="radio" name="view" value="Properties"><label for="property-pages"><?php
echo $language['property_pages_text'];
?></label><br/>
        <img src="images/dialogue-view-properties.jpg" width="125" height="125" />
    </li>
</ul>
</form>
</body>
</html>
