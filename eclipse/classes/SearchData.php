<?php

/**
 * @author Rational Root
 * @see ../main_abstract/SearchData_abstract.php
 * @since March 2014
 * @version 1.6
 * */
class SearchData {
    
    
    
   /**
     * This mehtod gets the state of the include fully leased properties checkbox.
     * <p>
     * @return int
     */
    public function show_no_vacancy(){
        $show_no_vacancy = 0;
        	if (isset($_REQUEST['show_no_vacancy'])) {
		if ($_REQUEST['show_no_vacancy'] == 'on') {
			$show_no_vacancy = 1;
		} else {
			$show_no_vacancy = 0;
		}
                } else {
                        $show_no_vacancy = 1;
                }

                return $show_no_vacancy;

        }
    
    
    /*
     * This mehtod gets the selected tag from a Building page.
     * Accesses either the abstract or its overriden method from the classes it extends.
     * <p>
     * @returns {String} $_REQUEST['tag']
     * @see building.php
     *
     * */

    public function get_tag_selected() {
        return (isset($_REQUEST['tag']) ? $_REQUEST['tag'] : "All");
    }

    /*
     * This mehtod gets the selected size from a Building page.
     * Accesses either the abstract or its overriden method from the classes it extends.
     * <p>
     * @returns {String} $_REQUEST['Size']
     * @see building.php
     *
     * */

    public function get_size_selected_from() {
        return ((isset($_REQUEST['sizeFrom']) && $_REQUEST['sizeFrom'] != "") && (isset($_REQUEST['Size']) && $_REQUEST['Size'] == "Custom") ? $_REQUEST['sizeFrom'] : 0);
    }

    /*
     * This mehtod gets the selected maximum from a size-range from a Building page.
     * Accesses either the abstract or its overriden method from the classes it extends.
     * <p>
     * @returns {String} $_REQUEST['sizeFrom']
     * @see building.php
     *
     * */

    public function get_size_selected_to() {
        return ((isset($_REQUEST['sizeTo']) && $_REQUEST['sizeTo'] != "") && (isset($_REQUEST['Size']) && $_REQUEST['Size'] == "Custom") ? $_REQUEST['sizeTo'] : 0);
    }

    /*
     * This mehtod gets the current page result from a main Index page.
     * Accesses either the abstract or its overriden method from the classes it extends.
     * <p>
     * @return {String} $_REQUEST['CurrentPage']
     * @see index.php
     *
     * */

    public function get_current_page_result() {
        return (isset($_REQUEST['CurrentPage']) ? $_REQUEST['CurrentPage'] : 0);
    }

    /*
     * This mehtod gets the selected province.
     * Accesses either the abstract or its overriden method from the classes it extends.
     * <p>
     * @param {Array} $language
     * @returns {String} $language['default_province_eng'] or $language['default_province_frc']
     * @see building.php
     * @see index.php
     * @see suite.php
     * */

    public function get_province_selected($language, $lang, $db) {

        if (strpos($_SERVER['QUERY_STRING'], 'province') !== false && (isset($_REQUEST['province']))) {
            return $_REQUEST['province'];
        } else if ($language['default_province'] == '') {
            return "All";
        } else {

            $language_id = "1";
            if ($lang == 'fr_CA') {
                $language_id = 0;
            }
//            $sqlquery = "SELECT province FROM buildings where lang = " . $language_id . " AND province='" . ucfirst($language['default_province']) . "'";
//            $suites_query = mysql_query($sqlquery) or die('query error in SearchData.php, get_province_selected: ' . mysql_error());
//            if (mysql_num_rows($suites_query) > 0) {
//                return $language['default_province'];
//            }
//            return "All";
            
            
            $province_query = array();  
            $province_query_string = "SELECT province FROM buildings where lang=? AND province=?";
            if ($province_query_statement = $db->prepare($province_query_string)) {
                // carry on
            } else {
                trigger_error('Wrong province_query_string SQL: ' . $province_query_string . ' Error: ' . $db->error, E_USER_ERROR);
            }
            $province_query_statement->bind_param('ss', mysqli_real_escape_string($db, $language_id), mysqli_real_escape_string($db, ucfirst($language['default_province'])));
            if ($province_query_statement->execute()){
                $result = $province_query_statement->get_result();
                while($row = $result->fetch_array(MYSQLI_ASSOC)){ 
                    $province_query[] = $row;
                };
            } 
            $province_query_statement->close();
            if (count($province_query) > 0) {
                return $province_query[0]; 
            }
            else {
                return "All";
            }

        }
    }

    /*
     * This mehtod gets the selected building type from the Building page.
     * Accesses either the abstract or its overriden method from the classes it extends.
     * <p>
     * @returns {String} $_REQUEST['BuildingType']
     * @see building.php
     * */

    public function get_buildingtype_selected() {

        return (isset($_REQUEST['SuiteType']) ? $_REQUEST['SuiteType'] : "All");
    }

    /*
     * This mehtod gets the selected suite type from either a Building or a main Index page.
     * Accesses either the abstract or its overriden method from the classes it extends.
     * <p>
     * @param {Array} $language
     * @returns {String} $_REQUEST['SuiteType']
     * @see building.php
     * @see index.php
     * */

    public function get_suitetype_selected($language) {

        if (strpos($_SERVER['QUERY_STRING'], 'SuiteType') !== false && (isset($_REQUEST['SuiteType']))) {
            return $_REQUEST['SuiteType'];
        } else if ($language['default_spacetype'] == '') {
            return "All";
        } else {

            $language_id = "1";
            if ($_REQUEST['lang'] == 'fr_CA') {
                $language_id = 0;
            }
            $sqlquery = "SELECT building_type FROM buildings where lang = " . $language_id . " AND building_type='" . ucfirst($language['default_spacetype']) . "'";
            $suites_query = mysql_query($sqlquery) or die('query error in SearchData.php, get_suitetype_selected: ' . mysql_error());
            if (mysql_num_rows($suites_query) > 0) {
                return $language['default_spacetype'];
            }
            return "All";
        }
    }

    /*
     * This mehtod gets the selected region from the main Index page.
     * Accesses either the abstract or its overriden method from the classes it extends.
     * <p>
     * @param {Array} $language
     * @returns {String} $_REQUEST['region']
     * @see index.php
     * */

    public function get_region_selected($language, $lang, $db) {

        if (strpos($_SERVER['QUERY_STRING'], 'region') !== false && (isset($_REQUEST['region']))) {
            return $_REQUEST['region'];
        } else if ($language['default_region'] == '') {
            return "All";
        } else {

            $language_id = "1";
            if ($_REQUEST['lang'] == 'fr_CA') {
                $language_id = 0;
            }
            $sqlquery = "SELECT region FROM buildings where lang = " . $language_id . " AND region='" . $language['default_region'] . "'";
            $suites_query = mysql_query($sqlquery) or die('query error in SearchData.php, get_region_selected: ' . mysql_error());
            if (mysql_num_rows($suites_query) > 0) {
                return $language['default_region'];
            }
            return "All";
        }
    }
    
    public function get_city_selected($language, $lang, $db) {

        if (strpos($_SERVER['QUERY_STRING'], 'city') !== false && (isset($_REQUEST['city']))) {
            return $_REQUEST['city'];
        } else if ($language['default_region'] == '') {
            return "All";
        } else {

            $language_id = "1";
            if ($_REQUEST['lang'] == 'fr_CA') {
                $language_id = 0;
            }
            $sqlquery = "SELECT city FROM buildings where lang = " . $language_id . " AND city='" . $language['default_region'] . "'";
            $suites_query = mysql_query($sqlquery) or die('query error in SearchData.php, get_city_selected: ' . mysql_error());
            if (mysql_num_rows($suites_query) > 0) {
                return $language['default_region'];
            }
            return "All";
        }
    }

    /*
     * This mehtod gets the selected sub-region from the main Index page.
     * Accesses either the abstract or its overriden method from the classes it extends.
     * <p>
     * @param {Array} $language
     * @returns {String} $_REQUEST['sub-region']
     * @see index.php
     * */

    public function get_sub_region_selected($language, $lang, $db) {

        if (strpos($_SERVER['QUERY_STRING'], 'sub-region') !== false && (isset($_REQUEST['sub-region']))) {
            return $_REQUEST['sub-region'];
        } else if ($language['default_subregion'] == '') {
            return "All";
        } else {
            $language_id = "1";
            if ($_REQUEST['lang'] == 'fr_CA') {
                $language_id = 0;
            }
            $sqlquery = "SELECT sub_region FROM buildings where lang = " . $language_id . " AND sub_region='" . ucfirst($language['default_subregion']) . "'";
            $suites_query = mysql_query($sqlquery) or die('query error in SearchData.php, get_sub_region_selected: ' . mysql_error());
            if (mysql_num_rows($suites_query) > 0) {
                return $language['default_subregion'];
            }
            return "All";
        }
    }

    /*
     * This mehtod gets the selected sub-region from Building or the main Index page.
     * Accesses either the abstract or its overriden method from the classes it extends.
     * <p>
     * @returns {String} $_REQUEST['Size']
     * @see building.php
     * @see index.php
     * */

    public function get_size_selected() {
        return (isset($_REQUEST['Size']) ? $_REQUEST['Size'] : "All");
    }

    /*
     * This mehtod gets the selected availability array, 'availability_notafter' and 'availability_notbefore' elements from a Building or the main Index page.
     * Accesses either the abstract or its overriden method from the classes it extends.
     * <p>
     * @returns {Array} $array
     * @see building.php
     * @see index.php
     * */

    public function get_availability_selected() {
        $array = array();
        if (isset($_REQUEST['Availability'])) {
            switch ("" . $_REQUEST['Availability']) {

                case 'All':
                    $array['availability_notafter'] = 2147483640;
                    $array['availability_notbefore'] = 0;
                    break;
                case 'Immediately':
                    $array['availability_notafter'] = time();
                    $array['availability_notbefore'] = 1;
                    break;
                case 'Under 3 Months':
                    $array['availability_notafter'] = strtotime('+3 Month');
                    $array['availability_notbefore'] = 1;
                    break;
                case '3-6 Months':
                    $array['availability_notafter'] = strtotime('+6 Month');
                    $array['availability_notbefore'] = strtotime('+3 Month');
                    break;
                case '6-12 Months':
                    $array['availability_notafter'] = strtotime('+1 Year');
                    $array['availability_notbefore'] = strtotime('+6 Month');
                    break;
                case 'Over 12 Months':
                    $array['availability_notafter'] = 2147483639;
                    $array['availability_notbefore'] = strtotime('+12 Month');
                    break;
                case 'Tous':
                    $array['availability_notafter'] = 2147483640;
                    $array['availability_notbefore'] = 0;
                    break;
                case 'Immédiatement':
                    $array['availability_notafter'] = time();
                    $array['availability_notbefore'] = 1;
                    break;
                case 'Moins de 3 mois':
                    $array['availability_notafter'] = strtotime('+3 Month');
                    $array['availability_notbefore'] = 1;
                    break;
                case '3-6 mois':
                    $array['availability_notafter'] = strtotime('+6 Month');
                    $array['availability_notbefore'] = strtotime('+3 Month');
                    break;
                case '6-12 mois':
                    $array['availability_notafter'] = strtotime('+1 Year');
                    $array['availability_notbefore'] = strtotime('+6 Month');
                    break;
                case 'Plus de 12 mois':
                    $array['availability_notafter'] = 2147483639;
                    $array['availability_notbefore'] = strtotime('+12 Month');
                    break;
            }
        } else {
            $array['availability_notafter'] = 2147483640;
            $array['availability_notbefore'] = 0;
        }

        return $array;
    }

    
    
      /*
     * This mehtod gets the selected availability array, 'availability_notafter' and 'availability_notbefore' elements from a Building or the main Index page.
     * Accesses either the abstract or its overriden method from the classes it extends.
     * <p>
     * @param $checkStr the availability string to process
     * @returns {Array} $array
     * @see building.php
     * @see index.php
     * */

    public function get_availability_selected_String($checkStr) {

        if ($checkStr!=null) {
            switch ("" . $checkStr) {

                case 'All':
                    $array['availability_notafter'] = 2147483640;
                    $array['availability_notbefore'] = 0;
                    break;
                case 'Immediately':
                    $array['availability_notafter'] = time();
                    $array['availability_notbefore'] = 1;
                    break;
                case 'Under 3 Months':
                    $array['availability_notafter'] = strtotime('+3 Month');
                    $array['availability_notbefore'] = 1;
                    break;
                case '3-6 Months':
                    $array['availability_notafter'] = strtotime('+6 Month');
                    $array['availability_notbefore'] = strtotime('+3 Month');
                    break;
                case '6-12 Months':
                    $array['availability_notafter'] = strtotime('+1 Year');
                    $array['availability_notbefore'] = strtotime('+6 Month');
                    break;
                case 'Over 12 Months':
                    $array['availability_notafter'] = 2147483639;
                    $array['availability_notbefore'] = strtotime('+12 Month');
                    break;
                case 'Tous':
                    $array['availability_notafter'] = 2147483640;
                    $array['availability_notbefore'] = 0;
                    break;
                case 'Immédiatement':
                    $array['availability_notafter'] = time();
                    $array['availability_notbefore'] = 1;
                    break;
                case 'Moins de 3 mois':
                    $array['availability_notafter'] = strtotime('+3 Month');
                    $array['availability_notbefore'] = 1;
                    break;
                case '3-6 mois':
                    $array['availability_notafter'] = strtotime('+6 Month');
                    $array['availability_notbefore'] = strtotime('+3 Month');
                    break;
                case '6-12 mois':
                    $array['availability_notafter'] = strtotime('+1 Year');
                    $array['availability_notbefore'] = strtotime('+6 Month');
                    break;
                case 'Plus de 12 mois':
                    $array['availability_notafter'] = 2147483639;
                    $array['availability_notbefore'] = strtotime('+12 Month');
                    break;
            }
        } else {
            $array['availability_notafter'] = 2147483640;
            $array['availability_notbefore'] = 0;
        }

        return $array;
    }
    
    
    
    
    
    
    
    
    /*
     * This mehtod gets the building code address from the main Index page.
     * Accesses either the abstract or its overriden method from the classes it extends.
     * <p>
     * @returns {String} $_REQUEST['Address']
     * @see index.php
     * */

    public function get_building_codes_narrow() {
        return ((isset($_REQUEST['Address']) && $_REQUEST['Address'] != "") ? explode(',', $_REQUEST['Address']) : null);
    }

    /*
     * This mehtod gets the narrow building codes array, the 'building_name_sterm' and 'building_name_sterm_prepop' elements from the main Index page.
     * Accesses either the abstract or its overriden method from the classes it extends.
     * <p>
     * @returns {Array} $array
     * @see index.php
     * */

    public function get_building_name_sterm_array() {

        if (isset($_REQUEST['building_name']) && $_REQUEST['building_name'] != "" && $_REQUEST['building_name'] != "none") {
            $array['building_name_sterm'] = $_REQUEST['building_name'];
            $array['building_name_sterm_prepop'] = $array['building_name_sterm'];
        } else {
            $array['building_name_sterm'] = "none";
            $array['building_name_sterm_prepop'] = "";
        }

        return $array;
    }
    
    
    
    
    /*
     * This mehtod gets the narrow building codes array, the 'building_name_sterm' and 'building_name_sterm_prepop' elements from the main Index page.
     * Accesses either the abstract or its overriden method from the classes it extends.
     * <p>
     * @param $str The string to process
     * @returns {Array} $array
     * @see index.php
     * */

    public function get_building_name_sterm_array_String($str) {

        if ($str!=null && $str != "" && $str != "none") {
            $array['building_name_sterm'] = $str;
            $array['building_name_sterm_prepop'] = $array['building_name_sterm'];
        } else {
            $array['building_name_sterm'] = "none";
            $array['building_name_sterm_prepop'] = "";
        }

        return $array;
    }

    
    
    
    
    
    
    
    // Advanced Industrial Suite Search
    
    
    
        /*
     * This method gets the selected industrial size element from Index page.
     * Accesses either the abstract or its overriden method from the classes it extends.
     * <p>
     * @returns {String} $_REQUEST['Size']
     * @see building.php
     * @see index.php
     * */

    public function get_unit_office_space_selected() {
        return (isset($_REQUEST['SuiteOfficeSize']) ? $_REQUEST['SuiteOfficeSize'] : "All");
    }
    
    
        /*
     * This method gets the selected industrial size element from Index page.
     * Accesses either the abstract or its overriden method from the classes it extends.
     * <p>
     * @returns {String} $_REQUEST['Size']
     * @see building.php
     * @see index.php
     * */

    public function get_unit_warehouse_space_selected() {
        return (isset($_REQUEST['SuiteWarehouseSize']) ? $_REQUEST['SuiteWarehouseSize'] : "All");
    }    
    
    
      
        /*
     * This method gets the selected industrial size element from Index page.
     * Accesses either the abstract or its overriden method from the classes it extends.
     * <p>
     * @returns {String} $_REQUEST['Size']
     * @see building.php
     * @see index.php
     * */

    public function get_unit_clear_height_selected() {
        return (isset($_REQUEST['SuiteClearHeight']) ? $_REQUEST['SuiteClearHeight'] : "All");
    }     
    
    
      
        /*
     * This method gets the selected industrial size element from Index page.
     * Accesses either the abstract or its overriden method from the classes it extends.
     * <p>
     * @returns {String} $_REQUEST['Size']
     * @see building.php
     * @see index.php
     * */

    public function get_unit_bay_size_width_selected() {
        return (isset($_REQUEST['BaySizeWidth']) ? $_REQUEST['BaySizeWidth'] : "All");
    }   
    
        /*
     * This method gets the selected industrial size element from Index page.
     * Accesses either the abstract or its overriden method from the classes it extends.
     * <p>
     * @returns {String} $_REQUEST['Size']
     * @see building.php
     * @see index.php
     * */

    public function get_unit_bay_size_depth_selected() {
        return (isset($_REQUEST['BaySizeDepth']) ? $_REQUEST['BaySizeDepth'] : "All");
    }   
    
        /*
     * This method gets the selected industrial size element from Index page.
     * Accesses either the abstract or its overriden method from the classes it extends.
     * <p>
     * @returns {String} $_REQUEST['Size']
     * @see building.php
     * @see index.php
     * */

    public function get_unit_shipping_doors_selected() {
        return (isset($_REQUEST['SuiteShippingDoors']) ? $_REQUEST['SuiteShippingDoors'] : "All");
    }         
    
    
    
        /*
     * This method gets the selected industrial size element from Index page.
     * Accesses either the abstract or its overriden method from the classes it extends.
     * <p>
     * @returns {String} $_REQUEST['Size']
     * @see building.php
     * @see index.php
     * */

    public function get_unit_drivein_doors_selected() {
        return (isset($_REQUEST['SuiteDriveInDoors']) ? $_REQUEST['SuiteDriveInDoors'] : "All");
    }   
    
    
        /*
     * This method gets the selected industrial size element from Index page.
     * Accesses either the abstract or its overriden method from the classes it extends.
     * <p>
     * @returns {String} $_REQUEST['Size']
     * @see building.php
     * @see index.php
     * */

    public function get_unit_electrical_volts_selected() {
        return (isset($_REQUEST['SuiteAvailElecVolts']) ? $_REQUEST['SuiteAvailElecVolts'] : "All");
    }    
        
    
    
        /*
     * This method gets the selected industrial size element from Index page.
     * Accesses either the abstract or its overriden method from the classes it extends.
     * <p>
     * @returns {String} $_REQUEST['Size']
     * @see building.php
     * @see index.php
     * */

    public function get_unit_electrical_amps_selected() {
        return (isset($_REQUEST['SuiteAvailElecAmps']) ? $_REQUEST['SuiteAvailElecAmps'] : "All");
    }        
    
    
      
    
    
    
        /*
     * This method gets the selected industrial size element from Index page.
     * Accesses either the abstract or its overriden method from the classes it extends.
     * <p>
     * @returns {String} $_REQUEST['Size']
     * @see building.php
     * @see index.php
     * */

    public function get_unit_parking_selected() {
        return (isset($_REQUEST['SuiteParking']) ? $_REQUEST['SuiteParking'] : "All");
    }          
    
    
        /*
     * This method gets the selected industrial size element from Index page.
     * Accesses either the abstract or its overriden method from the classes it extends.
     * <p>
     * @returns {String} $_REQUEST['Size']
     * @see building.php
     * @see index.php
     * */

    public function get_unit_parking_stalls_per_1000sf_selected() {
        return (isset($_REQUEST['SuiteParkingSurfaceStalls1000sf']) ? $_REQUEST['SuiteParkingSurfaceStalls1000sf'] : "All");
    }   
    

    
        /*
     * This method gets the selected industrial size element from Index page.
     * Accesses either the abstract or its overriden method from the classes it extends.
     * <p>
     * @returns {String} $_REQUEST['Size']
     * @see building.php
     * @see index.php
     * */

    public function get_unit_parking_stalls_selected() {
        return (isset($_REQUEST['SuiteParkingSurfaceStalls']) ? $_REQUEST['SuiteParkingSurfaceStalls'] : "All");
    }   
    
    
        /*
     * This method gets the selected industrial size element from Index page.
     * Accesses either the abstract or its overriden method from the classes it extends.
     * <p>
     * @returns {String} $_REQUEST['Size']
     * @see building.php
     * @see index.php
     * */

    public function get_unit_trucktrailer_parking_selected() {
        return (isset($_REQUEST['SuiteTruckTrailerParking']) ? $_REQUEST['SuiteTruckTrailerParking'] : "All");
    }      
    
    
    
    
}

?>
