<?php
$eol = "\n\r";
$eol2 = "\n\r\n\r";
$eolhtml = "<br>";
$eolhtml2 = "<br><br>";
date_default_timezone_set('America/New_York');



if ($_REQUEST['process'] == 'createPackage') {

    $packagetype = $_REQUEST['packagetype'];
    $building_code= $_REQUEST['building'];
    $building_id= $_REQUEST['building_id'];
    $lang_id = $_REQUEST['lang'];
    if (isset($_REQUEST['suitename']) && $_REQUEST['suitename'] != '') {
        $suite_name = $_REQUEST['suitename'];
        $suite_id = $_REQUEST['suite_id'];
    }
    
    include('theme/db.php');
    include('objects/DisplayHTML.php');
    include('objects/LanguageQuery.php');

    //Initiate DisplayHTML
    $displayHTML           = new DisplayHTML();

    

    echo $displayHTML-> zipBrokerPackage($lang_id, $building_id, $building_code, $packagetype, $suite_id, $db);

}
// end new package



if ($_REQUEST['process'] == 'sendEmail') {
    
    include('theme/db.php');
    include('objects/LanguageQuery.php');
    
    $lang_id = $_REQUEST['lang'];
    
    $language_query_string = 'select * from languages_dynamic ld, languages l, languages2 l2 where ld.lang_id = ' . $lang_id . ' and l.lang_id = ' . $lang_id . ' and l2.lang_id = ' . $lang_id . '';
//echo "languages query string: " . $language_query_string . "<br><br>";
    $language_query = mysql_query($language_query_string)or die("language query error: ". mysql_error());
    $language = mysql_fetch_array($language_query);
    date_default_timezone_set('America/New_York');
    
//echo "suiteid1: " . $suite_id . $eolhtml2;
    $building_selected = $_REQUEST['building'];
    if (isset($_REQUEST['suitename']) && $_REQUEST['suitename'] != '') {
        $suite_name = $_REQUEST['suitename'];
        $suite_id = $_REQUEST['suite_id'];
        $suite_sqft = $_REQUEST['suitesqft'];
        $suite_sqft = str_replace(",","",$suite_sqft);
    }
//echo "suiteid2: " . $suite_id . $eolhtml2;
    $packagetype = $_REQUEST['packagetype'];
    $from_email = $_REQUEST['fromemail'];
    $from_name = $_REQUEST['fromname'];

    $buildings_query_string = "select * from buildings where building_code='" . $building_selected . "' and lang=" . $lang_id;
    $buildings_query = mysql_query($buildings_query_string);
    $Building = mysql_fetch_array($buildings_query);
    $building_id = $Building['building_id'];
    $building_name = $Building['building_name'];
    $building_code = $Building['building_code'];
    $building_type = $Building['building_type'];
    $building_street = $Building['street_address']; 
    $building_city = $Building['city'];
    $building_province = $Building['province'];
    $building_region = $Building['region'];
    $building_sub_region = $Building['sub_region'];
    
   $contacts_query_string = "select * from contacts where building_code='" . $building_selected . "' and type='LEASING' and lang=" . $lang_id . " ORDER BY priority ASC LIMIT 2";
//echo "contacts query: " . $contacts_query_string . "<br><br>";
   $contacts_query = mysql_query($contacts_query_string) or die ("contacts query error: " . mysql_error());
   $i=0;
   while ($contacts = mysql_fetch_array($contacts_query)) {
       if ($i==0) {
           $contact_name1 = $contacts['name'];
           $contact_title1 = $contacts['title'];
           $contact_email1 = $contacts['email'];
           $contact_company1 = $contacts['company'];
       } else {
           $contact_name2 = $contacts['name'];
           $contact_title2 = $contacts['title'];
           $contact_email2 = $contacts['email'];
           $contact_company2 = $contacts['company'];
       }
       $i++;
   }
   
   
//echo "suiteid3: " . $suite_id . $eolhtml2;
$request_info_body = '';
if ($packagetype == "Building") {
    $request_info_body .='<p style = "font-size:13px;">' . $language['hi_text'] . ' ' . $from_name . ' ' . $language['brokerpackage_email_first_paragraph_text'] . '<a href="http://' . $_SERVER['HTTP_HOST'] . '/building.php?' . $_SERVER['QUERY_STRING'] . '">' . $building_street . ', ' . $building_city . ', ' . $building_province . '</a></p>' . $eolhtml2;
} else {
    $request_info_body .='<p style = "font-size:13px;">' . $language['hi_text'] . ' ' . $from_name . ' ' . $language['brokerpackage_email_first_paragraph_text'] . '<a href="http://' . $_SERVER['HTTP_HOST'] . '/suite.php?suiteid=' . $suite_id . '&' . $_SERVER['QUERY_STRING'] . '">' . $building_street . ', ' . $building_city . ', ' . $building_province . ', Suite ' . $suite_name . '</a></p>' . $eolhtml2;
}




// assemble and send the email


    if ($packagetype == "Building") {   
        $subject = $language['infopackage_email_subject_text'] . $building_name;
        $package_title_text = $language['package_email_title_text'] . $building_name;
    } else {
        $subject = $language['infopackage_email_subject_text'] . $building_name . ' ' . $suite_name;
        $package_title_text = $language['package_email_title_text'] . $building_name . ' Suite ' . $suite_name;
    }
    
    
    
    

    $message = '';
    $message .= '<html><head>' . $eol;
    $message .= '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />' . $eol;
    $message .= '<title> ' . $package_title_text . '</title>' . $eol;
    $message .= '</head><body>' . $eol;


    $message .= $eol . '<div><h3><div style="padding:5px 5px 5px 5px; background-color: #4B5F78;color: #fff;font-family: sans-serif;border-radius: 4px 4px 4px 4px;">' . $package_title_text . '</div></div>';
        
        


    $message .= $request_info_body;

        $message .= '<table style="width:100%"><tr>';
        $message .= '<td>&nbsp;</td>';
        $message .= '<td align="right">
<span style="margin: 110px auto;font-size: 15px;">
<span style="color:#000000">' . $language['powered_by'] . '</span>
<a target="_blank" href = "http://www.arcestra.com" style="text-decoration: none; white-space: nowrap;">
<span style="color:#4B5F78;letter-spacing: -3px;">A</span>
<span style="color:#4B5F78;letter-spacing: -3px;">R</span>
<span style="color:#4B5F78;letter-spacing: -3px;">C</span>
<span style="color:#EBD93D;letter-spacing: -3px; font-size: 21px;">e</span>
<span style="color:#40ABBB;letter-spacing: -3px;">S</span>
<span style="color:#584787;letter-spacing: -3px;">T</span>
<span style="color:#38B456;letter-spacing: -3px;">R</span>
<span style="color:#CF2C57;letter-spacing: -3px;">A</span>
</a>
</span>
</td>
</tr>
</table>';
    
    
    
    
        $message .= "</body></html>" . $eol;


        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
        $to_email = $contact_email1;
        if ($contact_email2 != '') {
            $headers .= "CC: " . $contact_email2 . "\r\n";
        }
        $headers .= 'From: ' . $from_name . ' <' . $from_email . '>' . $eol;
        
// Send second email to arcestra sales with same content
        $headers2  = 'MIME-Version: 1.0' . "\r\n";
        $headers2 .= 'Content-type: text/html; charset=utf-8' . "\r\n";
        $arcestra_email = "sales@arcestra.com";  
        $headers2 .= 'From: ' . $from_name . ' <' . $from_email . '>' . $eol;



     if (strpos($_SERVER['HTTP_HOST'],'rrrd.ca') > -1) {
        // Send any emails to Sean if we're on any rrrd.ca dev site
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
        $to_email = "sean@rationalroot.com";
        $arcestra_email = "sean@rationalroot.com";
        $headers .= 'From: ' . $from_name . ' <' . $from_email . '>' . $eol;
        @mail($to_email, $subject, $message, $headers, '-f updates@'.$emaildomain.'');
        @mail($arcestra_email, $subject, $message, $headers2, '-f updates@'.$emaildomain.'');
    } else {
        // send the email to leasing contacts
        @mail($to_email, $subject, $message, $headers, '-f updates@'.$emaildomain.'');
        // send the email to arcestra sales
        @mail($arcestra_email, $subject, $message, $headers2, '-f updates@'.$emaildomain.'');
    }   
        




//echo $headers;
//echo $message;  







// STATS TRACKING

        


if ($contact_email2 != '') {
    $submitcontacts = $contact_email1 . ', ' . $contact_email2;
} else {
    $submitcontacts = $contact_email1;
}
        
    
//    echo "BID: " . $building_id . "<br><br>";
//    echo "SID: " . $suite_id . "<br><br>";
    
    
$insert_packagedownload_query_string = "INSERT INTO stats_packagedownloads (
  clientname,
  timestamp,
  datetime,
  building_id,
  building_name,
  street_address,
  city,
  province,";
if ($packagetype == "Suite") {
    $suitelist = $suite_name;
    $insert_packagedownload_query_string .= "suite_id,";
}
$insert_packagedownload_query_string .="sqft,suite,spacetype,request_type,downloader_name,downloader_email,recipients_email) 
VALUES
  (
    '" . $language['clientname'] . "',
    '". time() . "',
    '". date("Y/m/d, g:ia") . "',
    '". $building_id ."',
    '". $building_name ."',
    '". $building_street ."',
    '". $building_city ."',
    '". $building_province ."',";

if ($packagetype == "Suite") {
    $suitelist = $suite_name;
    $insert_packagedownload_query_string .= "'". $suite_id . "','". $suite_sqft . "',";
} else {
    $suitelist = '';
    //  get available sqft of entire building
    $buildingSqft_query_string = "SELECT SUM(net_rentable_area) AS sqft FROM suites WHERE building_id = '".$building_id."' and leased = 'false'";
    $buildingSqft_query = mysql_query($buildingSqft_query_string) or die ("building sqft query error: " . mysql_error());
    $buildingSqft = mysql_fetch_array($buildingSqft_query);    

    $insert_packagedownload_query_string .= "'". $buildingSqft['sqft'] . "',";
}

$insert_packagedownload_query_string .= "'". $suitelist . "',
    '". $building_type . "',
    '". $packagetype ."',
    '". $from_name . "',
    '". $from_email ."',
    '". $submitcontacts ."'
  ) ;";

echo "querystring: " . $insert_packagedownload_query_string . "<br><br>";

$insert_packagedownload_query = mysql_query($insert_packagedownload_query_string) or die ("insert stats_packagedownload error: " . mysql_error());

}
?>