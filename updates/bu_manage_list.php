<?php
		$lang = "en_CA";
		$lang_id = 1;
		if (isset($_REQUEST['lang'])){
			if ($_REQUEST['lang'] == "fr_CA"){
			//	
				$lang = "fr_CA";
				$lang_id = 0;
			}else{
		//		
			}
		}else{	
		}
                
                $langselected = "en_CA";                
                if (isset($_REQUEST['subscription_language']) && $_REQUEST['subscription_language'] == "fr_CA") {
                    $lang = "fr_CA";
                    $lang_id = 0;
                    $langselected = "fr_CA";
                } else if (isset($_REQUEST['subscription_language']) && $_REQUEST['subscription_language'] == "en_CA") {
                    $lang = "en_CA";
                    $lang_id = 1;
                    $langselected = "en_CA";
                }
                

                
	include('../theme/db.php');
	include('../objects/FormatData.php');
	include('../objects/Queries.php');
	include('../objects/DisplayHTML.php');
	
		//Initiate DisplayHTML
		$displayHTML = new DisplayHTML();
	
		// Initiate the Queries class
		$queries = new Queries();
		
		//Initiate FormatData
		$formatData = new FormatData();

		// Language Query

		$language_query_string = 'select * from languages_dynamic ld, languages l, languages2 l2 where ld.lang_id = ' . $lang_id . ' and l.lang_id = ' . $lang_id . ' and l2.lang_id = ' . $lang_id;
		$language_query = mysql_query($language_query_string)or die("language query error: ". mysql_error());
		$language = mysql_fetch_array($language_query);



	   // Theme Query
	   $theme_query = mysql_query("select * from theme");
	   $theme = mysql_fetch_array($theme_query); 
	   
	   //setting locale for time, date, money, etc
	   setlocale(LC_ALL, $language['locale_string']);
	   date_default_timezone_set("Canada/Eastern");

           
        function startsWith($haystack, $needle) {
        // search backwards starting from haystack length characters from the end
        return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== FALSE;
        }

        function endsWith($haystack, $needle) {
            // search forward starting from end minus needle length characters
            return $needle === "" || strpos($haystack, $needle, strlen($haystack) - strlen($needle)) !== FALSE;
        }

        function get_query_str(){
		if ($_SERVER['QUERY_STRING'] == ""){
			return "";
		}else{
			return '&' . $_SERVER['QUERY_STRING'];
		}
	}
	
	function get_query_str_print(){
		if ($_SERVER['QUERY_STRING'] == ""){
			return "";
		}else{
			return $_SERVER['QUERY_STRING'];
		}
	}


        function translateProvince($province_selected) {
            $provTrans_query_string = "select french as fprovince from provinces where english = '".$province_selected."'";
            $provTrans_query = mysql_query($provTrans_query_string) or die ("prov trans query error: " . mysql_error());
            $provTrans = mysql_fetch_array($provTrans_query);
            return $provTrans['fprovince'];
        }

        function translateType($SuiteType_selected) {
            $typeTrans_query_string = "SELECT building_type AS fsuitetype FROM building_types WHERE lang=0 AND sort_order = (SELECT sort_order FROM building_types WHERE building_type = '".$SuiteType_selected."' AND lang=1)";
            $typeTrans_query = mysql_query($typeTrans_query_string) or die ("region trans query error: " . mysql_error());
            $typeTrans = mysql_fetch_array($typeTrans_query);
            return $typeTrans['fsuitetype'];
        }

        function translateRegion($region_selected) {
            $regionTrans_query_string = "SELECT region AS fregion FROM buildings WHERE building_id = (SELECT building_id FROM buildings WHERE region = '".$region_selected."' AND lang = 1 LIMIT 1) AND lang=0";
            $regionTrans_query = mysql_query($regionTrans_query_string) or die ("region trans query error: " . mysql_error());
            $regionTrans = mysql_fetch_array($regionTrans_query);
            return $regionTrans['fregion'];
        }

        function translateSubRegion($sub_region_selected) {
            $subregionTrans_query_string = "SELECT sub_region AS fsubregion FROM buildings WHERE building_id = (SELECT building_id FROM buildings WHERE sub_region = '".$sub_region_selected."' AND lang = 1 LIMIT 1) AND lang=0";
            $subregionTrans_query = mysql_query($subregionTrans_query_string) or die ("subregion trans query error: " . mysql_error());
            $subregionTrans = mysql_fetch_array($subregionTrans_query);
            return $subregionTrans['fsubregion'];
        }

        function translateSize($size_selected) {
            $sizeTrans_query_string = "SELECT size_display AS fsize FROM sizes WHERE maximum = (SELECT maximum FROM sizes WHERE size_display = '".$size_selected."' AND lang=1) AND lang=0";
            $sizeTrans_query = mysql_query($sizeTrans_query_string) or die ("size trans query error: " . mysql_error());
            $sizeTrans = mysql_fetch_array($sizeTrans_query);
            return $sizeTrans['fsize'];
        }

        function translateAvailability($availability_selected) {
            $availabilityTrans_query_string = "SELECT availabilities_display AS favailability FROM availabilities WHERE lang = 0 AND availability_index = (SELECT availability_index FROM availabilities WHERE availabilities_display = '".$availability_selected."' AND lang=1)";
            $availabilityTrans_query = mysql_query($availabilityTrans_query_string) or die ("subregion trans query error: " . mysql_error());
            $availabilityTrans = mysql_fetch_array($availabilityTrans_query);
            return $availabilityTrans['favailability'];
        }


        function buOnTheFlyTitle($province_selected, $SuiteType_selected, $region_selected, $sub_region_selected, $size_selected, $availability_selected, $lang) {

            if ($province_selected != "All" && $province_selected != '') {
                $title_province = $province_selected;
                if ($lang == "fr_CA") {
                    $title_province = translateProvince($province_selected);
                } else {
                    $title_province = $province_selected;
                }
            }   

            if ($SuiteType_selected != "All" && $SuiteType_selected != '') {
                if ($title_province != '') {
                    $title_type .= " | ";
                }
                if ($lang == "fr_CA") {
                    $title_type .= translateType($SuiteType_selected);
                } else {
                    $title_type .= $SuiteType_selected;
                }
            }

            if ($region_selected != "All" && $region_selected != '') {
                if ($title_province != '') {
                    $title_region .= " | ";
                }
                if ($lang == "fr_CA") {
                    $title_region .= translateRegion($region_selected);
                } else {
                    $title_region .= $region_selected;
                }
            }   

            if ($sub_region_selected != "All" && $sub_region_selected != '') {
                if ($title_province != '') {
                    $title_subregion .= " | ";
                }
                if ($lang == "fr_CA") {
                    $title_subregion .= translateSubRegion($sub_region_selected);
                } else {
                    $title_subregion .= $sub_region_selected;
                }
            }   

            if ($size_selected != "All" && $size_selected != '') {
                if ($title_province != '') {
                    $title_size .= " | ";
                }
                if ($lang == "fr_CA") {
                    $title_size .= translateSize($size_selected);
                } else {
                    $title_size .= $size_selected;
                }
            }   

            if ($availability_selected != "All" && $availability_selected != '') {
                if ($title_province != '') {
                    $title_availability .= " | ";
                }
                if ($lang == "fr_CA") {
                    $title_availability .= translateAvailability($availability_selected);
                } else {
                    $title_availability .= $availability_selected;
                }

            }   

            $onthefly_title = $title_province . $title_type . $title_region . $title_subregion . $title_size . $title_availability;

            if ($onthefly_title == '') {
                if ($lang == "fr_CA") {
                    $onthefly_title = "Toutes les inscriptions";
                } else {
                    $onthefly_title = "All Listings";
                }
                
                
            }
            return $onthefly_title;
        }

$title_province = '';
$title_region = '';
$title_subregion = '';
$title_type = '';
$title_size = '';
$title_availability = '';

	// get submitted email address
		if (isset($_REQUEST['email'])){				
			$bu_email = $_REQUEST['email'];
		}
		$bu_action='';
		if (isset($_REQUEST['action'])){				
			$bu_action = $_REQUEST['action'];
		}
		
		
		if (isset($_REQUEST['province'])){
			$province_selected = $_REQUEST['province'];
		}else{
			if (isset($_REQUEST['lang']) && $_REQUEST['lang'] == 'fr_CA'){
				$province_selected = "Tous";
			}else{
				$province_selected = "All";
			}
		}

		if (isset($_REQUEST['SuiteType'])){
			$SuiteType_selected = $_REQUEST['SuiteType'];
		}else{
			$SuiteType_selected = "All";
		}

		if (isset($_REQUEST['region'])){				
			$region_selected = $_REQUEST['region'];
		}else{
			$region_selected = "All";
		}
		
		if (isset($_REQUEST['sub-region'])){				
			$sub_region_selected = $_REQUEST['sub-region'];
		}else{
			$sub_region_selected = "All";
		}
		
		if (isset($_REQUEST['Size'])){
			$size_selected = $_REQUEST['Size'];
		}else{
			$size_selected = "All";
		}
		
		if (isset($_REQUEST['Availability'])){
			$availability_selected = $_REQUEST['Availability'];
			switch($availability_selected){
				
				case 'All':
					$availability_notafter = 2147483640;
					$availability_notbefore = 0; 
					break;
				case 'Immediately':
					$availability_notafter = time();
					$availability_notbefore = 0; 
					break;
				case 'Under 3 Months':
					$availability_notafter = strtotime('+3 Month');
					$availability_notbefore = 0; 
					break;
				case '3-6 Months':
					$availability_notafter = strtotime('+6 Month');
					$availability_notbefore = strtotime('+3 Month');
					break;
				case '6-12 Months':
					$availability_notafter = strtotime('+1 Year');
					$availability_notbefore = strtotime('+6 Month');
					break;
				case 'Over 12 Months':
					$availability_notafter = 2147483640;
					$availability_notbefore = strtotime('+1 Year');
					break;
				case 'Tous':
					$availability_notafter = 2147483640;
					$availability_notbefore = 0; 
					break;
				case 'Immédiatement':
					$availability_notafter = time();
					$availability_notbefore = 0; 
					break;
				case 'Moins de 3 mois':
					$availability_notafter = strtotime('+3 Month');
					$availability_notbefore = 0; 
					break;
				case '3-6 mois':
					$availability_notafter = strtotime('+6 Month');
					$availability_notbefore = strtotime('+3 Month');
					break;
				case '6-12 mois':
					$availability_notafter = strtotime('+1 Year');
					$availability_notbefore = strtotime('+6 Month');
					break;
				case 'Plus de 12 mois':
					$availability_notafter = 2147483640;
					$availability_notbefore = strtotime('+1 Year');
					break;
			}
			
		}else{
			$availability_notafter = 2147483640;
			$availability_notbefore = 0; 
		}


		$uniqueid = $_REQUEST['unique'];

	//this adds a variable to the query string gracefully.
	function append_to_query_str($query_string, $var, $appendee){
		if ($query_string == "" || $query_string == "&"){
			return $var . "=" . $appendee;
		}else{
			return $query_string . "&" . $var . "=" . $appendee;
		}
	}
                                
        
        function multiexplode ($delimiters,$string) {
   
    $ready = str_replace($delimiters, $delimiters[0], $string);
    $launch = explode($delimiters[0], $ready);
    return  $launch;
}



		// Broker Updates subscription query
		
                $bu_sub_query_string = "select * from bu_subscriptions where unique_id = '".$uniqueid."'";
		$bu_sub_query = mysql_query($bu_sub_query_string);
		$bu_sub = mysql_fetch_array($bu_sub_query);
                $bu_sub_title = $bu_sub['title'];
                while(startsWith($bu_sub_title," |")){$bu_sub_title = substr($bu_sub_title, 3);}
                while(endsWith($bu_sub_title," |")){$bu_sub_title = substr($bu_sub_title, 0,-2);}
                if ($bu_sub_title==''){$bu_sub_title=$language['all_listings'];}
                $bu_sub['title'] = $bu_sub_title;
                
                


	// ACTION items

	$vanilla = 1;

	// Unsubscribe
	$confirm = 0;
	if ($bu_action == "confirm") {
		$confirm = 1;	
	}
	
	$unsubscribed = 0;
	if ($bu_action == "unsub") {
//		// BU Unsubscribe
		$bu_unsub_query = "update bu_subscriptions set active='0' where unique_id = '".$uniqueid."'";
                $bu_unsub = mysql_query($bu_unsub_query);
                $bu_time_accounting_query = "update bu_subscriptions set updated_at =  '". date("Y/m/d, g:ia") . "' where unique_id = '".$uniqueid."'";
		$bu_time_accounting = mysql_query($bu_time_accounting_query);
		$unsubscribed = 1;
		$vanilla = 0;
	}
	
	
	// Edit
	$edited = 0;
	if ($bu_action == "edit") {
		$province = $_REQUEST['province'];
		$region = $_REQUEST['region'];
		$sub_region = $_REQUEST['sub-region'];
		$SuiteType = $_REQUEST['SuiteType'];
		$size = $_REQUEST['Size'];
		$availability = $_REQUEST['Availability'];
		
		// Build Report title
		
		$title = '';

		if ($province_selected != 'All') {
			$title .= $province_selected;
		}
		if ($region_selected != 'All') {
			$title .= ' | '.$region_selected;
		}
		if ($sub_region_selected != 'All') {
			$title .= ' | '. $sub_region_selected;
		}
		if ($SuiteType_selected != 'All') {
			$title .= ' | '. $SuiteType_selected;
		}
		if ($size_selected != 'All') {
			$title .= ' | '. $size_selected;
		}
		if ($availability_selected != 'All') {
			$title .= ' | '. $availability_selected;
		}
	
		if (($province_selected == 'All') && ($region_selected == 'All') && ($sub_region_selected == 'All') && ($SuiteType_selected == 'All') && ($size_selected == 'All') && ($availability_selected == 'All')) {
			
			$title = $language['all_listings'];
		}
		
                while(startsWith($title," |")){$title = substr($title, 3);}
                if ($title==''){$title=$language['all_listings'];}

		
		$edited = 1;
		$vanilla = 0;
	}
        
    
        // Change Subscription Language (global)
	$langchanged = 0;
	if ($bu_action == "languagechange") {
            $newlang = $lang;
            $oldlang = "en_CA";
            if ($newlang == "en_CA"){
                $oldlang = "fr_CA";
            } else {
                $oldlang = "en_CA";
            }
            $email = $_REQUEST['email'];
            $bu_languagechange_query = "update bu_subscriptions set subscription_language='" . $newlang . "' where email = '" . $email . "'";
            $bu_languagechange = mysql_query($bu_languagechange_query);
            
            $bu_languagechange2_query = "update bu_subscriptions set bu_query_string = replace(bu_query_string, '" . $oldlang . "', '" . $newlang . "') where instr(bu_query_string, '" . $oldlang . "') > 0 and email = '" . $email . "'";
            $bu_languagechange2 = mysql_query($bu_languagechange2_query);
            
            $bu_time_accounting_query = "update bu_subscriptions set updated_at =  '". date("Y/m/d, g:ia") . "' where email = '" . $email . "'";
            $bu_time_accounting = mysql_query($bu_time_accounting_query);
            
            
            $langchanged = 1;
            $vanilla = 0;
        }


        if (isset($_REQUEST['subscription_frequency']) && $_REQUEST['subscription_frequency'] !='') {
            if ($_REQUEST['subscription_frequency'] == "Monthly") {
                $freqselected = $language['email_frequency_monthly_text'];
                $freqenglish = $_REQUEST['subscription_frequency'];
            } else if ($_REQUEST['subscription_frequency'] == "Weekly") {
                $freqselected = $language['email_frequency_weekly_text'];
                $freqenglish = $_REQUEST['subscription_frequency'];
            } else if ($_REQUEST['subscription_frequency'] == "Daily") {
                $freqselected = $language['email_frequency_daily_text'];
                $freqenglish = $_REQUEST['subscription_frequency'];
            } else if ($_REQUEST['subscription_frequency'] == "") {
                $freqselected = $language['email_frequency_daily_text'];
                $freqenglish = $_REQUEST['subscription_frequency'];
            }
        } else {
            $freqselected = $bu_sub['subscription_frequency'];
            $freqenglish = $bu_sub['subscription_frequency'];
        }
    
        

        // Change Subscription Frequency (global)
	$freqchanged = 0;
	if ($bu_action == "frequencychange") {
            $newfreq = $_REQUEST['subscription_frequency'];

            $email = $_REQUEST['email'];
            $bu_frequencychange_query = "update bu_subscriptions set subscription_frequency='" . $newfreq . "' where email = '" . $email . "'";
            $bu_frequencychange = mysql_query($bu_frequencychange_query);
            
            $bu_time_accounting_query = "update bu_subscriptions set updated_at =  '". date("Y/m/d, g:ia") . "' where email = '" . $email . "'";
            $bu_time_accounting = mysql_query($bu_time_accounting_query);
            $freqchanged = 1;
            $vanilla = 0;
        }

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!--      <meta name="viewport" content="width=<?php // echo $wrapper_width; ?>" />-->
      <title><?php echo $language['page_title']; ?></title>
      <link rel="shortcut icon" href="../theme/favicon.ico" type="image/x-icon" />
      <link rel="stylesheet" type="text/css" href="../css/reset.css" />
      <link rel="stylesheet" type="text/css" href="../css/styles.css" />
      <?php 
      $_REQUEST['colorlang'] = $lang;
      include('../css/colour_styles.php'); ?>

      <script src="../lightbox/js/jquery-1.10.2.min.js"></script>
      <script type="text/javascript" src="../js/jquery-ui-1.8.13.custom.min.js"></script>
      <script type="text/javascript" src="../js/jquery.pagination.js"></script> 

      <script type="text/javascript" src="../js/jquery.tools.min.js"></script>
      <script type="text/javascript" src="../js/jquery.jdialogue.js"></script>


<script type="text/javascript"> 
	function addslashes (str) {
		// Escapes single quote, double quotes and backslash characters in a string with backslashes  
		// 
		// version: 1103.1210
		// discuss at: http://phpjs.org/functions/addslashes    // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
		// +   improved by: Ates Goral (http://magnetiq.com)
		// +   improved by: marrtins
		// +   improved by: Nate
		// +   improved by: Onno Marsman    // +   input by: Denny Wardhana
		// +   improved by: Brett Zamir (http://brett-zamir.me)
		// +   improved by: Oskar Larsson Högfeldt (http://oskar-lh.name/)
		// *     example 1: addslashes("kevin's birthday");
		// *     returns 1: 'kevin\'s birthday'    
		return (str + '').replace(/[\\"']/g, '\\$&').replace(/\u0000/g, '\\0');
	}


            
	// When document is ready, initialize pagination
	$(document).ready(function(){     


	
	
	
		
	//END DOCUMENT READY	
	});



</script>


<style>
body {
	
}

table.summarytable {
    margin-bottom: 20px;
    background: #efefef;
    border-top: 1px solid #ccc;	
}

table.summarytable td {
    font-size: 14px;
    padding: 5px;
    width: 120px;
}

table.summarytable td.summarydata {
    font-size: 14px;
    padding: 5px;
    width: 200px;
}

ol.subscriptions {
    font-size: 14px;	
}

ol.subscriptions li {
    margin-left: 30px;
    margin-bottom: 15px;	
}

.unsub_message {
    background: <?php echo $theme['secondary_colour_hex']; ?>;
    margin: 30px 0 30px 0;
    color: #fff;
}

span.unsub_title {
    font-weight: bold;
    font-size: 1.3em;
}

p {
    margin: 0px;
}

/*.li_link {
	float: left;
}

.li_edit {
	float: right;	
}

.li_unsubscribe {
	float: right;	
}
*/



select#subscription_frequency {
  width: 100%;
}

select#subscription_language {
  width: 100%;
}
</style>


</head>
 
<body>
 <?php 

 		$uri_sub_query_string = "select * from bu_subscriptions where email = '".$bu_sub['email']."' and active = '1'";
		$uri_sub_query = mysql_query($uri_sub_query_string);
		$uri_sub = mysql_fetch_array($uri_sub_query);
                                
  
$uri = 'unique='.$uri_sub['unique_id'].'&'.$uri_sub['bu_query_string'].'&bu_name='.$uri_sub['bu_name'].'&email='.$uri_sub['email'].'&oo_manage=manage'; 

 
$url = 'http'.(empty($_SERVER['HTTPS'])?'':'s').'://'.$_SERVER['SERVER_NAME']."/index.php?".$uri;


$englishUrl = $url."&lang=en_CA";
$frenchUrl = $url."&lang=fr_CA";

$switchToEnglish = '<span style="margin: 0px auto;font-size: 15px;"><a target="_top" href="'.$englishUrl.'">English</a> - <span>';
$switchToFrench = '<span style="margin: 0px auto;font-size: 15px;"><a target="_top" href="'.$frenchUrl.'">Français</a><span>';
$switchToEnglishNoLink = '<span style="margin: 0px auto;font-size: 15px;"><a>English</a> - <span>';
$switchToFrenchNoLink = '<span style="margin: 0px auto;font-size: 15px;"><a>Français</a><span>';




?>
 
    <p class="modal-copy">

    </p>
     
    
      	<div>

 
         <div class="clear"></div>
 

      
	<div class="clearfix">
    
  <h1 class="section-title"><?php echo $language['manage_daily_subscriptions']; ?></h1>


<?php   



// Other Subscriptions

$os_query_string = "select * from bu_subscriptions where email = '".$bu_sub['email']."' and active='1'";
$os_query = mysql_query($os_query_string);
$os_query2 = mysql_query($os_query_string);
$os_count = mysql_num_rows($os_query);


?>
  
  
<?php if ($unsubscribed == 1) { 

    $unsub_query_string = "select * from bu_subscriptions where unique_id = '". $_REQUEST['unique']."'";
    $unsub_query = mysql_query($unsub_query_string);
    $unsub = mysql_fetch_array($unsub_query);
?>  

<div class="clear"></div>  
<div class="unsub_message">  
<p class="modal-copy" style="font-size:15px;"><?php echo $language['unsubscribed_from_text']; ?> </p>

<p class="modal-copy" style="font-size:15px;"><?php echo $language['no_more_updates_text'] . ' : <span class="unsub_title">' . $unsub['title'] . '</span>'?></p>


</div> <!-- end unsub_message div -->

<?php } //end if ($unsubscribed == 1)  ?>


<?php if ($langchanged == 1) { 
    
    if ($newlang == "fr_CA") {
        $langname = "French";
    } else {
        $langname = "English";
    }
    
    
    ?>
<p class="modal-copy" style="font-size:15px;"><?php echo $language['language_change_thankyou_text'] ; ?></p>
<?php } //end if ($langchanged == 1)  ?>



<?php if ($freqchanged == 1) { ?>
<p class="modal-copy" style="font-size:15px;"><?php echo $language['frequency_change_thankyou_text'] . ' ' . $freqselected; ?></p>
<?php } //end if ($langchanged == 1)  ?>











<div class="clear"></div>
    <!-- eo clear //-->
<?php if ($os_count > 0) { ?>
    <p class="modal-copy" style="font-size:15px;"><?php echo $language['subscribed_as_text']; ?> <em><?php echo $bu_sub['email']; ?></em>.</p>
<br><p class="modal-copy" style="font-size:15px;"><?php echo $language['subscribed_to_reports']; ?></p>
<table border="0" width="100%" style="margin: 0px;"   class="summarytable">
<?php 
$count = 1;
while ($os = mysql_fetch_array($os_query)) {
    
    
    // title
$qString = $os['bu_query_string'];

parse_str($qString, $subscription_qstring);



$province_selected = $subscription_qstring['province'];
$region_selected = $subscription_qstring['region'];
$sub_region_selected = $subscription_qstring['sub_region'];
$SuiteType_selected = $subscription_qstring['SuiteType'];
$size_selected = $subscription_qstring['size'];
$availability_selected = $subscription_qstring['availability'];


echo '<tr><td class="summarydata"><li style = "list-style-type:none;">'.$count.'. '. buOnTheFlyTitle($province_selected, $SuiteType_selected, $region_selected, $sub_region_selected, $size_selected, $availability_selected, $lang).'</a></li></td>';


$count++;  
      
$os['bu_query_string'] = str_replace("&lang=en_CA","",$os['bu_query_string']);
$os['bu_query_string'] = str_replace("&lang=fr_CA","",$os['bu_query_string']);
$os['bu_query_string'] = str_replace("&language=en_CA","",$os['bu_query_string']);
$os['bu_query_string'] = str_replace("&language=fr_CA","",$os['bu_query_string']);
$os['bu_query_string'] = $os['bu_query_string']."&lang=".$lang;
?>
<td>
<a class="bttn-sm bu_edit" href="./bu_edit.php?<?php echo $os['bu_query_string'] ?>&action=edit&unique=<?php echo $os['unique_id']; ?>&lang=<?php echo $os['subscription_language'];?>"><?php echo $language['edit_subscription_button_text']; ?></a>
<a class="bttn-sm bu_unsub" href="./bu_manage.php?<?php echo $os['bu_query_string'] ?>&action=unsub&unique=<?php echo $os['unique_id']; ?>" style="color: #333;"><?php echo $language['unsubscribe_button_text']; ?></a>
</td>
</tr>

<?php
	}
?>





<!-- Language and Frequency Toggles -->
    <tr>
        <td><?php echo $language['language_preference_text']; ?></td>
                <td>
                    <form name="languageform" id="languageform" method="get" action="bu_manage_list.php">
                        <input type="hidden" name="unique" value="<?php echo $uniqueid; ?>"></input>
                        <input type="hidden" name="email" value="<?php echo $bu_email; ?>"></input>
                        <input type="hidden" name="action" value="languagechange"></input>
                    <select name="subscription_language" id="subscription_language" class="subscription_language" onchange="this.form.submit();">
                        <option value=""><?php echo $language['language_change_placeholder_text']; ?></option>
                        <option <?php if($lang == 'en_CA'){echo("selected");}?> value="en_CA"><?php echo $language['english_text']; ?> </option>
                        <option <?php if($lang == 'fr_CA'){echo("selected");}?> value="fr_CA"><?php echo $language['french_text']; ?></option>
                    </select>
                    </form>
                </td>
    </tr>
    <tr>
        <td><?php echo $language['email_preference_text']; ?></td>
                <td>
                    <form name="frequencyform" id="frequencyform" method="get" action="bu_manage_list.php">
                        <input type="hidden" name="unique" value="<?php echo $uniqueid; ?>"></input>
                        <input type="hidden" name="email" value="<?php echo $bu_email; ?>"></input>
                        <input type="hidden" name="lang" value="<?php echo $lang; ?>"></input>
                        <input type="hidden" name="action" value="frequencychange"></input>
                    <select name="subscription_frequency" id="subscription_frequency" class="subscription_frequency" onchange="this.form.submit();">
                        <option value=""><?php echo $language['email_frequency_placeholder_text']; ?></option>
                        <option <?php if($freqenglish == 'Daily'){echo("selected");}?> value="Daily"><?php echo $language['email_frequency_daily_text']; ?></option>
                        <option <?php if($freqenglish == 'Weekly'){echo("selected");}?> value="Weekly"><?php echo $language['email_frequency_weekly_text']; ?></option>
                        <option <?php if($freqenglish == 'Monthly'){echo("selected");}?> value="Monthly"><?php echo $language['email_frequency_monthly_text']; ?></option>
                    </select>
                    </form>
                </td>
    </tr>
</table>
    

    
<?php 


} // end if $os_count 


else{
    ?>
    <p class="modal-copy" style="font-size:15px;"><?php  echo $language['no_subscriptions'];?> </p> <?php   
}
?> 
 

 

    </div>
    <!-- eo content //-->
    <div class="clear"></div>
    <!-- eo clear //-->
    <br />
    
    
    	
</div> 

</body>
</html>