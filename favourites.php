<?php

include('theme/db.php');
include('objects/LanguageQuery.php');
include('eclipse/classes/Queries.php');

include("assets/php/krumo/class.krumo.php");


require_once 'dist/Mobile_Detect.php';
$detect = new Mobile_Detect;

date_default_timezone_set('America/New_York');

// Initiate the Queries class
$queries = new Queries();
// Language Query
$languageQuery = new LanguageQuery();
$langArray = $languageQuery->getLanguageAndID();
$lang = $langArray[0];
$thelang = $langArray[0];
$lang_id = $langArray[1];
//$language_query_string = 'select * from languages2 ll, languages_dynamic ld, languages l  where ld.lang_id = ' . $lang_id . ' and l.lang_id = ' . $lang_id . ' and ll.lang_id = ' . $lang_id;
//$language_query = mysql_query($language_query_string) or die("language query error: " . mysql_error());
//$language = mysql_fetch_array($language_query);

$language = array();
$language_query_string = "select * from languages_dynamic ld, languages l, languages2 l2 where ld.lang_id =? and l.lang_id =? and l2.lang_id =?";
$language_query_statement = $db->prepare($language_query_string);
$language_query_statement->bind_param('iii', mysqli_real_escape_string($db, $lang_id), mysqli_real_escape_string($db, $lang_id), mysqli_real_escape_string($db, $lang_id));
if ($language_query_statement->execute()) {
    $result = $language_query_statement->get_result();
    while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
        $language = $row;
    }
}
$language_query_statement->close();


//           $language_query_string2 = 'select * from languages_dynamic ld, languages l, languages2 l2 where ld.lang_id = ' . $lang_id . ' and l.lang_id = ' . $lang_id . ' and l2.lang_id = ' . $lang_id . '';
//           $language_query2 = mysql_query($language_query_string2)or die("language query2 error: ". mysql_error());
//           $language2 = mysql_fetch_array($language_query2);   


$language2 = array();
$language2_query_string = "select * from languages_dynamic ld, languages l, languages2 l2 where ld.lang_id =? and l.lang_id =? and l2.lang_id =?";
$language2_query_statement = $db->prepare($language2_query_string);
$language2_query_statement->bind_param('iii', mysqli_real_escape_string($db, $lang_id), mysqli_real_escape_string($db, $lang_id), mysqli_real_escape_string($db, $lang_id));
if ($language2_query_statement->execute()) {
    $result = $language2_query_statement->get_result();
    while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
        $language2 = $row;
    }
}
$language2_query_statement->close();

// Theme Query
$theme_query = mysql_query("select * from theme");
$theme = mysql_fetch_array($theme_query);

//setting locale for time, date, money, etc
//            date_default_timezone_set('America/New_York');
setlocale(LC_ALL, $language['locale_string']);
// for reporting
$today = date("Y/m/d");
$todaytimestamp = strtotime($today);


// array of selected suites
$suites_selected = $_REQUEST['suites'];
//krumo($suites_selected);

$faveHTML = '';

$industrialPresent = 0;
foreach ($suites_selected as $suiteID) {
    $industrial_test_query = array();  
        $industrial_test_query_string = "SELECT suite_type FROM suites WHERE suite_id = ? AND lang = ?";
        if ($industrial_test_query_statement = $db->prepare($industrial_test_query_string)) {
            // carry on
        } else {
            trigger_error('Wrong SQL: ' . $industrial_test_query_string . ' Error: ' . $db->error, E_USER_ERROR);
        }
    $industrial_test_query_statement->bind_param('si', mysqli_real_escape_string($db, $suiteID), mysqli_real_escape_string($db, $lang_id));
    if ($industrial_test_query_statement->execute()){
            $result = $industrial_test_query_statement->get_result();
            while($row = $result->fetch_array(MYSQLI_ASSOC)){ 
                if (strcasecmp($row['suite_type'], "Industrial") == 0 || strcasecmp($row['suite_type'], "Industriel") == 0) {
                    $industrialPresent++;
                }
            }
    }  else {
            trigger_error('Wrong SQL: ' . $industrial_test_query_string . ' Error: ' . $db->error, E_USER_ERROR);
    }
    $industrial_test_query_statement->close();
    
    
}

foreach ($suites_selected as $favourite) {
    
        $suites_query = array();  
        $suites_query_string = "SELECT DISTINCT s.building_code, s.suite_id, s.suite_name, s.suite_type, s.net_rentable_area, s.contiguous_area, s.min_divisible_area, s.availability, s.notice_period, s.net_rent, ss.shipping_doors_drive_in, ss.shipping_doors_truck, ss.available_electrical_volts, ss.available_electrical_amps, ss.clear_height, sb.building_name, sb.street_address, sb.city, sb.province, sbs.costs_add_rent_total, sbs.costs_operating, sbs.costs_realty FROM suites s, suites_specifications ss, buildings sb, building_specifications sbs WHERE s.suite_id = ? AND ss.suite_id = s.suite_id AND s.building_code = sb.building_code AND sbs.building_code = s.building_code AND s.lang = ? AND sb.lang=s.lang AND sbs.lang=s.lang";
        if ($suites_query_statement = $db->prepare($suites_query_string)) {
            // carry on
        } else {
            trigger_error('Wrong SQL: ' . $suites_query_string . ' Error: ' . $db->error, E_USER_ERROR);
        }
        
        $suites_query_statement->bind_param('si', mysqli_real_escape_string($db, $favourite), mysqli_real_escape_string($db, $lang_id));
        if ($suites_query_statement->execute()){
            $result = $suites_query_statement->get_result();
            while($row = $result->fetch_array(MYSQLI_ASSOC)){ 
                $suites_query[] = $row;
            }
        } else {
            trigger_error('Wrong SQL: ' . $suites_query_string . ' Error: ' . $db->error, E_USER_ERROR);
        }
        $suites_query_statement->close();

//krumo($suites_query[0]);
        
        if ($suites_query[0]['suite_type'] == "INDUSTRIAL" || $suites_query[0]['suite_type'] == "INDUSTRIEL") {
            $industrialPresent++;
        }
        
// Set up English vs French number formatting
$suite_net_rentable_area = $languageQuery->suite_net_rentable_area($suites_query[0], $thelang);
$suite_contiguous_area = $languageQuery->suite_contiguous_area($suites_query[0], $thelang);
$suite_contiguous_area_formatted = $suite_contiguous_area;
$suite_min_divisible_area = $queries->get_suite_min_divisible($thelang, $suites_query[0]);
$suite_availability = $queries->get_suite_availability($language, $suites_query[0]);
$suite_net_rent = '';
if ($suites_query[0]['net_rent'] == "0" || $suites_query[0]['net_rent'] == "") {
    $suite_net_rent = $language['negotiable_text'];
} else {
    $suite_net_rent = money_format("%.2n", $suites_query[0]['net_rent']);
}

//$suite_contiguous_area_formatted = '';
//if ($lang == "en_CA") {
//    $suite_contiguous_area_formatted = number_format($suite_contiguous_area);
//}
//if ($lang == "fr_CA") {
//    $suite_contiguous_area_formatted = number_format($suite_contiguous_area, 0, ',', ' ');
//}

$areaOutput = $suite_net_rentable_area . ' ' . $language['building_information_sq_ft_trans'];

if ((intval($suite_contiguous_area) != 0) && (intval($suite_min_divisible_area) != 0)) {
    $areaOutput .= ' (' . $suite_min_divisible_area . ' | ' . $suite_contiguous_area_formatted . ')';
} else if ((intval($suite_contiguous_area) == 0) && (intval($suite_min_divisible_area) != 0)) {
    $areaOutput .= ' (' . $suite_min_divisible_area . ')';
} else if ((intval($suite_contiguous_area) != 0) && (intval($suite_min_divisible_area) == 0)) {
    $areaOutput .= ' (' . $suite_contiguous_area_formatted . ')';
}


if (($suites_query[0]['costs_operating '] == 0) || ($suites_query[0]['costs_operating '] == '')) {
    $building_add_rent_operating = 'n/a';
} else {
    $building_add_rent_operating = (money_format('%.2n', (double) $suites_query[0]['costs_operating ']));
}

if (($suites_query[0]['costs_realty '] == 0) || ($suites_query[0]['costs_realty '] == '')) {
    $building_add_rent_realty = 'n/a';
} else {
    $building_add_rent_realty = money_format('%.2n', (double) $suites_query[0]['costs_realty ']);
}

//if (($suites_query[0]['costs_power'] == 0) || ($suites_query[0]['costs_power'] == '')) {
//    $building_add_rent_power = 'n/a';
//} else {
//    $building_add_rent_power = money_format('%.2n', (double) $suites_query[0]['costs_power']);
//}

if (($suites_query[0]['costs_add_rent_total'] == 0) || ($suites_query[0]['costs_add_rent_total'] == '')) {
    $building_add_rent_total = 'n/a';
} else {
    $building_add_rent_total = money_format('%.2n', (double) $suites_query[0]['costs_add_rent_total']);
}

$loadingOut = '';
 if ($suites_query[0]['shipping_doors_drive_in'] === 'n/a' || $suites_query[0]['shipping_doors_drive_in'] === 0) { 
    $loadingOut = "n/a" ;
 } else {
    $loadingOut = $suites_query[0]['shipping_doors_drive_in'] . " D/I" ;
   }
 if ($suites_query[0]['shipping_doors_truck'] === 'n/a' || $suites_query[0]['shipping_doors_truck'] === 0) { 
    $loadingOut .= " / n/a" ;
 } else {
    $loadingOut .= " / " . $suites_query[0]['shipping_doors_truck'] . " DK" ;
    }

   
$elecOut = '';
 if ($suites_query[0]['available_electrical_amps'] === 'n/a' || $suites_query[0]['available_electrical_amps'] === 0) { 
    $elecOut = "n/a"; 
 } else {
    $elecOut = $suites_query[0]['available_electrical_amps'] . "a"; 
   }
if ($suites_query[0]['available_electrical_volts'] === 'n/a' || $suites_query[0]['available_electrical_volts'] === 0) { 
    $elecOut .= " / n/a" ;
 } else {
    $elecOut .= " / " . $suites_query[0]['available_electrical_volts'] . "v" ;
   }

$building_thumbnail_query_string = $queries->get_building_thumbnail_query_string($lang_id, $suites_query[0]['building_code']);
//    $building_thumbnail_query = mysql_query($building_thumbnail_query_string) or die ("building thumbnail image error: " . mysql_error());
//    $building_thumbnail = mysql_fetch_array($building_thumbnail_query);
// process buildings query string
$building_thumbnail__string = $building_thumbnail_query_string[0];
//    $building_thumbnail__string .=" LIMIT ".$postnumbers." OFFSET ".$offset;
$pageCurrent = '';
//    $building_thumbnail__string = str_replace("and allb.building_name like '%none%'", "", $building_thumbnail__string);
if ($building_thumbnail__statement = $db->prepare($building_thumbnail__string)) {
    // carry on
} else {
    trigger_error('Wrong SQL: ' . $building_thumbnail__string . ' Error: ' . $db->error, E_USER_ERROR);
}
// prep dynamic variable query
$param_type = '';
$n = count($building_thumbnail_query_string[1]);
for ($i = 0; $i < $n; $i++) {
    $param_type .= $building_thumbnail_query_string[1][$i];
}
$a_params = array();
//krumo($a_params);
/* with call_user_func_array, array params must be passed by reference */
$a_params[] = & $param_type;

for ($i = 0; $i < $n; $i++) {
    /* with call_user_func_array, array params must be passed by reference */
    $a_params[] = & $building_thumbnail_query_string[2][$i];
}

//krumo($a_params);
// call_user_func_array is how we can pass dynamic quantities of variables to mysqli prepared statements.
call_user_func_array(array($building_thumbnail__statement, 'bind_param'), $a_params);
$building_thumbnail = array();
if ($building_thumbnail__statement->execute()) {
    $result = $building_thumbnail__statement->get_result();
    while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
        $building_thumbnail = $row;
    }
} else {
    trigger_error('Could not execute building_thumbnail query statement: Error: ' . $db->error, E_ALL);
}
$building_thumbnail__statement->close();


$building_thumbnail_count = 0;
$building_thumbnail_image_name = '';
$building_thumbnail_thumb_name = '';
if (isset($building_thumbnail['file_name']) && $building_thumbnail['file_name'] != '') {
    $building_thumbnail_image_name = "images/preview_images/" . $building_thumbnail['file_name'];
    $building_thumbnail_thumb_name = "images/preview_images/" . str_replace('.jpg', '_thumb.jpg', $building_thumbnail['file_name']);
    $building_thumbnail_count = 1;
} else {
    $building_thumbnail_image_name = "images/no_thumb_200.png";
}

$faveHTML .= '
            <div data-suite-id="'.$favourite.'" class="fs_favourite fs_favourite-'.$favourite.' ui-sortable-handle">
                <div class="fs_favourite-header row">
                    <div class="col fs_favourite-thumbnail">
                        <a href="suite.php?building='.$suites_query[0]['building_code'].'&suiteid='.$suites_query[0]['suite_id'].'&lang='.$thelang.'"><img src="'.$building_thumbnail_image_name.'" alt="thumbnail" class="img-fluid"></a>
                        <div class="fs_favourite-remove">
                            <a href="#" class="fs_remove-favourite"><i class="material-icons fs_icon-clear">clear</i></a>
                        </div>
                    </div>
                    <div class="col d-lg-none fs_favourite-details-header">
                        <h5 class="fs_favourite-title"><a href="suite.php?building='.$suites_query[0]['building_code'].'&suiteid='.$suites_query[0]['suite_id'].'&lang='.$thelang.'">'.$suites_query[0]['building_name'].'</a></h5>
                        <h6 class="fs_favourite-address">'.$suites_query[0]['street_address'].' '.$suites_query[0]['city'].' '.$suites_query[0]['province'].'</h6>
                    </div>
                </div>
                <ul class="fs_favourite-details-list">
                    <li class="row d-none d-lg-flex">
                        <div class="col-6 d-lg-none"></div>
                        <div class="col-6 col-lg-12">
                            <h5 class="fs_favourite-title"><a href="suite.php?building='.$suites_query[0]['building_code'].'&suiteid='.$suites_query[0]['suite_id'].'&lang='.$thelang.'">'.$suites_query[0]['building_name'].'</a></h5>
                            <h6 class="fs_favourite-address">'.$suites_query[0]['street_address'].' '.$suites_query[0]['city'].' '.$suites_query[0]['province'].'</h6>
                        </div>
                    </li>
                    <li class="row">
                        <div class="col-6 d-lg-none">'.$language['suite_text'].'</div>
                        <div class="col-6 col-lg-12">'.$suites_query[0]['suite_name'].'</div>
                    </li>
                    <li class="row">
                        <div class="col-6 d-lg-none">'.$language['space_type_text'].'</div>
                        <div class="col-6 col-lg-12">'.$suites_query[0]['suite_type'].'</div>
                    </li>
                    <li class="row">
                        <div class="col-6 d-lg-none">'.$language['size_text_lowercase'].'</div>
                        <div class="col-6 col-lg-12">'.$areaOutput.'</div>
                    </li>
                    <li class="row">
                        <div class="col-6 d-lg-none">'.$language['price_text'].'</div>
                        <div class="col-6 col-lg-12">'.$suite_net_rent.'</div>
                    </li>
                    <li class="row">
                        <div class="col-6 d-lg-none">'.$language['index_table_header_availability_text'].'</div>
                        <div class="col-6 col-lg-12">'.$suite_availability.'</div>
                    </li>';

            if ($industrialPresent > 0) {
                    $faveHTML .= '
                        <li class="row">
                        <div class="col-6 d-lg-none">'.$language['loading_text'].'</div>
                        <div class="col-6 col-lg-12">'.$loadingOut.'</div>
                    </li>
                    <li class="row">
                        <div class="col-6 d-lg-none">'.$language['power_text'].'</div>
                        <div class="col-6 col-lg-12">'.$elecOut.'</div>
                    </li>
                    <li class="row">
                        <div class="col-6 d-lg-none">'.$language['ceiling_text'].'</div>
                        <div class="col-6 col-lg-12">'.($suites_query[0]['clear_height'] != '' ? $suites_query[0]['clear_height'] : "n/a").'</div>
                    </li>';
            }
            
            $faveHTML .= '                
                </ul>
                <ul class="fs_favourite-details-other-costs">
                    <li class="row">
                        <div class="col-6 d-lg-none">'.$language['additional_rent_text'].'</div>
                        <div class="col-6 col-lg-12"> '.$building_add_rent_total.'</div>
                    </li>
                    <li class="row">
                        <div class="col-6 d-lg-none">'.$language['operating_costs_text'].'</div>
                        <div class="col-6 col-lg-12"> '.$building_add_rent_operating.'</div>
                    </li>
                    <li class="row">
                        <div class="col-6 d-lg-none">'.$language['realty_tax_text'].'</div>
                        <div class="col-6 col-lg-12"> '.$building_add_rent_realty.'</div>
                    </li>
                    <li class="row d-lg-none">
                        <div class="col-6">
                            <a href="#" class="btn btn-primary btn-block fs_favourite-btn-share">
                                <i class="material-icons fs_btn-icon fs_btn-icon-share">share</i> <span class="fs_btn-text">'.$language['share_text_share'].'</span>
                            </a>
                        </div>
                        <div class="col-6">
                            <a href="#" class="btn btn-primary btn-block fs_favourite-btn-details">
                                <i class="material-icons fs_btn-icon fs_btn-icon-share">info</i> <span class="fs_btn-text">Details</span>
                            </a>
                        </div>
                    </li>
                </ul>
            </div>';
    
    
    
    
    
    
    
    
    
}

        $ipaddress = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '';
//        $hostname = ''; // too many pitfalls waiting for DNS which will kill the page load
        $referer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '';
        
        $browser = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '';
//        $screensize = ''; // only available via javascript. Need to ajax-ify this function if we want to capture it.
        $datetime = date("Y/m/d, g:ia", $_SERVER['REQUEST_TIME']) ; // format this YYYY/MM/DD

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="x-ua-compatible" content="IE=edge">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css?family=Fira+Sans+Condensed|Roboto" rel="stylesheet">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="stylesheet" href="lib/owlcarousel/2.2.1/dist/assets/owl.carousel.min.css">
        <link rel="stylesheet" href="lib/owlcarousel/2.2.1/dist/assets/owl.theme.default.min.css">
        <link rel="stylesheet" href="style.css">
        <link rel="stylesheet" href="favourites.css">
    </head>
    <body class="fs_page-favourites">
        <header id="fs_masthead-favourites" class="fs_site-header">
        <?php
        	
                echo '<div class="fs_client-header">';
                if ($lang_id == '1') {
                	include('theme/clientheader.php');
                } else {
                	if (file_exists('theme/clientheader_fr.php')) {
                    	include('theme/clientheader_fr.php');
                    }
					else {
                    	include('theme/clientheader.php');
                    }
              	} 
                echo '</div>';
        	
            ?>
            
        </header>  
        <main class="fs_site-main fs_favourites-main">
            <section class="fs_section fs_section-favourites">
                <h1 class="fs_page-title"><?php echo $language['favourites_text']; ?></h1>
                    <div class="fs_user-favourites">
                        <div class="fs_user-favourites-container">
                            <div class="row no-gutters">
                                <div class="fs_favourites-headings d-none d-lg-block col">
                                    <ul class="fs_favourite-utilities-list">
                                        <li><a href="#" class="fs_favourites-print fs_nav-link"><i class="material-icons fs_nav-link-icon fs_icon-print">print</i> <span class="fs_nav-link-text"><?php echo $language['print_text']; ?></span></a></li>
                                        <li><a href="#" class="fs_favourites-share fs_nav-link" data-toggle="modal" data-target="#fs_modal-share" ><i class="material-icons fs_nav-link-icon fs_icon-share">share</i> <span class="fs_nav-link-text"><?php echo $language['share_this_text']; ?></span></a></li>
                                    </ul>
                                    <ul class="fs_favourite-headings-list">
                                        <li><span class="sr-only"><?php echo $language['property_text']; ?> <?php echo $language['name_text']; ?></span></li>
                                        <li><?php echo $language['suite_text']; ?></li>
                                        <li><?php echo $language['type_text']; ?></li>
                                        <li><?php echo $language['size_text_lowercase']; ?>(<?php echo $language['building_information_sq_ft_trans']; ?>)</li>
                                        <li><?php echo $language['price_text']; ?> (<?php echo $language['price_text']; ?>)</li>
                                        <li><?php echo $language['index_table_header_availability_text']; ?></li>
                                    <?php if ($industrialPresent > 0) { ?>
                                        <li><?php echo $language['loading_text']; ?></li>
                                        <li><?php echo $language['power_text']; ?></li>
                                        <li><?php echo $language['ceiling_text']; ?></li>
                                    <?php } ?>
                                    </ul>
                                    <ul class="fs_favourite-headings-other-costs">
                                        <li><?php echo $language['additional_rent_text']; ?></li>
                                        <li><?php echo $language['operating_costs_text']; ?></li>
                                        <li><?php echo $language['realty_tax_text']; ?></li>
                                    </ul>
                                </div>
                                <div class="col-12 col-lg-10">
                                    <div id="fs_favourites-carousel" class="owl-carousel owl-theme">
                                        <!--$industrialPresent-->
                                        
                                        <?php echo $faveHTML; ?>
                                        
<!--                                        <div data-suite-id="1113696968301378357" class="fs_favourite fs_favourite-1113696968301378357 ui-sortable-handle">
                                            <div class="fs_favourite-header row">
                                                <div class="col fs_favourite-thumbnail">
                                                    <a href="#"><img src="images/no_thumb_200.png" alt="thumbnail" class="img-fluid"></a>
                                                    <div class="fs_favourite-remove">
                                                        <a href="#" class="fs_remove-favourite"><i class="material-icons fs_icon-clear">clear</i></a>
                                                    </div>
                                                </div>
                                                <div class="col d-lg-none fs_favourite-details-header">
                                                    <h5 class="fs_favourite-title"><a href="#">building 1324</a></h5>
                                                    <h6 class="fs_favourite-address">1 Yonge St Toronto Ontario</h6>
                                                </div>
                                            </div>
                                            <ul class="fs_favourite-details-list">
                                                <li class="row d-none d-lg-flex">
                                                    <div class="col-6 d-lg-none"></div>
                                                    <div class="col-6 col-lg-12">
                                                        <h5 class="fs_favourite-title"><a href="#">building 1324</a></h5>
                                                        <h6 class="fs_favourite-address">1 Yonge St Toronto Ontario</h6>
                                                    </div>
                                                </li>
                                                <li class="row">
                                                    <div class="col-6 d-lg-none">Suite #</div>
                                                    <div class="col-6 col-lg-12">100</div>
                                                </li>
                                                <li class="row">
                                                    <div class="col-6 d-lg-none"><?php // echo $language['size_text_lowercase']; ?></div>
                                                    <div class="col-6 col-lg-12">32,321 sf (321)</div>
                                                </li>
                                                <li class="row">
                                                    <div class="col-6 d-lg-none"><?php // echo $language['price_text']; ?></div>
                                                    <div class="col-6 col-lg-12">Negotiable</div>
                                                </li>
                                                <li class="row">
                                                    <div class="col-6 d-lg-none"><?php // echo $language['index_table_header_availability_text']; ?></div>
                                                    <div class="col-6 col-lg-12">Immediate</div>
                                                </li>
                                                <li class="row">
                                                    <div class="col-6 d-lg-none"><?php // echo $language['loading_text']; ?></div>
                                                    <div class="col-6 col-lg-12">undefined</div>
                                                </li>
                                                <li class="row">
                                                    <div class="col-6 d-lg-none"><?php // echo $language['power_text']; ?></div>
                                                    <div class="col-6 col-lg-12">undefined</div>
                                                </li>
                                                <li class="row">
                                                    <div class="col-6 d-lg-none"><?php // echo $language['ceiling_text']; ?></div>
                                                    <div class="col-6 col-lg-12">12ft</div>
                                                </li>
                                            </ul>
                                            <ul class="fs_favourite-details-other-costs">
                                                <li class="row">
                                                    <div class="col-6 d-lg-none"><?php // echo $language['additional_rent_text']; ?></div>
                                                    <div class="col-6 col-lg-12"> n/a</div>
                                                </li>
                                                <li class="row">
                                                    <div class="col-6 d-lg-none"><?php // echo $language['operating_costs_text']; ?></div>
                                                    <div class="col-6 col-lg-12"> n/a</div>
                                                </li>
                                                <li class="row">
                                                    <div class="col-6 d-lg-none"><?php // echo $language['realty_tax_text']; ?></div>
                                                    <div class="col-6 col-lg-12"> n/a</div>
                                                </li>
                                                <li class="row d-lg-none">
                                                    <div class="col-6">
                                                        <a href="#" class="btn btn-primary btn-block fs_favourite-btn-share">
                                                            <i class="material-icons fs_btn-icon fs_btn-icon-share">share</i> <span class="fs_btn-text"><?php // echo $language['share_text_share']; ?></span>
                                                        </a>
                                                    </div>
                                                    <div class="col-6">
                                                        <a href="#" class="btn btn-primary btn-block fs_favourite-btn-details">
                                                            <i class="material-icons fs_btn-icon fs_btn-icon-share">info</i> <span class="fs_btn-text">Details</span>
                                                        </a>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>-->
                                        
                                        
                                        
                                        
                                        
                                    </div>
                                </div>    
                            </div>
                        </div>
                    </div>
            </section>
        </main>
        
        
        
<div class="modal fade" id="fs_modal-share" tabindex="-1" role="dialog" aria-labelledby="fs_modal-share-label" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="fs_modal-share-label"><?php echo $language['favourites_share_modal_header']; ?></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <i class="material-icons">clear</i>
          </button>
        </div>
        <div class="modal-body">
            <div class="alert alert-success d-none" role="alert">Your favourites have been shared. Thank you!</div>
            <div class="alert alert-danger d-none" role="alert">There is an error with your form. Please correct it.</div>

          <form>
              <div class="form-group">
                  <label for="recipient-name" class="form-control-label"><?php echo $language['toname_placeholder_text']; ?></label>
                  <input type="text" class="form-control form-control-danger" id="recipient-name">
              </div>
              <div class="form-group has-feedback">
                  <label for="recipient-email" class="control-label"><?php echo $language['toemail_placeholder_text']; ?></label>
                  <input type="text" class="form-control form-control-danger" id="recipient-email">
              </div>
              <div class="form-group has-feedback">
                  <label for="sender-name" class="control-label"><?php echo $language['yourname_placeholder_text']; ?></label>
                  <input type="text" class="form-control form-control-danger" id="sender-name">
              </div>
              <div class="form-group has-feedback">
                  <label for="sender-email" class="control-label"><?php echo $language['youremail_placeholder_text']; ?></label>
                  <input type="text" class="form-control form-control-danger" id="sender-email">
              </div>

              <div class="form-group">
                  <label for="bshare-comments" class="control-label">Message:</label>
                  <textarea class="form-control" id="bshare-comments"></textarea>
              </div>
              <div class="form-group">
              <label for="cc-me" class="custom-control custom-checkbox">
                  <input id="cc-me" type="checkbox" name="cc-me" value="" checked="yes" class="custom-control-input">
                  <span class="custom-control-indicator"></span>
                  <span class="custom-control-description"><?php echo $language['send_a_copy_text']; ?></span>
              </label>
              </div>
          </form>


            <div class="modal-body-bottom">
              <input type="hidden" id="jsuseripaddress" value="<?php echo $ipaddress; ?>">
              <div class="g-recaptcha" id="fshare_captcha" data-sitekey=""></div>
              <button type="button" class="btn btn-primary btn-block btn-modal-share-submit">Share</button>
            </div>
        </div>
      </div>
    </div>
</div>
        
        
        
    
    <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>

    <script src="lib/jquery.ui.touch-punch.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
    <script src="lib/owlcarousel/2.2.1/dist/owl.carousel.min.js"></script>
    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.3.3/underscore-min.js"></script>
    
    <script src="eclipse/js/moment.min.js"></script>
    
    <!--<script type="text/javascript" src="main.js"></script>-->
    <script src="https://www.google.com/recaptcha/api.js?onload=CaptchaCallback&render=explicit" async defer></script>
    <script type="text/javascript">
        
        
var fshare_captcha;
var CaptchaCallback = function() {
    // Renders the HTML element with id 'example1' as a reCAPTCHA widget.
    // The id of the reCAPTCHA widget is assigned to 'widgetId1'.
//    tourcaptcha = grecaptcha.render('tourcaptcha', {
//        'sitekey' : '6LfgPBoUAAAAAHYde0AUWfiCTJlKXhyUTAcon9ko'
//    });
//    aitccaptcha = grecaptcha.render('aitccaptcha', {
//        'sitekey' : '6LfgPBoUAAAAAHYde0AUWfiCTJlKXhyUTAcon9ko'
//    });
    fshare_captcha = grecaptcha.render('fshare_captcha', {
        'sitekey' : '6LcsOjkUAAAAAOMAVbfLS3o8KUDhisF1yr4MvLZj'
    });
    
//    bshare_captcha = grecaptcha.render('bshare_captcha', {
//        'sitekey' : '6LcsOjkUAAAAAOMAVbfLS3o8KUDhisF1yr4MvLZj'
//    });
};
        $(document).ready(function () {
            
            var isMobile;
            var viewportwidth;
            var viewportheight;
            var owl;
            
            
            //get viewport width and height, set isMobile
            if (typeof window.innerWidth != 'undefined') {
                viewportwidth = window.innerWidth;
                viewportheight = window.innerHeight;

                if (viewportwidth > 991) {
                    isMobile = false;
                } else {
                    isMobile = true;
                }
            }

            var addEvent = function (object, type, callback) {
                if (object == null || typeof (object) == 'undefined')
                    return;
                if (object.addEventListener) {
                    object.addEventListener(type, callback, false);
                } else if (object.attachEvent) {
                    object.attachEvent("on" + type, callback);
                } else {
                    object["on" + type] = callback;
                }
            };
            
            function updateFavourites() {
                //console.log('update favourites');
                //console.log(isMobile);
                //console.log(favCount);

                if (isMobile) {
                    
                        //remove favourites-carousel and return markup to initial state
                        //$("#fs_favourites-carousel").sortable("destroy");

                        //intialize carousel
                        owl = $('.owl-carousel');
                        owl.owlCarousel({
                            loop: false,
                            margin: 10,
                            nav: false,
                            dots: false,
                            slideBy: 1,
                            responsive: {
                                0: {
                                    items: 1,
                                    slideBy: 1
                                },
                                640: {
                                    items: 2,
                                    slideBy: 1
                                }
                            }
                        });
                    

                } else {
                   
                        //these 3 lines kill owl carousel, and returns the markup to the initial state
                        owl.trigger('destroy.owl.carousel');
                        owl.find('.owl-stage-outer').children().unwrap();
                        owl.removeClass("owl-center owl-loaded owl-text-select-on");
                        
                        var favouriteMargin = parseInt($('.fs_favourite').css('marginLeft'));
                        
                        if(favouriteMargin!==0) {
                            //removes the negative margin we added to remove white space between inline block elements.
                            //Owl carousel fixes it so after we destroy we can remove negative margin (it's only needed if load on desktop first)
                            $('.fs_favourite').css('marginLeft',0); 
                        }

                        //intialize favourites-carousel
                        //$("#fs_favourites-carousel").sortable();
                    
                }
            }

            //window resize listener
            addEvent(window, "resize", function (event) {
                if (typeof window.innerWidth != 'undefined') {
                    viewportwidth = window.innerWidth,
                            viewportheight = window.innerHeight

                    if (viewportwidth < 992 && isMobile == false) {
                        isMobile = true;

                        //switch from desktop to mobile viewport, update favourites layout/functionality

                        updateFavourites();


                    } else if (viewportwidth > 992 && isMobile == true) {
                        isMobile = false;
                        //switch from mobile to desktop viewport, update favourites layout/functionality

                        updateFavourites();

                    }
                    //window.location.reload(false); 
                }
            });
            //document.write('<p>Your viewport width is '+viewportwidth+'x'+viewportheight+'</p>');
        
            
            if (isMobile) {
                    
                //remove favourites-carousel and return markup to initial state
                //$("#fs_favourites-carousel").sortable("destroy");

                //intialize carousel
                owl = $('.owl-carousel');
                owl.owlCarousel({
                    loop: false,
                    margin: 10,
                    nav: false,
                    dots: false,
                    slideBy: 1,
                    responsive: {
                        0: {
                            items: 1,
                            slideBy: 1
                        },
                        640: {
                            items: 2,
                            slideBy: 1
                        }
                    }
                });
                    

            }






$('#fs_modal-share').on('hidden.bs.modal', function (e) {
    $('#fs_modal-share input').parent().removeClass('alert alert-danger');
});

    $('#fs_modal-share .btn-modal-share-submit').on('click', function(e) {
        $('#fs_modal-share input').parent().removeClass('alert alert-danger');

        var urlToShare = window.location.origin+"/favourites.php";

        var uu=0;
        _.each($("#fs_favourites-carousel").children("div"), function(i) {            
            if (uu===0){
                urlToShare += "?suites[]="+$(i).data("suite-id");
            } else {
                urlToShare += "&suites[]="+$(i).data("suite-id");
            }
            uu++;
        });

        
        
        
//        console.log("urlToShare");
//        console.log(urlToShare);

        var tname = $('#fs_modal-share #recipient-name').val();
        var temail = $('#fs_modal-share #recipient-email').val();
        var fname = $('#fs_modal-share #sender-name').val();
        var femail = $('#fs_modal-share #sender-email').val();
        var fcomments = $('#fs_modal-share #bshare-comments').val();


        var googleinfo = grecaptcha.getResponse(fshare_captcha);

        var error = 0;

        if(tname === '') {
            $('#fs_modal-share #recipient-name').parent().addClass('alert alert-danger');
//            $('#fs_modal-share .alert-danger').removeClass('d-none');
            error = error+1;
        }

        if(temail === '') {
            $('#fs_modal-share #recipient-email').parent().addClass('alert alert-danger');
//            $('#fs_modal-share .alert-danger').removeClass('d-none');
            error = error+1;
        }

        if(fname === '') {
            $('#fs_modal-share #sender-name').parent().addClass('alert alert-danger');
//            $('#fs_modal-share .alert-danger').removeClass('d-none');
            error = error+1;
        }

        if(femail === '') {
            $('#fs_modal-share #sender-email').parent().addClass('alert alert-danger');
//            $('#fs_modal-share .alert-danger').removeClass('d-none');
            error = error+1;
        }
        
//console.log("error");
//console.log(error);
        if (error === 0) {            
            $.ajax({
                type: "GET",
                url: "verifyemail.php",
                data: {fromemail:femail,toemail:temail},
                beforeSend: function() {
                    $('#fs_modal-share .modal-body').prepend('<div class="loader"><img src="images/loader.svg"></div>');
                },
                success: function(data){
                    
                },
                error: function(){
                    
                }
            }).done( function(data) {
                if (data === '66') {
                    $('.loader').remove();
                    $('#fs_modal-share #recipient-email').parent().addClass('alert alert-danger');
                    $('#fs_modal-share .alert-danger').removeClass('hidden');
                    error = error+1;
                } else if (data === '77') {
                    $('.loader').remove();
                    $('#fs_modal-share #sender-email').parent().addClass('alert alert-danger');
                    $('#fs_modal-share .alert-danger').removeClass('hidden');
                    error = error+1;
                } else if (data === '88') {
                    $('.loader').remove();
                    $('#fs_modal-share #recipient-email').parent().addClass('alert alert-danger');
                    $('#fs_modal-share #sender-email').parent().addClass('alert alert-danger');
                    $('#fs_modal-share .alert-danger').removeClass('hidden');
                    error = error+1;
                } else {
                    $('#fs_modal-share input').parent().removeClass('alert alert-danger');
                    // email verified in PHP OK - start recaptcha
                    // console.log("recaptcha started");
                    var googleinfo = grecaptcha.getResponse(fshare_captcha);
                    var ipaddress = $("#jsuseripaddress").val();

                    $.ajax({
                        type: "GET",
                        url: "docaptcha.php",
                        data: {response: googleinfo, remoteip:ipaddress},
                        success: function(data){

                        },
                        error: function(){

                        }
                    }).done(function(data) {

                        var newdata = JSON.parse(data);
                        var reply = newdata[0][0];

                        if (newdata[0].length === 2) {
                            var error = newdata[0][1][0];
                        }

                        if (reply === '66') {
                            $('.loader').remove();
                            alert("Are you sure you're not a robot? Please try again.")
                            error = error+1;
                        } else {
                            var ccme = false;
                            if ($("#cc-me").is(':checked')) {
                                ccme = true;
                            }
                            // captcha succes, send email
                            $.ajax({
                                type: "GET",
                                url: "share_a_link.php",
                                data: {toname:tname,toemail:temail,fromname:fname,fromemail:femail,fromcomments:fcomments,urltoshare:urlToShare,requesttype:"Favourite",ccme:ccme},
                                    beforeSend: function() {

                                    },
                                    complete: function(){
                                        //hide preloader
                                        $('.loader').remove();
                                    },
                                    success: function(data){

                                    },
                                    error: function(){

                                    }
                            }).done(function(data) {
                                // console.log("data returned from sentctcrequest.php: ");
                                // console.log(data);
                                $('#fs_modal-share #sender-email, #fs_modal-share #sender-name').parent().removeClass('alert alert-danger');
                                if(data == 66) {
                                    //email error
                                    $('#fs_modal-share #sender-email').parent().addClass('alert alert-danger');
                                    $('#fs_modal-share .alert-danger').removeClass('hidden');
                                }
                                else {
                                    $('#fs_modal-share #sender-email, #fs_modal-share #sender-name').parent().removeClass('alert alert-danger');
                                    $('#fs_modal-share .alert-success').removeClass('hidden');
                                    $('#fs_modal-share .alert-danger').addClass('hidden');
                                    $('textarea, :input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
                                    $('#fs_modal-share').modal('toggle');
                                }
                            });
                        } // end else
                    });
                } //end else 
            });
        }
    });




$('.fs_favourites-print').on('click', function(e) {
     
        var favePdfLink = 'printpdfFavourites.php';
        var uu=0;
        _.each($("#fs_favourites-carousel").children("div"), function(i) {            
            if (uu===0){
                favePdfLink += "?suites[]="+$(i).data("suite-id");
            } else {
                favePdfLink += "&suites[]="+$(i).data("suite-id");
            }
            uu++;
        });
        
//        console.log("favePdfLink");
//        console.log(favePdfLink);
        
//        var win = window.open(this.href+newQstring, '_blank');
//        win.focus();
        window.open(favePdfLink);
        
});


              

        });//eo document.ready
    </script>    
    </body>
</html>